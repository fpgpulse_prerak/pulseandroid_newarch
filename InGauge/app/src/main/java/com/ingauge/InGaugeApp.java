package com.ingauge;

import android.app.Application;
import android.os.StrictMode;

/*import com.crashlytics.android.Crashlytics;*/
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ingauge.session.InGaugeSession;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.squareup.leakcanary.LeakCanary;


/**
 * Created by mansurum on 27-Apr-17.
 */

public class InGaugeApp extends Application{

    private static InGaugeApp instance;
    private static FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(){
        super.onCreate();
        //Fabric.with(this, new Crashlytics());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        instance = this;
        initializeInstance();
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .build();
/*        if(LeakCanary.isInAnalyzerProcess(this)){
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);*/
        // Normal app init code...
    }

    void initializeInstance(){
        InGaugeSession.init(getInstance());
    }

    public static InGaugeApp getInstance(){
        return instance;
    }

    public static FirebaseAnalytics getFirebaseAnalytics(){
        return mFirebaseAnalytics;
    }
}
