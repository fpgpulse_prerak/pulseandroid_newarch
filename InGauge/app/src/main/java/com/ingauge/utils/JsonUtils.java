package com.ingauge.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by mansurum on 20-Sep-17.
 */

public class JsonUtils {

    public static Map<String, Object> jsonToMap(JsonObject json) {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != null) {
            try {
                retMap = toMap(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JsonObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keySet().iterator();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JsonArray) {
                value = toList((JsonArray) value);
            }

            else if(value instanceof JsonObject) {
                value = toMap((JsonObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JsonArray array) {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.size(); i++) {
            Object value = array.get(i);
            if(value instanceof JsonArray) {
                value = toList((JsonArray) value);
            }

            else if(value instanceof JsonObject) {
                try {
                    value = toMap((JsonObject) value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            list.add(value);
        }
        return list;
    }

    public static List<Object> jsonArray2List( JSONArray arrayOFKeys ) throws JSONException{
        System.out.println("Incoming value is of JSONArray : =========");
        List<Object> array2List = new ArrayList<Object>();
        for ( int i = 0; i < arrayOFKeys.length(); i++ )  {
            if ( arrayOFKeys.opt(i) instanceof JSONObject) {
                Map<String, Object> subObj2Map = jsonString2Map(arrayOFKeys.opt(i).toString());
                array2List.add(subObj2Map);
            }else if ( arrayOFKeys.opt(i) instanceof JSONArray ) {
                List<Object> subarray2List = jsonArray2List((JSONArray) arrayOFKeys.opt(i));
                array2List.add(subarray2List);
            }else {
               // keyNode( arrayOFKeys.opt(i) );
                array2List.add( arrayOFKeys.opt(i) );
            }
        }
        return array2List;
    }

    public static Map<String, Object> jsonString2Map( String jsonString ) throws JSONException{
        Map<String, Object> keys = new HashMap<String, Object>();

        org.json.JSONObject jsonObject = new org.json.JSONObject( jsonString ); // HashMap
        Iterator<?> keyset = jsonObject.keys(); // HM

        while (keyset.hasNext()) {
            String key =  (String) keyset.next();
            Object value = jsonObject.get(key);
            System.out.print("\n Key : "+key);
            if ( value instanceof org.json.JSONObject ) {
                System.out.println("Incomin value is of JSONObject : ");
                keys.put( key, jsonString2Map( value.toString() ));
            }else if ( value instanceof org.json.JSONArray) {
                org.json.JSONArray jsonArray = jsonObject.getJSONArray(key);
                //JSONArray jsonArray = new JSONArray(value.toString());
                keys.put( key, jsonArray2List( jsonArray ));
            } else {
                //keyNode(value);
                keys.put( key, value );
            }
        }
        return keys;
    }

    public static void displayJSONMAP( Map<String, Object> allKeys ) throws Exception{
        Set<String> keyset = allKeys.keySet(); // HM$keyset
        if (! keyset.isEmpty()) {
            Iterator<String> keys = keyset.iterator(); // HM$keysIterator
            while (keys.hasNext()) {
                String key = keys.next();
                Object value = allKeys.get( key );
                if ( value instanceof Map ) {
                    System.out.println("\n Object Key : "+key);
                    displayJSONMAP(jsonString2Map(value.toString()));
                }else if ( value instanceof List ) {
                    System.out.println("\n Array Key : "+key);
                    JSONArray jsonArray = new JSONArray(value.toString());
                    jsonArray2List(jsonArray);
                }else {
                    System.out.println("key : "+key+" value : "+value);
                }
            }
        }

    }
}
