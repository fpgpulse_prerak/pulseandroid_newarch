package com.ingauge.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by pathanaa on 21-07-2017.
 */

public class DateTimeUtils{

    public static String DATE_FORMATE_OBSEVATION_COUNTER_COACHING = "dd-MMM-yyyy hh:mm:ss aaa";

    //Get date for observation and counter coaching converting server (UTC time) to device local time
    public static String DateObservationCounterCoachingUTC(long time){
        try{
            DateFormat writeFormat = new SimpleDateFormat(DATE_FORMATE_OBSEVATION_COUNTER_COACHING);
            writeFormat.setTimeZone(TimeZone.getDefault());
            Date netDate = (new Date(time));
            String dateStr = writeFormat.format(netDate);
            DateFormat readFormat = new SimpleDateFormat(DATE_FORMATE_OBSEVATION_COUNTER_COACHING);
            readFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            try{
                date = readFormat.parse(dateStr);
            } catch(ParseException e){
                e.printStackTrace();
            }

            String formattedDate = "";
            if(date != null){
                formattedDate = writeFormat.format(date);
            }
            //return convertedTime;
            return formattedDate;
        } catch(Exception ex){
            return "xx";
        }
    }


    //Get date for observation and counter coaching without any type of conversion
    public static String DateObservationCounterCoachingNOUTC(long time){
        try{
            DateFormat writeFormat = new SimpleDateFormat(DATE_FORMATE_OBSEVATION_COUNTER_COACHING);
            writeFormat.setTimeZone(TimeZone.getDefault());
            Date netDate = (new Date(time));
            String dateStr = writeFormat.format(netDate);
            return dateStr;
        } catch(Exception ex){
            return "xx";
        }
    }

    public static String getFirstDay(Date d) throws Exception{
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date dddd = calendar.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return sdf1.format(dddd);
    }

    public static Long getFirstDayofCurrentMonthinMiliSeconds(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTimeInMillis();
    }

    public static String getFirstDayForDisplay(Date d) throws Exception{
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date dddd = calendar.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
        return sdf1.format(dddd);
    }

    public static String getLastDay(Date d) throws Exception{
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date dddd = calendar.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return sdf1.format(dddd);
    }

    public static String getLastDayForDisplay(Date d) throws Exception{
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date dddd = calendar.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
        return sdf1.format(dddd);
    }

    public static String getDate(Date d){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
        return sdf.format(d);
    }

    public static int getLastDayOfMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
        int daysBackToSat = calendar.get(Calendar.DAY_OF_WEEK);

        calendar.add(Calendar.DATE, daysBackToSat * -1);
        System.out.println(sdf.format(calendar.getTime()));
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static String getTodayDateString(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return dateFormat.format(cal.getTime());
    }


    public static String getYesterdayDateString(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());
    }


    public static String getYesterdayDateLastYearString(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        cal.add(Calendar.YEAR, -1);
        return dateFormat.format(cal.getTime());
    }

    public static String getDayBeforeYesterdayDateString(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -2);
        return dateFormat.format(cal.getTime());
    }

    public static String getSameDayofLastWeek(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, -7);
        return dateFormat.format(cal.getTime());
    }

    public static String getDayofLastThirtyDay(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, -30);
        return dateFormat.format(cal.getTime());
    }

    public static String getLastThirtyDayFromToday(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, -30);
        return dateFormat.format(cal.getTime());
    }

    public static String getLastThirtyDayFromTodayLastYear(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, -30);
        cal.add(Calendar.YEAR, -1);
        return dateFormat.format(cal.getTime());
    }

    public static String getSameDayofLastYear(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, -1);
        return dateFormat.format(cal.getTime());
    }

    public static String getCurrentMonth(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(new Date());
        return dateFormat.format(cal.getTime());
    }

    public static String getLastMonth(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        return dateFormat.format(cal.getTime());
    }

    public static String getLastYear(Date mBaseDate){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(mBaseDate);
        cal.add(Calendar.YEAR, -1);
        return dateFormat.format(cal.getTime());
    }

    public static String getLastMonthFromDate(Date baseDate){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(baseDate);
        cal.add(Calendar.MONTH, -1);
        return dateFormat.format(cal.getTime());
    }

    public String getDisplayString(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("EEEE,dd MMM");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(ParseException e){
            e.printStackTrace();
        }
        return "";
    }
}
