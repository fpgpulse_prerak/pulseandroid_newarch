package com.ingauge.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.ColorRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.api.APIClient;
import com.ingauge.pojo.ObsQuestionModelLocal;
import com.ingauge.session.InGaugeSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class UiUtils {
    public static String baseUrl = APIClient.UATbaseUrl;
    public static String baseUrlChart = APIClient.UATbaseUrlChart;
    public static SortedMap<Currency, Locale> currencyLocaleMap;
    public static float YES=5;
    public static float NO=1;
    public static float NA=0;
    //  float YES = 5, NO = 1, NA = 0;

    //public static int industryId= InGaugeSession.read(Resources.getSystem().getString(R.string.key_selected_industry_id),0);
    //  public static int tenantId= InGaugeSession.read(Resources.getSystem().getString(R.string.key_selected_tenant_id),0);
    // A method to find height of the status bar
    public static int getUserId(Context mContext) {
        return InGaugeSession.read(mContext.getString(R.string.key_user_id), 0);
    }

    public static int getIndustryId(Context mContext) {
        return InGaugeSession.read(mContext.getString(R.string.key_selected_industry_id), 1);
    }

    public static int getTenantId(Context mContext) {
        return InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), 1);
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static String getContrySymbol(String regionCurrency) {
        Currency currency = Currency.getInstance("USD");
        return currency.getSymbol();

    }


    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static int getColorFromRes(@ColorRes int resId) {
        return ContextCompat.getColor(InGaugeApp.getInstance(), resId);
    }


    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     *
     * @param arrayObsQuestion
     * @return
     */
    //Calculating Observation/Counter Coaching Question Score
    public static String CountScore(ArrayList<ObsQuestionModelLocal> arrayObsQuestion) {
            /*1. Answer score (AS) => YES = 5, NO = 1, NA = 0
             2. Question weight (QW) => From server response/ 0 for NA answer
             3. Question score
                           ( QS )  = AS * QW
            
             4. Question Weight Total     ( QWT ) = Sum ( QW )
             5. Answer Score Total        ( AST ) = Sum ( AS )
             6. Question Score Total      ( QST ) = Sum ( QS )
            
             7. Total possible score      ( TPS ) = QWT * valueOf('YES') (i.e. 5 for now)
             8. Segment Percentage        ( SP  ) = ( QST / TPS ) * 100
            
             **** For all Segments
             9. Degree of importance (DOI) => Get it from response
            
             10. Total percentage ( TP )  => totalPercentage += SP * DOI
             11.Total DOI                => totalDOI += DOI
            
             12. Final Score = totalPercentage / totalDOI*/



        ArrayList<Float> AS = getAS(arrayObsQuestion);
        ArrayList<Float> QW = getQW(arrayObsQuestion);
        ArrayList<Float> QS = getQS(AS, QW);

        //Next three variables are generalized one
        float QWT = getQWT(QW);
        float AST =  getAST(AS);
        float QST = getQST(QS);

        float TPS = getTPS(QWT, YES);

        float SP = 0;

        if (QST != 0 && TPS != 0) {
            SP = getSP(QST, TPS, 100);
        }

        //**** For all Segments
        ArrayList<Float> DOI = getDOI(arrayObsQuestion);
        float TP = getTP(SP, DOI);
        float TDOI = getTDOI(DOI);
        float FS = getFS(TP, TDOI);
        return String.valueOf(FS);
    }

    public static float getFS(float tp, float tdoi) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return Float.valueOf(decimalFormat.format(tp / tdoi));
    }

    public static float getTDOI(ArrayList<Float> doi) {
        float sum = 0.0f;
        for (int i = 0; i < doi.size(); i++) {
            sum = sum + doi.get(i);
        }
        return sum;
    }

    public static float getTP(float sp, ArrayList<Float> doi) {
        float sum = 0.0f;
        for (int i = 0; i < doi.size(); i++) {
            sum = sum + (sp * doi.get(i));
        }
        return sum;
    }

    public static ArrayList<Float> getDOI(ArrayList<ObsQuestionModelLocal> arrayObsQuestion) { //This thing can be taken from main model class
        ArrayList<Float> DOI = new ArrayList<>();
        for (int i = 0; i < arrayObsQuestion.size(); i++) {

            DOI.add((float) arrayObsQuestion.get(i).getQ_degree());

        }
        return DOI;
    }

    public static float getSP(float qst, float tps, float i) {
        return (qst / tps) * i;
    }

    public static float getTPS(float qwt, float i) {
        return qwt * i;
    }

    public static float getQST(ArrayList<Float> qs) {
        float sum = 0.0f;
        for (int i = 0; i < qs.size(); i++) {
            sum = sum + qs.get(i);
        }
        return sum;
    }


    public static float getQWT(ArrayList<Float> qw) {
        float sum = 0.0f;
        for (int i = 0; i < qw.size(); i++) {
            sum = sum + qw.get(i);
        }
        return sum;
    }

    public static float getAST(ArrayList<Float> as) {
        float sum = 0;
        for (int i = 0; i < as.size(); i++) {
            sum = sum + as.get(i);
        }
        return sum;
    }


    public static ArrayList<Float> getQS(ArrayList<Float> as, ArrayList<Float> qw) {
        //Can Update this same time when qusetion changed into main model class
        ArrayList<Float> QS = new ArrayList<>();
        for (int i = 0; i < as.size(); i++) {
            QS.add(as.get(i) * qw.get(i));
        }
        return QS;
    }


    public static ArrayList<Float> getAS(ArrayList<ObsQuestionModelLocal> arrayObsQuestion) {
        //YES = 5, NO = 1, NA = 0
        ArrayList<Float> AS = new ArrayList<>();
        for (int i = 0; i < arrayObsQuestion.size(); i++) {
            if (arrayObsQuestion.get(i).getQ_yesnona_applicable().equalsIgnoreCase("N/A")) {
                AS.add((float) 0.0f);
            } else if (arrayObsQuestion.get(i).getQ_yesnona_applicable().equalsIgnoreCase("NO")) {
                AS.add((float) 1.0f);
            } else {
                AS.add((float) 5.0f);
            }

        }
        return AS;
    } //Can add this all variable inside main model class

    public static ArrayList<Float> getQW(ArrayList<ObsQuestionModelLocal> arrayObsQuestion) { //Already there in main model class, this thing need to change whenever any question changed
        ArrayList<Float> QW = new ArrayList<>();
        for (int i = 0; i < arrayObsQuestion.size(); i++) {
            /*if (arrayObsQuestion.get(i).getQ_yesnona_applicable().equalsIgnoreCase("N/A")) {
                QW.add((float) 0.0f);
            }else if (arrayObsQuestion.get(i).getQ_yesnona_applicable().equalsIgnoreCase("NO")) {
                QW.add((float) 1.0f);
            }*/
            if (arrayObsQuestion.get(i).getQ_yesnona_applicable().equalsIgnoreCase("N/A")) {
                QW.add((float) 0.0f);
            }
            else {
                QW.add((float) arrayObsQuestion.get(i).getQ_weight());
            }
        }

        return QW;
    }
    //End of Calculating Observation/Counter Coaching Question Score

    public static boolean verifyStoragePermissions(Activity activity, int REQUEST_EXTERNAL_STORAGE, String[] permission_storage) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    permission_storage,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        return true;
    }

    public static String loadJSONFromAsset(String filename, Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("APIResponse/" + filename.toString());
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /*  public static double round(double value, int places) {
          if (places < 0) throw new IllegalArgumentException();

          long factor = (long) Math.pow(10, places);
          value = value * factor;
          long tmp = Math.round(value);
          return (double) tmp / factor;
      }*/
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static String getFormattedDate(String dateToConvert, String dateFormatetToConvert) {
        SimpleDateFormat serverFormat = new SimpleDateFormat(dateFormatetToConvert);
        SimpleDateFormat displayFormat = new SimpleDateFormat("MMM dd ,yyyy");
        try {
            Date date = serverFormat.parse(dateToConvert);
            return displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //For Android N , Using Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY) ,Html.fromHtml method depricated in Android N
    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    //Setting WebView cache based on different different API version
    @SuppressWarnings("deprecation")
    public static void setWebCache(WebView webGraph) {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            webGraph.getSettings().setAppCacheMaxSize(Long.MAX_VALUE);
        }
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webGraph.getSettings().setEnableSmoothTransition(true);
        }
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            webGraph.getSettings().setMediaPlaybackRequiresUserGesture(true);
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webGraph.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        } else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webGraph.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) InGaugeApp.getInstance().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void clearObsCCLocal(Context mContext, boolean isLocationTobeClear) {
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_cc_pl));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_cc_agent));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_cc));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_cc_is_location_changed));

        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_selected_pl_id));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_selected_pl_name));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_selected_agent_id));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_selected_agent_name));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_selected_observation_id));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_selected_observation_name));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_getstarted_millis));
        InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_getend_millis));

        if (isLocationTobeClear) {
            InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_cc_loc));
            InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_selected_location_id));
            InGaugeSession.remove(mContext.getResources().getString(R.string.key_obs_selected_location_name));
        }
    }
    public static Calendar DateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (1000 * 3600 * 24);

        Logger.Error("<<<< Day Difference >>>>" + diffDays);
        String time = null;

        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + "d";
            } else {
                time = diffDays + "d";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + "h";
                } else {
                    time = diffHours + "h";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        //time = diffMinutes + "min ago";
                        time = 0 + "h";
                    } else {
                        //time = diffMinutes + "mins ago";
                        time = 0 + "h";
                    }
                } else {
                    if (diffSeconds > 0) {
                        //time = diffSeconds + "secs ago";
                        time = 0 + "h";
                    }
                }

            }

        }
        return time;
    }
    public static String calculateFeedTime(long feedtime) {
        Date date2 = new Date();
        Date date1 = new Date(feedtime);


        Logger.Error(" Date 2 >>" + getDateStingForTest(date2.getTime(),"MM/dd/yy hh:mm:ss"));
        Logger.Error(" Date 1 >>>>" + getDateStingForTest(feedtime,"MM/dd/yy hh:mm:ss"));
        long timeDiff = Math.abs(date2.getTime() - date1.getTime());
        long dayDifference = Math.round(timeDiff / (1000 * 3600 * 24));
        String dateStr = "";
        if (dayDifference == 0) {
            dateStr = String.valueOf(Math.round(timeDiff / (1000 * 3600))) + 'h';
        } else {
            dateStr = String.valueOf(dayDifference) + 'd';
        }
        Logger.Error("Date One Two " + date2.getTime() + "<<< Date one >>>>" + date1.getTime());
        Logger.Error("<<< Day Str  " + dateStr + " Day Difference   " + dayDifference);
        return dateStr;
    }


    public static String getDateStingForTest(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
    public static JSONObject getMobileKeys(Context mContext) {
        String mobileKeys = new DatabaseHandler(mContext).getMobileKeys();
        try {
            return new JSONObject(mobileKeys);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    static {
        currencyLocaleMap = new TreeMap<Currency, Locale>(new Comparator<Currency>() {
            public int compare(Currency c1, Currency c2) {
                return c1.getCurrencyCode().compareTo(c2.getCurrencyCode());
            }
        });
        for (Locale locale : Locale.getAvailableLocales()) {
            try {
                Currency currency = Currency.getInstance(locale);
                currencyLocaleMap.put(currency, locale);
            } catch (Exception e) {
            }
        }
    }


    /* public static String getCurrencySymbol(String currencyCode) {
         Currency currency = Currency.getInstance(currencyCode);

         Logger.Error("<<<  Country Code " + ":-" + currency.getSymbol(currencyLocaleMap.get(currency)));
         Logger.Error("<<<  Country Code " + ":-" + currency.getSymbol(Locale.US));
         return currency.getSymbol(Locale.US);
     }*/
    public static String getCurrencySymbol(String currencyName) {
        String currencySign = currencyName;
        // US Dollar
        if (currencyName != null) {
            if (currencyName.equals("USD"))
                currencySign = "$";
            // EURO
            if (currencyName.equals("EUR"))
                currencySign = "€";
            // Pound
            if (currencyName.equals("GBP"))
                currencySign = "£";
            // INDIAN RUPEE
            if (currencyName.equals("INR"))
                currencySign = "₹";
            // Australian dollar
            if (currencyName.equals("AUD"))
                currencySign = "$";
            // Canadian dollar
            if (currencyName.equals("CAD"))
                currencySign = "CA$";
            // United Arab Emirates dirham
            if (currencyName.equals("AED"))
                currencySign = "AED";
            // Malaysian ringgit
            if (currencyName.equals("MYR"))
                currencySign = "MYR";
            // Swiss currency
            if (currencyName.equals("CHF"))
                currencySign = "₣";
            // Chinese Yuan / Japanese Yen
            if (currencyName.equals("CNY"))
                currencySign = "¥";
            //Maxico Currency
            if (currencyName.equals("MXN"))
                currencySign = "MXN ";

            //Maxico Currency
            if (currencyName.equals("ANG"))
                currencySign = "ANG ";
            //Thai Baht
            if (currencyName.equals("THB"))
                currencySign = "฿";
            // Brazilian currency
            if (currencyName.equals("BRL"))
                currencySign = "BRL";
        }

        return currencySign;
    }

    public static String TwoDigitDecimal(String value) {
        Logger.Error("" + value);
        if (value.trim().length() > 0) {
            NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
            return nf.format(Double.parseDouble(String.format(Locale.US, "%.2f", Double.parseDouble(value))));
        } else {
            return "0.00";
        }
    }

    public static String AnswerYes() {
        return "Yes";
    }

    public static String AnswerNo() {
        return "No";
    }

    public static String AnswerNA() {
        return "N/A";
    }

    public static String SurroundWithBraces(String input) {
        String output = "";
        if (input.trim().length() > 0) {
            output = "(" + input + ")";
        }
        return output;
    }

    public static String ConvertToDeviceLocal(long serverDate, String dateformate) {
        String dateStr = getDate(serverDate, dateformate);

        return dateStr;
    }

    public static Date ConvertToDeviceLocalDateObj(long serverDate, String dateformate) {
        Date mDateObj= getDateObj(serverDate, dateformate);

        return mDateObj;
    }
    public static String getDate(long time, String dateformate) {
        try {
            DateFormat writeFormat = new SimpleDateFormat(dateformate);
            writeFormat.setTimeZone(TimeZone.getDefault());
            Date netDate = (new Date(time));
            //return sdf.format(netDate);
            String dateStr = writeFormat.format(netDate);
            DateFormat readFormat = new SimpleDateFormat(dateformate);
            readFormat.setTimeZone(TimeZone.getDefault());
            Date date = null;
            try {
                date = readFormat.parse(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String formattedDate = "";
            if (date != null) {
                formattedDate = writeFormat.format(date);
            }
            //return convertedTime;
            return formattedDate;
        } catch (Exception ex) {
            return "xx";
        }
    }
    public static Date getDateObj(long time, String dateformate) {
        try {
            DateFormat writeFormat = new SimpleDateFormat(dateformate);
            writeFormat.setTimeZone(TimeZone.getDefault());
            Date netDate = (new Date(time));
            //return sdf.format(netDate);
            String dateStr = writeFormat.format(netDate);
            DateFormat readFormat = new SimpleDateFormat(dateformate);
            readFormat.setTimeZone(TimeZone.getDefault());
            Date date = null;
            try {
                date = readFormat.parse(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //return convertedTime;
            return date;
        } catch (Exception ex) {
            return null;
        }
    }


    public static Bitmap screenShot(View view) {

        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),view.getHeight(), Bitmap.Config.ARGB_8888);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream(bitmap.getWidth() * bitmap.getHeight());
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, buffer);
        Canvas canvas = new Canvas(bitmap);
        Paint mPaint = new Paint(Paint.FILTER_BITMAP_FLAG);
        canvas.drawBitmap(bitmap, 0, 0, mPaint);

        view.draw(canvas);
        return bitmap;
    }

    public static byte[] convertBitmapToByteArray(Context context, Bitmap bitmap) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream(bitmap.getWidth() * bitmap.getHeight());
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, buffer);
        return buffer.toByteArray();
    }

    public static String getRealPathFromURI(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
}
