package com.ingauge.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pathanaa on 16-06-2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "InGuageDB";
    private static final String TABLE_MOBILE_KEY = "tbl_mobilekeys";
    private static final String KEY_JSON_OBJECT = "key_json_object";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_MOBILE_KEY + "(" + KEY_JSON_OBJECT + " TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MOBILE_KEY);
        onCreate(sqLiteDatabase);
    }

    public void addKeys(String jsonMobileKey) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_JSON_OBJECT, jsonMobileKey);
        if (!Exists()) {
            Logger.Error("EXISTS?" + false);
            db.insert(TABLE_MOBILE_KEY, null, values);
        } else {
            Logger.Error("EXISTS?" + true);
            db.update(TABLE_MOBILE_KEY, values, null, null);
        }
        db.close();
    }

    public boolean Exists() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM " + TABLE_MOBILE_KEY;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0) {
            return true;
        } else {
            return false;
        }
    }

    public String getMobileKeys() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_MOBILE_KEY, new String[]{KEY_JSON_OBJECT,}, null, null, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        String mobilekeys = cursor.getString(0);
        // return contact
        return mobilekeys;
    }
}
