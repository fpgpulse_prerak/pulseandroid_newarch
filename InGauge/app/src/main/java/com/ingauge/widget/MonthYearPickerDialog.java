package com.ingauge.widget;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.fragments.GoalSettings;
import com.ingauge.session.InGaugeSession;

import java.util.Calendar;

/**
 * Created by mansurum on 20-Jun-17.
 */
public class MonthYearPickerDialog extends DialogFragment {

    private static final int MAX_YEAR = 2099;
    int selectedDay = -1;
    int selectedMonth = -1;
    int selectedYear = -1;

    private DatePickerDialog.OnDateSetListener listener;

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (getArguments() != null) {
            selectedDay = getArguments().getInt(GoalSettings.KEY_SELECTED_DAY);
            selectedMonth = getArguments().getInt(GoalSettings.KEY_SELECTED_MONTH)-1;
            selectedYear = getArguments().getInt(GoalSettings.KEY_SELECTED_YEAR);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        Calendar cal = Calendar.getInstance();
        if(selectedYear>0){
            cal.set(selectedYear,selectedMonth,1);
        }

        View dialog = inflater.inflate(R.layout.layout_month_year, null);
        final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
        final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);

        monthPicker.setMinValue(1);
        monthPicker.setMaxValue(12);
        monthPicker.setValue(cal.get(Calendar.MONTH) + 1);

        int year = cal.get(Calendar.YEAR);
        yearPicker.setMinValue(1000);
        yearPicker.setMaxValue(MAX_YEAR);
        yearPicker.setValue(year);
        builder.setView(dialog)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onDateSet(null, yearPicker.getValue(), monthPicker.getValue(), 0);

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MonthYearPickerDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}