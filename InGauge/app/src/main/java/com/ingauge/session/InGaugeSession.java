package com.ingauge.session;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * Created by mansurum on 10-May-17.
 */

public class InGaugeSession {
    private static SharedPreferences mSharedPreferences;
    public static void init(Context context)
    {
        if(mSharedPreferences == null)
            mSharedPreferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
    }


    /**
     *
     * @param key
     * @param defValue
     * @return Read String Value
     */
    public static String read(String key, String defValue) {
        return mSharedPreferences.getString(key, defValue);
    }

    /**
     *
     * @param key
     * @param value
     * Write String Value
     */
    public static void write(String key, String value) {
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }
    public static void remove(String key)
    {
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        prefsEditor.remove(key);
        prefsEditor.commit();
    }

    /**
     *
     * @param key
     * @param defValue
     * @return
     * Read Boolean Value
     */
    public static boolean read(String key, boolean defValue) {
        return mSharedPreferences.getBoolean(key, defValue);
    }

    /**
     *
     * @param key
     * @param value
     * Write Boolean Value
     */
    public static void write(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    /**
     *
     * @param key
     * @param defValue
     * @return Read Integet Value
     */
    public static Integer read(String key, int defValue) {
        return mSharedPreferences.getInt(key, defValue);
    }

    /**
     * Write Integer Value
     * @param key
     * @param value
     */
    public static void write(String key, Integer value) {
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        prefsEditor.putInt(key, value).commit();
    }
    public static void clearAllPreferences(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
