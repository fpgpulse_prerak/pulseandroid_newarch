package com.ingauge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.listener.LeaderboardClickListener;
import com.ingauge.pojo.CustomDashboardModel;

import java.util.List;

/**
 * Created by mansurum on 19-May-17.
 */

public class EntityListAdapter extends RecyclerView.Adapter<EntityListAdapter.FeedsAdapterViewHolder>{

    private Context mContext;

    private int lastPosition = -1;
    private List<CustomDashboardModel.CustomReport> mCustomReportList;

    LeaderboardClickListener mLeaderboardClickListener;

    public EntityListAdapter(Context mContext, List<CustomDashboardModel.CustomReport> mCustomReportList, LeaderboardClickListener mLeaderboardClickListener){

        this.mContext = mContext;
        this.mCustomReportList = mCustomReportList;
        this.mLeaderboardClickListener = mLeaderboardClickListener;

    }

    @Override
    public EntityListAdapter.FeedsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_entity_list, parent, false);

        return new EntityListAdapter.FeedsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EntityListAdapter.FeedsAdapterViewHolder holder, int position){

        CustomDashboardModel.CustomReport mCustomReport = mCustomReportList.get(position);

        if(!mCustomReport.getIsError()){
            holder.tvReportNamr.setText(mCustomReport.getReportName());
        }

    }

    @Override
    public int getItemCount(){
        return mCustomReportList.size();
    }

    public class FeedsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tvReportNamr;


        public FeedsAdapterViewHolder(View view){
            super(view);
            tvReportNamr = (TextView) view.findViewById(R.id.row_entity_tv_report_name);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            mLeaderboardClickListener.getClickData(getLayoutPosition(), mCustomReportList.get(getLayoutPosition()).getReportName(), mCustomReportList.get(getLayoutPosition()).getId());
        }
    }


    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position){
        // If the bound view wasn't previously displayed on screen, it's animated
        if(position > lastPosition){
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
