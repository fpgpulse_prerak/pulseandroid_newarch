package com.ingauge.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.ObservationTabActivity;
import com.ingauge.pojo.ObsQuestionModelLocal;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;

/**
 * Created by pathanaa on 13-07-2017.
 */

public class ObservationQuestionAdapter extends PagerAdapter {
    private Context mContext;
    private ViewPager mViewPagerQuestion;

    private HomeActivity mHomeActivity;
    int questionnumber=1;
    int basesection=1;

    public ObservationQuestionAdapter(Context context, ArrayList<ObsQuestionModelLocal> arrayObsQuestion, ViewPager mViewPagerQuestion, HomeActivity mHomeActivity) {
        this.mContext = context;
        this.mViewPagerQuestion = mViewPagerQuestion;
        this.mHomeActivity = mHomeActivity;

    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {
        TextView tvQuestionTitle;
        TextView tvQuestionDescription;
        final TextView tvSkip;
        final Button mBtnYes;
        final Button mBtnNA;
        final Button mBtnView;
        RelativeLayout mRelEditCreate;
        RelativeLayout mRelViewOnly;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.row_observation_question, collection, false);
        tvQuestionTitle = (TextView) layout.findViewById(R.id.tv_question_title);
        tvQuestionDescription = (TextView) layout.findViewById(R.id.tv_question_description);
        tvQuestionTitle.setText("Section" + " " + mHomeActivity.arrayObsQuestion.get(position).getSection() + ": " + mHomeActivity.arrayObsQuestion.get(position).getQ_title());
        tvQuestionDescription.setText((mHomeActivity.arrayObsQuestion.get(position).getQ_number()) + ". " + mHomeActivity.arrayObsQuestion.get(position).getQ_text());
        tvSkip = (TextView) layout.findViewById(R.id.tv_skip);
        tvSkip.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_cc_ans_skip)));

        mBtnYes = (Button) layout.findViewById(R.id.btn_yes);
        mBtnNA = (Button) layout.findViewById(R.id.btn_na);
        mBtnView = (Button) layout.findViewById(R.id.btn_view);

        mBtnYes.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_cc_ans_yes)));
        mBtnNA.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_cc_ans_na)));

        mRelEditCreate = (RelativeLayout) layout.findViewById(R.id.rel_botton_edit_create);
        mRelViewOnly = (RelativeLayout) layout.findViewById(R.id.rel_botton_view);

        if (mHomeActivity.isOnlyView) {
            mRelEditCreate.setVisibility(View.GONE);
            mRelViewOnly.setVisibility(View.VISIBLE);
            if (mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerYes())) {
                mBtnView.setText(UiUtils.AnswerYes());
            } else if (mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerNA())) {
                mBtnView.setText(UiUtils.AnswerNA());
            } else {
                mBtnView.setText("-");
            }

            setSelected(mBtnView);
            tvSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    setNotSelected(mBtnNA);
                    setNotSelected(mBtnYes);
                    mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());

                    NavigateNext();
                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score)+ " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
                    bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
                    bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
                    bundle.putString("survey_entity", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), ""));
                    bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), ""));
                    bundle.putString("question", mHomeActivity.arrayObsQuestion.get(position).getQ_text());
                    bundle.putString("answer", tvSkip.getText().toString());
                    InGaugeApp.getFirebaseAnalytics().logEvent("select_answer", bundle);
                }
            });
        } else {
            mRelEditCreate.setVisibility(View.VISIBLE);
            mRelViewOnly.setVisibility(View.GONE);

            if (mHomeActivity.arrayObsQuestion.get(position).getQ_yes_nona().equalsIgnoreCase("YESNONA")) {
                mBtnYes.setVisibility(View.VISIBLE);
                mBtnNA.setVisibility(View.VISIBLE);
            } else {
                mBtnYes.setVisibility(View.VISIBLE);
                mBtnNA.setVisibility(View.GONE);
            }

            if (mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerYes())) {
                setSelected(mBtnYes);
                setNotSelected(mBtnNA);

            } else if (mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerNA())) {
                setSelected(mBtnNA);
                setNotSelected(mBtnYes);
            }
            mBtnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerYes())) {
                        // setNotSelected(mBtnYes);
                        //mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());
                    } else {
                        setSelected(mBtnYes);
                        setNotSelected(mBtnNA);
                        mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerYes());
                        NavigateNext();
                    }
                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score)+ " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);

                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
                    bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
                    bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
                    bundle.putString("survey_entity", "Observation");
                    bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), ""));
                    bundle.putString("question", mHomeActivity.arrayObsQuestion.get(position).getQ_text());
                    bundle.putString("answer", mBtnYes.getText().toString());
                    InGaugeApp.getFirebaseAnalytics().logEvent("select_answer", bundle);
                }


            });

            mBtnNA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerNA())) {
                        //    setNotSelected(mBtnNA);
                        //  mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());
                    } else {
                        setSelected(mBtnNA);
                        setNotSelected(mBtnYes);
                        mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNA());
                        NavigateNext();
                    }
                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score)+ " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);

                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
                    bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
                    bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
                    bundle.putString("survey_entity", "Observation");
                    bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), ""));
                    bundle.putString("question", mHomeActivity.arrayObsQuestion.get(position).getQ_text());
                    bundle.putString("answer", mBtnNA.getText().toString());
                    InGaugeApp.getFirebaseAnalytics().logEvent("select_answer", bundle);
                }
            });
            tvSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerNA())||
                            mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerYes())) {
                        setNotSelected(mBtnNA);
                        setNotSelected(mBtnYes);
                        mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());
                    }
                    NavigateNext();
                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score)+ " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
                    bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
                    bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
                    bundle.putString("survey_entity", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), ""));
                    bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), ""));
                    bundle.putString("question", mHomeActivity.arrayObsQuestion.get(position).getQ_text());
                    bundle.putString("answer", tvSkip.getText().toString());
                    InGaugeApp.getFirebaseAnalytics().logEvent("select_answer", bundle);
                }
            });
        }


        collection.addView(layout);
        return layout;
    }

    public int getQuestionNumber(int section){
        if(section==basesection){
            return 1;
        }else{
            if(section!=basesection){
                return 1;
            }else{
                return questionnumber = questionnumber+1;
            }

        }
    }
    private void NavigateNext() {
        if (mViewPagerQuestion.getCurrentItem() + 1 == mHomeActivity.arrayObsQuestion.size()) {

           // mHomeActivity.selectFragment(mHomeActivity.nBottomNavigationView.getMenu().getItem(1));

        } else {
            mViewPagerQuestion.setCurrentItem(mViewPagerQuestion.getCurrentItem() + 1);
        }
    }

    private void setSelected(Button mBtn) {
        mBtn.setBackgroundResource(R.drawable.question_selected);
        mBtn.setTextColor(mContext.getResources().getColor(R.color.white));
    }

    private void setNotSelected(Button mBtn) {
        mBtn.setBackgroundResource(R.drawable.question_not_selected);
        mBtn.setTextColor(mContext.getResources().getColor(R.color.black));
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mHomeActivity.arrayObsQuestion.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Title";
    }

}
