package com.ingauge.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.ObservationAgentFragment;
import com.ingauge.pojo.ObservationDashboardModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 19-May-17.
 */

public class ObservationDashboardListAdapter extends RecyclerView.Adapter<ObservationDashboardListAdapter.FeedsAdapterViewHolder> {
    private Context mContext;
    private int lastPosition = -1;
    private HomeActivity mHomeActivity;
    private JSONObject mobilekeyObject;
    //private ObservationDashboardModel observationDashboardModel;
    private APIInterface apiInterface;
    private Call mCall;
    private boolean isEdit = true;
    private boolean isSuperAdmin = false;
    private boolean isPerformanceLeader = false;
    private boolean isFrontlineAssociate = false;
    private List<ObservationDashboardModel.Data.Datum> mDatumList;


    public ObservationDashboardListAdapter(Context mContext,HomeActivity mHomeActivity, boolean isEdit,List<ObservationDashboardModel.Data.Datum> mDatumList) {
        this.mContext = mContext;
        this.mHomeActivity = mHomeActivity;
        this.isEdit = isEdit;
      //  this.observationDashboardModel = observationDashboardModel;
        mobilekeyObject = UiUtils.getMobileKeys(mContext);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.mDatumList = mDatumList;
        getRoleStatus();
    }

    public void getRoleStatus() {
        String roleName = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_login_tenant_user_role), "");
        if (roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_super_admin))) {
            isSuperAdmin = true;
            isPerformanceLeader = false;
            isFrontlineAssociate = false;
        } else if (roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_performance_manager))) {
            isSuperAdmin = false;
            isPerformanceLeader = true;
            isFrontlineAssociate = false;
        } else if (roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_frontline_associate))) {
            isSuperAdmin = false;
            isPerformanceLeader = false;
            isFrontlineAssociate = true;
        }
    }

    @Override
    public ObservationDashboardListAdapter.FeedsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_dashboard_observation, parent, false);
        return new ObservationDashboardListAdapter.FeedsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ObservationDashboardListAdapter.FeedsAdapterViewHolder holder, final int position) {
        String loginUserId = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2));
        if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Completed")) {
            holder.tvStatus.setText("Completed");
            holder.tvStatus.setBackgroundResource(R.drawable.status_completed);
            /*holder.imgView.setVisibility(View.VISIBLE);
            holder.imgDelete.setVisibility(View.VISIBLE);
            holder.imgEdit.setVisibility(View.VISIBLE);*/
        } else if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")) {
            holder.tvStatus.setText("Pending");
            holder.tvStatus.setBackgroundResource(R.drawable.status_pending);
            /*holder.imgView.setVisibility(View.GONE);
            holder.imgDelete.setVisibility(View.VISIBLE);
            holder.imgEdit.setVisibility(View.VISIBLE);*/
        } else {
            holder.tvStatus.setText("Cancelled");
            holder.tvStatus.setBackgroundResource(R.drawable.status_canceled);
           /* holder.imgView.setVisibility(View.GONE);
            holder.imgDelete.setVisibility(View.GONE);
            holder.imgEdit.setVisibility(View.GONE);*/
        }

        String surveyEntityType = InGaugeSession.read(mContext.getString(R.string.key_obs_cc_survey_entity_type_id), null);



        /*
         * surveyEntityType = 0 =>>> Observation
         * surveyEntityType = 1 =>>> Counter Coaching
         */
        if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Cancelled")) {
            holder.imgView.setVisibility(View.GONE);
            holder.imgDelete.setVisibility(View.GONE);
            holder.imgEdit.setVisibility(View.GONE);
        } else {
            if (isSuperAdmin) {
                if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")) {
                    holder.imgView.setVisibility(View.GONE);
                    holder.imgDelete.setVisibility(View.VISIBLE);
                    holder.imgEdit.setVisibility(View.VISIBLE);
                } else if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Cancelled")) {
                    holder.imgView.setVisibility(View.GONE);
                    holder.imgDelete.setVisibility(View.GONE);
                    holder.imgEdit.setVisibility(View.GONE);
                } else {
                    holder.imgView.setVisibility(View.VISIBLE);
                    holder.imgDelete.setVisibility(View.VISIBLE);
                    holder.imgEdit.setVisibility(View.VISIBLE);
                }
            } else if (isPerformanceLeader || isFrontlineAssociate) {

                if (String.valueOf(mDatumList.get(position).getSurveyorId()).equalsIgnoreCase(loginUserId)) {
                    if (surveyEntityType.equalsIgnoreCase("0")) {
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeleteObservation")) {
                            holder.imgDelete.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgDelete.setVisibility(View.GONE);
                        }
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessViewObservation")) {
                            if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")) {
                                holder.imgView.setVisibility(View.GONE);
                            } else {
                                holder.imgView.setVisibility(View.VISIBLE);
                            }

                        } else {
                            holder.imgView.setVisibility(View.GONE);
                        }
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessEditObservation")) {
                            holder.imgEdit.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgEdit.setVisibility(View.GONE);
                        }
                    } else if (surveyEntityType.equalsIgnoreCase("1")) {
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeleteCounterCoaching")) {
                            holder.imgDelete.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgDelete.setVisibility(View.GONE);
                        }
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessViewCounterCoaching")) {
                            if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")) {
                                holder.imgView.setVisibility(View.GONE);
                            } else {
                                holder.imgView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            holder.imgView.setVisibility(View.GONE);
                        }
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessEditCounterCoaching")) {
                            holder.imgEdit.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgEdit.setVisibility(View.GONE);
                        }
                    }
                } else {
                    holder.imgEdit.setVisibility(View.GONE);
                    holder.imgDelete.setVisibility(View.GONE);
                    if (!mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")) {


                        if (surveyEntityType.equalsIgnoreCase("0")) {
                            if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessViewObservation")) {
                                holder.imgView.setVisibility(View.VISIBLE);
                            } else {
                                holder.imgView.setVisibility(View.GONE);
                            }
                        } else if (surveyEntityType.equalsIgnoreCase("1")) {
                            if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessViewCounterCoaching")) {
                                holder.imgView.setVisibility(View.VISIBLE);
                            } else {
                                holder.imgView.setVisibility(View.GONE);
                            }
                        }


                    } else {
                        holder.imgView.setVisibility(View.GONE);
                    }

                }
            } else {
                if (String.valueOf(mDatumList.get(position).getSurveyorId()).equalsIgnoreCase(loginUserId)) {
                    if (surveyEntityType.equalsIgnoreCase("0")) {
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeleteObservation")) {
                            holder.imgDelete.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgDelete.setVisibility(View.GONE);
                        }
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessViewObservation")) {
                            if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")) {
                                holder.imgView.setVisibility(View.GONE);
                            } else {
                                holder.imgView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            holder.imgView.setVisibility(View.GONE);
                        }
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessEditObservation")) {
                            holder.imgEdit.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgEdit.setVisibility(View.GONE);
                        }
                    } else if (surveyEntityType.equalsIgnoreCase("1")) {
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeleteCounterCoaching")) {
                            holder.imgDelete.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgDelete.setVisibility(View.GONE);
                        }
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessViewCounterCoaching")) {
                            if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")) {
                                holder.imgView.setVisibility(View.GONE);
                            } else {
                                holder.imgView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            holder.imgView.setVisibility(View.GONE);
                        }
                        if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessEditCounterCoaching")) {
                            holder.imgEdit.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgEdit.setVisibility(View.GONE);
                        }
                    }
                } else {
                    holder.imgEdit.setVisibility(View.GONE);
                    holder.imgDelete.setVisibility(View.GONE);
                    if (!mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")) {


                        if (surveyEntityType.equalsIgnoreCase("0")) {
                            if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessViewObservation")) {
                                holder.imgView.setVisibility(View.VISIBLE);
                            } else {
                                holder.imgView.setVisibility(View.GONE);
                            }
                        } else if (surveyEntityType.equalsIgnoreCase("1")) {
                            if (mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessViewCounterCoaching")) {
                                holder.imgView.setVisibility(View.VISIBLE);
                            } else {
                                holder.imgView.setVisibility(View.GONE);
                            }
                        }


                    } else {
                        holder.imgView.setVisibility(View.GONE);
                    }

                }
            }

        }

        //13-Jul-2017 10:28:43 PM

        holder.tvAgentName.setText(mDatumList.get(position).getRespondentName());
        holder.tvPlName.setText(mDatumList.get(position).getSurveyorName());
        holder.tvObservationName.setText(mDatumList.get(position).getQuestionnaireName()
                + "(" + mDatumList.get(position).getQuestionnaireSurveyCategoryName() + ")");

        if (mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Pending")
                || mDatumList.get(position).getSurveyStatus().equalsIgnoreCase("Cancelled")) {
            holder.tvScore.setVisibility(View.GONE);
        } else {
            holder.tvScore.setVisibility(View.VISIBLE);
            if (mDatumList.get(position).getScoreVal() != null)
                holder.tvScore.setText(mDatumList.get(position).getScoreVal() + " %");
        }


        holder.tvLocationName.setText(mDatumList.get(position).getTenantLocationName());
        holder.tvObsCreationTime.setText(DateTimeUtils.DateObservationCounterCoachingNOUTC(mDatumList.get(position).getStartDate()));
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    @Override
    public int getItemCount() {
        return mDatumList.size();
    }

    public class FeedsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvStatus;
        private TextView tvPlName;
        private TextView tvScore;
        private TextView tvAgentName;
        private TextView tvObservationName;
        private TextView tvLocationName;
        private TextView tvObsCreationTime;

        private ImageView imgView;
        private ImageView imgEdit;
        private ImageView imgDelete;

        public FeedsAdapterViewHolder(View view) {
            super(view);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvPlName = (TextView) view.findViewById(R.id.tv_pl_name);
            tvScore = (TextView) view.findViewById(R.id.tv_score);
            tvAgentName = (TextView) view.findViewById(R.id.tv_agent_name);
            tvObservationName = (TextView) view.findViewById(R.id.tv_observation_name);
            tvLocationName = (TextView) view.findViewById(R.id.tv_location_name);
            tvObsCreationTime = (TextView) view.findViewById(R.id.tv_obs_creation_time);

            imgView = (ImageView) view.findViewById(R.id.img_view);
            imgEdit = (ImageView) view.findViewById(R.id.img_edit);
            imgDelete = (ImageView) view.findViewById(R.id.img_delete);

            imgView.setOnClickListener(this);
            imgEdit.setOnClickListener(this);
            imgDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            switch (view.getId()) {

                case R.id.img_view:
                    ObservationAgentFragment observeAgentFragment = new ObservationAgentFragment();
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean("isFromDashboard", true);
                    mBundle.putBoolean("isOnlyView", true);
                    mBundle.putString("observationID", String.valueOf(mDatumList.get(getAdapterPosition()).getId()));
                    observeAgentFragment.setArguments(mBundle);
                    mHomeActivity.replace(observeAgentFragment, mContext.getResources().getString(R.string.tag_tab_observation_dashboard));
                    break;


                case R.id.img_edit:
                    observeAgentFragment = new ObservationAgentFragment();
                    mBundle = new Bundle();
                    mBundle.putBoolean("isFromDashboard", true);
                    mBundle.putBoolean("isOnlyView", false);
                    if (mDatumList.get(getAdapterPosition()).getSurveyStatus().equalsIgnoreCase("Pending")) {
                        mBundle.putBoolean("isPending", true);
                    } else {
                        mBundle.putBoolean("isPending", false);
                    }
                    mBundle.putString("observationID", String.valueOf(mDatumList.get(getAdapterPosition()).getId()));
                    observeAgentFragment.setArguments(mBundle);
                    mHomeActivity.replace(observeAgentFragment, mContext.getResources().getString(R.string.tag_tab_observation_dashboard));
                    break;


                case R.id.img_delete:
                    String AlertTitle = "";
                    if (InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))) {
                        AlertTitle = "Are you sure you want to remove this observation?";
                    } else {
                        AlertTitle = "Are you sure you want to remove this counter coaching?";
                    }
                    new AlertDialog.Builder(mContext)
                            .setTitle("Confirm Deletion")
                            .setMessage(AlertTitle)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    deleteObservation("application/json", InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 0)), "1", String.valueOf(mDatumList.get(getAdapterPosition()).getId()), getAdapterPosition());

                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                    break;


            }

        }
    }

    void deleteObservation(String contentType, String authToken, String industryId, String isMobile, String ObservationId, final int adapterPosition) {
        mCall = apiInterface.deleteObservation(ObservationId);
        mHomeActivity.startProgress(mContext);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mHomeActivity.endProgress();
                if (response.body() != null) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(String.valueOf(response.body()));
                        if (jsonObject.optInt("statusCode") == 200) {
                            mDatumList.remove(adapterPosition);
                            notifyDataSetChanged();
                            Logger.Error("OBSERVATION_DELETE_STATUS=>" + "SUCCESS");
                        } else {
                            Logger.Error("OBSERVATION_DELETE_STATUS=>" + "FAILED");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                mHomeActivity.endProgress();
            }
        });
    }


    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


}
