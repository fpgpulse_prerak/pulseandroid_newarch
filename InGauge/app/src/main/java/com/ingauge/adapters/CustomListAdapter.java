package com.ingauge.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.DashboardModelList;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.List;

/**
 * Created by pathanaa on 31-05-2017.
 */

public class CustomListAdapter extends BaseExpandableListAdapter {
    private Context context; //context
    List<DashboardModelList.Datum> datalist;
    private static RecyclerViewClickListener itemListener;

    //public constructor
    public CustomListAdapter(Context context) {
        this.context = context;
    }

    public CustomListAdapter(HomeActivity homeActivity, List<DashboardModelList.Datum> list) {
        datalist = list;
        this.context = homeActivity;
    }

    @Override
    public int getGroupCount() {
        return datalist.size (); //returns total of items in the list;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return datalist.get ( groupPosition ).getmDatumList ().size ();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return datalist.get ( groupPosition );
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return datalist.get ( groupPosition ).getmDatumList ().get ( childPosition );
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        if ( convertView == null ) {
            convertView = LayoutInflater.from ( context ).inflate ( R.layout.row_item_dashboard, parent, false );
            holder = new ViewHolder ();
            holder.tvTitle = (TextView) convertView.findViewById ( R.id.tv_row_item );
            holder.tvSectionHeader = (TextView) convertView.findViewById ( R.id.tv_section_header );
            holder.ivExpandedIcon = (ImageView) convertView.findViewById ( R.id.row_item_iv_expand_icon );
            holder.relDashList = (RelativeLayout) convertView.findViewById ( R.id.rel_dash_list );
            holder.mLayoutSectionHeader = (LinearLayout) convertView.findViewById ( R.id.layout_section );
            convertView.setTag ( holder );
        } else {
            holder = (ViewHolder) convertView.getTag ();
        }


        Logger.Error("<<< Dashboard Group Position  >>>> " + groupPosition);

        if(datalist!=null && datalist.size ()>0){
            DashboardModelList.Datum mDatum = datalist.get ( groupPosition );

            if ( mDatum != null && (mDatum.getmDatumList () != null && mDatum.getmDatumList ().size () > 0) ) {
                holder.ivExpandedIcon.setVisibility ( View.VISIBLE );
            } else {
                holder.ivExpandedIcon.setVisibility ( View.GONE );
            }
            if ( isExpanded ) {
                holder.ivExpandedIcon.setBackground ( context.getResources ().getDrawable ( R.drawable.ic_down_arrow) );
            } else {
                holder.ivExpandedIcon.setBackground ( context.getResources ().getDrawable ( R.drawable.ic_right_arrow ));
            }
            holder.tvTitle.setText ( mDatum.getName () );
            if ( InGaugeSession.read ( context.getResources ().getString ( R.string.key_selected_dashboard_id ), 0).equals ( mDatum.getId () ) ) {
                //holder.relDashList.setBackgroundResource ( R.color.selected_dashboard_color );
                holder.tvTitle.setTextColor ( ContextCompat.getColor ( context,R.color.pink_text_color_for_filter ) );
            } else {
                holder.tvTitle.setTextColor ( ContextCompat.getColor ( context,R.color.black ) );
            }
       /* if(position==0){
            if(datalist.get(position).getDashboardType()==1){
                holder.tvSectionHeader.setText("Dashboard");
            }else if(datalist.get(position).getDashboardType()==2){
                holder.tvSectionHeader.setText("How am i doing");
            }else{
                holder.tvSectionHeader.setText("dashboard type");
            }

            holder.mLayoutSectionHeader.setVisibility(View.VISIBLE);
        }else{
            holder.mLayoutSectionHeader.setVisibility(View.GONE);
        }*/
            if ( datalist.get ( groupPosition ).isSectionHeaderToShow () ) {
                holder.mLayoutSectionHeader.setVisibility ( View.VISIBLE );
                if ( datalist.get ( groupPosition ).getDashboardType () == 1 ) {
                    holder.tvSectionHeader.setText ( "Dashboard" );
                } else if ( datalist.get ( groupPosition ).getDashboardType () == 2 ) {
                    holder.tvSectionHeader.setText ( "How am i doing" );
                } else {
                    holder.tvSectionHeader.setText ( "How do i improve" );
                }
            } else {
                holder.mLayoutSectionHeader.setVisibility ( View.GONE );
            }
        }

        holder.ivExpandedIcon.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Logger.Error ( "<<<< Click on Image Button >>" );

                if ( isExpanded ) {
                    ((ExpandableListView) parent).collapseGroup ( groupPosition );
                } else {
                    ((ExpandableListView) parent).expandGroup ( groupPosition, true );
                }
            }
        } );
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if ( convertView == null ) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
            convertView = infalInflater.inflate ( R.layout.row_expand_item_dashboard, null );
        }

        TextView tvTitle = (TextView) convertView.findViewById ( R.id.tv_row_item );
        TextView tvSectionHeader = (TextView) convertView.findViewById ( R.id.tv_section_header );
        ImageView imvSelected = (ImageView) convertView.findViewById ( R.id.img_selected );
        RelativeLayout relDashList = (RelativeLayout) convertView.findViewById ( R.id.rel_dash_list );
        if((datalist!=null && datalist.size ()>0) && (datalist.get ( groupPosition ).getmDatumList ()!=null && datalist.get ( groupPosition ).getmDatumList ().size ()>0)){
            DashboardModelList.Datum mDatum = datalist.get ( groupPosition ).getmDatumList ().get ( childPosition );
            tvTitle.setText ( mDatum.getTitle ());
            if ( InGaugeSession.read ( context.getResources ().getString ( R.string.key_selected_dashboard_id ), 0 ).equals ( mDatum.getId () ) ) {
                imvSelected.setVisibility ( View.GONE );
                //relDashList.setBackgroundResource ( R.color.selected_dashboard_color );
                tvTitle.setTextColor ( ContextCompat.getColor ( context,R.color.pink_text_color_for_filter ) );
            } else {
                imvSelected.setVisibility ( View.GONE );
                //relDashList.setBackgroundColor ( Color.WHITE );
                tvTitle.setTextColor ( ContextCompat.getColor ( context,R.color.social_interaction_rounded_circle_dark_gray ) );
            }
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    class ViewHolder {
        TextView tvTitle;
        TextView tvSectionHeader;
        ImageView ivExpandedIcon;
        RelativeLayout relDashList;
        LinearLayout mLayoutSectionHeader;
    }


}