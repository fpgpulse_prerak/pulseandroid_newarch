package com.ingauge.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.fragments.FragmentFilterDynamicList;
import com.ingauge.fragments.FragmentFilterDynamicListForGoal;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.FilterBaseData;

import java.util.ArrayList;
import java.util.List;


public class FilterDetailListAdapterForGoal extends RecyclerView.Adapter<FilterDetailListAdapterForGoal.FilterViewHolder> implements Filterable {
    Activity context;
    List<FilterBaseData> mBaseDataList;
    private List<FilterBaseData> dictionaryWords;
    private List<FilterBaseData> filteredList;
    private CustomFilter mFilter;
    private static RecyclerViewClickListener itemListener;
    private boolean isFromDetails = false;
    private int lastCheckedPosition = 0;
    private String selectedId="";
    private String selectedName="";
//    public ImageView ivRight;


    public FilterDetailListAdapterForGoal(Activity context, List<FilterBaseData> mBaseDataList, RecyclerViewClickListener itemListener, boolean isFromDetails, String selectedId, String selectedName) {
        this.context = context;
        this.mBaseDataList = mBaseDataList;
        this.itemListener = itemListener;
        this.isFromDetails = isFromDetails;
        dictionaryWords = mBaseDataList;
        filteredList = new ArrayList<>();
        filteredList.addAll(dictionaryWords);
        this.selectedId = selectedId;
        this.selectedName = selectedName;
        mFilter = new CustomFilter(FilterDetailListAdapterForGoal.this);
        lastCheckedPosition = getIndexByProperty(selectedId);

    }
    public void setDataset(List<FilterBaseData> mBaseDataList) {
        this.mBaseDataList= mBaseDataList;
        notifyDataSetChanged();
    }

    // Swap itemA with itemB
    public void swapItems(FilterBaseData mFilterBaseData) {
        int index = dictionaryWords.indexOf(mFilterBaseData);
        //make sure to check if dataset is null and if itemA and itemB are valid indexes.
        FilterBaseData itemA = dictionaryWords.get(index);
        dictionaryWords.remove(index);
        /*FilterBaseData itemB = dictionaryWords.get(0);
        dictionaryWords.set(index, itemB);*/
        dictionaryWords.add(0, itemA);


        FragmentFilterDynamicListForGoal.mSearchView.setQuery("", false);
        //Collapse the action view
        FragmentFilterDynamicListForGoal.mSearchView.onActionViewCollapsed();
        mBaseDataList = new ArrayList<>();
        mBaseDataList =dictionaryWords;
        notifyDataSetChanged(); //This will trigger onBindViewHolder method from the adapter.
    }
    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_filter_list, parent, false);
        return new FilterDetailListAdapterForGoal.FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        holder.tvTitle.setText(mBaseDataList.get(position).filterName);
        holder.tvFilterValue.setText(mBaseDataList.get(position).name);
        if (TextUtils.isEmpty(mBaseDataList.get(position).name)) {
            holder.tvFilterValue.setVisibility(View.GONE);
        } else {
            holder.tvFilterValue.setVisibility(View.VISIBLE);
        }
        holder.tvTitle.setVisibility(View.GONE);
        if (position== lastCheckedPosition) {
            mBaseDataList.get(position).isSelectedPosition = true;
        } else {
            mBaseDataList.get(position).isSelectedPosition = false;
        }

        if (mBaseDataList.get(position).isSelectedPosition) {
            holder.ivRight.setVisibility(View.VISIBLE);
        } else {
            holder.ivRight.setVisibility(View.GONE);
        }
        /*if (!isFromDetails) {
            holder.ivRight.setVisibility(View.VISIBLE);
            holder.tvFilterValue.setVisibility(View.VISIBLE);
        } else {
            holder.ivRight.setVisibility(View.GONE);
            holder.tvFilterValue.setVisibility(View.GONE);
            holder.tvTitle.setText(mBaseDataList.get(position).name);
        }*/

    }

    @Override
    public int getItemCount() {
        return mBaseDataList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        private TextView tvFilterValue;
        private ImageView ivRight;


        public FilterViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.row_item_filter_list_tv_name);
            ivRight = (ImageView) view.findViewById(R.id.row_item_filter_iv);
            tvFilterValue = (TextView) view.findViewById(R.id.tv_filter_list_select_filter_name);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mBaseDataList != null) {
                //lastCheckedPosition = getLayoutPosition();
                lastCheckedPosition=0;
                //notifyDataSetChanged();
                swapItems(mBaseDataList.get(getLayoutPosition()));
                itemListener.recyclerViewListClicked(v, mBaseDataList.get(0), getLayoutPosition(), mBaseDataList.get(0).filterIndex, false);


                //updateAdapter();

            }
        }
    }

    public class CustomFilter extends Filter {
        private FilterDetailListAdapterForGoal mAdapter;

        private CustomFilter(FilterDetailListAdapterForGoal mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(dictionaryWords);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final FilterBaseData mWords : dictionaryWords) {
                    if (mWords.name.toLowerCase().contains(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println("Count Number 2 " + ((List<FilterBaseData>) results.values).size());
            mBaseDataList = (ArrayList<FilterBaseData>) results.values;
            this.mAdapter.notifyDataSetChanged();
        }
    }

    private int getIndexByProperty(String selected) {
        for (int i = 0; i < mBaseDataList.size(); i++) {
            if (mBaseDataList.get(i) !=null && mBaseDataList.get(i).id.equals(selected)) {
                return i;
            }
        }

        return 0;// not there is list
    }


}