package com.ingauge.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ingauge.R;
import com.ingauge.pojo.ReportDetailFeedsPojo;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.util.List;

/**
 * Created by mansurum on 27-Apr-18.
 */

public class ReportDetailsFeedsAdapter extends BaseAdapter{

    private Context mContext;
    private List<ReportDetailFeedsPojo.Datum> mDatumList;
    protected LayoutInflater inflater;


    public ReportDetailsFeedsAdapter(Context context, List<ReportDetailFeedsPojo.Datum> mDatumList){
        this.mContext = context;
        this.mDatumList = mDatumList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount(){
        return mDatumList.size();
    }

    @Override
    public Object getItem(int position){
        return mDatumList.get(position);
    }

    @Override
    public long getItemId(int position){
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final FeedsListViewHolder holder;

        //if it is a new item
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_user_notification_list, parent, false);

            holder = new FeedsListViewHolder();

            holder.ivUserImage = (ImageView) convertView.findViewById(R.id.row_user_notification_list_iv_user_profile);
            holder.tvContent = (TextView) convertView.findViewById(R.id.row_user_notification_list_tv_content);
            holder.tvTime = (TextView) convertView.findViewById(R.id.row_user_notification_list_tv_content_time);
            holder.ivSocialType = (ImageView) convertView.findViewById(R.id.row_user_notification_list_iv_content_type);

            convertView.setTag(holder);
        } else{
            //here we recycle the previous ViewHolder, by using an older one
            holder = (FeedsListViewHolder) convertView.getTag();
        }
        holder.tvContent.setText(mDatumList.get(position).getText());

        String imageUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(mContext) + "/" + mDatumList.get(position).getUserId();

        Logger.Error("<<<< Image URL " + imageUrl);

        if(mDatumList.get(position).getIsLike() != null && mDatumList.get(position).getIsLike()){

            holder.ivSocialType.setImageResource(R.drawable.ic_liked);

        } else if(mDatumList.get(position).getIsComment() != null && mDatumList.get(position).getIsComment()){
            holder.ivSocialType.setImageResource(R.drawable.ic_small_comment_green);
        } else if(mDatumList.get(position).getIsShare() != null && mDatumList.get(position).getIsShare()){
            holder.ivSocialType.setImageResource(R.drawable.ic_small_share_blue);
        }


        Glide.with(mContext).load(imageUrl).asBitmap().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).dontAnimate().centerCrop().into(new BitmapImageViewTarget(holder.ivUserImage){
            @Override
            protected void setResource(Bitmap resource){
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.ivUserImage.setImageDrawable(circularBitmapDrawable);
            }
        });

        String startDateString = String.valueOf(mDatumList.get(position).getDate());


        Long datelong = mDatumList.get(position).getDate();
        String monthYear = DateFormat.format("MMM dd,yyyy", datelong).toString();
        String hourtime = DateFormat.format("hh:mm a", datelong).toString();


        holder.tvTime.setText(hourtime);
        return convertView;
    }

    public class FeedsListViewHolder {

        private ImageView ivUserImage;
        private TextView tvContent;
        private ImageView ivSocialType;
        private TextView tvTime;

    }


}
