package com.ingauge.adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.listener.RvClickListenerForLocationGrpSocialInteraction;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.FilterBaseDataForLocationGroup;

import java.util.ArrayList;
import java.util.List;


public class SocialInteractionFilterLocGrpListAdapter extends RecyclerView.Adapter<SocialInteractionFilterLocGrpListAdapter.FilterViewHolder> implements Filterable {
    Activity context;
    List<FilterBaseDataForLocationGroup> mBaseDataList;
    private List<FilterBaseDataForLocationGroup> dictionaryWords;
    private List<FilterBaseDataForLocationGroup> filteredList;
    private CustomFilter mFilter;
    private static RvClickListenerForLocationGrpSocialInteraction itemListener;
    private boolean isFromDetails = false;
    private String selectedId = "";
    private String selectedName = "";
    private int lastCheckedPosition=0;


    public SocialInteractionFilterLocGrpListAdapter(Activity context, List<FilterBaseDataForLocationGroup> mBaseDataList, RvClickListenerForLocationGrpSocialInteraction itemListener, boolean isFromDetails, String selectedId, String selectedName) {
        this.context = context;
        this.mBaseDataList = mBaseDataList;
        this.itemListener = itemListener;
        this.isFromDetails = isFromDetails;
        dictionaryWords = mBaseDataList;
        filteredList = new ArrayList<>();
        filteredList.addAll(dictionaryWords);
        this.selectedId = selectedId;
        this.selectedName = selectedName;
        mFilter = new CustomFilter(SocialInteractionFilterLocGrpListAdapter.this);


    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_filter_list_social_int_loc_grp_left, parent, false);
        return new SocialInteractionFilterLocGrpListAdapter.FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        holder.tvTitle.setText(mBaseDataList.get(position).name);

        /*if (TextUtils.isEmpty(mBaseDataList.get(position).name)) {
            holder.tvTitle.setVisibility(View.GONE);
        } else {
            holder.tvTitle.setVisibility(View.VISIBLE);
        }*/

        if (mBaseDataList.get(position).selectedLocationGroupCount > 0) {
            holder.tvSelectedCount.setVisibility(View.VISIBLE);
            holder.tvSelectedCount.setText(String.valueOf(mBaseDataList.get(position).selectedLocationGroupCount));
        } else {
            holder.tvSelectedCount.setVisibility(View.GONE);
        }
        if (position== lastCheckedPosition) {
            mBaseDataList.get(position).isSelectedPosition = true;
        } else {
            mBaseDataList.get(position).isSelectedPosition = false;
        }

        if (mBaseDataList.get(position).isSelectedPosition) {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context,R.color.pink_text_color_for_filter));
        } else {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context,R.color.black));
        }

        /*if (!isFromDetails) {
            holder.ivRight.setVisibility(View.VISIBLE);
            holder.tvFilterValue.setVisibility(View.VISIBLE);
        } else {
            holder.ivRight.setVisibility(View.GONE);
            holder.tvFilterValue.setVisibility(View.GONE);
            holder.tvTitle.setText(mBaseDataList.get(position).name);
        }*/

    }

    @Override
    public int getItemCount() {
        return mBaseDataList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        //public ImageView ivRight;
        private TextView tvSelectedCount;


        public FilterViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.row_item_filter_list_tv_name);
            tvSelectedCount = (TextView) view.findViewById(R.id.row_item_filter_list_social_int_loc_grp_tv_count);
            tvSelectedCount.setVisibility(View.GONE);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mBaseDataList != null) {
                lastCheckedPosition = getLayoutPosition();
                /*FilterBaseDataForLocationGroup mFilterBaseData = mBaseDataList.get(getLayoutPosition());
                FilterBaseDataForLocationGroup mUpdateFilterBaseData = mFilterBaseData;
                mUpdateFilterBaseData.isSelectedPosition = !mUpdateFilterBaseData.isSelectedPosition;
                mBaseDataList.set(getLayoutPosition(),mUpdateFilterBaseData);*/
                notifyDataSetChanged();
                itemListener.recyclerViewListClicked(v, mBaseDataList.get(getLayoutPosition()), getLayoutPosition(), mBaseDataList.get(getLayoutPosition()).filterIndex, true);
            }
        }
    }

    public class CustomFilter extends Filter {
        private SocialInteractionFilterLocGrpListAdapter mAdapter;

        private CustomFilter(SocialInteractionFilterLocGrpListAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(dictionaryWords);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final FilterBaseDataForLocationGroup mWords : dictionaryWords) {
                    if (mWords.name.toLowerCase().contains(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }
            }
            System.out.println("Count Number " + filteredList.size());
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println("Count Number 2 " + ((List<FilterBaseData>) results.values).size());
            mBaseDataList = (ArrayList<FilterBaseDataForLocationGroup>) results.values;
            this.mAdapter.notifyDataSetChanged();
        }
    }

    private int getIndexByProperty(String selected) {
        for (int i = 0; i < mBaseDataList.size(); i++) {
            if (mBaseDataList.get(i) != null && mBaseDataList.get(i).id.equals(selected)) {
                return i;
            }
        }
        return 0;// not there is list
    }
}