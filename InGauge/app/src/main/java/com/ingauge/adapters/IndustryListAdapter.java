package com.ingauge.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.DashboardModelList;
import com.ingauge.pojo.IndustryDataModel;
import com.ingauge.session.InGaugeSession;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pathanaa on 31-05-2017.
 */

public class IndustryListAdapter extends BaseAdapter {
    private Context context; //context
    List<IndustryDataModel.Datum> industryList;
    private static RecyclerViewClickListener itemListener;

    public IndustryListAdapter(Context context,List<IndustryDataModel.Datum> industryList) {
        this.industryList = industryList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return industryList.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return industryList.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row

        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_listview_more, parent, false);
            holder = new ViewHolder();
            holder.txtListItem = (TextView) convertView.findViewById(R.id.txt_list_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtListItem.setText(industryList.get(position).getDescription());
        return convertView;
    }

    class ViewHolder {
        TextView txtListItem;

    }


}