package com.ingauge.adapters;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ingauge.R;
import com.ingauge.activities.DownloadProgressActivity;
import com.ingauge.activities.HomeActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.AttachmentModel;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 29-Jul-17.
 */

public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.ViewHolder> {

    private List<AttachmentModel> attachmentModelList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private boolean isFromCompose;
    String downloadFileUrl = UiUtils.baseUrl + "api/userMessage/attachment/";

    public static final String MESSAGE_PROGRESS = "message_progress";
    private Context mContext;
    private int totalFileSize;
    int downloadprogress = 0;
    int downloadcurrentFileSize = 0;
    int downloadtotalFileSize = 0;
    int emailId = 0;
    APIInterface apiInterface;

    // data is passed into the constructor
    public AttachmentAdapter(Context context, List<AttachmentModel> attachmentModelList, boolean isFromCompose, int emailId) {
        this.mInflater = LayoutInflater.from(context);
        this.attachmentModelList = attachmentModelList;
        this.isFromCompose = isFromCompose;
        this.mContext = context;
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.emailId = emailId;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_attachement, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        AttachmentModel attachmentModel = attachmentModelList.get(position);
        String fileTypeTitle = "";
        if (isFromCompose) {
            if (attachmentModel.extension.length() > 2)
                fileTypeTitle = attachmentModel.extension.substring(1);
            holder.tvFileTypeTitle.setText(fileTypeTitle);
        } else {
            holder.tvFileTypeTitle.setText(attachmentModel.fileType);
        }
        String fileNameToCheck = emailId + "_" + attachmentModelList.get(position).attachmentid + "_" + attachmentModelList.get(position).filename;
        String filePath = mContext.getExternalFilesDir(null) + File.separator + fileNameToCheck;
        final File mFile = new File(filePath);
        if (!isFromCompose) {
            if (mFile.exists()) {
                holder.ivDelete.setVisibility(View.VISIBLE);
            } else {
                holder.ivDelete.setVisibility(View.GONE);
            }
        }

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFromCompose) {
                    attachmentModelList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, attachmentModelList.size());
                    notifyDataSetChanged();
                }
            }
        });
        holder.rlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFile.exists()) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);

                        String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(mFile).toString()).toLowerCase();
                        String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
//                    String mimetype = getMimeType(mContext, Uri.fromFile(mFile));
                        // intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setDataAndType(Uri.fromFile(mFile), mimetype);
                        // intent.setType("*/*");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        //   intent.putExtra(Intent.EXTRA_MIME_TYPES,mimetype);
                        mContext.startActivity(intent);
                    }  catch (ActivityNotFoundException e) {
                        Toast.makeText(mContext,mContext.getResources().getString(R.string.no_app_found_for_view), Toast.LENGTH_SHORT).show();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    String downloadUrl = downloadFileUrl + attachmentModelList.get(position).attachmentid;
                    String fileName = emailId + "_" + attachmentModelList.get(position).attachmentid + "_" + attachmentModelList.get(position).filename;
                    startDownload(downloadUrl, fileName, mContext);
                }

            }
        });
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return attachmentModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvFileTypeTitle;
        private ImageView ivDelete;
        private RelativeLayout rlImage;


        public ViewHolder(View itemView) {
            super(itemView);
            tvFileTypeTitle = (TextView) itemView.findViewById(R.id.item_attachment_tv_file_type);
            ivDelete = (ImageView) itemView.findViewById(R.id.item_attachment_iv_delete);
            rlImage = (RelativeLayout) itemView.findViewById(R.id.item_attachment_rl);

            if (!isFromCompose) {
                ivDelete.setBackground(mContext.getDrawable(R.drawable.ic_file_downloaded));

            } else {
                ivDelete.setBackground(mContext.getDrawable(R.drawable.ic_attach_delete));
            }


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public AttachmentModel getItem(int id) {
        return attachmentModelList.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    private void sendIntent(int totalsize, int currentfileSize, int progress) {

        Intent intent = new Intent(MESSAGE_PROGRESS);
        intent.putExtra("progress", progress);
        intent.putExtra("currentfile_size", currentfileSize);
        intent.putExtra("totalfile_size", totalsize);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    private void startDownload(String fileURL, final String filename, final Context mContext) {
        startProgress(mContext);
        final Call<ResponseBody> call = apiInterface.downloadFileWithDynamicUrlAsync(fileURL);


        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    Logger.Error("<<<  server contacted and has file >>>>");

                    new AsyncTask<String, String, String>() {

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            //showDialog(progress_bar_type);



                        }

                        @Override
                        protected String doInBackground(String... voids) {
                            Logger.Error("<<<  server contacted and has file >>>>");
                            try {
                                ResponseBody body = call.clone().execute().body();
                                int count;
                                byte data[] = new byte[1024 * 4];
                                long fileSize = body.contentLength();
                                InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);

                                File outputFile = new File(mContext.getExternalFilesDir(null) + File.separator + filename);
                                OutputStream output = new FileOutputStream(outputFile);
                                long total = 0;
                                long startTime = System.currentTimeMillis();
                                int timeCount = 1;

                                while ((count = bis.read(data)) != -1) {
                                    total += count;
                                    totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
                                    // publishProgress("" + (int) ((total * 100) / totalFileSize));

//                                    progressDialog.setProgress((int) ((total * 100) / totalFileSize));
                                    double current = Math.round(total / (Math.pow(1024, 2)));
                                    //showProgressDialogHorizontal((int)total * 100);
                                    int progress = (int) ((total * 100) / fileSize);
                                    long currentTime = System.currentTimeMillis() - startTime;
                                    //Download download = new Download();
                                    //download.setTotalFileSize(totalFileSize);
                                    if (currentTime > 1000 * timeCount) {
                                        //download.setCurrentFileSize((int) current);
                                        //download.setProgress(progress);
                                        //sendNotification(download);
                                        downloadprogress = progress;
                                        downloadcurrentFileSize = (int) current;
                                        downloadtotalFileSize = totalFileSize;
                                        sendIntent(downloadtotalFileSize, downloadcurrentFileSize, downloadprogress);
                                        timeCount++;
                                    }
                                    output.write(data, 0, count);
                                }
                                output.flush();
                                output.close();
                                bis.close();
                                endProgress();

                                /*boolean writtenToDisk = false;
                                writtenToDisk = writeResponseBodyToDisk(call.clone().execute().body(), filename);
                                Logger.Error("<<<  file download was a success? " + writtenToDisk + ">>>>");*/
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                            return null;
                        }

                        @Override
                        protected void onPostExecute(String aVoid) {
                            super.onPostExecute(aVoid);
                            //progressDialog.dismiss();
                            endProgress();
                            notifyDataSetChanged();
                        }

                        @Override
                        protected void onProgressUpdate(String... values) {
                            super.onProgressUpdate(values);

                            // progressDialog.setProgress(Integer.parseInt(values[0]));
                        }
                    }.execute();
                } else {

                    Logger.Error("server contact failed!!!! to download File");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Logger.Error("error");

            }
        });
    }

    public void startProgress(Context mContext) {
        try {
            HomeActivity mHomeActivity = (HomeActivity) mContext;
            Intent i = new Intent(mHomeActivity, DownloadProgressActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void endProgress() {
        try {
            if (DownloadProgressActivity.instance != null)
                DownloadProgressActivity.instance.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}