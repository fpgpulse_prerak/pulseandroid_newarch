package com.ingauge.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.listener.SurveyDiscardMessageEnable;
import com.ingauge.pojo.ObsQuestionModelLocal;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by pathanaa on 13-07-2017.
 */

public class ObservationSurveyQuestionAdapter extends BaseAdapter implements StickyListHeadersAdapter{
    private Context mContext;


    private HomeActivity mHomeActivity;

    int questionnumber = 1;
    int basesection = 1;

    protected LayoutInflater inflater;
    ArrayList<ObsQuestionModelLocal> obsQuestionModelLocalArrayList = new ArrayList<>();
    SurveyDiscardMessageEnable surveyDiscardMessageEnable;

    public ObservationSurveyQuestionAdapter(Context context, ArrayList<ObsQuestionModelLocal> arrayObsQuestion, HomeActivity mHomeActivity, SurveyDiscardMessageEnable surveyDiscardMessageEnable){
        this.mContext = context;
        this.mHomeActivity = mHomeActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        obsQuestionModelLocalArrayList = arrayObsQuestion;
        this.surveyDiscardMessageEnable = surveyDiscardMessageEnable;

    }


    @Override
    public int getCount(){
        return obsQuestionModelLocalArrayList.size();
    }

    @Override
    public Object getItem(int position){
        return obsQuestionModelLocalArrayList.get(position);
    }

    @Override
    public long getItemId(int position){
        return Long.parseLong(obsQuestionModelLocalArrayList.get(position).getQ_id());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        final ViewHolder holder;

        //if it is a new item
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_question_survey, parent, false);

            holder = new ViewHolder();

            holder.tvQuestionTitle = (TextView) convertView.findViewById(R.id.row_question_survey_tv_question_title_index);
            holder.tvQuestionDescription = (TextView) convertView.findViewById(R.id.row_question_survey_tv_question_title_text);
            holder.tvYes = (TextView) convertView.findViewById(R.id.row_question_survey_tv_answer_yes);
            holder.tvNA = (TextView) convertView.findViewById(R.id.row_question_survey_tv_answer_na);

            holder.tvAnswerValue = (TextView) convertView.findViewById(R.id.row_question_survey_tv_answer_value);

            holder.llAnswerLabelEdits = (LinearLayout) convertView.findViewById(R.id.row_question_survey_ll_answer_label_edits);
            holder.llAnswerLabelViews = (LinearLayout) convertView.findViewById(R.id.row_question_survey_ll_answer_label_view);

            convertView.setTag(holder);
        } else{
            //here we recycle the previous ViewHolder, by using an older one
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvYes.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_cc_ans_yes)));
        holder.tvNA.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_cc_ans_na)));

       /* holder.titleTextView.setText("Title: " + currentItems.get(position));
        holder.subTitleTextView.setText("Subtitle: " + currentItems.get(position));*/
        holder.tvQuestionTitle.setText("Question: " + " " + mHomeActivity.arrayObsQuestion.get(position).getQ_number());
        holder.tvQuestionDescription.setText(mHomeActivity.arrayObsQuestion.get(position).getQ_text());
        if(mHomeActivity.isOnlyView){
            holder.llAnswerLabelEdits.setVisibility(View.GONE);
            holder.llAnswerLabelViews.setVisibility(View.VISIBLE);
            if(mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerYes())){
                holder.tvAnswerValue.setText(UiUtils.AnswerYes());
            } else if(mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerNA())){
                holder.tvAnswerValue.setText(UiUtils.AnswerNA());
            } else{
                holder.tvAnswerValue.setText(UiUtils.AnswerNo());
            }


            /*tvSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    setNotSelected(mBtnNA);
                    setNotSelected(mBtnYes);
                    mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());

                    NavigateNext();
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_agent_score)) + " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
                    bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
                    bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
                    bundle.putString("survey_entity", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), ""));
                    bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), ""));
                    bundle.putString("question", mHomeActivity.arrayObsQuestion.get(position).getQ_text());
                    bundle.putString("answer", tvSkip.getText().toString());
                    InGaugeApp.getFirebaseAnalytics().logEvent("select_answer", bundle);
                }
            });*/
        } else{
            holder.llAnswerLabelEdits.setVisibility(View.VISIBLE);
            holder.llAnswerLabelViews.setVisibility(View.GONE);

            if(mHomeActivity.arrayObsQuestion.get(position).getQ_yes_nona().equalsIgnoreCase("YESNONA")){
                holder.tvYes.setVisibility(View.VISIBLE);
                holder.tvNA.setVisibility(View.VISIBLE);
            } else{
                holder.tvYes.setVisibility(View.VISIBLE);
                holder.tvNA.setVisibility(View.GONE);
            }
            if(mHomeActivity.arrayObsQuestion.get(position).isYesSelected()){
                setAnswerYesSelected(holder);
            } else if(mHomeActivity.arrayObsQuestion.get(position).isNASelected()){
                setAnswerNASelected(holder);
            } else{
                holder.tvNA.setBackgroundColor(ContextCompat.getColor(mHomeActivity, android.R.color.transparent));
                holder.tvNA.setTextColor(ContextCompat.getColor(mHomeActivity, R.color.green_for_filter));

                holder.tvYes.setBackgroundColor(ContextCompat.getColor(mHomeActivity, android.R.color.transparent));
                holder.tvYes.setTextColor(ContextCompat.getColor(mHomeActivity, R.color.green_for_filter));
            }


            if(mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerYes())){
                mHomeActivity.arrayObsQuestion.get(position).setYesSelected(true);
                mHomeActivity.arrayObsQuestion.get(position).setNASelected(false);
                setAnswerYesSelected(holder);

            } else if(mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerNA())){
                mHomeActivity.arrayObsQuestion.get(position).setYesSelected(false);
                mHomeActivity.arrayObsQuestion.get(position).setNASelected(true);
                setAnswerNASelected(holder);
            }


            holder.tvYes.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    if(mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerYes())){
                        // setNotSelected(mBtnYes);
                        //mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());
                        setDeselectYesAnswer(holder);
                        mHomeActivity.arrayObsQuestion.get(position).setYesSelected(false);
                        mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());
                    } else{
                    /*    setSelected(mBtnYes);
                        setNotSelected(mBtnNA);*/
                        surveyDiscardMessageEnable.enabledtoDiscardMessage();
                        setAnswerYesSelected(holder);
                        mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerYes());
                        mHomeActivity.arrayObsQuestion.get(position).setYesSelected(true);
                        mHomeActivity.arrayObsQuestion.get(position).setNASelected(false);
                        //NavigateNext();
                    }
                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score) + " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);

                    Logger.Error("<<<< Agent Score: >>>> " + mHomeActivity.agentScore);
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
                    bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
                    bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
                    bundle.putString("survey_entity", "Observation");
                    bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), ""));
                    bundle.putString("question", mHomeActivity.arrayObsQuestion.get(position).getQ_text());
                    bundle.putString("answer", holder.tvYes.getText().toString());
                    InGaugeApp.getFirebaseAnalytics().logEvent("select_answer", bundle);
                }


            });

            holder.tvNA.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    if(mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerNA())){
                        //    setNotSelected(mBtnNA);
                        //  mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());
                        setDeselectNAAnswer(holder);
                        mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());
                        mHomeActivity.arrayObsQuestion.get(position).setNASelected(false);
                    } else{
                        /*setSelected(mBtnNA);
                        setNotSelected(mBtnYes);*/
                        surveyDiscardMessageEnable.enabledtoDiscardMessage();
                        setAnswerNASelected(holder);
                        mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNA());
                        mHomeActivity.arrayObsQuestion.get(position).setYesSelected(false);
                        mHomeActivity.arrayObsQuestion.get(position).setNASelected(true);
                        //NavigateNext();
                    }
                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score) + " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);

                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
                    bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
                    bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
                    bundle.putString("survey_entity", "Observation");
                    bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), ""));
                    bundle.putString("question", mHomeActivity.arrayObsQuestion.get(position).getQ_text());
                    bundle.putString("answer", holder.tvNA.getText().toString());
                    InGaugeApp.getFirebaseAnalytics().logEvent("select_answer", bundle);
                }
            });
           /* tvSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerNA())||
                            mHomeActivity.arrayObsQuestion.get(position).getQ_yesnona_applicable().equalsIgnoreCase(UiUtils.AnswerYes())) {
                        setNotSelected(mBtnNA);
                        setNotSelected(mBtnYes);
                        mHomeActivity.arrayObsQuestion.get(position).setQ_yesnona_applicable(UiUtils.AnswerNo());
                    }
                    NavigateNext();
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_agent_score)) + " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
                    bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
                    bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
                    bundle.putString("survey_entity", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), ""));
                    bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), ""));
                    bundle.putString("question", mHomeActivity.arrayObsQuestion.get(position).getQ_text());
                    bundle.putString("answer", tvSkip.getText().toString());
                    InGaugeApp.getFirebaseAnalytics().logEvent("select_answer", bundle);
                }
            });*/
        }

        return convertView;
    }


    private void setDeselectYesAnswer(ViewHolder mViewHolder){
        mViewHolder.tvYes.setBackgroundColor(ContextCompat.getColor(mHomeActivity, android.R.color.transparent));
        mViewHolder.tvYes.setTextColor(ContextCompat.getColor(mHomeActivity, R.color.green_for_filter));
    }

    private void setAnswerYesSelected(ViewHolder mViewHolder){
        mViewHolder.tvYes.setBackgroundColor(ContextCompat.getColor(mHomeActivity, R.color.green_for_filter));
        mViewHolder.tvYes.setTextColor(ContextCompat.getColor(mHomeActivity, R.color.white));

        mViewHolder.tvNA.setBackgroundColor(ContextCompat.getColor(mHomeActivity, android.R.color.transparent));
        mViewHolder.tvNA.setTextColor(ContextCompat.getColor(mHomeActivity, R.color.green_for_filter));
    }

    private void setDeselectNAAnswer(ViewHolder mViewHolder){
        mViewHolder.tvNA.setBackgroundColor(ContextCompat.getColor(mHomeActivity, android.R.color.transparent));
        mViewHolder.tvNA.setTextColor(ContextCompat.getColor(mHomeActivity, R.color.green_for_filter));
    }

    private void setAnswerNASelected(ViewHolder mViewHolder){
        mViewHolder.tvNA.setBackgroundColor(ContextCompat.getColor(mHomeActivity, R.color.green_for_filter));
        mViewHolder.tvNA.setTextColor(ContextCompat.getColor(mHomeActivity, R.color.white));

        mViewHolder.tvYes.setBackgroundColor(ContextCompat.getColor(mHomeActivity, android.R.color.transparent));
        mViewHolder.tvYes.setTextColor(ContextCompat.getColor(mHomeActivity, R.color.green_for_filter));
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent){
        DividerViewHolder holder;

        if(convertView == null){
            holder = new DividerViewHolder();
            convertView = inflater.inflate(R.layout.list_view_header_layout, parent, false);
            holder.tvSectionIndex = (TextView) convertView.findViewById(R.id.list_view_header_layout_tv_section_number);
            holder.tvSectionLabel = (TextView) convertView.findViewById(R.id.list_view_header_layout_tv_section_text);
            convertView.setTag(holder);
        } else{
            holder = (DividerViewHolder) convertView.getTag();
        }

        holder.tvSectionIndex.setText("Section: " + mHomeActivity.arrayObsQuestion.get(position).getSection());
        holder.tvSectionLabel.setText(mHomeActivity.arrayObsQuestion.get(position).getQ_title());


        return convertView;
    }

    @Override
    public long getHeaderId(int position){
        return obsQuestionModelLocalArrayList.get(position).getSection();
    }


    private class ViewHolder{
        TextView tvQuestionTitle;
        TextView tvQuestionDescription;
        TextView tvYes;
        TextView tvNA;
        TextView tvAnswerValue;
        LinearLayout llAnswerLabelViews;
        LinearLayout llAnswerLabelEdits;
    }

    public static class DividerViewHolder{
        TextView tvSectionIndex;
        TextView tvSectionLabel;
    }
}
