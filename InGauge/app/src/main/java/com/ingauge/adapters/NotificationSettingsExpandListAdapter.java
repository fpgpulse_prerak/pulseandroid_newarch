package com.ingauge.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.pojo.NotificationSettingsModel;
import com.ingauge.utils.Logger;

import java.util.List;

/**
 * Created by mansurum on 06-Apr-18.
 */

public class NotificationSettingsExpandListAdapter extends BaseExpandableListAdapter{

    private Context mContext;
    private List<NotificationSettingsModel.Datum> mDatumList;
    GroupViewHolder holder;
    NotificationSettingsModel.Datum mDatumUpdate;

    public NotificationSettingsExpandListAdapter(Context context, List<NotificationSettingsModel.Datum> mList){
        this.mContext = context;
        this.mDatumList = mList;
    }

    @Override
    public int getGroupCount(){
        return mDatumList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition){
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition){
        return mDatumList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition){
        return mDatumList.get(groupPosition).getIsInternalMail();
    }

    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition){
        return true;
    }

    @Override
    public boolean hasStableIds(){
        return false;
    }
/*
    @Override
    public boolean areAllItemsEnabled(){
        return true;
    }*/


    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent){


        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_fragment_notification_settings_list_group, parent, false);
            holder = new GroupViewHolder();
            holder.tvNotificationType = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_tv_notifiction_type);
            holder.tvNotificationMessage = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_tv_notification_message);
            holder.mSwitchCompatNotification = (SwitchCompat) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_switch);
            holder.ivGroupIcon = (ImageView) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_iv_open_close);

            convertView.setTag(holder);
        } else{
            holder = (GroupViewHolder) convertView.getTag();
        }



        Logger.Error("<<<<< Notification >>>>  group Position  >>>>   " + groupPosition);
        Logger.Error("<<<<< Notification >>>>  group Position  text >>>>   " + mDatumList.get(groupPosition).getLabel());
        mDatumUpdate = mDatumList.get(groupPosition);
        holder.tvNotificationType.setText(mDatumList.get(groupPosition).getLabel());
        holder.tvNotificationMessage.setText(mDatumList.get(groupPosition).getTooltip());
        /*ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);*/

        /*if(isExpandRequire(mDatumList.get(groupPosition))){
            //holder.mSwitchCompatNotification.setChecked(true);
            ((ExpandableListView) parent).expandGroup(groupPosition, true);
        }else{
            //holder.mSwitchCompatNotification.setChecked(false);
            ((ExpandableListView) parent).collapseGroup(groupPosition);

        }*/

       /* holder.mSwitchCompatNotification.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if(holder.mSwitchCompatNotification.isChecked()){
                    holder.mSwitchCompatNotification.setChecked(false);
                }else{
                    holder.mSwitchCompatNotification.setChecked(true);
                }

            }
        });*/
        if ( isExpanded ) {
            holder.ivGroupIcon.setBackground ( mContext.getResources ().getDrawable ( R.drawable.ic_down_arrow) );
        } else {
            holder.ivGroupIcon.setBackground ( mContext.getResources ().getDrawable ( R.drawable.ic_right_arrow ));
        }
        Logger.Error("<<<<< Notification >>>>  " + "POSITION    " + groupPosition + "  " + mDatumList.get(groupPosition).isGroupOpended());
        if(isExpandRequire(mDatumList.get(groupPosition))){

            ((ExpandableListView) parent).expandGroup(groupPosition, false);
        } else{
            ((ExpandableListView) parent).collapseGroup ( groupPosition );

        }



       /* if(isExpandRequire(mDatumList.get(groupPosition))){
            holder.mSwitchCompatNotification.setChecked(true);

        } else{
            holder.mSwitchCompatNotification.setChecked(false);
        }*/
      /*  holder.mSwitchCompatNotification.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

            }
        });*/

        holder.ivGroupIcon.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Logger.Error ( "<<<< Click on Image Button >>" );

                if ( isExpanded ) {
                    ((ExpandableListView) parent).collapseGroup ( groupPosition );
                } else {
                    ((ExpandableListView) parent).expandGroup ( groupPosition, true );
                }
            }
        } );
        // holder.mSwitchCompatNotification.setOnCheckedChangeListener(new CheckUpdateListener(mDatumUpdate));
        holder.mSwitchCompatNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isExpanded){
                    if(isChecked){
                        ((ExpandableListView) parent).collapseGroup(groupPosition);
                        holder.mSwitchCompatNotification.setOnCheckedChangeListener(new CheckUpdateListener(mDatumUpdate, groupPosition));
                    } else{
                        ((ExpandableListView) parent).expandGroup(groupPosition, true);
                        holder.mSwitchCompatNotification.setOnCheckedChangeListener(new CheckUpdateListener(mDatumUpdate, groupPosition));
                    }


                } else{
                    if(!isChecked){
                        ((ExpandableListView) parent).expandGroup(groupPosition, true);
                        holder.mSwitchCompatNotification.setOnCheckedChangeListener(new CheckUpdateListener(mDatumUpdate, groupPosition));
                    } else{
                        ((ExpandableListView) parent).collapseGroup(groupPosition);
                        holder.mSwitchCompatNotification.setOnCheckedChangeListener(new CheckUpdateListener(mDatumUpdate, groupPosition));
                    }


                }
                // notifyDataSetChanged();
            }
        });

        return convertView;
    }

    /******************* Checkbox Checked Change Listener ********************/

    private final class CheckUpdateListener implements CompoundButton.OnCheckedChangeListener{
        private final NotificationSettingsModel.Datum parent;
        private int position = 0;

        private CheckUpdateListener(NotificationSettingsModel.Datum parent, int position){
            this.parent = parent;
            this.position = position;
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
            Log.i("onCheckedChanged", "isChecked: " + isChecked);
            //parent.setGroupOpended(isChecked);
            NotificationSettingsModel.Datum mDatumUpdate = parent;
            mDatumUpdate.setGroupOpended(isChecked);
            mDatumList.set(position, mDatumUpdate);
            notifyDataSetChanged();

            final Boolean checked = parent.isGroupOpended();
            Logger.Error("<<<<< Parent Checed " + checked);

        }
    }

    public boolean isExpandRequire(NotificationSettingsModel.Datum mDatum){

        if(
                (mDatum.getIsExternalMail() != null && mDatum.getIsExternalMail()) ||
                        (mDatum.getIsInternalMail() != null && mDatum.getIsInternalMail())
                        || (mDatum.getIsPushNotification() != null && mDatum.getIsPushNotification())
                ){

            return true;
        }
        return false;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        GroupViewHolder holder;
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_fragment_notification_settings_list_child, parent, false);
            holder = new GroupViewHolder();
            holder.tvExternalMail = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_tv_ext_mail);
            holder.tvInternalMail = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_tv_int_mail);
            holder.tvPushNotification = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_tv_push_notification);
            holder.mSwitchExternalMail = (SwitchCompat) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_switch_ext_mail);
            holder.mSwitchInternalMail = (SwitchCompat) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_switch_int_mail);
            holder.mSwitchPushNotification = (SwitchCompat) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_switch_push_notification);

            convertView.setTag(holder);

        } else{
            holder = (GroupViewHolder) convertView.getTag();
        }

        if(mDatumList.get(groupPosition).getIsExternalMail() != null){
            holder.mSwitchExternalMail.setChecked(mDatumList.get(groupPosition).getIsExternalMail());
        }
        if(mDatumList.get(groupPosition).getIsInternalMail() != null){
            holder.mSwitchExternalMail.setChecked(mDatumList.get(groupPosition).getIsInternalMail());
        }
        if(mDatumList.get(groupPosition).getIsPushNotification() != null){
            holder.mSwitchExternalMail.setChecked(mDatumList.get(groupPosition).getIsPushNotification());
        }
        return convertView;
    }


    class GroupViewHolder{
        TextView tvNotificationType;
        TextView tvNotificationMessage;
        SwitchCompat mSwitchCompatNotification;
        ImageView ivGroupIcon;

        TextView tvExternalMail;
        TextView tvInternalMail;
        TextView tvPushNotification;
        SwitchCompat mSwitchExternalMail;
        SwitchCompat mSwitchInternalMail;
        SwitchCompat mSwitchPushNotification;
    }


    @Override
    public void registerDataSetObserver(DataSetObserver observer){
        super.registerDataSetObserver(observer);
    }
}
