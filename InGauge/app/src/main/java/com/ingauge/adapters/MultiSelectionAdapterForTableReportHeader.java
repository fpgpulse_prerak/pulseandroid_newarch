package com.ingauge.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.ObservationTabActivity;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.TableReportColumnPojo;

import java.util.ArrayList;
import java.util.List;


public class MultiSelectionAdapterForTableReportHeader extends RecyclerView.Adapter<MultiSelectionAdapterForTableReportHeader.FilterViewHolder> implements Filterable {
    Context context;

    List<TableReportColumnPojo> tableheader;
    private List<TableReportColumnPojo> dictionaryWords;
    private List<TableReportColumnPojo> filteredList;
    private CustomFilter mFilter;
    public MultiSelectionAdapterForTableReportHeader(Context context, List<TableReportColumnPojo> tableheader) {
        this.context = context;
        this.tableheader = tableheader;
        dictionaryWords = tableheader;
        filteredList = new ArrayList<>();
        filteredList.addAll(dictionaryWords);
        mFilter = new CustomFilter(MultiSelectionAdapterForTableReportHeader.this);

    }
    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType)  {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_multiselection, parent, false);
        return new MultiSelectionAdapterForTableReportHeader.FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        holder.tvTitle.setText(tableheader.get(position).getColumnKey());
        if(tableheader.get(position).isSelected()){
            holder.ivRight.setVisibility(View.VISIBLE);
        }else{
            holder.ivRight.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
       return tableheader.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tvTitle;
        private TextView tvFilterValue;
        public ImageView ivRight;

        public FilterViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.row_item_filter_list_tv_name);
            ivRight = (ImageView) view.findViewById(R.id.row_item_filter_iv);
            tvFilterValue = (TextView) view.findViewById(R.id.tv_filter_list_select_filter_name);
            view.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if(ivRight.getVisibility()==View.VISIBLE){
                ivRight.setVisibility(View.GONE);
                tableheader.get(getAdapterPosition()).setSelected(false);
            }else{
                int counter=0;
                for(int i=0;i<tableheader.size();i++){
                    if(tableheader.get(i).isSelected()){
                        counter++;
                    }
                }

                if(counter<3) {
                    ivRight.setVisibility(View.VISIBLE);
                    tableheader.get(getAdapterPosition()).setSelected(true);
                }else{
                    new AlertDialog.Builder(context)
                            .setMessage("You can select maximum 3 Column only")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }

            }

        }
    }

    public class CustomFilter extends Filter {
        private MultiSelectionAdapterForTableReportHeader mAdapter;
        private CustomFilter(MultiSelectionAdapterForTableReportHeader mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(dictionaryWords);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final TableReportColumnPojo mWords : dictionaryWords) {
                    if (mWords.getColumnName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }
            }
            System.out.println("Count Number " + filteredList.size());
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println("Count Number 2 " + ((List<TableReportColumnPojo>) results.values).size());
            tableheader = (ArrayList<TableReportColumnPojo>) results.values;
            this.mAdapter.notifyDataSetChanged();
        }
    }
}