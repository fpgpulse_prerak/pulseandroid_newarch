package com.ingauge.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.ObservationTabActivity;


public class MultiSelectionAdapter extends RecyclerView.Adapter<MultiSelectionAdapter.FilterViewHolder> implements Filterable {
    Context context;
    private HomeActivity mHomeActivity;
    public MultiSelectionAdapter(Context context,HomeActivity mHomeActivity) {
        this.context = context;
        this.mHomeActivity=mHomeActivity;

    }
    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType)  {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_multiselection, parent, false);
        return new MultiSelectionAdapter.FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        holder.tvTitle.setText(mHomeActivity.mObservationJobType.getData().get(position).getName());

        if(mHomeActivity.mObservationJobType.getData().get(position).isSelected()){
            holder.ivRight.setVisibility(View.VISIBLE);
        }else{
            holder.ivRight.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if(mHomeActivity.mObservationJobType.getData()==null){
            return 0;
        }else{
            return mHomeActivity.mObservationJobType.getData().size();
        }

    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tvTitle;
        private TextView tvFilterValue;
        public ImageView ivRight;

        public FilterViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.row_item_filter_list_tv_name);
            ivRight = (ImageView) view.findViewById(R.id.row_item_filter_iv);
            tvFilterValue = (TextView) view.findViewById(R.id.tv_filter_list_select_filter_name);
            view.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if(ivRight.getVisibility()==View.VISIBLE){
                ivRight.setVisibility(View.GONE);
                mHomeActivity.mObservationJobType.getData().get(getAdapterPosition()).setSelected(false);
            }else{
                int counter=0;
                for(int i=0;i<mHomeActivity.mObservationJobType.getData().size();i++){
                    if(mHomeActivity.mObservationJobType.getData().get(i).isSelected()){
                        counter++;
                    }
                }

                if(counter<3) {
                    ivRight.setVisibility(View.VISIBLE);
                    mHomeActivity.mObservationJobType.getData().get(getAdapterPosition()).setSelected(true);
                }else{
                    new AlertDialog.Builder(context)
                            .setMessage("You can select maximum 3 Job Types")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }

            }

        }
    }
}