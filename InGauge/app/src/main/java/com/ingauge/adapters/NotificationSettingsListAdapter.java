package com.ingauge.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.NotificationSettingsModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 06-Apr-18.
 */

public class NotificationSettingsListAdapter extends BaseAdapter{

    private Context mContext;
    private List<NotificationSettingsModel.Datum> mDatumList;
    GroupViewHolder holder;
    NotificationSettingsModel.Datum mDatumUpdate;
    APIInterface apiInterface;
    ListView mListView;
    //boolean isNeedSavedSettingsCalled=true;

    public NotificationSettingsListAdapter(Context context, List<NotificationSettingsModel.Datum> mList, ListView mListView){
        this.mContext = context;
        this.mDatumList = mList;
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.mListView = mListView;
    }


    private boolean isExpandRequire(NotificationSettingsModel.Datum mDatum, int position){

        if((mDatum.getIsExternalMail() != null && mDatum.getIsExternalMail()) ||
                (mDatum.getIsInternalMail() != null && mDatum.getIsInternalMail())
                || (mDatum.getIsPushNotification() != null && mDatum.getIsPushNotification())){
            NotificationSettingsModel.Datum mDatumUpdate = mDatum;
            mDatumUpdate.setGroupOpended(true);
            mDatumList.set(position, mDatumUpdate);
            return true;
        }
        return false;
    }


    class GroupViewHolder{
        TextView tvNotificationType;
        TextView tvNotificationMessage;
        SwitchCompat mSwitchCompatNotification;
        ImageView ivGroupIcon;
        ProgressBar mProgressBar;

        TextView tvExternalMail;
        TextView tvInternalMail;
        TextView tvPushNotification;
        SwitchCompat mSwitchExternalMail;
        SwitchCompat mSwitchInternalMail;
        SwitchCompat mSwitchPushNotification;
        LinearLayout llNotificationChild;
    }


    @Override
    public void registerDataSetObserver(DataSetObserver observer){
        super.registerDataSetObserver(observer);
    }

    @Override
    public int getCount(){
        return mDatumList.size();
    }

    @Override
    public Object getItem(int position){
        return mDatumList.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        boolean isNeedHideChild = false;
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_fragment_notification_settings_list, parent, false);
            holder = new GroupViewHolder();
            holder.tvNotificationType = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_tv_notifiction_type);
            holder.tvNotificationMessage = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_tv_notification_message);
            holder.mSwitchCompatNotification = (SwitchCompat) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_switch);
            holder.ivGroupIcon = (ImageView) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_iv_open_close);
            holder.mProgressBar = (ProgressBar) convertView.findViewById(R.id.row_fragment_notification_settings_list_group_pb);


            holder.tvExternalMail = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_tv_ext_mail);
            holder.tvInternalMail = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_tv_int_mail);
            holder.tvPushNotification = (TextView) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_tv_push_notification);
            holder.mSwitchExternalMail = (SwitchCompat) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_switch_ext_mail);
            holder.mSwitchInternalMail = (SwitchCompat) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_switch_int_mail);
            holder.mSwitchPushNotification = (SwitchCompat) convertView.findViewById(R.id.row_fragment_notification_settings_list_child_switch_push_notification);
            holder.llNotificationChild = (LinearLayout) convertView.findViewById(R.id.row_fragment_notification_settings_list_linear);
            convertView.setTag(holder);
        } else{
            holder = (GroupViewHolder) convertView.getTag();
        }
        if(isExpandRequire(mDatumList.get(position), position)){
            holder.mSwitchCompatNotification.setChecked(true);
            holder.llNotificationChild.setVisibility(View.VISIBLE);
            isNeedHideChild = false;
        } else{
            holder.mSwitchCompatNotification.setChecked(false);
            holder.llNotificationChild.setVisibility(View.GONE);
            isNeedHideChild = true;
        }


        Logger.Error("<<<<<< Expand Click  " + isExpandRequire(mDatumList.get(position), position) + "  Position " + position);
        mDatumUpdate = mDatumList.get(position);
        holder.tvNotificationType.setText(mDatumList.get(position).getLabel());
        holder.tvNotificationMessage.setText(mDatumList.get(position).getTooltip());

        if(mDatumList.get(position).getIsExternalMail() != null){
            holder.mSwitchExternalMail.setChecked(mDatumList.get(position).getIsExternalMail());
        }
        if(mDatumList.get(position).getIsInternalMail() != null){
            holder.mSwitchInternalMail.setChecked(mDatumList.get(position).getIsInternalMail());
        }
        if(mDatumList.get(position).getIsPushNotification() != null){
            holder.mSwitchPushNotification.setChecked(mDatumList.get(position).getIsPushNotification());
        }

        holder.mSwitchExternalMail.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if(mDatumList.get(position).getIsExternalMail()){
                    holder.mSwitchExternalMail.setChecked(false);
                    NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                    mDatumUpdate.setIsExternalMail(false);
                    mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                    mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                    mDatumList.set(position, mDatumUpdate);

                    getNotificationSettingsList(mDatumList.get(position), holder.mProgressBar);
                } else{
                    holder.mSwitchExternalMail.setChecked(true);
                    NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                    mDatumUpdate.setIsExternalMail(true);
                    mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                    mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                    mDatumList.set(position, mDatumUpdate);

                    getNotificationSettingsList(mDatumList.get(position), holder.mProgressBar);
                }

            }
        });
        holder.mSwitchInternalMail.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if(mDatumList.get(position).getIsInternalMail()){
                    holder.mSwitchInternalMail.setChecked(false);
                    NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                    mDatumUpdate.setIsInternalMail(false);
                    mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                    mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                    mDatumList.set(position, mDatumUpdate);

                    getNotificationSettingsList(mDatumList.get(position), holder.mProgressBar);
                } else{
                    holder.mSwitchInternalMail.setChecked(true);
                    NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                    mDatumUpdate.setIsInternalMail(true);
                    mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                    mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                    mDatumList.set(position, mDatumUpdate);

                    getNotificationSettingsList(mDatumList.get(position), holder.mProgressBar);
                }

            }
        });
        holder.mSwitchPushNotification.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if(mDatumList.get(position).getIsPushNotification()){
                    holder.mSwitchPushNotification.setChecked(false);
                    NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                    mDatumUpdate.setIsPushNotification(false);
                    mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                    mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                    mDatumList.set(position, mDatumUpdate);

                    getNotificationSettingsList(mDatumList.get(position), holder.mProgressBar);
                } else{
                    holder.mSwitchPushNotification.setChecked(true);
                    NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                    mDatumUpdate.setIsPushNotification(true);
                    mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                    mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                    mDatumList.set(position, mDatumUpdate);

                    getNotificationSettingsList(mDatumList.get(position), holder.mProgressBar);
                }

            }
        });
        /*holder.mSwitchExternalMail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                mDatumUpdate.setIsExternalMail(isChecked);
                mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                mDatumList.set(position, mDatumUpdate);
               // if(isNeedSavedSettingsCalled)
                    getNotificationSettingsList(mDatumList.get(position),holder.mProgressBar);
            }
        });
        holder.mSwitchInternalMail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                mDatumUpdate.setIsInternalMail(isChecked);
                mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                mDatumList.set(position, mDatumUpdate);
               // if(isNeedSavedSettingsCalled)
                    getNotificationSettingsList(mDatumList.get(position),holder.mProgressBar);
            }
        });
        holder.mSwitchPushNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){

                NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                mDatumUpdate.setIsPushNotification(isChecked);
                mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                mDatumList.set(position, mDatumUpdate);
               // if(isNeedSavedSettingsCalled)
                    getNotificationSettingsList(mDatumList.get(position),holder.mProgressBar);
            }
        });

*/
        holder.mSwitchCompatNotification.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if(isExpandRequire(mDatumList.get(position), position)){
                    NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                    mDatumUpdate.setGroupOpended(false);
                    mDatumUpdate.setIsExternalMail(false);
                    mDatumUpdate.setIsInternalMail(false);
                    mDatumUpdate.setIsPushNotification(false);
                    mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                    mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                    mDatumList.set(position, mDatumUpdate);
                    holder.mSwitchCompatNotification.setChecked(false);
                    notifyDataSetChanged();
                    // if(isNeedSavedSettingsCalled)
                    getNotificationSettingsList(mDatumList.get(position), holder.mProgressBar);

                } else{

                    //holder.llNotificationChild.setVisibility(View.VISIBLE);
                    NotificationSettingsModel.Datum mDatumUpdate = mDatumList.get(position);
                    mDatumUpdate.setGroupOpended(true);
                    mDatumUpdate.setIsExternalMail(true);
                    mDatumUpdate.setIsInternalMail(true);
                    mDatumUpdate.setIsPushNotification(true);
                    mDatumUpdate.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                    mDatumUpdate.setMapTo("USERNOTIFICATIONCONFIGURATION");
                    mDatumList.set(position, mDatumUpdate);
                    notifyDataSetChanged();
                    // if(isNeedSavedSettingsCalled)
                    getNotificationSettingsList(mDatumList.get(position), holder.mProgressBar);
                    holder.mSwitchCompatNotification.setChecked(true);
                }



                /*if(holder.llNotificationChild.getVisibility() == View.VISIBLE){
                    //holder.llNotificationChild.setVisibility(View.GONE);


                } else{

                }*/


            }
        });
        return convertView;
    }

    private void getNotificationSettingsList(NotificationSettingsModel.Datum mDatum, final ProgressBar mProgressBar){
        // isNeedSavedSettingsCalled=false;
        Logger.Error("<<<<<<<< Call >>>>>> Notification List Adapter ");
        mProgressBar.setVisibility(View.VISIBLE);
        notifyDataSetChanged();
        Call mCall = apiInterface.savedNotificationSettings(mDatum);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                Snackbar snackbar = Snackbar.make(mListView, "Saved Your Notification Settings", Snackbar.LENGTH_LONG);
                snackbar.show();
                mProgressBar.setVisibility(View.GONE);
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mProgressBar.setVisibility(View.GONE);
                notifyDataSetChanged();

            }
        });
    }
}
