package com.ingauge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.api.APIClient;
import com.ingauge.fragments.VideoFragment;
import com.ingauge.listener.VideoListClickListener;
import com.ingauge.pojo.VideoModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 19-May-17.
 */

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.FeedsAdapterViewHolder>{

    private Context mContext;

    private int lastPosition = -1;
    private List<VideoModel> videoModelList = new ArrayList<>();
    VideoListClickListener mVideoListClickListener;

    public VideoListAdapter(Context mContext, List<VideoModel> videoModelList,VideoListClickListener mVideoListClickListener){

        this.mContext = mContext;
        this.videoModelList = videoModelList;
        this.mVideoListClickListener = mVideoListClickListener;

    }

    @Override
    public VideoListAdapter.FeedsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_video_list, parent, false);

        return new VideoListAdapter.FeedsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VideoListAdapter.FeedsAdapterViewHolder holder, int position){

        VideoModel videoModel = videoModelList.get(position);

        // Here you apply the animation when the view is bound
        //   setAnimation(holder.itemView, position);
        //holder.tvTitle.setText(mDatum.getName());

        if(position==0){
            String mediaId= videoModel.getMediaId();
            String emailId = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), "");
            int userID = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2);
            int IndustryId=InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), -2);

            String videoUrl = UiUtils.baseUrlChart + "vtPlayer?" + "email=" + emailId + "&userId="+userID + "&mediaHasId=" + mediaId + "&industryId=" + IndustryId
                    + "&authorizationId=" + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), "");
            VideoFragment.webVideoUrl = videoUrl;
            Logger.Error("<<<<< Video Url = " + videoUrl );
        }
        String imageUrl = videoModel.getThumbnailURL();
        Glide.with(mContext)
                .load(imageUrl)
                .centerCrop()
                .error(R.drawable.placeholder)
                .into(holder.ivVideoThumbnail);
        holder.tvVideoName.setText(videoModel.getName());
        holder.tvVideoTime.setText(videoModel.getDuration());
    }

    @Override
    public int getItemCount(){
        return videoModelList.size();
    }

    public class FeedsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView ivVideoThumbnail;
        public TextView tvVideoName;
        public TextView tvVideoTime;

        public FeedsAdapterViewHolder(View view){
            super(view);
            tvVideoName = (TextView) view.findViewById(R.id.row_video_list_tv_video_name);
            tvVideoTime = (TextView) view.findViewById(R.id.row_video_list_tv_video_time);
            ivVideoThumbnail = (ImageView) view.findViewById(R.id.row_video_list_iv_thumbnail);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            mVideoListClickListener.onVideoClick(videoModelList.get(getLayoutPosition()).getMediaId(),String.valueOf(0),videoModelList.get(getLayoutPosition()).getName());
        }
    }


    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position){
        // If the bound view wasn't previously displayed on screen, it's animated
        if(position > lastPosition){
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
