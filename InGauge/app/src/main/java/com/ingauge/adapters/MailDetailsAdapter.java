package com.ingauge.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ingauge.R;
import com.ingauge.pojo.InboxModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mansurum on 18-Jul-17.
 */

public class MailDetailsAdapter extends BaseAdapter {


    private Context mContext;
    private Fragment mFragment;
    private List<InboxModel> inboxModelList;
    private static final int MAX_LINE_COUNT = 3;
    private AttachmentAdapter adapter;


    public MailDetailsAdapter(Context mContext, Fragment mFragment, List<InboxModel> inboxModelList) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.inboxModelList = inboxModelList;
    }

    @Override
    public int getCount() {
        return inboxModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return inboxModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder h;
        View v = convertView;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.row_email_detail_expand, null);
            h = new ViewHolder();


            h.tvMailSortBody = (TextView) v.findViewById(R.id.row_email_inbox_tv_short_desc);
            h.tvSender = (TextView) v.findViewById(R.id.row_email_inbox_tv_name_from);
            h.tvSubject = (TextView) v.findViewById(R.id.row_email_inbox_tv_subject);
            h.tvSentTime = (TextView) v.findViewById(R.id.row_email_inbox_tv_date);
            h.tvReceive = (TextView) v.findViewById(R.id.row_email_inbox_tv_name_to);
            //h.tv_to.setVisibility(View.VISIBLE);
            h.mCircleImageView = (CircleImageView) v.findViewById(R.id.row_email_inbox_iv_user_image);
            h.rvAttachments = (RecyclerView) v.findViewById(R.id.rv_attachments);
            h.rvAttachments.setLayoutManager(new GridLayoutManager(mContext, 3));

            //   makeTextViewResizable(h.tvMailSortBody, 3, "View More", true);
            // mImageLoader.get(arraySubMail.get(groupPosition).getProfileUrl(), ImageLoader.getImageListener(h.img_avatar, R.drawable.loading, R.drawable.loading));
            v.setTag(h);
        } else {
            h = (ViewHolder) v.getTag();
            // makeTextViewResizable(h.tvMailSortBody, 3, "View More", true);
        }


        h.tvSender.setText(inboxModelList.get(position).getSender());
        h.tvMailSortBody.setText(Html.fromHtml(inboxModelList.get(position).getMessageSort()));
        adapter = new AttachmentAdapter(mContext, inboxModelList.get(position).getAttachmentModelList(),false,inboxModelList.get(position).getID());
        h.rvAttachments.setAdapter(adapter);
//        h.tvMailSortBody.setText(yourText);



        /*if (!TextUtils.isEmpty(yourText)) {
            h.tvMailSortBody.setText(yourText);
            h.tvMailSortBody.setMaxLines(MAX_LINE_COUNT);
            h.tvMailSortBody.setEllipsize(TextUtils.TruncateAt.END);
        } else {
            h.tvMailSortBody.setVisibility(View.GONE);
        }
        h.tvMailSortBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //collapseExpandTextView(h.tvMailSortBody);
            }
        });*/



       /* if(_listDataHeader.get(groupPosition).isExpanded()){
            h.tvMailSortBody.setVisibility(View.GONE);
        }else{
            h.tvMailSortBody.setVisibility(View.VISIBLE);
        }*/
        Glide.with(mContext)
                .load(inboxModelList.get(position).getProfileUrl())
                .centerCrop()
                .placeholder(R.drawable.defaultimg)
                .error(R.drawable.defaultimg)
                .into(h.mCircleImageView);
        h.tvSubject.setText(inboxModelList.get(position).getSubject());
        h.tvSentTime.setText(inboxModelList.get(position).getSentDate());
        h.tvReceive.setText(inboxModelList.get(position).getToName());
        return v;
    }

    private class ViewHolder {

        TextView tvMailSortBody;
        TextView tvSender;
        TextView tvSubject;
        TextView tvSentTime;
        TextView tvReceive;
        CircleImageView mCircleImageView;
        RecyclerView rvAttachments;

    }


}
