package com.ingauge.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by desainid on 5/12/2017.
 */

public class Listadapter extends ArrayAdapter<String> implements Filterable {
    Activity context;
    List<String> values;
    List<String> description;
    boolean flag = false;
    List<String> original;
    ValueFilter valueFilter;
    List<String> mFilterList;
    boolean flagForImage = false;
    private String selectedName;
    public int lastCheckedPosition = 0;

    public Listadapter(Activity context, List<String> values, boolean flag, boolean flagForImage, String selectedName) {
        super(context, R.layout.item_listview_more, values);
        this.context = context;
        this.values = values;
        this.flag = flag;
        mFilterList = values;
        original = values;
        this.flagForImage = flagForImage;
        this.selectedName = selectedName;
        lastCheckedPosition = getIndexByName(selectedName);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.item_listview_more, null, true);
        if (flag == false) {
            TextView txtListItem = (TextView) listViewItem.findViewById(R.id.txt_list_item);
            ImageView ivRightTick = (ImageView) listViewItem.findViewById(R.id.row_item_filter_iv);
            ivRightTick.setVisibility(View.GONE);
            txtListItem.setText(values.get(position));
            ImageView listImages = (ImageView) listViewItem.findViewById(R.id.list_images);

            if (flagForImage == true) {
                listImages.setVisibility(View.VISIBLE);
                switch (position) {
                    case 0:
                        listImages.setImageResource(R.drawable.incentives);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.tint_gray_color));
                        txtListItem.setTextColor(Color.LTGRAY);
                        break;
                    case 1:
                        listImages.setImageResource(R.drawable.goals);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.black));
                        break;
                    case 2:
                        listImages.setImageResource(R.drawable.profile);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.black));
                        break;
                    case 3:
                        listImages.setImageResource(R.drawable.email);
                        txtListItem.setTextColor(Color.LTGRAY);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.tint_gray_color));
                        break;
                    case 4: //Observation
                        listImages.setImageResource(R.drawable.ic_notification);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.tint_gray_color));
                        break;
                    case 5: //Observation Dashboard
                        listImages.setImageResource(R.drawable.ic_notification);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.tint_gray_color));
                        break;
                    case 6://Counter Coaching
                        listImages.setImageResource(R.drawable.ic_notification);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.tint_gray_color));
                        break;
                    case 7://Counter Coaching Dashboard
                        listImages.setImageResource(R.drawable.ic_notification);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.tint_gray_color));
                        break;
                    case 8:
                        listImages.setImageResource(R.drawable.ic_notification);
                        txtListItem.setTextColor(Color.LTGRAY);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.tint_gray_color));
                        break;
                    case 9:
                        listImages.setImageResource(R.drawable.preferences);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.black));
                        break;
                    case 10:
                        listImages.setImageResource(R.drawable.logout);
                        listImages.setColorFilter(getContext().getResources().getColor(R.color.black));
                        break;
                    default:
                        break;
                }

            }

        } else {
            TextView txtListItem = (TextView) listViewItem.findViewById(R.id.txt_list_item);
            ImageView listImages = (ImageView) listViewItem.findViewById(R.id.list_images);
            ImageView ivRightTick = (ImageView) listViewItem.findViewById(R.id.row_item_filter_iv);
            listImages.setVisibility(View.GONE);
            txtListItem.setText(values.get(position));
            ivRightTick.setVisibility(View.GONE);
            if (position == lastCheckedPosition) {
                ivRightTick.setVisibility(View.VISIBLE);
            } else {
                ivRightTick.setVisibility(View.GONE);
            }
            //Just to hide description field for tenant
        }
        /*listViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastCheckedPosition = position;
                notifyDataSetChanged();
            }
        });*/
        return listViewItem;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getItem(int position) {
        return values.get(position);
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List filterList = new ArrayList();
                for (int i = 0; i < mFilterList.size(); i++) {
                    if (mFilterList.get(i).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filterList.add(mFilterList.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mFilterList.size();
                results.values = mFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            values = (List) results.values;
            notifyDataSetChanged();
        }

    }

    @Override
    public int getCount() {
        return values.size();
    }

    private int getIndexByName(String selectedName) {
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i) != null && values.get(i).equals(selectedName)) {
                return i;
            }
        }
        return 0;// not there is list
    }


}
