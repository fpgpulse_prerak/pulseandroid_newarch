package com.ingauge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.listener.VideocategoryListener;
import com.ingauge.utils.Logger;

import java.util.List;

/**
 * Created by mansurum on 19-May-17.
 */

public class VideoCategoryListAdapter extends RecyclerView.Adapter<VideoCategoryListAdapter.FeedsAdapterViewHolder>{

    private Context mContext;
    private List<String> mcategoryList;
    private VideocategoryListener mVideocategoryListener;

    public VideoCategoryListAdapter(Context mContext, List<String> mcategoryList,VideocategoryListener mVideocategoryListener){

        this.mContext = mContext;
        this.mcategoryList = mcategoryList;
        this.mVideocategoryListener = mVideocategoryListener;

    }

    @Override
    public VideoCategoryListAdapter.FeedsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_video_category, parent, false);

        return new VideoCategoryListAdapter.FeedsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VideoCategoryListAdapter.FeedsAdapterViewHolder holder, int position){

        holder.tvName.setText(mcategoryList.get(position));
    }

    @Override
    public int getItemCount(){
        Logger.Error("<<< category Size >>>" + mcategoryList.size());
        return mcategoryList.size();
    }

    public class FeedsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tvName;

        public FeedsAdapterViewHolder(View view){
            super(view);
            tvName= (TextView) view.findViewById(R.id.row_vieo_category_name);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            mVideocategoryListener.getVideoCategoryName(mcategoryList.get(getLayoutPosition()));
        }
    }



}
