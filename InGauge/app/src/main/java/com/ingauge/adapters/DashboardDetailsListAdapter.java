package com.ingauge.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.ShareFragment;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mansurum on 19-May-17.
 */

public class DashboardDetailsListAdapter extends RecyclerView.Adapter<DashboardDetailsListAdapter.FeedsAdapterViewHolder> {
    private Context mContext;
    private int lastPosition = -1;
    private ArrayList<String> mDashboardID;
    private HomeActivity mHomeActivity;
    private JSONObject mobilekeyObject;

    public DashboardDetailsListAdapter(Context mContext, ArrayList<String> mDashboardID, HomeActivity mHomeActivity) {
        this.mDashboardID = mDashboardID;
        this.mContext = mContext;
        this.mHomeActivity = mHomeActivity;
        mobilekeyObject = UiUtils.getMobileKeys(mContext);
    }

    @Override
    public DashboardDetailsListAdapter.FeedsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_dashboard_details, parent, false);
        return new DashboardDetailsListAdapter.FeedsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DashboardDetailsListAdapter.FeedsAdapterViewHolder holder, final int position) {
        holder.webGraph.getSettings().setJavaScriptEnabled(true);
        holder.webGraph.getSettings().setBuiltInZoomControls(true);
        holder.webGraph.getSettings().setDisplayZoomControls(false);
        holder.webGraph.getSettings().setDomStorageEnabled(true);
        holder.webGraph.getSettings().setAllowContentAccess(true);
        holder.webGraph.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        holder.webGraph.setDrawingCacheEnabled(true);
        UiUtils.setWebCache(holder.webGraph);
        holder.webGraph.getSettings().setAppCachePath(InGaugeApp.getInstance().getCacheDir().getAbsolutePath());
        holder.webGraph.getSettings().setAllowFileAccess(true);
        holder.webGraph.getSettings().setAppCacheEnabled(true);
        holder.webGraph.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        //holder.webGraph.setInitialScale(1);
        holder.webGraph.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                holder.mProgressBar.setVisibility(View.VISIBLE);
                holder.webGraph.setVisibility(View.GONE);
                if (progress == 100) {
                    holder.mProgressBar.setVisibility(View.GONE);
                    holder.webGraph.setVisibility(View.VISIBLE);
                }
            }


        });
        holder.mShareView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareFragment dialog = new ShareFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(mHomeActivity.KEY_CUSTOM_REPORT_ID, Integer.parseInt(mDashboardID.get(position)));
                dialog.setArguments(bundle);
                dialog.show(mHomeActivity.getFragmentManager(), "dialog");
            }
        });

        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype), -2);
        int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region), -2);
        int TenantLocationId = (InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
        int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group), -2);
        int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product), -2));
        int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user), -2));
        int LoginUserId = InGaugeSession.read(mContext.getString(R.string.key_user_id), -2);
        String FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
        String ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
        String CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
        String CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
        String MatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_matric_data_type), mContext.getResources().getStringArray(R.array.metric_type_array)[0]);
        boolean IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), false);
        int IndustryId = InGaugeSession.read(mContext.getString(R.string.key_selected_industry_id), -2);

        String GraphURL = UiUtils.baseUrlChart + "htmlReport/" + mDashboardID.get(position) + "?tenantId=" + TenantId + "&regionTypeId=" + RegionTypeId + "&regionId=" + RegionId + "&tenantLocationId=" + TenantLocationId + "&locationGroupId=" + LocationGroupId + "&productId=" + ProductId + "&userId=" + UserId + "&from=" + FromDate + "&to=" + ToDate + "&isCompare=" + IsCompare + "&compFrom=" + CompFromDate + "&compTo=" + CompToDate + "&metricsDateType=" + MatrixDataType + "&" + "industryId=" + IndustryId + "&loginUser=" + LoginUserId;
        Logger.Error("LOADING GRAPH.....POSITION : " + position + "\n" + GraphURL);
        holder.tvShare.setText(mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_share)));
        holder.webGraph.loadUrl(GraphURL);
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    @Override
    public int getItemCount() {
        return mDashboardID.size();
    }

    public class FeedsAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvShare;
        public WebView webGraph;
        private ProgressBar mProgressBar;
        private LinearLayout mShareView;

        public FeedsAdapterViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.row_item_dashboard_details_tv_title);
            tvShare = (TextView) view.findViewById(R.id.tv_share);
            webGraph = (WebView) view.findViewById(R.id.web_graph);
            mProgressBar = (ProgressBar) view.findViewById(R.id.progress_row);
            mShareView = (LinearLayout) view.findViewById(R.id.row_item_feeds_ll_share);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


}
