package com.ingauge.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.FeedsCommentFragment;
import com.ingauge.fragments.FeedsFragment;
import com.ingauge.pojo.FeedsModelList;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.TimeAgo;
import com.ingauge.utils.UiUtils;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 19-May-17.
 */

public class FeedsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private Context context;
    FeedsFragment feedsFragment;
    private int lastPosition = -1;
    private int likedPosition = 0;
    private List<FeedsModelList.Datum> mDatumList;
    FeedsModelList feedsModelList;
    private TimeAgo mTimeAgo;
    private Activity mHomeActivity;
    private JSONObject mobilekeyObject;
    //private HomeActivity mHomeActivity;
    APIInterface apiInterface;
    Call mCall;
    boolean isAlreadyLike = false;
    FeedsFragment mFeedsFragment;
    long mLastClickTime = 0;

    protected boolean showLoader = true;
    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;

    public FeedsListAdapter(Context mContext, List<FeedsModelList.Datum> mDatumList, HomeActivity mHomeActivity, FeedsFragment mFeedsFragment){
        this.mHomeActivity = mHomeActivity;
        this.mContext = mContext;
        this.mDatumList = mDatumList;
        mobilekeyObject = UiUtils.getMobileKeys(mContext);
        mTimeAgo = new TimeAgo(this.mContext);
        this.mFeedsFragment = mFeedsFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        apiInterface = APIClient.getClient().create(APIInterface.class);
        feedsModelList = new FeedsModelList();
        feedsModelList.setData(mDatumList);
        if(viewType == VIEWTYPE_LOADER){
            // Your Loader XML view here
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.loader_item_layout, parent, false);
            // Your LoaderViewHolder class
            return new FeedsListAdapter.FeedsALoaderViewHolder(view);
        } else if(viewType == VIEWTYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_feeds, parent, false);
            return new FeedsListAdapter.FeedsAdapterViewHolder(itemView);
        }
        throw new IllegalArgumentException("Invalid ViewType: " + viewType);

    }


    @Override
    public int getItemViewType(int position){
        // loader can't be at position 0
        // loader can only be at the last position
      /*  if (position != 0 && position == getItemCount() - 1) {
            return VIEWTYPE_LOADER;
        }

        return VIEWTYPE_ITEM;*/

        return (showLoader && position >= getItemCount()) ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position){
        if(viewHolder instanceof FeedsALoaderViewHolder){
            FeedsALoaderViewHolder holder = (FeedsALoaderViewHolder) viewHolder;
            if(showLoader){
                holder.mProgressBar.setVisibility(View.VISIBLE);
            } else{
                holder.mProgressBar.setVisibility(View.GONE);
            }
        } else if(viewHolder instanceof FeedsAdapterViewHolder){
            final FeedsAdapterViewHolder holder = (FeedsAdapterViewHolder) viewHolder;
            WebSettings wv_settings = holder.webGraph.getSettings();
            holder.mProgressBar.setVisibility(View.GONE);
            holder.webGraph.setVisibility(View.VISIBLE);
            //this is where you fixed your code I guess
            //And also by setting a WebClient to catch javascript's console messages :

            holder.webGraph.setWebChromeClient(new WebChromeClient(){
                public boolean onConsoleMessage(ConsoleMessage cm){
                /*Logger.Error("console Message" + cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId());*/
                    return true;
                }
            });
            wv_settings.setDomStorageEnabled(true);

            holder.webGraph.setWebViewClient(new WebViewClient(){

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request){
                    return false;
                }
            });

            //wv.setWebViewClient(new HelpClient(this));//
            holder.webGraph.clearCache(true);
            holder.webGraph.clearHistory();
            wv_settings.setJavaScriptEnabled(true);//XSS vulnerable
            wv_settings.setJavaScriptCanOpenWindowsAutomatically(true);
            //holder.webGraph.loadUrl("file:///android_asset/connect.php.html");
            holder.webGraph.loadUrl(UiUtils.baseUrlChart + "feeds/" + mDatumList.get(position).getId());

            holder.webGraph.setWebChromeClient(new WebChromeClient(){
                @Override
                public void onProgressChanged(WebView view, int progress){
                    holder.mProgressBar.setVisibility(View.VISIBLE);
                    holder.webGraph.setVisibility(View.GONE);
                    //Logger.Error("Web Progress " + progress);
                    if(progress == 100){
                        holder.mProgressBar.setVisibility(View.GONE);
                        holder.webGraph.setVisibility(View.VISIBLE);
                    }
                }


            });

            //holder.webGraph.loadUrl(APIClient.baseUrlChart + "feeds/" + mDatumList.get(position).getId());
            Logger.Error("Web URL " + UiUtils.baseUrlChart + "feeds/" + mDatumList.get(position).getId());
            String imageUrl = UiUtils.baseUrl + "api/user/avatar/1/" + mDatumList.get(position).getCreatedBy().getId();
            Glide.with(mContext)
                    .load(imageUrl)
                    .centerCrop()
                    .error(R.drawable.placeholder)
                    .into(holder.img_profile);
            holder.tvDesc.setText(mDatumList.get(position).getDescription());
            holder.tvTitle.setText(mDatumList.get(position).getCreatedBy().getName());
            holder.tvRole.setText(mDatumList.get(position).getCreatedBy().getJobTitle());
            //holder.tvLike.setText(mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_like)));
            //holder.tvComment.setText(mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_comment)));
            String startDateString = mDatumList.get(position).getCreatedOn();
            Logger.Error("  Date Created ON  " + startDateString);
            try{
//            2017-12-14 05:44:38

                String dateStr = startDateString;
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
                df.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = df.parse(dateStr);
                df.setTimeZone(TimeZone.getDefault());
                String formattedDate = df.format(date);

                SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                _24HourSDF.setTimeZone(TimeZone.getDefault());

                SimpleDateFormat _12HourSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
                _12HourSDF.setTimeZone(TimeZone.getDefault());
                Date _24HourDt = _24HourSDF.parse(formattedDate);

                _24HourSDF.setTimeZone(TimeZone.getDefault());
                _12HourSDF.setTimeZone(TimeZone.getDefault());
                Logger.Error("<<<<<<  Twenty Four " + _24HourDt);
                Logger.Error("<<<<<<  Twelve  " + _12HourSDF.format(_24HourDt));
                System.out.println(_24HourDt);
                System.out.println(_12HourSDF.format(_24HourDt));


                long now = System.currentTimeMillis();
                Date mDate1 = new Date();

                mDate1.getTime();
                String datetime1 = _12HourSDF.format(_24HourDt);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
                Date convertedDate = dateFormat.parse(datetime1);


                Calendar c = Calendar.getInstance();
                Date datetoday = c.getTime(); //current date and time in UTC
                SimpleDateFormat todaySDf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
                todaySDf.setTimeZone(TimeZone.getDefault()); //format in given timezone
                String strDate = todaySDf.format(datetoday);
                try{
                    Calendar todayFinalCalendar = UiUtils.DateToCalendar(todaySDf.parse(strDate));

                    CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                            convertedDate.getTime(),
                            todayFinalCalendar.getTimeInMillis(),
                            DateUtils.FORMAT_ABBREV_ALL);


                    if(getTimeAgo(todayFinalCalendar.getTime(), convertedDate) != null)
                        holder.tvTime.setText(getTimeAgo(todayFinalCalendar.getTime(), convertedDate));
                    else{

                        holder.tvTime.setText(String.valueOf(0) + 'h');
                    }


                } catch(ParseException e){
                    e.printStackTrace();
                }


            } catch(final ParseException e){
                e.printStackTrace();
            }
            try{
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                //df.setTimeZone(TimeZone.getDefault());
                //df.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = df.parse(startDateString);


                Date dateObj = UiUtils.ConvertToDeviceLocalDateObj(date.getTime(), "dd-MMM-yyyy hh:mm:ss aa");
                //String aTimeAgoString = UiUtils.calculateFeedTime(dateObj.getTime());
                String aTimeAgoString = UiUtils.getlongtoago(dateObj.getTime());
                Logger.Error("<<<< aTimeAgoString" + aTimeAgoString);
                String timeSpen = DateUtils.getRelativeTimeSpanString(new Date().getTime(), dateObj.getTime(), DateUtils.FORMAT_ABBREV_ALL).toString();
                Logger.Error("<<<< Time Span  " + timeSpen);

                //holder.tvTime.setText(aTimeAgoString);
            } catch(ParseException e){
                e.printStackTrace();
                holder.tvTime.setText("-");
            }
            if(mDatumList.get(position).isLiked){
                holder.tvLike.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
                holder.tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
                holder.imgLike.setBackgroundResource(R.drawable.ic_liked);
            } else{
                holder.tvLike.setTextColor(mContext.getResources().getColor(R.color.darker_grey));
                holder.tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.darker_grey));
                holder.imgLike.setBackgroundResource(R.drawable.ic_like);
            }
            holder.tvLikeCount.setText(String.valueOf(mDatumList.get(position).noOfLikes));
            holder.tvCommentCount.setText(String.valueOf(mDatumList.get(position).noOfComments));
            holder.tvLike.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    if(SystemClock.elapsedRealtime() - mLastClickTime < 2500){
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    if(mDatumList.get(position).isLiked){
                        isAlreadyLike = true;
                    } else{
                        isAlreadyLike = false;
                    }
                    likedPosition = position;
                    InGaugeSession.write(mContext.getString(R.string.key_selected_feed_id), mDatumList.get(position).getId());
                    saveLikeFeed(holder, InGaugeSession.read(mContext.getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getString(R.string.key_selected_feed_id), 0), InGaugeSession.read(mContext.getString(R.string.key_user_id), 0));

                }
            });
            holder.rowItemFeedsComment.setOnClickListener(new View.OnClickListener(){
                                                              @Override
                                                              public void onClick(View view){
                                                                  InGaugeSession.write(mContext.getString(R.string.key_selected_feed_id), mDatumList.get(position).getId());
                                                                  feedsModelList = new FeedsModelList();
                                                                  feedsModelList.setData(mDatumList);
                                                                  FeedsCommentFragment fragment = new FeedsCommentFragment();
                                                                  Bundle mbundle = new Bundle();
                                                                  mbundle.putSerializable(FeedsFragment.KEY_FEED_MODEL_LIST, feedsModelList);
                                                                  mbundle.putInt(FeedsFragment.KEY_SELECTED_POSITION, position);

                                                                  fragment.setArguments(mbundle);
                                                                  fragment.show(mFeedsFragment.getChildFragmentManager(), mContext.getResources().getString(R.string.tag_comment_dialog));

               /*HomeActivity homeActivity=(HomeActivity)mContext;
               homeActivity.add(fragment,"");*/
                                                              }
                                                          }
            );

            // Here you apply the animation when the view is bound
            //  setAnimation(holder.itemView, position);
        }
    }

   /* @Override
    public void onBindViewHolder(final FeedsListAdapter.FeedsAdapterViewHolder holder, final int position){
        //holder.webGraph.loadUrl("http://192.168.2.124:3000/feeds/34.html");
        //  holder.webGraph.loadUrl("file:///android_asset/result.html");
        //wv.loadUrl("file:///android_asset/aboutcertified.html");

        WebSettings wv_settings = holder.webGraph.getSettings();
        holder.mProgressBar.setVisibility(View.GONE);
        holder.webGraph.setVisibility(View.VISIBLE);
        //this is where you fixed your code I guess
        //And also by setting a WebClient to catch javascript's console messages :

        holder.webGraph.setWebChromeClient(new WebChromeClient(){
            public boolean onConsoleMessage(ConsoleMessage cm){
                *//*Logger.Error("console Message" + cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId());*//*
                return true;
            }
        });
        wv_settings.setDomStorageEnabled(true);

        holder.webGraph.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request){
                return false;
            }
        });

        //wv.setWebViewClient(new HelpClient(this));//
        holder.webGraph.clearCache(true);
        holder.webGraph.clearHistory();
        wv_settings.setJavaScriptEnabled(true);//XSS vulnerable
        wv_settings.setJavaScriptCanOpenWindowsAutomatically(true);
        //holder.webGraph.loadUrl("file:///android_asset/connect.php.html");
        holder.webGraph.loadUrl(UiUtils.baseUrlChart + "feeds/" + mDatumList.get(position).getId());

        holder.webGraph.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int progress){
                holder.mProgressBar.setVisibility(View.VISIBLE);
                holder.webGraph.setVisibility(View.GONE);
                //Logger.Error("Web Progress " + progress);
                if(progress == 100){
                    holder.mProgressBar.setVisibility(View.GONE);
                    holder.webGraph.setVisibility(View.VISIBLE);
                }
            }


        });

        //holder.webGraph.loadUrl(APIClient.baseUrlChart + "feeds/" + mDatumList.get(position).getId());
        Logger.Error("Web URL " + UiUtils.baseUrlChart + "feeds/" + mDatumList.get(position).getId());
        String imageUrl = UiUtils.baseUrl + "api/user/avatar/1/" + mDatumList.get(position).getCreatedBy().getId();
        Glide.with(mContext)
                .load(imageUrl)
                .centerCrop()
                .error(R.drawable.placeholder)
                .into(holder.img_profile);
        holder.tvDesc.setText(mDatumList.get(position).getDescription());
        holder.tvTitle.setText(mDatumList.get(position).getCreatedBy().getName());
        holder.tvRole.setText(mDatumList.get(position).getCreatedBy().getJobTitle());
        //holder.tvLike.setText(mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_like)));
        //holder.tvComment.setText(mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_comment)));
        String startDateString = mDatumList.get(position).getCreatedOn();
        Logger.Error("  Date Created ON  " + startDateString);
        try{
//            2017-12-14 05:44:38

            String dateStr = startDateString;
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(dateStr);
            df.setTimeZone(TimeZone.getDefault());
            String formattedDate = df.format(date);

            SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            _24HourSDF.setTimeZone(TimeZone.getDefault());

            SimpleDateFormat _12HourSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            _12HourSDF.setTimeZone(TimeZone.getDefault());
            Date _24HourDt = _24HourSDF.parse(formattedDate);

            _24HourSDF.setTimeZone(TimeZone.getDefault());
            _12HourSDF.setTimeZone(TimeZone.getDefault());
            Logger.Error("<<<<<<  Twenty Four " + _24HourDt);
            Logger.Error("<<<<<<  Twelve  " + _12HourSDF.format(_24HourDt));
            System.out.println(_24HourDt);
            System.out.println(_12HourSDF.format(_24HourDt));


            long now = System.currentTimeMillis();
            Date mDate1 = new Date();

            mDate1.getTime();
            String datetime1 = _12HourSDF.format(_24HourDt);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
            Date convertedDate = dateFormat.parse(datetime1);


            Calendar c = Calendar.getInstance();
            Date datetoday = c.getTime(); //current date and time in UTC
            SimpleDateFormat todaySDf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            todaySDf.setTimeZone(TimeZone.getDefault()); //format in given timezone
            String strDate = todaySDf.format(datetoday);
            try{
                Calendar todayFinalCalendar = UiUtils.DateToCalendar(todaySDf.parse(strDate));

                CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                        convertedDate.getTime(),
                        todayFinalCalendar.getTimeInMillis(),
                        DateUtils.FORMAT_ABBREV_ALL);


                if(getTimeAgo(todayFinalCalendar.getTime(), convertedDate) != null)
                    holder.tvTime.setText(getTimeAgo(todayFinalCalendar.getTime(), convertedDate));
                else{

                    holder.tvTime.setText(String.valueOf(0) + 'h');
                }


            } catch(ParseException e){
                e.printStackTrace();
            }


        } catch(final ParseException e){
            e.printStackTrace();
        }
        try{
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //df.setTimeZone(TimeZone.getDefault());
            //df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(startDateString);


            Date dateObj = UiUtils.ConvertToDeviceLocalDateObj(date.getTime(), "dd-MMM-yyyy hh:mm:ss aa");
            //String aTimeAgoString = UiUtils.calculateFeedTime(dateObj.getTime());
            String aTimeAgoString = UiUtils.getlongtoago(dateObj.getTime());
            Logger.Error("<<<< aTimeAgoString" + aTimeAgoString);
            String timeSpen = DateUtils.getRelativeTimeSpanString(new Date().getTime(), dateObj.getTime(), DateUtils.FORMAT_ABBREV_ALL).toString();
            Logger.Error("<<<< Time Span  " + timeSpen);

            //holder.tvTime.setText(aTimeAgoString);
        } catch(ParseException e){
            e.printStackTrace();
            holder.tvTime.setText("-");
        }
        if(mDatumList.get(position).isLiked){
            holder.tvLike.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
            holder.tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
            holder.imgLike.setBackgroundResource(R.drawable.ic_liked);
        } else{
            holder.tvLike.setTextColor(mContext.getResources().getColor(R.color.darker_grey));
            holder.tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.darker_grey));
            holder.imgLike.setBackgroundResource(R.drawable.ic_like);
        }
        holder.tvLikeCount.setText(String.valueOf(mDatumList.get(position).noOfLikes));
        holder.tvCommentCount.setText(String.valueOf(mDatumList.get(position).noOfComments));
        holder.tvLike.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(SystemClock.elapsedRealtime() - mLastClickTime < 2500){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if(mDatumList.get(position).isLiked){
                    isAlreadyLike = true;
                } else{
                    isAlreadyLike = false;
                }
                likedPosition = position;
                InGaugeSession.write(mContext.getString(R.string.key_selected_feed_id), mDatumList.get(position).getId());
                saveLikeFeed(holder, InGaugeSession.read(mContext.getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getString(R.string.key_selected_feed_id), 0), InGaugeSession.read(mContext.getString(R.string.key_user_id), 0));

            }
        });
        holder.rowItemFeedsComment.setOnClickListener(new View.OnClickListener(){
                                                          @Override
                                                          public void onClick(View view){
                                                              InGaugeSession.write(mContext.getString(R.string.key_selected_feed_id), mDatumList.get(position).getId());
                                                              feedsModelList = new FeedsModelList();
                                                              feedsModelList.setData(mDatumList);
                                                              FeedsCommentFragment fragment = new FeedsCommentFragment();
                                                              Bundle mbundle = new Bundle();
                                                              mbundle.putSerializable(FeedsFragment.KEY_FEED_MODEL_LIST, feedsModelList);
                                                              mbundle.putInt(FeedsFragment.KEY_SELECTED_POSITION, position);

                                                              fragment.setArguments(mbundle);
                                                              fragment.show(mFeedsFragment.getChildFragmentManager(), mContext.getResources().getString(R.string.tag_comment_dialog));

               *//*HomeActivity homeActivity=(HomeActivity)mContext;
               homeActivity.add(fragment,"");*//*
                                                          }
                                                      }
        );

        // Here you apply the animation when the view is bound
        //  setAnimation(holder.itemView, position);
    }*/


    public String getTimeAgo(Date current, Date createdDate){
        // Get msec from each, and subtract.
        long diff = current.getTime() - createdDate.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (1000 * 3600 * 24);

        Logger.Error("<<<< Day Difference >>>>" + diffDays);
        String time = null;

        if(diffDays > 0){
            if(diffDays == 1){
                time = diffDays + "d";
            } else{
                time = diffDays + "d";
            }
        } else{
            if(diffHours > 0){
                if(diffHours == 1){
                    time = diffHours + "h";
                } else{
                    time = diffHours + "h";
                }
            } else{
                if(diffMinutes > 0){
                    if(diffMinutes == 1){
                        //time = diffMinutes + "min ago";
                        time = 0 + "h";
                    } else{
                        //time = diffMinutes + "mins ago";
                        time = 0 + "h";
                    }
                } else{
                    if(diffSeconds > 0){
                        //time = diffSeconds + "secs ago";
                        time = 0 + "h";
                    }
                }

            }

        }
       /* if(time.length()>0){
            time  = 0 + "h";
        }*/
        return time;
    }

    public void saveLikeFeed(final FeedsAdapterViewHolder holder, String authToken, final int feedId, int userId){
        mCall = apiInterface.saveLikeFeed(feedId, userId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    int count = Integer.parseInt(holder.tvLikeCount.getText().toString());
                    if(isAlreadyLike){
                        holder.tvLikeCount.setText(String.valueOf(count - 1));
                        holder.tvLike.setTextColor(mContext.getResources().getColor(R.color.darker_grey));
                        holder.tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.darker_grey));
                        holder.imgLike.setBackgroundResource(R.drawable.ic_like);
                        mDatumList.get(likedPosition).isLiked = false;
                        mDatumList.get(likedPosition).noOfLikes = count - 1;
                    } else{
                        holder.tvLikeCount.setText(String.valueOf(count + 1));
                        holder.tvLike.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
                        holder.tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
                        holder.imgLike.setBackgroundResource(R.drawable.ic_liked);
                        mDatumList.get(likedPosition).isLiked = true;
                        mDatumList.get(likedPosition).noOfLikes = count + 1;
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("industry", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
                    bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
                    bundle.putInt("feed_id", feedId);
                    bundle.putBoolean("is_like", !isAlreadyLike);
                    InGaugeApp.getFirebaseAnalytics().logEvent("like_feed", bundle);

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        //mHomeactivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){

            }
        });
    }

    @Override
    public int getItemCount(){
        return mDatumList.size();
    }

    public class FeedsALoaderViewHolder extends RecyclerView.ViewHolder{
        ProgressBar mProgressBar;

        public FeedsALoaderViewHolder(View itemview){
            super(itemview);
            mProgressBar = (ProgressBar) itemview.findViewById(R.id.progressbar);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    public class FeedsAdapterViewHolder extends RecyclerView.ViewHolder{
        private TextView tvTitle;
        private TextView tvLike;
        private TextView tvComment;
        private TextView tvDesc;
        private TextView tvRole;
        private TextView tvTime;
        private TextView tvLikeCount;
        private TextView tvCommentCount;
        private CircleImageView img_profile;
        private WebView webGraph;
        private ProgressBar mProgressBar;
        private LinearLayout rowItemFeedsComment;
        private ImageView imgLike;

        public FeedsAdapterViewHolder(View view){
            super(view);
            img_profile = (CircleImageView) view.findViewById(R.id.img_profile);
            webGraph = (WebView) view.findViewById(R.id.web_graph);
            mProgressBar = (ProgressBar) view.findViewById(R.id.progress_row);
            tvDesc = (TextView) view.findViewById(R.id.row_item_feeds_desc);
            tvTitle = (TextView) view.findViewById(R.id.row_item_feeds_tv_title);
            tvRole = (TextView) view.findViewById(R.id.row_item_feeds_tv_role);
            tvTime = (TextView) view.findViewById(R.id.row_item_feeds_tv_time);
            tvLike = (TextView) view.findViewById(R.id.tv_like);
            tvLikeCount = (TextView) view.findViewById(R.id.tv_like_count);
            tvCommentCount = (TextView) view.findViewById(R.id.tv_comment_count);
            rowItemFeedsComment = (LinearLayout) view.findViewById(R.id.row_item_feeds_ll_comment);
            tvComment = (TextView) view.findViewById(R.id.tv_comment);
            imgLike = (ImageView) view.findViewById(R.id.img_like);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position){
        // If the bound view wasn't previously displayed on screen, it's animated
        if(position > lastPosition){
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void showLoading(boolean status){
        showLoader = status;
    }
}
