package com.ingauge.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.ProgressActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.LeaderDetailsListClickListener;
import com.ingauge.pojo.SocialInteractionDetailReportData;
import com.ingauge.pojo.TablePojo;
import com.ingauge.pojo.TableReportExpandItemPojo;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TableReportAdapterForLeader extends BaseExpandableListAdapter {
    public List<TablePojo> productList;
    HomeActivity mHomeActivity;
    boolean isFirstFilled;
    boolean isSecondFilled;
    boolean isThirdFilled;
    Map<Integer, List<TableReportExpandItemPojo>> map;
    TableReportExpandItemPojo childObject = new TableReportExpandItemPojo();

    APIInterface apiInterfaceForShareReport, mApiInterface;
    Call<ResponseBody> mCall;
    JsonArray parentJsonArray = new JsonArray();
    boolean isAPICallDone = true;


    JsonObject mJsonObject = new JsonObject();
    JsonArray reportJsonArrays = new JsonArray();

    List<SocialInteractionDetailReportData> mSocialInteractionDetailReportData = new ArrayList<>();
    LeaderDetailsListClickListener mLeaderDetailsListClickListener;
    //boolean isFromColumnSelection = false;


    public TableReportAdapterForLeader(HomeActivity mHomeActivity, List<TablePojo> productList, boolean isFirstColumn, boolean isCSecondColumn, boolean isThirdColumn, Map<Integer, List<TableReportExpandItemPojo>> map, boolean isFromColumnSelection,LeaderDetailsListClickListener mLeaderDetailsListClickListener){
        super();
        this.mHomeActivity = mHomeActivity;
        this.productList = productList;
        this.isFirstFilled = isFirstColumn;
        isSecondFilled = isCSecondColumn;
        isThirdFilled = isThirdColumn;
        this.map = map;
        this.mLeaderDetailsListClickListener = mLeaderDetailsListClickListener;

        apiInterfaceForShareReport = APIClient.getClientChartWithoutHeaders().create(APIInterface.class);
        mApiInterface = APIClient.getClient().create(APIInterface.class);
        //  this.isFromColumnSelection = isFromColumnSelection;
    }

    @Override
    public int getGroupCount(){
        Logger.Error("<<<<<<<  Group  Count " + map.size());
        return map.size();
    }

    @Override
    public int getChildrenCount(int groupPosition){
        Logger.Error("<<<<<<<  Children Count " + map.get(groupPosition).size());
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition){
        return map;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition){
        return map.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }

    @Override
    public boolean hasStableIds(){
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        final ViewHolder holder;
        LayoutInflater inflater = mHomeActivity.getLayoutInflater();


        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_table_report_for_leaderboard, null);
            holder = new ViewHolder();
            holder.rowone = (TextView) convertView.findViewById(R.id.row_table_report_column_value_one);
            holder.rowTwo = (TextView) convertView.findViewById(R.id.row_table_report_column_value_two);
            holder.rowThree = (TextView) convertView.findViewById(R.id.row_table_report_column_value_three);
            holder.ivCrown = (ImageView) convertView.findViewById(R.id.row_table_report_column_value_one_iv);
            holder.ivProfile = (ImageView) convertView.findViewById(R.id.row_table_report_iv_profile);


            convertView.setTag(holder);

        } else{
            holder = (ViewHolder) convertView.getTag();
        }

        if(isExpanded){

            isAPICallDone = true;


            int LoginUserId = InGaugeSession.read(mHomeActivity.getString(R.string.key_user_id), -2);
            mJsonObject = new JsonObject();
            reportJsonArrays = new JsonArray();

            for(int i = 0; i < map.get(groupPosition).size(); i++){
                childObject = map.get(groupPosition).get(i);
                mJsonObject.addProperty("entityId", childObject.entityId);
                mJsonObject.addProperty("entityType", childObject.entityType);
                mJsonObject.addProperty("loggedInUser", String.valueOf(LoginUserId));
                JsonObject reportInnerJson = new JsonObject();
                reportInnerJson.addProperty("regId", i);

                JsonObject mJsonObjectValues = new JsonObject();
                if(childObject.startDateCheck.length() > 0){
                    mJsonObjectValues.addProperty("startDate", childObject.startDateCheck.trim());
                } else{
                    mJsonObjectValues.addProperty("startDate", childObject.dataStartDate.trim());
                }
                if(childObject.endDateCheck.length() > 0){
                    mJsonObjectValues.addProperty("endDate", childObject.endDateCheck.trim());
                } else{
                    mJsonObjectValues.addProperty("endDate", childObject.dataEndDate.trim());
                }

                mJsonObjectValues.addProperty("value", childObject.reportValue.trim());
                mJsonObjectValues.addProperty("reportId", childObject.reportIdForServer.trim());
                mJsonObjectValues.addProperty("startCompDate", childObject.compDataStartDate.trim());
                mJsonObjectValues.addProperty("endCompDate", childObject.compDataEndDate.trim());
                mJsonObjectValues.addProperty("reportName", childObject.reportNameForServer.trim());
                reportInnerJson.add("values", mJsonObjectValues);
                reportJsonArrays.add(reportInnerJson);
                mJsonObject.add("reportValues", reportJsonArrays);
            }

        }

        if(groupPosition == 0){
            holder.rowone.setVisibility(View.GONE);
            holder.ivCrown.setVisibility(View.VISIBLE);
        } else{
            holder.rowone.setVisibility(View.VISIBLE);
            holder.ivCrown.setVisibility(View.GONE);
        }

        TablePojo item = productList.get(groupPosition);
        holder.rowone.setText(item.getColumnOne());
        holder.rowTwo.setText(item.getColumnTwo());
        holder.rowThree.setText(item.getColumnThree());
        if(isFirstFilled){
            holder.rowone.setVisibility(View.VISIBLE);
        } else{
            holder.rowone.setVisibility(View.GONE);
        }
        if(isSecondFilled){
            holder.rowTwo.setVisibility(View.VISIBLE);

        } else{
            holder.rowTwo.setVisibility(View.GONE);
        }

        if(isThirdFilled){

            holder.rowThree.setVisibility(View.VISIBLE);
        } else{
            holder.rowThree.setVisibility(View.GONE);
        }
        String entityid = "";
        if(map.get(groupPosition) != null && map.get(groupPosition).size() > 0){
            entityid = map.get(groupPosition).get(0).entityId;
        }
        String imageUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(mHomeActivity) + "/" + entityid;
        Logger.Error("<<< Image URL:  " + imageUrl);
        /*Glide.with(mHomeActivity)
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .placeholder(R.drawable.defaultimg)
                .error(R.drawable.defaultimg)
                .into(holder.ivProfile);*/
        Glide.with(mHomeActivity).load(imageUrl).asBitmap().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).centerCrop().into(new BitmapImageViewTarget(holder.ivProfile){
            @Override
            protected void setResource(Bitmap resource){
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(mHomeActivity.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.ivProfile.setImageDrawable(circularBitmapDrawable);
            }
        });

        return convertView;
    }


    @Override
    public void registerDataSetObserver(DataSetObserver observer){
        super.registerDataSetObserver(observer);
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent){


        if(convertView == null){
            LayoutInflater infalInflater = (LayoutInflater) mHomeActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_table_report_expand, null);
        }


        Logger.Error("<<<< <<<<< Group Position   >>>>>>> " + groupPosition + "Child Position >>> " + childPosition);


        childObject = (TableReportExpandItemPojo) getChild(groupPosition, childPosition);

        //}
        LinearLayout linearLayoutSocialInteraction = (LinearLayout) convertView.findViewById(R.id.row_table_report_expand_ll_social_interaction);
        TextView tvReportName = (TextView) convertView.findViewById(R.id.row_table_report_expand_report_name);
        TextView tvReportValue = (TextView) convertView.findViewById(R.id.row_table_report_expand_report_name_value);
        TextView tvLike = (TextView) convertView.findViewById(R.id.row_table_expand_tv_like);
        TextView tvComment = (TextView) convertView.findViewById(R.id.row_table_expand_tv_comment);
        tvReportName.setText(childObject.reportNameDisplay);
        tvReportValue.setText(childObject.reportValue);
        tvLike.setText("" + childObject.likeCount);
        tvComment.setText("" + childObject.commentCount);
        if(childObject.isLiked){
            tvLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.drawable_liked, 0, 0, 0);
        } else{
            tvLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.drawable_like, 0, 0, 0);
        }

        if(childObject.reportValue.length() > 0){
            if(childObject.isSocialInteractionEnabled){
                linearLayoutSocialInteraction.setVisibility(View.VISIBLE);
            } else{
                linearLayoutSocialInteraction.setVisibility(View.GONE);
            }
        } else{
            linearLayoutSocialInteraction.setVisibility(View.GONE);
        }

        if(isAPICallDone){
            if(productList.get(groupPosition).isSocialStatusCall()){
                if(!ProgressActivity.active){
                    mHomeActivity.startProgress(mHomeActivity);
                    Logger.Error("<<<<<<<<< CALLLLLLLLL >>>>>>>>>");
                    getSocialStatus(mJsonObject, groupPosition);
                }

            }
        }
/*
        if (!isFromColumnSelection) {

        } else {
           */
/* if (isAPICallDone) {
                if (productList.get(groupPosition).isSocialStatusCall()) {
                    mHomeActivity.startProgress(mHomeActivity);
                    Logger.Error("<<<<<<<<< CALLLLLLLLL >>>>>>>>>");
                    getSocialStatus(mJsonObject, groupPosition);
                }
            }*//*

        }
*/


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition){
        return true;
    }



    /* @Override
     public int getCount() {
         return productList.size();
     }

     @Override
     public Object getItem(int position) {
         return productList.get(position);
     }

     @Override
     public long getItemId(int position) {
         return position;
     }
     @Override
     public View getView(int position, View convertView, ViewGroup parent) {

         ViewHolder holder;
         LayoutInflater inflater = mHomeActivity.getLayoutInflater();

         if (convertView == null) {
             convertView = inflater.inflate(R.layout.row_table_report, null);
             holder = new ViewHolder();
             holder.rowone = (TextView) convertView.findViewById(R.id.row_table_report_column_value_one);
             holder.rowTwo = (TextView) convertView.findViewById(R.id.row_table_report_column_value_two);
             holder.rowThree = (TextView) convertView.findViewById(R.id.row_table_report_column_value_three);

             convertView.setTag(holder);
         } else {
             holder = (ViewHolder) convertView.getTag();
         }

         TablePojo item = productList.get(position);
         holder.rowone.setText(item.getColumnOne().toString());
         holder.rowTwo.setText(item.getColumnTwo().toString());
         holder.rowThree.setText(item.getColumnThree().toString());
         if (isFirstFilled) {

             holder.rowone.setVisibility(View.VISIBLE);
         } else {
             holder.rowone.setVisibility(View.GONE);
         }
         if (isSecondFilled) {
             holder.rowTwo.setVisibility(View.VISIBLE);

         } else {
             holder.rowTwo.setVisibility(View.GONE);
         }

         if (isThirdFilled) {

             holder.rowThree.setVisibility(View.VISIBLE);
         } else {
             holder.rowThree.setVisibility(View.GONE);
         }


         return convertView;
     }*/
    private class ViewHolder{
        TextView rowone;
        TextView rowTwo;
        TextView rowThree;
        ImageView ivCrown;
        ImageView ivProfile;

    }

    public void getSocialStatus(JsonObject requestJsonObject, final int groupposition){
        //getSocialStatus();
        isAPICallDone = false;

        Logger.Error("<<<< Json Request >>>> " + requestJsonObject.toString());
        Call<ResponseBody> mCall = mApiInterface.getSocialStatus(requestJsonObject);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response){
                mHomeActivity.endProgress();
                JSONObject mDataJson = null;
                mSocialInteractionDetailReportData = new ArrayList<>();
                try{
                    String strResponse = "";
                    if(response.body() != null){
                        mHomeActivity.endProgress();
                        //   Logger.Error("<<<< Json Response >>>> " + response.body().string());
                        strResponse = response.body().string();
                        Logger.Error("<<<<< Response >>>>>> 1111" + response.body().string());
                        // if(response.body().string()!=null){
                        Logger.Error("<<<<< Response >>>>>> 2222" + strResponse);
                        // strResponse = response.body().string();
                        Logger.Error("<<<<< Response >>>>>> 3333" + strResponse);
                        JSONObject mJsonObject = new JSONObject(strResponse);
                        Logger.Error("<<<<< Response >>>>>> 4444" + strResponse);
                        Logger.Error("<<<< Json Response >>>> " + mJsonObject.toString());
                        String dataJson = mJsonObject.optString("data");
                        JSONArray mJsonArrayData = new JSONArray(dataJson);
                        for(int i = 0; i < mJsonArrayData.length(); i++){
                            Logger.Error("<<<< Json Data Response >>>> " + mJsonArrayData.toString());
                            JSONObject mJsonObjectData = (JSONObject) mJsonArrayData.get(i);
                            SocialInteractionDetailReportData mSocialObj = new SocialInteractionDetailReportData();
                            mSocialObj.regId = mJsonObjectData.optString("regId");
                            JSONObject mLikeCommentsObj = mJsonObjectData.optJSONObject("likeCommentValues");
                            mSocialObj.mLikeCommentsValues.totalLikes = mLikeCommentsObj.optString("totallikes");
                            mSocialObj.mLikeCommentsValues.totalComments = mLikeCommentsObj.optString("totalcomments");
                            mSocialObj.mLikeCommentsValues.isLikeUsers = mLikeCommentsObj.optBoolean("likedUser");
                            mSocialObj.mLikeCommentsValues.isCommentuser = mLikeCommentsObj.optBoolean("commentUser");
                            mSocialInteractionDetailReportData.add(mSocialObj);
                            Logger.Error("<<<< Json Response >>>> " + mJsonObjectData.toString());
                        }
                        if(mSocialInteractionDetailReportData != null && mSocialInteractionDetailReportData.size() > 0){
                            mHomeActivity.endProgress();
                            updateChildObject(mSocialInteractionDetailReportData, groupposition);

                        }
                        Logger.Error("<<<<< Array List Object " + mSocialInteractionDetailReportData.size());
                        //  }


                        //JSONArray mJsonObject = new JSONArray(response.body().string());
                        //Logger.Error("<<<< JSON Object >>>> " + mJsonObject.toString());
                        //mDataJson = mJsonObject.optJSONObject("data");

                    }
                } catch(IOException e){
                    mHomeActivity.endProgress();
                    e.printStackTrace();
                } catch(Exception e){
                    mHomeActivity.endProgress();
                    e.printStackTrace();
                }

                mHomeActivity.endProgress();
                TablePojo mTablePojo = productList.get(groupposition);
                mTablePojo.setSocialStatusCall(false);
                productList.set(groupposition, mTablePojo);
                notifyDataSetChanged();

                //Logger.Error("<<< Response >>>>"  + response.toString());
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
                Logger.Error("<<<< Exception >>>> " + t.getMessage());
            }
        });
    }

    public void updateChildObject(List<SocialInteractionDetailReportData> mSocialInteractionDetailReportData, int groupposition){
        mHomeActivity.endProgress();
        List<TableReportExpandItemPojo> mTableReportExpandItemPojos = map.get(groupposition);
        /*for(int i=0;i<mSocialInteractionDetailReportData.size();i++){

        }*/
        Logger.Error("<<<<<<<<<<<<<<<<<<  GROUP >>>>>>>>> " + groupposition);
        for(int i = 0; i < mSocialInteractionDetailReportData.size(); i++){
            //mSocialInteractionDetailReportData.get(i).regId
            int child = Integer.parseInt(mSocialInteractionDetailReportData.get(i).regId);

            TableReportExpandItemPojo mTableReportExpandItemPojo = map.get(groupposition).get(child);
            if(mSocialInteractionDetailReportData.get(i).mLikeCommentsValues.totalLikes.length() > 0){
                mTableReportExpandItemPojo.likeCount = Integer.parseInt(mSocialInteractionDetailReportData.get(i).mLikeCommentsValues.totalLikes);
            } else{
                mTableReportExpandItemPojo.likeCount = 0;
            }
            if(mSocialInteractionDetailReportData.get(i).mLikeCommentsValues.totalComments.length() > 0){
                mTableReportExpandItemPojo.commentCount = Integer.parseInt(mSocialInteractionDetailReportData.get(i).mLikeCommentsValues.totalComments);
            } else{
                mTableReportExpandItemPojo.commentCount = 0;
            }
            mTableReportExpandItemPojo.isLiked = mSocialInteractionDetailReportData.get(i).mLikeCommentsValues.isLikeUsers;
            mTableReportExpandItemPojos.set(child, mTableReportExpandItemPojo);
            // mTableReportExpandItemPojos.add(mTableReportExpandItemPojo);

        }
        map.put(groupposition, mTableReportExpandItemPojos);
        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }

}