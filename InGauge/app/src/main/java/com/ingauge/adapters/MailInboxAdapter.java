package com.ingauge.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.pulsemail.FragmentInbox;
import com.ingauge.listener.RecyclerViewClickListenerForMail;
import com.ingauge.pojo.DeleteMailBody;
import com.ingauge.pojo.MailListModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 11-Jul-17.
 */

public class MailInboxAdapter extends RecyclerSwipeAdapter<MailInboxAdapter.MailViewHolder> implements Filterable{


    SwipeItemRecyclerMangerImpl mItemMange;

    private Context context;
    Fragment mFragment;
    private static RecyclerViewClickListenerForMail itemListener;
    private List<MailListModel.Datum> mMaDatumList;
    boolean[] itemChecked;
    String mailType;
    HomeActivity mHomeActivity;
    Call mCall;
    APIInterface apiInterface;

    public MailInboxAdapter(Context mContext, Fragment mFragment, RecyclerViewClickListenerForMail itemListener, List<MailListModel.Datum> mMaDatumList, String mailType){
        mItemMange = new SwipeItemRecyclerMangerImpl(this);
        this.context = mContext;
        mHomeActivity = (HomeActivity) mContext;
        this.mFragment = mFragment;
        this.itemListener = itemListener;
        this.mMaDatumList = mMaDatumList;
        itemChecked = new boolean[mMaDatumList.size()];
        this.mailType = mailType;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }


    @Override
    public MailViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_email_inbox, parent, false);
        return new MailInboxAdapter.MailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MailViewHolder viewHolder, final int position){


        String toStr = "To: ";
        if(mailType.equalsIgnoreCase("sent")){
            viewHolder.tvName.setText(toStr + mMaDatumList.get(position).getRecipientName());
        } else{
            viewHolder.tvName.setText(mMaDatumList.get(position).getFirstName() + " " + mMaDatumList.get(position).getLastName());
        }

        viewHolder.tvSubject.setText(mMaDatumList.get(position).getSubject());
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            viewHolder.tvShortDesc.setText(Html.fromHtml(mMaDatumList.get(position).getMessage(), Html.FROM_HTML_MODE_LEGACY));
        } else{
            viewHolder.tvShortDesc.setText(Html.fromHtml(mMaDatumList.get(position).getMessage()));

        }


        String profileUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(context) + "/" + mMaDatumList.get(position).getSenderId();
        Glide.with(context)
                .load(profileUrl)
                .centerCrop()
                .placeholder(R.drawable.defaultimg)
                .error(R.drawable.defaultimg)
                .into(viewHolder.mCircleImageView);
        viewHolder.swipeLayout.setEnabled(true);
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        mItemMange.bindView(viewHolder.swipeLayout, position);
        if(mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeletMail")){
            viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener(){

                @Override
                public void onStartOpen(SwipeLayout layout){
                    mItemManger.closeAllExcept(layout);
                }

                @Override
                public void onOpen(SwipeLayout layout){

                }

                @Override
                public void onStartClose(SwipeLayout layout){

                }

                @Override
                public void onClose(SwipeLayout layout){

                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset){

                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel){

                }
            });
        } else{

        }

        viewHolder.mCheckBoxDelete.setChecked(false);

        if(itemChecked[position])
            viewHolder.mCheckBoxDelete.setChecked(true);
        else
            viewHolder.mCheckBoxDelete.setChecked(false);

        viewHolder.mCheckBoxDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // TODO Auto-generated method stub
                if(viewHolder.mCheckBoxDelete.isChecked())
                    itemChecked[position] = true;
                else
                    itemChecked[position] = false;
            }
        });
        FragmentInbox mFragmentInbox = (FragmentInbox) mFragment;
        if(mFragmentInbox.isNeedtoDelete){
            viewHolder.mCheckBoxDelete.setVisibility(View.VISIBLE);
        } else{
            viewHolder.mCheckBoxDelete.setVisibility(View.GONE);
        }
        Logger.Error(" Read UR " + mMaDatumList.get(position).getIsReadUr());

        if(!mMaDatumList.get(position).getIsReadUr() && mailType.equalsIgnoreCase("inbox")){
            viewHolder.ivReadMail.setVisibility(View.VISIBLE);

            int logginuserid = InGaugeSession.read(context.getResources().getString(R.string.key_user_id), 0);
            if(mMaDatumList.get(position).getSenderId() == logginuserid){
                viewHolder.ivReadMail.setVisibility(View.GONE);
            }
        } else{
            viewHolder.ivReadMail.setVisibility(View.GONE);
        }
        /*if(!mMaDatumList.get(position).getIsReadUr() && (mMaDatumList.get(position).getSenderId()==InGaugeSession.read(context.getResources().getString(R.string.key_user_id), 0))){
            //viewHolder.readSepView.setVisibility(View.INVISIBLE);
            viewHolder.ivReadMail.setVisibility(View.GONE);
        }else{
            viewHolder.ivReadMail.setVisibility(View.VISIBLE);
        }*/

        if(mMaDatumList.get(position).getCreatedOn() != null){
            String readTime = UiUtils.ConvertToDeviceLocal(mMaDatumList.get(position).getCreatedOn(), "dd-MMM-yyyy hh:mm:ss aa");
            viewHolder.tvDate.setText(readTime);
        }


    }

    @Override
    public Filter getFilter(){
        return null;
    }

    public class MailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        SwipeLayout swipeLayout;
        CheckBox mCheckBoxDelete;
        TextView tvDelete;
        TextView tvName, tvSubject, tvShortDesc, tvDate;
        CircleImageView mCircleImageView;
        ImageView ivAttachment, ivReadMail;
        LinearLayout mRelativeLayoutLeft;


        private MailViewHolder(View itemView){
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            mCheckBoxDelete = (CheckBox) itemView.findViewById(R.id.row_email_inbox_cb);
            tvDelete = (TextView) itemView.findViewById(R.id.row_email_inbox_tv_delete);
            tvDelete.setOnClickListener(this);
            mCircleImageView = (CircleImageView) itemView.findViewById(R.id.row_email_inbox_iv_user_image);
            tvName = (TextView) itemView.findViewById(R.id.row_email_inbox_tv_name);
            tvSubject = (TextView) itemView.findViewById(R.id.row_email_inbox_tv_subject);
            tvShortDesc = (TextView) itemView.findViewById(R.id.row_email_inbox_tv_short_desc);
            ivAttachment = (ImageView) itemView.findViewById(R.id.row_email_inbox_iv_attachment);
            tvDate = (TextView) itemView.findViewById(R.id.row_email_inbox_tv_date);
            mRelativeLayoutLeft = (LinearLayout) itemView.findViewById(R.id.row_email_inbox_rl_left);
            ivReadMail = (ImageView) itemView.findViewById(R.id.iv_unread_mail);
            mRelativeLayoutLeft.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
//            Log.d(getClass().getSimpleName(), "onItemSelected: " + textViewData.getText().toString());
            switch(v.getId()){
                case R.id.row_email_inbox_rl_left:
                    itemListener.recyclerViewListClicked(v, getLayoutPosition());
                    break;
                case R.id.row_email_inbox_tv_delete:
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    AlertDialog dialog;
                    builder.setTitle("");
                    builder.setMessage(context.getResources().getString(R.string.alert_delete));
                    builder.setCancelable(false);
                    builder.setPositiveButton(
                            context.getResources().getString(R.string.alert_yes),
                            new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){
                                    DeleteMailBody deleteMailBody = new DeleteMailBody();
                                    List<String> mStringsIds = new ArrayList<>();
                                    mStringsIds.add(String.valueOf(mMaDatumList.get(getLayoutPosition()).getId()));
                                    deleteMailBody.setIds(mStringsIds);
                                    deleteMailBody.setType(mailType);
                                    mMaDatumList.remove(getLayoutPosition());
                                    notifyItemRemoved(getLayoutPosition());
                                    notifyItemRangeChanged(getLayoutPosition(), mMaDatumList.size());
                                    itemChecked = new boolean[mMaDatumList.size()];
                                    deleteMail(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), deleteMailBody);

                                    swipeLayout.postDelayed(new Runnable(){
                                        @Override
                                        public void run(){
                                            swipeLayout.close();
                                            notifyDataSetChanged();
                                        }
                                    }, 100);
                                }
                            });
                    builder.setNegativeButton(
                            context.getResources().getString(R.string.alert_no), new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){
                                }
                            }
                    );
                    //  builder.show();
                    dialog = builder.create();
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    dialog.show();

                    break;
            }

        }
    }

    @Override
    public int getItemCount(){
        return mMaDatumList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position){
        return R.id.swipe;
    }

    public void deleteData(){

        List<String> mStringsIds = new ArrayList<>();
        for(int i = 0; i < itemChecked.length; i++){
            if(itemChecked[i]){
                mStringsIds.add(String.valueOf(mMaDatumList.get(i).getId()));
                mMaDatumList.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, mMaDatumList.size());
                itemChecked = new boolean[mMaDatumList.size()];
            }
        }

        DeleteMailBody deleteMailBody = new DeleteMailBody();
        deleteMailBody.setIds(mStringsIds);
        deleteMailBody.setType(mailType);
        if(mStringsIds.size() > 0){
            Bundle bundle = new Bundle();
            bundle.putString("email", InGaugeSession.read(context.getResources().getString(R.string.key_user_user_email), ""));
            bundle.putString("role", InGaugeSession.read(context.getResources().getString(R.string.key_user_role_name), ""));
            bundle.putString("remove_ids", Arrays.asList(deleteMailBody.getIds()).toString());
            InGaugeApp.getFirebaseAnalytics().logEvent("remove_mails", bundle);
            deleteMail(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), deleteMailBody);
        }

    }

    void deleteMail(String authToken, final DeleteMailBody deleteMailBody){

        /*{"emailId":["10583","10590","646"],
            "subject":"New Mail",
                "message":"ABC"
        }*/
        mHomeActivity.startProgress(context);
        mCall = apiInterface.deleteMail(deleteMailBody);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                mHomeActivity.endProgress();
                try{
                    JSONObject mailData = new JSONObject(response.body().string());
                    Logger.Error("Response " + mailData.toString());
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(context.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(context.getResources().getString(R.string.key_user_role_name), ""));
                    String listString = "";

                    for(String s : deleteMailBody.getIds()){
                        listString += s + "\t";
                    }
                    bundle.putString("remove_ids", listString);
                    InGaugeApp.getFirebaseAnalytics().logEvent("remove_mails", bundle);
                } catch(JSONException e){
                    e.printStackTrace();
                } catch(IOException e){
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

}
