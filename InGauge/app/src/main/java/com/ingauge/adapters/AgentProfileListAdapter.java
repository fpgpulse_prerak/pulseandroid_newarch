package com.ingauge.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.pojo.AgentProfileHowAmIDoingList;
import com.ingauge.utils.Logger;

import java.util.List;

/**
 * Created by mansurum on 03-Apr-18.
 */

public class AgentProfileListAdapter extends RecyclerView.Adapter<AgentProfileListAdapter.AgentProfileViewHolder>{

    List<AgentProfileHowAmIDoingList> mAgentProfileHowAmIDoingLists;
    Context mContext;
    boolean isLocationAvailable=false;

    public AgentProfileListAdapter(Context mContext, List<AgentProfileHowAmIDoingList> mAgentProfileHowAmIDoingLists,boolean isLocationAvailable){
        this.mContext = mContext;
        this.mAgentProfileHowAmIDoingLists = mAgentProfileHowAmIDoingLists;
        this.isLocationAvailable = isLocationAvailable;
    }


    @Override
    public AgentProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_agent_profile_items, parent, false);
        return new AgentProfileListAdapter.AgentProfileViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AgentProfileViewHolder holder, int position){
        AgentProfileHowAmIDoingList mAgentProfileHowAmIDoingList = mAgentProfileHowAmIDoingLists.get(position);
        holder.tvReportName.setText(mAgentProfileHowAmIDoingList.reportName);
        if(isLocationAvailable){
            holder.tvReportValue.setText(mAgentProfileHowAmIDoingList.reportValue);
        }else{
            holder.tvReportValue.setText("-");
        }


        Typeface mTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/fa-regular-400_pro.ttf");
        holder.tvFontIcon.setTypeface(mTypeface);

        Logger.Error("<<<< Hex Code  >>>>  " +  mAgentProfileHowAmIDoingList.hexCode);
        holder.tvFontIcon.setText(String.valueOf((char) mAgentProfileHowAmIDoingList.hexCode));
        holder.tvFontIcon.setTextColor(Color.parseColor(mAgentProfileHowAmIDoingList.colorCode));

    }

    @Override
    public int getItemCount(){
        return mAgentProfileHowAmIDoingLists.size();
    }

    public class AgentProfileViewHolder extends RecyclerView.ViewHolder{
        TextView tvReportName;
        TextView tvReportValue;
        TextView tvFontIcon;


        AgentProfileViewHolder(View view){
            super(view);
            tvReportValue = (TextView) view.findViewById(R.id.row_agent_profile_items_tv_report_value);
            tvReportName = (TextView) view.findViewById(R.id.row_agent_profile_items_tv_report_name);
            tvFontIcon = (TextView) view.findViewById(R.id.row_agent_profile_items_tv_fa_icon);

        }

    }

}
