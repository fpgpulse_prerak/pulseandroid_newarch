package com.ingauge.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.ArrayList;
import java.util.List;


public class FilterDetailListAdapterObservation extends RecyclerView.Adapter<FilterDetailListAdapterObservation.FilterViewHolder> implements Filterable {
    Activity context;
    List<FilterBaseData> mBaseDataList;
    private List<FilterBaseData> dictionaryWords;
    private List<FilterBaseData> filteredList;
    private CustomFilter mFilter;
    private HomeActivity mHomeActivity;

    public FilterDetailListAdapterObservation(Activity context, List<FilterBaseData> mBaseDataList,HomeActivity mHomeActivity) {
        this.context = context;
        this.mBaseDataList = mBaseDataList;
        dictionaryWords = mBaseDataList;
        filteredList = new ArrayList<>();
        this.mHomeActivity=mHomeActivity;
        filteredList.addAll(dictionaryWords);
        mFilter = new CustomFilter(FilterDetailListAdapterObservation.this);
    }
    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType)  {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_filter_list, parent, false);
        return new FilterDetailListAdapterObservation.FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        holder.tvTitle.setText(mBaseDataList.get(position).filterName);
        holder.tvFilterValue.setText(mBaseDataList.get(position).name);
        Logger.Error("POS::::"+mBaseDataList.get(position).filterIndex);
        if (TextUtils.isEmpty(mBaseDataList.get(position).name)) {
            holder.tvFilterValue.setVisibility(View.GONE);
        } else {
            holder.tvFilterValue.setVisibility(View.VISIBLE);
        }
            holder.ivRight.setVisibility(View.GONE);
            holder.tvFilterValue.setVisibility(View.GONE);
            holder.tvTitle.setText(mBaseDataList.get(position).name);

    }

    @Override
    public int getItemCount() {
        return mBaseDataList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tvTitle;
        private TextView tvFilterValue;
        public ImageView ivRight;

        public FilterViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.row_item_filter_list_tv_name);
            ivRight = (ImageView) view.findViewById(R.id.row_item_filter_iv);
            tvFilterValue = (TextView) view.findViewById(R.id.tv_filter_list_select_filter_name);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
             switch (mBaseDataList.get(getAdapterPosition()).filterIndex){
                 case 0:
                     Logger.Error(getAdapterPosition()+"\n"+mBaseDataList.get(getAdapterPosition()).name);
                     String previousLocationId=InGaugeSession.read(context.getResources().getString(R.string.key_obs_selected_location_id),null);

                     if(previousLocationId!=null && !(mBaseDataList.get(getAdapterPosition()).id.equalsIgnoreCase(previousLocationId))){
                         InGaugeSession.write(context.getResources().getString(R.string.key_obs_cc_is_location_changed),true);
                     }else{
                         InGaugeSession.write(context.getResources().getString(R.string.key_obs_cc_is_location_changed),false);
                     }
                     InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_location_name),mBaseDataList.get(getAdapterPosition()).name);
                     InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_location_id),mBaseDataList.get(getAdapterPosition()).id);
                     //InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_agent_id), "");
                     break;
                 case 1:
                     InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_pl_name),mBaseDataList.get(getAdapterPosition()).name);
                     InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_pl_id),mBaseDataList.get(getAdapterPosition()).id);
                     break;
                 case 2:
                     InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_agent_name),mBaseDataList.get(getAdapterPosition()).name);
                     InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_agent_id),mBaseDataList.get(getAdapterPosition()).id);
                     break;
                 case 3:
                     InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_observation_name),"("+mBaseDataList.get(getAdapterPosition()).name+")");
                     InGaugeSession.write(context.getResources().getString(R.string.key_obs_selected_observation_id),mBaseDataList.get(getAdapterPosition()).id);
                     break;

             }

            mHomeActivity.onBackPressed();
        }
    }

    public class CustomFilter extends Filter {
        private FilterDetailListAdapterObservation mAdapter;
        private CustomFilter(FilterDetailListAdapterObservation mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(dictionaryWords);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final FilterBaseData mWords : dictionaryWords) {
                    if (mWords.name.toLowerCase().contains(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }
            }
            System.out.println("Count Number " + filteredList.size());
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println("Count Number 2 " + ((List<FilterBaseData>) results.values).size());
            mBaseDataList = (ArrayList<FilterBaseData>) results.values;
            this.mAdapter.notifyDataSetChanged();
        }
    }
}