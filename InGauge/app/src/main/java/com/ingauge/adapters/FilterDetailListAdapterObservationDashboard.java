package com.ingauge.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.listener.ObservationDashboardFilterListener;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.utils.Logger;

import java.util.ArrayList;
import java.util.List;


public class FilterDetailListAdapterObservationDashboard extends RecyclerView.Adapter<FilterDetailListAdapterObservationDashboard.FilterViewHolder> implements Filterable {
    Activity context;
    ArrayList<FilterBaseData> mBaseDataList;
    private List<FilterBaseData> dictionaryWords;
    private List<FilterBaseData> filteredList;
    private CustomFilter mFilter;
    private HomeActivity mHomeActivity;
    public ObservationDashboardFilterListener observationDashboardFilterListener;

    public FilterDetailListAdapterObservationDashboard(Activity context, ArrayList<FilterBaseData> mBaseDataList, HomeActivity mHomeActivity) {
        this.context = context;
        this.mBaseDataList = mBaseDataList;
        dictionaryWords = mBaseDataList;
        filteredList = new ArrayList<>();
        this.mHomeActivity = mHomeActivity;
        observationDashboardFilterListener = (ObservationDashboardFilterListener) context;
        filteredList.addAll(dictionaryWords);
        mFilter = new CustomFilter(FilterDetailListAdapterObservationDashboard.this);
    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_filter_list, parent, false);
        return new FilterDetailListAdapterObservationDashboard.FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        holder.tvTitle.setText(mBaseDataList.get(position).filterName);
        holder.tvFilterValue.setText(mBaseDataList.get(position).name);
        Logger.Error("POS::::" + mBaseDataList.get(position).filterIndex);
        if (TextUtils.isEmpty(mBaseDataList.get(position).name)) {
            holder.tvFilterValue.setVisibility(View.GONE);
        } else {
            holder.tvFilterValue.setVisibility(View.VISIBLE);
        }
        holder.ivRight.setVisibility(View.GONE);
        holder.tvFilterValue.setVisibility(View.GONE);
        holder.tvTitle.setText(mBaseDataList.get(position).name);

    }

    @Override
    public int getItemCount() {
        return mBaseDataList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        private TextView tvFilterValue;
        public ImageView ivRight;

        public FilterViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.row_item_filter_list_tv_name);
            ivRight = (ImageView) view.findViewById(R.id.row_item_filter_iv);
            tvFilterValue = (TextView) view.findViewById(R.id.tv_filter_list_select_filter_name);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mHomeActivity.onBackPressed();
            switch (mBaseDataList.get(getAdapterPosition()).filterIndex) {
                case 0:
                    observationDashboardFilterListener.updateData(mBaseDataList.get(getAdapterPosition()).name, mBaseDataList.get(getAdapterPosition()).id,0,mBaseDataList);
                    break;
                case 1:
                    observationDashboardFilterListener.updateData(mBaseDataList.get(getAdapterPosition()).name, mBaseDataList.get(getAdapterPosition()).id,1,mBaseDataList);
                    break;
                case 2:
                    observationDashboardFilterListener.updateData(mBaseDataList.get(getAdapterPosition()).name, mBaseDataList.get(getAdapterPosition()).id,2,mBaseDataList);
                    break;


            }
        }
    }

    public class CustomFilter extends Filter {
        private FilterDetailListAdapterObservationDashboard mAdapter;

        private CustomFilter(FilterDetailListAdapterObservationDashboard mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(dictionaryWords);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final FilterBaseData mWords : dictionaryWords) {
                    if (mWords.name.toLowerCase().contains(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }
            }
            System.out.println("Count Number " + filteredList.size());
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println("Count Number 2 " + ((List<FilterBaseData>) results.values).size());
            mBaseDataList = (ArrayList<FilterBaseData>) results.values;
            this.mAdapter.notifyDataSetChanged();
        }
    }
}