package com.ingauge.adapters;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ingauge.R;
import com.ingauge.api.APIClient;
import com.ingauge.utils.UiUtils;

import java.util.List;

/**
 * Created by desainid on 6/15/2017.
 */

public class FeedCommentListAdapter extends ArrayAdapter<String> {
    Activity context;
    List<String> values;
    List<String> valuesName;
    List<Integer> userId;

    public FeedCommentListAdapter(Activity context, List<String> values, List<String> valuesName,List<Integer> userId) {
        super(context, R.layout.row_item_feeds_comment, values);
        this.context = context;
        this.values = values;
        this.valuesName = valuesName;
        this.userId=userId;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    //    position=position-1;
        LayoutInflater inflater = context.getLayoutInflater();
        View view = inflater.inflate(R.layout.row_item_feeds_comment, null, false);
        final TextView tvComment = (TextView) view.findViewById(R.id.txt_comment);
        final TextView tvCommentName = (TextView) view.findViewById(R.id.txt_comment_name);
        final TextView tvMore = (TextView) view.findViewById(R.id.txt_more);
        final ImageView userImage=(ImageView) view.findViewById(R.id.user_picture);
        String imageUrl= UiUtils.baseUrl+"api/user/avatar/"+ UiUtils.getIndustryId(context)+"/"+userId.get(position);
        Glide.with(context)
                .load(imageUrl)
                .centerCrop()
                .error(R.drawable.placeholder)
                .into(userImage);
        tvComment.setMaxLines(2);
        try{
            tvComment.setText(values.get(position));
            tvCommentName.setText(valuesName.get(position));
        } catch(Exception e){
            e.printStackTrace();
        }
        tvComment.post(new Runnable() {
            @Override
            public void run() {
                int lineCount = tvComment.getLineCount();
                if (lineCount > 2) {
                      tvMore.setVisibility(View.VISIBLE);
                }

            }
        });
        tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tvMore.getText().toString().equalsIgnoreCase("More"))
                {
                    tvComment.setMaxLines(40);
                    tvMore.setText("Less");
                }
                else
                {
                    tvComment.setMaxLines(2);
                    tvMore.setText("More");
                }

            }
        });
        //  makeTextViewResizable(tvComment, 3, "View More", false);

        return view;
    }

    public void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                     final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, "View More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

}


