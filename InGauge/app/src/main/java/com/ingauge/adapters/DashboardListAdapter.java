package com.ingauge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.DashboardModelList;
import com.ingauge.session.InGaugeSession;

import java.util.List;

/**
 * Created by mansurum on 18-May-17.
 */

public class DashboardListAdapter extends RecyclerView.Adapter<DashboardListAdapter.DashboardViewHolder> {
    private Context mContext;
    private List<DashboardModelList.Datum> mList;
    private static RecyclerViewClickListener itemListener;
    public DashboardListAdapter(Context mContext, List<DashboardModelList.Datum> list, RecyclerViewClickListener itemListener) {
        this.mContext = mContext;
        this.mList = list;
        this.itemListener = itemListener;
    }

    @Override
    public DashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_dashboard, parent, false);
        return new DashboardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DashboardViewHolder holder, int position) {
        DashboardModelList.Datum mDatum = mList.get(position);
        holder.tvTitle.setText(mDatum.getName());
        if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard), "").equals(mDatum.getTitle())) {
            holder.imvSelected.setVisibility(View.VISIBLE);
        }else{
            holder.imvSelected.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DashboardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        public ImageView imvSelected;

        public DashboardViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tv_row_item);
            imvSelected = (ImageView) view.findViewById(R.id.img_selected);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v,null,this.getLayoutPosition(),0,false);
        }
    }
}
