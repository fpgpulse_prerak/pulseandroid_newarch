package com.ingauge.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ingauge.R;

import java.util.List;

/**
 * Created by desainid on 5/22/2017.
 */

public class PreferenceListadapter extends ArrayAdapter<String> {

    Activity context;
    List<String> values;
    List<String> description;
    public int lastCheckedPosition = 1;
    String selectedIndustry = "";

    public PreferenceListadapter(Activity context, List<String> values, List<String> description, String selectedIndustry) {
        super(context, R.layout.item_listview_more, values);
        this.context = context;
        this.values = values;
        this.description = description;
        this.selectedIndustry = selectedIndustry;
//        lastCheckedPosition = getIndexByName(selectedIndustry);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.row_item_preference_detail, null, true);
        TextView txtListItem = (TextView) listViewItem.findViewById(R.id.txt_list_item);
        TextView txtListDescriptnItem = (TextView) listViewItem.findViewById(R.id.txt_list_item_description);
        txtListDescriptnItem.setText(description.get(position));
        txtListItem.setText(values.get(position));
        /*if(description.size()!=0)
        {
            txtListDescriptnItem.setText(description.get(position));
        }*/

        if (position == lastCheckedPosition) {
            txtListItem.setTextColor(ContextCompat.getColor(context, R.color.pink_text_color_for_filter));
            txtListDescriptnItem.setTextColor(ContextCompat.getColor(context, R.color.black));
        } else {
            txtListItem.setTextColor(ContextCompat.getColor(context, R.color.black));
            txtListDescriptnItem.setTextColor(ContextCompat.getColor(context, R.color.dark_grey_for_filter));
        }
        return listViewItem;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    private int getIndexByName(String selectedName) {
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i) != null && values.get(i).equals(selectedName)) {
                return i;
            }
        }
        return 0;// not there is list
    }

}
