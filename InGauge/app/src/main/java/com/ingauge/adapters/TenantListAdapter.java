package com.ingauge.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.IndustryDataModel;
import com.ingauge.pojo.TenantDataModel;

import java.util.List;

/**
 * Created by pathanaa on 31-05-2017.
 */

public class TenantListAdapter extends BaseAdapter {
    private Context context; //context
    List<TenantDataModel.Data> tenantList;
    private static RecyclerViewClickListener itemListener;

    public TenantListAdapter(Context context,List<TenantDataModel.Data> tenantList) {
        this.tenantList = tenantList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return tenantList.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return tenantList.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row

        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_listview_more, parent, false);
            holder = new ViewHolder();
            holder.txtListItem = (TextView) convertView.findViewById(R.id.txt_list_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtListItem.setText(tenantList.get(position).getTenantName());
        return convertView;
    }

    class ViewHolder {
        TextView txtListItem;

    }


}