package com.ingauge.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.pojo.RecipientListModel;

import java.util.List;

/**
 * Created by mansurum on 14-Jul-17.
 */

public class TagEmailAdapter extends ArrayAdapter<RecipientListModel.Datum> {

    private Context mContext;
    private List<RecipientListModel.Datum> recipientListModels;

    int layoutResourceId;

    public TagEmailAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<RecipientListModel.Datum> objects) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.mContext = context;
        this.recipientListModels = objects;
    }
   /* public TagEmailAdapter(){

    }
    public TagEmailAdapter(Context mContext, List<RecipientListModel.Datum> recipientListModels, Fragment mFragment){
        this.mContext = mContext;
        this.recipientListModels = recipientListModels;
        this.mFragment = mFragment;
    }

    @Override
    public int getCount() {
        return recipientListModels.size();
    }

    @Override
    public Object getItem(int position) {
        return recipientListModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }*/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(layoutResourceId, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.row_email_tags_name);
            holder.tvEmail= (TextView) convertView.findViewById(R.id.row_email_tags_email);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        RecipientListModel.Datum mDatum = recipientListModels.get(position);

        holder.tvName.setText(mDatum.getName());
        holder.tvEmail.setText(mDatum.getEmail());
        return convertView;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView tvName;
        TextView tvEmail;
    }
}
