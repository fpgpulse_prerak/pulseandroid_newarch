package com.ingauge.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ingauge.R;
import com.ingauge.pojo.UserNotificationListModel;
import com.ingauge.utils.ImageLoader;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by mansurum on 13-07-2017.
 */

public class UserNotificationListAdapter extends BaseAdapter implements StickyListHeadersAdapter{

    private Context mContext;

    protected LayoutInflater inflater;
    List<UserNotificationListModel.Datum> mDatumArrayList = new ArrayList<>();

    public ImageLoader imageLoader;

    public UserNotificationListAdapter(Context context, List<UserNotificationListModel.Datum> mDatumArrayList){
        this.mContext = context;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mDatumArrayList = mDatumArrayList;
        imageLoader = new ImageLoader(mContext.getApplicationContext());

    }


    @Override
    public int getCount(){
        return mDatumArrayList.size();
    }

    @Override
    public Object getItem(int position){
        return mDatumArrayList.get(position);
    }

    @Override
    public long getItemId(int position){
        return mDatumArrayList.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        final ViewHolder holder;

        //if it is a new item
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_user_notification_list, parent, false);

            holder = new ViewHolder();
            holder.mCircleImageViewuser = (ImageView) convertView.findViewById(R.id.row_user_notification_list_iv_user_profile);
            holder.tvNotificationContent = (TextView) convertView.findViewById(R.id.row_user_notification_list_tv_content);
            holder.tvTime = (TextView) convertView.findViewById(R.id.row_user_notification_list_tv_content_time);
            holder.ivSocialType = (ImageView) convertView.findViewById(R.id.row_user_notification_list_iv_content_type);
            holder.tvLocation = (TextView) convertView.findViewById(R.id.row_user_notification_list_tv_location);


            convertView.setTag(holder);
        } else{
            //here we recycle the previous ViewHolder, by using an older one
            holder = (ViewHolder) convertView.getTag();
        }
        /*ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.defaultimg)
                .showImageOnFail(R.drawable.defaultimg)
                .showImageOnLoading(R.drawable.defaultimg).build();
*/
        holder.tvNotificationContent.setText(mDatumArrayList.get(position).getContent());

        String imageUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(mContext) + "/" + mDatumArrayList.get(position).getCreatedById();

        Logger.Error("<<<< Image URL " + imageUrl);
        if(mDatumArrayList.get(position).getEntityType().equalsIgnoreCase("SocialInteractionLike")
                || mDatumArrayList.get(position).getEntityType().equalsIgnoreCase("FeedSocialInteractionLike")){

            holder.ivSocialType.setImageResource(R.drawable.ic_liked);

        } else if(mDatumArrayList.get(position).getEntityType().equalsIgnoreCase("SocialInteractionComment")
                || mDatumArrayList.get(position).getEntityType().equalsIgnoreCase("FeedSocialInteractionComment")){
            holder.ivSocialType.setImageResource(R.drawable.ic_comment_for_notification);

        }
        holder.tvLocation.setText(mDatumArrayList.get(position).getTenantName());
        // Picasso.get().load(imageUrl).into(holder.mCircleImageViewuser);

        //Glide.with(view.getContext()).load(url).placeholder(R.drawable.default_profile).dontAnimate().into(view);

        /*Glide.with(mContext)
                .load(imageUrl)
                .placeholder(R.drawable.defaultimg)
                .into(holder.mCircleImageViewuser);
*/

        //imageLoader.displayImage(imageUrl, holder.mCircleImageViewuser, options);
        // imageLoader.DisplayImage(imageUrl, holder.mCircleImageViewuser);


        Glide.with(mContext).load(imageUrl).asBitmap().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).dontAnimate().centerCrop().into(new BitmapImageViewTarget(holder.mCircleImageViewuser){
            @Override
            protected void setResource(Bitmap resource){
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.mCircleImageViewuser.setImageDrawable(circularBitmapDrawable);
            }
        });

        String startDateString = String.valueOf(mDatumArrayList.get(position).getCreatedOn());


        Long datelong = mDatumArrayList.get(position).getCreatedOn();
        String monthYear = DateFormat.format("MMM dd,yyyy", datelong).toString();
        String hourtime = DateFormat.format("hh:mm a", datelong).toString();


        holder.tvTime.setText(hourtime);
        /*try{
//            2017-12-14 05:44:38

            String dateStr = startDateString;
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(dateStr);
            df.setTimeZone(TimeZone.getDefault());
            String formattedDate = df.format(date);

            SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            _24HourSDF.setTimeZone(TimeZone.getDefault());

            SimpleDateFormat _12HourSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            _12HourSDF.setTimeZone(TimeZone.getDefault());
            Date _24HourDt = _24HourSDF.parse(formattedDate);

            _24HourSDF.setTimeZone(TimeZone.getDefault());
            _12HourSDF.setTimeZone(TimeZone.getDefault());
            Logger.Error("<<<<<<  Twenty Four " + _24HourDt);
            Logger.Error("<<<<<<  Twelve  " + _12HourSDF.format(_24HourDt));
            System.out.println(_24HourDt);
            System.out.println(_12HourSDF.format(_24HourDt));


            long now = System.currentTimeMillis();
            Date mDate1 = new Date();

            mDate1.getTime();
            String datetime1 = _12HourSDF.format(_24HourDt);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
            Date convertedDate = dateFormat.parse(datetime1);


            Calendar c = Calendar.getInstance();
            Date datetoday = c.getTime(); //current date and time in UTC
            SimpleDateFormat todaySDf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            todaySDf.setTimeZone(TimeZone.getDefault()); //format in given timezone
            String strDate = todaySDf.format(datetoday);
            try{
                Calendar todayFinalCalendar = UiUtils.DateToCalendar(todaySDf.parse(strDate));

                CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                        convertedDate.getTime(),
                        todayFinalCalendar.getTimeInMillis(),
                        DateUtils.FORMAT_ABBREV_ALL);


                *//*if(getTimeAgo(todayFinalCalendar.getTime(), convertedDate) != null)
                    holder.tvTime.setText(getTimeAgo(todayFinalCalendar.getTime(), convertedDate));
                else{

                    holder.tvTime.setText(String.valueOf(0) + 'h');
                }
*//*

            } catch(ParseException e){
                e.printStackTrace();
            }


        } catch(final ParseException e){
            e.printStackTrace();
        }*/
        return convertView;
    }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent){
        DividerViewHolder holder;

        if(convertView == null){
            holder = new DividerViewHolder();
            convertView = inflater.inflate(R.layout.row_user_notification_list_header, parent, false);
            holder.tvDateHeader = (TextView) convertView.findViewById(R.id.row_user_notification_list_header_tv_date);

            convertView.setTag(holder);
        } else{
            holder = (DividerViewHolder) convertView.getTag();
        }

        Long datelong = mDatumArrayList.get(position).getCreatedOn();
        String date = DateFormat.format("dd", datelong).toString();
        String monthYear = DateFormat.format("MMM dd,yyyy", datelong).toString();
        holder.tvDateHeader.setText(monthYear);
        String hourtime = DateFormat.format("hh:mm a", datelong).toString();

        /*holder.tvSectionIndex.setText("Section: " + mHomeActivity.arrayObsQuestion.get(position).getSection());
        holder.tvSectionLabel.setText(mHomeActivity.arrayObsQuestion.get(position).getQ_title());*/


        return convertView;
    }

    @Override
    public long getHeaderId(int position){

        Long datelong = mDatumArrayList.get(position).getCreatedOn();
        String date = DateFormat.format("dd", datelong).toString();
        String monthYear = DateFormat.format("MMM dd,yyyy", datelong).toString();
        String hourtime = DateFormat.format("hh:mm a", datelong).toString();
        return Long.parseLong(date);
    }


    private class ViewHolder{
        ImageView mCircleImageViewuser;
        TextView tvLocation;
        TextView tvNotificationContent;
        TextView tvTime;
        ImageView ivSocialType;
    }

    public static class DividerViewHolder{
        TextView tvDateHeader;

    }
}
