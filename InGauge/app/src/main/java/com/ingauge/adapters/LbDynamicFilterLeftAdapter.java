package com.ingauge.adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.FilterBaseData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 11-Oct-17.
 */


public class LbDynamicFilterLeftAdapter extends RecyclerView.Adapter<LbDynamicFilterLeftAdapter.FilterViewHolder> implements Filterable {
    Activity context;
    List<FilterBaseData> mBaseDataList;
    private List<FilterBaseData> dictionaryWords;
    private List<FilterBaseData> filteredList;
    private LbDynamicFilterLeftAdapter.CustomFilter mFilter;
    private static RecyclerViewClickListener itemListener;
    private boolean isFromDetails = false;

    //  int level = 0;
    private int lastCheckedPosition = 0;

    public LbDynamicFilterLeftAdapter(Activity context, List<FilterBaseData> mBaseDataList, RecyclerViewClickListener itemListener, boolean isFromDetails, int level) {
        this.context = context;
        this.mBaseDataList = mBaseDataList;
        this.itemListener = itemListener;
        this.isFromDetails = isFromDetails;
        dictionaryWords = mBaseDataList;
        filteredList = new ArrayList<> ();
        filteredList.addAll ( dictionaryWords );
        //    this.level = level;
        mFilter = new LbDynamicFilterLeftAdapter.CustomFilter ( LbDynamicFilterLeftAdapter.this );

    }

    @Override
    public LbDynamicFilterLeftAdapter.FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from ( parent.getContext () )
                .inflate ( R.layout.row_filter_dynamic_keft, parent, false );
        return new LbDynamicFilterLeftAdapter.FilterViewHolder ( itemView );
    }

    @Override
    public void onBindViewHolder(LbDynamicFilterLeftAdapter.FilterViewHolder holder, int position) {
        FilterBaseData mFilterBaseData = mBaseDataList.get ( position );

        if ( mFilterBaseData.isDynamicFilter ) {
            if ( mFilterBaseData.isSubRegionOpen ) {
                holder.tvTitle.setText ( mBaseDataList.get ( position ).filterName );
                holder.tvFilterValue.setText ( mBaseDataList.get ( position ).name );
                if ( mBaseDataList.get ( position ).filterIndex == lastCheckedPosition ) {
                    mBaseDataList.get ( position ).isSelectedPosition = true;

                } else {
                    mBaseDataList.get ( position ).isSelectedPosition = false;
                }
                if ( mBaseDataList.get ( position ).isSelectedPosition ) {
                    holder.tvTitle.setTextColor ( ContextCompat.getColor ( context, R.color.pink_text_color_for_filter ) );
                    holder.tvFilterValue.setTextColor ( ContextCompat.getColor ( context, R.color.black ) );
                    if(mBaseDataList.get ( position ).isDynamicFilter){
                        mBaseDataList.get ( position ).isDynamicFilterSelected = true;
                    }else{
                        mBaseDataList.get ( position ).isDynamicFilterSelected = false;
                    }
                } else {
                    holder.tvTitle.setTextColor ( ContextCompat.getColor ( context, R.color.black ) );
                    holder.tvFilterValue.setTextColor ( ContextCompat.getColor ( context, R.color.dark_grey_for_filter ) );
                }
            } else {
                holder.tvTitle.setText ( mBaseDataList.get ( position ).filterName );
                holder.tvFilterValue.setText ( "" );
                holder.tvTitle.setTextColor ( ContextCompat.getColor ( context, R.color.dark_grey_for_filter ) );
                holder.tvFilterValue.setTextColor ( ContextCompat.getColor ( context, R.color.dark_grey_for_filter ) );
            }

        } else {
            holder.tvTitle.setText ( mBaseDataList.get ( position ).filterName );
            holder.tvFilterValue.setText ( mBaseDataList.get ( position ).name );
            if ( mBaseDataList.get ( position ).filterIndex == lastCheckedPosition ) {
                mBaseDataList.get ( position ).isSelectedPosition = true;
            } else {
                mBaseDataList.get ( position ).isSelectedPosition = false;
            }
            if ( mBaseDataList.get ( position ).isSelectedPosition ) {
                holder.tvTitle.setTextColor ( ContextCompat.getColor ( context, R.color.pink_text_color_for_filter ) );
                holder.tvFilterValue.setTextColor ( ContextCompat.getColor ( context, R.color.black ) );
            } else {
                holder.tvTitle.setTextColor ( ContextCompat.getColor ( context, R.color.black ) );
                holder.tvFilterValue.setTextColor ( ContextCompat.getColor ( context, R.color.dark_grey_for_filter ) );
            }

        }
        holder.itemView.setTag ( position );

    }

    @Override
    public int getItemCount() {
        return mBaseDataList.size ();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        private TextView tvFilterValue;
        private LinearLayout linearLayoutMain;


        public FilterViewHolder(View view) {
            super ( view );
            tvTitle = (TextView) view.findViewById ( R.id.row_filter_dynamic_left_tv_title );
            tvFilterValue = (TextView) view.findViewById ( R.id.row_filter_dynamic_left_tv_value );
            linearLayoutMain = (LinearLayout) view.findViewById ( R.id.filter_dynamic_left_ll_geography_type );
            view.setOnClickListener ( this );
        }

        @Override
        public void onClick(View v) {
            if ( mBaseDataList != null ) {
                if ( mBaseDataList.get ( getLayoutPosition () ).isDynamicFilter ) {
                    if ( mBaseDataList.get ( getLayoutPosition () ).isSubRegionOpen ) {
                        lastCheckedPosition = mBaseDataList.get ( getLayoutPosition () ).filterIndex;
                        notifyDataSetChanged ();
                        itemListener.recyclerViewListClicked ( v, mBaseDataList.get ( getLayoutPosition () ), getLayoutPosition (), mBaseDataList.get ( getLayoutPosition () ).filterIndex, true );
                    }
                } else {
                    lastCheckedPosition = mBaseDataList.get ( getLayoutPosition () ).filterIndex;
                    notifyDataSetChanged ();
                    itemListener.recyclerViewListClicked ( v, mBaseDataList.get ( getLayoutPosition () ), getLayoutPosition (), mBaseDataList.get ( getLayoutPosition () ).filterIndex, true );
                }


            }


        }
    }

    public class CustomFilter extends Filter {
        private LbDynamicFilterLeftAdapter mAdapter;

        private CustomFilter(LbDynamicFilterLeftAdapter mAdapter) {
            super ();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear ();
            final FilterResults results = new FilterResults ();
            if ( constraint.length () == 0 ) {
                filteredList.addAll ( dictionaryWords );
            } else {
                final String filterPattern = constraint.toString ().toLowerCase ().trim ();
                for (final FilterBaseData mWords : dictionaryWords) {
                    if ( mWords.name.toLowerCase ().contains ( filterPattern ) ) {
                        filteredList.add ( mWords );
                    }
                }
            }
            System.out.println ( "Count Number " + filteredList.size () );
            results.values = filteredList;
            results.count = filteredList.size ();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println ( "Count Number 2 " + ((List<FilterBaseData>) results.values).size () );
            mBaseDataList = (ArrayList<FilterBaseData>) results.values;
            this.mAdapter.notifyDataSetChanged ();
        }
    }
}
