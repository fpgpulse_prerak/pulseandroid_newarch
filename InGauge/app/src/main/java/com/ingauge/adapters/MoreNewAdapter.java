package com.ingauge.adapters;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ingauge.R;
import com.ingauge.pojo.MenuModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mansurum on 18-Jul-17.
 */

public class MoreNewAdapter extends BaseExpandableListAdapter{

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    private MenuModel menuModel;

    public MoreNewAdapter(Context context, List<String> listDataHeader,
                          HashMap<String, List<String>> listChildData, MenuModel menuModel){
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.menuModel = menuModel;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon){
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent){

        final String childText = (String) getChild(groupPosition, childPosition);

        if(convertView == null){
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_fragment_more_child, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);
        ImageView ivLeft = (ImageView) convertView
                .findViewById(R.id.row_fragment_more_child_iv);
        ivLeft.setColorFilter(_context.getResources().getColor(R.color.in_guage_blue));
        if(childPosition < menuModel.getData().get(groupPosition).getSubMenus().size() && menuModel.getData().get(groupPosition).getSubMenus() != null){

            if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/survey/observation/add")){
                ivLeft.setImageResource(R.drawable.ic_take_obs);
            } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/survey/observation/dashboard")){
                ivLeft.setImageResource(R.drawable.ic_obs_dashboard);
            } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/survey/counterCoaching/add")){
                ivLeft.setImageResource(R.drawable.ic_take_obs);
            } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/survey/counterCoaching/dashboard")){
                ivLeft.setImageResource(R.drawable.ic_obs_dashboard);
            } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/inbox")){
                ivLeft.setImageResource(R.drawable.ic_inbox_more);
            } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/sent")){
                ivLeft.setImageResource(R.drawable.ic_email_sent);
            } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/trash")){
                ivLeft.setImageResource(R.drawable.ic_delete_mail);
            }

            Logger.Error("<<<<  Child Position Title   Child Position  " + childPosition + menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl());
        }

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition){
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition){
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount(){
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent){
        String headerTitle = (String) getGroup(groupPosition);
        if(convertView == null){
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_fragment_more_new, null);
        }


        final ImageView listImages = (ImageView) convertView.findViewById(R.id.list_images);
        ImageView ivRightArrow = (ImageView) convertView.findViewById(R.id.img_Action_Right);
        TextView txtListItem = (TextView) convertView
                .findViewById(R.id.txt_list_item);
        TextView tvRight = (TextView) convertView.findViewById(R.id.row_fragment_more_new_tv_right);
        txtListItem.setText(headerTitle);


        if(isExpanded){
            ivRightArrow.setImageResource(R.drawable.ic_down_arrow);
        } else{
            ivRightArrow.setImageResource(R.drawable.ic_right_arrow);
        }

        int userid = InGaugeSession.read(_context.getString(R.string.key_user_id), 0);
        String imageUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(_context) + "/" + userid ;

        String logginusername = InGaugeSession.read(_context.getString(R.string.key_user_name), "");
        String roleName = InGaugeSession.read(_context.getString(R.string.key_user_role_name), "");
        if(groupPosition < menuModel.getData().size() && menuModel.getData().get(groupPosition) != null){

            Logger.Error("<<<<< Menu Model  GROUP POSITION " + groupPosition + " : " + menuModel.getData().get(groupPosition).getUrl());
            if(menuModel.getData().get(groupPosition).getUrl().equalsIgnoreCase("/goal/goalProgress")){
                listImages.setImageResource(R.drawable.goals);
//                         txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(null);
                ivRightArrow.setVisibility(View.VISIBLE);
                tvRight.setVisibility(View.GONE);
            } else if(menuModel.getData().get(groupPosition).getUrl().equalsIgnoreCase("/in-gauge-mail/mail")){
                listImages.setImageResource(R.drawable.email);
                //txtListItem.setTextColor(Color.LTGRAY);
              //  listImages.setColorFilter(_context.getResources().getColor(R.color.in_guage_blue));
                ivRightArrow.setVisibility(View.VISIBLE);
                tvRight.setVisibility(View.GONE);
            } else if(menuModel.getData().get(groupPosition).getUrl().equalsIgnoreCase("/survey/observation")){
                listImages.setImageResource(R.drawable.ic_observation);
                //txtListItem.setTextColor(Color.LTGRAY);
              //  listImages.setColorFilter(_context.getResources().getColor(R.color.in_guage_blue));
                ivRightArrow.setVisibility(View.VISIBLE);
                tvRight.setVisibility(View.GONE);
            } else if(menuModel.getData().get(groupPosition).getUrl().equalsIgnoreCase("/survey/counterCoaching")){
                listImages.setImageResource(R.drawable.ic_observation);
                //txtListItem.setTextColor(Color.LTGRAY);
              //  listImages.setColorFilter(_context.getResources().getColor(R.color.in_guage_blue));
                ivRightArrow.setVisibility(View.VISIBLE);
                tvRight.setVisibility(View.GONE);
            }
        } else{
            ivRightArrow.setVisibility(View.GONE);
            tvRight.setVisibility(View.GONE);
        }

        if(headerTitle.equalsIgnoreCase("My Profile")){
            /*listImages.setImageResource(R.drawable.profile);
            listImages.setColorFilter(_context.getResources().getColor(R.color.black));*/
           // listImages.setColorFilter(null);
            Logger.Error("<<<< User Profile Image URL >>> " + imageUrl);

            Glide.with(_context).load(imageUrl).asBitmap().diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).dontAnimate().centerCrop().into(new BitmapImageViewTarget(listImages){
                @Override
                protected void setResource(Bitmap resource){
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(_context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    listImages.setImageDrawable(circularBitmapDrawable);
                }
            });
            ivRightArrow.setVisibility(View.GONE);
            tvRight.setVisibility(View.VISIBLE);
            tvRight.setText(roleName);
            txtListItem.setText(logginusername);
        } else if(headerTitle.equalsIgnoreCase("Logout")){
            listImages.setImageResource(R.drawable.logout);
         //   listImages.setColorFilter(_context.getResources().getColor(R.color.black));
            ivRightArrow.setVisibility(View.GONE);
            tvRight.setVisibility(View.VISIBLE);
            try {
                PackageInfo pInfo = _context.getPackageManager().getPackageInfo(_context.getPackageName(), 0);
                String version = pInfo.versionName;
                tvRight.setText("IN-Gauge Ver." + " " + version);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                tvRight.setText("");
            }
        }
        /*switch (groupPosition) {
            case 0:
                listImages.setImageResource(R.drawable.incentives);
                listImages.setColorFilter(_context.getResources().getColor(R.color.tint_gray_color));
              //  txtListItem.setTextColor(Color.LTGRAY);
                break;
            case 1:
                listImages.setImageResource(R.drawable.goals);
//                         txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 2:
                listImages.setImageResource(R.drawable.profile);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 3:
                listImages.setImageResource(R.drawable.email);
                //txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 4:
                listImages.setImageResource(R.drawable.ic_obs_cc_create);
                // txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 5:
                listImages.setImageResource(R.drawable.ic_obs_cc_create);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 6:
                listImages.setImageResource(R.drawable.ic_notification);
                //txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(_context.getResources().getColor(R.color.tint_gray_color));
                break;
            case 7:
                listImages.setImageResource(R.drawable.preferences);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 8:
                listImages.setImageResource(R.drawable.logout);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            default:
                break;
        }*/
        /*switch (groupPosition) {
            case 0:
                listImages.setImageResource(R.drawable.incentives);
                listImages.setColorFilter(_context.getResources().getColor(R.color.tint_gray_color));
                //  txtListItem.setTextColor(Color.LTGRAY);
                break;
            case 1:
                listImages.setImageResource(R.drawable.goals);
//                         txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 2:
                listImages.setImageResource(R.drawable.profile);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 3:
                listImages.setImageResource(R.drawable.email);
                //txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 4:
                listImages.setImageResource(R.drawable.ic_obs_cc_create);
                // txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 5:
                listImages.setImageResource(R.drawable.ic_obs_cc_create);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 6:
                listImages.setImageResource(R.drawable.ic_notification);
                //txtListItem.setTextColor(Color.LTGRAY);
                listImages.setColorFilter(_context.getResources().getColor(R.color.tint_gray_color));
                break;
            case 7:
                listImages.setImageResource(R.drawable.preferences);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            case 8:
                listImages.setImageResource(R.drawable.logout);
                listImages.setColorFilter(_context.getResources().getColor(R.color.black));
                break;
            default:
                break;
        }*/
        return convertView;
    }

    @Override
    public boolean hasStableIds(){
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition){
        return true;
    }
}
