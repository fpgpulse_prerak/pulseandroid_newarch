package com.ingauge.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.FilterBaseData;

import java.util.List;


public class FilterListForObservation extends RecyclerView.Adapter<FilterListForObservation.FilterViewHolder> {
    Activity context;
    List<FilterBaseData> mBaseDataList;
    private static RecyclerViewClickListener itemListener;
    private boolean isFromDetails = false;


    public FilterListForObservation(Activity context, List<FilterBaseData> mBaseDataList, RecyclerViewClickListener itemListener, boolean isFromDetails) {
        this.context = context;
        this.mBaseDataList = mBaseDataList;
        this.itemListener = itemListener;
        this.isFromDetails = isFromDetails;
    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_filter_item_list_obs_cc, parent, false);
        return new FilterListForObservation.FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        holder.tvTitle.setText(mBaseDataList.get(position).filterName);
        holder.tvFilterValue.setText(mBaseDataList.get(position).name);

        if(TextUtils.isEmpty(mBaseDataList.get(position).name)){
            holder.tvFilterValue.setVisibility(View.GONE);
        }else{
            holder.tvFilterValue.setVisibility(View.VISIBLE);
        }

        if(mBaseDataList.get(position).isEditable==1) {
            holder.ivRight.setVisibility(View.VISIBLE);
        }else{
            holder.ivRight.setVisibility(View.GONE);
        }

        holder.tvFilterValue.setVisibility(View.VISIBLE);


    }

    @Override
    public int getItemCount() {
        return mBaseDataList.size();
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        private TextView tvFilterValue;
        public ImageView ivRight;

        public FilterViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.row_item_filter_list_tv_name);
            ivRight = (ImageView) view.findViewById(R.id.row_item_filter_iv);
            tvFilterValue = (TextView)view.findViewById(R.id.tv_filter_list_select_filter_name);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mBaseDataList!=null)
            itemListener.recyclerViewListClicked(v, mBaseDataList.get(getLayoutPosition()),getLayoutPosition(),mBaseDataList.get(getLayoutPosition()).filterIndex,false);
        }
    }
}