package com.ingauge.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ingauge.R;
import com.ingauge.pojo.AttachmentModel;
import com.ingauge.pojo.InboxModel;

import java.util.LinkedHashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mansurum on 13-Jul-17.
 */

public class MailDetailsExpandableAdapter extends BaseExpandableListAdapter {


    private Context mContext;
    private Fragment mFragment;
    private List<InboxModel> _listDataHeader;
   // private LinkedHashMap<InboxModel, String> _listDataChild;
   private AttachmentAdapter adapter;
    public MailDetailsExpandableAdapter(Context mContext, Fragment mFragment, List<InboxModel> _listDataHeader, LinkedHashMap<InboxModel, String> _listDataChild){
        this.mContext = mContext;
        this.mFragment = mFragment;
        this._listDataHeader = _listDataHeader;
     //   this._listDataChild =  _listDataChild;
    }

    @Override
    public int getGroupCount() {
        return _listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return _listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return _listDataHeader.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ViewHolder h;
        View v = convertView;
        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.row_email_detail_expand, null);
            h = new ViewHolder();

            h.tvMailSortBody= (TextView) v.findViewById(R.id.row_email_inbox_tv_short_desc);
            h.tvSender= (TextView) v.findViewById(R.id.row_email_inbox_tv_name_from);
            h.tvSubject= (TextView) v.findViewById(R.id.row_email_inbox_tv_subject);
            h.tvSentTime= (TextView) v.findViewById(R.id.row_email_inbox_tv_date);
            h.tvReceive= (TextView) v.findViewById(R.id.row_email_inbox_tv_name_to);
            //h.tv_to.setVisibility(View.VISIBLE);
            h.mCircleImageView= (CircleImageView) v.findViewById(R.id.row_email_inbox_iv_user_image);

            // mImageLoader.get(arraySubMail.get(groupPosition).getProfileUrl(), ImageLoader.getImageListener(h.img_avatar, R.drawable.loading, R.drawable.loading));
            v.setTag(h);
        }else{
            h = (ViewHolder) v.getTag();
        }
        h.tvSender.setText(_listDataHeader.get(groupPosition).getSender());
      //  h.tvMailSortBody.setText(Html.fromHtml(_listDataHeader.get(groupPosition).getMessageSort()));

       /* if(_listDataHeader.get(groupPosition).isExpanded()){
            h.tvMailSortBody.setVisibility(View.GONE);
        }else{
            h.tvMailSortBody.setVisibility(View.VISIBLE);
        }*/
        Glide.with(mContext)
                .load(_listDataHeader.get(groupPosition).getProfileUrl())
                .centerCrop()
                .placeholder(R.drawable.defaultimg)
                .error(R.drawable.defaultimg)
                .into(h.mCircleImageView);
        h.tvSubject.setText(_listDataHeader.get(groupPosition).getSubject());

        h.tvSentTime.setText(_listDataHeader.get(groupPosition).getSentDate());
        h.tvReceive.setText(_listDataHeader.get(groupPosition).getToName());

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater infalInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = infalInflater.inflate(R.layout.row_mailbody_child, null);
        }
        TextView tvMailBody = (TextView) convertView.findViewById(R.id.row_mail_body_child_tv_message);
        RecyclerView rvAttachments = (RecyclerView)convertView.findViewById(R.id.rv_attachments);

        if(_listDataHeader.get(groupPosition).getAttachmentModelList()!=null && _listDataHeader.get(groupPosition).getAttachmentModelList().size()>0){
            rvAttachments.setVisibility(View.VISIBLE);
            rvAttachments.setLayoutManager(new GridLayoutManager(mContext, 3));
            adapter = new AttachmentAdapter(mContext, _listDataHeader.get(groupPosition).getAttachmentModelList(), false, _listDataHeader.get(groupPosition).getID());
            rvAttachments.setAdapter(adapter);
        }else{
            rvAttachments.setVisibility(View.GONE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvMailBody.setText(Html.fromHtml(_listDataHeader.get(groupPosition).getMessageSort(), Html.FROM_HTML_MODE_LEGACY));
        }else{
            tvMailBody.setText(Html.fromHtml(_listDataHeader.get(groupPosition).getMessageSort()));
        }

        List<AttachmentModel> attachmentModelList =_listDataHeader.get(groupPosition).getAttachmentModelList();
       // tvMailBody.setText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private class ViewHolder {

        TextView tvMailSortBody;
        TextView tvSender;
        TextView tvSubject;
        TextView tvSentTime;
        TextView tvReceive;
        CircleImageView mCircleImageView;
        RecyclerView rvAttachments;

    }
}
