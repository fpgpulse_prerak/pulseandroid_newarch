package com.ingauge.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.listener.RecyclerViewClickListenerForDashboardDetails;
import com.ingauge.pojo.CustomDashboardModel;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by mansurum on 28-Aug-17.
 */

public class DashboardPanelAdapter extends RecyclerView.Adapter<DashboardPanelAdapter.DashboardPanelViewHolder>{


    private Context mContext;
    private List<CustomDashboardModel.CustomReport> mList;
    private static RecyclerViewClickListener itemListener;
    private RecyclerViewClickListenerForDashboardDetails dashboardListener;
    private static String FONT_AWESOME_HEX_PREFIX = "x";


    public DashboardPanelAdapter(Context mContext, List<CustomDashboardModel.CustomReport> list, RecyclerViewClickListenerForDashboardDetails dashboardListener){
        this.mContext = mContext;
        this.mList = list;
        this.dashboardListener = dashboardListener;
    }


    @Override
    public DashboardPanelViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dashboard_panel_fragment, parent, false);
        return new DashboardPanelAdapter.DashboardPanelViewHolder(itemView);
    }

    static String stringToHex(String string){
        StringBuilder buf = new StringBuilder(200);
        for(char ch : string.toCharArray()){
            if(buf.length() > 0)
                buf.append(' ');
            buf.append(String.format("%04x", (int) ch));
        }
        return buf.toString();
    }

    @Override
    public void onBindViewHolder(DashboardPanelViewHolder holder, int position){

        CustomDashboardModel.CustomReport mCustomReport = this.mList.get(position);
        if(mCustomReport != null && mCustomReport.getReportType() != null){
            holder.llMainReport.setVisibility(View.VISIBLE);
            holder.llReportError.setVisibility(View.GONE);
            if(mCustomReport.getReportType().equalsIgnoreCase("Block") || mCustomReport.getReportType().equalsIgnoreCase("Chart")
                    || mCustomReport.getReportType().equalsIgnoreCase("Table")){
                holder.llGraph.setVisibility(View.GONE);
                holder.tvBlockValue.setVisibility(View.VISIBLE);
                holder.tvReportName.setVisibility(View.VISIBLE);
                if(mCustomReport.isQueryBasedReport()){
                    holder.ivPreConfigBlock.setVisibility(View.VISIBLE);
                } else{
                    holder.ivPreConfigBlock.setVisibility(View.INVISIBLE);
                }

                String blockValue = UiUtils.getCurrencySymbol(mCustomReport.getPrefix()) + " " + TwoDigitDecimal(String.valueOf(mCustomReport.getValue())) + mCustomReport.getSuffix();
                String blockCompareValue = UiUtils.getCurrencySymbol(mCustomReport.getPrefix()) + " " + TwoDigitDecimal(String.valueOf(mCustomReport.getDataCompareValue())) + mCustomReport.getSuffix();
                //Logger.Error("Block Value" + UiUtils.getCurrencySymbol(mCustomReport.getPrefix()) + " " + TwoDigitDecimal(String.valueOf(mCustomReport.getValue())) + " " + mCustomReport.getSuffix());
                holder.tvReportName.setText(mCustomReport.getReportName());
                holder.tvBlockReportIcon.setVisibility(View.VISIBLE);
                String iconClassStr = mCustomReport.getIconClass();
                if(iconClassStr != null && iconClassStr.length() > 0){
                    String fab = "fab";
                    String fal = "fal";
                    String far = "far";
                    String fas = "fas";
                    Typeface mTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/fontawesome-webfont_pm.ttf");
                    Logger.Error("<<<<<     Font Class >>>> " +iconClassStr.split(" ")[0]);
                    switch(iconClassStr.split(" ")[0]){

                        case "fab":
                            mTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/fab.ttf");
                            break;
                        case "fal":
                            mTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/fal.ttf");
                            break;
                        case "far":
                            mTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/far.ttf");
                            break;
                        case "fas":
                            mTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/fas.ttf");
                            break;
                    }

                    holder.tvBlockReportIcon.setTypeface(mTypeface);
                } else{
                    Typeface mTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/fontpro.otf");
//                Typeface mTypeface = Typeface.createFromAsset(mContext.getAssets(), "fa-regular.ttf");
                    holder.tvBlockReportIcon.setTypeface(mTypeface);
                }


                if(mCustomReport.getReportType().equalsIgnoreCase("Table")){
                    holder.tvBlockReportIcon.setText(String.valueOf((char) 0xf0ce));
                    holder.tvBlockReportIcon.setTextColor(Color.parseColor("#00BCD4"));
                } else{
                    if(mCustomReport.getIconHexcode() != null && mCustomReport.getIconHexcode().length() > 0){
                        //String hexStr = FONT_AWESOME_HEX_PREFIX.toString() + mCustomReport.getIconHexcode() + ";";
                        Logger.Error("<<<< Icon Hex Code : " + mCustomReport.getIconHexcode() + " <<  Position  " + position);
                        String hexStr = "0x" + mCustomReport.getIconHexcode();
                        int hexInt = Long.decode(hexStr).intValue();
                        Logger.Error("<<<< Icon Hex Code : " + hexInt + " <<  Position  " + position);
                        holder.tvBlockReportIcon.setText(String.valueOf((char) hexInt));
                    } else{
                        holder.tvBlockReportIcon.setText(String.valueOf((char) 0xf080));
                    }

                    if(mCustomReport.getIconColor() != null && mCustomReport.getIconColor().length() > 0){

                        Logger.Error("<<<<<< POSITION  Report Details" + position);
                        try{
                            String colrhexCode = mCustomReport.getIconColor();
                            holder.tvBlockReportIcon.setTextColor(Color.parseColor(mCustomReport.getIconColor()));
                        } catch(Exception e){
                            e.printStackTrace();
                        }
                    } else{
                        holder.tvBlockReportIcon.setTextColor(Color.parseColor("#00BCD4"));
                    }
                }


                holder.tvUserNoUse.setVisibility(View.VISIBLE);
                holder.tvUser.setText(mCustomReport.getEntityName());


               /* if(InGaugeSession.read(mContext.getResources().getString(R.string.key_is_compare_on), true)){
                    holder.tvBlockValue.setVisibility(View.VISIBLE);
                    holder.tvBlockCompareValue.setVisibility(View.GONE);
                    holder.tvUserNoUse.setVisibility(View.GONE);
                    holder.tvUserNoUse.setText(mCustomReport.getCompEntityName() + "(Compare)");
                    Spannable upSpan = new SpannableString("↑");
                    Spannable downSpan = new SpannableString("↓");
                    upSpan.setSpan(new ForegroundColorSpan(Color.GREEN), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    downSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    *//*if(mCustomReport.getValue() > mCustomReport.getDataCompareValue()){
                        holder.tvBlockValue.setText(blockValue + "  " + upSpan);
                        holder.tvBlockCompareValue.setTextColor(Color.RED);
                        holder.tvBlockValue.setTextColor(mContext.getColor(R.color.in_guage_blue));;

                    } else{
                        holder.tvBlockValue.setText(blockValue + "  " + downSpan);
                        holder.tvBlockCompareValue.setTextColor(Color.GREEN);
                        holder.tvBlockValue.setTextColor(mContext.getColor(R.color.in_guage_blue));
                    }*//*
                    holder.tvBlockValue.setText(blockValue);
                    holder.tvBlockCompareValue.setText(blockCompareValue);

                } else{
                    holder.tvUserNoUse.setVisibility(View.GONE);
                    holder.tvBlockValue.setVisibility(View.VISIBLE);
                    holder.tvBlockCompareValue.setVisibility(View.GONE);
                    holder.tvBlockValue.setText(blockValue);
                    holder.tvBlockValue.setTextColor(mContext.getColor(R.color.in_guage_blue));
                }*/
                if(mCustomReport.getReportType().equalsIgnoreCase("Chart")
                        || mCustomReport.getReportType().equalsIgnoreCase("Table")){
                    holder.tvBlockValue.setVisibility(View.GONE);
                } else{
                    holder.tvBlockValue.setVisibility(View.VISIBLE);
                }
                holder.tvBlockValue.setText(blockValue);
                holder.tvBlockCompareValue.setText(blockCompareValue);
                holder.llBlockValues.setVisibility(View.VISIBLE);
                //holder.tvUserNoUse.setText(mCustomReport.getUsername());
            } /*else{
                holder.llGraph.setVisibility(View.VISIBLE);
                holder.tvBlockValue.setVisibility(View.GONE);
                holder.llBlockValues.setVisibility(View.GONE);
                holder.tvBlockCompareValue.setVisibility(View.GONE);
                holder.tvBlockReportIcon.setVisibility(View.GONE);
                holder.tvUserNoUse.setVisibility(View.GONE);
                holder.tvGraphReportName.setText(mCustomReport.getReportName());
                holder.tvReportName.setVisibility(View.GONE);

                *//*if(mCustomReport.getChartType().equalsIgnoreCase("column") || mCustomReport.getChartType().equalsIgnoreCase("Chart")){
                    holder.ivGraphIcon.setImageResource(R.drawable.ic_bar_chart);
                } else if(mCustomReport.getChartType().equalsIgnoreCase("table")){
                    holder.ivGraphIcon.setImageResource(R.drawable.ic_table);
                } else if(mCustomReport.getChartType().equalsIgnoreCase("pie")){
                    holder.ivGraphIcon.setImageResource(R.drawable.ic_pie_chart);
                } else if(mCustomReport.getChartType().equalsIgnoreCase("bubble")){
                    holder.ivGraphIcon.setImageResource(R.drawable.ic_bubble_chart);
                } else if(mCustomReport.getChartType().equalsIgnoreCase("spline") ||
                        mCustomReport.getChartType().equalsIgnoreCase("areaspline")){
                    holder.ivGraphIcon.setImageResource(R.drawable.ic_spline_chart);
                }*//*

                if(mCustomReport.getChartType().equalsIgnoreCase("table")){
                    holder.ivGraphIcon.setImageResource(R.drawable.ic_table);
                }

            }*/
        } else if(mCustomReport.getIsError()){
            holder.llMainReport.setVisibility(View.GONE);
            holder.llReportError.setVisibility(View.VISIBLE);
        }

    }

    public String TwoDigitDecimal(String value){

        if(value.trim().length() > 0){
            NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
            return nf.format(Double.parseDouble(String.format(Locale.US, "%.2f", Double.parseDouble(value))));
        } else{

            return "0.00";
        }
    }

    @Override
    public int getItemCount(){
        return mList.size();
    }

    public class DashboardPanelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tvBlockValue, tvGraphReportName, tvReportName, tvBlockReportIcon, tvUserNoUse, tvBlockCompareValue, tvUser;
        public ImageView ivGraphIcon, ivPreConfigBlock;
        public LinearLayout llGraph, llBlockValues, llMainReport, llReportError;

        public DashboardPanelViewHolder(View view){
            super(view);
            tvBlockValue = (TextView) view.findViewById(R.id.tv_row_dashboard_panel_fragment_block_value);
            tvGraphReportName = (TextView) view.findViewById(R.id.row_dashboard_panel_tv_graph_report_name);
            tvReportName = (TextView) view.findViewById(R.id.row_dashboard_panel_fragment_tv_report_name);
            tvBlockReportIcon = (TextView) view.findViewById(R.id.row_dashboard_panel_fragment_tv_table_headers_icons);
            tvUserNoUse = (TextView) view.findViewById(R.id.row_dashboard_panel_tv_user_name);
            ivGraphIcon = (ImageView) view.findViewById(R.id.iv_icon_value);
            llGraph = (LinearLayout) view.findViewById(R.id.fragment_dashboard_panel_ll_graph);
            llBlockValues = (LinearLayout) view.findViewById(R.id.ll_bottom_block);
            tvBlockCompareValue = (TextView) view.findViewById(R.id.tv_row_dashboard_panel_fragment_block_compare_value);
            llMainReport = (LinearLayout) view.findViewById(R.id.row_dashboard_panel_fragment_ll_main_report);
            llReportError = (LinearLayout) view.findViewById(R.id.row_dashboard_panel_fragment_ll_report_error);
            tvUser = (TextView) view.findViewById(R.id.row_dashboard_panel_tv_user_name_compare);
            ivPreConfigBlock = (ImageView) view.findViewById(R.id.row_dashboard_panel_fragment_iv_block_configured_report);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            if(llMainReport.getVisibility() == View.VISIBLE){
                if(ivPreConfigBlock.getVisibility() == View.INVISIBLE){
                    dashboardListener.recyclerViewListClicked(v, getLayoutPosition());
                }

            }
        }
    }
}
