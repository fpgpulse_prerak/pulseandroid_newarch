package com.ingauge.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.pojo.GoalCurrentMonthMap;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.UiUtils;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mansurum on 16-Jun-17.
 */

public class GoalProgressExpandAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<String>> expandableListDetail;
    private List<String> expandableGoalDetail;
    private List<String> expandableRevenueDetail;
    private List<String> expandableEffortDetail;
    private List<GoalCurrentMonthMap> productGoalDetails;
    private List<String> expandableWeeklyGoalDetails;

    public GoalProgressExpandAdapter(Context context, List<String> expandableListTitle, HashMap<String, List<String>> expandableListDetail, List<String> expandableGoalDetails, List<String> expandableRevenueDetail, List<String> expandableEffortDetail, List<GoalCurrentMonthMap> productGoalDetails, List<String> expandableWeeklyGoalDetails) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
        this.expandableGoalDetail = expandableGoalDetails;
        this.expandableRevenueDetail = expandableRevenueDetail;
        this.expandableEffortDetail = expandableEffortDetail;
        this.productGoalDetails = productGoalDetails;
        this.expandableWeeklyGoalDetails = expandableWeeklyGoalDetails;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }


    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(listPosition, expandedListPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.goal_progress_child_item_title);
        expandedListTextView.setText(expandedListText);

        TextView expandedItemValueTextView = (TextView) convertView.findViewById(R.id.goal_progress_child_item_value);

        RelativeLayout.LayoutParams layoutParams;
        layoutParams =
                (RelativeLayout.LayoutParams) expandedListTextView.getLayoutParams();
        switch (listPosition) {
            case 0:

                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
                expandedListTextView.setLayoutParams(layoutParams);
                expandedItemValueTextView.setText(expandableWeeklyGoalDetails.get(expandedListPosition));
                break;
            case 1:
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
                expandedListTextView.setLayoutParams(layoutParams);
                expandedItemValueTextView.setText(expandableGoalDetail.get(expandedListPosition));
                break;
            case 2:
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
                expandedListTextView.setLayoutParams(layoutParams);
                expandedItemValueTextView.setText(expandableRevenueDetail.get(expandedListPosition));
                break;
            case 3:
                if (productGoalDetails.size() > 0) {
                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
                    expandedListTextView.setLayoutParams(layoutParams);

                    if (productGoalDetails.get(expandedListPosition).isAchieved()) {
                        expandedItemValueTextView.setText(UiUtils.getCurrencySymbol(InGaugeSession.read(context.getResources().getString(R.string.key_region_currency), "USD")) + String.valueOf(productGoalDetails.get(expandedListPosition).getFpgLevel()) + "  " + " achieved");
                    } else {
                        expandedItemValueTextView.setText(UiUtils.getCurrencySymbol(InGaugeSession.read(context.getResources().getString(R.string.key_region_currency), "USD")) + String.valueOf(productGoalDetails.get(expandedListPosition).getFpgLevel()) + "  " + " Not achieved");
                    }

                } else {
                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                    expandedListTextView.setLayoutParams(layoutParams);
                    expandedItemValueTextView.setText("-");
                }
                break;
            case 4:
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
                expandedListTextView.setLayoutParams(layoutParams);
                if (expandableEffortDetail.get(expandedListPosition).length() > 0) {
                    expandedItemValueTextView.setText(expandableEffortDetail.get(expandedListPosition));
                } else {
                    expandedItemValueTextView.setText("-");
                }

                break;

            default:
                break;
        }


        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
