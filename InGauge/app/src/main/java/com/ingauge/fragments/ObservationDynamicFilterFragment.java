package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.DynamicFilterDetailListAdapterForObservation;
import com.ingauge.adapters.DynamicFilterLeftAdapter;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ObservationDashboardStatusModel;
import com.ingauge.pojo.ObservationSurveyCategoryModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 10-Dec-17.
 */

public class ObservationDynamicFilterFragment extends BaseFragment implements View.OnClickListener, RecyclerViewClickListener {

    private Context mContext;
    private HomeActivity mHomeActivity;
    private View rootView;
    private RecyclerView rvFilterList;
    private RecyclerView rvFilterLeftList;
    public static SearchView mSearchView;
    private TextView tvFilterClear;
    private TextView tvApply;


    ArrayList<FilterBaseData> mLeftFilterBaseDatas;
    ArrayList<FilterBaseData> mRightFilterBaseDatas;


    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModelForObserveAgent;
    private ObservationSurveyCategoryModel observationSurveyCategoryModel;
    private ArrayList<ObservationDashboardStatusModel> observationDashboardStatusModelArrayList;

    public ArrayList<FilterBaseData> mFilterBaseDatasforObsFilter = new ArrayList<>();


    DynamicFilterDetailListAdapterForObservation mFilterRightListAdapter;
    DynamicFilterLeftAdapter mDynamicFilterLeftAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLeftFilterBaseDatas = new ArrayList<>();
        mRightFilterBaseDatas = new ArrayList<>();

        if (getArguments() != null) {
            mAccessibleTenantLocationDataModelForObserveAgent = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_ONE);
            observationSurveyCategoryModel = (ObservationSurveyCategoryModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_TWO);
            observationDashboardStatusModelArrayList = (ArrayList) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_THREE);

            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 0;
            mFilterBaseData.filterName = "Location";
            mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_id), null);
            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_name), null);
            mFilterBaseData.isSelectedPosition = true;
            mFilterBaseDatasforObsFilter.add(mFilterBaseData);

            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 1;
            mFilterBaseData.filterName = "Category";
            mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), null));
            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_name), null);
            mFilterBaseData.isSelectedPosition = false;
            mFilterBaseDatasforObsFilter.add(mFilterBaseData);

            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 2;
            mFilterBaseData.filterName = "Status";
            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), null) == null) {
                mFilterBaseData.id = String.valueOf(observationDashboardStatusModelArrayList.get(0).getStatusID());
                mFilterBaseData.name = observationDashboardStatusModelArrayList.get(0).getStatusName();
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_name), String.valueOf(observationDashboardStatusModelArrayList.get(0).getStatusName()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), String.valueOf(observationDashboardStatusModelArrayList.get(0).getStatusID()));
            } else {
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), null);
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_name), null);
            }
            mFilterBaseData.isSelectedPosition = false;
            mFilterBaseDatasforObsFilter.add(mFilterBaseData);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dynamic_dashboard_filter, container, false);
        initView(rootView);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mFilterRightListAdapter.getFilter().filter(newText);
                return false;
            }
        });
        setLeftFilterAdapter(mFilterBaseDatasforObsFilter);
        sendListData(0, mFilterBaseDatasforObsFilter.get(0).id, mFilterBaseDatasforObsFilter.get(0).name);
        return rootView;
    }

    public void initView(View itemView) {
        tvFilterClear = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_clear_all);
        tvApply = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_apply);
        rvFilterLeftList = (RecyclerView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_rv_left);
        rvFilterList = (RecyclerView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_rv);
        mSearchView = (SearchView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_search_view);
        tvFilterClear.setOnClickListener(this);
        tvApply.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_dynamic_dashboard_filter_tv_clear_all:
                getInitialLeft();
                break;
            case R.id.fragment_dynamic_dashboard_filter_tv_apply:
                updateOnApply();
                break;
        }

    }

    public void updateOnApply() {
        String tenantLocationId = null;
        String tenantLocationName = null;
        String surveyCategoryId = null;
        String surveyCategoryName = null;
        String statusId = null;
        String statusName = null;
        tenantLocationId = String.valueOf(mFilterBaseDatasforObsFilter.get(0).id);
        tenantLocationName = String.valueOf(mFilterBaseDatasforObsFilter.get(0).name);
        surveyCategoryId = String.valueOf(mFilterBaseDatasforObsFilter.get(1).id);
        surveyCategoryName = String.valueOf(mFilterBaseDatasforObsFilter.get(1).name);

        statusId = String.valueOf(mFilterBaseDatasforObsFilter.get(2).id);
        statusName = String.valueOf(mFilterBaseDatasforObsFilter.get(2).name);


        //Location Prefernce
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_id), tenantLocationId);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_name), tenantLocationName);

        //Survey Cateogry Preference
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), surveyCategoryId);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_name), surveyCategoryName);

        //Status Preference
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), statusId);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_name), statusName);
        mHomeActivity.setDashboardObsCoachingUpdateAfterApply(true);
        mHomeActivity.onBackPressed();

    }

    /*public void ClearUpdate() {

    }
*/
    private void setLeftFilterAdapter(List<FilterBaseData> mFilterBaseDatasforObsFilter) {
        if (mFilterBaseDatasforObsFilter != null) {
            if (mFilterBaseDatasforObsFilter.size() > 0) {
                mDynamicFilterLeftAdapter = new DynamicFilterLeftAdapter(mHomeActivity, mFilterBaseDatasforObsFilter, this, true, 0);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterLeftList.setLayoutManager(mLayoutManager);
                rvFilterLeftList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterLeftList.setItemAnimator(new DefaultItemAnimator());
                rvFilterLeftList.setAdapter(mDynamicFilterLeftAdapter);
            }
        }
    }


    private void getObservationDashboardStatus(ArrayList<ObservationDashboardStatusModel> observationDashboardStatusModelArrayList, String selectedId, String selectedName) {
        if (mRightFilterBaseDatas != null && mRightFilterBaseDatas.size() > 0) {
            mRightFilterBaseDatas.clear();
        }
        if (observationDashboardStatusModelArrayList != null) {
            for (int i = 0; i < observationDashboardStatusModelArrayList.size(); i++) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(observationDashboardStatusModelArrayList.get(i).getStatusID());
                mFilterBaseData.name = observationDashboardStatusModelArrayList.get(i).getStatusName();
                mFilterBaseData.filterName = "Status";
                mFilterBaseData.filterIndex = 2;
                if (i == 0) {
                    mFilterBaseData.isSelectedPosition = true;
                }
                mRightFilterBaseDatas.add(mFilterBaseData);
            }

            int selectedPosiotn = 0;
            for (FilterBaseData mFilterBaseData : mRightFilterBaseDatas) {
                if (mFilterBaseData.isSelectedPosition) {
                    selectedPosiotn = mRightFilterBaseDatas.indexOf(mFilterBaseData);
                    mRightFilterBaseDatas.remove(selectedPosiotn);
                    mRightFilterBaseDatas.add(0, mFilterBaseData);

                    break;
                }
            }
            ArrayList<FilterBaseData> mRightListValues = new ArrayList<>();
            for (int i = 0; i < mRightFilterBaseDatas.size(); i++) {
                FilterBaseData mFilterRightData = new FilterBaseData();
                if (selectedId.length() > 0 && selectedId.equalsIgnoreCase(mRightFilterBaseDatas.get(i).id)) {
                    mFilterRightData = mRightFilterBaseDatas.get(i);
                    mFilterRightData.isSelectedPosition = true;
                } else {
                    mFilterRightData = mRightFilterBaseDatas.get(i);
                    mFilterRightData.isSelectedPosition = false;
                }
                mRightListValues.add(mFilterRightData);
            }

            setFilterdataListAdapterForProduct(mRightListValues, selectedId, selectedName);
        }
    }

    private void getObservationSurveyCategory(ObservationSurveyCategoryModel observationSurveyCategoryModel, String selectedId, String selectedName) {
        if (mRightFilterBaseDatas != null && mRightFilterBaseDatas.size() > 0) {
            mRightFilterBaseDatas.clear();
        }
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "-1";
        mFilterBaseData.name = "All Categories";
        mFilterBaseData.filterName = "Category";
        mFilterBaseData.filterIndex = 1;
        mFilterBaseData.isSelectedPosition = true;
        mRightFilterBaseDatas.add(mFilterBaseData);

        if (observationSurveyCategoryModel != null) {
            for (ObservationSurveyCategoryModel.Datum mDatum : observationSurveyCategoryModel.getData()) {
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterName = "Category";
                mFilterBaseData.filterIndex = 1;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }

            ArrayList<FilterBaseData> mRightListValues = new ArrayList<>();
            for (int i = 0; i < mRightFilterBaseDatas.size(); i++) {
                FilterBaseData mFilterRightData = new FilterBaseData();
                if (selectedId.length() > 0 && selectedId.equalsIgnoreCase(mRightFilterBaseDatas.get(i).id)) {
                    mFilterRightData = mRightFilterBaseDatas.get(i);
                    mFilterRightData.isSelectedPosition = true;
                } else {
                    mFilterRightData = mRightFilterBaseDatas.get(i);
                    mFilterRightData.isSelectedPosition = false;
                }
                mRightListValues.add(mFilterRightData);
            }

            setFilterdataListAdapterForProduct(mRightListValues, selectedId, selectedName);
        }
    }

    private void getAccessibleTenantLocation(AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel, String selectedId, String selectedName) {

        if (mRightFilterBaseDatas != null && mRightFilterBaseDatas.size() > 0) {
            mRightFilterBaseDatas.clear();
        }
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "-1";
        mFilterBaseData.name = "All Locations";
        mFilterBaseData.filterName = getString(R.string.location);
        mFilterBaseData.filterIndex = 0;
        mFilterBaseData.isSelectedPosition = true;
        mRightFilterBaseDatas.add(mFilterBaseData);

        if (mAccessibleTenantLocationDataModel != null) {
            for (AccessibleTenantLocationDataModel.Datum mDatum : mAccessibleTenantLocationDataModel.getData()) {
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterIndex = 0;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }

            ArrayList<FilterBaseData> mRightListValues = new ArrayList<>();
            for (int i = 0; i < mRightFilterBaseDatas.size(); i++) {
                FilterBaseData mFilterRightData = new FilterBaseData();
                if (selectedId.length() > 0 && selectedId.equalsIgnoreCase(mRightFilterBaseDatas.get(i).id)) {
                    mFilterRightData = mRightFilterBaseDatas.get(i);
                    mFilterRightData.isSelectedPosition = true;
                } else {
                    mFilterRightData = mRightFilterBaseDatas.get(i);
                    mFilterRightData.isSelectedPosition = false;
                }
                mRightListValues.add(mFilterRightData);
            }

            setFilterdataListAdapterForProduct(mRightListValues, selectedId, selectedName);
        }
    }


    private void setFilterdataListAdapterForProduct(List<FilterBaseData> mFilterBaseDataList, String selectedid, String selectedname) {
        if (mFilterBaseDataList != null) {
            if (mFilterBaseDataList.size() > 0) {
                int selectedPosiotn = 0;
                for (FilterBaseData mFilterBaseData : mFilterBaseDataList) {
                    if (mFilterBaseData.isSelectedPosition) {
                        selectedPosiotn = mFilterBaseDataList.indexOf(mFilterBaseData);
                        mFilterBaseDataList.remove(selectedPosiotn);
                        mFilterBaseDataList.add(0, mFilterBaseData);

                        break;
                    }
                }
                mFilterRightListAdapter = new DynamicFilterDetailListAdapterForObservation(mHomeActivity, mFilterBaseDataList, this, true, selectedid, selectedname);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                rvFilterList.setAdapter(mFilterRightListAdapter);
            }
        }
    }


    public void sendListData(int position, String selectedId, String selectedName) {
        switch (position) {
            case 0:
                getAccessibleTenantLocation(mAccessibleTenantLocationDataModelForObserveAgent, selectedId, selectedName);
                break;
            case 1:
                getObservationSurveyCategory(observationSurveyCategoryModel, selectedId, selectedName);
                break;
            case 2:
                getObservationDashboardStatus(observationDashboardStatusModelArrayList, selectedId, selectedName);
                break;
        }

    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterPosition, boolean isLeftFilter) {

        if (isLeftFilter) {
            sendListData(position, mFilterBaseData.id, mFilterBaseData.name);
        } else {
            updateLeftFilterUpdate(mFilterBaseData, filterPosition);
        }
    }

    public void updateLeftFilterUpdate(FilterBaseData mFilterBaseData, int position) {
        mFilterBaseDatasforObsFilter.set(position, mFilterBaseData);
        mDynamicFilterLeftAdapter.notifyDataSetChanged();
    }

    public void getInitialLeft() {
        mFilterBaseDatasforObsFilter.clear();
        mAccessibleTenantLocationDataModelForObserveAgent = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_ONE);
        observationSurveyCategoryModel = (ObservationSurveyCategoryModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_TWO);
        observationDashboardStatusModelArrayList = (ArrayList) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_THREE);

        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.filterIndex = 0;
        mFilterBaseData.filterName = "Location";
        mFilterBaseData.id = String.valueOf(-1);
        mFilterBaseData.name = "All Locations";
        mFilterBaseData.isSelectedPosition=true;

        mFilterBaseDatasforObsFilter.add(mFilterBaseData);

        mFilterBaseData = new FilterBaseData();
        mFilterBaseData.filterIndex = 1;
        mFilterBaseData.filterName = "Category";
        mFilterBaseData.id = String.valueOf(-1);
        mFilterBaseData.name = "All Categories";
        mFilterBaseData.isSelectedPosition=false;
        mFilterBaseDatasforObsFilter.add(mFilterBaseData);

        mFilterBaseData = new FilterBaseData();
        mFilterBaseData.filterIndex = 2;
        mFilterBaseData.filterName = "Status";
        mFilterBaseData.isSelectedPosition=false;
        mFilterBaseData.id = String.valueOf(observationDashboardStatusModelArrayList.get(0).getStatusID());
        mFilterBaseData.name = observationDashboardStatusModelArrayList.get(0).getStatusName();
        mFilterBaseDatasforObsFilter.add(mFilterBaseData);
        setLeftFilterAdapter(mFilterBaseDatasforObsFilter);
        getAccessibleTenantLocation(mAccessibleTenantLocationDataModelForObserveAgent, mFilterBaseDatasforObsFilter.get(0).id, mFilterBaseDatasforObsFilter.get(0).name);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        mHomeActivity.tvTitle.setText("Filter");
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
    }
}
