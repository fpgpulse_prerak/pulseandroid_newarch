package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.ingauge.R;
import com.ingauge.activities.BaseActivity;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.Listadapter;
import com.ingauge.adapters.PreferenceListadapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.IndustryDataModel;
import com.ingauge.pojo.TenantDataModel;
import com.ingauge.pojo.UserData;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by desainid on 5/16/2017.
 */

public class PreferencesFragment extends BaseFragment {
    Context mContext;
    List<String> values;
    ArrayList<String> tenantValues;
    List<String> description;
    ListView listview;
    BaseActivity mBaseActivity;
    PreferenceListadapter listadapter;
    private HomeActivity mHomeActivity;
    APIInterface apiInterface;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_preferences, container,false);
        initViews(rootView);
        Bundle arg = getArguments();
        if (arg != null) {
            description.add(arg.getString("selectedValue"));
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Object pos = listview.getItemAtPosition(position);
                String str = (String) pos;
                if (position == 0) {
                    InGaugeSession.write(getString(R.string.fragment_preference_detail), "Industry");
                    PreferenceDetailFragment fragment = new PreferenceDetailFragment();
                    mHomeActivity.replace(fragment, getString(R.string.tag_preferences_details_screen));
                } else if (position == 1) {
                    // Redirection to GETTanentFragment
                    InGaugeSession.write(getString(R.string.fragment_preference_detail), "Tenant");
                    PreferenceDetailFragment fragment = new PreferenceDetailFragment();
                    mHomeActivity.replace(fragment, getString(R.string.tag_preferences_details_screen));
                }
            }
        });
        return rootView;
    }


    private void initViews(View rootView) {
        values = new ArrayList<>();
        tenantValues=new ArrayList<>();
        description = new ArrayList<>();
        String selectedIndustry = InGaugeSession.read(getString(R.string.key_selected_industry), "");
        String selectedTenant = InGaugeSession.read(getString(R.string.key_selected_tenant), "");
        //String valueIndustry =mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_industry));
       // String valueTenant  =mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_tenant));
        String valueIndustry = "Select Industry";
        String valueTenant = "Select Tenant";
        values.add(valueIndustry);
        values.add(valueTenant);
        description.add(selectedIndustry);
        description.add(selectedTenant);
        /*description.add("selected industry");
        description.add("selected tenant");*/
        listview = (ListView) rootView.findViewById(R.id.listViewPref);
        listadapter = new PreferenceListadapter(getActivity(), values, description,"");
        listview.setAdapter(listadapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
        mBaseActivity=(BaseActivity) mContext;
    }

    public static Fragment newInstance(String text, int color) {
        Fragment frag = new PreferencesFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
//        args.putString(ARG_TEXT, text);
//        args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_title)));
        mHomeActivity.tvTitle.setText("Preferences");
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
    }
}
