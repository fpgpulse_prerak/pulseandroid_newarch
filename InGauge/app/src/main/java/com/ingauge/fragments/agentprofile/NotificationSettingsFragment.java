package com.ingauge.fragments.agentprofile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.NotificationSettingsListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.NotificationSettingsModel;
import com.ingauge.utils.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 05-Apr-18.
 */

public class NotificationSettingsFragment extends Fragment{

    private HomeActivity mHomeActivity;
    private APIInterface apiInterface;
    View rootView;
    ListView mListViewSettingsList;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){

        rootView = inflater.inflate(R.layout.fragment_notification_settings, container, false);
        mListViewSettingsList = (ListView) rootView.findViewById(R.id.fragment_notification_settings_exp_lv);
        //mListViewSettingsList .setTranscriptMode(ExpandableListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        mHomeActivity.startProgress(mHomeActivity);
        getNotificationSettingsList(Integer.parseInt(mHomeActivity.userIdForAPI));
        return rootView;
    }

    public void getNotificationSettingsList(int userId){
        Call mCall = apiInterface.getNotificationSettingsList(userId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                mHomeActivity.endProgress();
                NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) response.body();
                if(notificationSettingsModel != null && (notificationSettingsModel.getData() != null && notificationSettingsModel.getData().size() > 0)){

                    for(int i = 0; i < notificationSettingsModel.getData().size(); i++){

                        if(
                                (notificationSettingsModel.getData().get(i).getIsExternalMail() != null &&
                                        notificationSettingsModel.getData().get(i).getIsExternalMail()) ||
                                        (notificationSettingsModel.getData().get(i).getIsInternalMail() != null &&
                                                notificationSettingsModel.getData().get(i).getIsInternalMail()) ||
                                        (notificationSettingsModel.getData().get(i).getIsPushNotification() != null &&
                                                notificationSettingsModel.getData().get(i).getIsPushNotification())){
                            NotificationSettingsModel.Datum mDatumUpdate = notificationSettingsModel.getData().get(i);
                            mDatumUpdate.setGroupOpended(true);
                            notificationSettingsModel.getData().set(i, mDatumUpdate);

                        }else{
                            NotificationSettingsModel.Datum mDatumUpdate = notificationSettingsModel.getData().get(i);
                            mDatumUpdate.setGroupOpended(false);
                            notificationSettingsModel.getData().set(i, mDatumUpdate);
                        }

                    }
                    NotificationSettingsListAdapter mNotificationSettingsExpandListAdapter = new NotificationSettingsListAdapter(mHomeActivity, notificationSettingsModel.getData(),mListViewSettingsList);
                    mListViewSettingsList.setAdapter(mNotificationSettingsExpandListAdapter);
                    Logger.Error("<<<< Notification Model Data  " + notificationSettingsModel.getData().size());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();

            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.tvTitle.setText("My Notifications");
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibBack.setVisibility(View.VISIBLE);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
    }
}
