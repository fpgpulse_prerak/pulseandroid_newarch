package com.ingauge.fragments.dateselection.tabsfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Created by mansurum on 15-Feb-18.
 */

public class MonthFragmentForObs extends Fragment implements View.OnClickListener{

    View rootView;


    TextView tvLastThirtyDays;
    TextView tvThisMonth;
    TextView tvLastMonth;
    TextView tvPreviousPeriod;
    TextView tvFourWeeksAgo;
    TextView tvPreviousYear;
    TextView tv52WeeksAgo;

    ImageView ivLastThirtyDays;
    ImageView ivThisMonth;
    ImageView ivLastMonth;
    ImageView ivPreviousPeriod;
    ImageView ivFourWeeksAgo;
    ImageView ivPreviousYear;
    ImageView iv52WeeksAgo;


    SwitchCompat mASwitchCompare;

    LinearLayout llLastThirtyDays;
    LinearLayout llThisMonth;
    LinearLayout llLastMonth;
    LinearLayout llPreviousPeriod;
    LinearLayout llFourWeeksAgo;
    LinearLayout llPreviousYear;
    LinearLayout ll52WeeksAgo;

    LinearLayout llCompare;


    public String startDate = "";


    public String CompareStartDate = "";


    //Server Dates
    public String serverStartDate = "";
    public String serverEndDate = "";
    public String serverCompareStartDate = "";
    public String serverCompareEndDate = "";


    public String serverStartDateForCustom = "";
    public String serverEndDateForCustom = "";


    public boolean isCompare = true;

    public int dateRangeIndex = -1;
    public int ComparedateRangeIndex = -1;


    String combinedDate = "";
    String combinedCompareDate = "";


    private int tabIndex = 2;
    LinearLayout llLeaderboardCompare;
    boolean isFromLeaderBoard = false;
    HomeActivity mHomeActivity;


    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setTabIndex(2);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.month_fragment, container, false);
        initViews(rootView);


        if(getArguments() != null){
            Bundle mBundle = getArguments();
            /*startDate = mBundle.getString(DateSelectionFragment.KEY_START_DATE);
            endDate =  mBundle.getString(DateSelectionFragment.KEY_END_DATE);
            compareStartDate = mBundle.getString(DateSelectionFragment.KEY_COMPARE_START_DATE);
            compareEndDate =  mBundle.getString(DateSelectionFragment.KEY_COMPARE_END_DATE);
            serverStartDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_START_DATE);
            serverEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_END_DATE);
            serverCompareStartDate= mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_START_DATE);
            serverCompareEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_END_DATE);*/
            isCompare = mBundle.getBoolean(DateSelectionFragment.KEY_IS_COMPARE);
            isFromLeaderBoard = mBundle.getBoolean(DateSelectionFragment.KEY_IS_FROM_LEADERBOARD);

        }
        if(isFromLeaderBoard){
            llLeaderboardCompare.setVisibility(View.GONE);
        } else{
            llLeaderboardCompare.setVisibility(View.VISIBLE);
        }


        dateRangeIndex = InGaugeSession.read(getString(R.string.key_from_date_range_index_obs), -1);
        mASwitchCompare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isChecked){
                    llCompare.setVisibility(View.VISIBLE);
                    isCompare = true;
                    CompareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                } else{
                    llCompare.setVisibility(View.GONE);
                    isCompare = false;
                }
            }
        });

        if(getDateRangeIndex() == -1){
            setDateRangeIndex(1);
            if(isCompare){
                if(getComparedateRangeIndex() == -1){
                    setComparedateRangeIndex(1);
                } else{
                    setComparedateRangeIndex(getComparedateRangeIndex());
                }
            }
        } else{
            setDateRangeIndex(getDateRangeIndex());

            if(isCompare){
                if(getComparedateRangeIndex() == -1){
                    setComparedateRangeIndex(1);
                } else{
                    setComparedateRangeIndex(getComparedateRangeIndex());
                }
            }
        }
        tvLastThirtyDays.setText(getLastThirtyDaysRange(new Date()));
        tvThisMonth.setText(getMonthDisplay(DateTimeUtils.getCurrentMonth()));
        tvLastMonth.setText(getMonthDisplay(DateTimeUtils.getLastMonth()));
        String onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
        tvPreviousPeriod.setText(getPreviousMonthRange(getDateObj(onedaybefore)));
        tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(new Date()));
        tvPreviousYear.setText(getLastYearThirtyDaysRange(new Date()));
        tv52WeeksAgo.setText(getFiftyTwoWeeksAgoDateRangeofLastSevenDays(new Date()));

        if(dateRangeIndex == 1){
            visibleRightTick(1);
            startDate = tvLastThirtyDays.getText().toString();
            /*serverStartDate = getStartDateForServer(startDate);
            serverEndDate = getEndDateForServer(startDate);*/


            try{
                Calendar serverStartCalendar = UiUtils.DateToCalendar(sdfServer.parse(getStartDateForServer(startDate)));
                serverStartDate = "" + serverStartCalendar.getTimeInMillis();
                serverStartDateForCustom = getStartDateForServer(startDate);
            } catch(ParseException e){
                e.printStackTrace();
            }
            try{
                Calendar serverEndCalendar = UiUtils.DateToCalendar(sdfServer.parse(getEndDateForServer(startDate)));
                serverEndDate = "" + serverEndCalendar.getTimeInMillis();
                serverEndDateForCustom= getEndDateForServer(startDate);
            } catch(ParseException e){
                e.printStackTrace();
            }



        } else if(dateRangeIndex == 2){
            visibleRightTick(2);
            /*serverStartDate = getStartDateofThisMOnth();
            serverEndDate = getEndDateofThisMOnth();*/

            try{
                Calendar serverStartCalendar = UiUtils.DateToCalendar(sdfServer.parse(getStartDateofThisMOnth()));
                serverStartDate = "" + serverStartCalendar.getTimeInMillis();
                serverStartDateForCustom = getStartDateofThisMOnth();
            } catch(ParseException e){
                e.printStackTrace();
            }
            try{
                Calendar serverEndCalendar = UiUtils.DateToCalendar(sdfServer.parse(getEndDateofThisMOnth()));
                serverEndDate = "" + serverEndCalendar.getTimeInMillis();
                serverEndDateForCustom= getEndDateofThisMOnth();
            } catch(ParseException e){
                e.printStackTrace();
            }

        } else if(dateRangeIndex == 3){
            /*serverStartDate = getStartDateofLastMOnth();
            serverEndDate = getEndDateofLastMOnth();*/
            visibleRightTick(3);
            try{
                Calendar serverStartCalendar = UiUtils.DateToCalendar(sdfServer.parse(getStartDateofLastMOnth()));
                serverStartDate = "" + serverStartCalendar.getTimeInMillis();
                serverStartDateForCustom = getStartDateofLastMOnth();
            } catch(ParseException e){
                e.printStackTrace();
            }
            try{
                Calendar serverEndCalendar = UiUtils.DateToCalendar(sdfServer.parse(getEndDateofLastMOnth()));
                serverEndDate = "" + serverEndCalendar.getTimeInMillis();
                serverEndDateForCustom= getEndDateofLastMOnth();
            } catch(ParseException e){
                e.printStackTrace();
            }
        }

        if(getComparedateRangeIndex() == 1){
            visibleCompareRightTick(1);
            CompareStartDate = tvPreviousPeriod.getText().toString();
            serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
            serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
            Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
            Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));
        } else if(getComparedateRangeIndex() == 2){
            visibleCompareRightTick(2);
            CompareStartDate = tvFourWeeksAgo.getText().toString();
            serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
            serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());

            Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
            Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));

        } else if(getComparedateRangeIndex() == 3){
            visibleCompareRightTick(3);
            CompareStartDate = tvPreviousYear.getText().toString();
            serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
            serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());


            Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
            Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));
        } else if(getComparedateRangeIndex() == 4){
            visibleCompareRightTick(4);
            CompareStartDate = tv52WeeksAgo.getText().toString();
            serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
            serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

            Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
            Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
        }


        if(isCompare()){
            llCompare.setVisibility(View.VISIBLE);
            mASwitchCompare.setChecked(true);

        } else{
            llCompare.setVisibility(View.GONE);
            mASwitchCompare.setChecked(false);

        }
        return rootView;
    }

    public String getStartDateofThisMOnth(){
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            Date dddd = calendar.getTime();
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            Logger.Error("<<<<  First Day of This Month " + sdf1.format(calendar.getTime()));
            return sdf1.format(calendar.getTime());
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getEndDateofThisMOnth(){

        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

            Date dddd = calendar.getTime();
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            Logger.Error("<<<<  Last Day of This Month " + sdf1.format(calendar.getTime()));
            return sdf1.format(calendar.getTime());
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getStartDateofLastMOnth(){

        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.MONTH, -1);
            Date dddd = calendar.getTime();
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            Logger.Error("<<<<  First Day of Last Month " + sdf1.format(calendar.getTime()));
            return sdf1.format(calendar.getTime());
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getEndDateofLastMOnth(){
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            //calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DATE));
            calendar.add(Calendar.MONTH, -1);
            //calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Logger.Error("<<<<  Last Day of Last Month " + sdf1.format(calendar.getTime()));
            return sdf1.format(calendar.getTime());
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getStartDateForServer(String combinedDate){
        try{
            String strStartDate = "";
            String split[] = combinedDate.split("-");
            strStartDate = getServerDateFormat(split[0]);
            return strStartDate;
        } catch(Exception e){
            e.printStackTrace();
        }

        return "";
    }

    public String getEndDateForServer(String combinedDate){
        try{
            String strEndDate = "";
            String split[] = combinedDate.split("-");
            strEndDate = getServerDateFormat(split[1]);
            return strEndDate;
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }


    private void initViews(View itemView){


        llLeaderboardCompare = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_compare_for_leader_board);
        tvLastThirtyDays = (TextView) itemView.findViewById(R.id.month_fragment_tv_last_thirty_days_range);
        tvThisMonth = (TextView) itemView.findViewById(R.id.month_fragment_tv_this_month_range);
        tvLastMonth = (TextView) itemView.findViewById(R.id.month_fragment_tv_last_month_range);
        tvPreviousPeriod = (TextView) itemView.findViewById(R.id.month_fragment_tv_previous_period_range);
        tvFourWeeksAgo = (TextView) itemView.findViewById(R.id.month_fragment_tv_four_weeks_ago_range);
        tvPreviousYear = (TextView) itemView.findViewById(R.id.month_fragment_tv_previous_year_range);
        tv52WeeksAgo = (TextView) itemView.findViewById(R.id.month_fragment_tv_fifty_two_weeks_ago_range);
        llCompare = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_compare);
        mASwitchCompare = (SwitchCompat) itemView.findViewById(R.id.switch_compare_date);

        ivLastThirtyDays = (ImageView) itemView.findViewById(R.id.month_fragment_iv_last_thirty_days);
        ivThisMonth = (ImageView) itemView.findViewById(R.id.month_fragment_iv_this_month);
        ivLastMonth = (ImageView) itemView.findViewById(R.id.month_fragment_iv_day_last_month);
        ivPreviousPeriod = (ImageView) itemView.findViewById(R.id.month_fragment_iv_previous_period);
        ivFourWeeksAgo = (ImageView) itemView.findViewById(R.id.month_fragment_iv_four_weeks_ago);
        ivPreviousYear = (ImageView) itemView.findViewById(R.id.month_fragment_iv_previous_year);
        iv52WeeksAgo = (ImageView) itemView.findViewById(R.id.month_fragment_iv_fifty_two_weeks_ago);


        llLastThirtyDays = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_last_thirty_days);
        llLastThirtyDays.setOnClickListener(this);
        llThisMonth = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_this_month);
        llThisMonth.setOnClickListener(this);
        llLastMonth = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_last_month);
        llLastMonth.setOnClickListener(this);
        llPreviousPeriod = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_previous_period);
        llPreviousPeriod.setOnClickListener(this);
        llFourWeeksAgo = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_four_weeks_ago);
        llFourWeeksAgo.setOnClickListener(this);
        llPreviousYear = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_previous_year);
        llPreviousYear.setOnClickListener(this);
        ll52WeeksAgo = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_fifty_two_weeks_ago);
        ll52WeeksAgo.setOnClickListener(this);


    }

    @Override
    public void onClick(View view){

        switch(view.getId()){
            case R.id.month_fragment_ll_last_thirty_days:
                visibleRightTick(1);
                tvLastThirtyDays.setText(getLastThirtyDaysRange(new Date()));
                tvThisMonth.setText(getMonthDisplay(DateTimeUtils.getCurrentMonth()));
                tvLastMonth.setText(getMonthDisplay(DateTimeUtils.getLastMonth()));
                String onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
                tvPreviousPeriod.setText(getPreviousMonthRange(getDateObj(onedaybefore)));
                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(new Date()));
                tvPreviousYear.setText(getLastYearThirtyDaysRange(new Date()));
                tv52WeeksAgo.setText(getFiftyTwoWeeksAgoDateRangeofLastSevenDays(new Date()));

                startDate = tvLastThirtyDays.getText().toString();

                /*serverStartDate = getStartDateForServer(startDate);
                serverEndDate = getEndDateForServer(startDate);*/

                try{
                    Calendar serverStartCalendar = UiUtils.DateToCalendar(sdfServer.parse(getStartDateForServer(startDate)));
                    serverStartDate = "" + serverStartCalendar.getTimeInMillis();
                    serverStartDateForCustom = getStartDateForServer(startDate);
                } catch(ParseException e){
                    e.printStackTrace();
                }
                try{
                    Calendar serverEndCalendar = UiUtils.DateToCalendar(sdfServer.parse(getEndDateForServer(startDate)));
                    serverEndDate = "" + serverEndCalendar.getTimeInMillis();
                    serverStartDateForCustom = getEndDateForServer(startDate);
                } catch(ParseException e){
                    e.printStackTrace();
                }

                if(getComparedateRangeIndex() == 1){
                    CompareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                    Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                    Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));




                } else if(getComparedateRangeIndex() == 2){
                    CompareStartDate = tvFourWeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                    Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                    Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
                } else if(getComparedateRangeIndex() == 3){
                    CompareStartDate = tvPreviousYear.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());

                    Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                    Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));
                } else if(getComparedateRangeIndex() == 4){
                    CompareStartDate = tv52WeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                    Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                    Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
                }
                break;
            case R.id.month_fragment_ll_this_month:
                visibleRightTick(2);
                try{
                    onedaybefore = DateTimeUtils.getLastDay(new Date());
                    tvPreviousPeriod.setText(getPreviousMonthRange(getDateObj(onedaybefore)));
                    tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(getDateObj(onedaybefore)));
                    tvPreviousYear.setText(getLastYearThirtyDaysRange(getDateObj(onedaybefore)));
                    tv52WeeksAgo.setText(getFiftyTwoWeeksAgoDateRangeofLastSevenDays(getDateObj(onedaybefore)));

                    startDate = getMonthDisplay(DateTimeUtils.getCurrentMonth());
                    /*serverStartDate = getStartDateofThisMOnth();
                    serverEndDate = getEndDateofThisMOnth();
*/
                    try{
                        Calendar serverStartCalendar = UiUtils.DateToCalendar(sdfServer.parse(getStartDateofThisMOnth()));
                        serverStartDate = "" + serverStartCalendar.getTimeInMillis();
                        serverStartDateForCustom = getStartDateofThisMOnth();
                    } catch(ParseException e){
                        e.printStackTrace();
                    }
                    try{
                        Calendar serverEndCalendar = UiUtils.DateToCalendar(sdfServer.parse(getEndDateofThisMOnth()));
                        serverEndDate = "" + serverEndCalendar.getTimeInMillis();
                        serverStartDateForCustom = getEndDateofThisMOnth();
                    } catch(ParseException e){
                        e.printStackTrace();
                    }

                    if(getComparedateRangeIndex() == 1){
                        CompareStartDate = tvPreviousPeriod.getText().toString();
                        serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                        Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                        Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));
                    } else if(getComparedateRangeIndex() == 2){
                        CompareStartDate = tvFourWeeksAgo.getText().toString();
                        serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                        Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                        Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
                    } else if(getComparedateRangeIndex() == 3){

                        CompareStartDate = tvPreviousYear.getText().toString();
                        serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());

                        Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                        Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));
                    } else if(getComparedateRangeIndex() == 4){
                        CompareStartDate = tv52WeeksAgo.getText().toString();
                        serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                        Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                        Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.month_fragment_ll_last_month:
                visibleRightTick(3);
                onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
                tvPreviousPeriod.setText(getPreviousMonthRange(getDateObj(onedaybefore)));
                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(new Date()));
                tvPreviousYear.setText(getLastYearThirtyDaysRange(new Date()));
                tv52WeeksAgo.setText(getFiftyTwoWeeksAgoDateRangeofLastSevenDays(new Date()));

                startDate = getMonthDisplay(DateTimeUtils.getLastMonth());
                /*serverStartDate = getStartDateofLastMOnth();
                serverEndDate = getEndDateofLastMOnth();*/


                try{
                    Calendar serverStartCalendar = UiUtils.DateToCalendar(sdfServer.parse(getStartDateofLastMOnth()));
                    serverStartDate = "" + serverStartCalendar.getTimeInMillis();
                    serverStartDateForCustom = getStartDateofLastMOnth();
                } catch(ParseException e){
                    e.printStackTrace();
                }
                try{
                    Calendar serverEndCalendar = UiUtils.DateToCalendar(sdfServer.parse(getEndDateofLastMOnth()));
                    serverEndDate = "" + serverEndCalendar.getTimeInMillis();
                    serverEndDateForCustom= getEndDateofLastMOnth();
                } catch(ParseException e){
                    e.printStackTrace();
                }
                if(getComparedateRangeIndex() == 1){
                    CompareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                    Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                    Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));
                } else if(getComparedateRangeIndex() == 2){

                    CompareStartDate = tvFourWeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                    Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                    Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
                } else if(getComparedateRangeIndex() == 3){

                    CompareStartDate = tvPreviousYear.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());

                    Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                    Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));
                } else if(getComparedateRangeIndex() == 4){

                    CompareStartDate = tv52WeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                    Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                    Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
                }
                break;
            case R.id.month_fragment_ll_previous_period:
                visibleCompareRightTick(1);
                CompareStartDate = tvPreviousPeriod.getText().toString();
                serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                break;
            case R.id.month_fragment_ll_four_weeks_ago:
                visibleCompareRightTick(2);
                CompareStartDate = tvFourWeeksAgo.getText().toString();
                serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                break;
            case R.id.month_fragment_ll_previous_year:
                visibleCompareRightTick(3);
                CompareStartDate = tvPreviousYear.getText().toString();
                serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());
                break;
            case R.id.month_fragment_ll_fifty_two_weeks_ago:
                visibleCompareRightTick(4);
                CompareStartDate = tv52WeeksAgo.getText().toString();
                serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());
                break;

        }
    }

    public void visibleRightTick(int position){
        switch(position){
            case 1:
                ivLastThirtyDays.setVisibility(View.VISIBLE);
                ivThisMonth.setVisibility(View.GONE);
                ivLastMonth.setVisibility(View.GONE);
                setDateRangeIndex(1);
                break;
            case 2:
                ivLastThirtyDays.setVisibility(View.GONE);
                ivThisMonth.setVisibility(View.VISIBLE);
                ivLastMonth.setVisibility(View.GONE);
                setDateRangeIndex(2);
                break;
            case 3:
                ivLastThirtyDays.setVisibility(View.GONE);
                ivThisMonth.setVisibility(View.GONE);
                ivLastMonth.setVisibility(View.VISIBLE);
                setDateRangeIndex(3);
                break;
        }
    }

    public void visibleCompareRightTick(int position){
        switch(position){
            case 1:
                ivPreviousPeriod.setVisibility(View.VISIBLE);
                ivFourWeeksAgo.setVisibility(View.GONE);
                ivPreviousYear.setVisibility(View.GONE);
                iv52WeeksAgo.setVisibility(View.GONE);

                break;
            case 2:
                ivPreviousPeriod.setVisibility(View.GONE);
                ivFourWeeksAgo.setVisibility(View.VISIBLE);
                ivPreviousYear.setVisibility(View.GONE);
                iv52WeeksAgo.setVisibility(View.GONE);
                break;
            case 3:
                ivPreviousPeriod.setVisibility(View.GONE);
                ivFourWeeksAgo.setVisibility(View.GONE);
                ivPreviousYear.setVisibility(View.VISIBLE);
                iv52WeeksAgo.setVisibility(View.GONE);
                break;
            case 4:
                ivPreviousPeriod.setVisibility(View.GONE);
                ivFourWeeksAgo.setVisibility(View.GONE);
                ivPreviousYear.setVisibility(View.GONE);
                iv52WeeksAgo.setVisibility(View.VISIBLE);
                break;

        }
    }

    public String getLastThirtyDaysRange(Date inputDate){
        try{
            String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
            String sameDayofLastWeek = DateTimeUtils.getLastThirtyDayFromToday(inputDate);
            return getDisplayStringForWeekTab(sameDayofLastWeek) + " - " + getDisplayStringForWeekTab(yestrerday);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getLastYearThirtyDaysRange(Date inputDate){
        try{
            String yestrerday = DateTimeUtils.getYesterdayDateLastYearString(inputDate);
            String sameDayofLastWeek = DateTimeUtils.getLastThirtyDayFromTodayLastYear(inputDate);
            return getDisplayStringForWeekTab(sameDayofLastWeek) + " - " + getDisplayStringForWeekTab(yestrerday);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param DateStr
     * @return For Week Tab
     */
    public String getDisplayStringForWeekTab(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getMonthDisplay(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("MMMM");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getPreviousMonthRange(Date inputDate){
        try{
            String one = DateTimeUtils.getDayofLastThirtyDay(inputDate);
            Logger.Error("<<<<  ONE  " + one);

            String two = "";

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTime(getDateObj(one));
            cal.add(Calendar.DAY_OF_YEAR, -30);
            two = dateFormat.format(cal.getTime());

            Logger.Error("<<<<  TWO  " + two);

            return getDisplayStringForWeekTab(two) + " - " + getDisplayStringForWeekTab(one);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public Date getDateObj(String dateStr){
        try{
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return mDateFormat.parse(dateStr);
        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public String getFourWeeksAgoDateRangeofLastSevenDays(Date inputDate){
        String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
        String sameDayofLastWeek = DateTimeUtils.getDayofLastThirtyDay(inputDate);
        return getDisplayStringForWeekTab(getLastFourWeekFirstDay(getDateObj(sameDayofLastWeek))) + " - " + getDisplayStringForWeekTab(getLastFourWeekFirstDay(getDateObj(yestrerday)));

    }

    public String getFiftyTwoWeeksAgoDateRangeofLastSevenDays(Date inputDate){
        String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
        String sameDayofLastWeek = DateTimeUtils.getDayofLastThirtyDay(inputDate);
        return getDisplayStringForWeekTab(getLastFifityWeekFirstDay(getDateObj(sameDayofLastWeek))) + " - " + getDisplayStringForWeekTab(getLastFourWeekFirstDay(getDateObj(yestrerday)));

    }

    public String getLastFourWeekFirstDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DAY_OF_YEAR, -28);
        return sdf.format(cal.getTime());

    }

    public String getLastFifityWeekFirstDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DAY_OF_YEAR, -364);
        return sdf.format(cal.getTime());

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        setTabIndex(2);

    }

    public int getDateRangeIndex(){
        return dateRangeIndex;
    }

    public void setDateRangeIndex(int dateRangeIndex){
        this.dateRangeIndex = dateRangeIndex;
    }


    public int getComparedateRangeIndex(){
        return ComparedateRangeIndex;
    }

    public void setComparedateRangeIndex(int comparedateRangeIndex){
        ComparedateRangeIndex = comparedateRangeIndex;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }

    public String getServerDateFormat(String inputDateStr){

        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        try{
            Date inputDate = inputFormat.parse(inputDateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public boolean isCompare(){
        return isCompare;
    }

    public void setCompare(boolean compare){
        isCompare = compare;
    }

    public String getStartDate(){
        return startDate;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getServerStartDate(){
        return serverStartDate;
    }

    public void setServerStartDate(String serverStartDate){
        this.serverStartDate = serverStartDate;
    }

    public String getServerEndDate(){
        return serverEndDate;
    }

    public void setServerEndDate(String serverEndDate){
        this.serverEndDate = serverEndDate;
    }

    public String getServerCompareStartDate(){
        return serverCompareStartDate;
    }

    public void setServerCompareStartDate(String serverCompareStartDate){
        this.serverCompareStartDate = serverCompareStartDate;
    }

    public String getServerCompareEndDate(){
        return serverCompareEndDate;
    }

    public void setServerCompareEndDate(String serverCompareEndDate){
        this.serverCompareEndDate = serverCompareEndDate;
    }

    public String getCompareStartDate(){
        return CompareStartDate;
    }

    public void setCompareStartDate(String compareStartDate){
        CompareStartDate = compareStartDate;
    }

    @Override
    public void onResume(){
        super.onResume();
        // mHomeActivity.tvTitle.setText("Month");
    }
    public String getServerStartDateForCustom(){
        return serverStartDateForCustom;
    }

    public void setServerStartDateForCustom(String serverStartDateForCustom){
        this.serverStartDateForCustom = serverStartDateForCustom;
    }

    public String getServerEndDateForCustom(){
        return serverEndDateForCustom;
    }

    public void setServerEndDateForCustom(String serverEndDateForCustom){
        this.serverEndDateForCustom = serverEndDateForCustom;
    }


}
