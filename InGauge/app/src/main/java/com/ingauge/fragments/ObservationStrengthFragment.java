package com.ingauge.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.ObservationJobType;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pathanaa on 13-07-2017.
 */

public class ObservationStrengthFragment extends Fragment implements View.OnClickListener{
    private Context mContext;
    private FlexboxLayout mFlexStrength;
    private View rootView;
    private ArrayList<String> arrayCount;
    private ArrayList<String> tagID;
    private int counter = 0;
    private APIInterface apiInterface;

    private CardView mCardJobType;
    private AppCompatButton btnSave;
    public static final String KEY_FILTER_DETAILS = "filter_details";
    private EditText edtAreaOfImprovment;
    private ImageView imgRowItemFilter;
    private LinearLayout mLinearDisplay;
    private SwitchCompat mSwitchTeamMember;
    private SwitchCompat mSwitchSendMail;
    private TextView tvAreaofImprovment;
    private HomeActivity mHomeActivity;
    private TextView tvAgentScoreTitle;
    private TextView tvAgentScoreValue;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_observation_strength, container, false);
        Init();
        return rootView;
    }

    private void Init(){
        counter = 0;
        tvAgentScoreTitle = (TextView)rootView.findViewById(R.id.fragment_observation_agent_list_tv_agent_score_title);
        tvAgentScoreValue = (TextView)rootView.findViewById(R.id.fragment_observation_agent_list_tv_agent_score_value);
        tvAgentScoreTitle .setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_agent_score)));
        tvAgentScoreValue.setText(mHomeActivity.agentScore);
        mFlexStrength = (FlexboxLayout) rootView.findViewById(R.id.flex_strength);
        //mFlexStrength.setFlexGrow(1.0f);
        arrayCount = new ArrayList<>();
        tagID = new ArrayList<>();
        mCardJobType = (CardView) rootView.findViewById(R.id.card_great_job_on);
        mCardJobType.setOnClickListener(this);
        btnSave = (AppCompatButton) rootView.findViewById(R.id.btn_save);
        btnSave.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_ob_cc_save)));
        btnSave.setOnClickListener(this);

        tvAreaofImprovment = (TextView) rootView.findViewById(R.id.tv_area_of_improvment);
        tvAreaofImprovment.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_area_of_improvment)));

        edtAreaOfImprovment = (EditText) rootView.findViewById(R.id.edt_area_of_improvment);
        edtAreaOfImprovment.setText(mHomeActivity.areaOfImprovment);
        imgRowItemFilter = (ImageView) rootView.findViewById(R.id.row_item_filter_iv);
        mLinearDisplay = (LinearLayout) rootView.findViewById(R.id.linear_display_to_agent);

        mSwitchTeamMember = (SwitchCompat) rootView.findViewById(R.id.switch_isdisplay_team_member);
        mSwitchSendMail = (SwitchCompat) rootView.findViewById(R.id.switch_mail_to_send);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        mSwitchTeamMember.setChecked(mHomeActivity.donotDisplayToAgent);
        mSwitchSendMail.setChecked(mHomeActivity.donotMailToAgent);


        if(mHomeActivity.mObservationJobType == null){
            RequestJobType();
        } else{
            for(int position = 0; position < mHomeActivity.mObservationJobType.getData().size(); position++){
                if(mHomeActivity.mObservationJobType.getData().get(position).isSelected()){
                    CreateTag(mHomeActivity.mObservationJobType.getData().get(position).getName(), mHomeActivity.mObservationJobType.getData().get(position).getId(), mHomeActivity.isOnlyView);
                }
            }
        }

        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
            mLinearDisplay.setVisibility(View.GONE);
        } else{
            mLinearDisplay.setVisibility(View.VISIBLE);
        }


        if(mHomeActivity.isOnlyView){
            DisableElement();
        } else{

        }
    }

    private void DisableElement(){
        btnSave.setVisibility(View.INVISIBLE);
        imgRowItemFilter.setVisibility(View.GONE);
        edtAreaOfImprovment.setEnabled(false);
        mSwitchTeamMember.setClickable(false);
        mSwitchSendMail.setClickable(false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }


    private void RequestJobType(){
        try{
            JsonObject requestBody = new JsonObject();
            int total = 100000;

            requestBody.addProperty("total", total);
            requestBody.addProperty("first", 0);
            Logger.Error("JSON OBJECT==>" + requestBody.toString());

            GetObsJobType(InGaugeSession.read(getString(R.string.key_auth_token), ""), "application/json", String.valueOf(InGaugeSession.read(getString(R.string.key_selected_industry_id), 0)), true, requestBody);

        } catch(Exception e){
            e.printStackTrace();
        }

    }


    void GetObsJobType(String authToken, String ContentType, String IndustryId, boolean isMobile, JsonObject requestBody){
        final Call mCall = apiInterface.GetObsJobType(requestBody);
        mHomeActivity.startProgress(mContext);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                mHomeActivity.endProgress();
                mHomeActivity.mObservationJobType = (ObservationJobType) response.body();
                if(mHomeActivity.isFromDashboard){
                    for(int parent = 0; parent < mHomeActivity.mObservationJobType.getData().size(); parent++){
                        for(int child = 0; child < mHomeActivity.observationEditViewModel.getData().getSurveyStrengths().size(); child++){
                            if(mHomeActivity.observationEditViewModel.getData().getSurveyStrengths().get(child).getId() == mHomeActivity.mObservationJobType.getData().get(parent).getId()){
                                mHomeActivity.mObservationJobType.getData().get(parent).setSelected(true);
                                break;
                            }
                        }

                    }
                    for(int position = 0; position < mHomeActivity.mObservationJobType.getData().size(); position++){
                        if(mHomeActivity.mObservationJobType.getData().get(position).isSelected()){
                            CreateTag(mHomeActivity.mObservationJobType.getData().get(position).getName(), mHomeActivity.mObservationJobType.getData().get(position).getId(), mHomeActivity.isOnlyView);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    public static Fragment newInstance(String text, int color){
        Fragment frag = new ObservationStrengthFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    private void CreateTag(String title, final int tagId, boolean isOnlyView){
        LayoutInflater vi = (LayoutInflater) mContext.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.row_question_tag, null);
        // fill in any details dynamically here
        final TextView textView = (TextView) v.findViewById(R.id.a_text_view);
        textView.setText(title);
        LinearLayout llmain = (LinearLayout)v.findViewById(R.id.row_question_tag_ll);

        ImageView img_del = (ImageView) v.findViewById(R.id.img_del);

        if(isOnlyView){
            img_del.setVisibility(View.GONE);
        }

        img_del.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String selectedTitle = textView.getText().toString().trim();
                int selectedItem = 0;
                int k = 0;
                for(int i = 0; i < arrayCount.size(); i++){

                    if(arrayCount.get(i).contains(selectedTitle)){
                        String[] value = arrayCount.get(i).split("&&&");
                        selectedItem = Integer.parseInt(value[1]);

                        for(int position = 0; position < mHomeActivity.mObservationJobType.getData().size(); position++){
                            if(Integer.parseInt(tagID.get(i)) == mHomeActivity.mObservationJobType.getData().get(position).getId()){
                                mHomeActivity.mObservationJobType.getData().get(position).setSelected(false);
                            }
                        }

                        arrayCount.remove(i);
                        tagID.remove(i);

                        k = i;
                    }
                }

                mFlexStrength.removeViewAt(selectedItem);

                int selectedItem2 = 0;
                for(int i = k; i < arrayCount.size(); i++){
                    String[] value = arrayCount.get(i).split("&&&");
                    selectedItem2 = Integer.parseInt(value[1]);
                    selectedItem2--;
                    arrayCount.set(i, value[0] + "&&&" + selectedItem2);
                }

                mFlexStrength.invalidate();
                counter--;
            }
        });
        // insert into main view
        arrayCount.add(title + "&&&" + counter);
        String[] value = arrayCount.get(arrayCount.size() - 1).split("&&&");
        tagID.add("" + tagId);
        Logger.Error("COUNTER?" + counter);
        LinearLayout layout = new LinearLayout(mHomeActivity);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mFlexStrength.addView(llmain, counter, layout.getLayoutParams());
        counter++;
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.card_great_job_on:{
                if(!mHomeActivity.isOnlyView){
                    Bundle mBundle = new Bundle();
                    //mBundle.putSerializable(KEY_FILTER_DETAILS,mObservationJobType);
                    MultiSelectionFragment multiSelectionFragment = new MultiSelectionFragment();
                    multiSelectionFragment.setArguments(mBundle);
                    mHomeActivity.replace(multiSelectionFragment, "");
                }
                break;
            }
            case R.id.btn_save:{

                if(mFlexStrength != null && mFlexStrength.getChildCount() > 0){

                    if(!mHomeActivity.isOnlyView){
                        CreateJSONObject();
                    }
                } else{
                    Toast.makeText(mHomeActivity, "Select At Least One Great Job", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private void CreateJSONObject(){
        try{
            String endDateMillis = String.valueOf(System.currentTimeMillis());
            if(mHomeActivity.isFromDashboard && !mHomeActivity.isPending){
                endDateMillis = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_getend_millis), "0");
            }

            JsonObject requestBody = new JsonObject();
            requestBody.addProperty("activeStatus", "Normal");

            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                requestBody.addProperty("donotDisplayToAgent", true);
                requestBody.addProperty("donotMailToAgent", true); //Observation
            } else{
                //To check Counter Coaching
                requestBody.addProperty("donotDisplayToAgent", mSwitchTeamMember.isChecked());
                requestBody.addProperty("donotMailToAgent", mSwitchSendMail.isChecked()); //Counter Coaching
            }

            requestBody.addProperty("endDate", endDateMillis);
            if(mHomeActivity.isFromDashboard){
                requestBody.addProperty("id", mHomeActivity.eventID);
            }

            requestBody.addProperty("industryId", String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 0)));
            requestBody.addProperty("mapTo", "SURVEY");
            requestBody.addProperty("note", edtAreaOfImprovment.getText().toString().trim());
            requestBody.addProperty("questionnaireId", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_id), "0"));
            requestBody.addProperty("respondentId", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_id), "0"));
            requestBody.addProperty("scoreVal", mHomeActivity.agentScore);
            requestBody.addProperty("startDate", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_getstarted_millis), "0"));

            String symptomText = "";
            String strengthText = "";

            JsonArray questionAnswer = new JsonArray();
            for(int i = 0; i < mHomeActivity.arrayObsQuestion.size(); i++){
                if(mHomeActivity.arrayObsQuestion.get(i).getQ_yesnona_applicable().equalsIgnoreCase("No")){
                    String Sym = "";
                    if(mHomeActivity.arrayObsQuestion.get(i).getQ_sym() != null){
                        Sym = mHomeActivity.arrayObsQuestion.get(i).getQ_sym().trim();
                    }
                    if(symptomText.trim().length() > 0){
                        symptomText = symptomText + " &&& " + Sym;
                    } else{
                        symptomText = Sym;
                    }
                } else if(mHomeActivity.arrayObsQuestion.get(i).getQ_yesnona_applicable().equalsIgnoreCase("Yes")){
                    String Sym = "";
                    if(mHomeActivity.arrayObsQuestion.get(i).getQ_sym() != null){
                        Sym = mHomeActivity.arrayObsQuestion.get(i).getQ_sym().trim();
                    }
                    if(strengthText.trim().length() > 0){
                        strengthText = strengthText + " &&& " + Sym;
                    } else{
                        strengthText = Sym;
                    }
                }

                JsonObject jobjAnswer = new JsonObject();
                jobjAnswer.addProperty("answer", mHomeActivity.arrayObsQuestion.get(i).getQ_yesnona_applicable());
                jobjAnswer.addProperty("questionId", mHomeActivity.arrayObsQuestion.get(i).getQ_id());
                questionAnswer.add(jobjAnswer);
            }
            requestBody.add("questionAnswer", questionAnswer);
            requestBody.addProperty("strengthText", strengthText);
            requestBody.addProperty("symptomText", symptomText);

            requestBody.addProperty("surveyEntityType", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), null));
            requestBody.addProperty("surveyorId", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_id), "0"));
            requestBody.addProperty("surveyStatus", "Completed");
            requestBody.addProperty("tenantLocationId", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), "0"));
            Bundle bundle = new Bundle();
            bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
            bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
            bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), ""));
            bundle.putString("respondent", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), ""));
            bundle.putString("surveyor", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), ""));
            bundle.putString("survey_entity", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), ""));
            bundle.putString("note", edtAreaOfImprovment.getText().toString().trim());
            bundle.putString("survey_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), null));
            bundle.putString("start_date", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_getstarted_millis), "0"));
            bundle.putString("end_date", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_getend_millis), "0"));
            InGaugeApp.getFirebaseAnalytics().logEvent("save_survey", bundle);
            JsonArray oneToOneIds = new JsonArray();

            for(int position = 0; position < mHomeActivity.mObservationJobType.getData().size(); position++){
                if(mHomeActivity.mObservationJobType.getData().get(position).isSelected()){
                    oneToOneIds.add(mHomeActivity.mObservationJobType.getData().get(position).getId());
                }
            }

            requestBody.add("oneToOneIds", oneToOneIds);
            Logger.Error("REQUEST_BODY==>" + requestBody);
            SaveObservation(InGaugeSession.read(getString(R.string.key_auth_token), ""), "application/json", String.valueOf(InGaugeSession.read(getString(R.string.key_selected_industry_id), 0)), true, requestBody);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    void SaveObservation(String authToken, String ContentType, String IndustryId, boolean isMobile, JsonObject requestBody){
        Call mCall = null;
        if(mHomeActivity.isFromDashboard){
            mCall = apiInterface.EditObservation(requestBody);
        } else{
            mCall = apiInterface.SaveObservation(requestBody);
        }

        mHomeActivity.startProgress(mContext);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                mHomeActivity.endProgress();
                if(response.body() != null){
                    try{
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        if(jsonObject.optInt("statusCode") == 200){
                            Logger.Error("Obseravtion Status " + "Created");
                            String successMessage = "";
                            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                                successMessage = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_ob_saved));
                            } else{
                                successMessage = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_cc_saved));
                            }
                            new AlertDialog.Builder(mContext)
                                    .setTitle("Success")
                                    .setMessage(successMessage)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener(){
                                        @Override
                                        public void onClick(DialogInterface dialog, int which){
                                            mHomeActivity.onBackPressed();
                                            mHomeActivity.onBackPressed();
                                            mHomeActivity.onBackPressed();
                                            mHomeActivity.mObservationJobType = null;

                                        }
                                    }).show();


                        } else{
                            Logger.Error("Obseravtion Status " + "Error creating observation");
                        }
                        /*if(jsonObject.get("TEST")==200){

                        }*/
                    } catch(Exception e){
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.tvTitle.setText("");
        mHomeActivity.tvTitle.setVisibility(View.GONE);
        mHomeActivity.ibBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mHomeActivity.onBackPressed();
            }
        });
    }
}
