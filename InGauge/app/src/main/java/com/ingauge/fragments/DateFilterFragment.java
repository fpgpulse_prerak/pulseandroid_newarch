package com.ingauge.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.UiUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by desainid on 5/11/2017.
 */

public class DateFilterFragment extends BaseFragment implements View.OnClickListener{

    public static String KEY_IS_FROM_LEADERBOARD = "is_from_leaderboard";
    Context mContext;
    HomeActivity mHomeActivity;
    TextView txtviewstrtDteFilter;
    TextView txtviewEndDteFilter;
    TextView txtStrtDte;
    TextView txtEndDte;
    TextView txtpreEndDteLable;
    TextView txtpreStrtDteLable;
    TextView txtpreEndDte;
    TextView txtpreStrtDte;
    LinearLayout linearStartDte;
    LinearLayout linearEndDte;
    LinearLayout linearPreStartDte;
    LinearLayout linearPreEndDte;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    //   private int mYear, mMonth, mDay;
    private Calendar calendar;
    SwitchCompat switchCompareDate;
    LinearLayout llCompareDateLayout;
    boolean isFromLeaderBoard = false;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvApply.setVisibility(View.VISIBLE);
//        mHomeActivity.tvApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_apply)));
        mHomeActivity.tvApply.setText("APPLY");
        mHomeActivity.ibMenu.setOnClickListener(this);
        // mHomeActivity.ibMenu.setImageResource(android.R.drawable.ic_menu_save);
        mHomeActivity.tvApply.setOnClickListener(this);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);

        if(getArguments() != null){
            isFromLeaderBoard = getArguments().getBoolean(KEY_IS_FROM_LEADERBOARD);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_date_filter, container, false);
        initViews(rootView);
        setHasOptionsMenu(true);
        calendar.setTime(new Date());
        calendar.add(calendar.DATE, -30);

        linearStartDte.setOnClickListener(this);
        linearEndDte.setOnClickListener(this);
        linearPreStartDte.setOnClickListener(this);
        linearPreEndDte.setOnClickListener(this);

        if(InGaugeSession.read(getString(R.string.key_is_compare_on), true)){
            switchCompareDate.setChecked(true);
            txtpreEndDte.setEnabled(true);
            txtpreEndDteLable.setEnabled(true);
            txtpreStrtDteLable.setEnabled(true);
            txtpreStrtDte.setEnabled(true);

            txtpreStrtDteLable.setTextColor(Color.BLACK);
            txtpreEndDteLable.setTextColor(Color.BLACK);

        } else{
            switchCompareDate.setChecked(false);

            txtpreEndDte.setEnabled(false);
            txtpreEndDteLable.setEnabled(false);
            txtpreStrtDteLable.setEnabled(false);
            txtpreStrtDte.setEnabled(false);

            txtpreStrtDteLable.setTextColor(Color.DKGRAY);
            txtpreEndDteLable.setTextColor(Color.DKGRAY);
        }
        switchCompareDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked){
                if(isChecked){
                    txtpreEndDte.setEnabled(true);
                    txtpreEndDteLable.setEnabled(true);
                    txtpreStrtDteLable.setEnabled(true);
                    txtpreStrtDte.setEnabled(true);

                    txtpreStrtDteLable.setTextColor(Color.BLACK);
                    txtpreEndDteLable.setTextColor(Color.BLACK);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_compare_on), true);
                    //findPreviousDays();
                } else{
                    txtpreEndDte.setEnabled(false);
                    txtpreEndDteLable.setEnabled(false);
                    txtpreStrtDteLable.setEnabled(false);
                    txtpreStrtDte.setEnabled(false);
                    txtpreStrtDteLable.setTextColor(Color.DKGRAY);
                    txtpreEndDteLable.setTextColor(Color.DKGRAY);
                    InGaugeSession.write(getString(R.string.key_is_compare_on), false);

                }
            }
        });


        if(isFromLeaderBoard){
            llCompareDateLayout.setVisibility(View.GONE);
        } else{
            llCompareDateLayout.setVisibility(View.VISIBLE);
        }
        return rootView;
    }


    private void initViews(View rootView){
        llCompareDateLayout = (LinearLayout) rootView.findViewById(R.id.fragment_date_filter_ll_compare_date);
        txtviewstrtDteFilter = (TextView) rootView.findViewById(R.id.tv_startdate_label);
        txtviewEndDteFilter = (TextView) rootView.findViewById(R.id.tv_enddate_label);
        txtStrtDte = (TextView) rootView.findViewById(R.id.tv_start_date);
        txtEndDte = (TextView) rootView.findViewById(R.id.tv_end_date);
        txtpreStrtDteLable = (TextView) rootView.findViewById(R.id.tv_pre_startdate_label);
        txtpreEndDteLable = (TextView) rootView.findViewById(R.id.tv_pre_endtdate_label);
        txtpreStrtDte = (TextView) rootView.findViewById(R.id.tv_pre_start_date);
        txtpreEndDte = (TextView) rootView.findViewById(R.id.tv_pre_end_date);
        switchCompareDate = (SwitchCompat) rootView.findViewById(R.id.switch_compare_date);
        linearStartDte = (LinearLayout) rootView.findViewById(R.id.linearStartDte);
        linearEndDte = (LinearLayout) rootView.findViewById(R.id.linearEndDte);
        linearPreStartDte = (LinearLayout) rootView.findViewById(R.id.linearPreStartDte);
        linearPreEndDte = (LinearLayout) rootView.findViewById(R.id.linearPreEndDte);


        //Set Texts for Saved Dates
        if(isFromLeaderBoard){
            txtStrtDte.setText(InGaugeSession.read(getString(R.string.key_start_date_for_lb), ""));
            txtEndDte.setText(InGaugeSession.read(getString(R.string.key_end_date_for_lb), ""));

        } else{
            txtStrtDte.setText(InGaugeSession.read(getString(R.string.key_start_date), ""));
            txtEndDte.setText(InGaugeSession.read(getString(R.string.key_end_date), ""));
            txtpreStrtDte.setText(InGaugeSession.read(getString(R.string.key_compare_start_date), ""));
            txtpreEndDte.setText(InGaugeSession.read(getString(R.string.key_compare_end_date), ""));
        }

        calendar = Calendar.getInstance();
//        mYear = calendar.get(Calendar.YEAR);
//        mMonth = calendar.get(Calendar.MONTH);
//        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        mHomeActivity.tvTitle.setText("Date Filter");
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);


    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.linearStartDte:
                Date display = null;
                try{
                    display = sdf.parse(txtStrtDte.getText().toString());
                } catch(ParseException e){
                    e.printStackTrace();
                }
                calendar = UiUtils.DateToCalendar(display);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                txtStrtDte.setText(sdf.format(calendar.getTime()));
                                mHomeActivity.setLeaderNeedUpdate(true);
                                if(isFromLeaderBoard){
                                    InGaugeSession.write(getString(R.string.key_start_date_for_lb), txtStrtDte.getText().toString());
                                    InGaugeSession.write(getString(R.string.key_start_date_serverfor_lb), sdfServer.format(calendar.getTime()));
                                } else{
                                    InGaugeSession.write(getString(R.string.key_start_date), txtStrtDte.getText().toString());
                                    InGaugeSession.write(getString(R.string.key_start_date_server), sdfServer.format(calendar.getTime()));
                                }

                                // findPreviousDays();
                            }

                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
                Bundle bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("start_date", InGaugeSession.read(getResources().getString(R.string.key_start_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;

            case R.id.linearEndDte:

                try{
                    calendar = UiUtils.DateToCalendar(sdf.parse(txtEndDte.getText().toString()));
                } catch(ParseException e){
                    e.printStackTrace();
                }

                DatePickerDialog endDatePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){

                                mHomeActivity.setLeaderNeedUpdate(true);
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                txtEndDte.setText(sdf.format(calendar.getTime()));
                                if(isFromLeaderBoard){
                                    InGaugeSession.write(getString(R.string.key_end_date_for_lb), txtEndDte.getText().toString());
                                    InGaugeSession.write(getString(R.string.key_end_date_server_for_lb), sdfServer.format(calendar.getTime()));
                                } else{
                                    InGaugeSession.write(getString(R.string.key_end_date), txtEndDte.getText().toString());
                                    InGaugeSession.write(getString(R.string.key_end_date_server), sdfServer.format(calendar.getTime()));
                                }

                                // findPreviousDays();
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                DateFormat format = new SimpleDateFormat("MMM dd ,yyyy", Locale.UK);
                Date date = null;
                Date minDate = null;
                final Calendar cal;
                try{
                    date = format.parse(txtStrtDte.getText().toString());
                    cal = Calendar.getInstance();
                    cal.setTime(date);
                    endDatePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
                    endDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                    endDatePickerDialog.show();

                } catch(ParseException e){
                    e.printStackTrace();
                }
                bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("end_date", InGaugeSession.read(getResources().getString(R.string.key_end_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;
            case R.id.linearPreStartDte:
                try{
                    calendar = UiUtils.DateToCalendar(sdf.parse(txtpreStrtDte.getText().toString()));
                } catch(ParseException e){
                    e.printStackTrace();
                }
                DatePickerDialog PrestrtDatePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){

                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                txtpreStrtDte.setText(sdf.format(calendar.getTime()));
                                InGaugeSession.write(getString(R.string.key_compare_start_date), txtpreStrtDte.getText().toString());
                                InGaugeSession.write(getString(R.string.key_compare_start_date_server), sdfServer.format(calendar.getTime()));
                                //   findPreviousDays();
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                //DateFormat   format = new SimpleDateFormat("MMM dd ,yyyy", Locale.UK);
                Date Predate = null;
                final Calendar Precal;
                try{
                    date = sdf.parse(txtStrtDte.getText().toString());
                    Precal = Calendar.getInstance();
                    Precal.setTime(date);
                    PrestrtDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    PrestrtDatePickerDialog.show();

                } catch(ParseException e){
                    e.printStackTrace();
                }

                bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("start_date", InGaugeSession.read(getResources().getString(R.string.key_start_date), ""));
                bundle.putString("end_date", InGaugeSession.read(getResources().getString(R.string.key_end_date), ""));
                bundle.putString("compare_start_date", InGaugeSession.read(getResources().getString(R.string.key_compare_start_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;
            case R.id.linearPreEndDte:
                try{
                    calendar = UiUtils.DateToCalendar(sdf.parse(txtpreEndDte.getText().toString()));
                } catch(ParseException e){
                    e.printStackTrace();
                }
                DatePickerDialog PreendDatePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){

                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                txtpreEndDte.setText(sdf.format(calendar.getTime()));
                                InGaugeSession.write(getString(R.string.key_compare_end_date), txtpreEndDte.getText().toString());
                                InGaugeSession.write(getString(R.string.key_compare_end_date_server), sdfServer.format(calendar.getTime()));
                                //   findPreviousDays();
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                //DateFormat   format = new SimpleDateFormat("MMM dd ,yyyy", Locale.UK);


                try{
                    date = sdf.parse(txtStrtDte.getText().toString());
                    minDate = sdf.parse(txtpreStrtDte.getText().toString());
                    Calendar preMax = Calendar.getInstance();
                    Precal = Calendar.getInstance();
                    Precal.setTime(date);
                    preMax.setTime(minDate);

                    // Precal.add(Precal.DATE,getDayDifference());
                    //PreendDatePickerDialog.getDatePicker().setMaxDate(Precal.getTimeInMillis());
                    PreendDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    PreendDatePickerDialog.getDatePicker().setMinDate(preMax.getTimeInMillis());
                    PreendDatePickerDialog.show();

                } catch(ParseException e){
                    e.printStackTrace();
                }
                bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("start_date", InGaugeSession.read(getResources().getString(R.string.key_start_date), ""));
                bundle.putString("end_date", InGaugeSession.read(getResources().getString(R.string.key_end_date), ""));
                bundle.putString("compare_start_date", InGaugeSession.read(getResources().getString(R.string.key_compare_start_date), ""));
                bundle.putString("compare_end_date", InGaugeSession.read(getResources().getString(R.string.key_compare_end_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;
            case R.id.tv_apply:
                mHomeActivity.onBackPressed();
                break;
        }

    }


    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);

    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
    }
}
