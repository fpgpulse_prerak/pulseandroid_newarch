package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.R;
import com.ingauge.utils.Logger;

/**
 * Created by mansurum on 15-May-17.
 */

public class HomeFragment extends BaseFragment {


    //    private static final String ARG_TEXT = "arg_text";
//    private static final String ARG_COLOR = "arg_color";
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public static Fragment newInstance(String text, int color) {
        Fragment frag = new HomeFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
//        args.putString(ARG_TEXT, text);
//        args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();
        Logger.Error("<< Call >>>");
    }
}
