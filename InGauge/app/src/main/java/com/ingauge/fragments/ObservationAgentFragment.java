package com.ingauge.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.FilterListForObservation;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.OnFilterDataSetListener;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.AgentModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ObservationEditViewModel;
import com.ingauge.pojo.ObservationListModel;
import com.ingauge.pojo.PerformanceLeaderModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.UiUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by desainid on 5/22/2017.
 */

public class ObservationAgentFragment extends BaseFragment implements RecyclerViewClickListener, OnFilterDataSetListener, View.OnClickListener{
    private Context mContext;
    private View rootView;
    private RecyclerView rvObserveAgentFilter;
    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModelForObserveAgent;
    private PerformanceLeaderModel mPerformanceLeaderModel;
    public static ObservationEditViewModel observationEditViewModel;
    private AgentModel mAgentModel;
    private ObservationListModel mObservationListModel;
    private Call mCall;
    HomeActivity mHomeActivity;
    private APIInterface apiInterface;
    public ArrayList<FilterBaseData> mFilterBaseDatas;
    public FilterListForObservation listadapter;
    public static final String KEY_FILTER_DETAILS = "filter_details";
    public static final String KEY_FILTER_SELECTION = "filter_seclection";
    public static final String KEY_SELECTION_POSITION = "seclected_position";
    private SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtils.DATE_FORMATE_OBSEVATION_COUNTER_COACHING, Locale.getDefault());
    private AppCompatButton btnGetStarted;
    private String observationID = "";
    private boolean isFromDashboard = false;
    private boolean isOnlyView = false;
    private Calendar calendar;
    private boolean isPending = false;
    private String obsTime;
    private boolean isSuperAdmin = false;
    private boolean isPerformanceLeader = false;
    private boolean isFrontlineAssociate = false;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getRoleStatus();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_observe_agent, container, false);
        Init();
        return rootView;
    }

    private void Init(){
        rvObserveAgentFilter = (RecyclerView) rootView.findViewById(R.id.rv_observeagent_filter);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mFilterBaseDatas = new ArrayList<>();
        btnGetStarted = (AppCompatButton) rootView.findViewById(R.id.btn_get_started);
        btnGetStarted.setVisibility(View.GONE);

        btnGetStarted.setOnClickListener(this);
        calendar = Calendar.getInstance();
        if(getArguments() != null){
            isFromDashboard = getArguments().getBoolean("isFromDashboard");
            isOnlyView = getArguments().getBoolean("isOnlyView");
            observationID = getArguments().getString("observationID");
            isPending = getArguments().getBoolean("isPending");

            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_loc), null) == null){
                getEventDetailObs("application/json", InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 0)), "1", observationID);
            } else{
                getLocationFromLocal();
            }

            if(isOnlyView){
                btnGetStarted.setText("View Survey");
            } else{
                btnGetStarted.setText("Edit Survey");
            }
        } else{
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_loc), null) == null){
                getLocationForObserveAgent(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", "Normal");
            } else{
                getLocationFromLocal();
            }
        }
        if(isOnlyView){
            btnGetStarted.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_survey_view)));
        } else if(!isOnlyView && isFromDashboard){
            btnGetStarted.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_survey_edit)));
        } else{
            btnGetStarted.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_survey_start)));
        }
    }

    public void getRoleStatus(){
        String roleName = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_login_tenant_user_role), "");
        if(roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_super_admin))){
            isSuperAdmin = true;
            isPerformanceLeader = false;
            isFrontlineAssociate = false;
        } else if(roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_performance_manager))){
            isSuperAdmin = false;
            isPerformanceLeader = true;
            isFrontlineAssociate = false;
        } else if(roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_frontline_associate))){
            isSuperAdmin = false;
            isPerformanceLeader = false;
            isFrontlineAssociate = true;
        }
    }

    private void getLocationFromLocal(){
        Gson gson = new Gson();
        String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_loc), null);
        mAccessibleTenantLocationDataModelForObserveAgent = gson.fromJson(json, AccessibleTenantLocationDataModel.class);
        if(mAccessibleTenantLocationDataModelForObserveAgent.getData() != null && mAccessibleTenantLocationDataModelForObserveAgent.getData().size() > 0){
            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 0;
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
            mFilterBaseData.isEditable = 1;
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), null) == null){
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_location_id), String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getId()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_location_name), String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getName()));
                mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getId());
                mFilterBaseData.name = mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getName();
            } else{
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), null);
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), null);
            }
            mFilterBaseDatas.add(mFilterBaseData);


            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_is_location_changed), false)){
                //Location Has been changed so calling Performance Leader, Agent and Observation API again
                if(isPerformanceLeader){

                    mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2));
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_name), "");
                    mFilterBaseData.isEditable = 0;
                    mFilterBaseData.filterIndex = 1;
                    if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_pl));
                    } else{
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_coach));
                    }
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_name), mFilterBaseData.name);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_id), mFilterBaseData.id);
                    mFilterBaseDatas.add(mFilterBaseData);
                    mHomeActivity.startProgress(mContext);
                    getAgent(InGaugeSession.read(
                            mContext.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                            Integer.parseInt(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), "0")));


                } else{
                    mHomeActivity.startProgress(mContext);
                    String savedDate = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_getstarted_millis), null);
                    UiUtils.clearObsCCLocal(mContext, false);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getstarted_millis), savedDate);
                    getPerformanceLeader(
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                            Integer.parseInt(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), "0")));
                }


            } else{
                if(isPerformanceLeader){
                    mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2));
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_name), "");
                    mFilterBaseData.isEditable = 0;
                    mFilterBaseData.filterIndex = 1;
                    if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_pl));
                    } else{
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_coach));
                    }
                    mFilterBaseDatas.add(mFilterBaseData);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_name), mFilterBaseData.name);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_id), mFilterBaseData.id);
                    getAgentFromLocal();
                } else{
                    getPerformanceLeaderFromLocal();
                }
                //Location is not changed, so getting Performance Leader, Agent and Observation from Local

            }
        }
    }

    private void getPerformanceLeaderFromLocal(){
        Gson gson = new Gson();
        String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_pl), null);
        mPerformanceLeaderModel = gson.fromJson(json, PerformanceLeaderModel.class);
        if(mPerformanceLeaderModel.getData() != null && mPerformanceLeaderModel.getData().size() > 0){
            FilterBaseData mFilterBaseData = new FilterBaseData();

            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_id), null) == null){
                mFilterBaseData.id = String.valueOf(mPerformanceLeaderModel.getData().get(0).getId());
                mFilterBaseData.name = mPerformanceLeaderModel.getData().get(0).getName();
                mFilterBaseData.isEditable = 1;
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_name), String.valueOf(mPerformanceLeaderModel.getData().get(0).getName()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_id), String.valueOf(mPerformanceLeaderModel.getData().get(0).getId()));
            } else{
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_id), null);
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), null);
            }

            mFilterBaseData.filterIndex = 1;

            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_pl));
            } else{
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_coach));
                ;
            }

            mFilterBaseDatas.add(mFilterBaseData);
        }
        getAgentFromLocal();
    }

    private void getAgentFromLocal(){
        Gson gson = new Gson();
        String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_agent), null);
        mAgentModel = gson.fromJson(json, AgentModel.class);
        if(mAgentModel.getData() != null && mAgentModel.getData().size() > 0){
            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 2;
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_agent));
            } else{
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_team_member));
            }
            mFilterBaseData.isEditable = 1;
            mFilterBaseDatas.add(mFilterBaseData);

            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_id), null) == null){
                mFilterBaseData.id = String.valueOf(mAgentModel.getData().get(0).getId());
                mFilterBaseData.name = mAgentModel.getData().get(0).getName();
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_agent_name), String.valueOf(mAgentModel.getData().get(0).getName()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_agent_id), String.valueOf(mAgentModel.getData().get(0).getId()));
            } else{
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_id), null);
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), null);
            }
        }

        getObservationFromLocal();
    }

    private void getObservationFromLocal(){
        Gson gson = new Gson();
        String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc), null);
        mObservationListModel = gson.fromJson(json, ObservationListModel.class);
        if(mObservationListModel != null && (mObservationListModel.getData() != null && mObservationListModel.getData().size() > 0)){
            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 3;
            mFilterBaseData.isEditable = 1;
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_type));
            } else{
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_coaching_type));
            }
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_id), null) == null){
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_observation_name), String.valueOf(mObservationListModel.getData().get(0).getName()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_observation_id), String.valueOf(mObservationListModel.getData().get(0).getId()));
                mFilterBaseData.id = String.valueOf(mObservationListModel.getData().get(0).getId());
                String observationName = UiUtils.SurroundWithBraces(mObservationListModel.getData().get(0).getName());
                String categoryName = UiUtils.SurroundWithBraces(mObservationListModel.getData().get(0).getSurveyCategory().getName());
                mFilterBaseData.name = observationName + categoryName;
            } else{
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_id), null);
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), null);
            }

            mFilterBaseDatas.add(mFilterBaseData);
            SetOtherParam();
        }

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //Removing Locale filter data when fragment destroy
        UiUtils.clearObsCCLocal(mContext, true);
        observationEditViewModel = null;
        mHomeActivity.endProgress();
    }

    void getEventDetailObs(String contentType, String authToken, String industryId, String isMobile, String ObservationId){
        mCall = apiInterface.getEventDetailObs(ObservationId);
        mHomeActivity.startProgress(mContext);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    observationEditViewModel = (ObservationEditViewModel) response.body();
                    //Location
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_location_id), String.valueOf(observationEditViewModel.getData().getTenantLocationId()));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_location_name), observationEditViewModel.getData().getTenantLocationName());

                    //Performance Leader
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_id), String.valueOf(observationEditViewModel.getData().getSurveyorId()));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_name), observationEditViewModel.getData().getSurveyorName());

                    //Observation
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_observation_id), String.valueOf(observationEditViewModel.getData().getQuestionnaireId()));

                    String observationName = UiUtils.SurroundWithBraces(observationEditViewModel.getData().getQuestionnaireName());
                    String categoryName = UiUtils.SurroundWithBraces(observationEditViewModel.getData().getQuestionnaireSurveyCategoryName());

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_observation_name), observationName + categoryName);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_edit_event_id), observationEditViewModel.getData().getId());

                    //Agent
                    if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_id), null) == null){
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_agent_id), String.valueOf(observationEditViewModel.getData().getRespondentId()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_agent_name), observationEditViewModel.getData().getRespondentName());
                    }

                    if(observationEditViewModel.getData().getSurveyStatus().equalsIgnoreCase("Pending")){
                        getLocationForObserveAgent(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", "Normal");
                    } else{
                        getAgent(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1), observationEditViewModel.getData().getTenantLocationId());
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    void getLocationForObserveAgent(final String authToken, int tenantId, String userId, String orderBy, String sort, String activeStatus){
        mCall = apiInterface.getAccessibleTenantLocationForGoalFilter(tenantId, userId, orderBy, sort, activeStatus);

        if(observationEditViewModel == null && !isPending){
            mHomeActivity.startProgress(mContext);
        }
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    mAccessibleTenantLocationDataModelForObserveAgent = (AccessibleTenantLocationDataModel) response.body();
                    if(mAccessibleTenantLocationDataModelForObserveAgent.getData() != null && mAccessibleTenantLocationDataModelForObserveAgent.getData().size() > 0){
                        try{
                            Gson gson = new Gson();
                            String json = gson.toJson(mAccessibleTenantLocationDataModelForObserveAgent);
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_loc), json);
                        } catch(Exception e){
                            e.printStackTrace();
                        }
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.filterIndex = 0;
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                        mFilterBaseData.isEditable = 1;
                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), null) == null){
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_location_id), String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getId()));
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_location_name), String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getName()));
                            mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getId());
                            mFilterBaseData.name = mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getName();
                        } else{
                            mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), null);
                            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_name), null);
                        }
                        mFilterBaseDatas.add(mFilterBaseData);

                        if(isPerformanceLeader){
                            mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2));
                            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_name), "");
                            mFilterBaseData.isEditable = 0;
                            mFilterBaseData.filterIndex = 1;
                            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_pl));
                            } else{
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_coach));
                            }
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_name), mFilterBaseData.name);
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_id), mFilterBaseData.id);
                            mFilterBaseDatas.add(mFilterBaseData);

                            getAgent(InGaugeSession.read(
                                    mContext.getResources().getString(R.string.key_auth_token), ""),
                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                                    Integer.parseInt(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), "0")));


                        } else{
                            getPerformanceLeader(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1), Integer.parseInt(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_location_id), "0")));
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
            }
        });
    }

    void getPerformanceLeader(final String authToken, int tenantId, final int tenantLocationId){
        mCall = apiInterface.getPerformanceLeader(tenantId, String.valueOf(tenantLocationId),
                "'isPerformanceManager'" + "," + "'isAvailableForTakeObservation'",
                true, true, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    mPerformanceLeaderModel = (PerformanceLeaderModel) response.body();

                    try{
                        Gson gson = new Gson();
                        String json = gson.toJson(mPerformanceLeaderModel);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_pl), json);
                    } catch(Exception e){
                        e.printStackTrace();
                    }

                    if(mPerformanceLeaderModel.getData() != null && mPerformanceLeaderModel.getData().size() > 0){
                        FilterBaseData mFilterBaseData = new FilterBaseData();

                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_id), null) == null){
                            mFilterBaseData.id = String.valueOf(mPerformanceLeaderModel.getData().get(0).getId());
                            mFilterBaseData.name = mPerformanceLeaderModel.getData().get(0).getName();
                            mFilterBaseData.isEditable = 1;
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_name), String.valueOf(mPerformanceLeaderModel.getData().get(0).getName()));
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_pl_id), String.valueOf(mPerformanceLeaderModel.getData().get(0).getId()));
                        } else{
                            mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_id), null);
                            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_pl_name), null);
                        }

                        mFilterBaseData.filterIndex = 1;
                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_pl));
                        } else{
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_coach));
                        }
                        mFilterBaseDatas.add(mFilterBaseData);
                        getAgent(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1), tenantLocationId);
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    void getAgent(final String authToken, int tenantId, final int tenantLocationId){
        mCall = apiInterface.getAgent(tenantId, String.valueOf(tenantLocationId), 1, 0, 0, "'isAvailableForTakeObservation'", false, false, false);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    mAgentModel = (AgentModel) response.body();

                    try{
                        Gson gson = new Gson();
                        String json = gson.toJson(mAgentModel);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_agent), json);
                    } catch(Exception e){
                        e.printStackTrace();
                    }

                    if(mAgentModel.getData() != null && mAgentModel.getData().size() > 0){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.filterIndex = 2;
                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_agent));
                        } else{
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_team_member));
                        }
                        mFilterBaseData.isEditable = 1;


                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_id), null) == null){
                            mFilterBaseData.id = String.valueOf(mAgentModel.getData().get(0).getId());
                            mFilterBaseData.name = mAgentModel.getData().get(0).getName();
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_agent_name), String.valueOf(mAgentModel.getData().get(0).getName()));
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_agent_id), String.valueOf(mAgentModel.getData().get(0).getId()));
                        } else{

                            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_is_location_changed), false)){
                                mFilterBaseData.id = String.valueOf(mAgentModel.getData().get(0).getId());
                                mFilterBaseData.name = mAgentModel.getData().get(0).getName();
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_agent_name), String.valueOf(mAgentModel.getData().get(0).getName()));
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_agent_id), String.valueOf(mAgentModel.getData().get(0).getId()));
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_is_location_changed), false);
                            } else{
                                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_id), null);
                                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), null);
                            }

                        }
                        mFilterBaseDatas.add(mFilterBaseData);
                        if(!isFromDashboard){
                            getObservationType(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), null), tenantLocationId);
                        } else if(observationEditViewModel != null && isPending){
                            getObservationType(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), null), tenantLocationId);
                        } else{
                            mHomeActivity.endProgress();
                            SetOtherParam();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }


    void getObservationType(final String authToken, String surveyEntityType, int tenantLocationId){
        mCall = apiInterface.getObservation(tenantLocationId, surveyEntityType);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                mHomeActivity.endProgress();
                if(response.body() != null){
                    mObservationListModel = (ObservationListModel) response.body();

                    try{
                        Gson gson = new Gson();
                        String json = gson.toJson(mObservationListModel);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc), json);
                    } catch(Exception e){
                        e.printStackTrace();
                    }

                    if(mObservationListModel.getData() != null && mObservationListModel.getData().size() > 0){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.filterIndex = 3;
                        mFilterBaseData.isEditable = 1;
                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_type));
                        } else{
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_coaching_type));
                        }
                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_id), null) == null){
                            mFilterBaseData.id = String.valueOf(mObservationListModel.getData().get(0).getId());

                            String observationName = UiUtils.SurroundWithBraces(mObservationListModel.getData().get(0).getName());
                            String categoryName = UiUtils.SurroundWithBraces(mObservationListModel.getData().get(0).getSurveyCategory().getName());

                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_observation_name), observationName + categoryName);
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_selected_observation_id), String.valueOf(mObservationListModel.getData().get(0).getId()));
                            mFilterBaseData.name = observationName + categoryName;
                        } else{
                            mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_id), null);
                            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_observation_name), null);
                        }

                        mFilterBaseDatas.add(mFilterBaseData);

                        SetOtherParam();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    public void SetOtherParam(){

        //Start Date Time
        FilterBaseData mFilterBaseData = new FilterBaseData();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        if(isFromDashboard && !isPending){
            mFilterBaseDatas.clear();
            //Tenant Location
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 0;
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
            mFilterBaseData.id = String.valueOf(observationEditViewModel.getData().getTenantLocationId());
            mFilterBaseData.isEditable = 0;
            mFilterBaseData.name = observationEditViewModel.getData().getTenantLocationName();
            mFilterBaseDatas.add(mFilterBaseData);

            //Performance Leader
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_pl));
            } else{
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_coach));
                ;
            }
            mFilterBaseData.id = String.valueOf(observationEditViewModel.getData().getSurveyorId());
            mFilterBaseData.isEditable = 0;
            mFilterBaseData.name = observationEditViewModel.getData().getSurveyorName();
            mFilterBaseDatas.add(mFilterBaseData);

            //Agent
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_agent));
            } else{
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_team_member));
            }

            if(isOnlyView){
                mFilterBaseData.isEditable = 0;
            } else{
                mFilterBaseData.isEditable = 1;
            }


            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_id), null) != null){
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_name), null);
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_selected_agent_id), null);
            } else{
                mFilterBaseData.name = observationEditViewModel.getData().getRespondentName();
                mFilterBaseData.id = String.valueOf(observationEditViewModel.getData().getRespondentId());
            }

            mFilterBaseDatas.add(mFilterBaseData);
            //Observation Type
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_obs_type));
            } else{
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_coaching_type));
            }
            mFilterBaseData.id = String.valueOf(observationEditViewModel.getData().getQuestionnaireId());
            mFilterBaseData.isEditable = 0;
            mFilterBaseData.name = observationEditViewModel.getData().getQuestionnaireName() + "(" + observationEditViewModel.getData().getQuestionnaireSurveyCategoryName() + ")";
            mFilterBaseDatas.add(mFilterBaseData);

            //Start Date Time
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_start_date));
            mFilterBaseData.id = "0";

            if(isOnlyView){
                mFilterBaseData.isEditable = 0;
            } else{
                mFilterBaseData.isEditable = 1;
            }
            mFilterBaseData.name = DateTimeUtils.DateObservationCounterCoachingNOUTC(observationEditViewModel.getData().getStartDate());
            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getstarted_millis), String.valueOf(observationEditViewModel.getData().getStartDate()));
            mFilterBaseDatas.add(mFilterBaseData);

            //End Date/Time
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_end_date));
            mFilterBaseData.id = "0";
            mFilterBaseData.isEditable = 0;

            /*mFilterBaseData.name = DateTimeUtils.
                    DateObservationCounterCoachingUTC(observationEditViewModel.getData().getEndDate()) +
                    "\n" + "(" +
                    secToTime(0) + " Minutes)";*/

            if(observationEditViewModel.getData().getSurveyTakenTime() != null){
                mFilterBaseData.name = DateTimeUtils.
                        DateObservationCounterCoachingNOUTC(observationEditViewModel.getData().getEndDate()) +
                        "\n" + "(" +
                        secToTime(observationEditViewModel.getData().getSurveyTakenTime()) + " Minutes)";
            } else{
                mFilterBaseData.name = DateTimeUtils.
                        DateObservationCounterCoachingNOUTC(observationEditViewModel.getData().getEndDate()) +
                        "\n" + "(" +
                        secToTime(0) + " Minutes)";
            }

            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getend_millis), String.valueOf(observationEditViewModel.getData().getEndDate()));
            mFilterBaseDatas.add(mFilterBaseData);

            //Agent Score
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_agent_score));
            mFilterBaseData.id = "0";
            mFilterBaseData.isEditable = 0;
            mFilterBaseData.name = String.valueOf(observationEditViewModel.getData().getScoreVal()) + "%";
            mFilterBaseDatas.add(mFilterBaseData);

            //Great Job On
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_great_job_on));
            mFilterBaseData.id = "0";
            mFilterBaseData.isEditable = 0;
            String greatJobOn = "";
            for(int i = 0; i < observationEditViewModel.getData().getStrengths().size(); i++){
                if(greatJobOn.trim().length() == 0){
                    greatJobOn = observationEditViewModel.getData().getStrengths().get(i).getName();
                } else{
                    greatJobOn = greatJobOn + "," + observationEditViewModel.getData().getStrengths().get(i).getName();
                }
            }
            mFilterBaseData.name = greatJobOn;
            mFilterBaseDatas.add(mFilterBaseData);


            //Display on team member dashboard?
            if(isOnlyView){
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_survey_display_on_team_member));
                mFilterBaseData.id = "0";
                mFilterBaseData.isEditable = 0;
                if(observationEditViewModel.getData().getDonotDisplayToAgent()){
                    mFilterBaseData.name = "No";
                } else{
                    mFilterBaseData.name = "Yes";
                }
                mFilterBaseDatas.add(mFilterBaseData);
            }

            //Send an Email to the Team Member
            if(isOnlyView){
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_survey_send_email));
                mFilterBaseData.id = "0";
                mFilterBaseData.isEditable = 0;
                if(observationEditViewModel.getData().getDonotMailToAgent()){
                    mFilterBaseData.name = "No";
                } else{
                    mFilterBaseData.name = "Yes";
                }
                mFilterBaseDatas.add(mFilterBaseData);
            }

            //Area of Improvment
            if(observationEditViewModel.getData().getNote() != null && observationEditViewModel.getData().getNote().length() > 0){
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_area_of_improvment));
                mFilterBaseData.id = "0";
                mFilterBaseData.isEditable = 0;
                mFilterBaseData.name = observationEditViewModel.getData().getNote();
                mFilterBaseDatas.add(mFilterBaseData);

            }

            //Oppportunity
            if(observationEditViewModel.getData().getSymptomText() != null && observationEditViewModel.getData().getSymptomText().length() > 0){
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_area_of_opportunity));
                mFilterBaseData.id = "0";
                mFilterBaseData.isEditable = 0;
                mFilterBaseData.name = observationEditViewModel.getData().getSymptomText();
                mFilterBaseDatas.add(mFilterBaseData);
            }

        } else{
            //Start Date Time
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_start_date));
            mFilterBaseData.id = "0";

            if(isPending){
                mFilterBaseData.isEditable = 1;
                if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_getstarted_millis), null) == null){
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getstarted_millis), String.valueOf(observationEditViewModel.getData().getStartDate()));
                    mFilterBaseData.name = DateTimeUtils.DateObservationCounterCoachingUTC(observationEditViewModel.getData().getStartDate());
                } else{
                    mFilterBaseData.name = DateTimeUtils.DateObservationCounterCoachingUTC(Long.parseLong(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_getstarted_millis), null)));
                }
            } else{
                mFilterBaseData.isEditable = 0;
                mFilterBaseData.name = sdf.format(date);
            }

            mFilterBaseDatas.add(mFilterBaseData);
        }
        setFilterAdapter(mFilterBaseDatas);
    }

    String secToTime(int sec){
        int second = sec % 60;
        int minute = sec / 60;
        if(minute >= 60){
            int hour = sec / 60;
            minute %= 60;
            return hour + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
        }
        return minute + ":" + (second < 10 ? "0" + second : second);
    }

    private void setFilterAdapter(ArrayList<FilterBaseData> mFilterBaseDataArrayList){
        btnGetStarted.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        rvObserveAgentFilter.setLayoutManager(mLayoutManager);
        rvObserveAgentFilter.setItemAnimator(new DefaultItemAnimator());
        rvObserveAgentFilter.setVisibility(View.GONE);
        rvObserveAgentFilter.setVisibility(View.VISIBLE);
        listadapter = new FilterListForObservation(mHomeActivity, mFilterBaseDataArrayList, this, false);
        rvObserveAgentFilter.setAdapter(listadapter);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
            mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_observe_agent)));
        } else{
            mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_coaching_team_member)));
        }
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.ibBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mHomeActivity.onBackPressed();
            }
        });
    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterposition, boolean isleftFilter){
        sendListData(position, filterposition);
    }

    public void sendListData(int position, int filterposition){
        Bundle mBundle = new Bundle();
        switch(position){
            case 0:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mAccessibleTenantLocationDataModelForObserveAgent);
                break;
            case 1:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mPerformanceLeaderModel);
                break;
            case 2:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mAgentModel);
                break;
            case 3:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mObservationListModel);
                break;

        }
        FilterBaseData mFilterBaseData = mFilterBaseDatas.get(filterposition);
        mBundle.putParcelable(KEY_FILTER_SELECTION, mFilterBaseData);
        mBundle.putInt(KEY_SELECTION_POSITION, position);
        FilterDetailListFragmentForObservation mFilterDetailListFragmentforObservation = new FilterDetailListFragmentForObservation();
        mFilterDetailListFragmentforObservation.setArguments(mBundle);
        if(mFilterBaseDatas.get(position).isEditable == 1 && position != 4){
            mHomeActivity.replace(mFilterDetailListFragmentforObservation, mContext.getResources().getString(R.string.tag_filter_detail_list));
        } else if(mFilterBaseDatas.get(position).isEditable == 1 && position == 4){
            showDatePicker();
        }
    }

    private void showDatePicker(){
        TimePickerDialog mTimePicker = null;
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute){
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                calendar.set(Calendar.MINUTE, selectedMinute);
                mFilterBaseDatas.get(4).name = DateTimeUtils.DateObservationCounterCoachingNOUTC(calendar.getTimeInMillis());

                if(observationEditViewModel.getData().getSurveyTakenTime() != null){
                    long endDateMillis = calendar.getTimeInMillis() + TimeUnit.SECONDS.toMillis(observationEditViewModel.getData().getSurveyTakenTime());
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getstarted_millis), String.valueOf(calendar.getTimeInMillis()));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getend_millis), String.valueOf(endDateMillis));
                    obsTime = DateTimeUtils.DateObservationCounterCoachingNOUTC(endDateMillis) + "\n" + secToTime(observationEditViewModel.getData().getSurveyTakenTime()) + " Minutes)";
                    mFilterBaseDatas.get(4).name = DateTimeUtils.DateObservationCounterCoachingNOUTC(endDateMillis) + "\n" + secToTime(observationEditViewModel.getData().getSurveyTakenTime()) + " Minutes)";

                } else{
                    long endDateMillis = calendar.getTimeInMillis() + TimeUnit.SECONDS.toMillis(0);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getstarted_millis), String.valueOf(calendar.getTimeInMillis()));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getend_millis), String.valueOf(endDateMillis));
                    obsTime = DateTimeUtils.DateObservationCounterCoachingNOUTC(endDateMillis) + "\n" + secToTime(0) + " Minutes)";
                    // mFilterBaseDatas.get(4).name = DateTimeUtils.DateObservationCounterCoachingNOUTC(endDateMillis) + "\n" + secToTime(0) + " Minutes)";
                }
                listadapter.notifyDataSetChanged();

            }
        }, hour, minute, true);//Yes 24 hour time
        final TimePickerDialog finalMTimePicker = mTimePicker;
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener(){
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        finalMTimePicker.show();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        //datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    @Override
    public void onAttachFragment(Fragment childFragment){
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onFilterDetailsClicked(FilterBaseData mFilterBaseData, int clickedPositon, int filterpositoin){
        if(mFilterBaseData != null)
            updateFilterData(mFilterBaseData, clickedPositon);
    }

    public void updateFilterData(FilterBaseData mFilterBaseData, int clickedPosition){
        mFilterBaseDatas.set(0, mFilterBaseData);
        listadapter.notifyDataSetChanged();
    }

    public void refereshSurveyVariables(){
        mHomeActivity.agentScore = "0";
        mHomeActivity.areaOfImprovment = "";
        mHomeActivity.isFromDashboard = false;
        mHomeActivity.isPending = false;
        mHomeActivity.isOnlyView = false;
        mHomeActivity.donotMailToAgent = false;
        mHomeActivity.donotDisplayToAgent = false;
        mHomeActivity.eventID = "";
        mHomeActivity.arrayObsQuestion = null;

        if(isOnlyView){
            // This is For Only View Survey
        } else if(!isOnlyView && isFromDashboard){
            // This is For Edit Survey
            mHomeActivity.mObservationJobType=null;
        } else{

            //This is for New Survey
            if(mHomeActivity.mObservationJobType != null && (mHomeActivity.mObservationJobType.getData() != null &&
                    mHomeActivity.mObservationJobType.getData().size() > 0)){
                for(int position = 0; position < mHomeActivity.mObservationJobType.getData().size(); position++){
                    mHomeActivity.mObservationJobType.getData().get(position).setSelected(false);
                }
            }
        }


    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.btn_get_started:{

                refereshSurveyVariables();
                if(observationEditViewModel != null && observationEditViewModel.getData().getQuestionnaireQuestionSections().size() == 0){
                    new AlertDialog.Builder(mContext)
                            .setMessage("No Questions available for this event")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){
                                    dialog.dismiss();

                                }
                            }).show();
                    return;
                }
                TakeSurvey();
                /*if ((!isOnlyView && !(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_tip_remember), false)))) {
                    TakeSurvey();
                } else {
                    TakeSurvey();
                }*/

                break;
            }
        }
    }

    private void TakeSurvey(){

        if(!isFromDashboard){
            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getstarted_millis), String.valueOf(System.currentTimeMillis()));
        }
        if(isPending){
            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getstarted_millis), String.valueOf(System.currentTimeMillis()));
        }

        SurveyQuestionnaireFragment mSurveyQuestionnaireFragment = new SurveyQuestionnaireFragment();
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("isFromDashboard", isFromDashboard);

        if(observationEditViewModel != null){

            mBundle.putString("areaOfImprovment", observationEditViewModel.getData().getNote());
            mBundle.putBoolean("donotMailToAgent", observationEditViewModel.getData().getDonotMailToAgent());
            mBundle.putBoolean("donotDisplayToAgent", observationEditViewModel.getData().getDonotDisplayToAgent());
            mBundle.putString("eventID", String.valueOf(observationEditViewModel.getData().getId()));
        }

        mBundle.putBoolean("isPending", isPending);
        mBundle.putBoolean("isOnlyView", isOnlyView);
        mSurveyQuestionnaireFragment.setArguments(mBundle);
        mHomeActivity.replace(mSurveyQuestionnaireFragment, mHomeActivity.getResources().getString(R.string.tag_survey_question_fragment));
    }
}
