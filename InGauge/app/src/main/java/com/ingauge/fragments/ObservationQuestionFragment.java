package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.ObservationTabActivity;
import com.ingauge.adapters.ObservationQuestionAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.ObsQuestionModel;
import com.ingauge.pojo.ObsQuestionModelLocal;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pathanaa on 13-07-2017.
 */

public class ObservationQuestionFragment extends Fragment {
    private ViewPager mViewPagerQuestion;
    private View rootView;
    private ObservationQuestionAdapter observationQuestionAdapter;
    private Context mContext;
    public ObsQuestionModel mObsQuestionModel;
    private APIInterface apiInterface;

    private ObsQuestionModelLocal obsQuestionModelLocal;

    private HomeActivity mHomeActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_obseravtion_question, container, false);
        Init();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity)mContext;
    }

    private void Init() {
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mViewPagerQuestion = (ViewPager) rootView.findViewById(R.id.viewpager_question);

        if ((!mHomeActivity.isFromDashboard && mHomeActivity.arrayObsQuestion == null) ||
                (mHomeActivity.isPending && mHomeActivity.arrayObsQuestion == null)) {
            GetObsQuestions(InGaugeSession.read(getString(R.string.key_auth_token), ""), "application/json", String.valueOf(InGaugeSession.read(getString(R.string.key_selected_industry_id), 0)), true, InGaugeSession.read(getString(R.string.key_obs_selected_observation_id), "0"));
        } else if (mHomeActivity.isFromDashboard && mHomeActivity.arrayObsQuestion == null && !mHomeActivity.isPending) {
            mHomeActivity.arrayObsQuestion = new ArrayList<ObsQuestionModelLocal>();
            for (int i = 0; i < mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().size(); i++) {
                for (int j = 0; j < mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().size(); j++) {
                    obsQuestionModelLocal = new ObsQuestionModelLocal();
                    obsQuestionModelLocal.setQ_title(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getName());
                    obsQuestionModelLocal.setQ_text(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getQuestionText());
                    obsQuestionModelLocal.setQ_yes_nona(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getType());
                    int AnswerId = mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getId();
                    String applicableType = UiUtils.AnswerNo();
                    for (int k = 0; k < mHomeActivity.observationEditViewModel.getData().getQuestionAnswer().size(); k++) {
                        if (AnswerId == mHomeActivity.observationEditViewModel.getData().getQuestionAnswer().get(k).getQuestionId()) {
                            applicableType = mHomeActivity.observationEditViewModel.getData().getQuestionAnswer().get(k).getAnswer();
                            Logger.Error("TRUE====>" + mHomeActivity.observationEditViewModel.getData().getQuestionAnswer().get(k).getId());
                            break;
                        }
                    }
                    obsQuestionModelLocal.setQ_yesnona_applicable(applicableType);
                    obsQuestionModelLocal.setQ_weight(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getWeight());
                    obsQuestionModelLocal.setQ_degree(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getDegreeOfImportance());
                    obsQuestionModelLocal.setQ_sym(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getSymptom());
                    obsQuestionModelLocal.setQ_id(String.valueOf(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getId()));
                    obsQuestionModelLocal.setSection(i + 1);
                    obsQuestionModelLocal.setQ_number(j+1);

                    mHomeActivity.arrayObsQuestion.add(obsQuestionModelLocal);
                }

                mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score)+" "+ mHomeActivity.observationEditViewModel.getData().getScoreVal() + " %");
                mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
                observationQuestionAdapter = new ObservationQuestionAdapter(mContext, mHomeActivity.arrayObsQuestion, mViewPagerQuestion, mHomeActivity);
                mViewPagerQuestion.setAdapter(observationQuestionAdapter);
            }
        } else {
            mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
            observationQuestionAdapter = new ObservationQuestionAdapter(mContext, mHomeActivity.arrayObsQuestion, mViewPagerQuestion, mHomeActivity);
            mViewPagerQuestion.setAdapter(observationQuestionAdapter);
        }

    }

    void GetObsQuestions(String authToken, String ContentType, String IndustryId, boolean isMobile, String observationId) {
        Call mCall = apiInterface.GetObsQuestions(observationId);
        mHomeActivity.startProgress(mContext);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mHomeActivity.endProgress();
                mObsQuestionModel = (ObsQuestionModel) response.body();
                mHomeActivity.arrayObsQuestion = new ArrayList<ObsQuestionModelLocal>();
                if (response.body() != null) {
                    for (int i = 0; i < mObsQuestionModel.getData().getQuestionSections().size(); i++) {
                        for (int j = 0; j < mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().size(); j++) {
                            obsQuestionModelLocal = new ObsQuestionModelLocal();
                            obsQuestionModelLocal.setQ_title(mObsQuestionModel.getData().getQuestionSections().get(i).getName());
                            obsQuestionModelLocal.setQ_text(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getQuestionText());
                            obsQuestionModelLocal.setQ_yes_nona(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getType());
                            obsQuestionModelLocal.setQ_yesnona_applicable("No");
                            obsQuestionModelLocal.setQ_weight(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getWeight());
                            obsQuestionModelLocal.setQ_degree(mObsQuestionModel.getData().getQuestionSections().get(i).getDegreeOfImportance());
                            obsQuestionModelLocal.setQ_sym(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getSymptom());
                            obsQuestionModelLocal.setQ_id(String.valueOf(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getId()));
                            obsQuestionModelLocal.setSection(i + 1);
                            obsQuestionModelLocal.setQ_number(j+1);
                            mHomeActivity.arrayObsQuestion.add(obsQuestionModelLocal);
                        }
                    }

                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score)+" "+  UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
                    observationQuestionAdapter = new ObservationQuestionAdapter(mContext, mHomeActivity.arrayObsQuestion, mViewPagerQuestion, mHomeActivity);
                    mViewPagerQuestion.setAdapter(observationQuestionAdapter);
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.endProgress();
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mHomeActivity.endProgress();
            }
        });
    }

    public static Fragment newInstance(String text, int color) {
        Fragment frag = new ObservationQuestionFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

}
