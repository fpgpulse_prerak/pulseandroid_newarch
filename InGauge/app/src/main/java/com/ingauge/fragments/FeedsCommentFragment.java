package com.ingauge.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.FeedCommentListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.FeedsModelList;
import com.ingauge.pojo.SaveFeedCommentModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by desainid on 6/15/2017.
 */

public class FeedsCommentFragment extends DialogFragment implements View.OnClickListener{

    private TextView tvTitle;
    private TextView tvDesc;
    private TextView tvRole;
    private TextView tvTime;
    private CircleImageView img_profile;
    private WebView webGraph;
    private ProgressBar mProgressBar;
    private RelativeLayout relativeShare;
    private HomeActivity mHomeactivity;
    private ListView listViewComment;
    private LinearLayout rowLinearComment;
    private TextView tvLike;
    private TextView tvLikeCount;
    private TextView tvCommentCount;
    private ImageView imgLike;
    FeedCommentListAdapter feedCommentListAdapter;
    List<String> valuesComment;
    List<String> valuesName;
    List<Integer> userID;
    List<FeedsModelList.FeedComment> feedCommentList;

    EditText edtComment;
    FeedsModelList feedModellist;
    List<FeedsModelList.Datum> mDatumList;
    int position;
    boolean isAlreadyLike;
    Call mCall;
    ScrollView scroll;
    private View listheader;
    private LayoutInflater myListViewinflter;
    Context mContext;
    APIInterface apiInterface;
    long mLastClickTime = 0;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeactivity = (HomeActivity) mContext;
        mHomeactivity.toolbar.setVisibility(View.GONE);
    }

    private ImageView mImgSend;
    //  private UpdateCommentsListener updateCommentsListener;
    FeedsFragment mFeedsFragment;
    public static String KEY_FEEDS_BROADCAST = "key_feeds_broadcast";
    public static String KEY_FEEDS_DATA = "key_feeds_data";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        List<Fragment> mFragmentList = getFragmentManager().getFragments();
        for(int i = 0; i < mFragmentList.size(); i++){
            if(mFragmentList.get(i) instanceof FeedsFragment){
                onAttachToParentFragment(mFragmentList.get(i));
                break;
            }
        }
    }

    private void onAttachToParentFragment(Fragment fragmentByTag){
        try{
            //  updateCommentsListener = (UpdateCommentsListener) fragmentByTag;
        } catch(Exception e){
            e.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        mContext = getActivity();
        mHomeactivity = (HomeActivity) mContext;
        mHomeactivity.nBottomNavigationView.setVisibility(View.GONE);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        View view = inflater.inflate(R.layout.fragment_feeds_comment, container, false);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rel_layout_bg);
        mImgSend = (ImageView) view.findViewById(R.id.img_send);
        mHomeactivity.toolbar.setVisibility(View.GONE);
        valuesComment = new ArrayList<>();
        valuesName = new ArrayList<>();
        userID = new ArrayList<>();
        feedCommentList = new ArrayList<>();

        if(getArguments() != null){
            position = getArguments().getInt(FeedsFragment.KEY_SELECTED_POSITION, position);
            getData(position);
        }
        listViewComment = (ListView) view.findViewById(R.id.list_view_comment);
        feedCommentListAdapter = new FeedCommentListAdapter(getActivity(), valuesComment, valuesName, userID);

        listheader = inflater.inflate(R.layout.row_item_feeds, null);
        listViewComment.addHeaderView(listheader);

        initViews(view);
        listViewComment.setAdapter(feedCommentListAdapter);
        scrollMyListViewToBottom();
        WebSettings wv_settings = webGraph.getSettings();
        mProgressBar.setVisibility(View.GONE);
        webGraph.setVisibility(View.VISIBLE);
        webGraph.setWebChromeClient(new WebChromeClient(){
            public boolean onConsoleMessage(ConsoleMessage cm){
                Logger.Error("console Message" + cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId());
                return true;
            }
        });


        wv_settings.setDomStorageEnabled(true);
        webGraph.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request){
                return false;
            }
        });
        webGraph.clearCache(true);
        webGraph.clearHistory();
        wv_settings.setJavaScriptEnabled(true);
        wv_settings.setJavaScriptCanOpenWindowsAutomatically(true);
        webGraph.loadUrl(UiUtils.baseUrlChart + "feeds/" + mDatumList.get(position).getId());
        webGraph.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int progress){
                mProgressBar.setVisibility(View.VISIBLE);
                webGraph.setVisibility(View.GONE);
                Logger.Error("Web Progress " + progress);
                if(progress == 100){
                    mProgressBar.setVisibility(View.GONE);
                    webGraph.setVisibility(View.VISIBLE);
                }
            }


        });
        String imageUrl = UiUtils.baseUrl + "api/user/avatar/1/" + mDatumList.get(position).getCreatedBy().getId();
        Glide.with(mContext)
                .load(imageUrl)
                .centerCrop()
                .error(R.drawable.placeholder)
                .into(img_profile);
        tvDesc.setText(mDatumList.get(position).getDescription());
        tvTitle.setText(mDatumList.get(position).getCreatedBy().getName());
        tvRole.setText(mDatumList.get(position).getCreatedBy().getJobTitle());
       /* String startDateString = mDatumList.get(position).getCreatedOn();
        try{
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = df.parse(startDateString);
            Logger.Error("Time Stamp" + startDateString);
            Logger.Error("Time Zone " + df.getTimeZone());
            Logger.Error("Time Zone " + df.getTimeZone().getDisplayName());
            String aTimeAgoString = UiUtils.calculateFeedTime(date.getTime());
            tvTime.setText(aTimeAgoString);
        } catch(ParseException e){
            e.printStackTrace();
            tvTime.setText("-");
        }
*/

        String startDateString = mDatumList.get(position).getCreatedOn();
        Logger.Error("  Date Created ON  " + startDateString);
        try{
//            2017-12-14 05:44:38

            String dateStr = startDateString;
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(dateStr);
            df.setTimeZone(TimeZone.getDefault());
            String formattedDate = df.format(date);

            SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            _24HourSDF.setTimeZone(TimeZone.getDefault());

            SimpleDateFormat _12HourSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            _12HourSDF.setTimeZone(TimeZone.getDefault());
            Date _24HourDt = _24HourSDF.parse(formattedDate);

            _24HourSDF.setTimeZone(TimeZone.getDefault());
            _12HourSDF.setTimeZone(TimeZone.getDefault());
            Logger.Error("<<<<<<  Twenty Four " + _24HourDt);
            Logger.Error("<<<<<<  Twelve  " + _12HourSDF.format(_24HourDt));
            System.out.println(_24HourDt);
            System.out.println(_12HourSDF.format(_24HourDt));


            long now = System.currentTimeMillis();
            Date mDate1 = new Date();

            mDate1.getTime();
            String datetime1 = _12HourSDF.format(_24HourDt);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
            Date convertedDate = dateFormat.parse(datetime1);


            Calendar c = Calendar.getInstance();
            Date datetoday = c.getTime(); //current date and time in UTC
            SimpleDateFormat todaySDf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            todaySDf.setTimeZone(TimeZone.getDefault()); //format in given timezone
            String strDate = todaySDf.format(datetoday);
            try{
                Calendar todayFinalCalendar = UiUtils.DateToCalendar(todaySDf.parse(strDate));

                CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                        convertedDate.getTime(),
                        todayFinalCalendar.getTimeInMillis(),
                        DateUtils.FORMAT_ABBREV_ALL);


                //tvTime.setText(getTimeAgo(todayFinalCalendar.getTime(), convertedDate));

                if(getTimeAgo(todayFinalCalendar.getTime(), convertedDate) != null)
                    tvTime.setText(getTimeAgo(todayFinalCalendar.getTime(), convertedDate));
                else{

                    tvTime.setText(String.valueOf(0) + 'h');
                }


            } catch(ParseException e){
                e.printStackTrace();
            }


        } catch(final ParseException e){
            e.printStackTrace();
        }

        mImgSend.setOnClickListener(this);
        return view;
    }

    public String getTimeAgo(Date current, Date createdDate){
        // Get msec from each, and subtract.
        long diff = current.getTime() - createdDate.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (1000 * 3600 * 24);

        Logger.Error("<<<< Day Difference >>>>" + diffDays);
        String time = null;

        if(diffDays > 0){
            if(diffDays == 1){
                time = diffDays + "d";
            } else{
                time = diffDays + "d";
            }
        } else{
            if(diffHours > 0){
                if(diffHours == 1){
                    time = diffHours + "h";
                } else{
                    time = diffHours + "h";
                }
            } else{
                if(diffMinutes > 0){
                    if(diffMinutes == 1){
                        //time = diffMinutes + "min ago";
                        time = 0 + "h";
                    } else{
                        //time = diffMinutes + "mins ago";
                        time = 0 + "h";
                    }
                } else{
                    if(diffSeconds > 0){
                        //time = diffSeconds + "secs ago";
                        time = 0 + "h";
                    }
                }

            }

        }
        return time;
    }

    private void SaveCommentAPI(String authToken, HashMap<String, Object> commentMap){
        mCall = apiInterface.saveFeedComment(commentMap);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){

                    if(response.code() == 200){
                        Logger.Error("<<<<< Status Code : >>>> " + response.code());
                        SaveFeedCommentModel saveFeedComment = (SaveFeedCommentModel) response.body();
                        FeedsModelList.FeedComment feedComment = new FeedsModelList.FeedComment();
                        feedComment.setUser(saveFeedComment.data.user);
                        feedComment.setText(edtComment.getText().toString().trim());
                        valuesName.add(feedComment.getUser().name);

                        mDatumList.get(position).noOfComments++;
                        int feedsCommentsSize = mDatumList.get(position).getFeedComments().size();
                        tvCommentCount.setText(String.valueOf(mDatumList.get(position).noOfComments));
                        scrollMyListViewToBottom();
                        feedModellist.getData().get(position).getFeedComments().add(feedComment);

                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        bundle.putString("industry", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
                        bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
                        bundle.putInt("feed_id", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_feed_id), 0));
                        bundle.putString("comment", edtComment.getText().toString());
                        InGaugeApp.getFirebaseAnalytics().logEvent("comment_feed", bundle);

                        edtComment.setText("");
                    } else if(response.code() == 500){

                        Toast.makeText(mHomeactivity,"There's some issue. Please contact System Administrator",Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                Logger.Error("<<<< Exception : " + t.getMessage());
            }
        });
    }

    private void scrollMyListViewToBottom(){
        listViewComment.post(new Runnable(){
            @Override
            public void run(){
                // Select the last row so it will scroll into view...
                /*if (feedCommentListAdapter.getCount() > 1)*/
                listViewComment.setSelection(feedCommentListAdapter.getCount() - 1);
            }
        });
    }

    private void getData(int position){
        feedModellist = (FeedsModelList) getArguments().getSerializable(FeedsFragment.KEY_FEED_MODEL_LIST);
        mDatumList = feedModellist.getData();
        feedCommentList = feedModellist.getData().get(position).getFeedComments();
        for(int i = 0; i < feedCommentList.size(); i++){
            valuesComment.add(feedCommentList.get(i).getText());
            valuesName.add(feedCommentList.get(i).getUser().name);
            userID.add(feedCommentList.get(i).getUser().id);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog){
        super.onDismiss(dialog);
        mHomeactivity.visibleBottomView();
        mHomeactivity.toolbar.setVisibility(View.VISIBLE);
        try{
            Intent intent = new Intent(KEY_FEEDS_BROADCAST);
            Bundle mBundle = new Bundle();
            mBundle.putSerializable(KEY_FEEDS_DATA, feedModellist);
            intent.putExtras(mBundle);
            mHomeactivity.sendBroadcast(intent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onStart(){
        super.onStart();
        Dialog dialog = getDialog();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    private void initViews(View view){
        img_profile = (CircleImageView) listheader.findViewById(R.id.img_profile);
        webGraph = (WebView) listheader.findViewById(R.id.web_graph);
        mProgressBar = (ProgressBar) listheader.findViewById(R.id.progress_row);
        tvDesc = (TextView) listheader.findViewById(R.id.row_item_feeds_desc);
        tvTitle = (TextView) listheader.findViewById(R.id.row_item_feeds_tv_title);
        tvRole = (TextView) listheader.findViewById(R.id.row_item_feeds_tv_role);
        tvTime = (TextView) listheader.findViewById(R.id.row_item_feeds_tv_time);
        tvLike = (TextView) listheader.findViewById(R.id.tv_like);
        imgLike = (ImageView) listheader.findViewById(R.id.img_like);
        tvLike.setOnClickListener(this);
        tvLikeCount = (TextView) view.findViewById(R.id.tv_like_count);
        tvCommentCount = (TextView) view.findViewById(R.id.tv_comment_count);

        if(mDatumList.get(position).isLiked){
            tvLike.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
            tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
            tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
            imgLike.setBackgroundResource(R.drawable.ic_liked);
        } else{
            imgLike.setBackgroundResource(R.drawable.ic_like);
        }

        tvCommentCount = (TextView) view.findViewById(R.id.tv_comment_count);
        tvLikeCount.setText(String.valueOf(mDatumList.get(position).noOfLikes));
        tvCommentCount.setText(String.valueOf(mDatumList.get(position).noOfComments));
        tvLikeCount.setText(String.valueOf(mDatumList.get(position).noOfLikes));
        edtComment = (EditText) view.findViewById(R.id.edt_comment);
        relativeShare = (RelativeLayout) listheader.findViewById(R.id.rel_share);
        rowLinearComment = (LinearLayout) listheader.findViewById(R.id.row_item_feeds_ll_comment);
        rowLinearComment.setVisibility(View.VISIBLE);
        relativeShare.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view){


        switch(view.getId()){
            case R.id.tv_like:
                if(mDatumList.get(position).isLiked){
                    isAlreadyLike = true;
                } else{
                    isAlreadyLike = false;
                }
                saveLikeFeed(InGaugeSession.read(mContext.getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getString(R.string.key_selected_feed_id), 0), InGaugeSession.read(mContext.getString(R.string.key_user_id), 0));
                break;

            case R.id.img_send:
                if(SystemClock.elapsedRealtime() - mLastClickTime < 2500){
                    break;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if(edtComment.getText().toString().trim().length() > 0){
                    valuesComment.add(edtComment.getText().toString());
                    userID.add(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                    try{
                        HashMap<String, Object> CommentMap = new HashMap<String, Object>();
                        CommentMap.put("userId", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        CommentMap.put("mapTo", "FEEDCOMMENT");
                        CommentMap.put("text", "" + edtComment.getText().toString());
                        CommentMap.put("feedId", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_feed_id), 0));
                        CommentMap.put("industryId", String.valueOf(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0)));

                        SaveCommentAPI(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), CommentMap);
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }
                break;
            default:
                break;

        }

    }

    //Making Listener to refresh Feed Comments
    public FeedCommentRefreshListener getFeedCommentRefreshListener(){
        return feedCommentRefreshListener;
    }

    public void setFragmentRefreshListener(FeedCommentRefreshListener feedCommentRefreshListener){
        this.feedCommentRefreshListener = feedCommentRefreshListener;
    }

    private FeedCommentRefreshListener feedCommentRefreshListener;


    public interface FeedCommentRefreshListener{
        void onCommentRefresh();
    }

    public void saveLikeFeed(String authToken, int feedId, int userId){
        mCall = apiInterface.saveLikeFeed(feedId, userId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    int count = Integer.parseInt(tvLikeCount.getText().toString());
                    if(isAlreadyLike){
                        tvLikeCount.setText(String.valueOf(count - 1));
                        tvLike.setTextColor(mContext.getResources().getColor(R.color.darker_grey));
                        tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.darker_grey));
                        imgLike.setBackgroundResource(R.drawable.ic_like);
                        mDatumList.get(position).isLiked = false;
                        mDatumList.get(position).noOfLikes = count - 1;
                    } else{
                        tvLikeCount.setText(String.valueOf(count + 1));
                        tvLike.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
                        tvLikeCount.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue_light));
                        imgLike.setBackgroundResource(R.drawable.ic_liked);
                        mDatumList.get(position).isLiked = true;
                        mDatumList.get(position).noOfLikes = count + 1;
                    }
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeactivity.ForceLogout();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t){

            }
        });
    }


}
