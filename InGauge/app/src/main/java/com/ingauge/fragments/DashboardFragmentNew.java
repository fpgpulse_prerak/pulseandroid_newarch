package com.ingauge.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.DashboardPanelAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.fragments.leaderboard.FragmentLeaderBoard;
import com.ingauge.listener.RecyclerViewClickListenerForDashboardDetails;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.CustomDashboardModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardModelList;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.DynamicData;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;
import com.ingauge.widget.MonthYearPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by mansurum on 18-May-17.
 */

public class DashboardFragmentNew extends BaseFragment implements View.OnClickListener, RecyclerViewClickListenerForDashboardDetails, DatePickerDialog.OnDateSetListener{
    Context mContext;

    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private RecyclerView rvDashboardList;
    public static ImageView imgFilter;
    ArrayList<FilterBaseData> mFilterBaseDatas = new ArrayList<>();
    ArrayList<FilterBaseData> mFilterBaseDatasRegion = new ArrayList<>();
    ArrayList<FilterBaseData> mFilterBaseDatasTenantLocation = new ArrayList<>();
    ArrayList<FilterBaseData> mFilterBaseDatasLocationgroup = new ArrayList<>();
    ArrayList<FilterBaseData> mFilterBaseDatasProduct = new ArrayList<>();
    ArrayList<FilterBaseData> mFilterBaseDatasuser = new ArrayList<>();
    APIInterface apiInterface;
    Calendar calendar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    Call mCall;
    private TextView tvSelectDate;
    private TextView tvSelectPreviousDate;
    RelativeLayout relativeDateCompare;
    private ProgressBar pbFilter;
    private HomeActivity mHomeActivity;
    public static RelativeLayout rlProgress;
    List<CustomDashboardModel.CustomReport> mCustomReportList = new ArrayList<>();
    private TextView tvNoData;
    private LinearLayout llPlaceHolderNoData;
    int selectedDashboardId = -1;
    private String dashboardName = "Dashboard";
    private boolean isDashboardMTD = false;
    private boolean isHideCompareDate = true;
    private List<String> mFilterListString = new ArrayList<>();
    private View view;
    List<DynamicData> mCountryList;
    JSONObject mDataJson = null;

    public Map<Integer, List<DynamicData>> integerListHashMap = new LinkedHashMap<>();

    private String selectedRegionId = "";
    private String selectedRegionName = "";
    private String selectedRegionTypeId = "";
    private String selectedRegionTypeName = "";
    private boolean isNeedtoFilterCheck = true;


    ArrayList<FilterBaseData> masterFilterBaseDatas = new ArrayList<>();
    Date date = null;
    Date firstDateOfPreviousMonth = null;
    Date prevDate = null;

    private DateFormat dateFormatMonth;
    private DateFormat dateFormatYear;
    private String STR_MONTH[] = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    int savedMonth;
    int savedYear;
    public static String KEY_SELECTED_DAY = "key_selected_day";
    public static String KEY_SELECTED_MONTH = "key_selected_month";
    public static String KEY_SELECTED_YEAR = "key_selected_year";
    Calendar selectedCalendar = Calendar.getInstance();
    int selectedMonth = 0;
    int selectedYear = 0;
    int selectedDay = 0;
    public boolean isCheckDone = false;
    public boolean isChildDashboard = false;

    SimpleDateFormat sdfServerDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

    private RelativeLayout rlLeaderBoard;

    int countforFilters = 0;
    int countFordashboard = 0;
    boolean isGuideEnableFromLogin = false;
    boolean isGuideEnableFromChangetenant = false;
    boolean isNeedtoUpdateSavedFilterForLb = true;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }


    public static Fragment newInstance(String text, int color){
        Fragment frag = new DashboardFragmentNew();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mFilterBaseDatas = new ArrayList<>();
        mCountryList = new ArrayList<>();
        apiInterface = APIClient.getClient().create(APIInterface.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment

        if(view == null){
            view = inflater.inflate(R.layout.dashboard_fragment, container, false);
            rvDashboardList = (RecyclerView) view.findViewById(R.id.rv_feeds_list);
            rlLeaderBoard = (RelativeLayout) view.findViewById(R.id.dashboard_fragment_rl_leaderboard);
            rlLeaderBoard.setOnClickListener(this);
            GridLayoutManager mGridLayoutManager = new GridLayoutManager(mHomeActivity, 2);
            rvDashboardList.setHasFixedSize(true);
            rvDashboardList.setLayoutManager(mGridLayoutManager);

            tvNoData = (TextView) view.findViewById(R.id.tv_nodata);
            llPlaceHolderNoData = (LinearLayout)view.findViewById(R.id.dashboard_fragment_ll_placeholder);
            relativeDateCompare = (RelativeLayout) view.findViewById(R.id.relative_date_compare);

            imgFilter = (ImageView) view.findViewById(R.id.img_filter);
            tvSelectDate = (TextView) view.findViewById(R.id.tv_selectdate);
            tvSelectPreviousDate = (TextView) view.findViewById(R.id.tv_previousdate);

            pbFilter = (ProgressBar) view.findViewById(R.id.dashboard_fragment_pb);
            imgFilter.setOnClickListener(this);
            calendar = Calendar.getInstance();
            rlProgress = (RelativeLayout) view.findViewById(R.id.custom_progress_rl_main);
            rvDashboardList.setVisibility(View.VISIBLE);
            rlProgress.setVisibility(View.VISIBLE);
            relativeDateCompare.setOnClickListener(this);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            date = calendar.getTime();


            Calendar cal = Calendar.getInstance();

            cal.setTime(new Date());
            cal.add(Calendar.MONTH, -1);
            cal.set(Calendar.DATE, 1);
            firstDateOfPreviousMonth = cal.getTime();


            Calendar previouslastDate = Calendar.getInstance();
            previouslastDate.setTime(new Date());
            int daysInMonth = previouslastDate.getActualMaximum(Calendar.DAY_OF_MONTH);
            if(daysInMonth == 31){
                daysInMonth = daysInMonth - 1;
            } else{
                daysInMonth = daysInMonth + 1;
            }

            Logger.Error("Days  " + daysInMonth);
            previouslastDate.add(Calendar.DATE, -daysInMonth);  //not sure
            prevDate = previouslastDate.getTime();
            mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
            mSwipeRefreshLayout.setColorSchemeResources(
                    R.color.in_guage_blue,
                    R.color.rv_grey_color,
                    R.color.red);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
                @Override
                public void onRefresh(){
                    // Refresh items
                    refreshItems();
                }
            });

            selectedDashboardId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_id), -1);
            dashboardName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard), "");
            isDashboardMTD = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_mtd), false);
            // This is Change for IN-830 Jira to pass the satatic false because to hide the compare date

            isHideCompareDate = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), true);

/*            if (!(selectedDashboardId == -1)) {
                getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
            } else {
                setNeedtoFilterCheck(true);
                GetDashboard(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), true, false);

            }*/
            Date mDateMTD = new Date();
            dateFormatMonth = new SimpleDateFormat("MM");
            dateFormatYear = new SimpleDateFormat("yyyy");


            savedMonth = Integer.parseInt(dateFormatMonth.format(mDateMTD));
            savedYear = Integer.parseInt(dateFormatYear.format(mDateMTD));
            Logger.Error("Month In text View " + STR_MONTH[savedMonth - 1]);
            Logger.Error("Year In text View " + savedYear);
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "").equalsIgnoreCase("")){
                InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date), sdf.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_server), sdfServer.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date), sdf.format(new Date()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_server), sdfServer.format(new Date()));

            }
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "").equalsIgnoreCase("")){
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date), sdf.format(firstDateOfPreviousMonth));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date), sdf.format(prevDate));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(firstDateOfPreviousMonth));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(prevDate));
            }
            // updateDateMonthView();
            Gson gson = new Gson();
            String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_dynamic_map_filter_obj), null);
            mHomeActivity.integerListHashMap = gson.fromJson(json, new TypeToken<Map<Integer, List<DynamicData>>>(){}.getType());

            if(mHomeActivity.getIntegerListHashMap() != null && mHomeActivity.getIntegerListHashMap().size() > 0){
                rvDashboardList.setVisibility(View.GONE);
                rlProgress.setVisibility(View.VISIBLE);
                GetDashboard(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), true, false, false);
            } else{
                getDynamicFilterMap(InGaugeSession.read(getString(R.string.key_selected_tenant_id), -2), UiUtils.getUserId(mContext), false);
            }


            //Refreshing Dashboard Fragment if any dashboard changed from slide menu
            ((HomeActivity) getActivity()).setFragmentRefreshListener(new HomeActivity.DashboardRefreshListener(){
                @Override
                public void onRefresh(){
                    Logger.Error("<<< Click >>>>");
                    selectedDashboardId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_id), -1);
                    dashboardName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard), "");
                    isDashboardMTD = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_mtd), false);
                    isHideCompareDate = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), true);

                    String firstDate = "";
                    String lastDate = "";

                    if(isDashboardMTD){

                        String fromDate = InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "");
                        String toDate = InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date), "");


                        try{
                            Date date = sdf.parse(fromDate);
                            dateFormatMonth = new SimpleDateFormat("MM");
                            dateFormatYear = new SimpleDateFormat("yyyy");


                            savedMonth = Integer.parseInt(dateFormatMonth.format(date));
                            savedYear = Integer.parseInt(dateFormatYear.format(date));
                            selectedMonth = savedMonth;
                            selectedYear = savedYear;
                            selectedDay = 0;

                        } catch(Exception e){
                            e.printStackTrace();
                        }


  /*                      //if (InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "").equalsIgnoreCase("")) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date), sdf.format(date));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_server), firstDate);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date), sdf.format(new Date()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_server), lastDate);

                        //}
                        //if (InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "").equalsIgnoreCase("")) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date), sdf.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date), sdf.format(prevDate));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(prevDate));
*/
                    } else{
                       /* InGaugeSession.write(mContext.getResources().getString(R.string.key_is_compare_on), true);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date), sdf.format(date));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_server), sdfServer.format(date));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date), sdf.format(new Date()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_server), sdfServer.format(new Date()));

                        //}
                        //if (InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "").equalsIgnoreCase("")) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date), sdf.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date), sdf.format(prevDate));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(prevDate));
*/
                    }
                    if(!(selectedDashboardId == -1)){

                        setNeedtoFilterCheck(true);
                        updateDateMonthView();
                        isCheckDone = false;
                        //getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, true);
                        getDashboardFilters(selectedDashboardId);


                    } else{

                        rlProgress.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        llPlaceHolderNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));

                    }
                }
            });
        } else{
            setToolbarTitles();
            if(mHomeActivity.isDashboardUpdateAfterApply()){
                updateDateMonthView();

                mHomeActivity.setDashboardUpdateAfterApply(false);
                if(selectedDashboardId != -1){
                    isNeedtoUpdateSavedFilterForLb = false;
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                } else{
                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.menu_dashboard));
                    mHomeActivity.tvSubTitle.setText("");
                }
            } else{
                if(mHomeActivity.isDashboardUpdate()){
                    mHomeActivity.setDashboardUpdate(false);
                    setNeedtoFilterCheck(true);
                    GetDashboard(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), true, false, false);
                } else{
                    /*if ( mHomeActivity.list != null && mHomeActivity.list.size () > 0 ) {
                        imgFilter.setClickable ( true );
                        mHomeActivity.mDrawerLayout.closeDrawer ( Gravity.RIGHT );
                        //mHomeActivity.customListAdapter.notifyDataSetChanged ();
                        mHomeActivity.setDashboardList ( mHomeActivity.list, true );


                    } else {*/
                    GetDashboard(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), true, false, true);
                    //}


                }
            }

            if(view != null && view.getParent() != null){
                ((ViewGroup) view.getParent()).removeView(view);
            }
        }

       /* if ( mHomeActivity.list != null && mHomeActivity.list.size () > 0 ) {
            imgFilter.setClickable ( true );
            mHomeActivity.mDrawerLayout.closeDrawer ( Gravity.RIGHT );
            mHomeActivity.customListAdapter.notifyDataSetChanged ();

        }*/
        return view;
    }

    public void updateDateMonthView(){

        if(isDashboardMTD){
            mHomeActivity.setMonthOrYear(0);
        } else{
            mHomeActivity.setMonthOrYear(1);
        }
        //}
        if(isDashboardMTD){

            String fromDate = InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), "");
            String toDate = InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date), "");


            try{
                Date date = sdfServer.parse(fromDate);
                dateFormatMonth = new SimpleDateFormat("MM");
                dateFormatYear = new SimpleDateFormat("yyyy");


                savedMonth = Integer.parseInt(dateFormatMonth.format(date));
                savedYear = Integer.parseInt(dateFormatYear.format(date));
                selectedMonth = savedMonth;
                selectedYear = savedYear;
                selectedDay = 0;

            } catch(Exception e){
                e.printStackTrace();
            }


            tvSelectDate.setText("");
            tvSelectPreviousDate.setVisibility(View.GONE);
            tvSelectDate.setText("Month: " + STR_MONTH[savedMonth - 1] + " | Year " + savedYear);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_compare_on), false);
        } else{
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date), "") != null){
                if(InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date), "").length() > 0){
                    tvSelectDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "") + " To " + InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date), ""));
                } else{
                    tvSelectDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), ""));
                }
            } else{
                tvSelectDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), ""));
            }


            //InGaugeSession.write(mContext.getResources().getString(R.string.key_is_compare_on), isHideCompareDate);

            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_is_compare_on), false)){
                tvSelectPreviousDate.setVisibility(View.VISIBLE);
                if(InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_end_date), "") != null && InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_end_date), "").length() > 0){
                    tvSelectPreviousDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "") + " To " + InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_end_date), ""));
                } else{
                    tvSelectPreviousDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), ""));
                }
            } else{
                tvSelectPreviousDate.setVisibility(View.GONE);
            }
        }
        //tvSelectedDate.setText("Month: " + STR_MONTH[savedMonth - 1] + " - Year " + savedYear);


    }

    public DashboardModelList.Datum getDashboardDetails(int selectedDashboardId){

        for(int i = 0; i < mHomeActivity.list.size(); i++){
            if(mHomeActivity.list.get(i) != null){
                if(mHomeActivity.list.get(i).getId() == selectedDashboardId){
                    isChildDashboard = false;
                    return mHomeActivity.list.get(i);
                } else if(mHomeActivity.list.get(i).getmDatumList() != null && mHomeActivity.list.get(i).getmDatumList().size() > 0){
                    for(int j = 0; j < mHomeActivity.list.get(i).getmDatumList().size(); j++){
                        if(mHomeActivity.list.get(i).getmDatumList().get(j).getId() == selectedDashboardId){
                            isChildDashboard = true;
                            return mHomeActivity.list.get(i).getmDatumList().get(j);
                        }
                    }
                }

            }
        }

        return null;
    }

    void GetDashboard(String authToken, final boolean isFirst, final boolean isFromNavigation, final boolean fromOnCreateViewFalse){
        Call mCall = apiInterface.getDashboardList(
                InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0),
                InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0),
                InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_id), 0), false);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    DashboardModelList madDashboardModelList = (DashboardModelList) response.body();
                    if(madDashboardModelList != null){
                        mHomeActivity.list = madDashboardModelList.getData();
                        try{
                            if(mHomeActivity.list != null && mHomeActivity.list.size() > 0){
                                mHomeActivity.mDrawerList.setVisibility(View.VISIBLE);
                                mHomeActivity.tvNoDashboard.setVisibility(View.GONE);
                                mHomeActivity.setDashboardList(mHomeActivity.list, isFirst);
                                if(!isFromNavigation){
                                    mHomeActivity.tvNoDashboard.setVisibility(View.GONE);
                                    if(mHomeActivity.list.size() > 0){
                                        if(selectedDashboardId != -1){

                                            DashboardModelList.Datum madDashboardModelListNew = getDashboardDetails(selectedDashboardId);
                                            if(madDashboardModelListNew == null){
                                                selectedDashboardId = mHomeActivity.list.get(0).getId();
                                                madDashboardModelListNew = getDashboardDetails(selectedDashboardId);
                                            }
                                            if(isChildDashboard){
                                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard), madDashboardModelListNew.getTitle());
                                            } else{
                                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard), madDashboardModelListNew.getName());
                                            }

                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_id), madDashboardModelListNew.getId());
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_mtd), madDashboardModelListNew.isMTD());
                                            //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), madDashboardModelListNew.isHideCompareDate());
                                            // This is Change for IN-830 Jira to pass the satatic false because to hide the compare date
                                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), true);
                                            selectedDashboardId = madDashboardModelListNew.getId();
                                            if(isChildDashboard){
                                                dashboardName = madDashboardModelListNew.getTitle();
                                            } else{
                                                dashboardName = madDashboardModelListNew.getName();
                                            }

                                            isDashboardMTD = madDashboardModelListNew.isMTD();
                                            // This is Change for IN-830 Jira to pass the satatic false because to hide the compare date
                                            isHideCompareDate = true;
                                            //isHideCompareDate = madDashboardModelListNew.isHideCompareDate();

                                            setToolbarTitles();
                                        } else{
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard), mHomeActivity.list.get(0).getName());
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_id), mHomeActivity.list.get(0).getId());
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_mtd), mHomeActivity.list.get(0).isMTD());
                                            //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), mHomeActivity.list.get(0).isHideCompareDate());
                                            // This is Change for IN-830 Jira to pass the satatic false because to hide the compare date
                                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), true);
                                            selectedDashboardId = mHomeActivity.list.get(0).getId();
                                            dashboardName = mHomeActivity.list.get(0).getName();
                                            isDashboardMTD = mHomeActivity.list.get(0).isMTD();
                                            //isHideCompareDate = mHomeActivity.list.get(0).isHideCompareDate();
                                            // This is Change for IN-830 Jira to pass the satatic false because to hide the compare date
                                            isHideCompareDate = true;
                                        }

                                        if(!fromOnCreateViewFalse){
                                            Gson gson = new Gson();
                                            String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_filter_obj), null);
                                            mHomeActivity.mMainLeftFilterBaseDatas = gson.fromJson(json, new TypeToken<ArrayList<FilterBaseData>>(){
                                            }.getType());
                                            if(mHomeActivity.mMainLeftFilterBaseDatas != null && mHomeActivity.mMainLeftFilterBaseDatas.size() > 0){
                                                isNeedtoUpdateSavedFilterForLb = false;
                                                getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                                            } else{
                                                isNeedtoUpdateSavedFilterForLb = true;
                                                //getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, true);
                                                getDashboardFilters(selectedDashboardId);
                                            }
                                        }

                                    } else{
                                        rlProgress.setVisibility(View.GONE);
                                        mHomeActivity.tvNoDashboard.setVisibility(View.VISIBLE);
                                        tvNoData.setVisibility(View.VISIBLE);
                                        llPlaceHolderNoData.setVisibility(View.VISIBLE);
                                        tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                                        rvDashboardList.setVisibility(View.GONE);

                                    }
                                    //mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                                    //mHomeActivity.tvSubTitle.setText(dashboardName + "-" + InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
                                }

                                updateDateMonthView();
                            } else{
                                InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_id), -1);
                                InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_mtd), false);
                                InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), true);
                                selectedDashboardId = -1;
                                pbFilter.setVisibility(View.GONE);
                                imgFilter.setVisibility(View.VISIBLE);
                                mHomeActivity.list.clear();
                                rlProgress.setVisibility(View.GONE);
                                mHomeActivity.mDrawerList.setVisibility(View.GONE);
                                mHomeActivity.tvNoDashboard.setVisibility(View.VISIBLE);
                                tvNoData.setVisibility(View.VISIBLE);
                                llPlaceHolderNoData.setVisibility(View.VISIBLE);
                                tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                                rvDashboardList.setVisibility(View.GONE);
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard), "");
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_id), -1);
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_mtd), false);
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), true);
                                mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.menu_dashboard));
                                mHomeActivity.tvSubTitle.setText("");
                                //setToolbarTitles();
                            }
                        } catch(Exception e){
                            e.printStackTrace();
                            mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.menu_dashboard));
                            mHomeActivity.tvSubTitle.setText("");

                        }


                        mHomeActivity.isdashboardLoaded = true;
                    } else{
                        mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.menu_dashboard));
                        mHomeActivity.tvSubTitle.setText("");
                    }
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.endProgress();
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                t.printStackTrace();
            }
        });
    }

    void refreshItems(){

        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.VISIBLE);

        mHomeActivity.tvTitle.setText(dashboardName);
        //mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.menu_dashboard));
        mHomeActivity.toolbar.setClickable(true);
        mHomeActivity.ibMenu.setImageResource(R.drawable.ib_menu);
        mHomeActivity.tvTitle.setVisibility(View.VISIBLE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.tvSubTitle.setClickable(false);
       /* if (mHomeActivity.list != null && mHomeActivity.list.size() > 0) {
            mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
            if (InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_dashboard), "").equalsIgnoreCase("")) {
                //dashboardName = mHomeActivity.list.get(0).getName();
                //  InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard), mHomeActivity.list.get(0).getName());
            } else {

                dashboardName = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_dashboard), "");

            }
        } else {
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }*/

        //mHomeActivity.tvSubTitle.setText(dashboardName);
       /* if (mHomeActivity.list != null) {
            if (mHomeActivity.list.size() == 0) {
                mHomeActivity.tvSubTitle.setVisibility(View.GONE);
            } else {
                mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
            }
        } else {
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }*/

        mHomeActivity.ibMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mHomeActivity.OnMenuClick();
            }
        });
        if(!InGaugeSession.read(mContext.getResources().getString(R.string.key_is_need_filter_call), false)){
            // mHomeActivity.tvSubTitle.setText(dashboardName + "-" + InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
            Gson gson = new Gson();
            String json = "";
            if(mHomeActivity.isTenantLocation()){
                gson = new Gson();
                json = InGaugeSession.read(mContext.getResources().getString(R.string.key_location_obj), null);
                mHomeActivity.mAccessibleTenantLocationDataModel = gson.fromJson(json, AccessibleTenantLocationDataModel.class);
            }
            if(mHomeActivity.isLocationGroup()){
                gson = new Gson();
                json = InGaugeSession.read(mContext.getResources().getString(R.string.key_location_group_obj), null);
                mHomeActivity.mDashboardLocationGroupListModel = gson.fromJson(json, DashboardLocationGroupListModel.class);

            }
            if(mHomeActivity.isProduct()){
                gson = new Gson();
                json = InGaugeSession.read(mContext.getResources().getString(R.string.key_product_obj), null);
                mHomeActivity.mProductListFromLocationGroupModel = gson.fromJson(json, ProductListFromLocationGroupModel.class);

            }
            if(mHomeActivity.isUser()){
                gson = new Gson();
                json = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_obj), null);
                mHomeActivity.mDashboardUserListModel = gson.fromJson(json, DashboardUserListModel.class);
            }

        }

        mHomeActivity.toolbar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(rlProgress.getVisibility() != View.VISIBLE){
                    if(imgFilter.getVisibility() == View.VISIBLE){
                        if(mHomeActivity.list.size() > 0){
                            imgFilter.setClickable(true);
                            FragmentFilterDynamicList mFragmentFilterDynamicList = new FragmentFilterDynamicList();
                            Bundle mBundle = new Bundle();
                            mFragmentFilterDynamicList.setArguments(mBundle);
                            mHomeActivity.replace(mFragmentFilterDynamicList, mContext.getResources().getString(R.string.tag_filter_list));

                        } else{
                            imgFilter.setClickable(false);
                        }
                    }
                }


            }
        });
        setToolbarTitles();

        Logger.Error("<< Call >>>");

        isGuideEnableFromLogin = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_guide_enabled), false);
        isGuideEnableFromChangetenant = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_guide_enabled_from_preference), false);
        if(isGuideEnableFromLogin){
            InGaugeSession.write(getString(R.string.key_guide_enabled), false);
            showTenantGuide();

        } else if(isGuideEnableFromChangetenant){
            InGaugeSession.write(getString(R.string.key_guide_enabled_from_preference), false);
            showDashboardFilterGuide();
        }
    }


    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.img_filter:
                if(rlProgress.getVisibility() != View.VISIBLE){
                    if(mHomeActivity.list.size() > 0){
                        imgFilter.setClickable(true);
                        FragmentFilterDynamicList mFragmentFilterDynamicList = new FragmentFilterDynamicList();
                        Bundle mBundle = new Bundle();
                        mFragmentFilterDynamicList.setArguments(mBundle);
                        mHomeActivity.replace(mFragmentFilterDynamicList, mContext.getResources().getString(R.string.tag_filter_list));

                    } else{
                        imgFilter.setClickable(false);
                    }
                }


                break;
            case R.id.tv_fromDate:
                //  ShowDatePicker(tvFromDate);
                break;
            case R.id.tv_toDate:
                // ShowDatePicker(tvToDate);
                break;

            case R.id.relative_date_compare:
                if(isDashboardMTD){
                    MonthYearPickerDialog pd = new MonthYearPickerDialog();
                    Bundle mBunle = new Bundle();
                    mBunle.putInt(KEY_SELECTED_DAY, selectedDay);
                    mBunle.putInt(KEY_SELECTED_MONTH, selectedMonth);
                    mBunle.putInt(KEY_SELECTED_YEAR, selectedYear);
                    pd.setArguments(mBunle);
                    pd.setListener(this);
                    pd.show(getFragmentManager(), "MonthYearPickerDialog");
                } else{
                    DateSelectionFragment fragment = new DateSelectionFragment();
                    Bundle mBundle  = new Bundle();
                    mBundle.putBoolean(DateSelectionFragment.KEY_IS_HIDE_COMPARE,isHideCompareDate);

                    /*mBundle.putBoolean(DateSelectionFragment.KEY_IS_COMPARE,false);
                    mBundle.putBoolean(DateSelectionFragment.KEY_IS_FROM_LEADERBOARD,true);*/
                    fragment.setArguments(mBundle);
                    mHomeActivity.replace(fragment, mHomeActivity.getResources().getString(R.string.tag_fragment_dashboard_date_filter));
                }
                break;
            case R.id.dashboard_fragment_rl_leaderboard:
                FragmentLeaderBoard fragment = new FragmentLeaderBoard();
                mHomeActivity.setmLeftFilterBaseDatasForLb(new ArrayList<FilterBaseData>());
                mHomeActivity.replace(fragment, mHomeActivity.getResources().getString(R.string.tag_fragment_leaderboard));
                break;
        }
    }

    /*private void ShowDatePicker(final TextView tvDate) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if (tvDate == tvFromDate) {
                            fromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        } else {
                            toDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }

                        tvDate.setText(setFormattedDate(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }*/


    void getDashboardFilters(final int dashboardId){
        countforFilters++;
        Logger.Error("<<<<<< COUNT FOR API FILTERS  " + countforFilters);
        rvDashboardList.setVisibility(View.GONE);
        rlProgress.setVisibility(View.VISIBLE);
        String firstDate = "", lastDate = "";
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype), -2);
        int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region), -2);
        int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product), -2));
        int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user), -2));
        int TenantLocationId = (InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
        int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group), -2);
        if(!(mHomeActivity.mMainLeftFilterBaseDatas != null && mHomeActivity.mMainLeftFilterBaseDatas.size() > 0)){
            if(!mHomeActivity.isTenantLocation()){
               /* if(!mHomeActivity.isNoNeedUpdateMetric ()){
                    if(!IsNeedRecursive)
                        mHomeActivity.setNoNeedUpdateMetric ( false);
                    InGaugeSession.write ( mContext.getResources ().getString ( R.string.key_selected_metric_type_name ), mHomeActivity.mFilterMetricTypeModelList.get ( 1 ).name );
                }*/

            }
        }
        String finalMatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mContext.getResources().getStringArray(R.array.metric_type_array)[1]);
        String MatrixDataType = finalMatrixDataType;
        if(MatrixDataType.equalsIgnoreCase("Check out")){
            MatrixDataType = "Arrival";
        } else if(MatrixDataType.equalsIgnoreCase("Check in")){
            MatrixDataType = "Departure";
        } else if(MatrixDataType.equalsIgnoreCase("Daily")){
            MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
        }

        try{
            firstDate = DateTimeUtils.getFirstDay(new Date());
            lastDate = DateTimeUtils.getLastDay(new Date());
            System.out.println("First Date: " + firstDate);
            System.out.println("Last Date: " + lastDate);
        } catch(Exception e){
            e.printStackTrace();
        }
        String FromDate = "";
        String ToDate = "";
        String CompFromDate = "";
        String CompToDate = "";
        boolean IsCompare = false;
        if(isDashboardMTD){

            //Date date = sdf.parse ( fromDate );

            FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
            ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");

            try{
                Date fromDateDate = sdfServer.parse(FromDate);
                FromDate = DateTimeUtils.getFirstDay(fromDateDate);
                Date TomDateDate = sdfServer.parse(ToDate);
                ToDate = DateTimeUtils.getLastDay(TomDateDate);
            } catch(Exception e){
                e.printStackTrace();
            }
            CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
            CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
            IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), false);
        } else{
            FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
            ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
            CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
            CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
            IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), false);
        }


        Calendar startserverCalendar = null;
        Calendar endserverCalendar = null;

        Calendar compstartserverCalendar = null;
        Calendar compendserverCalendar = null;
        try{

            Logger.Error("<<< From Date " + FromDate);
            Logger.Error("<<< To Date " + ToDate);
            Calendar startCalendar = UiUtils.DateToCalendar(sdfServer.parse(FromDate));
            Long serverstartLong = startCalendar.getTimeInMillis();
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + serverstartLong);
            String serverStartDate = sdfServer.format(serverstartLong);
            startserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverStartDate + " 00:00:00"));
            startserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + startserverCalendar.getTimeInMillis());


            Calendar endCalendar = UiUtils.DateToCalendar(sdfServer.parse(ToDate));
            Long serverEndLong = endCalendar.getTimeInMillis();
            String serverEndDate = sdfServer.format(serverEndLong);
            endserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverEndDate + " 23:59:59"));
            endserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + serverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + endserverCalendar.getTimeInMillis());


            Calendar compstartCalendar = UiUtils.DateToCalendar(sdfServer.parse(CompFromDate));
            Long comserverstartLong = compstartCalendar.getTimeInMillis();
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + comserverstartLong);
            String compserverStartDate = sdfServer.format(comserverstartLong);
            compstartserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(compserverStartDate + " 00:00:00"));
            compstartserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + compstartserverCalendar.getTimeInMillis());


            Calendar compendCalendar = UiUtils.DateToCalendar(sdfServer.parse(CompToDate));
            Long compserverEndLong = compendCalendar.getTimeInMillis();
            String compserverEndDate = sdfServer.format(compserverEndLong);
            compendserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(compserverEndDate + " 23:59:59"));
            compendserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + compserverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + compendserverCalendar.getTimeInMillis());


        } catch(ParseException e){
            e.printStackTrace();
        }
        mCall = apiInterface.getDashboardFiltersCheck(
                dashboardId,
                TenantId,
                RegionTypeId,
                RegionId,
                ProductId,
                UserId,
                TenantLocationId,
                LocationGroupId,
                MatrixDataType,
                FromDate,
                ToDate,
                CompFromDate,
                CompToDate,
                startserverCalendar.getTimeInMillis(),
                endserverCalendar.getTimeInMillis(),
                compstartserverCalendar.getTimeInMillis(),
                compendserverCalendar.getTimeInMillis(),
                IsCompare);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){

                if(response.body() != null){
                    try{
                        JSONObject responseJson = new JSONObject(response.body().string());
                        if(responseJson != null){
                            mHomeActivity.setRegionFilter(false);
                            mHomeActivity.setTenantLocation(false);
                            mHomeActivity.setCustomGoalFilter(false);
                            mHomeActivity.setProduct(false);
                            mHomeActivity.setLocationGroup(false);
                            mHomeActivity.setUser(false);
                            JSONObject mdataJsonObject = responseJson.optJSONObject("data");
                            if(mdataJsonObject != null){
                                JSONArray mJsonArrayFilters = mdataJsonObject.optJSONArray("filters");
                                mFilterListString.clear();
                                if(mJsonArrayFilters != null && mJsonArrayFilters.length() > 0){
                                    for(int i = 0; i < mJsonArrayFilters.length(); i++){
                                        mFilterListString.add(mJsonArrayFilters.optString(i));
                                    }

                                    for(String filterName : mFilterListString){
                                        checkFilterFromCustomreport(filterName, true);
                                    }
                                    filterCallAfterCheck();
                                } else{
                                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);

                                }
                            }
                        }

                    } catch(JSONException e){
                        e.printStackTrace();
                        Logger.Error("Exceptiopn " + e.toString());
                    } catch(Exception e){
                        e.printStackTrace();

                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){

            }
        });
    }

    void getDashboardCustomResport(final String authToken, final int dashboardId, final boolean IsNeedRecursive){
        countFordashboard++;
        Logger.Error("<<<<<< COUNT FOR API Custom Report" + countFordashboard);
        rvDashboardList.setVisibility(View.GONE);
        rlProgress.setVisibility(View.VISIBLE);
        String firstDate = "", lastDate = "";

        if(!IsNeedRecursive){
            if(mHomeActivity.mMainLeftFilterBaseDatas != null && mHomeActivity.mMainLeftFilterBaseDatas.size() > 0){
                //This Check is useful to for where the Dashboard is load from Splash Sceeen
            } else{
                if(!mHomeActivity.isRegionFilter()){
                    InGaugeSession.write(mContext.getString(R.string.key_selected_regiontype), -2);
                    InGaugeSession.write(mContext.getString(R.string.key_selected_region), -2);
                }
                if(!mHomeActivity.isTenantLocation()){
                    InGaugeSession.write(mContext.getString(R.string.key_tenant_location_id), -2);
                }
                if(!mHomeActivity.isLocationGroup()){
                    InGaugeSession.write(mContext.getString(R.string.key_selected_location_group), -2);
                }
                if(!mHomeActivity.isProduct()){
                    InGaugeSession.write(mContext.getString(R.string.key_selected_location_group_product), -2);
                }
                if(!mHomeActivity.isUser()){
                    InGaugeSession.write(mContext.getString(R.string.key_selected_user), -2);
                }
            }
        }
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype), -2);
        int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region), -2);
        int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product), -2));
        int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user), -2));
        int TenantLocationId = (InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
        int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group), -2);
        if(!(mHomeActivity.mMainLeftFilterBaseDatas != null && mHomeActivity.mMainLeftFilterBaseDatas.size() > 0)){
            if(!mHomeActivity.isTenantLocation()){
               /* if(!mHomeActivity.isNoNeedUpdateMetric ()){
                    if(!IsNeedRecursive)
                        mHomeActivity.setNoNeedUpdateMetric ( false);
                    InGaugeSession.write ( mContext.getResources ().getString ( R.string.key_selected_metric_type_name ), mHomeActivity.mFilterMetricTypeModelList.get ( 1 ).name );
                }*/

            }
        }
        String finalMatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mContext.getResources().getStringArray(R.array.metric_type_array)[1]);
        String MatrixDataType = finalMatrixDataType;
        if(MatrixDataType.equalsIgnoreCase("Check out")){
            MatrixDataType = "Arrival";
        } else if(MatrixDataType.equalsIgnoreCase("Check in")){
            MatrixDataType = "Departure";
        } else if(MatrixDataType.equalsIgnoreCase("Daily")){
            MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
        }

        try{
            firstDate = DateTimeUtils.getFirstDay(new Date());
            lastDate = DateTimeUtils.getLastDay(new Date());
            System.out.println("First Date: " + firstDate);
            System.out.println("Last Date: " + lastDate);
        } catch(Exception e){
            e.printStackTrace();
        }
        String FromDate = "";
        String ToDate = "";
        String CompFromDate = "";
        String CompToDate = "";
        boolean IsCompare = false;
        if(isDashboardMTD){

            //Date date = sdf.parse ( fromDate );

            FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
            ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");

            try{
                Date fromDateDate = sdfServer.parse(FromDate);
                FromDate = DateTimeUtils.getFirstDay(fromDateDate);
                Date TomDateDate = sdfServer.parse(ToDate);
                ToDate = DateTimeUtils.getLastDay(TomDateDate);
            } catch(Exception e){
                e.printStackTrace();
            }
            CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
            CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
            IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), false);
        } else{
            FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
            ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
            CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
            CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
            IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), false);
        }


        Calendar startserverCalendar = null;
        Calendar endserverCalendar = null;

        Calendar compstartserverCalendar = null;
        Calendar compendserverCalendar = null;
        try{

            Logger.Error("<<< From Date " + FromDate);
            Logger.Error("<<< To Date " + ToDate);
            Calendar startCalendar = UiUtils.DateToCalendar(sdfServer.parse(FromDate));
            Long serverstartLong = startCalendar.getTimeInMillis();
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + serverstartLong);
            String serverStartDate = sdfServer.format(serverstartLong);
            startserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverStartDate + " 00:00:00"));
            startserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + startserverCalendar.getTimeInMillis());


            Calendar endCalendar = UiUtils.DateToCalendar(sdfServer.parse(ToDate));
            Long serverEndLong = endCalendar.getTimeInMillis();
            String serverEndDate = sdfServer.format(serverEndLong);
            endserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverEndDate + " 23:59:59"));
            endserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + serverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + endserverCalendar.getTimeInMillis());


            Calendar compstartCalendar = UiUtils.DateToCalendar(sdfServer.parse(CompFromDate));
            Long comserverstartLong = compstartCalendar.getTimeInMillis();
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + comserverstartLong);
            String compserverStartDate = sdfServer.format(comserverstartLong);
            compstartserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(compserverStartDate + " 00:00:00"));
            compstartserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + compstartserverCalendar.getTimeInMillis());


            Calendar compendCalendar = UiUtils.DateToCalendar(sdfServer.parse(CompToDate));
            Long compserverEndLong = compendCalendar.getTimeInMillis();
            String compserverEndDate = sdfServer.format(compserverEndLong);
            compendserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(compserverEndDate + " 23:59:59"));
            compendserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + compserverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + compendserverCalendar.getTimeInMillis());


        } catch(ParseException e){
            e.printStackTrace();
        }


        mCall = apiInterface.getDashboardCustomReports(
                dashboardId,
                TenantId,
                RegionTypeId,
                RegionId,
                ProductId,
                UserId,
                TenantLocationId,
                LocationGroupId,
                MatrixDataType,
                FromDate,
                ToDate,
                CompFromDate,
                CompToDate,
                startserverCalendar.getTimeInMillis(),
                endserverCalendar.getTimeInMillis(),
                compstartserverCalendar.getTimeInMillis(),
                compendserverCalendar.getTimeInMillis(),
                IsCompare);
        mCall.enqueue(new Callback<ResponseBody>(){
            CustomDashboardModel.Data mCustomDashboardModel;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                setToolbarTitles();

                if(isNeedtoUpdateSavedFilterForLb){
                    savedFilters();
                }


                if(!IsNeedRecursive){
                    imgFilter.setClickable(true);
                    rvDashboardList.setVisibility(View.VISIBLE);
                    rlProgress.setVisibility(View.GONE);
                }
                if(response.body() != null){
                    try{
                        JSONObject responseJson = new JSONObject(response.body().string());
                        if(responseJson != null){
                            JSONObject mdataJsonObject = responseJson.optJSONObject("data");
                            if(mdataJsonObject != null){
                                JSONArray mJsonArrayFilters = mdataJsonObject.optJSONArray("filters");
                                mFilterListString.clear();
                                if(mJsonArrayFilters != null && mJsonArrayFilters.length() > 0){
                                    for(int i = 0; i < mJsonArrayFilters.length(); i++){
                                        mFilterListString.add(mJsonArrayFilters.optString(i));
                                    }
                                }
                                Gson gson = new Gson();
                                mCustomDashboardModel = gson.fromJson(mdataJsonObject.toString(), CustomDashboardModel.Data.class);
                                mCustomReportList = mCustomDashboardModel.getCustomReport();
                            }
                        }

                    } catch(JSONException e){
                        e.printStackTrace();
                        Logger.Error("Exceptiopn " + e.toString());
                    } catch(Exception e){
                        e.printStackTrace();

                    }

                    if(isNeedtoFilterCheck){
                        if(IsNeedRecursive){
                            mHomeActivity.setRegionFilter(false);
                            mHomeActivity.setTenantLocation(false);
                            mHomeActivity.setCustomGoalFilter(false);
                            mHomeActivity.setProduct(false);
                            mHomeActivity.setLocationGroup(false);
                            mHomeActivity.setUser(false);
                        }
                    }
                    if(isNeedtoFilterCheck){
                        if(IsNeedRecursive){
                            for(String filterName : mFilterListString){
                                checkFilterFromCustomreport(filterName, IsNeedRecursive);
                            }
                            filterCallAfterCheck();
                        } else{
                            if(mCustomReportList.size() > 0){
                                for(String filterName : mFilterListString){
                                    checkFilterFromCustomreport(filterName, true);
                                }

                                if(mHomeActivity.isTenantLocation()){
                                    Gson gson = new Gson();
                                    String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_location_obj), null);
                                    mHomeActivity.mAccessibleTenantLocationDataModel = gson.fromJson(json, AccessibleTenantLocationDataModel.class);
                                }

                                if(mHomeActivity.isLocationGroup()){
                                    Gson gson = new Gson();
                                    String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_location_group_obj), null);
                                    mHomeActivity.mDashboardLocationGroupListModel = gson.fromJson(json, DashboardLocationGroupListModel.class);
                                }

                                if(mHomeActivity.isProduct()){
                                    Gson gson = new Gson();
                                    String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_product_obj), null);
                                    mHomeActivity.mProductListFromLocationGroupModel = gson.fromJson(json, ProductListFromLocationGroupModel.class);
                                }

                                if(mHomeActivity.isUser()){
                                    Gson gson = new Gson();
                                    String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_obj), null);
                                    mHomeActivity.mDashboardUserListModel = gson.fromJson(json, DashboardUserListModel.class);
                                }

                                tvNoData.setVisibility(View.GONE);
                                rvDashboardList.setVisibility(View.VISIBLE);
                                rlProgress.setVisibility(View.GONE);
                                llPlaceHolderNoData.setVisibility(View.GONE);
                                setDashboardCustomReportAdapter(mCustomReportList);
                                tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                            } else{
                                tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                                tvNoData.setVisibility(View.VISIBLE);
                                llPlaceHolderNoData.setVisibility(View.VISIBLE);
                                rvDashboardList.setVisibility(View.GONE);
                                rlProgress.setVisibility(View.GONE);
                            }
                        }

                    } else{
                        if(mCustomReportList.size() > 0){
                            tvNoData.setVisibility(View.GONE);
                            llPlaceHolderNoData.setVisibility(View.GONE);
                            rvDashboardList.setVisibility(View.VISIBLE);
                            rlProgress.setVisibility(View.GONE);
                            setDashboardCustomReportAdapter(mCustomReportList);
                            tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                        } else{
                            tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                            tvNoData.setVisibility(View.VISIBLE);
                            llPlaceHolderNoData.setVisibility(View.VISIBLE);
                            rvDashboardList.setVisibility(View.GONE);
                            rlProgress.setVisibility(View.GONE);
                        }
                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                rlProgress.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
                llPlaceHolderNoData.setVisibility(View.VISIBLE);
                rvDashboardList.setVisibility(View.GONE);
                tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                t.printStackTrace();
            }
        });

    }

    private void savedFilters(){
        mFilterBaseDatas = new ArrayList<>();

        if(mHomeActivity.isRegionFilter()){
            mFilterBaseDatas.addAll(mFilterBaseDatasRegion);
        }
        if(mHomeActivity.isTenantLocation()){
            mFilterBaseDatas.addAll(mFilterBaseDatasTenantLocation);
        }
        if(mHomeActivity.isLocationGroup()){
            mFilterBaseDatas.addAll(mFilterBaseDatasLocationgroup);
        }
        if(mHomeActivity.isProduct()){
            mFilterBaseDatas.addAll(mFilterBaseDatasProduct);
        }
        if(mHomeActivity.isUser()){
            mFilterBaseDatas.addAll(mFilterBaseDatasuser);
        }

        Logger.Error("<<<<< Saved Filter Data sets " + mFilterBaseDatas);
        Gson gson = new Gson();
        String json = gson.toJson(mFilterBaseDatas);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_filter_obj_for_leader_board), json);
    }


    private void setDashboardCustomReportAdapter(List<CustomDashboardModel.CustomReport> mCustomReportList){
        setToolbarTitles();
        DashboardPanelAdapter mDashboardPanelAdapter = new DashboardPanelAdapter(mHomeActivity, mCustomReportList, this);
        rvDashboardList.setAdapter(mDashboardPanelAdapter);
    }


    /**
     * @param authToken
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param regionId
     * @param activeStatus
     */
    void getAccessibleTenantLocation(final String authToken, int tenantId, String userId, String orderBy, String sort, String regionId, String activeStatus){

        mCall = apiInterface.getAccessibleTenantLocation(tenantId, userId, orderBy, sort, regionId, activeStatus, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mHomeActivity.mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_location_obj), json);

                    if((mHomeActivity.mAccessibleTenantLocationDataModel != null && (mHomeActivity.mAccessibleTenantLocationDataModel.getData() != null && mHomeActivity.mAccessibleTenantLocationDataModel.getData().size() > 0))){


                        Collections.sort(mHomeActivity.mAccessibleTenantLocationDataModel.getData(), new Comparator<AccessibleTenantLocationDataModel.Datum>(){
                            @Override
                            public int compare(AccessibleTenantLocationDataModel.Datum d1, AccessibleTenantLocationDataModel.Datum d2){
                                return Boolean.compare(d2.getMasterLocation(), d1.getMasterLocation());
                            }
                        });


                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getName();
                        mFilterBaseData.hotelMetricType = mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType();
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                        mFilterBaseData.isMasterLocation = mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getMasterLocation();
                        mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                        mFilterBaseDatasTenantLocation.add(mFilterBaseData);


                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getName());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType());

                        if(mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            mHomeActivity.setLocationGroup(true);
                        }
                        if(mHomeActivity.isLocationGroup() || mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            if(mHomeActivity.isLocationGroup()){
                                if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                                    FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)));
                                    if(mPreferenceFilterBaseData != null){
                                        if(checkTenantLocationData(mPreferenceFilterBaseData.id)){
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), Integer.parseInt(mPreferenceFilterBaseData.id));
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mPreferenceFilterBaseData.name);
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mPreferenceFilterBaseData.filterMetricType);
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mPreferenceFilterBaseData.filterMetricType);
                                            mFilterBaseData = new FilterBaseData();
                                            mFilterBaseDatasTenantLocation = new ArrayList<>();
                                            mFilterBaseData.id = mPreferenceFilterBaseData.id;
                                            mFilterBaseData.name = mPreferenceFilterBaseData.name;
                                            mFilterBaseData.hotelMetricType = mPreferenceFilterBaseData.filterMetricType;
                                            mFilterBaseData.isMasterLocation = mPreferenceFilterBaseData.isMasterLocation;
                                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                                            mFilterBaseDatasTenantLocation.add(mFilterBaseData);
                                            getDashboardLocationGroupList(authToken, mPreferenceFilterBaseData.id,
                                                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                        } else{
                                            getDashboardLocationGroupList(authToken,
                                                    String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId()),
                                                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                        }
                                    } else{
                                        getDashboardLocationGroupList(authToken,
                                                String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId()),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                    }

                                } else{
                                    getDashboardLocationGroupList(authToken,
                                            String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId()),
                                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                }

                            } else if(mHomeActivity.isProduct()){
                                getProductList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                        null);

                            } else if(mHomeActivity.isUser()){
                                FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)));
                                if(checkTenantLocationData(mPreferenceFilterBaseData.id)){

                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), Integer.parseInt(mPreferenceFilterBaseData.id));
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mPreferenceFilterBaseData.name);
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mPreferenceFilterBaseData.filterMetricType);
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mPreferenceFilterBaseData.filterMetricType);
                                    mFilterBaseData = new FilterBaseData();
                                    mFilterBaseDatasTenantLocation = new ArrayList<>();
                                    mFilterBaseData.id = mPreferenceFilterBaseData.id;
                                    mFilterBaseData.name = mPreferenceFilterBaseData.name;
                                    mFilterBaseData.hotelMetricType = mPreferenceFilterBaseData.filterMetricType;
                                    mFilterBaseData.isMasterLocation = mPreferenceFilterBaseData.isMasterLocation;
                                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                                    mFilterBaseDatasTenantLocation.add(mFilterBaseData);
                                    getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);
                                } else{
                                    getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);
                                }

                            }

                        } else{
                            if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                                FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)));
                                if(mPreferenceFilterBaseData != null){
                                    if(checkTenantLocationData(mPreferenceFilterBaseData.id)){
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), Integer.parseInt(mPreferenceFilterBaseData.id));
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mPreferenceFilterBaseData.name);
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mPreferenceFilterBaseData.filterMetricType);
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mPreferenceFilterBaseData.filterMetricType);
                                        mFilterBaseData = new FilterBaseData();
                                        mFilterBaseDatasTenantLocation = new ArrayList<>();
                                        mFilterBaseData.id = mPreferenceFilterBaseData.id;
                                        mFilterBaseData.name = mPreferenceFilterBaseData.name;
                                        mFilterBaseData.hotelMetricType = mPreferenceFilterBaseData.filterMetricType;
                                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                                        mFilterBaseData.isMasterLocation = mPreferenceFilterBaseData.isMasterLocation;

                                        mFilterBaseDatasTenantLocation.add(mFilterBaseData);
                                    }
                                }
                            }
                            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                            pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);
                        }

                    } else{
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), -2);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), "");
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), "");
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), "");
                        if(mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            mHomeActivity.setLocationGroup(true);
                        }
                        if(mHomeActivity.isLocationGroup() || mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            if(mHomeActivity.isLocationGroup()){
                                if(mHomeActivity.mAccessibleTenantLocationDataModel != null && mHomeActivity.mAccessibleTenantLocationDataModel.getData() != null){
                                    if(mHomeActivity.mAccessibleTenantLocationDataModel.getData().size() > 0){
                                        getDashboardLocationGroupList(authToken,
                                                String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId()),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                    } else{
                                        getDashboardLocationGroupList(authToken,
                                                String.valueOf(-2),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                    }
                                } else{
                                    getDashboardLocationGroupList(authToken,
                                            String.valueOf(-2),
                                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                }

                            } else if(mHomeActivity.isProduct()){
                                getProductList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                        null);

                            } else if(mHomeActivity.isUser()){
                                getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);

                            }

                        } else{
                            pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);
                            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                        }

                        InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupList(final String authToken, String tenantlocationId, String userId){

        mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mHomeActivity.mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_location_group_obj), json);
                    if((mHomeActivity.mDashboardLocationGroupListModel != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModel.getData().size() > 0))){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseDatasLocationgroup = new ArrayList<>();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID());
                        mFilterBaseData.name = mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName();
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                        mFilterBaseData.filterIndex = 3;
                        mFilterBaseDatasLocationgroup.add(mFilterBaseData);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), (mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName());


                        if(mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            if(mHomeActivity.isProduct()){

                                if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                                    FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)));
                                    if(mPreferenceFilterBaseData != null){
                                        if(checkLocationGroupData(mPreferenceFilterBaseData.id)){
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), Integer.parseInt(mPreferenceFilterBaseData.id));
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mPreferenceFilterBaseData.name);

                                            mFilterBaseData = new FilterBaseData();
                                            mFilterBaseDatasLocationgroup = new ArrayList<>();
                                            mFilterBaseData.id = mPreferenceFilterBaseData.id;
                                            mFilterBaseData.name = mPreferenceFilterBaseData.name;
                                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                                            mFilterBaseData.filterIndex = 3;
                                            mFilterBaseDatasLocationgroup.add(mFilterBaseData);

                                            getProductList(authToken, mPreferenceFilterBaseData.id);
                                        } else{
                                            getProductList(authToken, String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                        }
                                    } else{
                                        getProductList(authToken, String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                    }

                                } else{
                                    getProductList(authToken, String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                }


                            } else if(mHomeActivity.isUser()){
                                if(mHomeActivity.isTenantLocation()){
                                    if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                                        FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)));
                                        if(mPreferenceFilterBaseData != null){
                                            if(checkLocationGroupData(mPreferenceFilterBaseData.id)){
                                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), Integer.parseInt(mPreferenceFilterBaseData.id));
                                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mPreferenceFilterBaseData.name);
                                                mFilterBaseData = new FilterBaseData();
                                                mFilterBaseDatasLocationgroup = new ArrayList<>();
                                                mFilterBaseData.id = mPreferenceFilterBaseData.id;
                                                mFilterBaseData.name = mPreferenceFilterBaseData.name;
                                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                                                mFilterBaseData.filterIndex = 3;
                                                mFilterBaseDatasLocationgroup.add(mFilterBaseData);
                                                getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                                        mPreferenceFilterBaseData.id);

                                            } else{
                                                getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                                        String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                            }
                                        } else{
                                            getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                                    String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                        }

                                    } else{
                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                                String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                    }

                                } else{

                                    if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                                        FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)));
                                        if(checkLocationGroupData(mPreferenceFilterBaseData.id)){
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), Integer.parseInt(mPreferenceFilterBaseData.id));
                                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mPreferenceFilterBaseData.name);
                                            mFilterBaseData = new FilterBaseData();
                                            mFilterBaseDatasLocationgroup = new ArrayList<>();
                                            mFilterBaseData.id = mPreferenceFilterBaseData.id;
                                            mFilterBaseData.name = mPreferenceFilterBaseData.name;
                                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                                            mFilterBaseData.filterIndex = 3;
                                            mFilterBaseDatasLocationgroup.add(mFilterBaseData);
                                            getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                    null,
                                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                                    mPreferenceFilterBaseData.id);

                                        } else{
                                            getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                    null,
                                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                                    String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                        }
                                    } else{

                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                                String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                    }

                                }
                            }
                        } else{
                            pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);
                            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                        }

                    } else{
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), -2);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), "");
                        if(mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            if(mHomeActivity.isProduct()){
                                if(mHomeActivity.mDashboardLocationGroupListModel != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModel.getData().size() > 0)){
                                    getProductList(authToken, String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                } else{
                                    getProductList(authToken, String.valueOf(-2));
                                }


                            } else if(mHomeActivity.isUser()){
                                if(mHomeActivity.isTenantLocation()){
                                    if(mHomeActivity.mDashboardLocationGroupListModel != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModel.getData().size() > 0)){

                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                    } else{
                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(-2));

                                    }

                                } else{

                                    if(mHomeActivity.mDashboardLocationGroupListModel != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModel.getData().size() > 0)){

                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                    } else{
                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(-2));
                                    }
                                }
                            }
                        } else{
                            pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);
                            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                        }
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    }
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{

                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductList(final String authToken, final String locationGroupId){

        mCall = apiInterface.getProductList(locationGroupId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mHomeActivity.mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_product_obj), json);

                    if(mHomeActivity.isCustomGoalFilter()){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(-1);
                        mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                        mFilterBaseData.filterIndex = 4;
                        mFilterBaseDatasProduct.add(mFilterBaseData);

                        if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                            FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product)));
                            if(mPreferenceFilterBaseData != null){
                                if(checkProductData(mPreferenceFilterBaseData.id)){
                                    try{
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mPreferenceFilterBaseData.id));
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mPreferenceFilterBaseData.name);
                                    } catch(Exception e){
                                        e.printStackTrace();
                                    }
                                } else{
                                    try{
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);
                                    } catch(Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            } else{
                                try{
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);
                                } catch(Exception e){
                                    e.printStackTrace();
                                }
                            }

                        } else{
                            try{
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);
                            } catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        if(mHomeActivity.isTenantLocation() && mHomeActivity.isLocationGroup()){
                            getDashboardUserList(authToken,
                                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                    locationGroupId);
                        } else{
                            if(mHomeActivity.isTenantLocation()){
                                getDashboardUserList(authToken,
                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                        null);
                            } else{

                                if(mHomeActivity.isLocationGroup()){
                                    getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                            null,
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), locationGroupId);
                                } else{
                                    pbFilter.setVisibility(View.GONE);
                                    imgFilter.setVisibility(View.VISIBLE);
                                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                                }

                            }


                        }
                    } else{
                        if((mHomeActivity.mProductListFromLocationGroupModel != null && (mHomeActivity.mProductListFromLocationGroupModel.getData() != null && mHomeActivity.mProductListFromLocationGroupModel.getData().size() > 0))){

                            if(mHomeActivity.mProductListFromLocationGroupModel != null){
                                if(mHomeActivity.mProductListFromLocationGroupModel != null && mHomeActivity.mProductListFromLocationGroupModel.getData().size() > 0){

                                    try{
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), mHomeActivity.mProductListFromLocationGroupModel.getData().get(0).getId());
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mHomeActivity.mProductListFromLocationGroupModel.getData().get(0).getName());
                                        FilterBaseData mFilterBaseData = new FilterBaseData();
                                        mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModel.getData().get(0).getId());
                                        mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModel.getData().get(0).getName();
                                        mFilterBaseData.filterIndex = 4;
                                        mFilterBaseDatasProduct.add(mFilterBaseData);
                                    } catch(Exception e){
                                        e.printStackTrace();
                                    }
                                    if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                                        FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product)));
                                        if(mPreferenceFilterBaseData != null){
                                            if(checkProductData(mPreferenceFilterBaseData.id)){
                                                try{
                                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mPreferenceFilterBaseData.id));
                                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mPreferenceFilterBaseData.name);

                                                    FilterBaseData mFilterBaseData = new FilterBaseData();
                                                    mFilterBaseDatasProduct = new ArrayList<>();
                                                    mFilterBaseData.id = mPreferenceFilterBaseData.id;
                                                    mFilterBaseData.name = mPreferenceFilterBaseData.name;
                                                    mFilterBaseData.filterIndex = 4;
                                                    mFilterBaseDatasProduct.add(mFilterBaseData);
                                                } catch(Exception e){
                                                    e.printStackTrace();
                                                }
                                            }
                                        }

                                    }
                                } else{
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), -2);
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), "");
                                }

                            }


                            if(mHomeActivity.isTenantLocation() && mHomeActivity.isLocationGroup()){
                                getDashboardUserList(authToken,
                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                        locationGroupId);
                            } else{
                                if(mHomeActivity.isTenantLocation()){
                                    getDashboardUserList(authToken,
                                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                            null);
                                } else{

                                    if(mHomeActivity.isLocationGroup()){
                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), locationGroupId);
                                    } else{
                                        pbFilter.setVisibility(View.GONE);
                                        imgFilter.setVisibility(View.VISIBLE);
                                        getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                                    }

                                }


                            }
                        } else{
                            pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);
                            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                        }

                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    /**
     * @param authToken
     * @param tenantLocationFromIdFromFilter
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserList(String authToken, String tenantLocationFromIdFromFilter, String startDate, String endDate, String locationGroupId){
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);

        mCall = apiInterface.getDashboardUserListForDashboard(TenantId, true, false, false, tenantLocationFromIdFromFilter, locationGroupId, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);

                if(response.body() != null){
                    mHomeActivity.mDashboardUserListModel = (DashboardUserListModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardUserListModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_user_obj), json);
                    if((mHomeActivity.mDashboardUserListModel != null && (mHomeActivity.mDashboardUserListModel.getData() != null && mHomeActivity.mDashboardUserListModel.getData().size() > 0))){

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mDashboardUserListModel.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mDashboardUserListModel.getData().get(0).getName();
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                        mFilterBaseData.filterIndex = 5;
                        mFilterBaseDatasuser.add(mFilterBaseData);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), (mHomeActivity.mDashboardUserListModel.getData().get(0).getId()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name), mHomeActivity.mDashboardUserListModel.getData().get(0).getName());

                        if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                            FilterBaseData mPreferenceFilterBaseData = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user)));
                            if(mPreferenceFilterBaseData != null){
                                if(checkuserData(mPreferenceFilterBaseData.id)){
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), Integer.parseInt(mPreferenceFilterBaseData.id));
                                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name), mPreferenceFilterBaseData.name);
                                    mFilterBaseData = new FilterBaseData();
                                    mFilterBaseDatasuser = new ArrayList<>();
                                    mFilterBaseData.id = mPreferenceFilterBaseData.id;
                                    mFilterBaseData.name = mPreferenceFilterBaseData.name;
                                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                    mFilterBaseData.filterIndex = 5;
                                    mFilterBaseDatasuser.add(mFilterBaseData);
                                }
                            }
                        }

                        getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    } else{
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), -2);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name), "");
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();

                    }
                } else{
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
            }
        });
    }


    private void setMetricTypeList(){
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "" + 000000;
        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);
        mFilterBaseData.filterIndex = 6;
        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mHomeActivity.mStringListMetricType.get(1));
        mFilterBaseDatas.add(mFilterBaseData);
    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mHomeActivity.tvTitle.setText("");
        mHomeActivity.tvSubTitle.setText("");
        mHomeActivity.tvTitle.setText("");
        mHomeActivity.ibMenu.setVisibility(View.GONE);

        if(mCall != null)
            mCall.cancel();
    }

    @Override
    public void recyclerViewListClicked(View v, int position){
        DashboardDetailFragment mDashboardDetailFragment = new DashboardDetailFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(DashboardDetailFragment.KEY_REPORT_ID, String.valueOf(mCustomReportList.get(position).getId()));
        mBundle.putString(DashboardDetailFragment.KEY_REPORT_Name, mCustomReportList.get(position).getReportName());
        mBundle.putString(DashboardDetailFragment.KEY_REPORT_TYPE, mCustomReportList.get(position).getReportType());
        mBundle.putBoolean(DashboardDetailFragment.KEY_IS_SOCIAL_INTERACTION_ENABLE, false);
        mBundle.putBoolean(DashboardDetailFragment.KEY_IS_MTD, isDashboardMTD);
        mDashboardDetailFragment.setArguments(mBundle);
        mHomeActivity.replace(mDashboardDetailFragment, mContext.getResources().getString(R.string.tag_dashboard_detail));
    }

    private void checkFilterFromCustomreport(String filterTypeName, boolean isNeedRecursive){

        if(isNeedRecursive){
            switch(filterTypeName){
                case "Region":
                    mHomeActivity.setRegionFilter(true);
                    break;
                case "TenantLocation":
                    mHomeActivity.setTenantLocation(true);
                    break;
                case "CustomGoalFilter":
                    mHomeActivity.setCustomGoalFilter(true);
                    break;
                case "Product":
                    mHomeActivity.setProduct(true);
                    break;
                case "LocationGroup":
                    mHomeActivity.setLocationGroup(true);
                    break;
                case "User":
                    mHomeActivity.setUser(true);
                    break;

            }

        }

    }


    public void filterCallAfterCheck(){
        mFilterBaseDatas = new ArrayList<>();
        Gson gson = new Gson();
        String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_preference_filter_obj), null);
        mHomeActivity.mPrerferencedLeftFilterBaseDatas = gson.fromJson(json, new TypeToken<ArrayList<FilterBaseData>>(){
        }.getType());
        if(!mHomeActivity.isRegionFilter()){
            if(mHomeActivity.isTenantLocation()){
                pbFilter.setVisibility(View.VISIBLE);
                imgFilter.setVisibility(View.GONE);
                setNeedtoFilterCheck(false);
                getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                        , "name", "ASC", null,
                        "Normal");
                return;
            }
            if(mHomeActivity.isLocationGroup()){
                pbFilter.setVisibility(View.VISIBLE);
                imgFilter.setVisibility(View.GONE);
                setNeedtoFilterCheck(false);
                if(!mHomeActivity.isTenantLocation()){
                    mHomeActivity.setTenantLocation(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        null,
                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                return;
            }
            if(mHomeActivity.isProduct()){
                pbFilter.setVisibility(View.VISIBLE);
                imgFilter.setVisibility(View.GONE);
                setNeedtoFilterCheck(false);
                if(!mHomeActivity.isTenantLocation()){
                    mHomeActivity.setTenantLocation(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                if(!mHomeActivity.isLocationGroup()){
                    mHomeActivity.setLocationGroup(true);
                    getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                getProductList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        null);

            }
            if(mHomeActivity.isUser()){
                pbFilter.setVisibility(View.VISIBLE);
                imgFilter.setVisibility(View.GONE);
                setNeedtoFilterCheck(false);
                if(!mHomeActivity.isTenantLocation()){
                    mHomeActivity.setTenantLocation(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                if(!mHomeActivity.isLocationGroup()){
                    mHomeActivity.setLocationGroup(true);
                    getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        null,
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);

            } else{
                getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                return;
            }
        } else{
            List<DynamicData> mDynamicDatas = new ArrayList<>();
            integerListHashMap = mHomeActivity.getIntegerListHashMap();
            if(integerListHashMap != null && integerListHashMap.size() > 0){
                setNeedtoFilterCheck(false);
                for(int i = 0; i < integerListHashMap.size(); i++){
                    //int count = 0;
                    mDynamicDatas = new ArrayList<>();
                    mDynamicDatas = integerListHashMap.get(i);


                    if(isDynamicFilterExist()){
                        if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){

                            if(!isCheckDone){
                                setSelectedRegionTypeId(String.valueOf(mDynamicDatas.get(i).getRegionTypeId()));
                                setSelectedRegionTypeName(mDynamicDatas.get(i).getRegionTypeName());
                                setSelectedRegionId(String.valueOf(mDynamicDatas.get(i).getId()));
                                setSelectedRegionName(mDynamicDatas.get(i).getName());

                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.regionTypeId = mDynamicDatas.get(i).getRegionTypeId();
                                mFilterBaseData.id = String.valueOf(mDynamicDatas.get(i).getId());
                                mFilterBaseData.name = mDynamicDatas.get(i).getName();
                                mFilterBaseData.filterName = mDynamicDatas.get(i).getRegionTypeName();
                                mFilterBaseData.parentRegionId = mDynamicDatas.get(i).getParentRegionId();
                                mFilterBaseData.isMasterLocation = mDynamicDatas.get(i).isMAsterLocation();
                                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                mFilterBaseData.isDynamicFilter = true;
                                mFilterBaseData.isSelectedPosition = true;
                                mFilterBaseDatasRegion.add(mFilterBaseData);

                                InGaugeSession.write(mContext.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                                InGaugeSession.write(mContext.getString(R.string.key_selected_region_name), getSelectedRegionName());
                                InGaugeSession.write(mContext.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));
                                mHomeActivity.setSelectedRegionName(getSelectedRegionName());
                            }

                            if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                                if(checkRegionId(getSelectedRegionTypeId(), getSelectedRegionId(), mDynamicDatas) && !isCheckDone){
                                    //break;

                                } else{
                                    if(!isCheckDone){

                                        if(i == 0){
                                            mFilterBaseDatasRegion = new ArrayList<>();

                                            setSelectedRegionTypeId(String.valueOf(mDynamicDatas.get(i).getRegionTypeId()));
                                            setSelectedRegionTypeName(mDynamicDatas.get(i).getRegionTypeName());
                                            setSelectedRegionId(String.valueOf(mDynamicDatas.get(i).getId()));
                                            setSelectedRegionName(mDynamicDatas.get(i).getName());

                                            FilterBaseData mFilterBaseData = new FilterBaseData();
                                            mFilterBaseData.regionTypeId = mDynamicDatas.get(i).getRegionTypeId();
                                            mFilterBaseData.id = String.valueOf(mDynamicDatas.get(i).getId());
                                            mFilterBaseData.name = mDynamicDatas.get(i).getName();
                                            mFilterBaseData.filterName = mDynamicDatas.get(i).getRegionTypeName();
                                            mFilterBaseData.parentRegionId = mDynamicDatas.get(i).getParentRegionId();
                                            mFilterBaseData.isMasterLocation = mDynamicDatas.get(i).isMAsterLocation();
                                            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                            mFilterBaseData.isDynamicFilter = true;
                                            mFilterBaseData.isSelectedPosition = true;
                                            mFilterBaseData.isDynamicFilterSelected = true;
                                            mFilterBaseData.isSubRegionOpen = true;
                                            mFilterBaseDatasRegion.add(mFilterBaseData);
                                            InGaugeSession.write(mContext.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                                            InGaugeSession.write(mContext.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));
                                            InGaugeSession.write(mContext.getString(R.string.key_selected_region_name), getSelectedRegionName());
                                            mHomeActivity.setSelectedRegionName(getSelectedRegionName());
                                        } else{

                                            if(i >= mDynamicDatas.size()){
                                                //  throw new IndexOutOfBoundsException();
                                                int position = i - 1;
                                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                                mFilterBaseData.regionTypeId = mDynamicDatas.get(position).getRegionTypeId();
                                                mFilterBaseData.id = String.valueOf(mDynamicDatas.get(position).getId());
                                                mFilterBaseData.name = mDynamicDatas.get(position).getName();
                                                mFilterBaseData.filterName = mDynamicDatas.get(position).getRegionTypeName();
                                                mFilterBaseData.parentRegionId = mDynamicDatas.get(position).getParentRegionId();
                                                mFilterBaseData.isMasterLocation = mDynamicDatas.get(position).isMAsterLocation();
                                                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                                mFilterBaseData.isDynamicFilter = true;
                                                mFilterBaseDatasRegion.add(mFilterBaseData);
                                            } else{
                                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                                mFilterBaseData.regionTypeId = mDynamicDatas.get(i).getRegionTypeId();
                                                mFilterBaseData.id = String.valueOf(mDynamicDatas.get(i).getId());
                                                mFilterBaseData.name = mDynamicDatas.get(i).getName();
                                                mFilterBaseData.filterName = mDynamicDatas.get(i).getRegionTypeName();
                                                mFilterBaseData.parentRegionId = mDynamicDatas.get(i).getParentRegionId();
                                                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                                mFilterBaseData.isMasterLocation = mDynamicDatas.get(i).isMAsterLocation();
                                                mFilterBaseData.isDynamicFilter = true;
                                                mFilterBaseDatasRegion.add(mFilterBaseData);
                                            }


                                        }
                                    }

                                }
                            }


                        } else{
                            if(i == 0){
                                mFilterBaseDatasRegion = new ArrayList<>();

                                setSelectedRegionTypeId(String.valueOf(mDynamicDatas.get(i).getRegionTypeId()));
                                setSelectedRegionTypeName(mDynamicDatas.get(i).getRegionTypeName());
                                setSelectedRegionId(String.valueOf(mDynamicDatas.get(i).getId()));
                                setSelectedRegionName(mDynamicDatas.get(i).getName());

                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.regionTypeId = mDynamicDatas.get(i).getRegionTypeId();
                                mFilterBaseData.id = String.valueOf(mDynamicDatas.get(i).getId());
                                mFilterBaseData.name = mDynamicDatas.get(i).getName();
                                mFilterBaseData.filterName = mDynamicDatas.get(i).getRegionTypeName();
                                mFilterBaseData.parentRegionId = mDynamicDatas.get(i).getParentRegionId();
                                mFilterBaseData.isMasterLocation = mDynamicDatas.get(i).isMAsterLocation();
                                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                mFilterBaseData.isDynamicFilter = true;
                                mFilterBaseData.isSelectedPosition = true;
                                mFilterBaseData.isDynamicFilterSelected = true;
                                mFilterBaseData.isSubRegionOpen = true;
                                mFilterBaseDatasRegion.add(mFilterBaseData);
                                InGaugeSession.write(mContext.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                                InGaugeSession.write(mContext.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));
                                InGaugeSession.write(mContext.getString(R.string.key_selected_region_name), getSelectedRegionName());
                                mHomeActivity.setSelectedRegionName(getSelectedRegionName());
                            } else{

                                if(i >= mDynamicDatas.size()){
                                    int position = i - 1;
                                    FilterBaseData mFilterBaseData = new FilterBaseData();
                                    mFilterBaseData.regionTypeId = mDynamicDatas.get(position).getRegionTypeId();
                                    mFilterBaseData.id = String.valueOf(mDynamicDatas.get(position).getId());
                                    mFilterBaseData.name = mDynamicDatas.get(position).getName();
                                    mFilterBaseData.filterName = mDynamicDatas.get(position).getRegionTypeName();
                                    mFilterBaseData.parentRegionId = mDynamicDatas.get(position).getParentRegionId();
                                    mFilterBaseData.isMasterLocation = mDynamicDatas.get(position).isMAsterLocation();
                                    mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                    mFilterBaseData.isDynamicFilter = true;
                                    mFilterBaseDatasRegion.add(mFilterBaseData);

                                } else{
                                    FilterBaseData mFilterBaseData = new FilterBaseData();
                                    mFilterBaseData.regionTypeId = mDynamicDatas.get(i).getRegionTypeId();
                                    mFilterBaseData.id = String.valueOf(mDynamicDatas.get(i).getId());
                                    mFilterBaseData.name = mDynamicDatas.get(i).getName();
                                    mFilterBaseData.filterName = mDynamicDatas.get(i).getRegionTypeName();
                                    mFilterBaseData.parentRegionId = mDynamicDatas.get(i).getParentRegionId();
                                    mFilterBaseData.isMasterLocation = mDynamicDatas.get(i).isMAsterLocation();
                                    mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                    mFilterBaseData.isDynamicFilter = true;
                                    mFilterBaseDatasRegion.add(mFilterBaseData);
                                }
                            }
                        }
                    } else{
                        if(i == 0){
                            mFilterBaseDatasRegion = new ArrayList<>();

                            setSelectedRegionTypeId(String.valueOf(mDynamicDatas.get(i).getRegionTypeId()));
                            setSelectedRegionTypeName(mDynamicDatas.get(i).getRegionTypeName());
                            setSelectedRegionId(String.valueOf(mDynamicDatas.get(i).getId()));
                            setSelectedRegionName(mDynamicDatas.get(i).getName());

                            FilterBaseData mFilterBaseData = new FilterBaseData();

                            mFilterBaseData.regionTypeId = mDynamicDatas.get(i).getRegionTypeId();
                            mFilterBaseData.id = String.valueOf(mDynamicDatas.get(i).getId());
                            mFilterBaseData.name = mDynamicDatas.get(i).getName();
                            mFilterBaseData.filterName = mDynamicDatas.get(i).getRegionTypeName();
                            mFilterBaseData.parentRegionId = mDynamicDatas.get(i).getParentRegionId();
                            mFilterBaseData.isMasterLocation = mDynamicDatas.get(i).isMAsterLocation();
                            mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                            mFilterBaseData.isDynamicFilter = true;
                            mFilterBaseData.isSelectedPosition = true;
                            mFilterBaseData.isDynamicFilterSelected = true;
                            mFilterBaseData.isSubRegionOpen = true;
                            mFilterBaseDatasRegion.add(mFilterBaseData);
                            InGaugeSession.write(mContext.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                            InGaugeSession.write(mContext.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));
                            InGaugeSession.write(mContext.getString(R.string.key_selected_region_name), getSelectedRegionName());
                            mHomeActivity.setSelectedRegionName(getSelectedRegionName());
                        } else{
                            if(i >= mDynamicDatas.size()){
//                                throw new IndexOutOfBoundsException();
                                try{
                                    int position = i - 1;
                                    FilterBaseData mFilterBaseData = new FilterBaseData();
                                    mFilterBaseData.regionTypeId = mDynamicDatas.get(position).getRegionTypeId();
                                    mFilterBaseData.id = String.valueOf(mDynamicDatas.get(position).getId());
                                    mFilterBaseData.name = mDynamicDatas.get(position).getName();
                                    mFilterBaseData.filterName = mDynamicDatas.get(position).getRegionTypeName();
                                    mFilterBaseData.parentRegionId = mDynamicDatas.get(position).getParentRegionId();
                                    mFilterBaseData.isMasterLocation = mDynamicDatas.get(position).isMAsterLocation();
                                    mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                    mFilterBaseData.isDynamicFilter = true;
                                    mFilterBaseDatasRegion.add(mFilterBaseData);
                                } catch(Exception e){
                                    e.printStackTrace();
                                }
                            } else{
                                try{
                                    FilterBaseData mFilterBaseData = new FilterBaseData();
                                    mFilterBaseData.regionTypeId = mDynamicDatas.get(i).getRegionTypeId();
                                    mFilterBaseData.id = String.valueOf(mDynamicDatas.get(i).getId());
                                    mFilterBaseData.name = mDynamicDatas.get(i).getName();
                                    mFilterBaseData.filterName = mDynamicDatas.get(i).getRegionTypeName();
                                    mFilterBaseData.parentRegionId = mDynamicDatas.get(i).getParentRegionId();
                                    mFilterBaseData.isMasterLocation = mDynamicDatas.get(i).isMAsterLocation();
                                    mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                    mFilterBaseData.isDynamicFilter = true;
                                    mFilterBaseDatasRegion.add(mFilterBaseData);
                                } catch(Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    }


                }
                if(!mHomeActivity.isTenantLocation() && !mHomeActivity.isLocationGroup() && !mHomeActivity.isProduct() && !mHomeActivity.isUser()){
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    return;
                }
                if(mHomeActivity.isTenantLocation()){
                    pbFilter.setVisibility(View.VISIBLE);
                    imgFilter.setVisibility(View.GONE);
                    setNeedtoFilterCheck(false);
                    if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                        if(checkRegionId(getSelectedRegionTypeId(), getSelectedRegionId(), mDynamicDatas)){
                            getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                                    , "name", "ASC", getSelectedRegionId(),
                                    "Normal");
                        } else{
                            getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                                    , "name", "ASC", getSelectedRegionId(),
                                    "Normal");
                        }
                    } else{
                        getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                                , "name", "ASC", getSelectedRegionId(),
                                "Normal");
                    }


                    return;
                }
                if(mHomeActivity.isLocationGroup()){
                    pbFilter.setVisibility(View.VISIBLE);
                    imgFilter.setVisibility(View.GONE);
                    setNeedtoFilterCheck(false);
                    getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                if(mHomeActivity.isProduct()){
                    pbFilter.setVisibility(View.VISIBLE);
                    imgFilter.setVisibility(View.GONE);
                    setNeedtoFilterCheck(false);
                    if(!mHomeActivity.isTenantLocation()){
                        mHomeActivity.setTenantLocation(true);
                        getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                                , "name", "ASC", null,
                                "Normal");
                        return;
                    }
                    if(!mHomeActivity.isLocationGroup()){
                        mHomeActivity.setLocationGroup(true);
                        getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                null,
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                        return;
                    }
                    getProductList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null);
                    return;
                }
                if(mHomeActivity.isUser()){
                    pbFilter.setVisibility(View.VISIBLE);
                    imgFilter.setVisibility(View.GONE);
                    setNeedtoFilterCheck(false);
                    if(!mHomeActivity.isTenantLocation()){
                        mHomeActivity.setTenantLocation(true);
                        getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                                , "name", "ASC", null,
                                "Normal");
                        return;
                    }
                    if(!mHomeActivity.isLocationGroup()){
                        mHomeActivity.setLocationGroup(true);
                        getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                null,
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                        return;
                    }
                    getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);

                } else{
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    return;
                }
            } else{
                getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                return;
            }

        }
    }

    void getDynamicFilterMap(final int tenantId, int userID, boolean isShowDeactivatedLocation){

        Call<ResponseBody> mCall = apiInterface.getDynamicFiltersListMap(tenantId, userID, isShowDeactivatedLocation);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response){


                JSONObject data = null;
                if(response.body() != null){
                    try{

                        JSONObject mJsonObject = new JSONObject(response.body().string());
                        mDataJson = mJsonObject.optJSONObject("data");
                        JSONArray firstJson = mDataJson.optJSONArray("-1");
                        //Map<Integer,JSONArray> integerJSONArrayMap  = new HashMap<>();

                        if(firstJson != null && firstJson.length() > 0){
                            Map<Integer, List<DynamicData>> integerListHashMap = new LinkedHashMap<>();
                            int level = 0;
                            for(int i = 0; i < firstJson.length(); i++){
                                DynamicData mDynamicData = new DynamicData();
                                mDynamicData.setId(firstJson.getJSONObject(i).optInt("id"));
                                mDynamicData.setName(firstJson.getJSONObject(i).optString("name"));
                                mDynamicData.setRegionTypeName(firstJson.getJSONObject(i).optString("regionTypeName"));
                                mDynamicData.setRegionTypeId(firstJson.getJSONObject(i).optInt("regionTypeId"));
                                mDynamicData.setChildRegion(firstJson.getJSONObject(i).optString("childRegion"));
                                mDynamicData.setParentRegionId(firstJson.getJSONObject(i).optInt("parentRegionId"));
                                String masterLocation = firstJson.getJSONObject(i).optString("masterLocation");
                                mDynamicData.setMasterLocation(masterLocation);
                                if(masterLocation != null && masterLocation.length() > 0){
                                    mDynamicData.setMAsterLocation(true);
                                } else{
                                    mDynamicData.setMAsterLocation(false);
                                }
                                mCountryList.add(mDynamicData);
                            }
                            integerListHashMap.put(level, mCountryList);
                            for(DynamicData mDynamicDataParent : mCountryList){
                                if(mDynamicDataParent.getChildRegion() != null && mDynamicDataParent.getChildRegion().length() > 0)
                                    searchParent(mDynamicDataParent, integerListHashMap, level);
                            }


                            for(List<DynamicData> mDynamicData : integerListHashMap.values()){
                                Collections.sort(mDynamicData, new Comparator<DynamicData>(){
                                    @Override
                                    public int compare(DynamicData d1, DynamicData d2){
                                        return Boolean.compare(d2.isMAsterLocation(), d1.isMAsterLocation());
                                    }
                                });

                            }
                            if(mHomeActivity.getIntegerListHashMap() != null && mHomeActivity.getIntegerListHashMap().size() > 0){
                                mHomeActivity.getIntegerListHashMap().clear();
                                mHomeActivity.setIntegerListHashMap(integerListHashMap);
                            } else{
                                mHomeActivity.setIntegerListHashMap(integerListHashMap);
                            }

                            Logger.Error("<<<< List HashMap >>> " + integerListHashMap);


                           /* for(List<DynamicData> mDynamicData : integerListHashMap.values()){
                                Collections.sort(mDynamicData, new Comparator<DynamicData>(){
                                    public int compare(DynamicData v1, DynamicData v2){
                                        return v2.getName().compareTo(v1.getName());
                                    }
                                });


                            }*/



                            Logger.Error("<<<<After Sort List HashMap >>> " + integerListHashMap);
                            Gson gson = new Gson();
                            String json = gson.toJson(integerListHashMap);
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_dynamic_map_filter_obj), json);
                            GetDashboard(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), true, false, false);
                        } else{
                            Gson gson = new Gson();
                            Logger.Error("<<<< List HashMap >>> " + integerListHashMap);
                            String json = gson.toJson(integerListHashMap);
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_dynamic_map_filter_obj), json);
                            GetDashboard(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), true, false, false);
                        }

                    } catch(JSONException e){
                        e.printStackTrace();
                        Logger.Error("<<< Exception  >>>>" + e.toString());
                    } catch(IOException e){
                        e.printStackTrace();
                        Logger.Error("<<< Exception  >>>>" + e.toString());
                    }


                    //getRegionTypes(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){
                t.printStackTrace();
            }
        });
    }

    /**
     * @param mDynamicDataParent
     * @param integerJSONArrayMap
     * @param level
     */
    public void searchParent(DynamicData mDynamicDataParent, Map<Integer, List<DynamicData>> integerJSONArrayMap, int level){
        try{

            if(mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId())) != null){
                level++;
                JSONArray mJsonArray = mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId()));
                for(int i = 0; i < mJsonArray.length(); i++){
                    DynamicData mDynamicData = new DynamicData();
                    mDynamicData.setId(mJsonArray.getJSONObject(i).optInt("id"));
                    mDynamicData.setName(mJsonArray.getJSONObject(i).optString("name"));
                    mDynamicData.setRegionTypeName(mJsonArray.getJSONObject(i).optString("regionTypeName"));
                    mDynamicData.setRegionTypeId(mJsonArray.getJSONObject(i).optInt("regionTypeId"));
                    mDynamicData.setChildRegion(mJsonArray.getJSONObject(i).optString("childRegion"));
                    mDynamicData.setParentRegionId(mJsonArray.getJSONObject(i).optInt("parentRegionId"));
                    String masterLocation = mJsonArray.getJSONObject(i).optString("masterLocation");
                    mDynamicData.setMasterLocation(masterLocation);
                    if(masterLocation != null && masterLocation.length() > 0){
                        mDynamicData.setMAsterLocation(true);
                    } else{
                        mDynamicData.setMAsterLocation(false);
                    }
                    if(integerJSONArrayMap.get(level) != null){
                        List<DynamicData> mDynamicDataList = integerJSONArrayMap.get(level);
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level, mDynamicDataList);

                    } else{
                        List<DynamicData> mDynamicDataList = new ArrayList<>();
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level, mDynamicDataList);
                    }
                    searchParent(mDynamicData, integerJSONArrayMap, level);
                    //mCountryList.add(mDynamicData);
                }
                return;
            }
        } catch(JSONException e){
            e.printStackTrace();
        }


    }

    public String getSelectedRegionId(){
        return selectedRegionId;
    }

    public void setSelectedRegionId(String selectedRegionId){
        this.selectedRegionId = selectedRegionId;
    }

    public String getSelectedRegionName(){
        return selectedRegionName;
    }

    public void setSelectedRegionName(String selectedRegionName){
        this.selectedRegionName = selectedRegionName;
    }

    public String getSelectedRegionTypeId(){
        return selectedRegionTypeId;
    }

    public void setSelectedRegionTypeId(String selectedRegionTypeId){
        this.selectedRegionTypeId = selectedRegionTypeId;
    }

    public String getSelectedRegionTypeName(){
        return selectedRegionTypeName;
    }

    public void setSelectedRegionTypeName(String selectedRegionTypeName){
        this.selectedRegionTypeName = selectedRegionTypeName;
    }


    public boolean isNeedtoFilterCheck(){
        return isNeedtoFilterCheck;
    }

    public void setNeedtoFilterCheck(boolean needtoFilterCheck){
        isNeedtoFilterCheck = needtoFilterCheck;
    }

    public void setToolbarTitles(){

        if(selectedDashboardId != -1){
            String locationName = InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), "");
            String userName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");
            String productName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name), "");
            String cname = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_region_name), "");
            String countryName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_region_name), "");
            ;
            /*if(countryName.equalsIgnoreCase(getSelectedRegionName())){
                countryName = getSelectedRegionName();
            }else{
                countryName = cname;
            }*/


            mHomeActivity.tvTitle.setText(dashboardName);
            mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
            if(mHomeActivity.isRegionFilter()){
                if(countryName.length() > 0){
                    mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                    mHomeActivity.tvSubTitle.setText(countryName);
                }

            }

            if(mHomeActivity.isTenantLocation() && mHomeActivity.isUser()){
                mHomeActivity.tvSubTitle.setText(locationName + "-" + userName);
                return;
            }
            if(mHomeActivity.isTenantLocation() && mHomeActivity.isProduct()){
                mHomeActivity.tvSubTitle.setText(locationName + "-" + productName);
                return;
            }
            if(mHomeActivity.isTenantLocation()){
                mHomeActivity.tvSubTitle.setText(locationName);
            } else{
                if(!mHomeActivity.isRegionFilter()){
                    mHomeActivity.tvSubTitle.setVisibility(View.GONE);
                }
            }


        } else{
            mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.menu_dashboard));
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth){
        Logger.Error("Month " + month + "Year " + year + "Day of Month " + dayOfMonth);
        //Month: June - Year 2017
        String firstDate = "", lastDate = "";
        String firstDateDisplay = "", lastDateDisplay = "";
        selectedMonth = month;
        selectedYear = year;
        selectedDay = dayOfMonth;
        savedMonth = month;
        savedYear = year;
        selectedCalendar.set(year, month, dayOfMonth);
        try{
            firstDate = DateTimeUtils.getFirstDay(selectedCalendar.getTime());
            lastDate = DateTimeUtils.getLastDay(selectedCalendar.getTime());
            firstDateDisplay = DateTimeUtils.getFirstDayForDisplay(selectedCalendar.getTime());
            lastDateDisplay = DateTimeUtils.getLastDayForDisplay(selectedCalendar.getTime());
            Logger.Error("First Date: " + firstDate);
            Logger.Error("Last Date: " + lastDate);

            Logger.Error("First Date Display : " + firstDateDisplay);
            Logger.Error("Last Date Display : " + lastDateDisplay);
        } catch(Exception e){
            e.printStackTrace();
        }

        if(TextUtils.isEmpty(firstDate) || TextUtils.isEmpty(lastDate)){
            try{
                firstDate = DateTimeUtils.getFirstDay(new Date());
                lastDate = DateTimeUtils.getLastDay(new Date());
                firstDateDisplay = DateTimeUtils.getFirstDayForDisplay(new Date());
                lastDateDisplay = DateTimeUtils.getLastDayForDisplay(new Date());
                System.out.println("First Date: " + firstDate);
                System.out.println("Last Date: " + lastDate);

            } catch(Exception e){
                e.printStackTrace();
            }
        }


        //if (InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "").equalsIgnoreCase("")) {
        InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date), firstDateDisplay);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_server), firstDate);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date), lastDateDisplay);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_server), lastDate);

        //}
        //if (InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "").equalsIgnoreCase("")) {
        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date), sdf.format(firstDateOfPreviousMonth));
        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date), sdf.format(prevDate));
        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(firstDateOfPreviousMonth));
        InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(prevDate));
        //}
        tvSelectDate.setText("Month: " + STR_MONTH[savedMonth - 1] + " | Year " + savedYear);


        mHomeActivity.setDashboardUpdateAfterApply(false);
        if(selectedDashboardId != -1){
            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
        } else{
            mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.menu_dashboard));
            mHomeActivity.tvSubTitle.setText("");
        }
    }


    public boolean checkRegionId(String regionTypeId, String regionId, List<DynamicData> mDynamicDatas){
        mFilterBaseDatasRegion = new ArrayList<>();
        if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
            for(int i = 0; i < mHomeActivity.mPrerferencedLeftFilterBaseDatas.size(); i++){
                if(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).isDynamicFilter && mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).isDynamicFilterSelected){

                    if(mDynamicDatas != null && mDynamicDatas.size() > 0){
                        for(int j = 0; j < mDynamicDatas.size(); j++){
                            if(String.valueOf(mDynamicDatas.get(j).getRegionTypeId()).equalsIgnoreCase(String.valueOf(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).regionTypeId)) &&
                                    String.valueOf(mDynamicDatas.get(j).getId()).equalsIgnoreCase(String.valueOf(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).id))){
                                setSelectedRegionTypeId(String.valueOf(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).regionTypeId));
                                setSelectedRegionTypeName(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).filterName);
                                setSelectedRegionId(String.valueOf(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).id));
                                setSelectedRegionName(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).name);
                                mHomeActivity.setSelectedRegionName(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).name);
                                InGaugeSession.write(mContext.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                                InGaugeSession.write(mContext.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));
                                InGaugeSession.write(mContext.getString(R.string.key_selected_region_name), getSelectedRegionName());
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.regionTypeId = mDynamicDatas.get(j).getRegionTypeId();
                                mFilterBaseData.id = String.valueOf(mDynamicDatas.get(j).getId());
                                mFilterBaseData.name = mDynamicDatas.get(j).getName();
                                mFilterBaseData.filterName = mDynamicDatas.get(j).getRegionTypeName();
                                mFilterBaseData.parentRegionId = mDynamicDatas.get(j).getParentRegionId();
                                mFilterBaseData.filterIndex = mFilterBaseDatas.size();
                                mFilterBaseData.isDynamicFilter = true;
                                mFilterBaseData.isMasterLocation = mDynamicDatas.get(j).isMAsterLocation();
                                mFilterBaseDatasRegion.add(mFilterBaseData);
                                isCheckDone = true;
                                return true;
                            }
                        }
                    }

                }
            }
        }
        return false;
    }

    public boolean checkTenantLocationData(String id){

        if(mHomeActivity.mAccessibleTenantLocationDataModel != null && (mHomeActivity.mAccessibleTenantLocationDataModel.getData() != null && mHomeActivity.mAccessibleTenantLocationDataModel.getData().size() > 0)){

            for(int i = 0; i < mHomeActivity.mAccessibleTenantLocationDataModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(i).getId()))){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkLocationGroupData(String id){

        if(mHomeActivity.mDashboardLocationGroupListModel != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModel.getData().size() > 0)){

            for(int i = 0; i < mHomeActivity.mDashboardLocationGroupListModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(i).getLocationGroupID()))){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkProductData(String id){

        if(mHomeActivity.mProductListFromLocationGroupModel != null && (mHomeActivity.mProductListFromLocationGroupModel.getData() != null && mHomeActivity.mProductListFromLocationGroupModel.getData().size() > 0)){
            for(int i = 0; i < mHomeActivity.mProductListFromLocationGroupModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mHomeActivity.mProductListFromLocationGroupModel.getData().get(i).getId()))){
                    return true;
                }
            }
        } else if(id.equalsIgnoreCase("-1")){
            return true;
        } else if(id.equalsIgnoreCase("-2")){
            return true;
        }


        return false;
    }

    public boolean checkuserData(String id){

        if(mHomeActivity.mDashboardUserListModel != null && (mHomeActivity.mDashboardUserListModel.getData() != null && mHomeActivity.mDashboardUserListModel.getData().size() > 0)){

            for(int i = 0; i < mHomeActivity.mDashboardUserListModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mHomeActivity.mDashboardUserListModel.getData().get(i).getId()))){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isDynamicFilterExist(){

        if((mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0)){
            for(int i = 0; i < mHomeActivity.mPrerferencedLeftFilterBaseDatas.size(); i++){
                if(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).isDynamicFilter){
                    return true;
                }
            }
        }
        return false;

    }

    public FilterBaseData getSelectedPreferenceData(String filterName){
        if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
            for(int i = 0; i < mHomeActivity.mPrerferencedLeftFilterBaseDatas.size(); i++){
                if(filterName.equalsIgnoreCase(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).filterName)){
                    return mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i);
                }
            }
        }
        return null;
    }


}
