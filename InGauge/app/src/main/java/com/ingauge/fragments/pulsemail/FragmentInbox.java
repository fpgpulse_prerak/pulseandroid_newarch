package com.ingauge.fragments.pulsemail;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.MailInboxAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.listener.RecyclerViewClickListenerForMail;
import com.ingauge.pojo.FetchMailBody;
import com.ingauge.pojo.MailListDataModel;
import com.ingauge.pojo.MailListModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 11-Jul-17.
 */

public class FragmentInbox extends BaseFragment implements View.OnClickListener, RecyclerViewClickListenerForMail{
    private RecyclerView mRecyclerViewMailList;
    private MailInboxAdapter mailInboxAdapter;
    View mview;
    private HomeActivity mHomeActivity;
    public boolean isNeedtoDelete = false;
    FloatingActionButton mFloatingActionButtonEditDelete;
    private TextView tvEmailStatus;
    private Context mContext;
    List<MailListModel.Datum> mMaDatumList;
    APIInterface apiInterface;
    Call mCall;
    private TextView tvNoData;
    public static String KEY_SENDER_ID = "key_sender_id";
    public static String KEY_RECEIVER_ID = "key_receiver_id";
    public static String KEY_MAIL_TYPE = "key_mail_type";
    public static String KEY_MAIL_STATUS = "key_mail_status";
    private int senderid;
    private int receiverid;
    private String mailType = "";
    private String mailStatus = "";
    private LinearLayout llBottomMailStatus;
    private TextView tvAll;
    private TextView tvRead;
    private TextView tvUnread;
    //private String mailStatuslabel = "All";
    int unreadCount = 0;
    private SearchView mSearchViewEmail;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        //  mMaDatumList = new ArrayList<>();
        if(getArguments() != null){
            senderid = getArguments().getInt(KEY_SENDER_ID);
            receiverid = getArguments().getInt(KEY_RECEIVER_ID);
            mailType = getArguments().getString(KEY_MAIL_TYPE);
            mailStatus = getArguments().getString(KEY_MAIL_STATUS);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        mview = inflater.inflate(R.layout.fragment_inbox, container, false);
        initViews(mview);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        mRecyclerViewMailList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerViewMailList.setHasFixedSize(true);
        mRecyclerViewMailList.setLayoutManager(mLayoutManager);
        mRecyclerViewMailList.setItemAnimator(new DefaultItemAnimator());

        if(mMaDatumList != null && mMaDatumList.size() > 0){
            mMaDatumList.clear();
            /*mailInboxAdapter = new MailInboxAdapter(mHomeActivity, FragmentInbox.this, this, mMaDatumList, mailType);
            mRecyclerViewMailList.setAdapter(mailInboxAdapter);*/
        }
//        } else {
        Logger.Error("Call 1");
        if(mailStatus != null){
            if(mailStatus.equalsIgnoreCase("all")){
                tvEmailStatus.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_all)));
            } else if(mailStatus.equalsIgnoreCase("read")){
                tvEmailStatus.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_read)));
            } else if(mailStatus.equalsIgnoreCase("unread")){
                tvEmailStatus.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_unread)));
            }
        }
        setFetchEmailBody(senderid, receiverid, mailType, mailStatus, "");
//        }

        return mview;
    }

    void initViews(View mView){
        mSearchViewEmail = (SearchView) mView.findViewById(R.id.fragment_detail_filter_list_search_view);
        mSearchViewEmail.setQueryHint(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_search)));
        SearchManager searchManager = (SearchManager) mContext.getSystemService(Context.SEARCH_SERVICE);
        //mSearchViewEmail.setSearchableInfo(searchManager.getSearchableInfo());
        mSearchViewEmail.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        mRecyclerViewMailList = (RecyclerView) mView.findViewById(R.id.recycler_view);
        mFloatingActionButtonEditDelete = (FloatingActionButton) mView.findViewById(R.id.fragment_inbox_fb_edit);
        if(mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessComposeMail") || mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeletMail")){
            mFloatingActionButtonEditDelete.setVisibility(View.VISIBLE);
        } else if(mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessComposeMail")){
            mFloatingActionButtonEditDelete.setVisibility(View.VISIBLE);
        } else if(mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeletMail")){
            mFloatingActionButtonEditDelete.setVisibility(View.VISIBLE);
        } else{
            mFloatingActionButtonEditDelete.setVisibility(View.GONE);
        }
        mFloatingActionButtonEditDelete.setOnClickListener(this);
        tvNoData = (TextView) mView.findViewById(R.id.fragment_inbox_tv_no_data);
        llBottomMailStatus = (LinearLayout) mView.findViewById(R.id.row_mail_status_parent_ll);
        tvEmailStatus = (TextView) mView.findViewById(R.id.fragment_inbox_tv_all);
        tvEmailStatus.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_all)));
        tvAll = (TextView) mView.findViewById(R.id.row_mail_status_tv_all);
        tvAll.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_all)));
        tvAll.setOnClickListener(this);
        tvRead = (TextView) mView.findViewById(R.id.row_mail_status_tv_read);
        tvRead.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_read)));
        tvRead.setOnClickListener(this);
        tvUnread = (TextView) mView.findViewById(R.id.row_mail_status_tv_unread);
        tvUnread.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_unread)));
        tvUnread.setOnClickListener(this);
        tvEmailStatus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(llBottomMailStatus.getVisibility() == View.GONE){
                    Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_up);
                    llBottomMailStatus.startAnimation(bottomUp);
                    llBottomMailStatus.setVisibility(View.VISIBLE);
                } else{
                    Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_down);
                    llBottomMailStatus.startAnimation(bottomUp);
                    llBottomMailStatus.setVisibility(View.GONE);
                }

            }
        });

        mSearchViewEmail.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String query){
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText){
                if(newText.length() > 3){
                    Logger.Error("Call 2");
                    setFetchEmailBody(senderid, receiverid, mailType, mailStatus, newText);
                } else if(newText.length() == 0){
                    Logger.Error("Call 3");
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    InGaugeApp.getFirebaseAnalytics().logEvent("search_mail", bundle);
                    setFetchEmailBody(senderid, receiverid, mailType, mailStatus, "");
                }
                return false;
            }
        });
        mSearchViewEmail.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener(){
            @Override
            public void onViewAttachedToWindow(View v){

            }

            @Override
            public void onViewDetachedFromWindow(View v){

            }
        });
        mSearchViewEmail.setOnCloseListener(new SearchView.OnCloseListener(){
            @Override
            public boolean onClose(){
                //mSearchViewEmail.setQuery("", false);
                // mSearchViewEmail.setIconified(false);

                Logger.Error("Call 4");

                return false;
            }
        });
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeRefreshLayout);
        //   mSwipeRefreshLayout.setVisibility(View.INVISIBLE);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.in_guage_blue,
                R.color.rv_grey_color,
                R.color.red);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh(){
                // Refresh items
                refreshItems();
            }
        });
    }


    void refreshItems(){

        setFetchEmailBody(senderid, receiverid, mailType, mailStatus, "");
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void setFetchEmailBody(int recUserid, int senderUserid, String mailType, String mailStatus, String searchText){
        /*{
            "solrInput":{
            "first": 0,
                    "total": 100,
                    "sortBy": ["createdOn"],
            "sortOrder": ["DESC"],
            "searchText": "test"

        },
            "recipientId":646,
                "senderId":646,
                "type":"inbox",
                "mailReadStatus":"all"
        }*/
        FetchMailBody fetchMailBody = new FetchMailBody();
        FetchMailBody.SolrInput mSolrInput = fetchMailBody.getSolrInput();
        mSolrInput.setFirst(0);
        mSolrInput.setTotal(100);
        List<String> mSortyByStrings = new ArrayList<>();
        mSortyByStrings.add("createdOn");
        mSolrInput.setSortBy(mSortyByStrings);
        mSolrInput.setSearchText(searchText);
        List<String> mSortyOrderStrings = new ArrayList<>();
        mSortyOrderStrings.add("DESC");
        mSolrInput.setSortOrder(mSortyOrderStrings);
        fetchMailBody.setSolrInput(mSolrInput);
        fetchMailBody.setRecipientId(recUserid);
        fetchMailBody.setSenderId(senderUserid);
        fetchMailBody.setType(mailType);
        fetchMailBody.setMailReadStatus(mailStatus);
        Gson gson = new Gson();
        String json = gson.toJson(fetchMailBody);
        Logger.Error("<<<<  Json Response >>>> " + json);
        fetchEmail(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), fetchMailBody);


    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        if(mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeletMail")){
            mHomeActivity.tvApply.setVisibility(View.VISIBLE);
        } else{
            mHomeActivity.tvApply.setVisibility(View.GONE);
        }
        //mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.delete));
        mHomeActivity.tvApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_delete)));
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        mHomeActivity.tvApply.setOnClickListener(this);

        mHomeActivity.ibMenu.setVisibility(View.GONE);
        if(mailType.equalsIgnoreCase("inbox")){
            mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail)) + "(" + unreadCount + ")");
            getUnreadCount(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""));
            tvEmailStatus.setVisibility(View.VISIBLE);
        } else if(mailType.equalsIgnoreCase("sent")){
            mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_sent)));

            new Handler().postDelayed(new Runnable(){
                @Override
                public void run(){
                    try{
                        mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_sent)));
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }, 1500);
            tvEmailStatus.setVisibility(View.GONE);
        } else if(mailType.equalsIgnoreCase("trash")){
            mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_trash)));
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run(){
                    try{
                        mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_trash)));
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }, 1500);
            tvEmailStatus.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onAttachFragment(Fragment childFragment){
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.tv_apply:

                if(mHomeActivity.tvApply.getText().toString().equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_delete)))){
                    if(mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessDeletMail")){
                        mFloatingActionButtonEditDelete.setVisibility(View.VISIBLE);
                    } else{
                        mFloatingActionButtonEditDelete.setVisibility(View.GONE);
                    }
                    if(mailInboxAdapter != null && mailInboxAdapter.getItemCount() > 0){
                        //mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.cancel));
                        mHomeActivity.tvApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_cancel)));
                        mFloatingActionButtonEditDelete.setImageResource(R.drawable.ic_delete_mail);
                        isNeedtoDelete = true;
                        mailInboxAdapter.notifyDataSetChanged();
                    }
                } else if(mHomeActivity.tvApply.getText().toString().equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_cancel)))){
                    if(mailInboxAdapter != null && mailInboxAdapter.getItemCount() > 0){

                        //mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.delete));
                        mHomeActivity.tvApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_delete)));
                        mFloatingActionButtonEditDelete.setImageResource(R.drawable.ic_email_edit);
                        isNeedtoDelete = false;
                        mailInboxAdapter.notifyDataSetChanged();


                        if(mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessComposeMail")){
                            mFloatingActionButtonEditDelete.setVisibility(View.VISIBLE);
                        } else{
                            mFloatingActionButtonEditDelete.setVisibility(View.GONE);
                        }
                    }
                }
                break;
            case R.id.fragment_inbox_fb_edit:
                if(mHomeActivity.tvApply.getText().toString().equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_cancel)))){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    AlertDialog dialog;
                    builder.setTitle("");
                    builder.setMessage(mContext.getResources().getString(R.string.alert_delete));
                    builder.setCancelable(false);
                    builder.setPositiveButton(
                            mContext.getResources().getString(R.string.alert_yes),
                            new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){
                                    mailInboxAdapter.deleteData();
                                    //mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.delete));
                                    mHomeActivity.tvApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_delete)));
                                    mFloatingActionButtonEditDelete.setImageResource(R.drawable.ic_email_edit);
                                    isNeedtoDelete = false;
                                    mailInboxAdapter.notifyDataSetChanged();
                                }
                            });
                    builder.setNegativeButton(
                            mContext.getResources().getString(R.string.alert_no), new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){

                                    //mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.delete));
                                    mHomeActivity.tvApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_delete)));
                                    mFloatingActionButtonEditDelete.setImageResource(R.drawable.ic_email_edit);
                                    isNeedtoDelete = false;
                                    mailInboxAdapter.notifyDataSetChanged();
                                }
                            }
                    );
                    //  builder.show();
                    dialog = builder.create();
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    dialog.show();


                } else{
                    FragmentComposeMail mFragmentComposeMail = new FragmentComposeMail();
                    mHomeActivity.replace(mFragmentComposeMail, mContext.getResources().getString(R.string.tag_compose_mail));
                }
                break;
            case R.id.row_mail_status_tv_all:
                tvAll.setBackground(mContext.getResources().getDrawable(R.drawable.white_transperant_left_rounded_drawable));
                tvAll.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue));

                tvRead.setBackgroundColor(0);
                tvRead.setTextColor(mContext.getResources().getColor(R.color.white));

                tvUnread.setBackground(null);
                tvUnread.setTextColor(mContext.getResources().getColor(R.color.white));
                // mailStatuslabel = "All";
                tvEmailStatus.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_all)));
                //tvEmailStatus.setText(mailStatuslabel);
                mailStatus = "all";
                setFetchEmailBody(senderid, receiverid, mailType, mailStatus, "");
                Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                        R.anim.bottom_down);
                llBottomMailStatus.startAnimation(bottomUp);
                llBottomMailStatus.setVisibility(View.GONE);
                break;
            case R.id.row_mail_status_tv_read:
                tvRead.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                tvRead.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue));


                tvAll.setBackground(null);
                tvAll.setTextColor(mContext.getResources().getColor(R.color.white));

                tvUnread.setBackground(null);
                tvUnread.setTextColor(mContext.getResources().getColor(R.color.white));
                //mailStatuslabel = "Read";
                mailStatus = "read";
                //tvEmailStatus.setText(mailStatuslabel);
                tvEmailStatus.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_read)));
                Logger.Error("Call 6");
                setFetchEmailBody(senderid, receiverid, mailType, mailStatus, "");
                bottomUp = AnimationUtils.loadAnimation(getContext(),
                        R.anim.bottom_down);
                llBottomMailStatus.startAnimation(bottomUp);
                llBottomMailStatus.setVisibility(View.GONE);
                break;
            case R.id.row_mail_status_tv_unread:
                tvUnread.setBackground(mContext.getResources().getDrawable(R.drawable.white_transperant_right_rounded_drawable));
                tvUnread.setTextColor(mContext.getResources().getColor(R.color.in_guage_blue));

                tvAll.setBackground(null);
                tvAll.setTextColor(mContext.getResources().getColor(R.color.white));

                tvRead.setBackgroundColor(0);
                tvRead.setTextColor(mContext.getResources().getColor(R.color.white));
                //mailStatuslabel = "Unread";
                mailStatus = "unread";
                //tvEmailStatus.setText(mailStatuslabel);
                tvEmailStatus.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_unread)));
                Logger.Error("Call 7");
                setFetchEmailBody(senderid, receiverid, mailType, mailStatus, "");

                bottomUp = AnimationUtils.loadAnimation(getContext(),
                        R.anim.bottom_down);
                llBottomMailStatus.startAnimation(bottomUp);
                llBottomMailStatus.setVisibility(View.GONE);
                break;

        }
    }

    @Override
    public void recyclerViewListClicked(View v, int position){
        if(mHomeActivity.tvApply.getText().toString().equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_delete)))){
            Bundle mBundle = new Bundle();
            //TODO Remove Static when module is complete
            mBundle.putString(FragmentEmailDetailsNew.KEY_EMAIL_ID, String.valueOf(mMaDatumList.get(position).getId()));
            mBundle.putString(FragmentEmailDetailsNew.KEY_EMAIL_TYPE, mailType);
            mBundle.putString(FragmentEmailDetailsNew.KEY_MAIL_SUBJECT, mMaDatumList.get(position).getSubject());
            FragmentEmailDetailsNew mFragmentEmailDetails = new FragmentEmailDetailsNew();
            mFragmentEmailDetails.setArguments(mBundle);
            mHomeActivity.replace(mFragmentEmailDetails, mContext.getResources().getString(R.string.tag_pulse_mail_details));
        }

    }

    void fetchEmail(String authToken, FetchMailBody fetchMailBody){

        mHomeActivity.endProgress();
        ///mHomeActivity.startProgress(mContext);
        mCall = apiInterface.fetchMail(fetchMailBody);
        mCall.enqueue(new Callback<MailListDataModel>(){
            @Override
            public void onResponse(Call<MailListDataModel> call, Response<MailListDataModel> response){
                mHomeActivity.endProgress();
                try{
                    MailListDataModel mailData = response.body();
                    bindAdapter(mailData.getMailListModel().getData());


                } catch(Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    public void bindAdapter(List<MailListModel.Datum> mMaDatumList){
        this.mMaDatumList = mMaDatumList;
        if(mMaDatumList != null && mMaDatumList.size() > 0){
            mRecyclerViewMailList.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);

            mailInboxAdapter = new MailInboxAdapter(mHomeActivity, FragmentInbox.this, this, mMaDatumList, mailType);
            mRecyclerViewMailList.setAdapter(mailInboxAdapter);
        } else{
            mRecyclerViewMailList.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
            if(mailType.equalsIgnoreCase("inbox")){
                //tvNoData.setText(mContext.getResources().getString(R.string.nothing_in_inbox));
                tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_not_found)));
                // tvNoData.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_inbox, 0, 0);
            } else if(mailType.equalsIgnoreCase("sent")){
                //tvNoData.setText(mContext.getResources().getString(R.string.nothing_in_sent));
                tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_not_found)));
                // tvNoData.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_sent, 0, 0);
            } else if(mailType.equalsIgnoreCase("trash")){
                //tvNoData.setText(mContext.getResources().getString(R.string.nothing_in_trash));
                tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_not_found)));
                //  tvNoData.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_trash, 0, 0);
            }
        }
    }

    void getUnreadCount(String authToken){

        /*{"emailId":["10583","10590","646"],
            "subject":"New Mail",
                "message":"ABC"
        }*/

        mHomeActivity.endProgress();
        //mHomeActivity.startProgress(mContext);
        mCall = apiInterface.getUnreadCount();
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                mHomeActivity.endProgress();
                try{
                    JSONObject mJsonObject = new JSONObject(response.body().string());
                    JSONObject unreadCountObj = mJsonObject.optJSONObject("data");
                    unreadCount = unreadCountObj.optInt("unreadCount");
                    //mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_mail) + " " + "(" + unreadCount + ")");
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail)) + "" + "(" + unreadCount + ")");
                    Logger.Error("Response " + mJsonObject.toString());

                } catch(JSONException e){
                    e.printStackTrace();
                } catch(IOException e){
                    e.printStackTrace();
                } catch(Exception e){
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);

    }
}
