package com.ingauge.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.DynamicFilterLeftAdapter;
import com.ingauge.adapters.FilterDetailListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.DynamicData;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.FilterMetricTypeModel;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.pojo.RegionByTenantRegionTypeModel;
import com.ingauge.pojo.RegionTypeDataModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.SimpleDividerItemDecoration;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 05-Oct-17.
 */

public class FragmentFilterDynamicList extends BaseFragment implements View.OnClickListener, RecyclerViewClickListener{


    Context mContext;
    View rootView;
    HomeActivity mHomeActivity;
    ArrayList<FilterBaseData> mLeftFilterBaseDatas;
    ArrayList<FilterBaseData> mRightFilterBaseDatas;
    Call mCall;
    APIInterface apiInterface;

    private TextView tvFilterClear;
    private TextView tvApply;
    private RecyclerView rvFilterList;
    private RecyclerView rvFilterLeftList;
    public static SearchView mSearchView;

    //    private RegionTypeDataModel mRegionTypeDataModel;
//    private RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel;
    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel;
    private DashboardLocationGroupListModel mDashboardLocationGroupListModel;
    private ProductListFromLocationGroupModel mProductListFromLocationGroupModel;
    private DashboardUserListModel mDashboardUserListModel;

    FilterDetailListAdapter mFilterListAdapter;
    DynamicFilterLeftAdapter mDynamicFilterLeftAdapter;

    String selectedId = "";
    String selectedName = "";

    public Map<Integer, List<DynamicData>> integerListHashMap;


    String TenantLocationIdForDynamic = "-2";
    String LocationGroupIdDynamic = "-2";
    private String selectedRegionId = "";
    private String selectedRegionName = "";
    private String selectedRegionTypeId = "";
    private String selectedRegionTypeName = "";

    boolean isFromFilledInitial = false;
    private boolean isNeedUpdateMetricType = false;
    private String metricType = "";

    private boolean isOnCLearClicked = false;
    long mLastClickTime = 0;
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mLeftFilterBaseDatas = new ArrayList<>();
        mRightFilterBaseDatas = new ArrayList<>();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_dynamic_dashboard_filter, container, false);
        initView(rootView);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){


            @Override
            public boolean onQueryTextSubmit(String query){
                Logger.Error("<<<<   onQueryTextSubmit >>>>>");
                // FilterDetailListAdapter.ivRight.setVisibility(View.VISIBLE);
                //mFilterRightListAdapter.notifyDataSetChanged();
                Logger.Error("<<<<   Called onQueryTextSubmit >>>>>");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText){
                if(mFilterListAdapter != null){
                    mFilterListAdapter.getFilter().filter(newText);
                }

                Logger.Error("<<<<   onQueryTextChange  >>>>>");
                /*if(TextUtils.isEmpty(newText)){
                    this.onQueryTextSubmit("");
                    //if(FilterDetailListAdapter.ivRight!=null){
                    //}
                }else{
                    FilterDetailListAdapter.ivRight.setVisibility(View.GONE);
                }*/
                return false;
            }

        });

        Gson gson = new Gson();
        String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_filter_obj), null);
        mHomeActivity.mMainLeftFilterBaseDatas = gson.fromJson(json, new TypeToken<ArrayList<FilterBaseData>>(){
        }.getType());
        gson = new Gson();
        json = InGaugeSession.read(mContext.getResources().getString(R.string.key_preference_filter_obj), null);
        mHomeActivity.mPrerferencedLeftFilterBaseDatas = gson.fromJson(json, new TypeToken<ArrayList<FilterBaseData>>(){
        }.getType());

        if(mHomeActivity.getmMainLeftFilterBaseDatas() != null && mHomeActivity.getmMainLeftFilterBaseDatas().size() > 0){
            mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModel;
            mDashboardLocationGroupListModel = mHomeActivity.mDashboardLocationGroupListModel;
            mProductListFromLocationGroupModel = mHomeActivity.mProductListFromLocationGroupModel;
            mDashboardUserListModel = mHomeActivity.mDashboardUserListModel;
            integerListHashMap = mHomeActivity.getIntegerListHashMap();
            mLeftFilterBaseDatas.clear();
            mLeftFilterBaseDatas = mHomeActivity.getmMainLeftFilterBaseDatas();
            isFromFilledInitial = true;
            fetchInitalRightListData();
            setLeftFilterdataListAdapter(mLeftFilterBaseDatas, 0);
        } else{
            isFromFilledInitial = false;
            integerListHashMap = mHomeActivity.getIntegerListHashMap();
            if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                isFromFilledInitial = true;
                checkPreferenceandgetLeftFilter();
            } else{
                fetchinitalDataForLeftFilter();
                fetchInitalRightListData();
            }
        }
        /*integerListHashMap = mHomeActivity.getIntegerListHashMap();
        fetchinitalDataForLeftFilter();
        fetchInitalRightListData();*/
        selectedId = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_regiontype), 1));
        selectedName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_regiontype_name), "");

        return rootView;
    }

    public void initView(View itemView){

        tvFilterClear = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_clear_all);
        tvApply = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_apply);
        rvFilterLeftList = (RecyclerView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_rv_left);
        rvFilterList = (RecyclerView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_rv);
        mSearchView = (SearchView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_search_view);
        tvFilterClear.setOnClickListener(this);
        tvApply.setOnClickListener(this);
    }

    public void fetchInitalRightListData(){
        mHomeActivity.endProgress();
        mHomeActivity.endProgress();
        if(mLeftFilterBaseDatas.size() > 0){
            if(mRightFilterBaseDatas != null && mRightFilterBaseDatas.size() > 0){
                mRightFilterBaseDatas.clear();
            }
            if(mHomeActivity.isRegionFilter()){

                int firstregionTypeId = mLeftFilterBaseDatas.get(0).regionTypeId;
                if(integerListHashMap != null && integerListHashMap.size() > 0){
                    for(int i = 0; i < integerListHashMap.size(); i++){
                        int count = 0;
                        List<DynamicData> mDynamicDatas = integerListHashMap.get(i);
                        for(DynamicData mDynamicData : mDynamicDatas){

                            if(mDynamicData.getRegionTypeId() == firstregionTypeId){
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(mDynamicData.getId());
                                mFilterBaseData.name = mDynamicData.getName();
                                mFilterBaseData.regionTypeId = mDynamicData.getRegionTypeId();
                                setSelectedRegionId(mFilterBaseData.id);
                                setSelectedRegionName(mFilterBaseData.name);
                                setSelectedRegionTypeId(String.valueOf(mFilterBaseData.regionTypeId));
                                setSelectedRegionTypeName(String.valueOf(mFilterBaseData.filterName));
                                mFilterBaseData.filterMetricType = mDynamicData.getRegionTypeName();
                                mFilterBaseData.parentRegionId = mDynamicData.getParentRegionId();
                                //mFilterBaseData.filterName = getString(R.string.location);
                                mFilterBaseData.isMasterLocation = mDynamicData.isMAsterLocation();
                                mFilterBaseData.filterName = mDynamicData.getRegionTypeName();
                                mFilterBaseData.filterIndex = i;
                                if(mHomeActivity.getmMainLeftFilterBaseDatas() != null && mHomeActivity.getmMainLeftFilterBaseDatas().size() > 0){
                                    if(mHomeActivity.getmMainLeftFilterBaseDatas().get(0).id.equalsIgnoreCase(mFilterBaseData.id)){
                                        selectedId = mHomeActivity.getmMainLeftFilterBaseDatas().get(0).id;
                                        selectedName = mHomeActivity.getmMainLeftFilterBaseDatas().get(0).name;
                                        mFilterBaseData.isSelectedPosition = true;
                                    } else{
                                        mFilterBaseData.isSelectedPosition = false;
                                    }

                                } else if((mHomeActivity.getmPrerferencedLeftFilterBaseDatas() != null && mHomeActivity.getmPrerferencedLeftFilterBaseDatas().size() > 0) && !isOnCLearClicked){
                                    if(mHomeActivity.getmPrerferencedLeftFilterBaseDatas().get(0).isDynamicFilter){
                                        if(mHomeActivity.getmPrerferencedLeftFilterBaseDatas().get(0).id.equalsIgnoreCase(mFilterBaseData.id)){
                                            selectedId = mHomeActivity.getmPrerferencedLeftFilterBaseDatas().get(0).id;
                                            selectedName = mHomeActivity.getmPrerferencedLeftFilterBaseDatas().get(0).name;
                                            mFilterBaseData.isSelectedPosition = true;
                                        } else{
                                            mFilterBaseData.isSelectedPosition = false;
                                        }
                                    } else{
                                        if(count == 0){
                                            selectedId = mFilterBaseData.id;
                                            selectedName = mFilterBaseData.name;
                                            mFilterBaseData.isSelectedPosition = true;
                                        } else{
                                            mFilterBaseData.isSelectedPosition = false;
                                        }
                                    }

                                } else{
                                    if(count == 0){
                                        selectedId = mFilterBaseData.id;
                                        selectedName = mFilterBaseData.name;
                                        mFilterBaseData.isSelectedPosition = true;

                                    } else{
                                        mFilterBaseData.isSelectedPosition = false;
                                    }
                                }

                                mFilterBaseData.isDynamicFilterSelected = true;
                                mFilterBaseData.isDynamicFilter = true;
                                count++;
                                mRightFilterBaseDatas.add(mFilterBaseData);
                            } else{
                                break;
                            }
                        }
                    }
                    setFilterdataListAdapter(mRightFilterBaseDatas, selectedId, selectedName);
                }
            } else{
                String filterTypeName = mLeftFilterBaseDatas.get(0).filterName;
                if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)))){
                    //Accesible Tenant Location
                    //mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                    getAccessibleTenantLocation(mAccessibleTenantLocationDataModel, selectedId, selectedName);

                } else if(filterTypeName.equalsIgnoreCase("CustomGoalFilter")){

                } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)))){
                    //Location Group
                    //mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                    getDashboardLocationGroupList(mDashboardLocationGroupListModel, selectedId, selectedName);

                } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product)))){
                    //mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                    getProductListFromLocationGroup(mProductListFromLocationGroupModel, selectedId, selectedName);

                } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user)))){
                    //User
                    //mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                    getDashboardUserList(mDashboardUserListModel, selectedId, selectedName);
                } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type)))){
                    //Metric Tpe
                    //mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                    getMetricTypeList(selectedId, selectedName);
                }
            }

        } else{

        }

    }

    public FilterBaseData getSelectedPreferenceData(String filterName){
        if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
            for(int i = 0; i < mHomeActivity.mPrerferencedLeftFilterBaseDatas.size(); i++){
                if(filterName.equalsIgnoreCase(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).filterName)){
                    return mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i);
                }
            }
        }
        return null;
    }

    public void checkPreferenceandgetLeftFilter(){
        mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModel;
        mDashboardLocationGroupListModel = mHomeActivity.mDashboardLocationGroupListModel;
        mProductListFromLocationGroupModel = mHomeActivity.mProductListFromLocationGroupModel;
        mDashboardUserListModel = mHomeActivity.mDashboardUserListModel;
        mLeftFilterBaseDatas.clear();
        boolean isNeedFillRegion = true;
        if(mHomeActivity.isRegionFilter()){
            for(int i = 0; i < mHomeActivity.mPrerferencedLeftFilterBaseDatas.size(); i++){
                if(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).isDynamicFilter){
                    mLeftFilterBaseDatas.add(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i));
                    isNeedFillRegion = false;
                } else{
                    if(isNeedFillRegion){
                        isNeedFillRegion = false;
                        if(integerListHashMap != null){
                            for(int k = 0; k < integerListHashMap.size(); k++){

                                List<DynamicData> mDynamicDatas = integerListHashMap.get(k);

                                if(mDynamicDatas.size() > 0){

                                    FilterBaseData mFilterBaseData = new FilterBaseData();

                                    for(int j = 0; j < mDynamicDatas.size(); j++){

                                        if(j == 0){

                                            mFilterBaseData.id = String.valueOf(mDynamicDatas.get(j).getId());

                                            mFilterBaseData.name = mDynamicDatas.get(j).getName();

                                            mFilterBaseData.regionTypeId = mDynamicDatas.get(j).getRegionTypeId();

                                            mFilterBaseData.filterName = mDynamicDatas.get(j).getRegionTypeName();
                                            mFilterBaseData.parentRegionId = mDynamicDatas.get(j).getParentRegionId();
                                            mFilterBaseData.isMasterLocation = mDynamicDatas.get(j).isMAsterLocation();
                                            if(k == 0){
                                                mFilterBaseData.isSelectedPosition = true;
                                                mFilterBaseData.isDynamicFilterSelected = true;
                                                mFilterBaseData.isSubRegionOpen = true;
                                            }
                                            mFilterBaseData.isDynamicFilter = true;
                                            mFilterBaseData.filterIndex = k;
                                            mLeftFilterBaseDatas.add(mFilterBaseData);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
        //// Start Tenant Location Check
        if(mHomeActivity.isTenantLocation()){
            FilterBaseData mFilterBaseDataToCheck = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)));
            if(mFilterBaseDataToCheck != null){
                if(checkTenantLocationData(mFilterBaseDataToCheck.id)){
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), Integer.parseInt(mFilterBaseDataToCheck.id));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mFilterBaseDataToCheck.name);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mFilterBaseDataToCheck.filterMetricType);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mFilterBaseDataToCheck.filterMetricType);

                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), -2));
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), "");
                    mFilterBaseData.filterMetricType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), "");
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    TenantLocationIdForDynamic = mFilterBaseData.id;
                    mLeftFilterBaseDatas.add(mFilterBaseData);
                } else{
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 1));
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), "");
                    mFilterBaseData.filterMetricType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), "");
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    TenantLocationIdForDynamic = mFilterBaseData.id;
                    mLeftFilterBaseDatas.add(mFilterBaseData);
                }
            } else{
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 1));
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), "");
                mFilterBaseData.filterMetricType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                TenantLocationIdForDynamic = mFilterBaseData.id;
                mLeftFilterBaseDatas.add(mFilterBaseData);
            }

        }
        //Complete Tenant Location Check

        //Start Location Group Check
        if(mHomeActivity.isLocationGroup()){
            FilterBaseData mFilterBaseDataToCheck = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)));
            if(mFilterBaseDataToCheck != null){
                if(checkLocationGroupData(mFilterBaseDataToCheck.id)){
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(mFilterBaseDataToCheck.id);
                    mFilterBaseData.name = mFilterBaseDataToCheck.name;
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    mLeftFilterBaseDatas.add(mFilterBaseData);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), Integer.parseInt(mFilterBaseDataToCheck.id));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mFilterBaseDataToCheck.name);
                } else{
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group), -2));
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name), "");
                    //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    LocationGroupIdDynamic = mFilterBaseData.id;
                    mLeftFilterBaseDatas.add(mFilterBaseData);

                }
            } else{
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group), -2));
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name), "");
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                LocationGroupIdDynamic = mFilterBaseData.id;
                mLeftFilterBaseDatas.add(mFilterBaseData);
            }

        }
        if(mHomeActivity.isProduct()){
            FilterBaseData mFilterBaseDataToCheck = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product)));
            if(mFilterBaseDataToCheck != null){
                if(checkProductData(mFilterBaseDataToCheck.id)){
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(mFilterBaseDataToCheck.id);
                    mFilterBaseData.name = mFilterBaseDataToCheck.name;
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mLeftFilterBaseDatas.add(mFilterBaseData);
                    try{
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);
                    } catch(Resources.NotFoundException e){
                        e.printStackTrace();
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                } else if(mHomeActivity.isCustomGoalFilter()){

                    if(mFilterBaseDataToCheck.id.equalsIgnoreCase(String.valueOf(-1))){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(-1);
                        mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                        mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                        mFilterBaseData.isSelectedPosition = false;
                        mFilterBaseData.isDynamicFilter = false;
                        mLeftFilterBaseDatas.add(mFilterBaseData);
                        try{
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);
                        } catch(Resources.NotFoundException e){
                            e.printStackTrace();
                        } catch(Exception e){
                            e.printStackTrace();
                        }
                    } else if(mFilterBaseDataToCheck.id.equalsIgnoreCase(String.valueOf(-2))){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(-1);
                        mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                        mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                        mFilterBaseData.isSelectedPosition = false;
                        mFilterBaseData.isDynamicFilter = false;
                        mLeftFilterBaseDatas.add(mFilterBaseData);
                        try{
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);
                        } catch(Resources.NotFoundException e){
                            e.printStackTrace();
                        } catch(Exception e){
                            e.printStackTrace();
                        }
                    }

                } else{
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product), 1));
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name), "");
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mLeftFilterBaseDatas.add(mFilterBaseData);
                }
            } else{
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product), 1));
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mLeftFilterBaseDatas.add(mFilterBaseData);
            }


        }
        if(mHomeActivity.isUser()){
            FilterBaseData mFilterBaseDataToCheck = getSelectedPreferenceData(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user)));
            if(mFilterBaseDataToCheck != null){
                if(checkuserData(mFilterBaseDataToCheck.id)){
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(mFilterBaseDataToCheck.id);
                    mFilterBaseData.name = mFilterBaseDataToCheck.name;
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;

                    if(mFilterBaseDataToCheck.id.length() > 0){
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), Integer.parseInt(mFilterBaseDataToCheck.id));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name), mFilterBaseDataToCheck.name);
                        mLeftFilterBaseDatas.add(mFilterBaseData);
                    } else{
                        mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = "" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user), -2);
                        mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                        mFilterBaseData.isSelectedPosition = false;
                        mFilterBaseData.isDynamicFilter = false;
                        mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                        mLeftFilterBaseDatas.add(mFilterBaseData);
                    }
                } else{
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = "" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user), -2);
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    mLeftFilterBaseDatas.add(mFilterBaseData);
                }
            } else{
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = "" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user), -2);
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                mLeftFilterBaseDatas.add(mFilterBaseData);
            }

        }

        setMetricTypeandLeftAdapter(0);
        fetchInitalRightListData();
        //  fetchInitalRightListData();
    }

    public boolean checkTenantLocationData(String id){
        mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModel;
        if(mAccessibleTenantLocationDataModel != null){
            for(int i = 0; i < mAccessibleTenantLocationDataModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mAccessibleTenantLocationDataModel.getData().get(i).getId()))){
                    return true;
                }
            }
        }

        return false;
    }

    public boolean checkLocationGroupData(String id){
        mDashboardLocationGroupListModel = mHomeActivity.mDashboardLocationGroupListModel;
        if(mDashboardLocationGroupListModel != null){
            for(int i = 0; i < mDashboardLocationGroupListModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mDashboardLocationGroupListModel.getData().get(i).getLocationGroupID()))){
                    return true;
                }
            }
        }

        return false;
    }

    public boolean checkProductData(String id){
        mProductListFromLocationGroupModel = mHomeActivity.mProductListFromLocationGroupModel;
        if(mProductListFromLocationGroupModel != null){
            for(int i = 0; i < mProductListFromLocationGroupModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mProductListFromLocationGroupModel.getData().get(i).getId()))){
                    return true;
                }
            }
        }


        return false;
    }

    public boolean checkuserData(String id){
        mDashboardUserListModel = mHomeActivity.mDashboardUserListModel;
        if(mDashboardUserListModel != null){
            for(int i = 0; i < mDashboardUserListModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mDashboardUserListModel.getData().get(i).getId()))){
                    return true;
                }
            }
        }

        return false;
    }

    public void fetchinitalDataForLeftFilter(){

        mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModel;
        mDashboardLocationGroupListModel = mHomeActivity.mDashboardLocationGroupListModel;
        mProductListFromLocationGroupModel = mHomeActivity.mProductListFromLocationGroupModel;
        mDashboardUserListModel = mHomeActivity.mDashboardUserListModel;


        mLeftFilterBaseDatas.clear();
        FilterBaseData mFilterBaseData = new FilterBaseData();
        if(mHomeActivity.isRegionFilter()){
            if(integerListHashMap != null){
                for(int k = 0; k < integerListHashMap.size(); k++){

                    List<DynamicData> mDynamicDatas = integerListHashMap.get(k);

                    if(mDynamicDatas.size() > 0){

                        mFilterBaseData = new FilterBaseData();

                        for(int j = 0; j < mDynamicDatas.size(); j++){

                            if(j == 0){

                                mFilterBaseData.id = String.valueOf(mDynamicDatas.get(j).getId());

                                mFilterBaseData.name = mDynamicDatas.get(j).getName();

                                mFilterBaseData.regionTypeId = mDynamicDatas.get(j).getRegionTypeId();

                                mFilterBaseData.filterName = mDynamicDatas.get(j).getRegionTypeName();
                                mFilterBaseData.parentRegionId = mDynamicDatas.get(j).getParentRegionId();
                                mFilterBaseData.isMasterLocation = mDynamicDatas.get(j).isMAsterLocation();

                                if(k == 0){
                                    mFilterBaseData.isSelectedPosition = true;
                                    mFilterBaseData.isDynamicFilterSelected = true;
                                    mFilterBaseData.isSubRegionOpen = true;
                                }
                                mFilterBaseData.isDynamicFilter = true;
                                mFilterBaseData.filterIndex = k;
                                mLeftFilterBaseDatas.add(mFilterBaseData);
                            }
                        }
                    }
                }
            }
        }
        if(mHomeActivity.isTenantLocation()){
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 1));
            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), "");
            //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
            mFilterBaseData.filterMetricType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), "");
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
            mFilterBaseData.isSelectedPosition = false;
            mFilterBaseData.isDynamicFilter = false;
            mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
            TenantLocationIdForDynamic = mFilterBaseData.id;
            mLeftFilterBaseDatas.add(mFilterBaseData);
        }
        if(mHomeActivity.isLocationGroup()){
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group), 1));
            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name), "");
            //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
            mFilterBaseData.isSelectedPosition = false;
            mFilterBaseData.isDynamicFilter = false;
            mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
            mLeftFilterBaseDatas.add(mFilterBaseData);
            LocationGroupIdDynamic = mFilterBaseData.id;
        }
        if(mHomeActivity.isProduct()){
            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product), 1));
            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name), "");
            //mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
            mFilterBaseData.isSelectedPosition = false;
            mFilterBaseData.isDynamicFilter = false;
            mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
            mLeftFilterBaseDatas.add(mFilterBaseData);
        }
        if(mHomeActivity.isUser()){
            try{
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = "" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user), 1);
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                mLeftFilterBaseDatas.add(mFilterBaseData);

            } catch(Exception e){
                e.printStackTrace();
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = "";
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                mLeftFilterBaseDatas.add(mFilterBaseData);

            }
        }
        //Pass the first level
        setMetricTypeandLeftAdapter(0);
    }


    public void setMetricTypeandLeftAdapter(final int level){
        List<String> mStringKeyListMetricType = new ArrayList<>();
        List<String> mStringListMetricType = new ArrayList<>();
        mStringKeyListMetricType = Arrays.asList(mHomeActivity.getResources().getStringArray(R.array.key_metric_type_array));
        mHomeActivity.mFilterMetricTypeModelList.clear();
        // mStringListMetricType.clear();
        for(int i = 0; i < mStringKeyListMetricType.size(); i++){

            mStringListMetricType.add(mHomeActivity.mobilekeyObject.optString(mStringKeyListMetricType.get(i)));
        }
        for(int i = 0; i < mStringListMetricType.size(); i++){
            FilterMetricTypeModel metricTypeModel = new FilterMetricTypeModel();
            metricTypeModel.id = "000" + String.valueOf(i);
            metricTypeModel.name = mStringListMetricType.get(i);
            metricTypeModel.filterName = mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_filter_metric_type));
            mHomeActivity.mFilterMetricTypeModelList.add(metricTypeModel);

        }
        if(!(mHomeActivity.mMainLeftFilterBaseDatas != null && mHomeActivity.mMainLeftFilterBaseDatas.size() > 0)){
            if(!mHomeActivity.isTenantLocation()){
                // InGaugeSession.write ( mContext.getResources ().getString ( R.string.key_selected_metric_type_name ), mHomeActivity.mFilterMetricTypeModelList.get ( 1 ).name );
            }
        }
        String metricTypeName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mHomeActivity.mFilterMetricTypeModelList.get(1).name);
        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
            if(metricTypeName.equalsIgnoreCase("Check out")){
                metricTypeName = "Arrival";
            } else if(metricTypeName.equalsIgnoreCase("Check in")){
                metricTypeName = "Departure";
            } else if(metricTypeName.equalsIgnoreCase("Daily")){
                metricTypeName = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
            }
        }
        for(FilterMetricTypeModel metricTypeModels : mHomeActivity.mFilterMetricTypeModelList){
            if(metricTypeModels.name.equals(metricTypeName)){
                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_id), metricTypeModels.id);
            }
        }

        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), metricTypeName);
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_id), mHomeActivity.mFilterMetricTypeModelList.get(1).id);
        String selctedMetricName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mHomeActivity.mFilterMetricTypeModelList.get(1).name);

        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
            if(selctedMetricName.equalsIgnoreCase("Arrival")){
                mFilterBaseData.name = "Check out";
            } else if(selctedMetricName.equalsIgnoreCase("Departure")){
                mFilterBaseData.name = "Check in";
            } else if(selctedMetricName.equalsIgnoreCase("Daily")){
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mHomeActivity.mFilterMetricTypeModelList.get(1).name);
            }
        } else{
            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mHomeActivity.mFilterMetricTypeModelList.get(1).name);
        }
        //mFilterBaseData.filterName = mContext.getResources().getString(R.string.metric_type);
        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
        mFilterBaseData.isSelectedPosition = false;
        mFilterBaseData.isDynamicFilter = false;
        mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
        mLeftFilterBaseDatas.add(mFilterBaseData);
        setLeftFilterdataListAdapter(mLeftFilterBaseDatas, level);
    }


    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.fragment_dynamic_dashboard_filter_tv_clear_all:
               /* try {
                   *//* mLeftFilterBaseDatas.clear();
                    //getRegionTypes(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));



                    integerListHashMap = mHomeActivity.getIntegerListHashMap();
                    fetchinitalDataForLeftFilter();
                    fetchInitalRightListData();


                    *//**//*mLeftFilterBaseDatas.clear();
                    listadapter.notifyDataSetChanged();
                    rlProgress.setVisibility(View.VISIBLE);
                    rvFilterList.setVisibility(View.GONE);
                    getRegionTypes(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1));*//*
                    mLeftFilterBaseDatas.clear();
                    mRightFilterBaseDatas.clear();
                    Gson gson = new Gson();
                    String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_filter_obj), null);
                    mHomeActivity.mMainLeftFilterBaseDatas = gson.fromJson(json, new TypeToken<ArrayList<FilterBaseData>>() {
                    }.getType());

                    if (mHomeActivity.getmMainLeftFilterBaseDatas() != null && mHomeActivity.getmMainLeftFilterBaseDatas().size() > 0) {
                        mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModel;
                        mDashboardLocationGroupListModel = mHomeActivity.mDashboardLocationGroupListModel;
                        mProductListFromLocationGroupModel = mHomeActivity.mProductListFromLocationGroupModel;
                        mDashboardUserListModel = mHomeActivity.mDashboardUserListModel;
                        integerListHashMap = mHomeActivity.getIntegerListHashMap();
                        mLeftFilterBaseDatas.clear();
                        mLeftFilterBaseDatas = mHomeActivity.getmMainLeftFilterBaseDatas();
                        //setLeftFilterdataListAdapter(mLeftFilterBaseDatas, 0);
                        mDynamicFilterLeftAdapter = new DynamicFilterLeftAdapter(mHomeActivity, mLeftFilterBaseDatas, this, true, 0);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                        rvFilterLeftList.setLayoutManager(mLayoutManager);
                        rvFilterLeftList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                        rvFilterLeftList.setItemAnimator(new DefaultItemAnimator());
                        rvFilterLeftList.setAdapter(mDynamicFilterLeftAdapter);
                        fetchInitalRightListData();

                    } else {
                        integerListHashMap = mHomeActivity.getIntegerListHashMap();
                        fetchinitalDataForLeftFilter();
                        fetchInitalRightListData();
                    }
        *//*integerListHashMap = mHomeActivity.getIntegerListHashMap();
        fetchinitalDataForLeftFilter();
        fetchInitalRightListData();*//*
                    selectedId = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_regiontype), 1));
                    selectedName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_regiontype_name), "");


                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                /*isFromFilledInitial = false;
                integerListHashMap = mHomeActivity.getIntegerListHashMap();
                fetchinitalDataForLeftFilter();
                fetchInitalRightListData();*/
                if(SystemClock.elapsedRealtime() - mLastClickTime < 2500){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                mHomeActivity.startProgress(mHomeActivity);
                isOnCLearClicked = true;
                filterCallAfterCheck();
                break;
            case R.id.fragment_dynamic_dashboard_filter_tv_apply:
                new ApplySync().execute();
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_title)));
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setTag("Back");
    }


    public int getIndexofMetricType(String filterType){
        int index = 0;
        for(FilterBaseData mFilterBaseData : mLeftFilterBaseDatas){
            index++;
            if(mFilterBaseData.filterName != null && mFilterBaseData.filterName.contains(filterType)){
                Logger.Error("Index OF " + index);
                return index;
                //break;
            }
            //something here
        }
        return -1;
    }

    private void GetRegionTypeService(RegionTypeDataModel regionTypedata, String selectedid, String selectedname){
        if(regionTypedata != null){
            int count = 0;
            for(RegionTypeDataModel.Datum datum : regionTypedata.getData()){
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(datum.getId());
                mFilterBaseData.name = datum.getName();
                if(count == 0){
                    mFilterBaseData.isSelectedPosition = true;
                } else{
                    mFilterBaseData.isSelectedPosition = false;
                }
                //mFilterBaseData.filterName = getString(R.string.region_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region_type));
                mFilterBaseData.filterIndex = 0;
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }

    private void getRegionByTenantRegionTypeByCountry(RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel, String selectedid, String selectedname){
        if(mRegionByTenantRegionTypeModel != null){
            int count = 0;
            for(RegionByTenantRegionTypeModel.Datum mDatum : mRegionByTenantRegionTypeModel.getData()){
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                if(count == 0){
                    mFilterBaseData.isSelectedPosition = true;
                } else{
                    mFilterBaseData.isSelectedPosition = false;
                }
                //mFilterBaseData.filterName = getString(R.string.region);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region));
                mFilterBaseData.filterIndex = 1;
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }

    }


    private void getAccessibleTenantLocation(AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel, String selectedid, String selectedname){
        if(mAccessibleTenantLocationDataModel != null){
            int count = 0;
            for(AccessibleTenantLocationDataModel.Datum mDatum : mAccessibleTenantLocationDataModel.getData()){
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterMetricType = mDatum.getHotelMetricsDataType();
                mFilterBaseData.isMasterLocation = mDatum.getMasterLocation();
                selectedId = mFilterBaseData.id;
                selectedName = mFilterBaseData.name;
                //mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                if(mLeftFilterBaseDatas.size() > 0){
                    for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                        if(mFilterBaseData.filterName.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                            mFilterBaseData.filterIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                            break;
                        } else{
                            mFilterBaseData.filterIndex = -1;
                        }
                    }
                }
                if(isFromFilledInitial && !isOnCLearClicked){
                    selectedid = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 1));
                    selectedname = InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), "");
                } else{
                    if(isOnCLearClicked){
                        //isOnCLearClicked=false;
                        if(count == 0){
                            selectedid = mFilterBaseData.id;
                            selectedname = mFilterBaseData.name;
                        }
                    }
                }
                if(mFilterBaseData.id.equalsIgnoreCase(selectedid)){
                    mFilterBaseData.isSelectedPosition = true;
                } else{
                    mFilterBaseData.isSelectedPosition = false;
                }
                /*if (count == 0) {

                } else {

                }*/
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }

    private void getDashboardLocationGroupList(DashboardLocationGroupListModel mGetAccessibleTenantLocationData, String selectedid, String selectedname){
        if(mGetAccessibleTenantLocationData != null){
            int count = 0;
            for(DashboardLocationGroupListModel.Datum mDatum : mGetAccessibleTenantLocationData.getData()){
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getLocationGroupID());
                mFilterBaseData.name = mDatum.getLocationGroupName();
                //mFilterBaseData.filterName = getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                if(mLeftFilterBaseDatas.size() > 0){
                    for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                        if(mFilterBaseData.filterName.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                            mFilterBaseData.filterIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                            break;
                        } else{
                            mFilterBaseData.filterIndex = -1;
                        }
                    }
                }
                //  selectedid= String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group), 1));
//                selectedname= InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name), "");
                if(mFilterBaseData.id.equalsIgnoreCase(selectedid)){
                    mFilterBaseData.isSelectedPosition = true;
                } else{
                    mFilterBaseData.isSelectedPosition = false;
                }
                /*if (count == 0) {
                    mFilterBaseData.isSelectedPosition = true;
                } else {
                    mFilterBaseData.isSelectedPosition = false;
                }*/
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }

    private void getProductListFromLocationGroup(ProductListFromLocationGroupModel mProductListFromLocationGroupModel, String selectedid, String selectedname){
        if(mProductListFromLocationGroupModel != null){
            int count = 0;
            int productFilterIndex = -1;
            List<FilterBaseData> mProductList = new ArrayList<>();
            List<FilterBaseData> mIncrementalARPD = new ArrayList<>();
            for(ProductListFromLocationGroupModel.Datum mDatum : mProductListFromLocationGroupModel.getData()){

                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getTenantProductName();
                if(mDatum.getIsMajor() != null){
                    mFilterBaseData.isMajor = mDatum.getIsMajor();
                } else{
                    mFilterBaseData.isMajor = false;
                }

                //mFilterBaseData.filterName = getString(R.string.product);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                if(mLeftFilterBaseDatas.size() > 0){
                    for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                        if(mFilterBaseData.filterName.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                            mFilterBaseData.filterIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                            productFilterIndex = mFilterBaseData.filterIndex;
                            break;
                        } else{
                            mFilterBaseData.filterIndex = -1;
                        }
                    }
                }
                if(mFilterBaseData.id.equalsIgnoreCase(selectedid)){
                    mFilterBaseData.isSelectedPosition = true;
                } else{
                    mFilterBaseData.isSelectedPosition = false;
                }
                count++;

                if(mHomeActivity.isCustomGoalFilter()){
                    if(!mFilterBaseData.isMajor){
                        mProductList.add(mFilterBaseData);
                    }
                } else{
                    mProductList.add(mFilterBaseData);
                }


            }
            if(mHomeActivity.isCustomGoalFilter()){
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(-1);
                mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                if(mLeftFilterBaseDatas.size() > 0){
                    for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                        if(mFilterBaseData.filterName.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                            mFilterBaseData.filterIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                            productFilterIndex = mFilterBaseData.filterIndex;
                            break;
                        } else{
                            mFilterBaseData.filterIndex = -1;
                        }
                    }
                }
                if(mFilterBaseData.id.equalsIgnoreCase(selectedid)){
                    mFilterBaseData.isSelectedPosition = true;
                } else{
                    mFilterBaseData.isSelectedPosition = false;
                }

                mIncrementalARPD.add(mFilterBaseData);
                if(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
                    mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(-2);
                    mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_goal_product_filter_upsell_product));
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                    if(mLeftFilterBaseDatas.size() > 0){
                        for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                            if(mFilterBaseData.filterName.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                                productFilterIndex = mFilterBaseData.filterIndex;
                                break;
                            } else{
                                mFilterBaseData.filterIndex = -1;
                            }
                        }
                    }
                    if(mFilterBaseData.id.equalsIgnoreCase(selectedid)){
                        mFilterBaseData.isSelectedPosition = true;
                    } else{
                        mFilterBaseData.isSelectedPosition = false;
                    }

                    mIncrementalARPD.add(mFilterBaseData);
                }

                mRightFilterBaseDatas.addAll(mIncrementalARPD);
                mRightFilterBaseDatas.addAll(mProductList);
            } else{
                mRightFilterBaseDatas.addAll(mProductList);
            }
//            selectedid= String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product), -2));
//            selectedname= InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name), "");
            setFilterdataListAdapterForProduct(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }

    private void getDashboardUserList(DashboardUserListModel mDashboardUserListModel, String selectedid, String selectedname){
        if(mDashboardUserListModel != null){
            int count = 0;
            if(mDashboardUserListModel.getData() != null && mDashboardUserListModel.getData().size() > 0){
                for(DashboardUserListModel.Datum mDatum : mDashboardUserListModel.getData()){
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(mDatum.getId());
                    mFilterBaseData.name = mDatum.getName();
                    //mFilterBaseData.filterName = getString(R.string.user);
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                    if(mLeftFilterBaseDatas.size() > 0){
                        for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                            if(mFilterBaseData.filterName.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                                break;
                            } else{
                                mFilterBaseData.filterIndex = -1;
                            }
                        }
                    }
                    if(mFilterBaseData.id.equalsIgnoreCase(selectedid)){
                        mFilterBaseData.isSelectedPosition = true;
                    } else{
                        mFilterBaseData.isSelectedPosition = false;
                    }
                    count++;
                    mRightFilterBaseDatas.add(mFilterBaseData);
                }
            }
//            selectedid= "" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user), 1);
//            selectedname= InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");

            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }


    public void getMetricTypeList(String selectedid, String selectedname){
        int count = 0;
        String savedMetricname = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), "" + 0000);
        String selectedMetricName = "";
        for(FilterMetricTypeModel filterMetricTypeModel : mHomeActivity.mFilterMetricTypeModelList){
            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.id = filterMetricTypeModel.id;

            mFilterBaseData.name = filterMetricTypeModel.name;
            mFilterBaseData.filterName = filterMetricTypeModel.filterName;
            if(mLeftFilterBaseDatas.size() > 0){
                for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                    if(mFilterBaseData.filterName.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                        mFilterBaseData.filterIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                        break;
                    } else{
                        mFilterBaseData.filterIndex = -1;
                    }
                }
            }
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                if(savedMetricname.equalsIgnoreCase("Arrival")){
                    selectedMetricName = "Check out";
                } else if(savedMetricname.equalsIgnoreCase("Departure")){
                    selectedMetricName = "Check in";
                } else if(savedMetricname.equalsIgnoreCase("Daily")){
                    selectedMetricName = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mHomeActivity.mFilterMetricTypeModelList.get(1).name);
                }
            } else{
                selectedMetricName = savedMetricname;
            }

            //mFilterBaseData.filterName = getString(R.string.metric_type);
            //InGaugeSession.write(mContext.getResources().getString(R.string.key_matric_data_type), mHomeActivity.mStringListMetricType.get(0));
            if(mFilterBaseData.name.equalsIgnoreCase(selectedMetricName)){
                mFilterBaseData.isSelectedPosition = true;
            } else{
                mFilterBaseData.isSelectedPosition = false;
            }
            /*if (count == 0) {
                mFilterBaseData.isSelectedPosition = true;
            } else {
                mFilterBaseData.isSelectedPosition = false;
            }*/
            count++;
            mRightFilterBaseDatas.add(mFilterBaseData);

        }
        for(FilterBaseData mFilterBaseData : mRightFilterBaseDatas){
            if(mFilterBaseData.name.equalsIgnoreCase(selectedname)){
                selectedid = mFilterBaseData.id;
            }
        }
        setFilterdataListAdapterForMetricType(mRightFilterBaseDatas, selectedid, selectedname);

    }

    private void setMertricTypeData(String selectedid, String selectedname){
        if(mHomeActivity.mStringListMetricType != null){
            int count = 0;
            for(String filterName : mHomeActivity.mStringListMetricType){

                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = "" + count;
                mFilterBaseData.name = filterName;
                //mFilterBaseData.filterName = getString(R.string.metric_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
                mFilterBaseData.filterIndex = 6;
                if(count == 0){
                    mFilterBaseData.isSelectedPosition = true;
                } else{
                    mFilterBaseData.isSelectedPosition = false;
                }
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }

    private void setFilterdataListAdapterForProduct(List<FilterBaseData> mFilterBaseDataList, String selectedid, String selectedname){
        if(mFilterBaseDataList != null){
            if(mFilterBaseDataList.size() > 0){
                int selectedPosiotn = 0;

                /*Collections.sort(mFilterBaseDataList, new Comparator<FilterBaseData>() {
                    @Override
                    public int compare(FilterBaseData mFilterBaseData1, FilterBaseData mFilterBaseData2) {
                        return Boolean.compare(mFilterBaseData2.isMasterLocation,mFilterBaseData1.isMasterLocation);
                    }
                });*/
                for(FilterBaseData mFilterBaseData : mFilterBaseDataList){
                    if(mFilterBaseData.isSelectedPosition){
                        selectedPosiotn = mFilterBaseDataList.indexOf(mFilterBaseData);
                        mFilterBaseDataList.remove(selectedPosiotn);
                        mFilterBaseDataList.add(0, mFilterBaseData);
                        break;
                    }
                }
                /*mFilterBaseDataList = setFirstSelectedPositionAfterMasterLocationSelected(mFilterBaseDataList);
                selectedid = mFilterBaseDataList.get(0).id;
                selectedname = mFilterBaseDataList.get(0).name;*/
                mFilterListAdapter = new FilterDetailListAdapter(mHomeActivity, mFilterBaseDataList, this, true, selectedid, selectedname);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                rvFilterList.setAdapter(mFilterListAdapter);
            }
        }
    }

    private void setFilterdataListAdapterForMetricType(List<FilterBaseData> mFilterBaseDataList, String selectedid, String selectedname){
        if(mFilterBaseDataList != null){
            if(mFilterBaseDataList.size() > 0){
                int selectedPosiotn = 0;
                /*Collections.sort(mFilterBaseDataList, new Comparator<FilterBaseData>() {
                    @Override
                    public int compare(FilterBaseData mFilterBaseData1, FilterBaseData mFilterBaseData2) {
                        return Boolean.compare(mFilterBaseData2.isMasterLocation,mFilterBaseData1.isMasterLocation);
                    }
                });*/
                for(FilterBaseData mFilterBaseData : mFilterBaseDataList){
                    if(mFilterBaseData.isSelectedPosition){
                        selectedPosiotn = mFilterBaseDataList.indexOf(mFilterBaseData);
                        mFilterBaseDataList.remove(selectedPosiotn);
                        mFilterBaseDataList.add(0, mFilterBaseData);

                        break;
                    }
                }

                /*mFilterBaseDataList = setFirstSelectedPositionAfterMasterLocationSelected(mFilterBaseDataList);

                selectedid = mFilterBaseDataList.get(0).id;
                selectedname = mFilterBaseDataList.get(0).name;*/
                mFilterListAdapter = new FilterDetailListAdapter(mHomeActivity, mFilterBaseDataList, this, true, selectedid, selectedname);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                rvFilterList.setAdapter(mFilterListAdapter);
            } else{
                mFilterListAdapter = new FilterDetailListAdapter(mHomeActivity, new ArrayList<FilterBaseData>(), this, true, selectedid, selectedname);
                rvFilterList.setAdapter(mFilterListAdapter);
            }
        }
    }

    private void setFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList, String selectedid, String selectedname){
        mHomeActivity.endProgress();
        List<FilterBaseData> mDataList = new ArrayList<>();
        int selectedPosiotn = 0;
        if(mFilterBaseDataList != null){
            if(mFilterBaseDataList.size() > 0){
                String product = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                String metricType = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
                String location = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                if(product.equalsIgnoreCase(mFilterBaseDataList.get(0).filterName) ||
                        metricType.equalsIgnoreCase(mFilterBaseDataList.get(0).filterName) || location.equalsIgnoreCase(mFilterBaseDataList.get(0).filterName)){
                    Logger.Error("<<<<  Filter List >>>>> " + mFilterBaseDataList.size());
                } else{
                    /*Collections.sort(mFilterBaseDataList, new Comparator<FilterBaseData>(){
                        public int compare(FilterBaseData v1, FilterBaseData v2){
                            return v1.name.compareTo(v2.name);
                        }
                    });*/
                }

                for(FilterBaseData mFilterBaseData : mFilterBaseDataList){
                    if(mFilterBaseData.isSelectedPosition){
                        selectedPosiotn = mFilterBaseDataList.indexOf(mFilterBaseData);
                        mFilterBaseDataList.remove(selectedPosiotn);
                        mFilterBaseDataList.add(0, mFilterBaseData);

                        break;
                    }
                }
                // mFilterBaseDataList = setFirstSelectedPositionAfterMasterLocationSelected(mFilterBaseDataList);

               /* selectedid = mFilterBaseDataList.get(0).id;
                selectedname = mFilterBaseDataList.get(0).name;*/
                mFilterListAdapter = new FilterDetailListAdapter(mHomeActivity, mFilterBaseDataList, this, true, selectedid, selectedname);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                rvFilterList.setAdapter(mFilterListAdapter);
                //updateAdapter(mFilterBaseDataList);

            } else{
                mFilterListAdapter = new FilterDetailListAdapter(mHomeActivity, new ArrayList<FilterBaseData>(), this, true, selectedid, selectedname);
                rvFilterList.setAdapter(mFilterListAdapter);
            }
        }
    }

    /* public List<FilterBaseData> setFirstSelectedPositionAfterMasterLocationSelected(List<FilterBaseData> mFilterBaseDataList){
         List<FilterBaseData> mFilterBaseDataListBase =mFilterBaseDataList;
         int selectedPosiotn = 0;
         for(FilterBaseData mFilterBaseData : mFilterBaseDataListBase){
             if(mFilterBaseData.isSelectedPosition){
                 selectedPosiotn = mFilterBaseDataListBase.indexOf(mFilterBaseData);
                 mFilterBaseDataListBase.remove(selectedPosiotn);
                 mFilterBaseDataListBase.add(0, mFilterBaseData);
                 break;
             }
         }
         FilterBaseData mUpdateFilterData = mFilterBaseDataListBase.get(0);
         mUpdateFilterData.isSelectedPosition=false;
         mFilterBaseDataListBase.set(0,mUpdateFilterData);
         Collections.sort(mFilterBaseDataListBase, new Comparator<FilterBaseData>() {
             @Override
             public int compare(FilterBaseData mFilterBaseData1, FilterBaseData mFilterBaseData2) {
                 return Boolean.compare(mFilterBaseData2.isMasterLocation,mFilterBaseData1.isMasterLocation);
             }
         });

         mUpdateFilterData = mFilterBaseDataListBase.get(0);
         mUpdateFilterData.isSelectedPosition=true;
         mFilterBaseDataListBase.set(0,mUpdateFilterData);
         return mFilterBaseDataListBase;
     }*/
    private void updateAdapter(List<FilterBaseData> mFilterBaseDataList){
        mRightFilterBaseDatas = new ArrayList<>();

        int selectedPosition;
        for(FilterBaseData mFilterBaseData : mFilterBaseDataList){
            if(mFilterBaseData.isSelectedPosition){
                selectedPosition = mFilterBaseDataList.indexOf(mFilterBaseData);
                mFilterBaseDataList.remove(selectedPosition);
                mFilterBaseDataList.add(0, mFilterBaseData);
                break;
            }
        }
        mRightFilterBaseDatas.addAll(mFilterBaseDataList);
        mFilterListAdapter.notifyDataSetChanged();
        for(int i = 0; i < mFilterBaseDataList.size(); i++){
            Logger.Error("#### Filter ID " + mFilterBaseDataList.get(i).id + " #### Filter Name  " + mFilterBaseDataList.get(i).name);
        }
    }

    private void setLeftFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList, int level){
        if(mFilterBaseDataList != null){
            if(mFilterBaseDataList.size() > 0){

                /*if (mDynamicFilterLeftAdapter != null) {
                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                } else {*/
                mDynamicFilterLeftAdapter = new DynamicFilterLeftAdapter(mHomeActivity, mFilterBaseDataList, this, true, level);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterLeftList.setLayoutManager(mLayoutManager);
                rvFilterLeftList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterLeftList.setItemAnimator(new DefaultItemAnimator());
                rvFilterLeftList.setAdapter(mDynamicFilterLeftAdapter);
                /*}*/

            }
        }
    }

    void getRightFilterData(String selectedId, String regionTypeId, String selectedname, String filterTypeName, boolean isDynamicFilter){
        mRightFilterBaseDatas.clear();
        Logger.Error("Map Array " + integerListHashMap);
        if(isDynamicFilter){
            for(int i = 0; i < integerListHashMap.size(); i++){
                int count = 0;
                List<DynamicData> mDynamicDatas = integerListHashMap.get(i);
                for(DynamicData mDynamicData : mDynamicDatas){

                    if(mDynamicData.getRegionTypeId() == Integer.parseInt(regionTypeId)){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mDynamicData.getId());

                        mFilterBaseData.name = mDynamicData.getName();
                        mFilterBaseData.regionTypeId = mDynamicData.getRegionTypeId();
                        setSelectedRegionId(mFilterBaseData.id);
                        setSelectedRegionName(mFilterBaseData.name);
                        setSelectedRegionTypeId(String.valueOf(mFilterBaseData.regionTypeId));
                        setSelectedRegionTypeName(String.valueOf(mFilterBaseData.filterName));
                        mFilterBaseData.filterMetricType = mDynamicData.getRegionTypeName();

                        //mFilterBaseData.filterName = getString(R.string.location);
                        mFilterBaseData.filterName = mDynamicData.getRegionTypeName();
                        mFilterBaseData.isMasterLocation = mDynamicData.isMAsterLocation();
                        mFilterBaseData.filterIndex = i;
                        if(count == 0){
                            mFilterBaseData.isSelectedPosition = true;
                        } else{
                            mFilterBaseData.isSelectedPosition = false;
                        }
                        mFilterBaseData.isDynamicFilter = true;
                        count++;
                        mRightFilterBaseDatas.add(mFilterBaseData);

                    } else{
                        break;
                    }
                }
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedId, selectedname);

        } else{
            if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region)))){

            } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)))){
                //Accesible Tenant Location
                //mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                if(mAccessibleTenantLocationDataModel == null){
                    mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModel;
                }
                getAccessibleTenantLocation(mAccessibleTenantLocationDataModel, selectedId, selectedname);

            } else if(filterTypeName.equalsIgnoreCase("CustomGoalFilter")){

            } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)))){
                //Location Group
                //mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getDashboardLocationGroupList(mDashboardLocationGroupListModel, selectedId, selectedname);

            } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product)))){
                //mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getProductListFromLocationGroup(mProductListFromLocationGroupModel, selectedId, selectedname);

            } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user)))){
                //User
                //mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getDashboardUserList(mDashboardUserListModel, selectedId, selectedname);
            } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type)))){
                //Metric Tpe
                //mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getMetricTypeList(selectedId, selectedname);
            }
        }
    }


    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterPosition, boolean isLeftFilter){
//        Logger.Error("<<< Size >>>" + mHomeActivity.getmMainLeftFilterBaseDatas().size());
        //      Toast.makeText(mHomeActivity,"<< Size >>"+  mHomeActivity.getmMainLeftFilterBaseDatas().size(),Toast.LENGTH_SHORT).show();
        if(isLeftFilter){
            if(mFilterBaseData.isDynamicFilter){
                if(position == -1){
                    fetchinitalDataForLeftFilter();
                    fetchInitalRightListData();

                } else{

                    mRightFilterBaseDatas.clear();
                    if(mFilterBaseData.mFilterBaseDatas != null && mFilterBaseData.mFilterBaseDatas.size() > 0){
                        mRightFilterBaseDatas.addAll(mFilterBaseData.mFilterBaseDatas);
                    } else{
                        List<FilterBaseData> mBaseDataList = getleftChildRegionList(String.valueOf(mFilterBaseData.parentRegionId), mFilterBaseData.id);
                        mRightFilterBaseDatas.addAll(mBaseDataList);
                    }

                    if(mRightFilterBaseDatas.size() > 0){
                        selectedId = mRightFilterBaseDatas.get(0).id;
                        selectedName = mRightFilterBaseDatas.get(0).name;
                    }
                    setFilterdataListAdapter(mRightFilterBaseDatas, mFilterBaseData.id, mFilterBaseData.name);
                }


            } else{
                getRightFilterData(mFilterBaseData.id, String.valueOf(mFilterBaseData.regionTypeId), mFilterBaseData.name, mFilterBaseData.filterName, mFilterBaseData.isDynamicFilter);
            }

            Logger.Error("<<< Left Click >>>" + position + "Filter Position " + filterPosition);
        } else{
            Logger.Error("<<< Right Click >>>" + position + "Filter Position " + filterPosition);
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), true);
            updateOnRightListClick(mFilterBaseData);
            //   setFilterdataListAdapter(mRightFilterBaseDatas, selectedId, selectedName);

        }
        //Clear query
        if(mSearchView != null){

            mSearchView.setQuery("", false);
            //Collapse the action view
            mSearchView.onActionViewCollapsed();

        }
        final InputMethodManager imm = (InputMethodManager) mHomeActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);


    }

    public void updateOnRightListClick(FilterBaseData mFilterBaseData){

        if(mFilterBaseData.isDynamicFilter){
            if(mFilterBaseData.isDynamicFilterSelected){

                mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                mDynamicFilterLeftAdapter.notifyDataSetChanged();
                int index = getIndexByProperty(mFilterBaseData.id);
                FilterBaseData mUpdateFilterData = mLeftFilterBaseDatas.get(index);
                mUpdateFilterData.isDynamicFilterSelected = true;
                mUpdateFilterData.isSubRegionOpen = true;
                mLeftFilterBaseDatas.set(index, mUpdateFilterData);
                mDynamicFilterLeftAdapter.notifyDataSetChanged();
                setSelectedRegionId(mFilterBaseData.id);
                setSelectedRegionName(mFilterBaseData.name);
                selectedId = mFilterBaseData.id;
                selectedName = mFilterBaseData.name;
                setSelectedRegionTypeId(String.valueOf(mFilterBaseData.regionTypeId));
                setSelectedRegionTypeName(String.valueOf(mFilterBaseData.filterName));
                setSelectedRegionId(mFilterBaseData.id);
                setSelectedRegionName(mFilterBaseData.name);
                index++;
                mUpdateFilterData = mLeftFilterBaseDatas.get(index);
                if(mUpdateFilterData.isDynamicFilter){
                    List<FilterBaseData> mBaseDataList = getleftChildRegionListFromRightClick(String.valueOf(mFilterBaseData.parentRegionId), mFilterBaseData.id);
                    if(mUpdateFilterData.mFilterBaseDatas != null && mUpdateFilterData.mFilterBaseDatas.size() > 0){
                        mUpdateFilterData.mFilterBaseDatas = new ArrayList<>();
                    }
                    mUpdateFilterData.mFilterBaseDatas.addAll(mBaseDataList);

                    if(mBaseDataList != null && mBaseDataList.size() > 0){
                        mUpdateFilterData = new FilterBaseData();
                        mUpdateFilterData.id = "";

                        mUpdateFilterData.name = "";
                        mUpdateFilterData.regionTypeId = mBaseDataList.get(0).regionTypeId;
                        mUpdateFilterData.parentRegionId = mBaseDataList.get(0).parentRegionId;

                        //mUpdateFilterData.filterMetricType = dynamicData.getRegionTypeName();
                        //mFilterBaseData.filterName = getString(R.string.location);
                        mUpdateFilterData.filterName = mBaseDataList.get(0).filterName;
                        mUpdateFilterData.filterIndex = index;
                        mUpdateFilterData.isSelectedPosition = false;
                        mUpdateFilterData.isDynamicFilter = true;
                        mUpdateFilterData.isDynamicFilterSelected = false;
                        mUpdateFilterData.isSubRegionOpen = true;

                    }
                    mLeftFilterBaseDatas.set(index, mUpdateFilterData);
                }

                if(mHomeActivity.isTenantLocation()){
                    int filterIndex = getFilterIndexFromFilterName(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)));
                    mHomeActivity.startProgress(mHomeActivity);
                    getAccessibleTenantLocationForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)),
                            "name",
                            "ASC",
                            mFilterBaseData.id,
                            "Normal", filterIndex, mFilterBaseData.level);
                    return;
                } else{
                    mHomeActivity.endProgress();
                    return;
                }
            }
        } else{
            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
            selectedId = mFilterBaseData.id;
            selectedName = mFilterBaseData.name;
            int index = getIndexByProperty(mFilterBaseData.id);
            /*setSelectedRegionIdForLb(String.valueOf(-2));
            setSelectedRegionName("");
            setSelectedRegionTypeId(String.valueOf(-2));
            setSelectedRegionTypeName("");*/


            if(mFilterBaseData.filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_filter_location)))){
                isNeedUpdateMetricType = true;
                metricType = mFilterBaseData.filterMetricType;
            }
            if(!mFilterBaseData.filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_filter_metric_type)))){
                index++;
            }


            FilterBaseData mUpdateFilterData = mLeftFilterBaseDatas.get(index);

            if(mUpdateFilterData != null){
                String filterName = mUpdateFilterData.filterName;

                if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)))){
                    //Accessible Tenant Location
//                        Thread.sleep(1000);
                    mHomeActivity.startProgress(mHomeActivity);
                    getAccessibleTenantLocationForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", mFilterBaseData.id, "Normal", mUpdateFilterData.filterIndex, mUpdateFilterData.level);
                    return;
                } else if(filterName.equalsIgnoreCase("CustomGoalFilter")){
//                        Thread.sleep(1000);
                    mHomeActivity.endProgress();
                } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)))){
                    //Location Group
//                        Thread.sleep(1000);
                    mHomeActivity.startProgress(mHomeActivity);
                    TenantLocationIdForDynamic = getFilterValueIdFromFilterName(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)));
                    getDashboardLocationGroupListForFilter(
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            TenantLocationIdForDynamic, String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), mUpdateFilterData.filterIndex, mUpdateFilterData.level);

                    return;
                } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product)))){
                    //Product
//                        Thread.sleep(1000);
                    mHomeActivity.startProgress(mHomeActivity);
                    TenantLocationIdForDynamic = getFilterValueIdFromFilterName(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)));
                    LocationGroupIdDynamic = getFilterValueIdFromFilterName(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)));
                    getProductListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            LocationGroupIdDynamic,
                            TenantLocationIdForDynamic,
                            mUpdateFilterData.filterIndex, mUpdateFilterData.level);
                    return;
                } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user)))){
                    //User
//                        Thread.sleep(1000);
                    mHomeActivity.startProgress(mHomeActivity);
                    TenantLocationIdForDynamic = getFilterValueIdFromFilterName(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)));
                    LocationGroupIdDynamic = getFilterValueIdFromFilterName(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)));
                    getDashboardUserListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            TenantLocationIdForDynamic,
                            "2016-08-01", "2016-08-31",
                            LocationGroupIdDynamic,
                            mUpdateFilterData.filterIndex,
                            mUpdateFilterData.level);

                    return;

                } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type)))){
                    /*String hotelMetricTypeName = mLeftFilterBaseDatas.get(2).filterMetricType;
                    Logger.Error("Hotel Metric Type Name" + hotelMetricTypeName);
                    mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = "" + 000000;
                    mFilterBaseData.filterIndex = 6;
                    if (!TextUtils.isEmpty(hotelMetricTypeName)) {
                        if (hotelMetricTypeName.equalsIgnoreCase("Arrival")) {
                            mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);

                        } else if (hotelMetricTypeName.equalsIgnoreCase("Departure")) {
                            mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(1);
                        } else if (hotelMetricTypeName.equalsIgnoreCase("Daily")) {
                            mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(2);
                        }
                    } else {
                        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);
                    }
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
                    mLeftFilterBaseDatas.set(mLeftFilterBaseDatas.size() - 1, mFilterBaseData);
*/
                    /*Arrival
                    Daily
                    Departure*/
                    String arrival = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_ARRIVAL");
                    String daily = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
                    String departure = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DEPARTURE");


                    if((selectedName.equalsIgnoreCase(arrival) || selectedName.equalsIgnoreCase(daily) || selectedName.equalsIgnoreCase(departure))){
                        if(selectedName.equalsIgnoreCase(arrival)){
                            mFilterBaseData.id = "" + 0000;

                        } else if(selectedName.equalsIgnoreCase(departure)){
                            mFilterBaseData.id = "" + 0001;

                        } else if(selectedName.equalsIgnoreCase(daily)){
                            mFilterBaseData.id = "" + 0002;
                        }

                        mFilterBaseData.name = selectedName;
                        mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size() - 1;
//                                mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
                        mLeftFilterBaseDatas.set(mLeftFilterBaseDatas.size() - 1, mFilterBaseData);
                    }
                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                    updateMetricType(metricType);
                    mHomeActivity.endProgress();
                } else{
                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                    updateMetricType(metricType);
                    mHomeActivity.endProgress();
                    return;
                }
                mHomeActivity.endProgress();
            }

        }
    }


    public List<FilterBaseData> getleftChildRegionListFromRightClick(String id, String filterSelectedId){

        List<FilterBaseData> mFilterBaseDataList = new ArrayList<>();
        if(integerListHashMap != null && integerListHashMap.size() > 0){
            {

                for(int i = 0; i < integerListHashMap.size(); i++){
                    List<DynamicData> mDynamicDatas = integerListHashMap.get(i);
                    for(int k = 0; k < mDynamicDatas.size(); k++){
                        DynamicData dynamicData = mDynamicDatas.get(k);
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        if(filterSelectedId.equalsIgnoreCase(String.valueOf(dynamicData.getParentRegionId()))){
                            mFilterBaseData.id = String.valueOf(dynamicData.getId());

                            mFilterBaseData.name = dynamicData.getName();
                            mFilterBaseData.regionTypeId = dynamicData.getRegionTypeId();
                            mFilterBaseData.parentRegionId = dynamicData.getParentRegionId();

                            mFilterBaseData.filterMetricType = dynamicData.getRegionTypeName();
                            //mFilterBaseData.filterName = getString(R.string.location);
                            mFilterBaseData.filterName = dynamicData.getRegionTypeName();
                            mFilterBaseData.filterIndex = i;
                            if(k == 0){
                                mFilterBaseData.isSelectedPosition = true;
                            } else{
                                mFilterBaseData.isSelectedPosition = false;
                            }

                            mFilterBaseData.isDynamicFilter = true;
                            mFilterBaseData.isDynamicFilterSelected = true;
                            mFilterBaseDataList.add(mFilterBaseData);
                        }

                    }

                }
            }

        }

        return mFilterBaseDataList;
    }

    public List<FilterBaseData> getleftChildRegionList(String id, String filterSelectedId){

        List<FilterBaseData> mFilterBaseDataList = new ArrayList<>();
        if(integerListHashMap != null && integerListHashMap.size() > 0){
            {

                for(int i = 0; i < integerListHashMap.size(); i++){
                    List<DynamicData> mDynamicDatas = integerListHashMap.get(i);
                    for(int k = 0; k < mDynamicDatas.size(); k++){
                        DynamicData dynamicData = mDynamicDatas.get(k);
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        if(id.equalsIgnoreCase(String.valueOf(dynamicData.getParentRegionId()))){
                            mFilterBaseData.id = String.valueOf(dynamicData.getId());

                            mFilterBaseData.name = dynamicData.getName();
                            mFilterBaseData.regionTypeId = dynamicData.getRegionTypeId();
                            mFilterBaseData.parentRegionId = dynamicData.getParentRegionId();

                            mFilterBaseData.filterMetricType = dynamicData.getRegionTypeName();
                            //mFilterBaseData.filterName = getString(R.string.location);
                            mFilterBaseData.filterName = dynamicData.getRegionTypeName();
                            mFilterBaseData.isMasterLocation = dynamicData.isMAsterLocation();
                            mFilterBaseData.filterIndex = i;
                            if(filterSelectedId.equalsIgnoreCase(mFilterBaseData.id)){
                                mFilterBaseData.isSelectedPosition = true;
                            } else{
                                mFilterBaseData.isSelectedPosition = false;
                            }

                            mFilterBaseData.isDynamicFilter = true;
                            mFilterBaseData.isDynamicFilterSelected = true;
                            mFilterBaseDataList.add(mFilterBaseData);
                        }

                    }

                }
            }

        }

        return mFilterBaseDataList;
    }


    public String getFilterValueIdFromFilterName(String filtername){
        for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
            if(mLeftFilterBaseDatas.get(i) != null && mLeftFilterBaseDatas.get(i).filterName.equals(filtername)){
                return mLeftFilterBaseDatas.get(i).id;
            }
        }
        return "" + -1;
    }

    public int getFilterIndexFromFilterName(String filtername){
        for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
            if(mLeftFilterBaseDatas.get(i) != null && mLeftFilterBaseDatas.get(i).filterName.equals(filtername)){
                return mLeftFilterBaseDatas.get(i).filterIndex;
            }
        }
        return -1;
    }


    private int getIndexByProperty(String selected){
        for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
            if(mLeftFilterBaseDatas.get(i) != null && mLeftFilterBaseDatas.get(i).id.equals(selected)){
                return i;
            }
        }
        return 0;// not there is list
    }


    public void updateAfterThisIndex(FilterBaseData mFilterBaseData, int level){
        int nextFilterIndex = mFilterBaseData.filterIndex;

        while(mLeftFilterBaseDatas.size() - 1 > nextFilterIndex){
            nextFilterIndex = nextFilterIndex + 1;

            try{
                if(!mLeftFilterBaseDatas.get(nextFilterIndex).isDynamicFilter){
                    String filterName = mLeftFilterBaseDatas.get(nextFilterIndex).filterName;
                    Logger.Error("<<< Filter Name >>>" + filterName);
                    if(!filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type)))){
                        mHomeActivity.startProgress(mHomeActivity);
                    }

                    if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)))){
                        //Accessible Tenant Location
//                        Thread.sleep(1000);
                        getAccessibleTenantLocationForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", mFilterBaseData.id, "Normal", mLeftFilterBaseDatas.get(nextFilterIndex).filterIndex, level);
                        break;
                    } else if(filterName.equalsIgnoreCase("CustomGoalFilter")){
//                        Thread.sleep(1000);
                        mHomeActivity.endProgress();
                    } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)))){
                        //Location Group
//                        Thread.sleep(1000);

                        getDashboardLocationGroupListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), TenantLocationIdForDynamic, String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), mLeftFilterBaseDatas.get(nextFilterIndex).filterIndex, level);

                        break;
                    } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product)))){
                        //Product
//                        Thread.sleep(1000);
                        getProductListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                LocationGroupIdDynamic,
                                TenantLocationIdForDynamic,
                                mLeftFilterBaseDatas.get(nextFilterIndex).filterIndex, level);
                        break;
                    } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user)))){
                        //User
//                        Thread.sleep(1000);

                        getDashboardUserListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), TenantLocationIdForDynamic, "2016-08-01", "2016-08-31", LocationGroupIdDynamic, mLeftFilterBaseDatas.get(nextFilterIndex).filterIndex, level);

                        break;

                    } else{
                        mHomeActivity.endProgress();
                        mHomeActivity.endProgress();
                        mHomeActivity.endProgress();
                        mHomeActivity.endProgress();
                        mHomeActivity.endProgress();
                        break;
                    }
                    mHomeActivity.endProgress();

                }
            } catch(Exception e){
                e.printStackTrace();
            }

        }
        mDynamicFilterLeftAdapter.notifyDataSetChanged();

    }

    public void updateFilterData(FilterBaseData mFilterBaseData, int clickedPosition, int dynamicIndex){

        //  isLoaded = true;
        InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), true);
        //mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
        int index = mFilterBaseData.filterIndex + 1;


        switch(index){
            case 0:
                mHomeActivity.startProgress(mHomeActivity);
                //  getRegionTypesForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), clickedPosition, InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1));
                break;
            case 1:
                mHomeActivity.startProgress(mHomeActivity);
                //getRegionByTenantRegionTypeByCountryForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1), String.valueOf(mHomeActivity.mRegionTypeDataModel.getData().get(clickedPosition).getId()));
                break;
            case 2:
                mHomeActivity.startProgress(mHomeActivity);
                Logger.Error(" Filter  " + mFilterBaseData.id);
                //Logger.Error(" Selected ID For Region Model " + mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(clickedPosition).getId());
                /*getAccessibleTenantLocationForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", mFilterBaseData.id, "Normal", -1);*/
                break;
            case 3:
                mHomeActivity.startProgress(mHomeActivity);
                //getDashboardLocationGroupListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), mFilterBaseData.id, String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), -1);
                break;
            case 4:
                mHomeActivity.startProgress(mHomeActivity);
                //getProductListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), mFilterBaseData.id, mLeftFilterBaseDatas.get(2).id, -1);
                break;
            case 5:
                mHomeActivity.startProgress(mHomeActivity);
                //getDashboardUserListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), mLeftFilterBaseDatas.get(2).id, "2016-08-01", "2016-08-31", mLeftFilterBaseDatas.get(3).id, -1);
                break;
            case 6:
                break;
            case 7:
                InGaugeSession.write(mContext.getResources().getString(R.string.key_matric_data_type), mFilterBaseData.name);
                //setLeftFilterdataListAdapter(mLeftFilterBaseDatas);
                break;
        }
    }

    private void VoiUpdateFilterData(int filterindex){
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "";
        mFilterBaseData.name = "";
        mFilterBaseData.filterIndex = filterindex;
        //mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
        //mDynamicFilterLeftAdapter.notifyDataSetChanged();
        /*switch (filterindex) {
            case 0:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.region_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region_type));
                mRegionTypeDataModel = null;
                *//*Gson gson = new Gson();
                String json = gson.toJson(mHomeActivity.mRegionTypeDataModel);
                InGaugeSession.write(mContext.getString(R.string.key_region_type_obj), json);*//*
                break;
            case 1:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.region);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region));
                mRegionByTenantRegionTypeModel = null;
                *//*gson = new Gson();
                json = gson.toJson(mHomeActivity.mRegionByTenantRegionTypeModel);
                InGaugeSession.write(mContext.getString(R.string.key_region_obj), json);*//*
                break;
            case 2:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                mAccessibleTenantLocationDataModel = null;
                *//*gson = new Gson();
                json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
                InGaugeSession.write(mContext.getString(R.string.key_location_obj), json);*//*
                break;
            case 3:
                mDashboardLocationGroupListModel = null;
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                *//*gson = new Gson();
                json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
                InGaugeSession.write(mContext.getString(R.string.key_location_group_obj), json);*//*
                break;
            case 4:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                mProductListFromLocationGroupModel = null;
                *//*gson = new Gson();
                json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModel);
                InGaugeSession.write(mContext.getString(R.string.key_product_obj), json);*//*
                break;
            case 5:
                // mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                mDashboardUserListModel = null;
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                *//*gson = new Gson();
                json = gson.toJson(mHomeActivity.mDashboardUserListModel);
                InGaugeSession.write(mContext.getString(R.string.key_user_obj), json);*//*
                break;
        }*/
       /* if (mDynamicFilterLeftAdapter != null) {
            if (filterindex >= mLeftFilterBaseDatas.size()) {
                mLeftFilterBaseDatas.add(mFilterBaseData);
            } else {
                mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
            }

            mDynamicFilterLeftAdapter.notifyDataSetChanged();
        } else {
            if (mDynamicFilterLeftAdapter != null) {
                mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                mDynamicFilterLeftAdapter.notifyDataSetChanged();

            } else {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                mLeftFilterBaseDatas.add(mFilterBaseData.filterIndex, mFilterBaseData);
                rvFilterLeftList.setAdapter(mDynamicFilterLeftAdapter);

            }
        }*/

    }


    /**
     * @param authToken
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param regionId
     * @param activeStatus
     */
    void getAccessibleTenantLocationForFilter(final String authToken, int tenantId, String userId, String orderBy, String sort, String regionId, String activeStatus, final int dynamicIndex, final int levels){
        try{

            mCall = apiInterface.getAccessibleTenantLocation(tenantId, userId, orderBy, sort, regionId, activeStatus, true);
            mCall.enqueue(new Callback(){
                @Override
                public void onResponse(Call call, Response response){
                    mHomeActivity.endProgress();
                    if(response.body() != null){

                        mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) response.body();
                        Collections.sort(mAccessibleTenantLocationDataModel.getData(), new Comparator<AccessibleTenantLocationDataModel.Datum>(){
                            @Override
                            public int compare(AccessibleTenantLocationDataModel.Datum d1, AccessibleTenantLocationDataModel.Datum d2){
                                return Boolean.compare(d2.getMasterLocation(), d1.getMasterLocation());
                            }
                        });

                        Gson gson = new Gson();
                        Type type = new TypeToken<AccessibleTenantLocationDataModel>(){
                        }.getType();
                        String json = gson.toJson(mAccessibleTenantLocationDataModel, type);


                        Logger.Error("<<< Response : >>> " + json);

                        /*mHomeActivity.mAccessibleTenantLocationDataModel = null;
                        mHomeActivity.mAccessibleTenantLocationDataModel = new AccessibleTenantLocationDataModel();
                        mHomeActivity.mAccessibleTenantLocationDataModel = mAccessibleTenantLocationDataModel;
                        Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
                        InGaugeSession.write(mContext.getString(R.string.key_location_obj), json);*/
                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if(mAccessibleTenantLocationDataModel.getData() != null && mAccessibleTenantLocationDataModel.getData().size() > 0){
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId());
                            mFilterBaseData.name = mAccessibleTenantLocationDataModel.getData().get(0).getName();
                            mFilterBaseData.filterIndex = dynamicIndex;
//                            mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                            mFilterBaseData.filterMetricType = mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType();
                            mFilterBaseData.isMasterLocation = mAccessibleTenantLocationDataModel.getData().get(0).getMasterLocation();
                            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            //mLeftFilterBaseDatas.add(mFilterBaseData);
                            metricType = mFilterBaseData.filterMetricType;
                            isNeedUpdateMetricType = true;
                            if(mLeftFilterBaseDatas.size() > 0){
                                int innerdynamicIndex = -1;
                                String locationGroup = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                                for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){

                                    if(locationGroup.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                        innerdynamicIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                                        break;
                                    }
                                }
                                TenantLocationIdForDynamic = mFilterBaseData.id;
                                if(mHomeActivity.isLocationGroup()){

                                    getDashboardLocationGroupListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), TenantLocationIdForDynamic, String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), innerdynamicIndex, levels);
                                } else if(mHomeActivity.isProduct()){
                                    if(mLeftFilterBaseDatas.size() > 0){
                                        int innerdynamicIndexForProduct = -1;
                                        String Product = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                                        for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){

                                            if(Product.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                                innerdynamicIndexForProduct = mLeftFilterBaseDatas.get(i).filterIndex;
                                                break;
                                            }
                                        }
                                        getProductListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                LocationGroupIdDynamic,
                                                TenantLocationIdForDynamic,
                                                innerdynamicIndexForProduct, levels);
                                    }
                                } else if(mHomeActivity.isUser()){
                                    if(mLeftFilterBaseDatas.size() > 0){
                                        int innerdynamicIndexUser = -1;
                                        String user = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                        for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                                            if(user.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                                innerdynamicIndexUser = mLeftFilterBaseDatas.get(i).filterIndex;
                                                break;
                                            }
                                        }


                                        getDashboardUserListForFilter(InGaugeSession.read(mContext.getResources().
                                                        getString(R.string.key_auth_token), ""),
                                                TenantLocationIdForDynamic, "2016-08-01", "2016-08-31",
                                                LocationGroupIdDynamic,
                                                innerdynamicIndexUser, levels);
                                    }
                                } else{
                                    mHomeActivity.endProgress();
                                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                                    updateMetricType(metricType);
                                }
                            }


                        } else{
                            VoiUpdateFilterData(dynamicIndex);
                            // rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t){
                    Logger.Error("API Exception " + t.getMessage());
                    mHomeActivity.endProgress();
                }
            });
        } catch(Exception e){
            e.printStackTrace();
            mHomeActivity.endProgress();
        }
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupListForFilter(final String authToken, final String tenantlocationId, String userId, final int dynamicIndex, final int levels){

        try{
            mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
            mCall.enqueue(new Callback(){
                @Override
                public void onResponse(Call call, Response response){

                    if(response.body() != null){
                        mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) response.body();
                        /*mHomeActivity.mDashboardLocationGroupListModel = null;
                        mHomeActivity.mDashboardLocationGroupListModel = new DashboardLocationGroupListModel();
                        mHomeActivity.mDashboardLocationGroupListModel = mDashboardLocationGroupListModel;
                        Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
                        InGaugeSession.write(mContext.getString(R.string.key_location_group_obj), json);*/
                        //String authToken,String locationGroupId

                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if(mDashboardLocationGroupListModel != null && mDashboardLocationGroupListModel.getData().size() > 0){
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID());
                            mFilterBaseData.name = mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName();
                            mFilterBaseData.filterIndex = dynamicIndex;
                            //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            LocationGroupIdDynamic = mFilterBaseData.id;
                            //mLeftFilterBaseDatas.add(mFilterBaseData);


                            if(mLeftFilterBaseDatas.size() > 0){
                                int innerdynamicIndex = -1;
                                String Product = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                                for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){

                                    if(Product.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                        innerdynamicIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                                        break;
                                    }
                                }

                                if(mHomeActivity.isProduct()){

                                    getProductListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                            LocationGroupIdDynamic,
                                            TenantLocationIdForDynamic,
                                            innerdynamicIndex, levels);
                                } else if(mHomeActivity.isUser()){
                                    if(mLeftFilterBaseDatas.size() > 0){
                                        int innerdynamicIndexforUser = -1;
                                        String user = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                        for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                                            if(user.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                                innerdynamicIndexforUser = mLeftFilterBaseDatas.get(i).filterIndex;
                                                break;
                                            }
                                        }
                                        getDashboardUserListForFilter(InGaugeSession.read(mContext.getResources().
                                                        getString(R.string.key_auth_token), ""),
                                                TenantLocationIdForDynamic, "2016-08-01", "2016-08-31",
                                                LocationGroupIdDynamic,
                                                innerdynamicIndexforUser, levels);
                                    }
                                } else{
                                    mHomeActivity.endProgress();
                                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                                    updateMetricType(metricType);
                                }
                            }
                        } else{
                            VoiUpdateFilterData(dynamicIndex);
                            mHomeActivity.endProgress();
                        }


                    }
                }

                @Override
                public void onFailure(Call call, Throwable t){

                    Logger.Error("API Exception " + t.getMessage());
                    mHomeActivity.endProgress();


                }
            });
        } catch(Exception e){
            e.printStackTrace();

        }
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductListForFilter(final String authToken, final String locationGroupId, final String tenantLocationID, final int dynamicIndex, final int level){

        try{
            mCall = apiInterface.getProductList(locationGroupId);
            mCall.enqueue(new Callback(){
                @Override
                public void onResponse(Call call, Response response){
                    List<FilterBaseData> mProductList = new ArrayList<>();
                    List<FilterBaseData> mIncrementalARPD = new ArrayList<>();
                    if(response.body() != null){
                        mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) response.body();
                        /*mHomeActivity.mProductListFromLocationGroupModel = null;
                        mHomeActivity.mProductListFromLocationGroupModel = mProductListFromLocationGroupModel;
                        Gson gson = new Gson();
                        String json = gson.toJson(mProductListFromLocationGroupModel);
                        InGaugeSession.write(mContext.getString(R.string.key_product_obj), json);*/

                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {

                        if(mHomeActivity.isCustomGoalFilter()){
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(-1);
                            mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
                            mFilterBaseData.filterName = getString(R.string.product);
                            mFilterBaseData.filterIndex = dynamicIndex;
                            mIncrementalARPD.add(mFilterBaseData);
                            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
                                mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(-2);
                                mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_goal_product_filter_upsell_product));
                                mFilterBaseData.filterName = getString(R.string.product);
                                mFilterBaseData.filterIndex = dynamicIndex;
                                mIncrementalARPD.add(mFilterBaseData);
                            }
                            mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mIncrementalARPD.get(0).id);
                            mFilterBaseData.name = mIncrementalARPD.get(0).name;
                            mFilterBaseData.filterIndex = dynamicIndex;
//                            mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                            //  mLeftFilterBaseDatas.add(mFilterBaseData);
                            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            if(mLeftFilterBaseDatas.size() > 0){
                                int innerdynamicIndex = -1;
                                String user = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                                    if(user.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                        innerdynamicIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                                        break;
                                    }
                                }
                                if(mHomeActivity.isUser()){
                                    getDashboardUserListForFilter(InGaugeSession.read(mContext.getResources().
                                                    getString(R.string.key_auth_token), ""),
                                            TenantLocationIdForDynamic, "2016-08-01", "2016-08-31",
                                            LocationGroupIdDynamic,
                                            innerdynamicIndex, level);
                                } else{
                                    mHomeActivity.endProgress();
                                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                                    updateMetricType(metricType);
                                }
                            }

                        } else if(mProductListFromLocationGroupModel.getData() != null && mProductListFromLocationGroupModel.getData().size() > 0){
                            for(int i = 0; i < mProductListFromLocationGroupModel.getData().size(); i++){

                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                if(!mProductListFromLocationGroupModel.getData().get(i).getIsMajor()){
                                    mFilterBaseData.id = String.valueOf(mProductListFromLocationGroupModel.getData().get(i).getId());
                                    mFilterBaseData.name = mProductListFromLocationGroupModel.getData().get(i).getTenantProductName();
                                    mFilterBaseData.filterIndex = dynamicIndex;
//                            mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                                    mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                                    break;
                                    //  mLeftFilterBaseDatas.add(mFilterBaseData);
                                } else{
                                    mFilterBaseData.id = String.valueOf(-2);
                                    mFilterBaseData.name = "";
                                    mFilterBaseData.filterIndex = dynamicIndex;
//                            mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                                    mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                                    //break;
                                }


                            }

                            if(mLeftFilterBaseDatas.size() > 0){
                                int innerdynamicIndex = -1;
                                String user = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                                    if(user.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                                        innerdynamicIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                                        break;
                                    }
                                }
                                if(mHomeActivity.isUser()){
                                    getDashboardUserListForFilter(InGaugeSession.read(mContext.getResources().
                                                    getString(R.string.key_auth_token), ""),
                                            TenantLocationIdForDynamic, "2016-08-01", "2016-08-31",
                                            LocationGroupIdDynamic,
                                            innerdynamicIndex, level);
                                } else{
                                    mHomeActivity.endProgress();
                                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                                    updateMetricType(metricType);
                                }
                            }

                            //   }
                        } else{
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = "";
                            mFilterBaseData.name = "";
                            mFilterBaseData.filterIndex = dynamicIndex;
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            mDynamicFilterLeftAdapter.notifyDataSetChanged();
                            //VoiUpdateFilterData(dynamicIndex);
                            //  rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                            updateMetricType(metricType);
                        }
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t){
                    Logger.Error("API Exception " + t.getMessage());
                    mHomeActivity.endProgress();

                }
            });
        } catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param tenantLocationFromIdFromFilter
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserListForFilter(String authToken, String tenantLocationFromIdFromFilter, String startDate, String endDate, String locationGroupId, final int dynamicIndex, final int levels){
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        try{
            mCall = apiInterface.getDashboardUserListForDashboard(TenantId, true, false, false, tenantLocationFromIdFromFilter, locationGroupId, true);
            mCall.enqueue(new Callback(){
                @Override
                public void onResponse(Call call, Response response){
                    mHomeActivity.endProgress();
                    if(response.body() != null){
                        mDashboardUserListModel = (DashboardUserListModel) response.body();
                        /*mHomeActivity.mDashboardUserListModel = null;
                        mHomeActivity.mDashboardUserListModel = new DashboardUserListModel();
                        mHomeActivity.mDashboardUserListModel = mDashboardUserListModel;
                        Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mDashboardUserListModel);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_user_obj), json);*/
                        if(mDashboardUserListModel != null){

                            if(mDashboardUserListModel.getData() != null && mDashboardUserListModel.getData().size() > 0){
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(mDashboardUserListModel.getData().get(0).getId());
                                mFilterBaseData.name = mDashboardUserListModel.getData().get(0).getName();
                                mFilterBaseData.filterIndex = dynamicIndex;
//                                mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                                //rlProgress.setVisibility(View.GONE);
                                mDynamicFilterLeftAdapter.notifyDataSetChanged();
                                updateMetricType(metricType);
                            } else{

                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = "";
                                mFilterBaseData.name = "";
                                mFilterBaseData.filterIndex = dynamicIndex;
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                                mDynamicFilterLeftAdapter.notifyDataSetChanged();
                                //VoiUpdateFilterData(dynamicIndex);
                                //  rlProgress.setVisibility(View.GONE);
                                mHomeActivity.endProgress();
                                updateMetricType(metricType);
                            }
                        } else{
                            //VoiUpdateFilterData(dynamicIndex);
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = "";
                            mFilterBaseData.name = "";
                            mFilterBaseData.filterIndex = dynamicIndex;
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            mDynamicFilterLeftAdapter.notifyDataSetChanged();
                            //rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                            updateMetricType(metricType);
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t){
                    Logger.Error("API Exception " + t.getMessage());
                    mHomeActivity.endProgress();
                }
            });
        } catch(Exception e){
            e.printStackTrace();
            mHomeActivity.endProgress();
        }
    }

    public class ApplySync extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            mHomeActivity.startProgress(mHomeActivity);
        }

        @Override
        protected Void doInBackground(Void... voids){
            clickOnApplytoUpdatePreference();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid){
            super.onPostExecute(aVoid);
            mHomeActivity.endProgress();
            mHomeActivity.onBackPressed();
        }
    }

    public void clickOnApplytoUpdatePreference(){

        mHomeActivity.setDashboardUpdateAfterApply(true);
        if(InGaugeSession.read(getString(R.string.key_is_need_update_on_apply), false)){
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), false);

            mHomeActivity.mAccessibleTenantLocationDataModel = null;
            mHomeActivity.mAccessibleTenantLocationDataModel = mAccessibleTenantLocationDataModel;


            Gson gson = new Gson();
            String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_obj), json);


            mHomeActivity.mDashboardLocationGroupListModel = null;
            mHomeActivity.mDashboardLocationGroupListModel = mDashboardLocationGroupListModel;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_group_obj), json);

            mHomeActivity.mProductListFromLocationGroupModel = null;
            mHomeActivity.mProductListFromLocationGroupModel = mProductListFromLocationGroupModel;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_product_obj), json);


            mHomeActivity.mDashboardUserListModel = null;
            mHomeActivity.mDashboardUserListModel = mDashboardUserListModel;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mDashboardUserListModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_user_obj), json);
        }

        Logger.Error("<<< Selected Region ID >>>>>" + getSelectedRegionId());

        if(mLeftFilterBaseDatas.size() > 0){

            Gson gson = new Gson();
            String json = gson.toJson(mLeftFilterBaseDatas);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_filter_obj), json);

            gson = new Gson();
            json = gson.toJson(mLeftFilterBaseDatas);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_preference_filter_obj), json);

            gson = new Gson();
            json = gson.toJson(mLeftFilterBaseDatas);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_filter_obj_for_leader_board), json);
            //mHomeActivity.setmMainLeftFilterBaseDatas(mLeftFilterBaseDatas);
            /*if(mHomeActivity.isRegionFilter()){
                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), Integer.parseInt(mFilterBaseData.id));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype_name), mFilterBaseData.name);
            }*/

            for(FilterBaseData mFilterBaseData : mLeftFilterBaseDatas){
                String filterTypeName = mFilterBaseData.filterName;
                if(mFilterBaseData.isDynamicFilterSelected && mFilterBaseData.name.length() > 0){
                    try{
                        /*if (getSelectedRegionTypeId().length() > 0) {
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                        } else {*/
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), mFilterBaseData.regionTypeId);
                        //}
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype_name), getSelectedRegionTypeName());

                      /*  if (getSelectedRegionIdForLb().length() > 0) {
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionIdForLb()));
                        } else {*/
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), Integer.parseInt(mFilterBaseData.id));
                        //}

                       /* if (getSelectedRegionName().length() > 0) {
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region_name), getSelectedRegionName());
                        } else {*/
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region_name), mFilterBaseData.name);
                        mHomeActivity.setSelectedRegionName(mFilterBaseData.name);

                        //}

                    } catch(Resources.NotFoundException e){
                        e.printStackTrace();
                    } catch(NumberFormatException e){
                        e.printStackTrace();
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), -2);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype_name), getSelectedRegionTypeName());

                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), -2);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region_name), getSelectedRegionName());
                    }
                } else{
                    if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location)))){
                        //Accesible Tenant Location
                        //mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mFilterBaseData.name);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mFilterBaseData.filterMetricType);

                    } else if(filterTypeName.equalsIgnoreCase("CustomGoalFilter")){

                    } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group)))){
                        //Location Group
                        //mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mFilterBaseData.name);
                    } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product)))){
                        //mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);
                    } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user)))){
                        //User
                        //mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                        int id = -2;
                        if(mFilterBaseData.id.length() > 0){
                            id = Integer.parseInt(mFilterBaseData.id);
                        }
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), id);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name), mFilterBaseData.name);
                    } else if(filterTypeName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type)))){
                        //Metric Tpe
                        //mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                        //mHomeActivity.setNeedUpdateMetric ( true );
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_id), mFilterBaseData.id);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mFilterBaseData.name);
                    }
                }
            }

        }

        Bundle bundle = new Bundle();
        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
        bundle.putString("industry", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
        bundle.putString("geography_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_regiontype_name), ""));
        bundle.putString("region", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_region_name), ""));
        bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
        bundle.putString("location_group", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name), ""));
        bundle.putString("product", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name), ""));
        bundle.putString("user", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), ""));
        bundle.putString("metric_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), ""));
        InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_filter", bundle);

    }


    private void setMetricTypeList(){
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "" + 0;
        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);
        mFilterBaseData.filterIndex = 6;
        //mFilterBaseData.filterName = getString(R.string.metric_type);
        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
        //InGaugeSession.write(mContext.getResources().getString(R.string.key_matric_data_type), mHomeActivity.mStringListMetricType.get(0));
        mLeftFilterBaseDatas.add(mFilterBaseData);
        mDynamicFilterLeftAdapter.notifyDataSetChanged();
        updateMetricType(metricType);

        //selectedId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_id), "");
        //selectedName= InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), "");


        //setFilterAdapter(mLeftFilterBaseDatas);
    }


    public String getSelectedRegionId(){
        return selectedRegionId;
    }

    public void setSelectedRegionId(String selectedRegionId){
        this.selectedRegionId = selectedRegionId;
    }

    public String getSelectedRegionName(){
        return selectedRegionName;
    }

    public void setSelectedRegionName(String selectedRegionName){
        this.selectedRegionName = selectedRegionName;
    }

    public String getSelectedRegionTypeId(){
        return selectedRegionTypeId;
    }

    public void setSelectedRegionTypeId(String selectedRegionTypeId){
        this.selectedRegionTypeId = selectedRegionTypeId;
    }

    public String getSelectedRegionTypeName(){
        return selectedRegionTypeName;
    }

    public void setSelectedRegionTypeName(String selectedRegionTypeName){
        this.selectedRegionTypeName = selectedRegionTypeName;
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
        InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), false);
    }

    public void updateMetricType(String MetricType){

        if(isNeedUpdateMetricType){
            isNeedUpdateMetricType = false;
            int innerdynamicIndexforUser = -1;
            String filterMetricType = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
            for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                if(filterMetricType.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                    innerdynamicIndexforUser = mLeftFilterBaseDatas.get(i).filterIndex;
                    break;
                }
            }
            for(FilterMetricTypeModel metricTypeModels : mHomeActivity.mFilterMetricTypeModelList){
                if(metricTypeModels.name.equals(MetricType)){
                    FilterBaseData mFilterBaseData = new FilterBaseData();

                    mFilterBaseData.id = metricTypeModels.id;
                    if(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                        if(MetricType.equalsIgnoreCase("Arrival")){
                            mFilterBaseData.name = "Check out";
                        } else if(MetricType.equalsIgnoreCase("Departure")){
                            mFilterBaseData.name = "Check in";
                        } else if(MetricType.equalsIgnoreCase("Daily")){
                            mFilterBaseData.name = "Daily";
                        }
                    } else{
                        mFilterBaseData.name = MetricType;
                    }
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size() - 1;
                    mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                }
            }
            //mFilterBaseData.filterName = mContext.getResources().getString(R.string.metric_type);
        }

    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


    public void filterCallAfterCheck(){

        if(!mHomeActivity.isRegionFilter()){
            if(mHomeActivity.isTenantLocation()){
                getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                        , "name", "ASC", null,
                        "Normal");
                return;
            }
            if(mHomeActivity.isLocationGroup()){
                if(!mHomeActivity.isTenantLocation()){
                    mHomeActivity.setTenantLocation(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        null,
                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                return;
            }
            if(mHomeActivity.isProduct()){

                if(!mHomeActivity.isTenantLocation()){
                    mHomeActivity.setTenantLocation(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                if(!mHomeActivity.isLocationGroup()){
                    mHomeActivity.setLocationGroup(true);
                    getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                getProductList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        null, null);

            }
            if(mHomeActivity.isUser()){
                if(!mHomeActivity.isTenantLocation()){
                    mHomeActivity.setTenantLocation(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                if(!mHomeActivity.isLocationGroup()){
                    mHomeActivity.setLocationGroup(true);
                    getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        null,
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);

            } else{
                fetchinitalDataForLeftFilterAfterClearAll();
                return;
            }
        } else{
            integerListHashMap = mHomeActivity.getIntegerListHashMap();
            if(integerListHashMap != null && integerListHashMap.size() > 0){

                for(int i = 0; i < integerListHashMap.size(); i++){
                    int count = 0;
                    List<DynamicData> mDynamicDatas = integerListHashMap.get(i);
                    if(i == 0){
                        if(mDynamicDatas != null && mDynamicDatas.size() > 0){
                            setSelectedRegionTypeId(String.valueOf(mDynamicDatas.get(0).getRegionTypeId()));
                            setSelectedRegionTypeName(mDynamicDatas.get(0).getRegionTypeName());
                            setSelectedRegionId(String.valueOf(mDynamicDatas.get(0).getId()));
                            setSelectedRegionName(mDynamicDatas.get(0).getName());
                        }
                    }
                }
                if(!mHomeActivity.isTenantLocation() && !mHomeActivity.isLocationGroup() && !mHomeActivity.isProduct() && !mHomeActivity.isUser()){
                    fetchinitalDataForLeftFilterAfterClearAll();
                }
                if(mHomeActivity.isTenantLocation()){

                    getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", getSelectedRegionId(),
                            "Normal");
                    return;
                }
                if(mHomeActivity.isLocationGroup()){

                    getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                if(mHomeActivity.isProduct()){

                    if(!mHomeActivity.isTenantLocation()){
                        mHomeActivity.setTenantLocation(true);
                        getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                                , "name", "ASC", null,
                                "Normal");
                        return;
                    }
                    if(!mHomeActivity.isLocationGroup()){
                        mHomeActivity.setLocationGroup(true);
                        getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                null,
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                        return;
                    }
                    getProductList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null, null);
                    return;
                }
                if(mHomeActivity.isUser()){

                    if(!mHomeActivity.isTenantLocation()){
                        mHomeActivity.setTenantLocation(true);
                        getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0))
                                , "name", "ASC", null,
                                "Normal");
                        return;
                    }
                    if(!mHomeActivity.isLocationGroup()){
                        mHomeActivity.setLocationGroup(true);
                        getDashboardLocationGroupList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                null,
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                        return;
                    }
                    getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);

                } else{
                    fetchinitalDataForLeftFilterAfterClearAll();

                    return;
                }
            } else{
                fetchinitalDataForLeftFilterAfterClearAll();
                return;
            }

        }
    }

    /**
     * @param authToken
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param regionId
     * @param activeStatus
     */
    void getAccessibleTenantLocation(final String authToken, int tenantId, String userId, String orderBy, String sort, String regionId, String activeStatus){

        mCall = apiInterface.getAccessibleTenantLocation(tenantId, userId, orderBy, sort, regionId, activeStatus, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) response.body();

                    if((mAccessibleTenantLocationDataModel != null && (mAccessibleTenantLocationDataModel.getData() != null && mAccessibleTenantLocationDataModel.getData().size() > 0))){

                        Collections.sort(mAccessibleTenantLocationDataModel.getData(), new Comparator<AccessibleTenantLocationDataModel.Datum>(){
                            @Override
                            public int compare(AccessibleTenantLocationDataModel.Datum d1, AccessibleTenantLocationDataModel.Datum d2){
                                return Boolean.compare(d2.getMasterLocation(), d1.getMasterLocation());
                            }
                        });
                        if(mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            mHomeActivity.setLocationGroup(true);
                        }
                        if(mHomeActivity.isLocationGroup() || mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            if(mHomeActivity.isLocationGroup()){
                                getDashboardLocationGroupList(authToken,
                                        String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId()),
                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                            } else if(mHomeActivity.isProduct()){
                                getProductList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), null, null);

                            } else if(mHomeActivity.isUser()){
                                getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0)),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);
                            }

                        } else{

                            fetchinitalDataForLeftFilterAfterClearAll();
                            //getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                            /*pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
                        }

                    } else{

                        if(mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            mHomeActivity.setLocationGroup(true);
                        }
                        if(mHomeActivity.isLocationGroup() || mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            if(mHomeActivity.isLocationGroup()){
                                if(mAccessibleTenantLocationDataModel != null && mAccessibleTenantLocationDataModel.getData() != null){
                                    if(mAccessibleTenantLocationDataModel.getData().size() > 0){
                                        getDashboardLocationGroupList(authToken,
                                                String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId()),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                    } else{
                                        getDashboardLocationGroupList(authToken,
                                                String.valueOf(-2),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                    }
                                } else{
                                    getDashboardLocationGroupList(authToken,
                                            String.valueOf(-2),
                                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                                }

                            } else if(mHomeActivity.isProduct()){
                                getProductList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                        null, null);

                            } else if(mHomeActivity.isUser()){
                                getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), null);

                            }

                        } else{
                            fetchinitalDataForLeftFilterAfterClearAll();
                      /*      pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
//                            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                        }

                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{
                    fetchinitalDataForLeftFilterAfterClearAll();
                    /*pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);*/
//                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupList(final String authToken, final String tenantlocationId, String userId){

        mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) response.body();

                    if((mDashboardLocationGroupListModel != null && (mDashboardLocationGroupListModel.getData() != null && mDashboardLocationGroupListModel.getData().size() > 0))){
                        if(mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            if(mHomeActivity.isProduct()){
                                getProductList(authToken, String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()), tenantlocationId);

                            } else if(mHomeActivity.isUser()){
                                if(mHomeActivity.isTenantLocation()){
                                    getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                            tenantlocationId,
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                } else{
                                    getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                            null,
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                }
                            }
                        } else{
                            /*pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
//                            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                            fetchinitalDataForLeftFilterAfterClearAll();
                        }

                    } else{

                        if(mHomeActivity.isProduct() || mHomeActivity.isUser()){
                            if(mHomeActivity.isProduct()){
                                if(mDashboardLocationGroupListModel != null && (mDashboardLocationGroupListModel.getData() != null && mDashboardLocationGroupListModel.getData().size() > 0)){
                                    getProductList(authToken, String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()), tenantlocationId);
                                } else{
                                    getProductList(authToken, String.valueOf(-2), tenantlocationId);
                                }


                            } else if(mHomeActivity.isUser()){
                                if(mHomeActivity.isTenantLocation()){
                                    if(mDashboardLocationGroupListModel != null && (mDashboardLocationGroupListModel.getData() != null && mDashboardLocationGroupListModel.getData().size() > 0)){

                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                    } else{
                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(-2));

                                    }

                                } else{

                                    if(mDashboardLocationGroupListModel != null && (mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModel.getData().size() > 0)){

                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                                    } else{
                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), String.valueOf(-2));
                                    }
                                }
                            }
                        } else{
                            fetchinitalDataForLeftFilterAfterClearAll();
                 /*           pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
                            //getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                        }
                    }
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{
                    fetchinitalDataForLeftFilterAfterClearAll();
                    /*pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);*/
                    //InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
//                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductList(final String authToken, final String locationGroupId, final String tenantId){

        mCall = apiInterface.getProductList(locationGroupId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) response.body();


                    if(mHomeActivity.isCustomGoalFilter()){

                        if(mHomeActivity.isTenantLocation() && mHomeActivity.isLocationGroup()){
                            getDashboardUserList(authToken,
                                    tenantId,
                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                    locationGroupId);
                        } else{
                            if(mHomeActivity.isTenantLocation()){
                                getDashboardUserList(authToken,
                                        tenantId,
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                        null);
                            } else{

                                if(mHomeActivity.isLocationGroup()){
                                    getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                            null,
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), locationGroupId);
                                } else{

                                    fetchinitalDataForLeftFilterAfterClearAll();
                                    /*pbFilter.setVisibility(View.GONE);
                                    imgFilter.setVisibility(View.VISIBLE);*/
                                    //getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                                }

                            }


                        }
                    } else{
                        if((mProductListFromLocationGroupModel != null && (mProductListFromLocationGroupModel.getData() != null && mProductListFromLocationGroupModel.getData().size() > 0))){

                            if(mHomeActivity.isTenantLocation() && mHomeActivity.isLocationGroup()){
                                getDashboardUserList(authToken,
                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                        InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                        locationGroupId);
                            } else{
                                if(mHomeActivity.isTenantLocation()){
                                    getDashboardUserList(authToken,
                                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 0)),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                            InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""),
                                            null);
                                } else{

                                    if(mHomeActivity.isLocationGroup()){
                                        getDashboardUserList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""),
                                                InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), locationGroupId);
                                    } else{
                                        fetchinitalDataForLeftFilterAfterClearAll();
                                        /*pbFilter.setVisibility(View.GONE);
                                        imgFilter.setVisibility(View.VISIBLE);*/
                                        //getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                                    }
                                }
                            }
                        } else{
                            fetchinitalDataForLeftFilterAfterClearAll();
                          /*  pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
                            //      getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                            //    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                        }

                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{
                    fetchinitalDataForLeftFilterAfterClearAll();
                    /*pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);*/
                    //InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    //getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });
    }


    /**
     * @param authToken
     * @param tenantLocationFromIdFromFilter
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserList(String authToken, String tenantLocationFromIdFromFilter, String startDate, String endDate, String locationGroupId){
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        mCall = apiInterface.getDashboardUserListForDashboard(TenantId, true, false, false, tenantLocationFromIdFromFilter, locationGroupId, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
//                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/

                if(response.body() != null){
                    mDashboardUserListModel = (DashboardUserListModel) response.body();
                    fetchinitalDataForLeftFilterAfterClearAll();
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{
                    fetchinitalDataForLeftFilterAfterClearAll();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/

            }
        });
    }

    public void fetchinitalDataForLeftFilterAfterClearAll(){

        mHomeActivity.endProgress();

//        mRegionTypeDataModel = mHomeActivity.mRegionTypeDataModel;
//        mRegionByTenantRegionTypeModel = mHomeActivity.mRegionByTenantRegionTypeModel;


     /*   for (int i = 0; i < mRegionTypeDataModel.getData().size(); i++) {
            Logger.Error("<<<<< Filter Region Type Name " + mRegionTypeDataModel.getData().get(i).getName());
            Logger.Error("<<<<< Filter Level " + mRegionTypeDataModel.getData().get(i).getLevel());
        }*/
        mLeftFilterBaseDatas.clear();
        FilterBaseData mFilterBaseData = new FilterBaseData();
        if(mHomeActivity.isRegionFilter()){
            if(integerListHashMap != null){
                for(int k = 0; k < integerListHashMap.size(); k++){

                    List<DynamicData> mDynamicDatas = integerListHashMap.get(k);

                    if(mDynamicDatas.size() > 0){

                        mFilterBaseData = new FilterBaseData();

                        for(int j = 0; j < mDynamicDatas.size(); j++){

                            if(j == 0){

                                mFilterBaseData.id = String.valueOf(mDynamicDatas.get(j).getId());

                                mFilterBaseData.name = mDynamicDatas.get(j).getName();

                                mFilterBaseData.regionTypeId = mDynamicDatas.get(j).getRegionTypeId();

                                mFilterBaseData.filterName = mDynamicDatas.get(j).getRegionTypeName();
                                mFilterBaseData.parentRegionId = mDynamicDatas.get(j).getParentRegionId();
                                if(k == 0){
                                    mFilterBaseData.isSelectedPosition = true;
                                    mFilterBaseData.isDynamicFilterSelected = true;
                                    mFilterBaseData.isSubRegionOpen = true;
                                }
                                mFilterBaseData.isDynamicFilter = true;
                                mFilterBaseData.filterIndex = k;
                                mLeftFilterBaseDatas.add(mFilterBaseData);
                            }
                        }
                    }
                }
            }
        }
        if(mHomeActivity.isTenantLocation()){
            if(mAccessibleTenantLocationDataModel != null && (mAccessibleTenantLocationDataModel.getData() != null && mAccessibleTenantLocationDataModel.getData().size() > 0)){
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId());
                mFilterBaseData.name = mAccessibleTenantLocationDataModel.getData().get(0).getName();
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
                mFilterBaseData.filterMetricType = mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType();
                mFilterBaseData.isMasterLocation = mAccessibleTenantLocationDataModel.getData().get(0).getMasterLocation();
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                TenantLocationIdForDynamic = mFilterBaseData.id;
                isNeedUpdateMetricType = true;
                metricType = mFilterBaseData.filterMetricType;
                mLeftFilterBaseDatas.add(mFilterBaseData);
            } else{
                mLeftFilterBaseDatas.add(new FilterBaseData());
            }

        }
        if(mHomeActivity.isLocationGroup()){
            if(mDashboardLocationGroupListModel != null && (mDashboardLocationGroupListModel.getData() != null && mDashboardLocationGroupListModel.getData().size() > 0)){
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID());
                mFilterBaseData.name = mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName();
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                mLeftFilterBaseDatas.add(mFilterBaseData);
                LocationGroupIdDynamic = mFilterBaseData.id;
            } else{
                mLeftFilterBaseDatas.add(new FilterBaseData());
            }

        }
        if(mHomeActivity.isProduct()){
            if(mHomeActivity.isCustomGoalFilter()){
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(-1);
                mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
                mFilterBaseData.filterName = getString(R.string.product);
                mFilterBaseData.isSelectedPosition = false;
                mFilterBaseData.isDynamicFilter = false;
                mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                mLeftFilterBaseDatas.add(mFilterBaseData);
            } else{
                if(mProductListFromLocationGroupModel != null && (mProductListFromLocationGroupModel.getData() != null && mProductListFromLocationGroupModel.getData().size() > 0)){
                    mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(mProductListFromLocationGroupModel.getData().get(0).getId());
                    mFilterBaseData.name = mProductListFromLocationGroupModel.getData().get(0).getName();
                    //mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    mLeftFilterBaseDatas.add(mFilterBaseData);
                } else{
                    mLeftFilterBaseDatas.add(new FilterBaseData());
                }
            }

        }
        if(mHomeActivity.isUser()){

            if(mDashboardUserListModel != null && (mDashboardUserListModel.getData() != null && mDashboardUserListModel.getData().size() > 0)){
                try{
                    mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = "" + mDashboardUserListModel.getData().get(0).getId();
                    mFilterBaseData.name = mDashboardUserListModel.getData().get(0).getName();
                    //mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                    mFilterBaseData.isSelectedPosition = false;
                    mFilterBaseData.isDynamicFilter = false;
                    mFilterBaseData.filterIndex = mLeftFilterBaseDatas.size();
                    mLeftFilterBaseDatas.add(mFilterBaseData);

                } catch(Exception e){
                    e.printStackTrace();

                }
            }

        }
        //Pass the first level
        setMetricTypeandLeftAdapter(0);
        updateMetricType(metricType);
        fetchInitalRightListData();
    }
}
