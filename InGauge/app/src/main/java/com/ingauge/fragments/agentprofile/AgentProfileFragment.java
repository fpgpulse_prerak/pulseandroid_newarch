package com.ingauge.fragments.agentprofile;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.AgentProfileListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.UserProfileFragment;
import com.ingauge.pojo.AccessibleTenantLocationDataModelForAgentProfile;
import com.ingauge.pojo.AgentProfileDetailsPojo;
import com.ingauge.pojo.AgentProfileHowAmIDoingList;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.GamificationPostAPIPojo;
import com.ingauge.pojo.UserRoleTenantListModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.GridSpanDecoration;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 29-Mar-18.
 */

public class AgentProfileFragment extends Fragment implements View.OnClickListener{


    public static String KEY_IS_FROM_LEADER_BOARD = "is_from_leaderboard";
    public static String KEY_USER_ID_FROM_LEADER_BOARD = "user_id_from_leaderboard";
    private HomeActivity mHomeActivity;
    private View rootView;

    private ImageView ivProfile;

    private TextView tvAgentName;
    private ImageView ivEditProfile;
    private TextView tvAgentRole;

    private TextView tvLikeLabel;
    private TextView tvLikeCount;

    private TextView tvShortOutLabel;
    private TextView tvShortOutCount;

    private TextView tvLobbyPostLabel;
    private TextView tvLobbyPostCount;

    private TextView tvIncrementalRevenueLabel;
    private TextView tvIncrementalRevenueValue;

    private TextView tvTransactoinLabel;
    private TextView tvTransactoinValue;

    private TextView tvHowAmIDoingLabel;

    private RecyclerView mRecyclerViewAgentProfileRecords;
    private ProgressBar pbUserProfile;

    APIInterface apiInterface;
    //Parameter Profile For API Calling

    private String imageUrl = "";
    private String userName = "";
    //    public String userIdForAPI = "646";
    public Integer tenantId = 1;
    //    public Integer tenantLocationId = 55;
    public String from;
    public String to;
    public Integer fromDateTime;
    public Integer toDateTime;
    public String metricsDateType;
    List<String> mHowAmIDoingHeaders = new ArrayList<>();
    List<AgentProfileHowAmIDoingList> mAgentProfileHowAmIDoingLists = new ArrayList<>();
    AgentProfileListAdapter mAgentProfileListAdapter;

//    public ArrayList<FilterBaseData> mFilterBaseDatas;


    public boolean isAgentProfileFragment = true;
//    public AccessibleTenantLocationDataModelForAgentProfile mAccessibleTenantLocationDataModel;
//    public DashboardUserListModel mDashboardUserListModel;


    private boolean isUserOperator = false;
    SimpleDateFormat sdfServerDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);


    private String userIdFromLeaderBoard = "";
    private boolean isFromLeaderBoard = false;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        isAgentProfileFragment = true;
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setOnClickListener(this);
        mHomeActivity.ibMenu.setImageResource(android.R.drawable.ic_menu_save);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        if(getArguments() != null){
            isFromLeaderBoard = getArguments().getBoolean(KEY_IS_FROM_LEADER_BOARD, false);
            userIdFromLeaderBoard = getArguments().getString(KEY_USER_ID_FROM_LEADER_BOARD, String.valueOf(UiUtils.getUserId(mHomeActivity)));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_agent_profile, container, false);
            initView(rootView);
            if(isFromLeaderBoard){
                mHomeActivity.userIdForAPI = userIdFromLeaderBoard;
            } else{
                mHomeActivity.userIdForAPI = String.valueOf(UiUtils.getUserId(mHomeActivity));
            }

            ivEditProfile.setVisibility(View.VISIBLE);
            pbUserProfile.setVisibility(View.VISIBLE);
            if(mHomeActivity.userIdForAPI.equalsIgnoreCase(String.valueOf(UiUtils.getUserId(mHomeActivity)))){
                ivEditProfile.setVisibility(View.VISIBLE);
            } else{
                ivEditProfile.setVisibility(View.GONE);
            }

            mHomeActivity.mFilterBaseDatas = new ArrayList<>();
            mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile = new AccessibleTenantLocationDataModelForAgentProfile();
            mHomeActivity.mDashboardUserListModelForAgentProfile = new DashboardUserListModel();
            mHomeActivity.startProgress(mHomeActivity);
            GetUsersProfile(mHomeActivity.userIdForAPI, false);




            /*
            Glide.with(ivProfile.getContext())
                    .load(imageUrl).placeholder(R.drawable.placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .override(200, 200)
                    .skipMemoryCache(true)
                    .dontAnimate()
                    .listener(new RequestListener<String, GlideDrawable>(){
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource){
                            pbUserProfile.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache, boolean isFirstResource){
                            pbUserProfile.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(ivProfile);*/


            mHomeActivity.mFilterBaseDatas.clear();


        } else{

            isAgentProfileFragment = true;
            if(mHomeActivity.isNeedAgentProfileUpdate()){

                mHomeActivity.startProgress(mHomeActivity);

                if(mHomeActivity.userIdForAPI.equalsIgnoreCase(String.valueOf(UiUtils.getUserId(mHomeActivity)))){
                    ivEditProfile.setVisibility(View.VISIBLE);
                } else{
                    ivEditProfile.setVisibility(View.GONE);
                }

                pbUserProfile.setVisibility(View.VISIBLE);
                String imageUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(mHomeActivity) + "/" + mHomeActivity.userIdForAPI;
                Glide.with(ivProfile.getContext())
                        .load(imageUrl).placeholder(R.drawable.defaultimg)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .override(200, 200)
                        .dontAnimate()
                        .listener(new RequestListener<String, GlideDrawable>(){
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource){
                                pbUserProfile.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                           boolean isFromMemoryCache, boolean isFirstResource){
                                pbUserProfile.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(ivProfile);

                mHomeActivity.setNeedAgentProfileUpdate(false);
                userName = mHomeActivity.userSelectedName;
                tvAgentName.setText(userName);
                tvAgentRole.setText("");
                getUserRoleAndTeneantList(Integer.parseInt(mHomeActivity.userIdForAPI), false);

            } else if(mHomeActivity.isNeedAgentProfileUpdateForUserProfile()){
                mHomeActivity.startProgress(mHomeActivity);
                GetUsersProfile(mHomeActivity.userIdForAPI, true);
                mHomeActivity.setNeedAgentProfileUpdateForUserProfile(false);
            }

            if(rootView.getParent() != null){
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }
        }

        return rootView;
    }

    private void initView(View itemView){
        ivProfile = (ImageView) itemView.findViewById(R.id.layout_agent_profile_top_iv_user_profile);
        tvAgentName = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_agent_name);
        ivEditProfile = (ImageView) itemView.findViewById(R.id.layout_agent_profile_top_iv_edit_profile);
        ivEditProfile.setOnClickListener(this);
        tvAgentRole = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_agent_role);
        tvLikeLabel = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_like_label);
        tvLikeCount = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_like_count);
        tvShortOutLabel = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_shortout_label);
        tvShortOutCount = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_shortout_count);
        tvLobbyPostLabel = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_lobbypost_label);
        tvLobbyPostCount = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_lobbypost_count);
        tvIncrementalRevenueLabel = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_incre_revenue_label);
        tvIncrementalRevenueValue = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_incre_revenue_value);
        tvTransactoinLabel = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_transaction_label);
        tvTransactoinValue = (TextView) itemView.findViewById(R.id.layout_agent_profile_top_tv_transaction_value);
        tvHowAmIDoingLabel = (TextView) itemView.findViewById(R.id.fragment_agent_profile_tv_how_am_i_doing_label);
        mRecyclerViewAgentProfileRecords = (RecyclerView) itemView.findViewById(R.id.layout_agent_profile_bottom_rv);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(mHomeActivity, 2);
        mRecyclerViewAgentProfileRecords.setHasFixedSize(true);
        mRecyclerViewAgentProfileRecords.setLayoutManager(mGridLayoutManager);

        mRecyclerViewAgentProfileRecords.addItemDecoration(new GridSpanDecoration(3));
        pbUserProfile = (ProgressBar) itemView.findViewById(R.id.layout_profile_top_iv_profile_progress);


        if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
            tvIncrementalRevenueLabel.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_agent_profile_ancillary_revenue)));
        } else if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
            tvIncrementalRevenueLabel.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_agent_profile_incremental_revenue)));
        }

        tvLikeLabel.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_agent_profile_likes)));

        tvShortOutLabel.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_agent_profile_shoutouts)));

        tvLobbyPostLabel.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_agent_profile_lobby_posts)));
        tvHowAmIDoingLabel.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_agent_profile_how_am_i_doing)));
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.layout_agent_profile_top_iv_edit_profile:
                isAgentProfileFragment = false;
                UserProfileFragment fragment = new UserProfileFragment();
                mHomeActivity.replace(fragment, mHomeActivity.getResources().getString(R.string.tag_user_profile));
                break;
        }
    }

    /*{"tenantId":1,"regionTypeId":0,"regionId":0,"tenantLocationId":55,"locationGroupId":0,
     "productId":0,"userId":706,"from":"2018-03-01","to":"2018-03-06","isCompare":false,"compFrom":null,
      "compTo":null,"fromDateTime":1519842600000,"toDateTime":1520360999000,"compFromDateTime":null,
      "compToDateTime":null,"metricsDateType":"Departure","jsonValue":null,"groupBy":null,"dimensionSet":null}*/
    void getAgentGamificationDetails(){

        GamificationPostAPIPojo mGamificationPostAPIPojo = new GamificationPostAPIPojo();
        mGamificationPostAPIPojo.setTenantId(tenantId);
        mGamificationPostAPIPojo.setRegionTypeId(0);
        mGamificationPostAPIPojo.setRegionId(0);
        mGamificationPostAPIPojo.setTenantLocationId(mHomeActivity.tenantLocationId);
        mGamificationPostAPIPojo.setLocationGroupId(0);
        mGamificationPostAPIPojo.setProductId(0);
        mGamificationPostAPIPojo.setUserId(Integer.parseInt(mHomeActivity.userIdForAPI));
        try{
            mGamificationPostAPIPojo.setFrom(DateTimeUtils.getFirstDay(new Date()));
        } catch(Exception e){
            e.printStackTrace();
        }

        try{
            mGamificationPostAPIPojo.setTo(DateTimeUtils.getTodayDateString(new Date()));
        } catch(Exception e){
            e.printStackTrace();
        }

        mGamificationPostAPIPojo.setIsCompare(false);
        mGamificationPostAPIPojo.setCompFrom(null);
        mGamificationPostAPIPojo.setCompTo(null);

        try{
            String fromDate = DateTimeUtils.getFirstDay(new Date());
            String toDate = DateTimeUtils.getTodayDateString(new Date());
            Calendar fromserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(fromDate + " 00:00:00"));
            Calendar toserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(toDate + " 23:59:59"));
            mGamificationPostAPIPojo.setFromDateTime(fromserverCalendar.getTimeInMillis());
            mGamificationPostAPIPojo.setToDateTime(toserverCalendar.getTimeInMillis());
        } catch(Exception e){
            e.printStackTrace();
        }

        mGamificationPostAPIPojo.setCompFromDateTime(null);
        mGamificationPostAPIPojo.setCompToDateTime(null);
        mGamificationPostAPIPojo.setMetricsDateType("Departure");
        mGamificationPostAPIPojo.setJsonValue(null);
        mGamificationPostAPIPojo.setGroupBy(null);
        mGamificationPostAPIPojo.setDimensionSet(null);

        Call mCall = apiInterface.getGamificationDetails(mGamificationPostAPIPojo);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){

                mHomeActivity.endProgress();
                try{
                    tvAgentRole.setText(mHomeActivity.userRoleName);
                    String jsonResponse = response.body().string();
                    Logger.Error("<<< Response  >>>>> " + jsonResponse);
                    try{
                        JSONObject mJsonObject = new JSONObject(jsonResponse);
                        JSONObject mDataJsonObj = mJsonObject.optJSONObject("data");
                        String qTableHeaders = mDataJsonObj.optString("qtableHeaders");
                        Logger.Error("<<< Qtable Headers String  >>> " + qTableHeaders);

//                        List<String> mQTableHeadersList = Arrays.asList(qTableHeaders.split(","));


                        String[] mQTableHeadersList = new String[]{};
                        if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                            mQTableHeadersList = new String[10];
                        } else{

                            mQTableHeadersList = new String[9];
                        }

                        List<String> mQTableTempHeadersList = Arrays.asList(qTableHeaders.split(","));
                        for(int i = 0; i < mQTableTempHeadersList.size(); i++){
                            if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                                if(mQTableTempHeadersList.get(i).equalsIgnoreCase("MTD ANCILLARY REVENUE")){
                                    mQTableHeadersList[0] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(0,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("AGENT INCENTIVE PAYOUT")){
                                    mQTableHeadersList[1] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(1,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("GOAL PROGRESS")){
                                    mQTableHeadersList[2] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(2,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("GOAL")){
                                    //mQTableHeadersList.add(3,mQTableTempHeadersList.get(i));
                                    mQTableHeadersList[3] = mQTableTempHeadersList.get(i);
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("AVG. UPSELL RATE")){
                                    //mQTableHeadersList.add(4,mQTableTempHeadersList.get(i));
                                    mQTableHeadersList[4] = mQTableTempHeadersList.get(i);
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("YTD ANCILLARY REVENUE")){
                                    //mQTableHeadersList.add(5,mQTableTempHeadersList.get(i));
                                    mQTableHeadersList[5] = mQTableTempHeadersList.get(i);
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("PROJECTED EOM")){
                                    //mQTableHeadersList.add(6,mQTableTempHeadersList.get(i));
                                    mQTableHeadersList[6] = mQTableTempHeadersList.get(i);
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("MOM VARIANCE")){
                                    //mQTableHeadersList.add(7,mQTableTempHeadersList.get(i));
                                    mQTableHeadersList[7] = mQTableTempHeadersList.get(i);
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("IR LOCATION RANK")){
                                    //mQTableHeadersList.add(8,mQTableTempHeadersList.get(i));
                                    mQTableHeadersList[9] = mQTableTempHeadersList.get(i);
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("MTD AGENT ARPD")){
                                    //mQTableHeadersList.add(9,mQTableTempHeadersList.get(i));
                                    mQTableHeadersList[8] = mQTableTempHeadersList.get(i);
                                }
                            } else if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
                                if(mQTableTempHeadersList.get(i).equalsIgnoreCase("MTD INCREMENTAL REVENUE")){
                                    //mQTableHeadersList.add(0,mQTableTempHeadersList.get(i));
                                    mQTableHeadersList[0] = mQTableTempHeadersList.get(i);
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("AGENT INCENTIVE PAYOUT")){
                                    mQTableHeadersList[1] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(1,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("GOAL PROGRESS")){
                                    mQTableHeadersList[2] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(2,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("GOAL")){
                                    mQTableHeadersList[3] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(3,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("AVG. UPSELL RATE")){
                                    mQTableHeadersList[4] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(4,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("YTD INCREMENTAL REVENUE")){
                                    mQTableHeadersList[5] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(5,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("PROJECTED EOM")){
                                    mQTableHeadersList[6] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(6,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("MOM VARIANCE")){
                                    mQTableHeadersList[7] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(7,mQTableTempHeadersList.get(i));
                                } else if(mQTableTempHeadersList.get(i).equalsIgnoreCase("IR LOCATION RANK")){
                                    mQTableHeadersList[8] = mQTableTempHeadersList.get(i);
                                    //mQTableHeadersList.add(8,mQTableTempHeadersList.get(i));
                                }
                            }
                        }
                        mHowAmIDoingHeaders = new ArrayList<>();
                        mHowAmIDoingHeaders = getmQHeadersKey(mQTableHeadersList);
                        JSONObject qJsonTableData = (JSONObject) ((JSONObject) mDataJsonObj.optJSONArray("reportingDatas").opt(0)).optJSONArray("qtableData").opt(0);

                        if(mHowAmIDoingHeaders != null && mHowAmIDoingHeaders.size() > 0){
                            if(qJsonTableData != null){
                                Logger.Error("<<< Q Table Data >>>" + qJsonTableData.toString());
                                mAgentProfileHowAmIDoingLists = getAgentProfileListData(qJsonTableData);

                                if(mHomeActivity.isLocationAvailable){
                                    tvLikeCount.setText("" + qJsonTableData.optInt("LIKES"));
                                    tvShortOutCount.setText("" + qJsonTableData.optInt("SHOUTOUTS"));
                                    tvLobbyPostCount.setText("" + qJsonTableData.optInt("LOBBY POSTS"));
                                    tvTransactoinValue.setText("" + qJsonTableData.optInt("TRANSACTIONS"));
                                    if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                                        tvIncrementalRevenueValue.setText(qJsonTableData.optString("ANCILLARY REVENUE"));
                                    } else if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
                                        tvIncrementalRevenueValue.setText(qJsonTableData.optString("INCREMENTAL REVENUE"));
                                    }
                                } else{
                                    tvLikeCount.setText("-");
                                    tvShortOutCount.setText("-");
                                    tvLobbyPostCount.setText("-");
                                    tvTransactoinValue.setText("-");
                                    tvIncrementalRevenueValue.setText("-");
                                }


                                mHomeActivity.tvTitle.setText(mHomeActivity.tenantLocationLocationName);
                                mAgentProfileListAdapter = new AgentProfileListAdapter(mHomeActivity, mAgentProfileHowAmIDoingLists, mHomeActivity.isLocationAvailable);
                                mRecyclerViewAgentProfileRecords.setAdapter(mAgentProfileListAdapter);
                            }
                        }

                    } catch(Exception e){
                        e.printStackTrace();
                    }
                } catch(IOException e){


                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                t.getMessage();

            }
        });

    }

    private List<AgentProfileHowAmIDoingList> getAgentProfileListData(JSONObject mJsonObjectQTableData){

        List<AgentProfileHowAmIDoingList> mAgentProfileHowAmIDoingLists = new ArrayList<>();
        for(String qHeaderKey : mHowAmIDoingHeaders){
            AgentProfileHowAmIDoingList mAgentProfileHowAmIDoingList = new AgentProfileHowAmIDoingList();
            if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                switch(qHeaderKey){
                    case "MTD ANCILLARY REVENUE":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf201;
                        mAgentProfileHowAmIDoingList.colorCode = "#00BCD4";
                        break;
                    case "AGENT INCENTIVE PAYOUT":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf3d1;
                        mAgentProfileHowAmIDoingList.colorCode = "#EC407A";
                        break;
                    case "GOAL PROGRESS":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf140;
                        mAgentProfileHowAmIDoingList.colorCode = "#546E7A";

                        break;
                    case "GOAL":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf1e3;
                        mAgentProfileHowAmIDoingList.colorCode = "#616161";
                        break;
                    case "AVG. UPSELL RATE":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf1ad;
                        mAgentProfileHowAmIDoingList.colorCode = "#009688";
                        break;
                    case "YTD ANCILLARY REVENUE":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf2e1;
                        mAgentProfileHowAmIDoingList.colorCode = "#2979FF";
                        break;
                    case "PROJECTED EOM":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf11e;
                        mAgentProfileHowAmIDoingList.colorCode = "#FF9300";
                        break;
                    case "MOM VARIANCE":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf422;
                        mAgentProfileHowAmIDoingList.colorCode = "#61A771";
                        break;
                    case "IR LOCATION RANK":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf161;
                        mAgentProfileHowAmIDoingList.colorCode = "#80D8FF";
                        break;
                    case "MTD AGENT ARPD":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = String.valueOf(mJsonObjectQTableData.optInt(qHeaderKey));
                        mAgentProfileHowAmIDoingList.hexCode = 0xf073;
                        mAgentProfileHowAmIDoingList.colorCode = "#8D6E63";
                        break;
                }
            } else if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
                switch(qHeaderKey){
                    case "MTD INCREMENTAL REVENUE":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf201;
                        mAgentProfileHowAmIDoingList.colorCode = "#00BCD4";
                        break;
                    case "AGENT INCENTIVE PAYOUT":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf3d1;
                        mAgentProfileHowAmIDoingList.colorCode = "#EC407A";
                        break;
                    case "GOAL PROGRESS":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf140;
                        mAgentProfileHowAmIDoingList.colorCode = "#546E7A";
                        break;
                    case "GOAL":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf1e3;
                        mAgentProfileHowAmIDoingList.colorCode = "#616161";
                        break;
                    case "AVG. UPSELL RATE":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf1ad;
                        mAgentProfileHowAmIDoingList.colorCode = "#009688";
                        break;
                    case "YTD INCREMENTAL REVENUE":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf2e1;
                        mAgentProfileHowAmIDoingList.colorCode = "#2979FF";
                        break;
                    case "PROJECTED EOM":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf11e;
                        mAgentProfileHowAmIDoingList.colorCode = "#FF9300";
                        break;
                    case "MOM VARIANCE":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;
                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf422;
                        mAgentProfileHowAmIDoingList.colorCode = "#61A771";
                        break;
                    case "IR LOCATION RANK":
                        mAgentProfileHowAmIDoingList.reportName = qHeaderKey;

                        mAgentProfileHowAmIDoingList.reportValue = mJsonObjectQTableData.optString(qHeaderKey);
                        mAgentProfileHowAmIDoingList.hexCode = 0xf161;
                        mAgentProfileHowAmIDoingList.colorCode = "#80D8FF";
                        break;
                }
            }
            mAgentProfileHowAmIDoingLists.add(mAgentProfileHowAmIDoingList);
        }

        return mAgentProfileHowAmIDoingLists;
    }

    private List<String> getmQHeadersKey(String[] mAllQHeadersList){
        List<String> keyList = new ArrayList<>();
        for(String key : mAllQHeadersList){
            if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                if(key.equalsIgnoreCase("MTD ANCILLARY REVENUE")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("AGENT INCENTIVE PAYOUT")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("GOAL PROGRESS")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("GOAL")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("AVG. UPSELL RATE")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("YTD ANCILLARY REVENUE")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("PROJECTED EOM")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("MOM VARIANCE")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("IR LOCATION RANK")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("MTD AGENT ARPD")){
                    keyList.add(key);
                }
            } else if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
                if(key.equalsIgnoreCase("MTD INCREMENTAL REVENUE")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("AGENT INCENTIVE PAYOUT")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("GOAL PROGRESS")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("GOAL")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("AVG. UPSELL RATE")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("YTD INCREMENTAL REVENUE")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("PROJECTED EOM")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("MOM VARIANCE")){
                    keyList.add(key);
                } else if(key.equalsIgnoreCase("IR LOCATION RANK")){
                    keyList.add(key);
                }
            }
        }
        return keyList;
    }

    void GetUsersProfile(final String userId, final boolean isOnlyProfileUpdate){

        Call mCall = apiInterface.getAgentProfileDetails(userId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(isOnlyProfileUpdate){
                    mHomeActivity.endProgress();
                }
                if(response.body() != null){

                    AgentProfileDetailsPojo userdata = (AgentProfileDetailsPojo) response.body();
                    userName = userdata.getData().getUserFullName();
                    mHomeActivity.userRoleName = InGaugeSession.read(getString(R.string.key_user_role_name), "");
                    if(isAdded()){

                        tvAgentName.setText(userName);
                        if(isFromLeaderBoard){
                            tvAgentRole.setText("");
                        }else{
                            tvAgentRole.setText(mHomeActivity.userRoleName);
                        }


                        String imageUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(mHomeActivity) + "/" + userdata.getData().getId();

                        Logger.Error("<<< User Photo Url  >>>>  " + imageUrl);
                        Glide.with(ivProfile.getContext())
                                .load(imageUrl).placeholder(R.drawable.defaultimg)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .override(200, 200)
                                .dontAnimate()
                                .listener(new RequestListener<String, GlideDrawable>(){
                                    @Override
                                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource){
                                        pbUserProfile.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                                   boolean isFromMemoryCache, boolean isFirstResource){
                                        pbUserProfile.setVisibility(View.GONE);
                                        return false;
                                    }
                                })
                                .into(ivProfile);
                    }
                    if(isFromLeaderBoard){
                        getUserRoleAndTeneantList(Integer.parseInt(mHomeActivity.userIdForAPI), isFromLeaderBoard);
                    } else{
                        if(!isOnlyProfileUpdate){
                            getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                    InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1),
                                    mHomeActivity.userIdForAPI);
                        } else{
                            mHomeActivity.endProgress();
                        }
                    }


                    Logger.Error("<<<< Agent Profile  Name >>>>>" + userdata.getData().getUserFullName());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }


    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.ibAttachment.setImageResource(R.drawable.ic_bell_notification);
        mHomeActivity.tvTitle.setText(mHomeActivity.tenantLocationLocationName);
        mHomeActivity.ibMenu.setImageResource(R.drawable.ic_settings);
        mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
        mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        mHomeActivity.toolbar.setClickable(true);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibBack.setVisibility(View.VISIBLE);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                isAgentProfileFragment = false;
                NotificationSettingsFragment mNotificationSettingsFragment = new NotificationSettingsFragment();
                mHomeActivity.replace(mNotificationSettingsFragment, mHomeActivity.getResources().getString(R.string.tag_notification_list_screen));
            }
        });
        mHomeActivity.ibAttachment.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                isAgentProfileFragment = false;
                UserNotificationsFragment mUserNotificationsFragment = new UserNotificationsFragment();
                Bundle mBundle = new Bundle();
                mBundle.putInt(UserNotificationsFragment.KEY_USERID, Integer.parseInt(mHomeActivity.userIdForAPI));
                mUserNotificationsFragment.setArguments(mBundle);
                mHomeActivity.replace(mUserNotificationsFragment, mHomeActivity.getResources().getString(R.string.tag_agent_notification_list));

            }
        });
        mHomeActivity.toolbar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                isAgentProfileFragment = false;
                AgentProfileFilterFragment mAgentProfileFilterFragment = new AgentProfileFilterFragment();
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AgentProfileFilterFragment.KEY_IS_OPEATOR, isUserOperator);
                mAgentProfileFilterFragment.setArguments(mBundle);
                mHomeActivity.replace(mAgentProfileFilterFragment, mHomeActivity.getResources().getString(R.string.tag_filter_list_agent_profile));
            }
        });
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
    }


    /**
     * @param authToken
     * @param tenantId
     * @param userId
     */
    void getAccessibleTenantLocation(final String authToken, final int tenantId, String userId){

        Call mCall = apiInterface.getAccessibleTenantLocationForAgentProfile(tenantId, userId, "name", "ASC", "Normal", false);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile = (AccessibleTenantLocationDataModelForAgentProfile) response.body();

                    if((mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile != null &&
                            (mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile.getData() != null && (mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile.getData().getAllLocation() != null && mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile.getData().getAllLocation().size() > 0)))){

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile.getData().getAllLocation().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile.getData().getAllLocation().get(0).getName();
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_location));
                        mFilterBaseData.filterIndex = 0;
                        mFilterBaseData.isSelectedPosition = true;
                        mHomeActivity.mFilterBaseDatas.add(mFilterBaseData);
                        mHomeActivity.tenantLocationId = mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile.getData().getAllLocation().get(0).getId();
                        mHomeActivity.tenantLocationLocationName = mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile.getData().getAllLocation().get(0).getName();
                        mHomeActivity.isLocationAvailable = true;
                        getisUserCompare(String.valueOf(mHomeActivity.tenantLocationId), mHomeActivity.userIdForAPI, false);
                    } else{
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(0);
                        mFilterBaseData.name = "";
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_location));
                        mFilterBaseData.filterIndex = 0;
                        mFilterBaseData.isSelectedPosition = true;
                        mHomeActivity.mFilterBaseDatas.add(mFilterBaseData);
                        mHomeActivity.tenantLocationId = 0;
                        mHomeActivity.tenantLocationLocationName = "";
                        mHomeActivity.isLocationAvailable = false;
                        getisUserCompare(String.valueOf(mHomeActivity.tenantLocationId), mHomeActivity.userIdForAPI, false);
                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });
    }


    void getisUserCompare(final String TenantLocationId, String userId, final boolean isNeedOnlyOperatorCheck){

        int tenantId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1);
        Call<ResponseBody> mCall = apiInterface.getIsOperator(tenantId, TenantLocationId, userId);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                if(response.body() != null){
                    try{

                        String jsonResponse = response.body().string();
                        Logger.Error("<<< Response  >>>>> " + jsonResponse);
                        JSONObject mJsonObject = new JSONObject(jsonResponse);
                        if(mJsonObject.optBoolean("data")){
                            if(!isNeedOnlyOperatorCheck){
                                getDashboardUserList(mHomeActivity.tenantLocationId);
                            } else{
                                getAgentGamificationDetails();
                            }
                            isUserOperator = true;
                            mHomeActivity.isUserOperator = isUserOperator;
                        } else{
                            isUserOperator = false;
                            mHomeActivity.isUserOperator = isUserOperator;

                            mHomeActivity.endProgress();
                            if(!isNeedOnlyOperatorCheck){
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = mHomeActivity.userIdForAPI;
                                mFilterBaseData.name = userName;
                                mFilterBaseData.filterIndex = 1;
                                mFilterBaseData.isSelectedPosition = false;
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user));
                                mHomeActivity.mFilterBaseDatas.add(mFilterBaseData);
                            }

                            getAgentGamificationDetails();


                        }


                    } catch(Exception e){
                        e.printStackTrace();
                    }
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);*/
            }
        });
    }

    /**
     * @param TenantLocationId
     */
    void getDashboardUserList(int TenantLocationId){

        int tenantId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1);
        Call mCall = apiInterface.getDashboardUserListForAgentProfile(tenantId,
                false,
                false,
                false,
                true,
                false,
                TenantLocationId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){


                if(response.body() != null){
                    mHomeActivity.mDashboardUserListModelForAgentProfile = (DashboardUserListModel) response.body();
                    /*Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardUserListModelForLeaderBoard);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_obj), json);*/
                    if((mHomeActivity.mDashboardUserListModelForAgentProfile != null &&
                            (mHomeActivity.mDashboardUserListModelForAgentProfile.getData() != null &&
                                    mHomeActivity.mDashboardUserListModelForAgentProfile.getData().size() > 0))){

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = mHomeActivity.userIdForAPI;
                        mFilterBaseData.name = userName;
                        mFilterBaseData.filterIndex = 1;
                        mFilterBaseData.isSelectedPosition = false;
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user));
                        mHomeActivity.mFilterBaseDatas.add(mFilterBaseData);
                        /*mFilterBaseData.id = String.valueOf(mDashboardUserListModel.getData().get(0).getId());
                        mFilterBaseData.name = mDashboardUserListModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 1;*/
                        getAgentGamificationDetails();

//                        getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    } else{
                        //                      getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();

                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);*/
            }
        });
    }

    /**
     * get the Industry List
     */
    void getUserRoleAndTeneantList(int userId, final boolean isFromLeaderBoard){
        Call mCall = apiInterface.getUserRoleAndTenantList(String.valueOf(userId));
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    UserRoleTenantListModel userRoleTenantListModel = (UserRoleTenantListModel) response.body();
                    if(userRoleTenantListModel.getData() != null && userRoleTenantListModel.getData().size() > 0){

                        Logger.Error(" <<<<<   ROLE ID   >>>>>>" + userRoleTenantListModel.getData().get(0).getRoleId());
                        String tenantRoleName = userRoleTenantListModel.getData().get(0).getRoleName();
                        mHomeActivity.userRoleName = tenantRoleName;
                        //InGaugeSession.write(getString(R.string.key_login_tenant_user_role), userRoleTenantListModel.getData().get(0).getIndustryDescription());
                        tvAgentRole.setText(mHomeActivity.userRoleName );
                    } else{

                    }
                }
                if(isFromLeaderBoard){
                    getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1),
                            mHomeActivity.userIdForAPI);

                } else{
                    getAgentGamificationDetails();
                }
//                getisUserCompare(String.valueOf(mHomeActivity.tenantLocationId), mHomeActivity.userIdForAPI,true);

            }

            @Override
            public void onFailure(Call call, Throwable t){

            }
        });
    }

}
