package com.ingauge.fragments.dateselection;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.fragments.dateselection.tabsfragments.CustomFragmentForObservationCC;
import com.ingauge.fragments.dateselection.tabsfragments.DayFragmentForObs;
import com.ingauge.fragments.dateselection.tabsfragments.MonthFragmentForObs;
import com.ingauge.fragments.dateselection.tabsfragments.WeekFragmentForObs;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 20-Feb-18.
 */

public class DateSelectionFragmentForObsCC extends BaseFragment{

    public static String KEY_IS_FROM_LEADERBOARD = "is_from_leaderboard";

    public static String KEY_START_DATE = "key_start_date";
    public static String KEY_END_DATE = "key_end_date";
    public static String KEY_COMPARE_START_DATE = "key_compare_start_date";
    public static String KEY_COMPARE_END_DATE = "key_compare_end_date";


    public static String KEY_SERVER_START_DATE = "key_server_start_date";
    public static String KEY_SERVER_END_DATE = "key_server_end_date";
    public static String KEY_SERVER_COMPARE_START_DATE = "key_server_compare_start_date";
    public static String KEY_SERVER_COMPARE_END_DATE = "key_server_compare_end_date";
    public static String KEY_IS_COMPARE = "key_is_compare";


    View rootView;

    TabLayout mTabLayoutDate;
    ViewPager mViewPagerTab;
    ViewPagerAdapter adapter;
    HomeActivity mHomeActivity;

    /**
     * 1 =  Dashbooard Date Filter
     * 2 =  Leaderboard Date filter
     * 3 =  Observation Date Filter
     * 4 =  Counter Coaching Filter.
     */
    public int DateFilterIndex = -1;

    public String startDate = "";
    public String endDate = "";
    public String compareStartDate = "";
    public String compareEndDate = "";

    //Server Dates
    public String serverStartDate = "";
    public String serverEndDate = "";

    public String serverStartDateForCustom = "";
    public String serverEndDateForCustom = "";

    public String serverCompareStartDate = "";
    public String serverCompareEndDate = "";

    public boolean isCompare = true;


    /**
     * 0 = Day
     * 1 = Week
     * 2 = Month
     * 3 = Custom
     */
    public int tabIndex = -1;

    public int dateRangeIndex = -1;
    public int ComparedateRangeIndex = -1;

    boolean isFromLeaderBoard = false;


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        isCompare = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_is_compare_on), isCompare);
    }


    private Bundle getFragmentBundleforCustomTab(){

        Bundle mBundle = new Bundle();

        mBundle.putString(KEY_START_DATE, startDate);
        mBundle.putString(KEY_END_DATE, endDate);
        mBundle.putString(KEY_COMPARE_START_DATE, compareStartDate);
        mBundle.putString(KEY_COMPARE_END_DATE, compareEndDate);

        mBundle.putString(KEY_SERVER_START_DATE, serverStartDate);
        mBundle.putString(KEY_SERVER_END_DATE, serverEndDate);
        mBundle.putString(KEY_SERVER_COMPARE_START_DATE, serverCompareStartDate);
        mBundle.putString(KEY_SERVER_COMPARE_END_DATE, serverCompareEndDate);
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());

        return mBundle;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.date_selection_fragment, container, false);
        initViews(rootView);

        tabIndex = InGaugeSession.read(getString(R.string.key_tag_index_obs), 4);
        dateRangeIndex = InGaugeSession.read(getString(R.string.key_from_date_range_index_obs), 1);
        ComparedateRangeIndex = InGaugeSession.read(getString(R.string.key_compare_date_range_index), 1);

        if(getArguments() != null){
            isFromLeaderBoard = getArguments().getBoolean(KEY_IS_FROM_LEADERBOARD);
        }


        //mViewPagerTab.setOffscreenPageLimit(1);
        if(InGaugeSession.read(getString(R.string.key_is_compare_on), true)){
            isCompare = true;
        } else{
            isCompare = false;
        }

        setupViewPager(mViewPagerTab);

        mViewPagerTab.setCurrentItem(tabIndex);
        return rootView;
    }

    private void initViews(View itemView){
        mTabLayoutDate = (TabLayout) itemView.findViewById(R.id.date_selection_tabs);
        mViewPagerTab = (ViewPager) itemView.findViewById(R.id.date_selection_viewpager);
    }

    private void setupViewPager(ViewPager viewPager){
        adapter = new ViewPagerAdapter(getChildFragmentManager());

        DayFragmentForObs mDayFragment = new DayFragmentForObs();
        Bundle mBundle = new Bundle();
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());
        mBundle.putBoolean(KEY_IS_FROM_LEADERBOARD, true);
        mDayFragment.setArguments(mBundle);
        adapter.addFrag(mDayFragment, "Day");

        WeekFragmentForObs mWeekFragment = new WeekFragmentForObs();
        mBundle = new Bundle();
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());
        mBundle.putBoolean(KEY_IS_FROM_LEADERBOARD, true);
        mWeekFragment.setArguments(mBundle);
        adapter.addFrag(mWeekFragment, "Week");

        MonthFragmentForObs mMonthFragment = new MonthFragmentForObs();
        mBundle = new Bundle();
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());
        mBundle.putBoolean(KEY_IS_FROM_LEADERBOARD, true);
        mMonthFragment.setArguments(mBundle);
        adapter.addFrag(mMonthFragment, "Month");

        // Bundle mBundle = getFragmentBundleforCustomTab();
        CustomFragmentForObservationCC mCustomFragment = new CustomFragmentForObservationCC();
        mBundle = new Bundle();
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());
        mBundle.putBoolean(KEY_IS_FROM_LEADERBOARD, true);


        mCustomFragment.setArguments(mBundle);
        adapter.addFrag(mCustomFragment, "Custom");
        viewPager.setAdapter(adapter);
        mTabLayoutDate.setupWithViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter{
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager){
            super(manager);
        }

        @Override
        public Fragment getItem(int position){
            return mFragmentList.get(position);
        }

        @Override
        public int getCount(){
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position){
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.tvApply.setVisibility(View.VISIBLE);
        mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.user_profile_save));
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvTitle.setText("Date Filter");
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvApply.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                // Click on Save
                Fragment mFragment = (Fragment) adapter.getItem(mViewPagerTab.getCurrentItem());

                if(mFragment instanceof DayFragmentForObs){

                    setTabIndex(((DayFragmentForObs) mFragment).getTabIndex());
                    startDate = ((DayFragmentForObs) mFragment).getStartDate();
                    endDate = ((DayFragmentForObs) mFragment).getEndDate();


                    serverStartDate = ((DayFragmentForObs) mFragment).getServerStartDate();
                    serverEndDate = ((DayFragmentForObs) mFragment).getServerEndDate();


                    dateRangeIndex = ((DayFragmentForObs) mFragment).getDateRangeIndex();


                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter), startDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter), endDate);

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter_server), serverStartDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter_server), serverEndDate);

                    serverStartDateForCustom = ((DayFragmentForObs) mFragment).getServerStartDateForCustom();
                    serverEndDateForCustom = ((DayFragmentForObs) mFragment).getServerEndDateForCustom();
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter_server_for_custom), serverStartDateForCustom);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter_server_for_custom), serverEndDateForCustom);


                    InGaugeSession.write(getString(R.string.key_tag_index_obs), getTabIndex());
                    InGaugeSession.write(getString(R.string.key_from_date_range_index_obs), dateRangeIndex);

                    mHomeActivity.setDashboardObsCoachingUpdateAfterApply(true);
                    mHomeActivity.onBackPressed();
                } else if(mFragment instanceof WeekFragmentForObs){
                    setTabIndex(((WeekFragmentForObs) mFragment).getTabIndex());
                    startDate = ((WeekFragmentForObs) mFragment).getStartDate();

                    serverStartDate = ((WeekFragmentForObs) mFragment).getServerStartDate();
                    serverEndDate = ((WeekFragmentForObs) mFragment).getServerEndDate();

                    dateRangeIndex = ((WeekFragmentForObs) mFragment).getDateRangeIndex();


                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter), startDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter), "");

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter_server), serverStartDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter_server), serverEndDate);


                    serverStartDateForCustom = ((WeekFragmentForObs) mFragment).getServerStartDateForCustom();
                    serverEndDateForCustom = ((WeekFragmentForObs) mFragment).getServerEndDateForCustom();
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter_server_for_custom), serverStartDateForCustom);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter_server_for_custom), serverEndDateForCustom);

                    InGaugeSession.write(getString(R.string.key_tag_index_obs), getTabIndex());
                    InGaugeSession.write(getString(R.string.key_from_date_range_index_obs), dateRangeIndex);
                    mHomeActivity.setDashboardObsCoachingUpdateAfterApply(true);

                    mHomeActivity.onBackPressed();

                } else if(mFragment instanceof MonthFragmentForObs){
                    setTabIndex(((MonthFragmentForObs) mFragment).getTabIndex());
                    startDate = ((MonthFragmentForObs) mFragment).getStartDate();


                    serverStartDate = ((MonthFragmentForObs) mFragment).getServerStartDate();
                    serverEndDate = ((MonthFragmentForObs) mFragment).getServerEndDate();


                    dateRangeIndex = ((MonthFragmentForObs) mFragment).getDateRangeIndex();
                    isCompare = ((MonthFragmentForObs) mFragment).isCompare();


                    /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), startDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), "");

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), serverStartDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), serverEndDate);
*/
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter), startDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter), "");

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter_server), serverStartDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter_server), serverEndDate);

                    serverStartDateForCustom = ((MonthFragmentForObs) mFragment).getServerStartDateForCustom();
                    serverEndDateForCustom = ((MonthFragmentForObs) mFragment).getServerEndDateForCustom();

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter_server_for_custom), serverStartDateForCustom);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter_server_for_custom), serverEndDateForCustom);

                    InGaugeSession.write(getString(R.string.key_tag_index_obs), getTabIndex());
                    InGaugeSession.write(getString(R.string.key_from_date_range_index_obs), dateRangeIndex);

                    mHomeActivity.setDashboardObsCoachingUpdateAfterApply(true);
                    mHomeActivity.onBackPressed();

                } else if(mFragment instanceof CustomFragmentForObservationCC){

                    setTabIndex(((CustomFragmentForObservationCC) mFragment).getTabIndex());
                    startDate = ((CustomFragmentForObservationCC) mFragment).getStartDate();
                    endDate = ((CustomFragmentForObservationCC) mFragment).getEndDate();

                    serverStartDate = ((CustomFragmentForObservationCC) mFragment).getServerStartDate();
                    serverEndDate = ((CustomFragmentForObservationCC) mFragment).getServerEndDate();

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter), startDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter), endDate);

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter_server), serverStartDate);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter_server), serverEndDate);

                    serverStartDateForCustom = ((CustomFragmentForObservationCC) mFragment).getServerStartDateForCustom();
                    serverEndDateForCustom = ((CustomFragmentForObservationCC) mFragment).getServerEndDateForCustom();

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_observation_date_filter_server_for_custom), serverStartDateForCustom);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_observation_date_filter_server_for_custom), serverEndDateForCustom);

                    InGaugeSession.write(getString(R.string.key_tag_index_obs), getTabIndex());
                    InGaugeSession.write(getString(R.string.key_from_date_range_index_obs), 1);

                    mHomeActivity.setDashboardObsCoachingUpdateAfterApply(true);
                    mHomeActivity.onBackPressed();


                }

                Logger.Error("<<<<< Click on Save >>>>>>   " + getTabIndex());
                Logger.Error("<<<<< Click Date Range Index  >>>>>>   " + getDateRangeIndex());
                Logger.Error("<<<<< Click Compare Date Range Index  >>>>>>   " + getComparedateRangeIndex());

            }
        });


    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
    }

    public int getDateFilterIndex(){
        return DateFilterIndex;
    }

    public String getStartDate(){
        return startDate;
    }

    public String getEndDate(){
        return endDate;
    }

    public String getCompareStartDate(){
        return compareStartDate;
    }

    public String getCompareEndDate(){
        return compareEndDate;
    }

    public boolean isCompare(){
        return isCompare;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public int getDateRangeIndex(){
        return dateRangeIndex;
    }

    public int getComparedateRangeIndex(){
        return ComparedateRangeIndex;
    }

    public String getServerStartDate(){
        return serverStartDate;
    }

    public String getServerEndDate(){
        return serverEndDate;
    }

    public String getServerCompareStartDate(){
        return serverCompareStartDate;
    }

    public String getServerCompareEndDate(){
        return serverCompareEndDate;
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }

    public String getServerStartDateForCustom(){
        return serverStartDateForCustom;
    }

    public void setServerStartDateForCustom(String serverStartDateForCustom){
        this.serverStartDateForCustom = serverStartDateForCustom;
    }

    public String getServerEndDateForCustom(){
        return serverEndDateForCustom;
    }

    public void setServerEndDateForCustom(String serverEndDateForCustom){
        this.serverEndDateForCustom = serverEndDateForCustom;
    }


}
