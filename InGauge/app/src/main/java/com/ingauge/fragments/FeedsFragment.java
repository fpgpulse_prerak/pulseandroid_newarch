package com.ingauge.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.FeedsListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.EndlessRecyclerViewScrollListener;
import com.ingauge.listener.OnFeedLikeListener;
import com.ingauge.pojo.FeedsModelList;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 15-May-17.
 */

public class FeedsFragment extends BaseFragment{

    //    private static final String ARG_TEXT = "arg_text";
//    private static final String ARG_COLOR = "arg_color";
    private RecyclerView rvFeedsList;
    private HomeActivity mHomeActivity;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout rlProgress;
    APIInterface apiInterface;
    Call mCall;
    OnFeedLikeListener onFeedLikeListener;
    public FeedsModelList mFeedsModelList;
    private TextView tvNoData;
    private LinearLayout llNoFeeds;
    String cryEmoji = "";
    Context mContext;
    int pos;
    FeedsCommentFragment f = new FeedsCommentFragment();
    public static final String KEY_FEED_MODEL_LIST = "feed_model_list";
    public static final String KEY_SELECTED_POSITION = "selected_position";
    FeedsListAdapter mFeedsListAdapter;

    FeedsCommentReceiver feedsCommentReceiver;
    LinearLayoutManager mLayoutManager;
    long start = 0;
    long pagesize = 5;

    public boolean isNeedLoadMore = true;
    List<FeedsModelList.Datum> mFeedsListDatabase = new ArrayList<>();


    boolean isGuideEnableFromLogin = false;
    boolean isGuideEnableFromChangetenant = false;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
        start = 0;

    }

  /*  public void UpdateComment(String comment, String name, int userId, int position) {
        Logger.Error("FROM INERFACE" + mFeedsModelList.getData().size());
        FeedsModelList.FeedComment feedComment = (FeedsModelList.FeedComment) mFeedsModelList.getData().get(position).getFeedComments();
        feedComment.setText(comment);
        feedComment.getUser().setId(userId);
        feedComment.getUser().setName(name);
        mFeedsModelList.getData().get(position).getFeedComments().add(feedComment);
        mFeedsListAdapter.notifyDataSetChanged();

    }*/

    public static Fragment newInstance(String text, int color){
        Fragment frag = new FeedsFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
//        args.putString(ARG_TEXT, text);
//        args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        cryEmoji = SpannableString.valueOf(Html.fromHtml("\ud83d\ude12")).toString();
        isGuideEnableFromLogin = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_guide_enabled), false);
        isGuideEnableFromChangetenant = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_guide_enabled_from_preference), false);
     /*  f.setOnDismissListener(new DialogInterface.OnDismissListener() {
           @Override
           public void onDismiss(DialogInterface dialog) {

           }
       });*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.feeds_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        rvFeedsList = (RecyclerView) view.findViewById(R.id.rv_feeds_list);
        mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        rvFeedsList.setLayoutManager(mLayoutManager);
        tvNoData = (TextView) view.findViewById(R.id.tv_nodata);
        rlProgress = (RelativeLayout) view.findViewById(R.id.custom_progress_rl_main);
        rvFeedsList.setVisibility(View.GONE);
        rlProgress.setVisibility(View.VISIBLE);
        llNoFeeds = (LinearLayout)view.findViewById(R.id.feeds_fragment_ll_placeholder);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        //mSwipeRefreshLayout.setVisibility(View.INVISIBLE);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.in_guage_blue,
                R.color.rv_grey_color,
                R.color.red);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh(){
                // Refresh items
                refreshItems(mFeedsModelList.getData());
            }
        });
        if(mFeedsModelList == null){
            FeedsAPI(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0), start, pagesize, false);
        } else{
            if(mFeedsListAdapter != null){
                refreshItems(mFeedsModelList.getData());

            } else{
                rlProgress.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
                llNoFeeds.setVisibility(View.VISIBLE);
                tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_feed_nodata)) + cryEmoji);
            }

        }


        rvFeedsList.addOnScrollListener(new RecyclerView.OnScrollListener(){

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                super.onScrolled(recyclerView, dx, dy);

                if(!isNeedLoadMore)
                    return;
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                if(pastVisibleItems + visibleItemCount >= totalItemCount){

                    start = start + 5;
                    mHomeActivity.mProgressBarWhite.setVisibility(View.VISIBLE);
                    Logger.Error("<<<<< Feids Page  " + start + "  Total Items Count ");
                    /*rvFeedsList.post(new Runnable(){
                        @Override
                        public void run(){
                            try{*/
                    mFeedsListAdapter.showLoading(true);
                    mFeedsListAdapter.notifyDataSetChanged();
                      /*      } catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                    });*/


                    FeedsAPI(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0), start, pagesize, true);

                    /*} else{
                        mFeedsListAdapter.showLoading(false);
                        mFeedsListAdapter.notifyDataSetChanged();
                    }*/
                }


            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState){
                super.onScrollStateChanged(recyclerView, newState);

            }
        });
        rvFeedsList.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLayoutManager){
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view){


            }
        });

    }


    @Override
    public void onDetach(){
        super.onDetach();

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
    }

    private void FeedsAPI(String authToken, int tenantId, long start, long pagesize, final boolean isFromLoadMore){
        if(isFromLoadMore){
            mHomeActivity.mProgressBarWhite.setVisibility(View.VISIBLE);
        }
        mCall = apiInterface.getfeedsList(tenantId, start, pagesize);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){


                rvFeedsList.setVisibility(View.VISIBLE);
                rlProgress.setVisibility(View.GONE);


                if(response.body() != null){
                    mFeedsModelList = (FeedsModelList) response.body();
                    if((mFeedsModelList != null) && ((mFeedsModelList.getData() != null && mFeedsModelList.getData().size() > 0))){
                        mFeedsListDatabase.addAll(mFeedsModelList.getData());

                        setFeedsAdapter(mFeedsListDatabase, isFromLoadMore);
                        rvFeedsList.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                        llNoFeeds.setVisibility(View.GONE);
                    } else{
                        if(mFeedsListAdapter != null){
                            mFeedsListAdapter.showLoading(false);
                            mFeedsListAdapter.notifyDataSetChanged();
                            mHomeActivity.mProgressBarWhite.setVisibility(View.GONE);
                            if(isFromLoadMore){
                                mFeedsListAdapter.showLoading(false);
                                mFeedsListAdapter.notifyDataSetChanged();
                                isNeedLoadMore = false;
                            } else{
                                mFeedsListAdapter.showLoading(false);
                                mFeedsListAdapter.notifyDataSetChanged();
                                isNeedLoadMore = false;
                                tvNoData.setVisibility(View.VISIBLE);
                                llNoFeeds.setVisibility(View.VISIBLE);
                                rvFeedsList.setVisibility(View.GONE);

                                //tvNoData.setText(getString(R.string.tv_no_feeds) + cryEmoji);
                                tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_feed_nodata)) + cryEmoji);
                            }
                        } else{
                            isNeedLoadMore = false;
                            tvNoData.setVisibility(View.VISIBLE);
                            llNoFeeds.setVisibility(View.VISIBLE);
                            rvFeedsList.setVisibility(View.GONE);
                            //tvNoData.setText(getString(R.string.tv_no_feeds) + cryEmoji);
                            tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_feed_nodata)) + cryEmoji);
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                Logger.Error("Exception " + t.getMessage());
                try{
                    if(mFeedsListAdapter != null){
                        mFeedsListAdapter.showLoading(false);
                        mFeedsListAdapter.notifyDataSetChanged();
                    }
                    tvNoData.setVisibility(View.VISIBLE);
                    llNoFeeds.setVisibility(View.VISIBLE);
                    rvFeedsList.setVisibility(View.GONE);
                    tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_feed_nodata)) + cryEmoji);
                    rlProgress.setVisibility(View.GONE);
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        });

    }


    private void setFeedsAdapter(List<FeedsModelList.Datum> mFeedsListData, final boolean isFromLoadMore){
        mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.menu_feeds));

        rvFeedsList.setItemAnimator(new DefaultItemAnimator());
        mFeedsListAdapter = new FeedsListAdapter(InGaugeApp.getInstance(), mFeedsListData, mHomeActivity, this);
        if(isFromLoadMore){
            mHomeActivity.mProgressBarWhite.setVisibility(View.GONE);
            mFeedsListAdapter.showLoading(false);
            mFeedsListAdapter.notifyDataSetChanged();
        } else{
            rvFeedsList.setAdapter(mFeedsListAdapter);
        }

    }

    void refreshItems(List<FeedsModelList.Datum> mFeedsListData){
        FeedsAPI(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0), 0, pagesize, false);
        // mFeedsListAdapter = new FeedsListAdapter(InGaugeApp.getInstance(), mFeedsListData, mHomeActivity,this);
        // rvFeedsList.setAdapter(mFeedsListAdapter);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.visibleBottomView();

        mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.menu_feeds));
        mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
        mHomeActivity.tvSubTitle.setText(InGaugeSession.read(getString(R.string.key_selected_tenant), ""));
        feedsCommentReceiver = new FeedsCommentReceiver();
        IntentFilter intentFilter = new IntentFilter(FeedsCommentFragment.KEY_FEEDS_BROADCAST);
        mHomeActivity.registerReceiver(feedsCommentReceiver, intentFilter);
        mHomeActivity.toolbar.setClickable(false);
        Logger.Error("<< Call >>>");

        new Handler().postDelayed(new Runnable(){

            @Override
            public void run(){
                try{

                    mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.menu_feeds));
                    mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                    mHomeActivity.tvSubTitle.setText(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant), ""));
                    mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);


                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        }, 2500);


        if(isGuideEnableFromLogin){
            InGaugeSession.write(getString(R.string.key_guide_enabled), false);
            showTenantGuide();

        } else if(isGuideEnableFromChangetenant){
            InGaugeSession.write(getString(R.string.key_guide_enabled_from_preference), false);
            showDashboardFilterGuide();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        mHomeActivity.unregisterReceiver(feedsCommentReceiver);
        feedsCommentReceiver = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mHomeActivity.mProgressBarWhite.setVisibility(View.GONE);
        if(mCall != null)
            mCall.cancel();
    }

    private class FeedsCommentReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent){

            //verify if the message we received is the one that we want
            if(intent.getAction().equals(FeedsCommentFragment.KEY_FEEDS_BROADCAST)){

                Bundle bundle = intent.getExtras();
                FeedsModelList feedModellist = (FeedsModelList) bundle.getSerializable(FeedsCommentFragment.KEY_FEEDS_DATA);
                mFeedsModelList = feedModellist;
                mFeedsListAdapter.notifyDataSetChanged();

            }

        }
    }


}

