package com.ingauge.fragments.dateselection;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.fragments.dateselection.tabsfragments.CustomFragment;
import com.ingauge.fragments.dateselection.tabsfragments.DayFragment;
import com.ingauge.fragments.dateselection.tabsfragments.MonthFragment;
import com.ingauge.fragments.dateselection.tabsfragments.WeekFragment;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 20-Feb-18.
 */

public class DateSelectionFragment extends BaseFragment{

    public static String KEY_IS_FROM_LEADERBOARD = "is_from_leaderboard";
    public static String KEY_IS_HIDE_COMPARE= "is_hide_compare";

    public static String KEY_START_DATE = "key_start_date";
    public static String KEY_END_DATE = "key_end_date";
    public static String KEY_COMPARE_START_DATE = "key_compare_start_date";
    public static String KEY_COMPARE_END_DATE = "key_compare_end_date";


    public static String KEY_SERVER_START_DATE = "key_server_start_date";
    public static String KEY_SERVER_END_DATE = "key_server_end_date";
    public static String KEY_SERVER_COMPARE_START_DATE = "key_server_compare_start_date";
    public static String KEY_SERVER_COMPARE_END_DATE = "key_server_compare_end_date";
    public static String KEY_IS_COMPARE = "key_is_compare";


    public static String KEY_DATE_RANGE_INDEX="key_date_range_index";
    public static String KEY_COMPARE_DATE_RANGE_INDEX="key_compare_date_range_index";

    View rootView;

    TabLayout mTabLayoutDate;
    ViewPager mViewPagerTab;
    ViewPagerAdapter adapter;
    HomeActivity mHomeActivity;

    /**
     * 1 =  Dashbooard Date Filter
     * 2 =  Leaderboard Date filter
     * 3 =  Observation Date Filter
     * 4 =  Counter Coaching Filter.
     */
    public int DateFilterIndex = -1;

    public String startDate = "";
    public String endDate = "";
    public String compareStartDate = "";
    public String compareEndDate = "";

    //Server Dates
    public String serverStartDate = "";
    public String serverEndDate = "";
    public String serverCompareStartDate = "";
    public String serverCompareEndDate = "";

    public boolean isCompare = true;


    /**
     * 0 = Day
     * 1 = Week
     * 2 = Month
     * 3 = Custom
     */
    public int tabIndex = -1;

    public int dateRangeIndex = -1;
    public int ComparedateRangeIndex = -1;

    boolean isFromLeaderBoard = false;
    boolean isHideCompare = false;


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        isCompare = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_is_compare_on), isCompare);
    }


    private Bundle getFragmentBundleforCustomTab(){

        Bundle mBundle = new Bundle();

        mBundle.putString(KEY_START_DATE, startDate);
        mBundle.putString(KEY_END_DATE, endDate);
        mBundle.putString(KEY_COMPARE_START_DATE, compareStartDate);
        mBundle.putString(KEY_COMPARE_END_DATE, compareEndDate);

        mBundle.putString(KEY_SERVER_START_DATE, serverStartDate);
        mBundle.putString(KEY_SERVER_END_DATE, serverEndDate);
        mBundle.putString(KEY_SERVER_COMPARE_START_DATE, serverCompareStartDate);
        mBundle.putString(KEY_SERVER_COMPARE_END_DATE, serverCompareEndDate);
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());

        return mBundle;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.date_selection_fragment, container, false);
        initViews(rootView);

        tabIndex = InGaugeSession.read(getString(R.string.key_tag_index), 4);
        dateRangeIndex = InGaugeSession.read(getString(R.string.key_from_date_range_index), 1);
        ComparedateRangeIndex = InGaugeSession.read(getString(R.string.key_compare_date_range_index), 1);

        if(getArguments() != null){
            isFromLeaderBoard = getArguments().getBoolean(KEY_IS_FROM_LEADERBOARD);
            isHideCompare = getArguments().getBoolean(KEY_IS_HIDE_COMPARE);
        }



        //mViewPagerTab.setOffscreenPageLimit(1);
        if(InGaugeSession.read(getString(R.string.key_is_compare_on), true)){
            isCompare = true;
        } else{
            isCompare = false;
        }

        setupViewPager(mViewPagerTab);

        mViewPagerTab.setCurrentItem(tabIndex);
        return rootView;
    }

    private void initViews(View itemView){
        mTabLayoutDate = (TabLayout) itemView.findViewById(R.id.date_selection_tabs);
        mViewPagerTab = (ViewPager) itemView.findViewById(R.id.date_selection_viewpager);
    }

    private void setupViewPager(ViewPager viewPager){
        adapter = new ViewPagerAdapter(getChildFragmentManager());

        DayFragment mDayFragment = new DayFragment();
        Bundle mBundle = new Bundle();
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());
        mBundle.putBoolean(KEY_IS_FROM_LEADERBOARD, isFromLeaderBoard);
        mBundle.putBoolean(KEY_IS_HIDE_COMPARE, isHideCompare);
        mDayFragment.setArguments(mBundle);
        adapter.addFrag(mDayFragment, "Day");

        WeekFragment mWeekFragment = new WeekFragment();
        mBundle = new Bundle();
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());
        mBundle.putBoolean(KEY_IS_FROM_LEADERBOARD, isFromLeaderBoard);
        mBundle.putBoolean(KEY_IS_HIDE_COMPARE, isHideCompare);
        mWeekFragment.setArguments(mBundle);
        adapter.addFrag(mWeekFragment, "Week");

        MonthFragment mMonthFragment = new MonthFragment();
        mBundle = new Bundle();
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());
        mBundle.putBoolean(KEY_IS_FROM_LEADERBOARD, isFromLeaderBoard);
        mBundle.putBoolean(KEY_IS_HIDE_COMPARE, isHideCompare);
        mMonthFragment.setArguments(mBundle);
        adapter.addFrag(mMonthFragment, "Month");

        // Bundle mBundle = getFragmentBundleforCustomTab();
        CustomFragment mCustomFragment = new CustomFragment();
        mBundle = new Bundle();
        mBundle.putBoolean(KEY_IS_COMPARE, isCompare());
        mBundle.putBoolean(KEY_IS_FROM_LEADERBOARD, isFromLeaderBoard);
        mBundle.putBoolean(KEY_IS_HIDE_COMPARE, isHideCompare);
        mCustomFragment.setArguments(mBundle);
        adapter.addFrag(mCustomFragment, "Custom");
        viewPager.setAdapter(adapter);
        mTabLayoutDate.setupWithViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter{
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager){
            super(manager);
        }

        @Override
        public Fragment getItem(int position){
            return mFragmentList.get(position);
        }

        @Override
        public int getCount(){
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position){
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.tvApply.setVisibility(View.VISIBLE);
        mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.user_profile_save));
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvTitle.setText("Date Filter");
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvApply.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                // Click on Save
                Fragment mFragment = (Fragment) adapter.getItem(mViewPagerTab.getCurrentItem());

                if(mFragment instanceof DayFragment){

                    setTabIndex(((DayFragment) mFragment).getTabIndex());
                    startDate = ((DayFragment) mFragment).getStartDate();
                    endDate = ((DayFragment) mFragment).getEndDate();
                    compareStartDate = ((DayFragment) mFragment).getCompareStartDate();
                    compareEndDate = ((DayFragment) mFragment).getCompareEndDate();

                    serverStartDate = ((DayFragment) mFragment).getServerStartDate();
                    serverEndDate = ((DayFragment) mFragment).getServerEndDate();

                    serverCompareStartDate = ((DayFragment) mFragment).getServerCompareStartDate();
                    serverCompareEndDate = ((DayFragment) mFragment).getServerCompareEndDate();
                    dateRangeIndex = ((DayFragment) mFragment).getDateRangeIndex();
                    isCompare = ((DayFragment) mFragment).isCompare();
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_compare_on), isCompare);
                    if(isCompare()){
                        ComparedateRangeIndex = ((DayFragment) mFragment).getComparedateRangeIndex();
                    }

                    if(isFromLeaderBoard){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), startDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), endDate);

                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), serverStartDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), serverEndDate);
                        mHomeActivity.setLeaderNeedUpdate(true);

                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date), startDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date), endDate);

                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_server), serverStartDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server), serverEndDate);
                        if(isCompare()){
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_start_date), compareStartDate);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_end_date), compareEndDate);

                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_start_date_server), serverCompareStartDate);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_end_date_server), serverCompareEndDate);
                        }
                        mHomeActivity.setDashboardUpdateAfterApply(true);
                    }


                    InGaugeSession.write(getString(R.string.key_tag_index), getTabIndex());
                    InGaugeSession.write(getString(R.string.key_from_date_range_index), dateRangeIndex);
                    InGaugeSession.write(getString(R.string.key_compare_date_range_index), ComparedateRangeIndex);

                    mHomeActivity.onBackPressed();
                } else if(mFragment instanceof WeekFragment){
                    setTabIndex(((WeekFragment) mFragment).getTabIndex());
                    startDate = ((WeekFragment) mFragment).getStartDate();

                    compareStartDate = ((WeekFragment) mFragment).getCompareStartDate();


                    serverStartDate = ((WeekFragment) mFragment).getServerStartDate();
                    serverEndDate = ((WeekFragment) mFragment).getServerEndDate();

                    serverCompareStartDate = ((WeekFragment) mFragment).getServerCompareStartDate();
                    serverCompareEndDate = ((WeekFragment) mFragment).getServerCompareEndDate();
                    dateRangeIndex = ((WeekFragment) mFragment).getDateRangeIndex();
                    isCompare = ((WeekFragment) mFragment).isCompare();
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_compare_on), isCompare);
                    if(isCompare()){
                        ComparedateRangeIndex = ((WeekFragment) mFragment).getComparedateRangeIndex();
                    }


                    if(isFromLeaderBoard){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), startDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), "");

                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), serverStartDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), serverEndDate);
                        mHomeActivity.setLeaderNeedUpdate(true);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date), startDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date), "");

                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_server), serverStartDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server), serverEndDate);
                        if(isCompare()){
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_start_date), compareStartDate);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_end_date), "");

                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_start_date_server), serverCompareStartDate);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_end_date_server), serverCompareEndDate);
                        }
                        mHomeActivity.setDashboardUpdateAfterApply(true);
                    }


                    InGaugeSession.write(getString(R.string.key_tag_index), getTabIndex());
                    InGaugeSession.write(getString(R.string.key_from_date_range_index), dateRangeIndex);
                    InGaugeSession.write(getString(R.string.key_compare_date_range_index), ComparedateRangeIndex);

                    mHomeActivity.onBackPressed();

                } else if(mFragment instanceof MonthFragment){
                    setTabIndex(((MonthFragment) mFragment).getTabIndex());
                    startDate = ((MonthFragment) mFragment).getStartDate();

                    compareStartDate = ((MonthFragment) mFragment).getCompareStartDate();


                    serverStartDate = ((MonthFragment) mFragment).getServerStartDate();
                    serverEndDate = ((MonthFragment) mFragment).getServerEndDate();

                    serverCompareStartDate = ((MonthFragment) mFragment).getServerCompareStartDate();
                    serverCompareEndDate = ((MonthFragment) mFragment).getServerCompareEndDate();
                    dateRangeIndex = ((MonthFragment) mFragment).getDateRangeIndex();
                    isCompare = ((MonthFragment) mFragment).isCompare();
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_compare_on), isCompare);
                    if(isCompare()){
                        ComparedateRangeIndex = ((MonthFragment) mFragment).getComparedateRangeIndex();
                    }


                    if(isFromLeaderBoard){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), startDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), "");

                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), serverStartDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), serverEndDate);
                        mHomeActivity.setLeaderNeedUpdate(true);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date), startDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date), "");

                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_server), serverStartDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server), serverEndDate);
                        if(isCompare()){
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_start_date), compareStartDate);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_end_date), "");

                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_start_date_server), serverCompareStartDate);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_end_date_server), serverCompareEndDate);
                        }
                        mHomeActivity.setDashboardUpdateAfterApply(true);
                    }


                    InGaugeSession.write(getString(R.string.key_tag_index), getTabIndex());
                    InGaugeSession.write(getString(R.string.key_from_date_range_index), dateRangeIndex);
                    InGaugeSession.write(getString(R.string.key_compare_date_range_index), ComparedateRangeIndex);

                    mHomeActivity.onBackPressed();

                } else if(mFragment instanceof CustomFragment){
                    setTabIndex(((CustomFragment) mFragment).getTabIndex());
                    startDate = ((CustomFragment) mFragment).getStartDate();
                    endDate = ((CustomFragment) mFragment).getEndDate();
                    compareStartDate = ((CustomFragment) mFragment).getCompareStartDate();
                    compareEndDate = ((CustomFragment) mFragment).getCompareEndDate();

                    serverStartDate = ((CustomFragment) mFragment).getServerStartDate();
                    serverEndDate = ((CustomFragment) mFragment).getServerEndDate();

                    serverCompareStartDate = ((CustomFragment) mFragment).getServerCompareStartDate();
                    serverCompareEndDate = ((CustomFragment) mFragment).getServerCompareEndDate();

                    isCompare = ((CustomFragment) mFragment).isCompare();
                    if(isCompare()){

                    }

                    if(isFromLeaderBoard){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), startDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), endDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), serverStartDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), serverEndDate);
                        mHomeActivity.setLeaderNeedUpdate(true);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date), startDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date), endDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_compare_on), isCompare);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_server), serverStartDate);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server), serverEndDate);
                        if(isCompare()){
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_start_date), compareStartDate);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_end_date), compareEndDate);

                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_start_date_server), serverCompareStartDate);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_compare_end_date_server), serverCompareEndDate);
                        }
                        mHomeActivity.setDashboardUpdateAfterApply(true);
                    }


                    InGaugeSession.write(getString(R.string.key_tag_index), getTabIndex());
                    InGaugeSession.write(getString(R.string.key_from_date_range_index), 1);
                    InGaugeSession.write(getString(R.string.key_compare_date_range_index), 1);

                    mHomeActivity.onBackPressed();


                }


                Logger.Error("<<<<< Click on Save >>>>>>   " + getTabIndex());

                Logger.Error("<<<<< Click Date Range Index  >>>>>>   " + getDateRangeIndex());

                Logger.Error("<<<<< Click Compare Date Range Index  >>>>>>   " + getComparedateRangeIndex());

            }
        });


    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
    }

    public int getDateFilterIndex(){
        return DateFilterIndex;
    }

    public String getStartDate(){
        return startDate;
    }

    public String getEndDate(){
        return endDate;
    }

    public String getCompareStartDate(){
        return compareStartDate;
    }

    public String getCompareEndDate(){
        return compareEndDate;
    }

    public boolean isCompare(){
        return isCompare;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public int getDateRangeIndex(){
        return dateRangeIndex;
    }

    public int getComparedateRangeIndex(){
        return ComparedateRangeIndex;
    }

    public String getServerStartDate(){
        return serverStartDate;
    }

    public String getServerEndDate(){
        return serverEndDate;
    }

    public String getServerCompareStartDate(){
        return serverCompareStartDate;
    }

    public String getServerCompareEndDate(){
        return serverCompareEndDate;
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }


}
