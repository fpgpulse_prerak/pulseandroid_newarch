package com.ingauge.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.ObservationSurveyQuestionAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.SurveyDiscardMessageEnable;
import com.ingauge.pojo.ObsQuestionModel;
import com.ingauge.pojo.ObsQuestionModelLocal;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by mansurum on 14-Mar-18.
 */

public class SurveyQuestionnaireFragment extends Fragment implements View.OnClickListener, SurveyDiscardMessageEnable{


    private HomeActivity mHomeActivity;
    View rootView;
    StickyListHeadersListView mStickyListHeadersListView;

    private ObservationSurveyQuestionAdapter mObservationSurveyQuestionAdapter;
    private Context mContext;
    public ObsQuestionModel mObsQuestionModel;
    private APIInterface apiInterface;

    private ObsQuestionModelLocal obsQuestionModelLocal;

    private ImageView ivTipsClose;
    private TextView tvTipsLabel;
    private LinearLayout llTips;
    private boolean isNeedtoShowDiscardAlerDialog = false;
    SurveyDiscardMessageEnable surveyDiscardMessageEnable;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        surveyDiscardMessageEnable = this;
        Bundle b = getArguments();
        if(b != null){
            mHomeActivity.isFromDashboard = b.getBoolean("isFromDashboard");
            mHomeActivity.isOnlyView = b.getBoolean("isOnlyView");
            mHomeActivity.isPending = b.getBoolean("isPending");
            mHomeActivity.donotMailToAgent = b.getBoolean("donotMailToAgent");
            mHomeActivity.donotDisplayToAgent = b.getBoolean("donotDisplayToAgent");
            mHomeActivity.areaOfImprovment = b.getString("areaOfImprovment");
            mHomeActivity.eventID = b.getString("eventID");
            //observationEditViewModel= (ObservationEditViewModel) getIntent().getSerializableExtra("obsEventDetail");
            mHomeActivity.observationEditViewModel = ObservationAgentFragment.observationEditViewModel;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_observation_survey_list, container, false);
        initView(rootView);
        initData();
        return rootView;
    }

    private void initView(View itemView){
        ivTipsClose = (ImageView) itemView.findViewById(R.id.tips_layout_for_obs_cc_iv_close);
        tvTipsLabel = (TextView) itemView.findViewById(R.id.tips_layout_for_obs_cc_tv_message);
        tvTipsLabel.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_ob_info_msg)));

        ivTipsClose.setOnClickListener(this);
        llTips = (LinearLayout) itemView.findViewById(R.id.fragment_observation_survey_list_ll_tips);
        mStickyListHeadersListView = (StickyListHeadersListView) itemView.findViewById(R.id.fragment_observation_survey_list_view);
        mStickyListHeadersListView.setFastScrollEnabled(false);

        if(mHomeActivity.isOnlyView){
            llTips.setVisibility(View.GONE);
        } else{
            llTips.setVisibility(View.VISIBLE);
        }
    }

    private void initData(){
        apiInterface = APIClient.getClient().create(APIInterface.class);

        if((!mHomeActivity.isFromDashboard && mHomeActivity.arrayObsQuestion == null) ||
                (mHomeActivity.isPending && mHomeActivity.arrayObsQuestion == null)){
            GetObsQuestions(InGaugeSession.read(getString(R.string.key_auth_token), ""), "application/json", String.valueOf(InGaugeSession.read(getString(R.string.key_selected_industry_id), 0)), true, InGaugeSession.read(getString(R.string.key_obs_selected_observation_id), "0"));
        } else if(mHomeActivity.isFromDashboard && mHomeActivity.arrayObsQuestion == null && !mHomeActivity.isPending){
            mHomeActivity.arrayObsQuestion = new ArrayList<ObsQuestionModelLocal>();
            for(int i = 0; i < mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().size(); i++){
                for(int j = 0; j < mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().size(); j++){
                    obsQuestionModelLocal = new ObsQuestionModelLocal();
                    obsQuestionModelLocal.setQ_title(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getName());
                    obsQuestionModelLocal.setQ_text(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getQuestionText());
                    obsQuestionModelLocal.setQ_yes_nona(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getType());
                    int AnswerId = mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getId();
                    String applicableType = UiUtils.AnswerNo();
                    for(int k = 0; k < mHomeActivity.observationEditViewModel.getData().getQuestionAnswer().size(); k++){
                        if(AnswerId == mHomeActivity.observationEditViewModel.getData().getQuestionAnswer().get(k).getQuestionId()){
                            applicableType = mHomeActivity.observationEditViewModel.getData().getQuestionAnswer().get(k).getAnswer();
                            Logger.Error("TRUE====>" + mHomeActivity.observationEditViewModel.getData().getQuestionAnswer().get(k).getId());
                            break;
                        }
                    }
                    obsQuestionModelLocal.setQ_yesnona_applicable(applicableType);
                    obsQuestionModelLocal.setQ_weight(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getWeight());
                    obsQuestionModelLocal.setQ_degree(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getDegreeOfImportance());
                    obsQuestionModelLocal.setQ_sym(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getSymptom());
                    obsQuestionModelLocal.setQ_id(String.valueOf(mHomeActivity.observationEditViewModel.getData().getQuestionnaireQuestionSections().get(i).getQuestions().get(j).getId()));
                    obsQuestionModelLocal.setSection(i + 1);
                    obsQuestionModelLocal.setQ_number(j + 1);

                    mHomeActivity.arrayObsQuestion.add(obsQuestionModelLocal);
                }

                mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score) + " " + mHomeActivity.observationEditViewModel.getData().getScoreVal() + " %");
                mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
                mObservationSurveyQuestionAdapter = new ObservationSurveyQuestionAdapter(mContext, mHomeActivity.arrayObsQuestion, mHomeActivity, this);
                mStickyListHeadersListView.setAdapter(mObservationSurveyQuestionAdapter);
            }
        } else{
            mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
            mObservationSurveyQuestionAdapter = new ObservationSurveyQuestionAdapter(mContext, mHomeActivity.arrayObsQuestion, mHomeActivity, this);
            mStickyListHeadersListView.setAdapter(mObservationSurveyQuestionAdapter);
        }

    }

    void GetObsQuestions(String authToken, String ContentType, String IndustryId, boolean isMobile, String observationId){
        Call mCall = apiInterface.GetObsQuestions(observationId);
        mHomeActivity.startProgress(mContext);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                mHomeActivity.endProgress();
                mObsQuestionModel = (ObsQuestionModel) response.body();
                mHomeActivity.arrayObsQuestion = new ArrayList<ObsQuestionModelLocal>();
                if(response.body() != null){
                    for(int i = 0; i < mObsQuestionModel.getData().getQuestionSections().size(); i++){
                        for(int j = 0; j < mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().size(); j++){
                            obsQuestionModelLocal = new ObsQuestionModelLocal();
                            obsQuestionModelLocal.setQ_title(mObsQuestionModel.getData().getQuestionSections().get(i).getName());
                            obsQuestionModelLocal.setQ_text(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getQuestionText());
                            obsQuestionModelLocal.setQ_yes_nona(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getType());
                            obsQuestionModelLocal.setQ_yesnona_applicable("No");
                            obsQuestionModelLocal.setQ_weight(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getWeight());
                            obsQuestionModelLocal.setQ_degree(mObsQuestionModel.getData().getQuestionSections().get(i).getDegreeOfImportance());
                            obsQuestionModelLocal.setQ_sym(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getSymptom());
                            obsQuestionModelLocal.setQ_id(String.valueOf(mObsQuestionModel.getData().getQuestionSections().get(i).getQuestions().get(j).getId()));
                            obsQuestionModelLocal.setSection(i + 1);
                            obsQuestionModelLocal.setQ_number(j + 1);
                            mHomeActivity.arrayObsQuestion.add(obsQuestionModelLocal);
                        }
                    }

                    mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.str_score) + " " + UiUtils.CountScore(mHomeActivity.arrayObsQuestion) + " %");
                    mHomeActivity.agentScore = UiUtils.CountScore(mHomeActivity.arrayObsQuestion);
                    mObservationSurveyQuestionAdapter = new ObservationSurveyQuestionAdapter(mContext, mHomeActivity.arrayObsQuestion, mHomeActivity, surveyDiscardMessageEnable);
                    mStickyListHeadersListView.setAdapter(mObservationSurveyQuestionAdapter);
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.endProgress();
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.tips_layout_for_obs_cc_iv_close:
                llTips.setVisibility(View.GONE);
                break;
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        float score = Float.parseFloat(mHomeActivity.agentScore);
        if(score > 0){
            mHomeActivity.tvTitle.setText("Score: " + mHomeActivity.agentScore);
        }


        mHomeActivity.tvTitle.setVisibility(View.VISIBLE);
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        if(mHomeActivity.isOnlyView){
            mHomeActivity.tvApply.setVisibility(View.GONE);
        } else{
            mHomeActivity.tvApply.setVisibility(View.VISIBLE);
        }

        mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.next));
        mHomeActivity.tvApply.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                mHomeActivity.replace(new ObservationStrengthFragment(), mHomeActivity.getResources().getString(R.string.tag_fragment_survey_strength_fragment));
            }
        });
        mHomeActivity.ibBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(isNeedtoShowDiscardAlerDialog){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mHomeActivity);
                    AlertDialog dialog;
                    builder.setTitle("");
                    builder.setMessage(mContext.getResources().getString(R.string.observation_survey_discard_message));
                    builder.setCancelable(false);
                    builder.setPositiveButton(mHomeActivity.getResources().getString(R.string.alert_yes),
                            new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){
                                    dialog.dismiss();
                                    mHomeActivity.onBackPressed();
                                }
                            });
                    builder.setNegativeButton(
                            mHomeActivity.getResources().getString(R.string.alert_no), new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){
                                    dialog.dismiss();
                                }
                            }
                    );
                    //  builder.show();
                    dialog = builder.create();
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        /*if(VerifyOnBackPressed()){

                        }*/
                    dialog.show();
                } else{
                    mHomeActivity.onBackPressed();
                }
            }
        });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mHomeActivity.endProgress();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event){

                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    if(keyCode == KeyEvent.KEYCODE_BACK){
                        if(isNeedtoShowDiscardAlerDialog){
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            AlertDialog dialog;
                            builder.setTitle("");
                            builder.setMessage(mContext.getResources().getString(R.string.observation_survey_discard_message));
                            builder.setCancelable(false);
                            builder.setPositiveButton(
                                    getActivity().getResources().getString(R.string.alert_yes),
                                    new DialogInterface.OnClickListener(){
                                        @Override
                                        public void onClick(DialogInterface dialog, int which){
                                            dialog.dismiss();
                                            mHomeActivity.onBackPressed();
                                        }
                                    });
                            builder.setNegativeButton(
                                    getActivity().getResources().getString(R.string.alert_no), new DialogInterface.OnClickListener(){
                                        @Override
                                        public void onClick(DialogInterface dialog, int which){
                                            dialog.dismiss();
                                        }
                                    }
                            );
                            //  builder.show();
                            dialog = builder.create();
                            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        /*if(VerifyOnBackPressed()){

                        }*/
                            dialog.show();
                            return true;
                        }

                    }
                }
                return false;
            }
        });
    }

    @Override
    public void enabledtoDiscardMessage(){
        isNeedtoShowDiscardAlerDialog = true;
    }
}
