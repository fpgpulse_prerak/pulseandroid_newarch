package com.ingauge.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.FilterListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.OnFilterDataSetListener;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by desainid on 5/22/2017.
 */

public class FilterListFragmentForGoal extends BaseFragment implements RecyclerViewClickListener, OnFilterDataSetListener, View.OnClickListener {
    public static final String KEY_FILTER_SELECTION = "filter_seclection";
    public static final String KEY_FILTER_DETAILS = "filter_details";
    public static final String KEY_SELECTION_POSITION = "seclected_position";
    Context mContext;
    RecyclerView rvFilterList;
    FilterListAdapter listadapter;
    List<String> values;
    ArrayList<FilterBaseData> mFilterBaseDatas;
    private HomeActivity mHomeActivity;
    APIInterface apiInterface;
    private RelativeLayout rlProgress;

    private Button mButtonApply;
    private Button mButtonClear;
    Call mCall;
    private RelativeLayout rlHorProgress;
    public boolean isLoaded = false;
    View rootView;
    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModelForGoal;
    private DashboardLocationGroupListModel mDashboardLocationGroupListModelForGoal;
    private ProductListFromLocationGroupModel mProductListFromLocationGroupModelForGoal;
    private DashboardUserListModel mDashboardUserListModelForGoal;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mFilterBaseDatas = new ArrayList<>();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_fliter_list, container, false);
        initViews(rootView);
        if (InGaugeSession.read(getString(R.string.key_is_filter_loaded_for_goal), false)) {
            if (isLoaded) {
                setFilterAdapter(mFilterBaseDatas);
            } else {
                clickOnCleartoUpdateFilterObj();
            }

        } else {
            if (!(mFilterBaseDatas != null && mFilterBaseDatas.size() > 0)) {
                InGaugeSession.write(getString(R.string.key_is_filter_loaded_for_goal), true);
                isLoaded = true;
                bindFilterObject();

            } else {
                setFilterAdapter(mFilterBaseDatas);

            }
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Logger.Error("<Call>>");
    }

    private void initViews(View rootView) {
        values = new ArrayList<>();
        rlProgress = (RelativeLayout) rootView.findViewById(R.id.custom_progress_rl_main);
        rvFilterList = (RecyclerView) rootView.findViewById(R.id.fragment_filter_list_recycleview);
        mButtonApply = (Button) rootView.findViewById(R.id.btn_apply_filter);
        mButtonApply.setOnClickListener(this);
        mButtonClear = (Button) rootView.findViewById(R.id.btn_clear_filter);
        mButtonClear.setOnClickListener(this);
        rlHorProgress = (RelativeLayout) rootView.findViewById(R.id.rl_hor_progress);


        mButtonApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_apply)));
        mButtonClear.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_clear)));
    }

    private void setFilterAdapter(ArrayList<FilterBaseData> mFilterBaseDataArrayList) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        rvFilterList.setLayoutManager(mLayoutManager);
        rvFilterList.setItemAnimator(new DefaultItemAnimator());
        rlProgress.setVisibility(View.GONE);
        rvFilterList.setVisibility(View.VISIBLE);

        listadapter = new FilterListAdapter(mHomeActivity, mFilterBaseDataArrayList, this, false);
        rvFilterList.setAdapter(listadapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_title)));
        mHomeActivity.tvTitle.setText(getString(R.string.filter));
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterposition,boolean isLeftFilter) {
        sendListData(position, filterposition);

    }

    public void sendListData(int position, int filterposition) {
        Bundle mBundle = new Bundle();

        switch (position) {
            case 0:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mAccessibleTenantLocationDataModelForGoal);
                break;
            case 1:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mDashboardLocationGroupListModelForGoal);
                break;
            case 2:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mProductListFromLocationGroupModelForGoal);
                break;
            case 3:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mDashboardUserListModelForGoal);
                break;


        }
        FilterBaseData mFilterBaseData = mFilterBaseDatas.get(filterposition);
        mBundle.putParcelable(KEY_FILTER_SELECTION, mFilterBaseData);
        mBundle.putInt(KEY_SELECTION_POSITION, position);
        FilterDetailListFragmentForGoal mFilterDetailListFragment = new FilterDetailListFragmentForGoal();
        mFilterDetailListFragment.setArguments(mBundle);
        mHomeActivity.replace(mFilterDetailListFragment, getString(R.string.tag_filter_detail_list_for_goal));
    }

    @Override
    public void onFilterDetailsClicked(FilterBaseData mFilterBaseData, int clickedPosition, int filterposition) {
        Logger.Error("< Call Listener Method >>>>");
        if (mFilterBaseData != null)
            updateFilterData(mFilterBaseData, clickedPosition);

    }

    public void updateFilterData(FilterBaseData mFilterBaseData, int clickedPosition) {

        isLoaded = true;
        mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
        int index = mFilterBaseData.filterIndex + 1;

        switch (index) {

            case 0:
                mHomeActivity.startProgress(mHomeActivity);
                Logger.Error(" Filter  " + mFilterBaseData.id);


                getAccessibleTenantLocationForFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                        String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), "name", "ASC", "Normal", false);
                break;
            case 1:
                mHomeActivity.startProgress(mHomeActivity);
                getDashboardLocationGroupListForFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), mFilterBaseData.id, String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)));
                break;
            case 2:
                mHomeActivity.startProgress(mHomeActivity);
                getProductListForFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), mFilterBaseData.id, "" + mFilterBaseDatas.get(0).id);
                break;
            case 3:
                mHomeActivity.startProgress(mHomeActivity);
                getDashboardUserListForFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), "" + mFilterBaseDatas.get(0).id, "2016-08-01", "2016-08-31", mFilterBaseDatas.get(1).id);
                break;

        }
    }


    private void VoiUpdateFilterData(int filterindex) {
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "";
        mFilterBaseData.name = "";
        mFilterBaseData.filterIndex = filterindex;
        switch (filterindex) {

            case 0:
                //mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                mHomeActivity.mAccessibleTenantLocationDataModelForGoal = null;
                /*Gson gson = new Gson();
                String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForGoal);
                InGaugeSession.write(mContext.getString(R.string.key_location_obj_for_goal), json);*/
                break;
            case 1:
                mHomeActivity.mDashboardLocationGroupListModelForGoal = null;
//                mFilterBaseData.filterName = getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModelForGoal);
                InGaugeSession.write(mContext.getString(R.string.key_location_group_obj_for_goal), json);*/
                break;
            case 2:
                //mFilterBaseData.filterName = getString(R.string.product);
                mHomeActivity.mProductListFromLocationGroupModelForGoal = null;
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModelForGoal);
                InGaugeSession.write(mContext.getString(R.string.key_product_obj), json);*/
                break;
            case 3:
               // mFilterBaseData.filterName = getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                mHomeActivity.mDashboardUserListModel = null;
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mDashboardUserListModelForGoal);
                InGaugeSession.write(mContext.getString(R.string.key_user_obj_for_goal), json);
                mFilterBaseData.filterName = getString(R.string.user);*/
                break;
        }
        if (listadapter != null) {
            if(filterindex >= mFilterBaseDatas.size()){
                mFilterBaseDatas.add(mFilterBaseData);
            }else{
                mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
            }

            listadapter.notifyDataSetChanged();
        } else {

            if (listadapter != null) {
                mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                listadapter.notifyDataSetChanged();
                rlProgress.setVisibility(View.GONE);
                rvFilterList.setVisibility(View.VISIBLE);
            } else {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                mFilterBaseDatas.add(mFilterBaseData.filterIndex, mFilterBaseData);
                rvFilterList.setAdapter(listadapter);
                rlProgress.setVisibility(View.GONE);
                rvFilterList.setVisibility(View.VISIBLE);
            }
        }

    }


    /**
     * @param authToken
     * @param tenantId
     * @param orderBy
     * @param sort
     * @param activeStatus
     */

    void getAccessibleTenantLocationForFilter(final String authToken, int tenantId, String userId, String orderBy, String sort, String activeStatus, final boolean isNeedUpdateOnApply) {

        if (isNeedUpdateOnApply) {
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), true);
        }
        try {
            mCall = apiInterface.getAccessibleTenantLocationForGoalFilter(tenantId, userId, orderBy, sort, activeStatus);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mAccessibleTenantLocationDataModelForGoal = (AccessibleTenantLocationDataModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_location_obj_for_goal), json);*/
                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mAccessibleTenantLocationDataModelForGoal.getData() != null && mAccessibleTenantLocationDataModelForGoal.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId());
                            mFilterBaseData.name = mAccessibleTenantLocationDataModelForGoal.getData().get(0).getName();
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_region_currency), mAccessibleTenantLocationDataModelForGoal.getData().get(0).getRegionCurrency());
                            mFilterBaseData.filterIndex = 0;
                            //mFilterBaseData.filterName = getString(R.string.location);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                            mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);


                            //mLeftFilterBaseDatas.add(mFilterBaseData);
                            //}

                            //String authToken,String tenantlocationId,String userId
                            //  InGaugeSession.write(getString(R.string.key_tenant_location_id), mAccessibleTenantLocationDataModel.getData().get(0).getId());
                            getDashboardLocationGroupListForFilter(authToken, String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId()), String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)));
                        } else {
                            VoiUpdateFilterData(0);
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);

                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupList(final String authToken, final String tenantlocationId, String userId) {

        try {
            mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mDashboardLocationGroupListModelForGoal = (DashboardLocationGroupListModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mDashboardLocationGroupListModelForGoal);
                        InGaugeSession.write(getString(R.string.key_location_group_obj_for_goal), json);*/
                        //String authToken,String locationGroupId

                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mDashboardLocationGroupListModelForGoal != null && mDashboardLocationGroupListModelForGoal.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID());
                            mFilterBaseData.name = mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupName();
                            mFilterBaseData.filterIndex = 1;
                            //mFilterBaseData.filterName = getString(R.string.location_group);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                            // if(mLeftFilterBaseDatas.size()>0){
                            //   mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            //}else{
                            mFilterBaseDatas.add(mFilterBaseData);
                            //}

                            //mLeftFilterBaseDatas.add(mFilterBaseData);
                            //}
                            //InGaugeSession.write(getString(R.string.key_selected_location_group), (mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                            getProductList(authToken, String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID()), tenantlocationId);
                        } else {
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }


                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupListForFilter(final String authToken, final String tenantlocationId, String userId) {
        try {
            mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if (response.body() != null) {
                        mDashboardLocationGroupListModelForGoal = (DashboardLocationGroupListModel) response.body();
                        if (mDashboardLocationGroupListModelForGoal != null && mDashboardLocationGroupListModelForGoal.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID());
                            mFilterBaseData.name = mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupName();
                            mFilterBaseData.filterIndex = 1;
                            //mFilterBaseData.filterName = getString(R.string.location_group);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                            mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                           getProductListForFilter(authToken, String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID()), tenantlocationId);
                        } else {
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }
                    }
                }
                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductList(final String authToken, final String locationGroupId, final String tenantlocationId) {

        try {
            mCall = apiInterface.getProductList(locationGroupId);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mProductListFromLocationGroupModelForGoal = (ProductListFromLocationGroupModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mProductListFromLocationGroupModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_product_obj_for_goal), json);*/

                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mProductListFromLocationGroupModelForGoal.getData() != null && mProductListFromLocationGroupModelForGoal.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            /*mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                            mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName();*/

                            mFilterBaseData.id = String.valueOf(-1);
                            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)) {
                                mFilterBaseData.name = "ARPD";
                            } else {
                                mFilterBaseData.name = "Incremental Revenue";
                            }
                            mFilterBaseData.filterIndex = 2;
//                            mFilterBaseData.filterName = getString(R.string.product);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                            mFilterBaseDatas.add(mFilterBaseData);


                            //   }
                            //String authToken, String tenantLocationI
                            // d, String startDate, String endDate, String locationGroupId
                            // InGaugeSession.write(getString(R.string.key_selected_location_group_product), (mProductListFromLocationGroupModel.getData().get(0).getId()));
                            getDashboardUserList(authToken, tenantlocationId, "2016-08-01", "2016-08-31", locationGroupId);
                        } else {
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductListForFilter(final String authToken, final String locationGroupId, final String tenantlocationId) {

        try {
            mCall = apiInterface.getProductList(locationGroupId);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mProductListFromLocationGroupModelForGoal = (ProductListFromLocationGroupModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mProductListFromLocationGroupModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_product_obj_for_goal), json);*/

                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mProductListFromLocationGroupModelForGoal.getData() != null && mProductListFromLocationGroupModelForGoal.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            /*mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                            mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName();*/

                            mFilterBaseData.id = String.valueOf(-1);
                            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)) {
                                mFilterBaseData.name = "ARPD";
                            } else {
                                mFilterBaseData.name = "Incremental Revenue";
                            }
                            mFilterBaseData.filterIndex = 2;
                            //mFilterBaseData.filterName = getString(R.string.product);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                            //  mLeftFilterBaseDatas.add(mFilterBaseData);


                            mFilterBaseDatas.set(mFilterBaseDatas.size() - 1, mFilterBaseData);

                            //   }
                            //String authToken, String tenantLocationI
                            // d, String startDate, String endDate, String locationGroupId
                            // InGaugeSession.write(getString(R.string.key_selected_location_group_product), (mProductListFromLocationGroupModel.getData().get(0).getId()));
                            getDashboardUserListForFilter(authToken, tenantlocationId, "2016-08-01", "2016-08-31", locationGroupId);
                        } else {
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param authToken
     * @param tenantLocationId
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserList(String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId) {

        try {
            mCall = apiInterface.getDashboardUserListForGoalFilter(tenantLocationId, locationGroupId, true);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mDashboardUserListModelForGoal = (DashboardUserListModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mDashboardUserListModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_user_obj_for_goal), json);*/


                        if (mDashboardUserListModelForGoal != null) {

                            if (mDashboardUserListModelForGoal.getData() != null && mDashboardUserListModelForGoal.getData().size() > 0) {
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(mDashboardUserListModelForGoal.getData().get(0).getId());
                                mFilterBaseData.name = mDashboardUserListModelForGoal.getData().get(0).getName();
                                mFilterBaseData.filterIndex = 3;
                                //mFilterBaseData.filterName = getString(R.string.user);
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                mFilterBaseDatas.add(mFilterBaseData);

                                rlProgress.setVisibility(View.GONE);
                                mHomeActivity.endProgress();
                                // InGaugeSession.write(getString(R.string.key_selected_user), mDashboardUserListModel.getData().get(0).getId());
                                setFilterAdapter(mFilterBaseDatas);
                            } else {
                                VoiUpdateFilterData(3);
                                rlProgress.setVisibility(View.GONE);
                                mHomeActivity.endProgress();
                            }
                        } else {
                            VoiUpdateFilterData(3);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param authToken
     * @param tenantLocationId
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserListForFilter(String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId) {

        try {
            mCall = apiInterface.getDashboardUserListForGoalFilter(tenantLocationId, locationGroupId, true);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mDashboardUserListModelForGoal = (DashboardUserListModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mDashboardUserListModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_user_obj_for_goal), json);*/


                        if (mDashboardUserListModelForGoal != null) {

                            if (mDashboardUserListModelForGoal.getData() != null && mDashboardUserListModelForGoal.getData().size() > 0) {
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(mDashboardUserListModelForGoal.getData().get(0).getId());
                                mFilterBaseData.name = mDashboardUserListModelForGoal.getData().get(0).getName();
                                mFilterBaseData.filterIndex = 3;
                                //mFilterBaseData.filterName = getString(R.string.user);
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                //mLeftFilterBaseDatas.add(mFilterBaseData);


                                mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);

                                rlProgress.setVisibility(View.GONE);
                                mHomeActivity.endProgress();
                                // InGaugeSession.write(getString(R.string.key_selected_user), mDashboardUserListModel.getData().get(0).getId());
                                setFilterAdapter(mFilterBaseDatas);
                            } else {
                                VoiUpdateFilterData(3);
                                rlProgress.setVisibility(View.GONE);
                                mHomeActivity.endProgress();
                            }
                        } else {
                            VoiUpdateFilterData(3);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_apply_filter:
                clickOnApplytoUpdatePreference();

                break;
            case R.id.btn_clear_filter:
                //mHomeActivity.onBackPressed();
                try {

                    // fetchinitalDataForFilter();
                    mHomeActivity.startProgress(mHomeActivity);
                    mFilterBaseDatas.clear();
                    getAccessibleTenantLocation(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), "name", "ASC", "Normal", true);
                    /*listadapter.notifyDataSetChanged();
                    rlProgress.setVisibility(View.VISIBLE);
                    rvFilterList.setVisibility(View.GONE);
                    getRegionTypes(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1));*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //getRegionTypesForFilter(InGaugeSession.read(getString(R.string.key_auth_token),""),-1);
                break;
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mCall != null) {
            mCall.cancel();
        }
    }

    public void clickOnApplytoUpdatePreference() {


        if (InGaugeSession.read(getString(R.string.key_is_need_update_on_apply_for_goal), false)) {
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), false);

            mHomeActivity.mAccessibleTenantLocationDataModelForGoal = null;
            mHomeActivity.mAccessibleTenantLocationDataModelForGoal = mAccessibleTenantLocationDataModelForGoal;
            Gson gson = new Gson();
            String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForGoal);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_obj_for_goal), json);


            mHomeActivity.mDashboardLocationGroupListModelForGoal = null;
            mHomeActivity.mDashboardLocationGroupListModelForGoal = mDashboardLocationGroupListModelForGoal;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModelForGoal);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_group_obj_for_goal), json);

            mHomeActivity.mProductListFromLocationGroupModelForGoal = null;
            mHomeActivity.mProductListFromLocationGroupModelForGoal = mProductListFromLocationGroupModelForGoal;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModelForGoal);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_product_obj_for_goal), json);


            mHomeActivity.mDashboardUserListModelForGoal = null;
            mHomeActivity.mDashboardUserListModelForGoal = mDashboardUserListModelForGoal;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mDashboardUserListModelForGoal);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_user_obj_for_goal), json);
        }
        for (int i = 0; i < mFilterBaseDatas.size(); i++) {
            switch (i) {

                case 0:
                    FilterBaseData mFilterBaseData = mFilterBaseDatas.get(i);
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(getString(R.string.key_selected_tenant_location_id_for_default_goal), Integer.parseInt(mFilterBaseData.id));

                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), mFilterBaseData.name);
                    break;
                case 1:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(1));
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), mFilterBaseData.id);
                        InGaugeSession.write(getString(R.string.key_selected_location_group_id_for_default_goal), mFilterBaseData.id);

                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name_for_goal), mFilterBaseData.name);
                    break;
                case 2:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(getString(R.string.key_selected_location_group_product_id_for_default_goal), Integer.parseInt(mFilterBaseData.id));

                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), mFilterBaseData.name);
                    break;
                case 3:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(getString(R.string.key_selected_user_id_for_default_goal), Integer.parseInt(mFilterBaseData.id));
                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), mFilterBaseData.name);
                    break;


            }
        }


        Bundle bundle = new Bundle();
        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
        bundle.putString("role",InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
        bundle.putString("industry",InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
        bundle.putString("location",InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), ""));
        bundle.putString("location_group",InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name_for_goal), ""));
        bundle.putString("product",InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), ""));
        bundle.putString("user",InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), ""));
        InGaugeApp.getFirebaseAnalytics().logEvent("change_goal_filter", bundle);
        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_update_goal_settings_filter),true);
        mHomeActivity.onBackPressed();
    }


    public void clickOnCleartoUpdateFilterObj() {

        mAccessibleTenantLocationDataModelForGoal = mHomeActivity.mAccessibleTenantLocationDataModelForGoal;
        mDashboardLocationGroupListModelForGoal = mHomeActivity.mDashboardLocationGroupListModelForGoal;
        mProductListFromLocationGroupModelForGoal = mHomeActivity.mProductListFromLocationGroupModelForGoal;
        mDashboardUserListModelForGoal = mHomeActivity.mDashboardUserListModelForGoal;


        mFilterBaseDatas.clear();
        for (int i = 0; i < 4; i++) {
            FilterBaseData mFilterBaseData = new FilterBaseData();
            if (i == 0) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_location_id_for_goal), 1));
                mFilterBaseData.name = InGaugeSession.read(getString(R.string.key_selected_tenant_location_name_for_goal), "");
                mFilterBaseData.filterName =mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
            } else if (i == 1) {
                String key = mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal);
                String id = InGaugeSession.read(key, "" + 1);
                mFilterBaseData.id = id;
                mFilterBaseData.name = InGaugeSession.read(getString(R.string.key_selected_location_group_name_for_goal), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
            } else if (i == 2) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(getString(R.string.key_selected_location_group_product_id_for_goal), 1));
                mFilterBaseData.name = InGaugeSession.read(getString(R.string.key_selected_location_group_product_name_for_goal), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
            } else if (i == 3) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(getString(R.string.key_selected_user_id_for_goal), 1));
                mFilterBaseData.name = InGaugeSession.read(getString(R.string.key_selected_user_name_for_goal), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
            }
            mFilterBaseData.filterIndex = i;
            mFilterBaseDatas.add(mFilterBaseData);
        }

        setFilterAdapter(mFilterBaseDatas);
    }

    public void bindFilterObject() {
        //mLeftFilterBaseDatas.clear();
        mAccessibleTenantLocationDataModelForGoal = mHomeActivity.mAccessibleTenantLocationDataModelForGoal;
        mDashboardLocationGroupListModelForGoal = mHomeActivity.mDashboardLocationGroupListModelForGoal;
        mProductListFromLocationGroupModelForGoal = mHomeActivity.mProductListFromLocationGroupModelForGoal;
        mDashboardUserListModelForGoal = mHomeActivity.mDashboardUserListModelForGoal;
        for (int i = 0; i < 4; i++) {
            FilterBaseData mFilterBaseData = new FilterBaseData();

            if (i == 0) {
                if (mAccessibleTenantLocationDataModelForGoal != null && (mAccessibleTenantLocationDataModelForGoal.getData() != null && mAccessibleTenantLocationDataModelForGoal.getData().size() > 0)) {
                    mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId());
                    mFilterBaseData.name = mAccessibleTenantLocationDataModelForGoal.getData().get(0).getName();
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_region_currency), mAccessibleTenantLocationDataModelForGoal.getData().get(0).getRegionCurrency());
                    InGaugeSession.write(getString(R.string.key_selected_tenant_location_id_for_goal), mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId());
                    InGaugeSession.write(getString(R.string.key_selected_tenant_location_name_for_goal), mAccessibleTenantLocationDataModelForGoal.getData().get(0).getName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }
                //mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));

            } else if (i == 1) {
                if (mDashboardLocationGroupListModelForGoal != null && (mDashboardLocationGroupListModelForGoal.getData() != null && mDashboardLocationGroupListModelForGoal.getData().size() > 0)) {
                    mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID());
                    mFilterBaseData.name = mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupName();
                    int locationGroupID = mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID();
                    InGaugeSession.write(getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(locationGroupID));
                    InGaugeSession.write(getString(R.string.key_selected_location_group_name_for_goal), mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }
                //mFilterBaseData.filterName = getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));

            } else if (i == 2) {
                if (mProductListFromLocationGroupModelForGoal != null && (mProductListFromLocationGroupModelForGoal.getData() != null && mProductListFromLocationGroupModelForGoal.getData().size() > 0)) {
                    /*mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                    mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName();
                    InGaugeSession.write(getString(R.string.key_selected_location_group_product_id_for_goal), mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                    InGaugeSession.write(getString(R.string.key_selected_location_group_product_name_for_goal), mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName());*/
                    mFilterBaseData.id = String.valueOf(-1);
                    if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)) {
                        mFilterBaseData.name = "ARPD";
                    } else {
                        mFilterBaseData.name = "Incremental Revenue";
                    }
                    InGaugeSession.write(getString(R.string.key_selected_location_group_product_id_for_goal), -1);
                    InGaugeSession.write(getString(R.string.key_selected_location_group_product_name_for_goal), "Incremental Revenue");
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }

                //mFilterBaseData.filterName = getString(R.string.product);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
            } else if (i == 3) {
                if (mDashboardUserListModelForGoal != null && (mDashboardUserListModelForGoal.getData() != null && mDashboardUserListModelForGoal.getData().size() > 0)) {

                    mFilterBaseData.id = String.valueOf(mDashboardUserListModelForGoal.getData().get(0).getId());
                    mFilterBaseData.name = mDashboardUserListModelForGoal.getData().get(0).getName();
                    InGaugeSession.write(getString(R.string.key_selected_user_id_for_goal), (mDashboardUserListModelForGoal.getData().get(0).getId()));
                    InGaugeSession.write(getString(R.string.key_selected_user_name_for_goal), mDashboardUserListModelForGoal.getData().get(0).getName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }
                //mFilterBaseData.filterName = getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
            }
            mFilterBaseData.filterIndex = i;
            mFilterBaseDatas.add(mFilterBaseData);
        }

        setFilterAdapter(mFilterBaseDatas);
    }

    /**
     * @param authToken
     * @param tenantId
     * @param orderBy
     * @param sort
     * @param activeStatus
     */

    void getAccessibleTenantLocation(final String authToken, int tenantId, String userId, String orderBy, String sort, String activeStatus, final boolean isNeedUpdateOnApply) {

        if (isNeedUpdateOnApply) {
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), true);
        }
        try {
            mCall = apiInterface.getAccessibleTenantLocationForGoalFilter(tenantId, userId, orderBy, sort, activeStatus);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mAccessibleTenantLocationDataModelForGoal = (AccessibleTenantLocationDataModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_location_obj_for_goal), json);*/
                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mAccessibleTenantLocationDataModelForGoal.getData() != null && mAccessibleTenantLocationDataModelForGoal.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId());
                            mFilterBaseData.name = mAccessibleTenantLocationDataModelForGoal.getData().get(0).getName();
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_region_currency), mAccessibleTenantLocationDataModelForGoal.getData().get(0).getRegionCurrency());
                            mFilterBaseData.filterIndex = 0;
                            //mFilterBaseData.filterName = getString(R.string.location);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));

                            mFilterBaseDatas.add(mFilterBaseData);


                            //mLeftFilterBaseDatas.add(mFilterBaseData);
                            //}

                            //String authToken,String tenantlocationId,String userId
                            //  InGaugeSession.write(getString(R.string.key_tenant_location_id), mAccessibleTenantLocationDataModel.getData().get(0).getId());
                            getDashboardLocationGroupList(authToken, String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId()), String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)));
                        } else {
                            VoiUpdateFilterData(0);
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);

                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
