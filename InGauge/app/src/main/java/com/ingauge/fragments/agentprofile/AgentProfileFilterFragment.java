package com.ingauge.fragments.agentprofile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.DynamicFilterLeftAdapter;
import com.ingauge.adapters.FilterDetailListAdapterForAgentProfile;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModelForAgentProfile;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.SimpleDividerItemDecoration;
import com.ingauge.utils.UiUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 03-Apr-18.
 */

public class AgentProfileFilterFragment extends BaseFragment implements View.OnClickListener, RecyclerViewClickListener{


    public static String KEY_IS_OPEATOR = "key_is_operator";
    private HomeActivity mHomeActivity;
    private View rootView;
    Call mCall;
    APIInterface apiInterface;

    private TextView tvFilterClear;
    private TextView tvApply;
    private RecyclerView rvFilterList;
    private RecyclerView rvFilterLeftList;
    public static SearchView mSearchView;
    View mView;


    String selectedId = "";
    String selectedName = "";

    public AccessibleTenantLocationDataModelForAgentProfile mAccessibleTenantLocationDataModel;
    public DashboardUserListModel mDashboardUserListModel;
    ArrayList<FilterBaseData> mLeftFilterBaseDatas;
    ArrayList<FilterBaseData> mRightFilterBaseDatas;

    //Right List Adapter
    FilterDetailListAdapterForAgentProfile mFilterListAdapter;

    //Left List Adapter
    DynamicFilterLeftAdapter mDynamicFilterLeftAdapter;

    private String selectedTenantLocationId = "";
    private String selectedTenantLocationName = "";
    private String selectedUserId = "";
    private boolean isUserOperator = false;
    private TextView tvNoDataFound;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        /*if(getArguments() != null){
            isUserOperator = getArguments().getBoolean(KEY_IS_OPEATOR, false);
        }*/

        isUserOperator = mHomeActivity.isUserOperator;
        mLeftFilterBaseDatas = new ArrayList<>();
        mRightFilterBaseDatas = new ArrayList<>();

        mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile;
        mDashboardUserListModel = mHomeActivity.mDashboardUserListModelForAgentProfile;
        if(mLeftFilterBaseDatas != null){
            mLeftFilterBaseDatas.clear();
        }
        mLeftFilterBaseDatas.addAll(mHomeActivity.mFilterBaseDatas);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){

        rootView = inflater.inflate(R.layout.fragment_dynamic_dashboard_filter, container, false);
        initView(rootView);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){


            @Override
            public boolean onQueryTextSubmit(String query){
                Logger.Error("<<<<   onQueryTextSubmit >>>>>");
                // FilterDetailListAdapter.ivRight.setVisibility(View.VISIBLE);
                //mFilterRightListAdapter.notifyDataSetChanged();
                Logger.Error("<<<<   Called onQueryTextSubmit >>>>>");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText){
                if(mFilterListAdapter != null){
                    mFilterListAdapter.getFilter().filter(newText);
                }

                Logger.Error("<<<<   onQueryTextChange  >>>>>");
                /*if(TextUtils.isEmpty(newText)){
                    this.onQueryTextSubmit("");
                    //if(FilterDetailListAdapter.ivRight!=null){
                    //}
                }else{
                    FilterDetailListAdapter.ivRight.setVisibility(View.GONE);
                }*/
                return false;
            }

        });

        //getisUserCompare(String.valueOf(mHomeActivity.tenantLocationId), mHomeActivity.userIdForAPI, true);
        if(mLeftFilterBaseDatas != null && mLeftFilterBaseDatas.size() > 0){
            selectedId = mLeftFilterBaseDatas.get(0).id;
            selectedName = mLeftFilterBaseDatas.get(0).name;
            selectedTenantLocationId = selectedId;
            selectedTenantLocationName = selectedName;
            setLeftFilterdataListAdapter(mLeftFilterBaseDatas, 0);
            getStoreAccessibleTenantLocation(mAccessibleTenantLocationDataModel, selectedId, selectedName);
        }

        selectedUserId = mHomeActivity.userIdForAPI;
        return rootView;

    }

    public void initView(View itemView){
        tvFilterClear = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_clear_all);
        tvApply = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_apply);
        rvFilterLeftList = (RecyclerView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_rv_left);
        rvFilterList = (RecyclerView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_rv);
        mSearchView = (SearchView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_search_view);
        mView = itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_view_sep);
        tvNoDataFound = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_no_data);
        tvFilterClear.setOnClickListener(this);
        tvFilterClear.setVisibility(View.GONE);
        mView.setVisibility(View.GONE);
        tvApply.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.fragment_dynamic_dashboard_filter_tv_apply:
                selectedTenantLocationId = mLeftFilterBaseDatas.get(0).id;
                selectedTenantLocationName = mLeftFilterBaseDatas.get(0).name;
                selectedUserId = mLeftFilterBaseDatas.get(1).id;
                selectedName = mLeftFilterBaseDatas.get(1).name;
                if(Integer.parseInt(selectedTenantLocationId) == 0){
                    mHomeActivity.isLocationAvailable = false;
                } else{
                    mHomeActivity.isLocationAvailable = true;
                }
                mHomeActivity.setNeedAgentProfileUpdate(true);
                mHomeActivity.mFilterBaseDatas.clear();
                mHomeActivity.mFilterBaseDatas.addAll(mLeftFilterBaseDatas);
                mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile = null;
                mHomeActivity.mAccessibleTenantLocationDataModelforAgentProfile = mAccessibleTenantLocationDataModel;
                mHomeActivity.mDashboardUserListModelForAgentProfile = null;
                mHomeActivity.mDashboardUserListModelForAgentProfile = mDashboardUserListModel;
                mHomeActivity.userIdForAPI = selectedUserId;
                mHomeActivity.userSelectedName = selectedName;
                mHomeActivity.tenantLocationId = Integer.parseInt(selectedTenantLocationId);
                mHomeActivity.tenantLocationLocationName = selectedTenantLocationName;
                mHomeActivity.isUserOperator = isUserOperator;
                mHomeActivity.onBackPressed();

                break;
        }
    }


    private void getStoreAccessibleTenantLocation(AccessibleTenantLocationDataModelForAgentProfile mAccessibleTenantLocationDataModel, String selectedid, String selectedname){
        if(mAccessibleTenantLocationDataModel != null){
            int count = 0;
            for(AccessibleTenantLocationDataModelForAgentProfile.AllLocation mDatum : mAccessibleTenantLocationDataModel.getData().getAllLocation()){
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterMetricType = mDatum.getHotelMetricsDataType();
                mFilterBaseData.filterIndex = 0;
                selectedId = mFilterBaseData.id;
                selectedName = mFilterBaseData.name;
                //mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_location));
                if(mLeftFilterBaseDatas.size() > 0){
                    for(int i = 0; i < mLeftFilterBaseDatas.size(); i++){
                        if(mFilterBaseData.filterName.equalsIgnoreCase(mLeftFilterBaseDatas.get(i).filterName)){
                            mFilterBaseData.filterIndex = mLeftFilterBaseDatas.get(i).filterIndex;
                            break;
                        } else{
                            mFilterBaseData.filterIndex = -1;
                        }
                    }
                }

                if(mFilterBaseData.id.equalsIgnoreCase(selectedid)){
                    mFilterBaseData.isSelectedPosition = true;
                } else{
                    mFilterBaseData.isSelectedPosition = false;
                }
                /*if (count == 0) {

                } else {

                }*/
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }


    private void getDashboardUserList(DashboardUserListModel mDashboardUserListModel, String selectedid, String selectedname){
        if(mDashboardUserListModel != null){
            int count = 0;
            String loginUseruserName = InGaugeSession.read(getString(R.string.key_user_name), "");
            String loginUserid = String.valueOf(UiUtils.getUserId(mHomeActivity));
            if(selectedid.equalsIgnoreCase(loginUserid)){
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = selectedid;
                mFilterBaseData.name = loginUseruserName;
                mFilterBaseData.filterIndex = 1;
                //mFilterBaseData.filterName = getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user));
                mFilterBaseData.isSelectedPosition = true;
                mRightFilterBaseDatas.add(mFilterBaseData);
            } else{
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = selectedid;
                mFilterBaseData.name = loginUseruserName;
                mFilterBaseData.filterIndex = 1;
                //mFilterBaseData.filterName = getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user));
                mFilterBaseData.isSelectedPosition = false;
                mRightFilterBaseDatas.add(mFilterBaseData);

            }
            if(isUserOperator){
                if(mDashboardUserListModel.getData() != null && mDashboardUserListModel.getData().size() > 0){

                    for(int i = 0; i < mDashboardUserListModel.getData().size(); i++){
                        if(mRightFilterBaseDatas.get(0).id.equalsIgnoreCase(String.valueOf(mDashboardUserListModel.getData().get(i).getId()))){
                            mRightFilterBaseDatas.clear();
                            break;
                        }
                    }
                    for(DashboardUserListModel.Datum mDatum : mDashboardUserListModel.getData()){

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mDatum.getId());
                        mFilterBaseData.name = mDatum.getName();
                        mFilterBaseData.filterIndex = 1;
                        //mFilterBaseData.filterName = getString(R.string.user);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user));

                        if(mFilterBaseData.id.equalsIgnoreCase(selectedid)){
                            mFilterBaseData.isSelectedPosition = true;
                        } else{
                            mFilterBaseData.isSelectedPosition = false;
                        }
                        count++;
                        mRightFilterBaseDatas.add(mFilterBaseData);
                    }
                }
            }


//            selectedid= "" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user), 1);
//            selectedname= InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");

            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }

    private void setFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList, String selectedid, String selectedname){
        List<FilterBaseData> mDataList = new ArrayList<>();
        if(mFilterBaseDataList != null){
            if(mFilterBaseDataList.size() > 0){

                int selectedPosiotn = 0;
                for(FilterBaseData mFilterBaseData : mFilterBaseDataList){
                    if(mFilterBaseData.isSelectedPosition){
                        selectedPosiotn = mFilterBaseDataList.indexOf(mFilterBaseData);
                        mFilterBaseDataList.remove(selectedPosiotn);
                        mFilterBaseDataList.add(0, mFilterBaseData);
                        break;
                    }
                }
                mFilterListAdapter = new FilterDetailListAdapterForAgentProfile(mHomeActivity, mFilterBaseDataList, this, true, selectedid, selectedname);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                rvFilterList.setAdapter(mFilterListAdapter);
                tvNoDataFound.setVisibility(View.GONE);
                rvFilterList.setVisibility(View.VISIBLE);

            } else{
                tvNoDataFound.setVisibility(View.VISIBLE);
                rvFilterList.setVisibility(View.GONE);
                mFilterListAdapter = new FilterDetailListAdapterForAgentProfile(mHomeActivity, new ArrayList<FilterBaseData>(), this, true, selectedid, selectedname);
                rvFilterList.setAdapter(mFilterListAdapter);
            }
        }
    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterPosition, boolean isLeftFilter){

        if(isLeftFilter){
            mRightFilterBaseDatas.clear();
            String filterName = mFilterBaseData.filterName;
            if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_location)))){
                if(mLeftFilterBaseDatas != null && mLeftFilterBaseDatas.size() > 0){
                    selectedId = mLeftFilterBaseDatas.get(0).id;
                    selectedName = mLeftFilterBaseDatas.get(0).name;
                    getStoreAccessibleTenantLocation(mAccessibleTenantLocationDataModel, selectedId, selectedName);
                }
            } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user)))){
                if(mLeftFilterBaseDatas != null && mLeftFilterBaseDatas.size() > 0){
                    selectedId = mLeftFilterBaseDatas.get(1).id;
                    selectedName = mLeftFilterBaseDatas.get(1).name;
                    getDashboardUserList(mDashboardUserListModel, selectedId, selectedName);
                }
            }
        } else{
            updateOnRightClick(mFilterBaseData);
        }
    }

    private void setLeftFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList, int level){
        if(mFilterBaseDataList != null){
            if(mFilterBaseDataList.size() > 0){

                /*if (mDynamicFilterLeftAdapter != null) {
                    mDynamicFilterLeftAdapter.notifyDataSetChanged();
                } else {*/
                mDynamicFilterLeftAdapter = new DynamicFilterLeftAdapter(mHomeActivity, mFilterBaseDataList, this, true, level);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterLeftList.setLayoutManager(mLayoutManager);
                rvFilterLeftList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterLeftList.setItemAnimator(new DefaultItemAnimator());
                rvFilterLeftList.setAdapter(mDynamicFilterLeftAdapter);
                /*}*/

            }
        }
    }

    private void updateOnRightClick(FilterBaseData mFilterBaseData){

        if(mFilterBaseData != null){
            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
            mDynamicFilterLeftAdapter.notifyDataSetChanged();
            String filterName = mFilterBaseData.filterName;
            if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_location)))){

                String loginUseruserName = InGaugeSession.read(getString(R.string.key_user_name), "");
                String loginUserid = String.valueOf(UiUtils.getUserId(mHomeActivity));
                selectedUserId = loginUserid;
                selectedName = loginUseruserName;
                mHomeActivity.startProgress(mHomeActivity);
                getisUserCompare(mFilterBaseData.id, selectedUserId, false);

            } else if(filterName.equalsIgnoreCase(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user)))){
                selectedUserId = mFilterBaseData.id;
                selectedName = mFilterBaseData.name;
            }
        }
    }


    void getisUserCompare(final String TenantLocationId, String userId, final boolean isNeedtoCheckIperatorOnly){

        if(isNeedtoCheckIperatorOnly){
            mHomeActivity.startProgress(mHomeActivity);
        }
        int tenantId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1);
        Call<ResponseBody> mCall = apiInterface.getIsOperator(tenantId, TenantLocationId, userId);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                if(response.body() != null){
                    try{

                        String jsonResponse = response.body().string();
                        Logger.Error("<<< Response  >>>>> " + jsonResponse);
                        JSONObject mJsonObject = new JSONObject(jsonResponse);
                        if(mJsonObject.optBoolean("data")){

                            if(!isNeedtoCheckIperatorOnly){

                                getDashboardUserList(Integer.parseInt(TenantLocationId));
                            }else{
                                mHomeActivity.endProgress();
                            }


                            isUserOperator = true;
                        } else{

                            mHomeActivity.endProgress();
                            if(!isNeedtoCheckIperatorOnly){
                                String loginUseruserName = InGaugeSession.read(getString(R.string.key_user_name), "");
                                String loginUserid = String.valueOf(UiUtils.getUserId(mHomeActivity));
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = loginUserid;
                                mFilterBaseData.name = loginUseruserName;
                                mFilterBaseData.filterIndex = 1;
                                //mFilterBaseData.filterName = getString(R.string.user);
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user));
                                mFilterBaseData.isSelectedPosition = true;
                                mLeftFilterBaseDatas.set(1, mFilterBaseData);
                                mDynamicFilterLeftAdapter.notifyDataSetChanged();

                            }

                            isUserOperator = false;
                        }

                    } catch(Exception e){
                        e.printStackTrace();
                    }
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);*/
            }
        });
    }

    /**
     * @param TenantLocationId
     */
    void getDashboardUserList(int TenantLocationId){
        int tenantId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1);
        Call mCall = apiInterface.getDashboardUserListForAgentProfile(tenantId,
                false,
                false,
                false,
                true,
                false,
                TenantLocationId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                mHomeActivity.endProgress();
                if(response.body() != null){
                    mDashboardUserListModel = (DashboardUserListModel) response.body();
                    /*Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardUserListModelForLeaderBoard);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_obj), json);*/
                    String loginUseruserName = InGaugeSession.read(getString(R.string.key_user_name), "");
                    String loginUserid = String.valueOf(UiUtils.getUserId(mHomeActivity));
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = loginUserid;
                    mFilterBaseData.name = loginUseruserName;
                    mFilterBaseData.filterIndex = 1;
                    //mFilterBaseData.filterName = getString(R.string.user);
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_user));
                    mFilterBaseData.isSelectedPosition = true;
                    mLeftFilterBaseDatas.set(1, mFilterBaseData);
                    mDynamicFilterLeftAdapter.notifyDataSetChanged();

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();

                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);*/
            }
        });
    }


    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.tvTitle.setText("Filter By");
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibBack.setVisibility(View.VISIBLE);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
    }
}
