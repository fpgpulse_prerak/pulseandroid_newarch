package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.FilterDetailListAdapterObservationDashboard;
import com.ingauge.listener.OnFilterDataSetListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ObservationDashboardStatusModel;
import com.ingauge.pojo.ObservationSurveyCategoryModel;
import com.ingauge.utils.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mansurum on 24-May-17.
 */

public class FilterDetailListFragmentForObservationDashboard extends Fragment {
    private RecyclerView rvFilterDataList;
    private HomeActivity mHomeActivity;
    View rootView;
    FilterBaseData mFilterBaseData;
    ArrayList<FilterBaseData> mFilterBaseDatas;
    private int selectedItemPosition = -1;
    private OnFilterDataSetListener onFilterDataSetListener;
    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel;
    private ObservationSurveyCategoryModel observationSurveyCategoryModel;
    private ArrayList<ObservationDashboardStatusModel> observationDashboardStatusModelArrayList;
    private SearchView searchView;
    private FilterDetailListAdapterObservationDashboard mFilterListAdapter;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onAttachToParentFragment(getFragmentManager().findFragmentByTag(getString(R.string.tag_filter_list_for_observe_agent)));
        mFilterBaseDatas = new ArrayList<>();
    }

    public void onAttachToParentFragment(Fragment fragment) {
        try {
            onFilterDataSetListener = (OnFilterDataSetListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString() + " must implement OnFilterDataSetListener");
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_detail_filter_list, container, false);
        initViews(rootView);
        if (getArguments() != null) {
            selectedItemPosition = getArguments().getInt(FilterListFragment.KEY_SELECTION_POSITION, selectedItemPosition);
            getFilterData(selectedItemPosition);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mFilterListAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return rootView;
    }


    void getFilterData(int position) {
        switch (position) {
            case 0:
                mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getAccessibleTenantLocation(mAccessibleTenantLocationDataModel);
                mHomeActivity.tvTitle.setText("Location");
                break;
            case 1:
                observationSurveyCategoryModel = (ObservationSurveyCategoryModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getObservationSurveyCategory(observationSurveyCategoryModel);
                mHomeActivity.tvTitle.setText("Survey Category");
                break;
            case 2:
                observationDashboardStatusModelArrayList = (ArrayList<ObservationDashboardStatusModel>) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getObservationDashboardStatus(observationDashboardStatusModelArrayList);
                mHomeActivity.tvTitle.setText("Status");
                break;

        }

    }
    private void initViews(View rootView) {
        rvFilterDataList = (RecyclerView) rootView.findViewById(R.id.fragment_detail_filter_list_recycleview);
        searchView = (SearchView) rootView.findViewById(R.id.fragment_detail_filter_list_search_view);
    }
    private void getObservationDashboardStatus(ArrayList<ObservationDashboardStatusModel> observationDashboardStatusModelArrayList) {
        if (observationDashboardStatusModelArrayList != null) {
            for(int i=0;i<observationDashboardStatusModelArrayList.size();i++){
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(observationDashboardStatusModelArrayList.get(i).getStatusID());
                mFilterBaseData.name = observationDashboardStatusModelArrayList.get(i).getStatusName();
                mFilterBaseData.filterName = "Status";
                mFilterBaseData.filterIndex = 2;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas, true);
        }
    }

    private void getObservationSurveyCategory(ObservationSurveyCategoryModel observationSurveyCategoryModel) {

        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "-1";
        mFilterBaseData.name ="All Categories";
        mFilterBaseData.filterName = "Category";
        mFilterBaseData.filterIndex = 1;
        mFilterBaseDatas.add(mFilterBaseData);

        if (observationSurveyCategoryModel != null) {
            for (ObservationSurveyCategoryModel.Datum mDatum : observationSurveyCategoryModel.getData()) {
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterName = "Category";
                mFilterBaseData.filterIndex = 1;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas, true);
        }
    }

    private void getAccessibleTenantLocation(AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel) {

        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "-1";
        mFilterBaseData.name ="All Locations";
        mFilterBaseData.filterName = getString(R.string.location);
        mFilterBaseData.filterIndex = 0;
        mFilterBaseDatas.add(mFilterBaseData);

        if (mAccessibleTenantLocationDataModel != null) {
            for (AccessibleTenantLocationDataModel.Datum mDatum : mAccessibleTenantLocationDataModel.getData()) {
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterIndex = 0;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas, true);
        }
    }


    private void setFilterdataListAdapter(ArrayList<FilterBaseData> mFilterBaseDataList, boolean isSort) {
        if (mFilterBaseDataList != null) {
            if (mFilterBaseDataList.size() > 0) {
                if (isSort) {
                    /*Collections.sort(mFilterBaseDataList, new Comparator<FilterBaseData>() {
                        public int compare(FilterBaseData v1, FilterBaseData v2) {
                            return v1.name.compareTo(v2.name);
                        }
                    });*/
                }
                mFilterListAdapter = new FilterDetailListAdapterObservationDashboard(getActivity(),mFilterBaseDataList,mHomeActivity);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterDataList.setLayoutManager(mLayoutManager);
                rvFilterDataList.setItemAnimator(new DefaultItemAnimator());
                rvFilterDataList.setAdapter(mFilterListAdapter);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
    }
}
