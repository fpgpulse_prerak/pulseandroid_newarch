package com.ingauge.fragments.socialinteraction;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.SocialInteractionFilterListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.fragments.DashboardDetailFragment;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.listener.SocialInteractionRvClickListenerForLocation;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 09-Nov-17.
 */

public class FragmentSocialInteractionLocationFilter extends BaseFragment implements SocialInteractionRvClickListenerForLocation {

    APIInterface apiInterface;
    Call mCall;

    private List<FilterBaseData> mFilterBaseDatas = new ArrayList<>();
    SocialInteractionFilterListAdapter mFilterListAdapter;
    private HomeActivity mHomeActivity;
    View rootView;

    private SearchView mSearchView;
    private RecyclerView mRecyclerViewFilterList;
    private TextView tvNoData;
    public boolean isNeedFilterCall = false;
    List<FilterBaseData> list = new ArrayList<FilterBaseData>();
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (getArguments() != null) {
            isNeedFilterCall = getArguments().getBoolean(DashboardDetailFragment.KEY_IS_NEED_FILTER_CALL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_social_interaction_location_filter, container, false);
        initView(rootView);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mFilterListAdapter.getFilter() != null) {
                    mFilterListAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });

        if (isNeedFilterCall) {
            mHomeActivity.startProgress(mHomeActivity);
            getAccessibleTenantLocationForFilter(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1),
                    String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", "-2", "Normal");
        } else {
            mFilterBaseDatas.addAll(mHomeActivity.getmLocationFilterData());
            setFilterdataListAdapter(mHomeActivity.getmLocationFilterData());
        }
        setSelectedUpdateCount();
        return rootView;
    }

    public void initView(View view) {
        mSearchView = (SearchView) view.findViewById(R.id.fragment_social_interaction_location_filter_sv);
        mRecyclerViewFilterList = (RecyclerView) view.findViewById(R.id.fragment_social_interaction_location_filter_rv);
        tvNoData = (TextView) view.findViewById(R.id.tv_nodata);
    }

    @Override
    public void onResume() {
        super.onResume();
      //  mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.zero_selected));
        mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        mHomeActivity.ibMenu.setImageResource(R.drawable.ic_select_all);
        //mHomeActivity.ibMenu.setBackgroundTintList(ContextCompat.getColorStateList(mHomeActivity, R.color.white));
        mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
        mHomeActivity.ibAttachment.setImageResource(R.drawable.ic_clear_cross);
        //mHomeActivity.ibMenu.setColorFilter(ContextCompat.getColor(mHomeActivity, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
        mHomeActivity.ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < mFilterBaseDatas.size(); i++) {
                    FilterBaseData mFilterBaseData = mFilterBaseDatas.get(i);
                    mFilterBaseData.selectedItemCount = 0;
                    FilterBaseData mUpdateFilterBaseData = mFilterBaseData;
                    if (!mUpdateFilterBaseData.isSelectedPosition)
                        mUpdateFilterBaseData.isSelectedPosition = true;
                    mFilterBaseDatas.set(i, mUpdateFilterBaseData);
                    FilterBaseData mFilterBaseDataUpdate = mFilterBaseDatas.get(i);
                    if (mFilterBaseDataUpdate.isSelectedPosition) {
                        if (mFilterBaseDataUpdate.selectedItemCount < mFilterBaseDatas.size()) {
                            mFilterBaseData.selectedItemCount = mFilterBaseData.selectedItemCount + 1;
                        }
                    } else {
                        if (mFilterBaseData.selectedItemCount > 0) {
                            mFilterBaseData.selectedItemCount = mFilterBaseData.selectedItemCount - 1;
                        }

                    }
                    mFilterBaseDatas.set(i, mFilterBaseDataUpdate);

                }
                mFilterListAdapter.getFilter().filter("");
                validateBlankSearchViewwithKeyboardClose();
                mHomeActivity.setmLocationFilterData(new ArrayList<FilterBaseData>());
                mHomeActivity.getmLocationFilterData().addAll(mFilterBaseDatas);
                mFilterListAdapter.notifyDataSetChanged();
                setSelectedUpdateCount();

            }
        });
        mHomeActivity.ibAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < mFilterBaseDatas.size(); i++) {
                    FilterBaseData mFilterBaseData = mFilterBaseDatas.get(i);
                    FilterBaseData mUpdateFilterBaseData = mFilterBaseData;
                    mUpdateFilterBaseData.isSelectedPosition = false;
                    mFilterBaseDatas.set(i, mUpdateFilterBaseData);

                    FilterBaseData mFilterBaseDataUpdate = mFilterBaseDatas.get(i);
                    if (mUpdateFilterBaseData.isSelectedPosition) {
                        mFilterBaseData.selectedItemCount = mFilterBaseData.selectedItemCount + 1;

                    } else {
                        if (mFilterBaseData.selectedItemCount > 0) {
                            mFilterBaseData.selectedItemCount = mFilterBaseData.selectedItemCount - 1;
                        }

                    }
                    mFilterBaseDatas.set(i, mFilterBaseDataUpdate);

                }

                mHomeActivity.setmLocationFilterData(new ArrayList<FilterBaseData>());
                mHomeActivity.getmLocationFilterData().addAll(mFilterBaseDatas);
                mFilterListAdapter.notifyDataSetChanged();
                setSelectedUpdateCount();
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHomeActivity.ibMenu.setColorFilter(null);
        mHomeActivity.ibAttachment.setColorFilter(null);


        /*Set<FilterBaseData> s= new HashSet<FilterBaseData>();
        s.addAll(mFilterBaseDatas);
        List<FilterBaseData> list = new ArrayList<FilterBaseData>();
        list.addAll(s);*/
        mHomeActivity.setmLocationFilterData(mFilterBaseDatas);
        //Clear query
        validateBlankSearchViewwithKeyboardClose();
    }

    public void validateBlankSearchViewwithKeyboardClose(){
        if (mSearchView != null) {

            mSearchView.setQuery("", false);
            //Collapse the action view
            mSearchView.onActionViewCollapsed();

        }
        final InputMethodManager imm = (InputMethodManager) mHomeActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    /**
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param regionId
     * @param activeStatus
     */
    void getAccessibleTenantLocationForFilter(int tenantId, String userId, String orderBy, String sort, String regionId, String activeStatus) {
        try {
            mCall = apiInterface.getAccessibleTenantLocationForSocialInteraction(tenantId, userId, orderBy, sort, regionId, activeStatus);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    mHomeActivity.endProgress();
                    if (response.body() != null) {
                        AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) response.body();
                        if (mAccessibleTenantLocationDataModel.getData() != null && mAccessibleTenantLocationDataModel.getData().size() > 0) {
                            for (AccessibleTenantLocationDataModel.Datum mDatum : mAccessibleTenantLocationDataModel.getData()) {
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(mDatum.getId());
                                mFilterBaseData.name = mDatum.getName();
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_location));
                                mFilterBaseData.filterMetricType = mDatum.getHotelMetricsDataType();
                                mFilterBaseDatas.add(mFilterBaseData);
                            }


                        } else {
                            mHomeActivity.endProgress();
                        }
                        setFilterdataListAdapter(mFilterBaseDatas);

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    mHomeActivity.endProgress();
                    mRecyclerViewFilterList.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList) {
        if (mFilterBaseDataList != null) {
            if (mFilterBaseDataList.size() > 0) {
                mRecyclerViewFilterList.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
                Collections.sort(mFilterBaseDataList, new Comparator<FilterBaseData>() {
                    public int compare(FilterBaseData v1, FilterBaseData v2) {
                        return v1.name.compareTo(v2.name);
                    }
                });
                mFilterListAdapter = new SocialInteractionFilterListAdapter(mHomeActivity, mFilterBaseDataList, this, true, "", "");
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                mRecyclerViewFilterList.setLayoutManager(mLayoutManager);
                mRecyclerViewFilterList.setItemAnimator(new DefaultItemAnimator());
                mRecyclerViewFilterList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                mRecyclerViewFilterList.setAdapter(mFilterListAdapter);
            } else {
                mRecyclerViewFilterList.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterPosition, List<FilterBaseData> mFilterBaseDataList) {

       /* FilterBaseData mFilterBaseDataUpdate = mFilterBaseData;
        if (mFilterBaseDataUpdate.isSelectedPosition) {
            mFilterBaseDataUpdate.selectedItemCount = mFilterBaseDataUpdate.selectedItemCount + 1;
        } else {
            mFilterBaseDataUpdate.selectedItemCount = mFilterBaseDataUpdate.selectedItemCount - 1;
        }
        mFilterBaseDatas.set(position, mFilterBaseData);
        // mHomeActivity.getmLocationFilterData().clear();

        if(list!=null){
            list.clear();
        }
        //Removing Duplicates;
        Set<FilterBaseData> s= new HashSet<FilterBaseData>();
        s.addAll(mFilterBaseDatas);

        list.addAll(s);
        setFilterdataListAdapter(list);*/
        mFilterBaseDatas = new ArrayList<>();
        mFilterBaseDatas.addAll(mFilterBaseDataList);
        mFilterListAdapter.notifyDataSetChanged();
        setSelectedUpdateCount();
    }

    public void setSelectedUpdateCount() {
        int count = 0;
        if (mFilterBaseDatas != null && mFilterBaseDatas.size() > 0) {
            for (FilterBaseData mFilterBaseData : mFilterBaseDatas) {
                if(mFilterBaseData.isSelectedPosition){
                    count = count + 1;
                }

            }
        }
        mHomeActivity.tvTitle.setText(String.valueOf(count) + " Selected");
    }


}
