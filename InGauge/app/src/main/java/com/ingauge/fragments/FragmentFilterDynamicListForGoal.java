package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.DynamicFilterLeftAdapter;
import com.ingauge.adapters.FilterDetailListAdapter;
import com.ingauge.adapters.FilterDetailListAdapterForGoal;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 11-Oct-17.
 */

public class FragmentFilterDynamicListForGoal extends BaseFragment implements View.OnClickListener, RecyclerViewClickListener {

    public static final String KEY_FILTER_SELECTION = "filter_seclection";
    public static final String KEY_FILTER_DETAILS = "filter_details";
    public static final String KEY_FILTER_DETAILS_ONE = "filter_details_one";
    public static final String KEY_FILTER_DETAILS_TWO = "filter_details_two";
    public static final String KEY_FILTER_DETAILS_THREE = "filter_details_three";
    public static final String KEY_FILTER_DETAILS_FOUR = "filter_details_four";
    public static final String KEY_FILTER_DETAILS_FIVE = "filter_details_five";
    public static final String KEY_FILTER_DETAILS_SIX = "filter_details_six";
    public static final String KEY_SELECTION_POSITION = "seclected_position";

    Context mContext;
    View rootView;
    HomeActivity mHomeActivity;
    ArrayList<FilterBaseData> mLeftFilterBaseDatas;
    ArrayList<FilterBaseData> mRightFilterBaseDatas;
    Call mCall;
    APIInterface apiInterface;


    private TextView tvFilterClear;
    private TextView tvApply;


    private RecyclerView rvFilterList;
    private RecyclerView rvFilterLeftList;
    public static SearchView mSearchView;


    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModelForGoal;
    private DashboardLocationGroupListModel mDashboardLocationGroupListModelForGoal;
    private ProductListFromLocationGroupModel mProductListFromLocationGroupModelForGoal;
    private DashboardUserListModel mDashboardUserListModelForGoal;

    FilterDetailListAdapterForGoal mFilterListAdapter;
    DynamicFilterLeftAdapter mDynamicFilterLeftAdapter;


    String selectedId = "";
    String selectedName = "";
    boolean isClickClear=false;
    boolean isReadyOnClear=false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mLeftFilterBaseDatas = new ArrayList<>();
        mRightFilterBaseDatas = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dynamic_dashboard_filter, container, false);
        initView(rootView);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mFilterListAdapter.getFilter().filter(newText);
                return false;
            }
        });
        fetchinitalDataForFilter();
        //updateLeftUsability(1);
        selectedId = String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_location_id_for_goal), 1));
        selectedName = InGaugeSession.read(getString(R.string.key_selected_tenant_location_name_for_goal), "");
        getRightFilterData(1, selectedId, selectedName);
        return rootView;
    }

    public void initView(View itemView) {
        tvFilterClear = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_clear_all);
        tvApply = (TextView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_tv_apply);
        rvFilterLeftList = (RecyclerView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_rv_left);
        rvFilterList = (RecyclerView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_rv);
        mSearchView = (SearchView) itemView.findViewById(R.id.fragment_dynamic_dashboard_filter_search_view);
        tvFilterClear.setOnClickListener(this);
        tvApply.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_dynamic_dashboard_filter_tv_clear_all:
                isClickClear=true;
                mLeftFilterBaseDatas.clear();
                mRightFilterBaseDatas.clear();
                mHomeActivity.startProgress(mHomeActivity);
                getAccessibleTenantLocation(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                        String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), "name", "ASC", "Normal", true);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if(isClickClear && isReadyOnClear){
                            mHomeActivity.endProgress();
                            getRightFilterData(1, selectedId, selectedName);
                            setLeftFilterdataListAdapter(mLeftFilterBaseDatas);

                        }
                    }
                }, 3500);
                //mHomeActivity.onBackPressed();
               /* try {



                    // fetchinitalDataForFilter();

                    mLeftFilterBaseDatas.clear();



                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if(isClickClear && isReadyOnClear){
                                getRightFilterData(1, selectedId, selectedName);
                                setLeftFilterdataListAdapter(mLeftFilterBaseDatas);

                            }
                        }
                    }, 3500);
                    *//*listadapter.notifyDataSetChanged();
                    rlProgress.setVisibility(View.VISIBLE);
                    rvFilterList.setVisibility(View.GONE);
                    getRegionTypes(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1));*//*

                } catch (Exception e) {
                    e.printStackTrace();
                }*/


                //fetchinitalDataForFilter();
                //updateLeftUsability(1);
                //selectedId = String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_location_id_for_goal), 1));
                //selectedName = InGaugeSession.read(getString(R.string.key_selected_tenant_location_name_for_goal), "");
                getRightFilterData(1, selectedId, selectedName);


                break;
            case R.id.fragment_dynamic_dashboard_filter_tv_apply:
                clickOnApplytoUpdatePreference();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_title)));
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
    }

    public void fetchinitalDataForFilter() {

        mAccessibleTenantLocationDataModelForGoal = mHomeActivity.mAccessibleTenantLocationDataModelForGoal;
        mDashboardLocationGroupListModelForGoal = mHomeActivity.mDashboardLocationGroupListModelForGoal;
        mProductListFromLocationGroupModelForGoal = mHomeActivity.mProductListFromLocationGroupModelForGoal;
        mDashboardUserListModelForGoal = mHomeActivity.mDashboardUserListModelForGoal;
        mLeftFilterBaseDatas.clear();
        for (int i = 0; i < 4; i++) {
            FilterBaseData mFilterBaseData = new FilterBaseData();
            if (i == 0) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_location_id_for_goal), 1));
                mFilterBaseData.name = InGaugeSession.read(getString(R.string.key_selected_tenant_location_name_for_goal), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
            } else if (i == 1) {
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), "");
                mFilterBaseData.name = InGaugeSession.read(getString(R.string.key_selected_location_group_name_for_goal), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
            } else if (i == 2) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(getString(R.string.key_selected_location_group_product_id_for_goal), 1));
                mFilterBaseData.name = InGaugeSession.read(getString(R.string.key_selected_location_group_product_name_for_goal), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
            } else if (i == 3) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(getString(R.string.key_selected_user_id_for_goal), 1));
                mFilterBaseData.name = InGaugeSession.read(getString(R.string.key_selected_user_name_for_goal), "");
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
            }
            mFilterBaseData.filterIndex = i;
            mLeftFilterBaseDatas.add(mFilterBaseData);
        }
        setLeftFilterdataListAdapter(mLeftFilterBaseDatas);
    }


    private void getAccessibleTenantLocation(AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel, String selectedid, String selectedname) {
        if (mAccessibleTenantLocationDataModel != null) {
            int count = 0;
            for (AccessibleTenantLocationDataModel.Datum mDatum : mAccessibleTenantLocationDataModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterMetricType = mDatum.getHotelMetricsDataType();
                //mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                mFilterBaseData.filterIndex = 0;
                if (mFilterBaseData.id.equalsIgnoreCase(selectedid)) {
                    mFilterBaseData.isSelectedPosition = true;
                } else {
                    mFilterBaseData.isSelectedPosition = false;
                }
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }else{
            setFilterdataListAdapter(new ArrayList<FilterBaseData>(), selectedid, selectedname);
        }
    }

    private void getDashboardLocationGroupList(DashboardLocationGroupListModel mGetAccessibleTenantLocationData, String selectedid, String selectedname) {
        if (mGetAccessibleTenantLocationData != null) {
            int count = 0;
            for (DashboardLocationGroupListModel.Datum mDatum : mGetAccessibleTenantLocationData.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getLocationGroupID());
                mFilterBaseData.name = mDatum.getLocationGroupName();
                //mFilterBaseData.filterName = getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                mFilterBaseData.filterIndex = 1;
                if (mFilterBaseData.id.equalsIgnoreCase(selectedid)) {
                    mFilterBaseData.isSelectedPosition = true;
                } else {
                    mFilterBaseData.isSelectedPosition = false;
                }
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }else{
            setFilterdataListAdapter(new ArrayList<FilterBaseData>(), selectedid, selectedname);
        }
    }

    private void getProductListFromLocationGroup(ProductListFromLocationGroupModel mProductListFromLocationGroupModel, String selectedid, String selectedname) {
        List<FilterBaseData> mIncrementalARPD = new ArrayList<>();
        List<FilterBaseData> mProductList = new ArrayList<>();
        if (mProductListFromLocationGroupModel != null) {
            int count = 0;


            for (ProductListFromLocationGroupModel.Datum mDatum : mProductListFromLocationGroupModel.getData()) {
                if (!mDatum.getIsMajor()) {
                    FilterBaseData mFilterBaseData = new FilterBaseData();
                    mFilterBaseData.id = String.valueOf(mDatum.getId());
                    mFilterBaseData.name = mDatum.getName();
                    //mFilterBaseData.filterName = getString(R.string.product);
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                    mFilterBaseData.filterIndex = 2;
                    if (mFilterBaseData.id.equalsIgnoreCase(selectedid)) {
                        mFilterBaseData.isSelectedPosition = true;
                    } else {
                        mFilterBaseData.isSelectedPosition = false;
                    }
                    count++;
                    mProductList.add(mFilterBaseData);
                }

            }
            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.id = String.valueOf(-1);
            mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
            mFilterBaseData.filterName = getString(R.string.product);
            mFilterBaseData.filterIndex = 2;
            if (mFilterBaseData.id.equalsIgnoreCase(selectedid)) {
                mFilterBaseData.isSelectedPosition = true;
            } else {
                mFilterBaseData.isSelectedPosition = false;
            }
            mIncrementalARPD.add(mFilterBaseData);
            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)) {
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(-2);
                mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_goal_product_filter_upsell_product));
                mFilterBaseData.filterName = getString(R.string.product);
                mFilterBaseData.filterIndex = 2;
                if (mFilterBaseData.id.equalsIgnoreCase(selectedid)) {
                    mFilterBaseData.isSelectedPosition = true;
                } else {
                    mFilterBaseData.isSelectedPosition = false;
                }
                mIncrementalARPD.add(mFilterBaseData);
            }

            mRightFilterBaseDatas.addAll(mIncrementalARPD);
            mRightFilterBaseDatas.addAll(mProductList);
            setFilterdataListAdapterForProduct(mRightFilterBaseDatas, selectedid, selectedname);
        } else {
            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.id = String.valueOf(-1);
            mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
            mFilterBaseData.filterName = getString(R.string.product);
            mFilterBaseData.filterIndex = 2;
            if (mFilterBaseData.id.equalsIgnoreCase(selectedid)) {
                mFilterBaseData.isSelectedPosition = true;
            } else {
                mFilterBaseData.isSelectedPosition = false;
            }
            mIncrementalARPD.add(mFilterBaseData);
            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)) {
                mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(-2);
                mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_goal_product_filter_upsell_product));
                mFilterBaseData.filterName = getString(R.string.product);
                mFilterBaseData.filterIndex = 2;
                if (mFilterBaseData.id.equalsIgnoreCase(selectedid)) {
                    mFilterBaseData.isSelectedPosition = true;
                } else {
                    mFilterBaseData.isSelectedPosition = false;
                }
                mIncrementalARPD.add(mFilterBaseData);
            }

            mRightFilterBaseDatas.addAll(mIncrementalARPD);
            setFilterdataListAdapterForProduct(mRightFilterBaseDatas, selectedid, selectedname);
        }
    }

    private void getDashboardUserList(DashboardUserListModel mDashboardUserListModel, String selectedid, String selectedname) {
        if (mDashboardUserListModel != null) {
            int count = 0;
            for (DashboardUserListModel.Datum mDatum : mDashboardUserListModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                //mFilterBaseData.filterName = getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                mFilterBaseData.filterIndex = 3;
                if (mFilterBaseData.id.equalsIgnoreCase(selectedid)) {

                    mFilterBaseData.isSelectedPosition = true;
                } else {
                    mFilterBaseData.isSelectedPosition = false;
                }
                count++;
                mRightFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mRightFilterBaseDatas, selectedid, selectedname);
        }else{
            setFilterdataListAdapter(new ArrayList<FilterBaseData>(), selectedid, selectedname);
        }
    }

    private void setFilterdataListAdapterForProduct(List<FilterBaseData> mFilterBaseDataList, String selectedid, String selectedname) {
        if (mFilterBaseDataList != null) {
            if (mFilterBaseDataList.size() > 0) {
                int selectedPosiotn=0;
                for(FilterBaseData mFilterBaseData: mFilterBaseDataList){
                    if(mFilterBaseData.isSelectedPosition){
                        selectedPosiotn = mFilterBaseDataList.indexOf(mFilterBaseData);
                        mFilterBaseDataList.remove(selectedPosiotn);
                        mFilterBaseDataList.add(0,mFilterBaseData);

                        break;
                    }
                }
                mFilterListAdapter = new FilterDetailListAdapterForGoal(mHomeActivity, mFilterBaseDataList, this, true, selectedid, selectedname);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                rvFilterList.setAdapter(mFilterListAdapter);
            }
        }
    }

    private void setFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList, String selectedid, String selectedname) {
        if (mFilterBaseDataList != null) {
            if (mFilterBaseDataList.size() > 0) {
                Collections.sort(mFilterBaseDataList, new Comparator<FilterBaseData>() {
                    public int compare(FilterBaseData v1, FilterBaseData v2) {
                        return v1.name.compareTo(v2.name);
                    }
                });
                int selectedPosiotn=0;
                for(FilterBaseData mFilterBaseData: mFilterBaseDataList){
                    if(mFilterBaseData.isSelectedPosition){
                        selectedPosiotn = mFilterBaseDataList.indexOf(mFilterBaseData);
                        mFilterBaseDataList.remove(selectedPosiotn);
                        mFilterBaseDataList.add(0,mFilterBaseData);

                        break;
                    }
                }
                mFilterListAdapter = new FilterDetailListAdapterForGoal(mHomeActivity, mFilterBaseDataList, this, true, selectedid, selectedname);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                rvFilterList.setAdapter(mFilterListAdapter);
            }else {
                if(mFilterListAdapter!=null){
                    mFilterListAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void setLeftFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList) {
        if (mFilterBaseDataList != null) {
            if (mFilterBaseDataList.size() > 0) {

                mDynamicFilterLeftAdapter = new DynamicFilterLeftAdapter(mHomeActivity, mFilterBaseDataList, this, true, 0);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterLeftList.setLayoutManager(mLayoutManager);
                rvFilterLeftList.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
                rvFilterLeftList.setItemAnimator(new DefaultItemAnimator());
                rvFilterLeftList.setAdapter(mDynamicFilterLeftAdapter);
            }
        }
    }


    void getRightFilterData(int position, String selectedId, String selectedname) {
        mRightFilterBaseDatas.clear();
        switch (position) {
            case 1:
                //mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getAccessibleTenantLocation(mAccessibleTenantLocationDataModelForGoal, selectedId, selectedname);
                break;
            case 2:
                //mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getDashboardLocationGroupList(mDashboardLocationGroupListModelForGoal, selectedId, selectedname);
                break;
            case 3:
                //mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getProductListFromLocationGroup(mProductListFromLocationGroupModelForGoal, selectedId, selectedname);
                break;
            case 4:
                //mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getDashboardUserList(mDashboardUserListModelForGoal, selectedId, selectedname);
                break;

        }

    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterPosition, boolean isLeftFilter) {

        if (isLeftFilter) {
            getRightFilterData(position + 1, mFilterBaseData.id, mFilterBaseData.name);
            Logger.Error("<<< Left Click >>>" + position + "Filter Position " + filterPosition);
        } else {
            Logger.Error("<<< Right Click >>>" + position + "Filter Position " + filterPosition);
            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
            mDynamicFilterLeftAdapter.notifyDataSetChanged();
            updateFilterData(mFilterBaseData, position);
        }

        //Clear query
        if(mSearchView!=null){

            mSearchView.setQuery("", false);
            //Collapse the action view
            mSearchView.onActionViewCollapsed();

        }
        final InputMethodManager imm = (InputMethodManager) mHomeActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public void updateFilterData(FilterBaseData mFilterBaseData, int clickedPosition) {

        //  isLoaded = true;
        InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), true);
        //mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
        int index = mFilterBaseData.filterIndex + 1;

        switch (index) {

            case 0:
                mHomeActivity.startProgress(mHomeActivity);
                Logger.Error(" Filter  " + mFilterBaseData.id);


                getAccessibleTenantLocationForFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                        String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), "name", "ASC", "Normal", false);
                break;
            case 1:
                mHomeActivity.startProgress(mHomeActivity);
                getDashboardLocationGroupListForFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), mFilterBaseData.id, String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)));
                break;
            case 2:
                mHomeActivity.startProgress(mHomeActivity);
                getProductListForFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), mFilterBaseData.id, "" + mLeftFilterBaseDatas.get(0).id);
                break;
            case 3:
                mHomeActivity.startProgress(mHomeActivity);
                getDashboardUserListForFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), "" + mLeftFilterBaseDatas.get(0).id, mLeftFilterBaseDatas.get(1).id);
                break;

        }
    }

    private void VoiUpdateFilterData(int filterindex) {
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "";
        mFilterBaseData.name = "";
        mFilterBaseData.filterIndex = filterindex;
        switch (filterindex) {
            case 0:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                mAccessibleTenantLocationDataModelForGoal = null;
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
                InGaugeSession.write(mContext.getString(R.string.key_location_obj), json);*/
                break;
            case 1:
                mDashboardLocationGroupListModelForGoal = null;
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
                InGaugeSession.write(mContext.getString(R.string.key_location_group_obj), json);*/
                break;
            case 2:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                mProductListFromLocationGroupModelForGoal = null;
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModel);
                InGaugeSession.write(mContext.getString(R.string.key_product_obj), json);*/
                break;
            case 3:
                // mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                mDashboardUserListModelForGoal = null;
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mDashboardUserListModel);
                InGaugeSession.write(mContext.getString(R.string.key_user_obj), json);*/
                break;
        }
        if (mDynamicFilterLeftAdapter != null) {
            if (filterindex >= mLeftFilterBaseDatas.size()) {
                mLeftFilterBaseDatas.add(mFilterBaseData);
            } else {
                mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
            }

            mDynamicFilterLeftAdapter.notifyDataSetChanged();
        } else {
            if (mDynamicFilterLeftAdapter != null) {
                mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                mDynamicFilterLeftAdapter.notifyDataSetChanged();

            } else {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                mLeftFilterBaseDatas.add(mFilterBaseData.filterIndex, mFilterBaseData);
                rvFilterLeftList.setAdapter(mDynamicFilterLeftAdapter);

            }
        }

    }


    /**
     * @param authToken
     * @param tenantId
     * @param orderBy
     * @param sort
     * @param activeStatus
     */

    void getAccessibleTenantLocationForFilter(final String authToken, int tenantId, String userId, String orderBy, String sort, String activeStatus, final boolean isNeedUpdateOnApply) {

        if (isNeedUpdateOnApply) {
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), true);
        }
        try {
            mCall = apiInterface.getAccessibleTenantLocationForGoalFilter(tenantId, userId, orderBy, sort, activeStatus);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mAccessibleTenantLocationDataModelForGoal = (AccessibleTenantLocationDataModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_location_obj_for_goal), json);*/
                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mAccessibleTenantLocationDataModelForGoal.getData() != null && mAccessibleTenantLocationDataModelForGoal.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId());
                            mFilterBaseData.name = mAccessibleTenantLocationDataModelForGoal.getData().get(0).getName();
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_region_currency), mAccessibleTenantLocationDataModelForGoal.getData().get(0).getRegionCurrency());
                            mFilterBaseData.filterIndex = 0;
                            //mFilterBaseData.filterName = getString(R.string.location);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);


                            //mLeftFilterBaseDatas.add(mFilterBaseData);
                            //}

                            //String authToken,String tenantlocationId,String userId
                            //  InGaugeSession.write(getString(R.string.key_tenant_location_id), mAccessibleTenantLocationDataModel.getData().get(0).getId());
                            getDashboardLocationGroupListForFilter(authToken, String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId()), String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)));
                        } else {
                            VoiUpdateFilterData(0);
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);

                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupListForFilter(final String authToken, final String tenantlocationId, String userId) {
        try {
            mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), true);
                    if (response.body() != null) {
                        mDashboardLocationGroupListModelForGoal = (DashboardLocationGroupListModel) response.body();
                        if (mDashboardLocationGroupListModelForGoal != null && mDashboardLocationGroupListModelForGoal.getData().size() > 0) {
                            /*mHomeActivity.mDashboardLocationGroupListModelForGoal = mDashboardLocationGroupListModelForGoal;
                            Gson gson = new Gson();
                            String json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModelForGoal);
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_group_obj_for_goal), json);*/

                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID());
                            mFilterBaseData.name = mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupName();
                            mFilterBaseData.filterIndex = 1;
                            //mFilterBaseData.filterName = getString(R.string.location_group);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                            mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            getProductListForFilter(authToken, String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID()), tenantlocationId);
                        } else {
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);
                            mHomeActivity.endProgress();
                        }
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductListForFilter(final String authToken, final String locationGroupId, final String tenantlocationId) {

        try {
            mCall = apiInterface.getProductList(locationGroupId);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), true);
                    if (response.body() != null) {
                        mProductListFromLocationGroupModelForGoal = (ProductListFromLocationGroupModel) response.body();
                        /*mHomeActivity.mProductListFromLocationGroupModelForGoal = mProductListFromLocationGroupModelForGoal;
                        Gson gson = new Gson();
                        String json = gson.toJson(mProductListFromLocationGroupModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_product_obj_for_goal), json);*/

                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                            /*mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                            mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName();*/

                        mFilterBaseData.id = String.valueOf(-1);
                        if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)) {
                            mFilterBaseData.name = "ARPD";
                        } else {
                            mFilterBaseData.name = "Incremental Revenue";
                        }
                        mFilterBaseData.filterIndex = 2;
                        //mFilterBaseData.filterName = getString(R.string.product);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                        //  mLeftFilterBaseDatas.add(mFilterBaseData);


                        mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);


                        //   }
                        //String authToken, String tenantLocationI
                        // d, String startDate, String endDate, String locationGroupId
                        // InGaugeSession.write(getString(R.string.key_selected_location_group_product), (mProductListFromLocationGroupModel.getData().get(0).getId()));
                        getDashboardUserListForFilter(authToken, tenantlocationId, locationGroupId);


                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param tenantLocationId
     * @param locationGroupId
     */
    void getDashboardUserListForFilter(String authToken, String tenantLocationId, String locationGroupId) {

        try {
            mCall = apiInterface.getDashboardUserListForGoalFilter(tenantLocationId, locationGroupId, true);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), true);
                    if (response.body() != null) {
                        mDashboardUserListModelForGoal = (DashboardUserListModel) response.body();
                        /*mHomeActivity.mDashboardUserListModelForGoal= mDashboardUserListModelForGoal;
                        Gson gson = new Gson();
                        String json = gson.toJson(mDashboardUserListModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_user_obj_for_goal), json);*/


                        if (mDashboardUserListModelForGoal != null) {

                            if (mDashboardUserListModelForGoal.getData() != null && mDashboardUserListModelForGoal.getData().size() > 0) {
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(mDashboardUserListModelForGoal.getData().get(0).getId());
                                mFilterBaseData.name = mDashboardUserListModelForGoal.getData().get(0).getName();
                                mFilterBaseData.filterIndex = 3;
                                //mFilterBaseData.filterName = getString(R.string.user);
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                //mLeftFilterBaseDatas.add(mFilterBaseData);


                                mLeftFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);


                                mHomeActivity.endProgress();
                                // InGaugeSession.write(getString(R.string.key_selected_user), mDashboardUserListModel.getData().get(0).getId());
                                //setLeftFilterdataListAdapter(mLeftFilterBaseDatas);
                                mDynamicFilterLeftAdapter.notifyDataSetChanged();
                            } else {
                                VoiUpdateFilterData(3);

                                mHomeActivity.endProgress();
                            }
                        } else {
                            VoiUpdateFilterData(3);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clickOnApplytoUpdatePreference() {


        if (InGaugeSession.read(getString(R.string.key_is_need_update_on_apply_for_goal), false)) {
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), false);

            mHomeActivity.mAccessibleTenantLocationDataModelForGoal = null;
            mHomeActivity.mAccessibleTenantLocationDataModelForGoal = mAccessibleTenantLocationDataModelForGoal;
            Gson gson = new Gson();
            String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForGoal);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_obj_for_goal), json);


            mHomeActivity.mDashboardLocationGroupListModelForGoal = null;
            mHomeActivity.mDashboardLocationGroupListModelForGoal = mDashboardLocationGroupListModelForGoal;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModelForGoal);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_group_obj_for_goal), json);

            mHomeActivity.mProductListFromLocationGroupModelForGoal = null;
            mHomeActivity.mProductListFromLocationGroupModelForGoal = mProductListFromLocationGroupModelForGoal;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModelForGoal);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_product_obj_for_goal), json);


            mHomeActivity.mDashboardUserListModelForGoal = null;
            mHomeActivity.mDashboardUserListModelForGoal = mDashboardUserListModelForGoal;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mDashboardUserListModelForGoal);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_user_obj_for_goal), json);
        }
        for (int i = 0; i < mLeftFilterBaseDatas.size(); i++) {
            switch (i) {

                case 0:
                    FilterBaseData mFilterBaseData = mLeftFilterBaseDatas.get(i);
                    mFilterBaseData = mLeftFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_default_goal), Integer.parseInt(mFilterBaseData.id));
                    }
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), mFilterBaseData.name);
                    break;
                case 1:
                    mFilterBaseData = mLeftFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(1));
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), mFilterBaseData.id);
                        InGaugeSession.write(getString(R.string.key_selected_location_group_id_for_default_goal), mFilterBaseData.id);

                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name_for_goal), mFilterBaseData.name);
                    break;
                case 2:
                    mFilterBaseData = mLeftFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(getString(R.string.key_selected_location_group_product_id_for_default_goal), Integer.parseInt(mFilterBaseData.id));

                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), mFilterBaseData.name);
                    break;
                case 3:
                    mFilterBaseData = mLeftFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), Integer.parseInt(mFilterBaseData.id));
                        InGaugeSession.write(getString(R.string.key_selected_user_id_for_default_goal), Integer.parseInt(mFilterBaseData.id));
                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), mFilterBaseData.name);
                    break;

            }
        }


        Bundle bundle = new Bundle();
        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
        bundle.putString("industry", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
        bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), ""));
        bundle.putString("location_group", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name_for_goal), ""));
        bundle.putString("product", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), ""));
        bundle.putString("user", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), ""));
        InGaugeApp.getFirebaseAnalytics().logEvent("change_goal_filter", bundle);
        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_update_goal_settings_filter), true);
        mHomeActivity.onBackPressed();
    }


    /**
     * @param authToken
     * @param tenantId
     * @param orderBy
     * @param sort
     * @param activeStatus
     */

    void getAccessibleTenantLocation(final String authToken, int tenantId, String userId, String orderBy, String sort, String activeStatus, final boolean isNeedUpdateOnApply) {

        if (isNeedUpdateOnApply) {
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), true);
        }
        try {
            mCall = apiInterface.getAccessibleTenantLocationForGoalFilter(tenantId, userId, orderBy, sort, activeStatus);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mAccessibleTenantLocationDataModelForGoal = (AccessibleTenantLocationDataModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForGoal);
                        InGaugeSession.write(mContext.getString(R.string.key_location_obj_for_goal), json);*/
                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mAccessibleTenantLocationDataModelForGoal.getData() != null && mAccessibleTenantLocationDataModelForGoal.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId());
                            mFilterBaseData.name = mAccessibleTenantLocationDataModelForGoal.getData().get(0).getName();
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_region_currency), mAccessibleTenantLocationDataModelForGoal.getData().get(0).getRegionCurrency());
                            mFilterBaseData.filterIndex = 0;
                            //mFilterBaseData.filterName = getString(R.string.location);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                            selectedId= mFilterBaseData.id;
                            selectedName = mFilterBaseData.name;
//                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), Integer.parseInt(mFilterBaseData.id));
//                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), mFilterBaseData.name);
                            mLeftFilterBaseDatas.add(mFilterBaseData);


                            //mLeftFilterBaseDatas.add(mFilterBaseData);
                            //}

                            //String authToken,String tenantlocationId,String userId
                            //  InGaugeSession.write(getString(R.string.key_tenant_location_id), mAccessibleTenantLocationDataModel.getData().get(0).getId());
                            getDashboardLocationGroupList(authToken, String.valueOf(mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId()), String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)));

                        } else {
                            isReadyOnClear=true;
                            VoiUpdateFilterData(0);
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);

                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupList(final String authToken, final String tenantlocationId, String userId) {

        mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {


                    mDashboardLocationGroupListModelForGoal = (DashboardLocationGroupListModel) response.body();

                    //String authToken,String locationGroupId
                    if ((mDashboardLocationGroupListModelForGoal != null && (mDashboardLocationGroupListModelForGoal.getData() != null && mDashboardLocationGroupListModelForGoal.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID());
                        mFilterBaseData.name = mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupName();
                        mFilterBaseData.filterIndex = 1;
                        //mFilterBaseData.filterName =mContext.getResources().getString(R.string.location_group);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                        selectedId= mFilterBaseData.id;
                        selectedName = mFilterBaseData.name;
//                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), mFilterBaseData.id);
//                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name_for_goal), mFilterBaseData.name);
                        mLeftFilterBaseDatas.add(mFilterBaseData);

                        getProductList(authToken, String.valueOf(mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID()), tenantlocationId);
                    } else {
                        isReadyOnClear=true;
                        VoiUpdateFilterData(1);
                        VoiUpdateFilterData(3);
                        VoiUpdateFilterData(3);


                    }
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    VoiUpdateFilterData(1);
                    VoiUpdateFilterData(2);
                    VoiUpdateFilterData(3);
                    mHomeActivity.endProgress();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                VoiUpdateFilterData(1);
                VoiUpdateFilterData(2);
                VoiUpdateFilterData(3);
                mHomeActivity.endProgress();

            }
        });
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductList(final String authToken, final String locationGroupId, final String tenantLocationId) {

        mCall = apiInterface.getProductList(locationGroupId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {


                    mProductListFromLocationGroupModelForGoal = (ProductListFromLocationGroupModel) response.body();

                    if ((mProductListFromLocationGroupModelForGoal != null && (mProductListFromLocationGroupModelForGoal.getData() != null && mProductListFromLocationGroupModelForGoal.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                        mFilterBaseData.name = mProductListFromLocationGroupModelForGoal.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 2;
                        //mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                        selectedId= mFilterBaseData.id;
                        selectedName = mFilterBaseData.name;
//                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), Integer.parseInt(mFilterBaseData.id));
//                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), mFilterBaseData.name);
                        mLeftFilterBaseDatas.add(mFilterBaseData);

                        //String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId
                        getDashboardUserList(authToken, tenantLocationId, InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), locationGroupId);
                    } else {
                        isReadyOnClear=true;
                        VoiUpdateFilterData(2);
                        VoiUpdateFilterData(3);
                        mHomeActivity.endProgress();

                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    VoiUpdateFilterData(2);
                    VoiUpdateFilterData(3);
                    mHomeActivity.endProgress();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                VoiUpdateFilterData(2);
                VoiUpdateFilterData(3);
                mHomeActivity.endProgress();

            }
        });
    }


    /**
     * @param authToken
     * @param tenantLocationId
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserList(String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId) {
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);

        mCall = apiInterface.getDashboardUserList(TenantId, startDate, endDate,tenantLocationId, locationGroupId, true,false,false);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.body() != null) {
                    isReadyOnClear=true;

                    mDashboardUserListModelForGoal = (DashboardUserListModel) response.body();

                    if ((mDashboardUserListModelForGoal != null && (mDashboardUserListModelForGoal.getData() != null && mDashboardUserListModelForGoal.getData().size() > 0))) {

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mDashboardUserListModelForGoal.getData().get(0).getId());
                        mFilterBaseData.name = mDashboardUserListModelForGoal.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 3;
                        //mFilterBaseData.filterName = getString(R.string.user);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                        selectedId= mFilterBaseData.id;
                        selectedName = mFilterBaseData.name;
//                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), Integer.parseInt(mFilterBaseData.id));
//                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), mFilterBaseData.name);
                        mLeftFilterBaseDatas.add(mFilterBaseData);
                        setLeftFilterdataListAdapter(mLeftFilterBaseDatas);


                    } else {
                        isReadyOnClear=true;
                        VoiUpdateFilterData(3);


                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    VoiUpdateFilterData(3);


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                VoiUpdateFilterData(5);
                mHomeActivity.endProgress();

            }
        });
    }


}
