package com.ingauge.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.BaseActivity;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.GoalProgressExpandAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.GoalCurrentMonthMap;
import com.ingauge.pojo.GoalProgressModel;
import com.ingauge.pojo.GoalSettingModelNew;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;
import com.ingauge.widget.MonthYearPickerDialog;
import com.timqi.sectorprogressview.SectorProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 15-Jun-17.
 */
public class GoalSettings extends BaseFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {


    public static String KEY_SELECTED_DAY = "key_selected_day";
    public static String KEY_SELECTED_MONTH = "key_selected_month";
    public static String KEY_SELECTED_YEAR = "key_selected_year";

    HomeActivity mHomeActivity;
    BaseActivity mBaseActivity;
    Context mContext;
    View mView;
    private RelativeLayout rlDate;
    Call mCall;
    APIInterface apiInterface;
    ArrayList<FilterBaseData> mFilterBaseDatas;
    private ExpandableListView mExpandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableWeeklyGoalDetails;
    List<String> expandableListTitle;
    List<String> expandableGoalDetails;
    List<String> expandableRevenueDetails;
    List<String> expandableEffortDetails;
    LinkedHashMap<String, List<String>> expandableListDetail;
    SectorProgressView chartGoalAchieved, chartmyGoalAchieved, chartTimeElapsed;
    TextView tvChartGoalAchieved, tvChartMyGoalAchieved, tvChartTimeElapsed;
    private ImageView imgFilter;
    private ProgressBar pbFilter;
    private View expHeader;
    private TextView tvSelectedDate;
    private String STR_MONTH[] = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private TextView txtInGaugeRecommndGoal;
    private TextView txtNoGoal;
    private TextView txtMyIngaugeGoal;
    private TextView txtIncrementalRevenue;

    private CardView mCardViewMyGoal;
    private TextView txtInGaugeRecommndGoalTitle;
    private TextView txtMyIngaugeGoalTitle;
    private TextView tvChartGoalAchievedTitle;
    private TextView tvChartMyGoalAchievedTitle;
    private TextView tvChartTimeElapsedTitle;
    private LinearLayout llMyGoalAchieved;

    private DateFormat dateFormatMonth;
    private DateFormat dateFormatYear;
    private TextView tvDisplayIncrementalRevenue;
    int counter = 0;
    double VAL_IN_GOAL_ACHIEVED = 0.0;
    double VAL_MY_GOAL_ACHIEVED = 0.0;
    double VAL_TIME_ELAPSED = 0.0;
    int MILIS_IN_FUTURE = 10000;
    int PROGRESS_SPEED = 1;
    private ArrayList<GoalCurrentMonthMap> productGoalsDetails;
    Calendar selectedCalendar = Calendar.getInstance();
    int selectedMonth = 0;
    int selectedYear = 0;
    int selectedDay = 0;
    boolean isNeedFilterUpdate=false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
        mBaseActivity = (BaseActivity) mContext;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFilterBaseDatas = new ArrayList<>();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        isNeedFilterUpdate = InGaugeSession.read(mContext.getResources().getString(R.string.key_is_need_filter_call_goal), false);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_goal_settings, container, false);
            expHeader = inflater.inflate(R.layout.fragment_goal_blocks, null);
            initViews(mView);


//
            int locationid = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_default_goal), -2);
            String locationGroupID = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_default_goal), String.valueOf(-2));
            int productId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_default_goal), -2);
            int userId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_id_for_default_goal), -2);


            Date date = new Date();
            dateFormatMonth = new SimpleDateFormat("MM");
            dateFormatYear = new SimpleDateFormat("yyyy");

            InGaugeSession.write(mContext.getString(R.string.key_selected_month_for_goal), Integer.parseInt(dateFormatMonth.format(date)));
            InGaugeSession.write(mContext.getString(R.string.key_selected_year_for_goal), Integer.parseInt(dateFormatYear.format(date)));
            int savedMonth = InGaugeSession.read(mContext.getString(R.string.key_selected_month_for_goal), 0);
            int savedYear = InGaugeSession.read(mContext.getString(R.string.key_selected_year_for_goal), 0);
            Logger.Error("Month In text View " + STR_MONTH[savedMonth - 1]);
            Logger.Error("Year In text View " + savedYear);

            Logger.Error("Month In API " + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_month_for_goal), 1));
            Logger.Error("Year In API" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_year_for_goal), 1));
            tvSelectedDate.setText("Month: " + STR_MONTH[savedMonth - 1] + " - Year " + savedYear);
            if (!(locationid < 0 || Integer.parseInt(locationGroupID) < 0 || userId < 0)) {

                if(!isNeedFilterUpdate ){

                    getGoalProgress(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), -2)),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_month_for_goal), -2),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_year_for_goal), -2),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -2),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), -2),
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(-2)),
                            InGaugeSession.read(mContext.getString(R.string.key_selected_user_id_for_goal), -2),
                            InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product_id_for_goal), -2));
                }

                // getGoalProgress(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
            } else {
                txtNoGoal.setVisibility(View.VISIBLE);
                txtNoGoal.setText(mContext.getResources().getString(R.string.no_goal_found));
            }
        } else {
            if (mView != null && mView.getParent() != null) {
                ((ViewGroup) mView.getParent()).removeView(mView);
            }
        }


        return mView;
    }

    private void initViews(View view) {

        txtNoGoal = (TextView) view.findViewById(R.id.tv_no_goal);
        rlDate = (RelativeLayout) view.findViewById(R.id.relative_date_compare);
        rlDate.setOnClickListener(this);
        pbFilter = (ProgressBar) view.findViewById(R.id.fragment_goal_settings_pb);
        imgFilter = (ImageView) view.findViewById(R.id.fragment_goal_settings_iv_filter);
        tvSelectedDate = (TextView) view.findViewById(R.id.tv_selected_date);
        imgFilter.setOnClickListener(this);
        mExpandableListView = (ExpandableListView) view.findViewById(R.id.fragment_goal_progress_expandlistview);
        mCardViewMyGoal = (CardView) expHeader.findViewById(R.id.card_view2);

        expandableListDetail = GoalProgressModel.getData(mContext, mHomeActivity);
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableWeeklyGoalDetails = new ArrayList<>();
        expandableGoalDetails = new ArrayList<>();
        expandableRevenueDetails = new ArrayList<>();
        expandableEffortDetails = new ArrayList<>();
        productGoalsDetails = new ArrayList<>();
        chartGoalAchieved = (SectorProgressView) expHeader.findViewById(R.id.chart_goal_achieved);
        chartmyGoalAchieved = (SectorProgressView) expHeader.findViewById(R.id.chart_my_goal_achieved);
        chartTimeElapsed = (SectorProgressView) expHeader.findViewById(R.id.chart_time_elapsed);
        tvChartGoalAchieved = (TextView) expHeader.findViewById(R.id.txt_chart_goal_achieved);
        tvChartGoalAchievedTitle = (TextView) expHeader.findViewById(R.id.txt_chart_goal_achieved_title);
        tvChartMyGoalAchieved = (TextView) expHeader.findViewById(R.id.txt_chart_my_goal_achieved);
        llMyGoalAchieved = (LinearLayout) expHeader.findViewById(R.id.ll_my_goal_achieved_ll);
        tvChartMyGoalAchievedTitle = (TextView) expHeader.findViewById(R.id.txt_chart_my_goal_achieved_title);
        tvChartTimeElapsed = (TextView) expHeader.findViewById(R.id.txt_chart_time_elapsed);
        tvChartTimeElapsedTitle = (TextView) expHeader.findViewById(R.id.txt_chart_time_elapsed_title);
        tvDisplayIncrementalRevenue = (TextView) expHeader.findViewById(R.id.txt_display_incremental_revenue);


        //expandableListAdapter = new GoalProgressExpandAdapter(mContext, expandableListTitle, expandableListDetail);
        // mExpandableListView.setAdapter(expandableListAdapter);
        /* if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)) {
            tvDisplayIncrementalRevenue.setText("ARPD");
        }*/

        tvDisplayIncrementalRevenue.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc)));
        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
/*                if (groupPosition != previousGroup)
                    mExpandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;*/

            }
        });
        txtInGaugeRecommndGoal = (TextView) expHeader.findViewById(R.id.txt_inGauge_recommnd_goal);
        txtIncrementalRevenue = (TextView) expHeader.findViewById(R.id.txt_incremental_revenue);
        txtMyIngaugeGoal = (TextView) expHeader.findViewById(R.id.txt_my_ingauge_goal);

        txtInGaugeRecommndGoalTitle = (TextView) expHeader.findViewById(R.id.txt_inGauge_recommnd_goal_title);
        txtMyIngaugeGoalTitle = (TextView) expHeader.findViewById(R.id.txt_my_ingauge_goal_title);
        mExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(mContext,
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
             /*   Toast.makeText(
                        mContext,
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();*/
                return false;
            }
        });
        mExpandableListView.addHeaderView(expHeader);

        txtInGaugeRecommndGoalTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_inguage_recommended_goal_title)));
        txtMyIngaugeGoalTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_my_inguage_goal_title)));
        tvChartGoalAchievedTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_my_inguage_goal_acheived_title)));
        tvChartMyGoalAchievedTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_my_goal_acheived)));
        tvChartTimeElapsedTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_time_elapsed)));
        if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)) {
            mCardViewMyGoal.setVisibility(View.VISIBLE);
            llMyGoalAchieved.setVisibility(View.VISIBLE);
        } else {
            mCardViewMyGoal.setVisibility(View.GONE);
            llMyGoalAchieved.setVisibility(View.GONE);
        }
    }

    private void setDataForPieChart(GoalSettingModelNew goalSettingModel) {
        // chartGoalAchieved.setPercent(Float.parseFloat(String.valueOf(goalSettingModel.data.getFpgGoalProgress().getTargetAchievedPer())));
        // chartmyGoalAchieved.setPercent(Float.parseFloat(String.valueOf(goalSettingModel.data.getSelfGoalProgress().getTargetAchievedPer())));
        //   chartTimeElapsed.setPercent(Float.parseFloat(String.valueOf(goalSettingModel.data.getFpgGoalProgress().getTimeElapsed())));
/*        tvChartGoalAchieved.setText(String.valueOf(UiUtils.round(goalSettingModel.data.getFpgGoalProgress().getTargetAchievedPer(), 2)) + "%");
        tvChartMyGoalAchieved.setText(String.valueOf(UiUtils.round(goalSettingModel.data.getSelfGoalProgress().getTargetAchievedPer(), 2)) + "%");
        tvChartTimeElapsed.setText(String.valueOf(UiUtils.round(goalSettingModel.data.getFpgGoalProgress().getTimeElapsed(), 2)) + "%");*/


        tvChartGoalAchieved.setText(String.valueOf(UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getTargetAchievedPer())) + "%");
        tvChartMyGoalAchieved.setText(String.valueOf(UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getSelfGoalProgress().getTargetAchievedPer())) + "%");
        tvChartTimeElapsed.setText(String.valueOf(UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getTimeElapsed())) + "%");
        chartGoalAchieved.setFgColor(R.color.chart_goal_achieved);
        chartGoalAchieved.setStartAngle(0);
        chartmyGoalAchieved.setStartAngle(0);
        chartTimeElapsed.setStartAngle(0);
        VAL_IN_GOAL_ACHIEVED = goalSettingModel.data.getFpgGoalProgress().getTargetAchievedPer();
        VAL_MY_GOAL_ACHIEVED = goalSettingModel.data.getSelfGoalProgress().getTargetAchievedPer();
        VAL_TIME_ELAPSED = goalSettingModel.data.getFpgGoalProgress().getTimeElapsed();

        new MyCountDownTimer(MILIS_IN_FUTURE, PROGRESS_SPEED, VAL_IN_GOAL_ACHIEVED, chartGoalAchieved).start();
        new MyCountDownTimer(MILIS_IN_FUTURE, PROGRESS_SPEED, VAL_MY_GOAL_ACHIEVED, chartmyGoalAchieved).start();
        new MyCountDownTimer(MILIS_IN_FUTURE, PROGRESS_SPEED, VAL_TIME_ELAPSED, chartTimeElapsed).start();
    }

    public class MyCountDownTimer extends CountDownTimer {
        double MAX_VAL = 0;
        SectorProgressView myChart;

        public MyCountDownTimer(long millisInFuture, long countDownInterval, double maxVal, SectorProgressView myChart) {
            super(millisInFuture, countDownInterval);
            MAX_VAL = maxVal;
            this.myChart = myChart;
            counter = 0;
            Logger.Error("CHART:" + counter + "\n" + MAX_VAL);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            if (counter <= MAX_VAL) {
                myChart.setPercent(counter);
                counter++;
            }
        }

        @Override
        public void onFinish() {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_date_compare:
                MonthYearPickerDialog pd = new MonthYearPickerDialog();
                Bundle mBunle = new Bundle();
                mBunle.putInt(KEY_SELECTED_DAY, selectedDay);
                mBunle.putInt(KEY_SELECTED_MONTH, selectedMonth);
                mBunle.putInt(KEY_SELECTED_YEAR, selectedYear);
                pd.setArguments(mBunle);
                pd.setListener(this);
                pd.show(getFragmentManager(), "MonthYearPickerDialog");
                break;
            case R.id.fragment_goal_settings_iv_filter:
                FragmentFilterDynamicListForGoal mFilterListFragment = new FragmentFilterDynamicListForGoal();
                Bundle mBundle = new Bundle();
                mBundle.putParcelableArrayList(FilterListFragment.KEY_FILTER_SELECTION, mFilterBaseDatas);
                mFilterListFragment.setArguments(mBundle);
                mHomeActivity.replace(mFilterListFragment, mContext.getResources().getString(R.string.tag_filter_list_for_goal));
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Logger.Error("Month " + month + "Year " + year + "Day of Month " + dayOfMonth);
        //Month: June - Year 2017
        selectedMonth = month;
        selectedYear = year;
        selectedDay = dayOfMonth;
        selectedCalendar.set(year, month, dayOfMonth);
        tvSelectedDate.setText("Month: " + STR_MONTH[month - 1] + " - Year " + year);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_month_for_goal), month);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_year_for_goal), year);

        int locationid = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), -2);
        String locationGroupID = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(-2));

        int productId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), -2);
        int userId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), -2);
        Bundle bundle = new Bundle();
        bundle.putString("email", InGaugeSession.read(getActivity().getResources().getString(R.string.key_user_user_email), ""));
        bundle.putString("role", InGaugeSession.read(getActivity().getResources().getString(R.string.key_user_role_name), ""));
        bundle.putString("industry", InGaugeSession.read(getActivity().getResources().getString(R.string.key_selected_industry), ""));
        bundle.putString("location", InGaugeSession.read(getActivity().getResources().getString(R.string.key_tenant_location_name), ""));
        bundle.putString("month", STR_MONTH[month - 1]);
        bundle.putInt("year", year);
        bundle.putString("user", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), ""));
        InGaugeApp.getFirebaseAnalytics().logEvent("change_goal_date", bundle);
        if (!(locationid < 0 || Integer.parseInt(locationGroupID) < 0 || userId < 0)) {

            getGoalProgress(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), -2)),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_month_for_goal), -2),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_year_for_goal), -2),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -2),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), -2),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(-2)),
                    InGaugeSession.read(mContext.getString(R.string.key_selected_user_id_for_goal), -2),
                    InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product_id_for_goal), -2));

            // getGoalProgress(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
        } else {
            txtNoGoal.setVisibility(View.GONE);
            txtNoGoal.setText(mContext.getResources().getString(R.string.no_goal_found));
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imgFilter.getVisibility() == View.VISIBLE) {
                    FragmentFilterDynamicListForGoal mFilterListFragment = new FragmentFilterDynamicListForGoal();
                    Bundle mBundle = new Bundle();
                    mBundle.putParcelableArrayList(FilterListFragment.KEY_FILTER_SELECTION, mFilterBaseDatas);
                    mFilterListFragment.setArguments(mBundle);
                    mHomeActivity.replace(mFilterListFragment, mContext.getResources().getString(R.string.tag_filter_list_for_goal));
                }
            }
        });
        boolean isNeedUpdate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_update_goal_settings_filter), false);
        if (isNeedUpdate) {
            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_update_goal_settings_filter), false);
            Logger.Error("<<<<< Call in On Receive Method>>>>");
            int locationid = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), -2);
            String locationGroupID = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(-2));

            int productId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), -2);
            int userId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), 0);

            if (!(locationid < 0 || Integer.parseInt(locationGroupID) < 0 || userId < 0)) {

                getGoalProgress(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), -2)),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_month_for_goal), -2),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_year_for_goal), -2),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -2),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), -2),
                        InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(-2)),
                        InGaugeSession.read(mContext.getString(R.string.key_selected_user_id_for_goal), -2),
                        InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product_id_for_goal), -2));

                // getGoalProgress(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
            } else {
                txtNoGoal.setVisibility(View.VISIBLE);
                txtNoGoal.setText(mContext.getResources().getString(R.string.no_goal_found));
            }
        }
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
        mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_goal_setting_title)));
        if (InGaugeSession.read(mContext.getResources().getString(R.string.key_is_need_filter_call_goal), false)) {
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call_goal), false);
//            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0));
            pbFilter.setVisibility(View.VISIBLE);
            imgFilter.setVisibility(View.GONE);
            getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -2),
                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2)), "name", "ASC", "Normal");
        } else {


            mHomeActivity.tvSubTitle.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), "") + "-" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), ""));
            if (mHomeActivity.mAccessibleTenantLocationDataModelForGoal == null || mHomeActivity.mDashboardLocationGroupListModelForGoal == null
                    || mHomeActivity.mProductListFromLocationGroupModelForGoal == null || mHomeActivity.mDashboardUserListModelForGoal == null) {
                for (int i = 0; i < 4; i++) {
                    switch (i) {

                        case 0:
                            Gson gson = new Gson();
                            String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_location_obj_for_goal), null);
                            mHomeActivity.mAccessibleTenantLocationDataModelForGoal = gson.fromJson(json, AccessibleTenantLocationDataModel.class);
                            break;
                        case 1:
                            gson = new Gson();
                            json = InGaugeSession.read(mContext.getResources().getString(R.string.key_location_group_obj_for_goal), null);
                            mHomeActivity.mDashboardLocationGroupListModelForGoal = gson.fromJson(json, DashboardLocationGroupListModel.class);
                            break;
                        case 2:
                            gson = new Gson();
                            json = InGaugeSession.read(mContext.getResources().getString(R.string.key_product_obj_for_goal), null);
                            mHomeActivity.mProductListFromLocationGroupModelForGoal = gson.fromJson(json, ProductListFromLocationGroupModel.class);
                            break;
                        case 3:
                            gson = new Gson();
                            json = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_obj_for_goal), null);
                            mHomeActivity.mDashboardUserListModelForGoal = gson.fromJson(json, DashboardUserListModel.class);
                            break;

                    }
                }
            } else {
               /* pbFilter.setVisibility(View.VISIBLE);
                imgFilter.setVisibility(View.GONE);
                getAccessibleTenantLocation(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0),
                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", "Normal");*/
            }
        }
    }

    void getGoalProgress(String authToken, String industrId, int month, int year, int tenantId, int locationId, String locationGrpId, int userId, int locationGrpProductId) {
        mBaseActivity.startProgress(mContext);

        mCall = apiInterface.getGoalProgress(month, year, tenantId, locationId, locationGrpId, userId, locationGrpProductId);
        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // GoalSettingModel goalSettingModel = null;

                mExpandableListView.setVisibility(View.VISIBLE);
                GoalSettingModelNew goalSettingModel = null;
                if (response.body() != null) {
                    try {
                        txtNoGoal.setVisibility(View.GONE);
                        if(response.raw().code()==200){
                            //JSONObject responseObj = new JSONObject(response.body().string());
                            JSONObject responseJson = new JSONObject(response.body().string());

                            Logger.Error("<<<<<   Json Response >>>>>" + responseJson.toString());
                            //  Logger.Error("Response json is " + responseJson.getJSONObject("body").toString());
                            GsonBuilder gsonb = new GsonBuilder();
                            // Gson gson = gsonb.create();
                            Gson gson = new Gson();
                            goalSettingModel = gson.fromJson(responseJson.toString(), GoalSettingModelNew.class);
                            setTextValue(goalSettingModel);
                            setDataForPieChart(goalSettingModel);
                            expandableWeeklyGoalDetails = new ArrayList<>();
                            expandableGoalDetails = new ArrayList<>();
                            expandableRevenueDetails = new ArrayList<>();
                            expandableEffortDetails = new ArrayList<>();
                            productGoalsDetails = new ArrayList<>();
                            setCurretnGoalMonth(responseJson.getJSONObject("data"));
                            expandableWeeklyGoalDetails = getWeeklyGoalValue(goalSettingModel);
                            expandableGoalDetails = getGoalValue(goalSettingModel);
                            expandableRevenueDetails = getValueRevenue(goalSettingModel);
                            expandableEffortDetails = getValueEffort(goalSettingModel);
                            expandableListAdapter = new GoalProgressExpandAdapter(mContext, expandableListTitle, expandableListDetail, expandableGoalDetails, expandableRevenueDetails, expandableEffortDetails, productGoalsDetails, expandableWeeklyGoalDetails);
                            mExpandableListView.setAdapter(expandableListAdapter);

                        }
                        mBaseActivity.endProgress();


                        Logger.Error("goalSettingModel is " + goalSettingModel);
                        //   Logger.Error("goalSettingModel Product is " + goalSettingModel.data.getSearchGoalPredictionForm().getGoalSettingProduct());


                    } catch (JSONException e) {
                        e.printStackTrace();
                        mBaseActivity.endProgress();
                    } catch (IOException e) {
                        e.printStackTrace();
                        mBaseActivity.endProgress();
                    } catch (Exception e) {
                        e.printStackTrace();
                        mBaseActivity.endProgress();
                        txtNoGoal.setVisibility(View.VISIBLE);
                        mExpandableListView.setVisibility(View.GONE);
                        txtNoGoal.setText(mContext.getResources().getString(R.string.no_goal_found));

                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mBaseActivity.endProgress();
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                }
            }


            @Override
            public void onFailure(Call call, Throwable t) {
                mBaseActivity.endProgress();
                txtNoGoal.setVisibility(View.VISIBLE);
                mExpandableListView.setVisibility(View.GONE);
                txtNoGoal.setText(mContext.getResources().getString(R.string.no_goal_found));
                Logger.Error("Exception: " + t.getMessage());
              /*  pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });


    }

    private List<String> getValueEffort(GoalSettingModelNew goalSettingModel) {

        if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)) {


            if (goalSettingModel.data.getFpgGoalProgress().getNotes() != null) {
                expandableEffortDetails.add(goalSettingModel.data.getFpgGoalProgress().getNotes());
            } else {
                goalSettingModel.data.getFpgGoalProgress().setNotes("");
                expandableEffortDetails.add(String.valueOf(goalSettingModel.data.getFpgGoalProgress().getNotes()));
            }


            if (goalSettingModel.data.getFpgGoalProgress().getVerdict() != null) {
                expandableEffortDetails.add(String.valueOf(goalSettingModel.data.getFpgGoalProgress().getVerdict()));
            } else {
                goalSettingModel.data.getFpgGoalProgress().setVerdict("");
                expandableEffortDetails.add(String.valueOf(goalSettingModel.data.getFpgGoalProgress().getVerdict()));
            }
        } else {
            expandableEffortDetails.add(String.valueOf(UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getRequiredNumberOfUnitsPerDay())));
            expandableEffortDetails.add(String.valueOf(UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getNumberOfUnitsToHitNextTier())));
            expandableEffortDetails.add("");
            if (goalSettingModel.data.getFpgGoalProgress().getVerdict() != null) {
                expandableEffortDetails.add(String.valueOf(goalSettingModel.data.getFpgGoalProgress().getVerdict()));
            } else {
                goalSettingModel.data.getFpgGoalProgress().setVerdict("");
                expandableEffortDetails.add(String.valueOf(goalSettingModel.data.getFpgGoalProgress().getVerdict()));
            }
        }


        return expandableEffortDetails;
    }


    private List<String> getWeeklyGoalValue(GoalSettingModelNew goalSettingModel) {

        expandableWeeklyGoalDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getWeeklyGoalPrediction().getReqRevWeeklyFPG())));
        expandableWeeklyGoalDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getWeeklyGoalPrediction().getEarnedRevenueWeekly())));


        return expandableWeeklyGoalDetails;
    }

    private List<String> getGoalValue(GoalSettingModelNew goalSettingModel) {

        expandableGoalDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getProductGoal())));
        if (InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)) {
            expandableGoalDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getProductARPDThisMonth())));
        } else {
            expandableGoalDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getProductIRThisMonth())));
        }


        expandableGoalDetails.add(String.valueOf(UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getTargetAchievedPer()) + "%"));
        expandableGoalDetails.add(String.valueOf(UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getTimeElapsed()) + "%"));


        return expandableGoalDetails;
    }

    private List<String> getValueRevenue(GoalSettingModelNew goalSettingModel) {
        expandableRevenueDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getRevenuetoHitGoal())));
        expandableRevenueDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getAverageRevenueMTD())));
        expandableRevenueDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getAvgRevenuPerUnit())));
        expandableRevenueDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getProjectedRevenueEOM())));
        expandableRevenueDetails.add(String.valueOf(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + UiUtils.TwoDigitDecimal("" + goalSettingModel.data.getFpgGoalProgress().getRevenueToHitNextTier())));

        return expandableRevenueDetails;
    }


    public void callGoalProgressAPI(){
        int locationid = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), -2);
        String locationGroupID = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(-2));

        int productId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), -2);
        int userId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), -2);

        if (!(locationid < 0 || Integer.parseInt(locationGroupID) < 0 || userId < 0)) {

            getGoalProgress(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""),
                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), -2)),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_month_for_goal), -2),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_year_for_goal), -2),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -2),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), -2),
                    InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(-2)),
                    InGaugeSession.read(mContext.getString(R.string.key_selected_user_id_for_goal), -2),
                    InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product_id_for_goal), -2));

            // getGoalProgress(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
        } else {
            txtNoGoal.setVisibility(View.VISIBLE);
            txtNoGoal.setText(mContext.getResources().getString(R.string.no_goal_found));
        }
    }

    /**
     * @param authToken
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param activeStatus
     */
    void getAccessibleTenantLocation(final String authToken, int tenantId, String userId, String orderBy, String sort, String activeStatus) {

        mCall = apiInterface.getAccessibleTenantLocationForGoalFilter(tenantId, userId, orderBy, sort, activeStatus);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {

                    mHomeActivity.mAccessibleTenantLocationDataModelForGoal = (AccessibleTenantLocationDataModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForGoal);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_location_obj_for_goal), json);
                    if ((mHomeActivity.mAccessibleTenantLocationDataModelForGoal != null && (mHomeActivity.mAccessibleTenantLocationDataModelForGoal.getData() != null && mHomeActivity.mAccessibleTenantLocationDataModelForGoal.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mAccessibleTenantLocationDataModelForGoal.getData().get(0).getName();
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_region_currency), mHomeActivity.mAccessibleTenantLocationDataModelForGoal.getData().get(0).getRegionCurrency());
                        mFilterBaseData.filterIndex = 0;
                        mFilterBaseDatas.add(mFilterBaseData);

                        //String authToken,String tenantlocationId,String userId
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), mHomeActivity.mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), mHomeActivity.mAccessibleTenantLocationDataModelForGoal.getData().get(0).getName());
                        mHomeActivity.tvSubTitle.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), "") + "-" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), ""));
                        getDashboardLocationGroupList(authToken, String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModelForGoal.getData().get(0).getId()), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2)));
                    } else {
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), 0);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), "");
                        callGoalProgressAPI();
                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
              /*  pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });
    }

    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupList(final String authToken, String tenantlocationId, String userId) {

        mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mHomeActivity.mDashboardLocationGroupListModelForGoal = (DashboardLocationGroupListModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModelForGoal);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_location_group_obj_for_goal), json);
                    //String authToken,String locationGroupId
                    if ((mHomeActivity.mDashboardLocationGroupListModelForGoal != null && (mHomeActivity.mDashboardLocationGroupListModelForGoal.getData() != null && mHomeActivity.mDashboardLocationGroupListModelForGoal.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID());
                        mFilterBaseData.name = mHomeActivity.mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupName();
                        mFilterBaseData.filterIndex = 1;
                        mFilterBaseDatas.add(mFilterBaseData);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name_for_goal), mHomeActivity.mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupName());
                        getProductList(authToken, String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForGoal.getData().get(0).getLocationGroupID()));
                    } else {
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(0));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name_for_goal), "");
                        callGoalProgressAPI();
                    }
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {

                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductList(final String authToken, final String locationGroupId) {

        mCall = apiInterface.getProductList(locationGroupId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mHomeActivity.mProductListFromLocationGroupModelForGoal = (ProductListFromLocationGroupModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModelForGoal);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_product_obj_for_goal), json);
                    if ((mHomeActivity.mProductListFromLocationGroupModelForGoal != null && (mHomeActivity.mProductListFromLocationGroupModelForGoal.getData() != null && mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        //mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                        //mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName();
                        mFilterBaseData.id = String.valueOf(-1);
                        mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
                        mFilterBaseData.filterIndex = 2;
                        mFilterBaseDatas.add(mFilterBaseData);
                        //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                        //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), -1);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc)));

                        //String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId
                        getDashboardUserList(authToken, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), -2)), InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), locationGroupId);
                    } else {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        //mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                        //mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName();
                        mFilterBaseData.id = String.valueOf(-1);
                        mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc));
                        mFilterBaseData.filterIndex = 2;
                        mFilterBaseDatas.add(mFilterBaseData);
                        //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getId());
                        //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), mHomeActivity.mProductListFromLocationGroupModelForGoal.getData().get(0).getName());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), -1);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name_for_goal), mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_arpd_inc)));
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        callGoalProgressAPI();
                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    /**
     * @param authToken
     * @param tenantLocationId
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserList(String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId) {

        mCall = apiInterface.getDashboardUserListForGoalFilter(tenantLocationId, locationGroupId, true);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                if (response.body() != null) {
                    mHomeActivity.mDashboardUserListModelForGoal = (DashboardUserListModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardUserListModelForGoal);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_user_obj_for_goal), json);
                    if ((mHomeActivity.mDashboardUserListModelForGoal != null && (mHomeActivity.mDashboardUserListModelForGoal.getData() != null && mHomeActivity.mDashboardUserListModelForGoal.getData().size() > 0))) {

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mDashboardUserListModelForGoal.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mDashboardUserListModelForGoal.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 5;
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), mHomeActivity.mDashboardUserListModelForGoal.getData().get(0).getId());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), mHomeActivity.mDashboardUserListModelForGoal.getData().get(0).getName());
                        mFilterBaseDatas.add(mFilterBaseData);

                        mHomeActivity.tvSubTitle.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_location_name_for_goal), "") + "-" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), ""));
                        callGoalProgressAPI();

                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), 0);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name_for_goal), "");
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        callGoalProgressAPI();
                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCall != null)
            mCall.cancel();
    }

    public void setTextValue(GoalSettingModelNew goalsettingModel) {
        txtInGaugeRecommndGoal.setText(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + String.valueOf(goalsettingModel.data.getFpgGoalProgress().getProductGoal()));
        txtMyIngaugeGoal.setText(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + String.valueOf(goalsettingModel.data.getSelfGoalProgress().getProductGoal()));
        txtIncrementalRevenue.setText(UiUtils.getCurrencySymbol(InGaugeSession.read(mContext.getResources().getString(R.string.key_region_currency), "USD")) + String.valueOf(goalsettingModel.data.getFpgGoalProgress().getProductIRThisMonth()));
    }

    public void setCurretnGoalMonth(JSONObject curretnGoalMonth) {
        GoalCurrentMonthMap goalCurrentMonthMap;
        List<String> productName = new ArrayList<>();
        try {
            JSONObject currentGoal = curretnGoalMonth.getJSONObject("curMonthGoalMap");
            Iterator<String> iterator = currentGoal.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                JSONObject goalData = currentGoal.getJSONObject(key);
                goalCurrentMonthMap = new GoalCurrentMonthMap();
                goalCurrentMonthMap.setFpgLevel(goalData.getInt("fpgGoal"));
                if (goalData.has("achieved")) {
                    goalCurrentMonthMap.setAchieved(goalData.getBoolean("achieved"));
                } else {
                    goalCurrentMonthMap.setAchieved(false);
                }
                productGoalsDetails.add(goalCurrentMonthMap);
                productName.add(goalData.getString("locationGroupProductName"));

            }
            if (productName.size() == 0) {

                productName.add("No Product Available");
            }
            expandableListDetail.put("Product Goals", productName);
            expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        } catch (Exception ex) {
            Logger.Error(ex.getMessage());
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);

    }


}
