package com.ingauge.fragments.dateselection.tabsfragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by desainid on 5/11/2017.
 */

public class CustomFragmentForObservationCC extends Fragment implements View.OnClickListener{
    Context mContext;
    HomeActivity mHomeActivity;
    TextView txtviewstrtDteFilter;
    TextView txtviewEndDteFilter;
    TextView txtStrtDte;
    TextView txtEndDte;
    LinearLayout linearStartDte;
    LinearLayout linearEndDte;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private int mYear, mMonth, mDay;
    Calendar calendar;
    SwitchCompat switchCompareDate;

    SimpleDateFormat sdfServerDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);


    public String startDate = "";
    public String endDate = "";

    public String serverStartDate = "";
    public String serverEndDate = "";

    public String serverStartDateForCustom = "";


    public String serverEndDateForCustom = "";

    public int tabIndex = 3;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_date_filter_observation, container, false);
        initViews(rootView);
        setHasOptionsMenu(true);
        calendar.setTime(new Date());
        calendar.add(calendar.DATE, -30);

        linearStartDte.setOnClickListener(this);
        linearEndDte.setOnClickListener(this);
        return rootView;
    }


    private void initViews(View rootView){

        txtviewstrtDteFilter = (TextView) rootView.findViewById(R.id.tv_startdate_label);
        txtviewEndDteFilter = (TextView) rootView.findViewById(R.id.tv_enddate_label);
        txtStrtDte = (TextView) rootView.findViewById(R.id.tv_start_date);
        txtEndDte = (TextView) rootView.findViewById(R.id.tv_end_date);

        switchCompareDate = (SwitchCompat) rootView.findViewById(R.id.switch_compare_date);
        linearStartDte = (LinearLayout) rootView.findViewById(R.id.linearStartDte);
        linearEndDte = (LinearLayout) rootView.findViewById(R.id.linearEndDte);


        serverStartDate = InGaugeSession.read(getString(R.string.key_start_date_observation_date_filter_server), "");
        serverEndDate = InGaugeSession.read(getString(R.string.key_end_date_observation_date_filter_server), "");


        serverStartDateForCustom = InGaugeSession.read(getString(R.string.key_start_date_observation_date_filter_server_for_custom), "");
        serverEndDateForCustom = InGaugeSession.read(getString(R.string.key_end_date_observation_date_filter_server_for_custom), "");

        if(serverStartDateForCustom != null && serverStartDateForCustom.length() > 0){
            startDate = getDisplayStringForCustomTab(serverStartDateForCustom);
            endDate = getDisplayStringForCustomTab(serverEndDateForCustom);
            txtStrtDte.setText(startDate);
            txtEndDte.setText(endDate);
        } else{

            startDate = InGaugeSession.read(getString(R.string.key_start_date_observation_date_filter), "");
            endDate = InGaugeSession.read(getString(R.string.key_end_date_observation_date_filter), "");

            serverStartDateForCustom = getSavedStringForCustomTab(startDate);
            serverEndDateForCustom = getSavedStringForCustomTab(endDate);

            txtStrtDte.setText(startDate);
            txtEndDte.setText(endDate);
        }


        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        mHomeActivity.tvTitle.setText("Date Filter");
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);

    }

    /**
     * @param DateStr
     * @return For Week Tab
     */
    public String getDisplayStringForCustomTab(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("MMM dd ,yyyy");

        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param DateStr
     * @return For Week Tab
     */
    public String getSavedStringForCustomTab(String DateStr){
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat inputFormat = new SimpleDateFormat("MMM dd ,yyyy");

        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.linearStartDte:

                Date display = null;
                try{
                    display = sdf.parse(txtStrtDte.getText().toString());
                } catch(ParseException e){
                    e.printStackTrace();
                }
                //calendar = UiUtils.DateToCalendar(display);
                try{
                    calendar = UiUtils.DateToCalendar(sdf.parse(txtStrtDte.getText().toString()));
                } catch(ParseException e){
                    e.printStackTrace();
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
                                //mHomeActivity.setDashboardObsCoachingUpdateAfterApply(true);
                                Calendar startCalendar = Calendar.getInstance();
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                txtStrtDte.setText(sdf.format(calendar.getTime()));
                                try{
                                    Date startDsiplay = sdf.parse(txtStrtDte.getText().toString());
                                    startCalendar = UiUtils.DateToCalendar(startDsiplay);
                                } catch(ParseException e){
                                    e.printStackTrace();
                                }

                                try{
                                    Calendar checkEnddate = UiUtils.DateToCalendar(sdf.parse(txtEndDte.getText().toString()));
                                    if(calendar.compareTo(checkEnddate) > 0){
                                        Calendar mCalendarForlastDateofMonth = calendar;
                                        mCalendarForlastDateofMonth.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                                        //Calendar toCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(EXTRAToDate + " 23:59:59"));

                                        txtEndDte.setText(sdf.format(mCalendarForlastDateofMonth.getTime()));
                                        endDate = txtEndDte.getText().toString();
                                        serverEndDate = "" + mCalendarForlastDateofMonth.getTimeInMillis();
                                        serverEndDateForCustom = getSavedStringForCustomTab(endDate);
                                        /*InGaugeSession.write(getString(R.string.key_end_date_observation_date_filter), txtEndDte.getText().toString());
                                        InGaugeSession.write(getString(R.string.key_end_date_observation_date_filter_server), "" + mCalendarForlastDateofMonth.getTimeInMillis());*/


                                    }
                                } catch(ParseException e){
                                    e.printStackTrace();
                                }

                                /*try {*/
                                    /*String serverDate = sdfServer.format(calendar.getTime());

                                    Calendar startserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverDate + " 00:00:00"));
                                    startserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));*/
                                /*} catch (ParseException e) {
                                    e.printStackTrace();
                                }*/


                                startDate = txtStrtDte.getText().toString();
                                serverStartDate = "" + startCalendar.getTimeInMillis();
                                serverStartDateForCustom = getSavedStringForCustomTab(startDate);
                                /*InGaugeSession.write(getString(R.string.key_start_date_observation_date_filter), txtStrtDte.getText().toString());
                                InGaugeSession.write(getString(R.string.key_start_date_observation_date_filter_server), "" + startCalendar.getTimeInMillis());*/


                                Logger.Error("@@@@@@@@ Calenar Start Milli second in Start Date calendar >>>>> " + startCalendar.getTimeInMillis());

                            }

                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


                //datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
                Bundle bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("start_date", InGaugeSession.read(getResources().getString(R.string.key_start_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_observation_date", bundle);

                break;

            case R.id.linearEndDte:


                try{
                    calendar = UiUtils.DateToCalendar(sdf.parse(txtEndDte.getText().toString()));
                } catch(ParseException e){
                    e.printStackTrace();
                }

                DatePickerDialog endDatePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){
                                //mHomeActivity.setDashboardObsCoachingUpdateAfterApply(true);
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                txtEndDte.setText(sdf.format(calendar.getTime()));

                                try{
                                    Calendar checkStartdate = UiUtils.DateToCalendar(sdf.parse(txtStrtDte.getText().toString()));
                                    if(calendar.compareTo(checkStartdate) < 0){
                                        Calendar mCalendarForFirstDateofMonth = calendar;
                                        mCalendarForFirstDateofMonth.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));

                                        mCalendarForFirstDateofMonth.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        /*String serverDate = sdfServer.format(mCalendarForFirstDateofMonth.getTime());

                                        Calendar startserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverDate + " 00:00:00"));
                                        startserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));*/
                                        txtStrtDte.setText(sdf.format(mCalendarForFirstDateofMonth.getTime()));
                                        Logger.Error("@@@@@@@@ Calenar Start Milli second in End Date calendar >>>>> " + mCalendarForFirstDateofMonth.getTimeInMillis());


                                        /*InGaugeSession.write(getString(R.string.key_start_date_observation_date_filter), txtStrtDte.getText().toString());
                                        InGaugeSession.write(getString(R.string.key_start_date_observation_date_filter_server), "" + mCalendarForFirstDateofMonth.getTimeInMillis());*/
                                        startDate = txtStrtDte.getText().toString();
                                        serverStartDate = "" + mCalendarForFirstDateofMonth.getTimeInMillis();
                                        serverStartDateForCustom = getSavedStringForCustomTab(startDate);
                                        /*InGaugeSession.write(getString(R.string.key_start_date_observation_date_filter),txtStrtDte.getText().toString());
                                        InGaugeSession.write(getString(R.string.key_start_date_observation_date_filter_server),""+ mCalendarForFirstDateofMonth.getTimeInMillis());*/

                                    }
                                } catch(ParseException e){
                                    e.printStackTrace();
                                }

                                /*try {
                                    String serverDate = sdfServer.format(calendar.getTime());

                                    Calendar endserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverDate + " 23:59:59"));
                                    endserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
*/
                                Logger.Error("@@@@@@@@ Calenar End Milli second in End Date calendar >>>>> " + calendar.getTimeInMillis());
                                /*InGaugeSession.write(getString(R.string.key_end_date_observation_date_filter), txtEndDte.getText().toString());*/
                                endDate = txtEndDte.getText().toString();

                                try{
                                    Calendar endDatecalendar = UiUtils.DateToCalendar(sdf.parse(txtEndDte.getText().toString()));
                                    serverEndDate = "" + endDatecalendar.getTimeInMillis();
                                    serverEndDateForCustom = getSavedStringForCustomTab(endDate);
                                    /*InGaugeSession.write(getString(R.string.key_end_date_observation_date_filter_server), "" + endDatecalendar.getTimeInMillis());*/
                                } catch(ParseException e){
                                    e.printStackTrace();
                                }


                                // findPreviousDays();
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                DateFormat format = new SimpleDateFormat("MMM dd ,yyyy", Locale.UK);
                Date date = null;
                Date minDate = null;
                final Calendar cal;
                try{
                    date = format.parse(txtStrtDte.getText().toString());
                    cal = Calendar.getInstance();
                    cal.setTime(date);
                    // endDatePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
                    //endDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                    endDatePickerDialog.show();

                } catch(ParseException e){
                    e.printStackTrace();
                }
                bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("end_date", InGaugeSession.read(getResources().getString(R.string.key_end_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;


        }

    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mHomeActivity.ibMenu.setVisibility(View.GONE);

//        mHomeActivity.tvApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_apply)));


        // mHomeActivity.ibMenu.setImageResource(android.R.drawable.ic_menu_save);

        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
    }

    private int getDayDifference(){
        SimpleDateFormat dfDate = new SimpleDateFormat("MMM dd ,yyyy");
        String startDte = txtStrtDte.getText().toString();
        String endDte = txtEndDte.getText().toString();
        int diffDays = 0;
        try{
            Date date1 = dfDate.parse(startDte);
            Date date2 = dfDate.parse(endDte);
            diffDays = (int) ((date2.getTime() - date1.getTime()) / (1000 * 60 * 60 * 24));


        } catch(Exception e){
            e.printStackTrace();
        }
        return diffDays;
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
    }

    public String getStartDate(){
        return startDate;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getEndDate(){
        return endDate;
    }

    public void setEndDate(String endDate){
        this.endDate = endDate;
    }

    public String getServerStartDate(){
        return serverStartDate;
    }

    public void setServerStartDate(String serverStartDate){
        this.serverStartDate = serverStartDate;
    }

    public String getServerEndDate(){
        return serverEndDate;
    }

    public void setServerEndDate(String serverEndDate){
        this.serverEndDate = serverEndDate;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }

    public String getServerStartDateForCustom(){
        return serverStartDateForCustom;
    }

    public void setServerStartDateForCustom(String serverStartDateForCustom){
        this.serverStartDateForCustom = serverStartDateForCustom;
    }

    public String getServerEndDateForCustom(){
        return serverEndDateForCustom;
    }

    public void setServerEndDateForCustom(String serverEndDateForCustom){
        this.serverEndDateForCustom = serverEndDateForCustom;
    }
}
