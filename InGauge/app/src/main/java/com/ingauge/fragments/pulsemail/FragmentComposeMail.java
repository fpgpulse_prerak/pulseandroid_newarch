package com.ingauge.fragments.pulsemail;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.ProgressActivity;
import com.ingauge.adapters.AttachmentAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.pojo.AttachmentModel;
import com.ingauge.pojo.EmailBody;
import com.ingauge.pojo.RecipientListModel;
import com.ingauge.pojo.ReplyMailBody;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.FileUtils;
import com.ingauge.utils.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import dk.nodes.filepicker.FilePickerActivity;
import dk.nodes.filepicker.FilePickerConstants;
import dk.nodes.filepicker.uriHelper.FilePickerUriHelper;
import mabbas007.tagsedittext.TagsEditText;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 14-Jul-17.
 */

public class FragmentComposeMail extends BaseFragment implements TagsEditText.TagsEditListener, View.OnClickListener, AttachmentAdapter.ItemClickListener{


    public static String KEY_IS_FROM_REPLY = "is_from_reply";
    public static String KEY_REPLY_SUBJECT = "key_reply_subject";
    public static String KEY_REPLY_EMAIL_ID = "key_reply_from_mail";
    public static String KEY_REPLY_SENDER_ID = "key_reply_sender_id";

    private static final int PERMISSION_REQUEST_CODE = 001;
    private static final int PICK_FILE_REQUEST = 002;
    public static int MY_REQUEST_CODE = 10;
    View rootView;
    private TagsEditText mTagsEditTextEmail;
    private TextView tvToLabel, tvSubjectLabel, tvMessageLabel, tvAttachmentLabel;
    private HomeActivity mHomeActivity;
    APIInterface apiInterface;
    Call mCall;
    //  private TagEmailAdapter mTagEmailAdapter;
    RecipientListModel recipientListModel;
    private EditText etSubject;
    private EditText etMessage;
    private EditText etSenderEmailForReply;
    List<String> senderEmailIds;
    Context mContext;
    String emailJson = "";

    int numberOfColumns = 3;
    RecyclerView recyclerView;
    private AttachmentAdapter adapter;

    Uri selectedFileUri;
    private List<AttachmentModel> mAttachmentModelList;
    private NestedScrollView mNestedScrollView;

    private boolean isFromreply = false;
    private String replyEmailId = "";
    private String replySubject = "";
    private int senderId;


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mAttachmentModelList = new ArrayList<>();
        adapter = new AttachmentAdapter(mHomeActivity, mAttachmentModelList, true, 0);
        adapter.setClickListener(this);
        Bundle bundle = new Bundle();
        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));

        if(getArguments() != null){
            isFromreply = getArguments().getBoolean(KEY_IS_FROM_REPLY);
            if(isFromreply){
                InGaugeApp.getFirebaseAnalytics().logEvent("reply_mail", bundle);
                replyEmailId = getArguments().getString(KEY_REPLY_EMAIL_ID);
                replySubject = getArguments().getString(KEY_REPLY_SUBJECT);
                senderId = getArguments().getInt(KEY_REPLY_SENDER_ID);
            } else{
                InGaugeApp.getFirebaseAnalytics().logEvent("compose_mail", bundle);
                getRecipientListApi(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), 0));
            }
        } else{
            InGaugeApp.getFirebaseAnalytics().logEvent("compose_mail", bundle);
            getRecipientListApi(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), 0));
        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_compose_mail, container, false);
        initView(rootView);
        mNestedScrollView.setFocusableInTouchMode(true);
        mNestedScrollView.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    if(keyCode == KeyEvent.KEYCODE_BACK){
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        AlertDialog dialog;
                        builder.setTitle("");
                        builder.setMessage(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_discard_message)));
                        builder.setCancelable(false);
                        builder.setPositiveButton(
                                getActivity().getResources().getString(R.string.alert_yes),
                                new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                        dialog.dismiss();
                                        mHomeActivity.onBackPressed();
                                    }
                                });
                        builder.setNegativeButton(
                                getActivity().getResources().getString(R.string.alert_no), new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                        dialog.dismiss();
                                    }
                                }
                        );
                        //  builder.show();
                        dialog = builder.create();
                        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        dialog.show();
                        return true;
                    }
                }
                return false;
            }
        });

        return rootView;
    }

    private void initView(View view){
        mNestedScrollView = (NestedScrollView) view.findViewById(R.id.parent_ns);
        tvToLabel = (TextView) view.findViewById(R.id.fragment_compose_mail_tv_rec_label);
        tvSubjectLabel = (TextView) view.findViewById(R.id.fragment_compose_mail_tv_subject_label);
        tvAttachmentLabel = (TextView) view.findViewById(R.id.fragment_compose_mail_tv_attachment_label);
        tvMessageLabel = (TextView) view.findViewById(R.id.fragment_compose_mail_tv_message_label);

        tvToLabel.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_to_title)));
        tvSubjectLabel.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_subject_title)));
        tvAttachmentLabel.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_attachments)));
        tvMessageLabel.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_message_title)));
        mTagsEditTextEmail = (TagsEditText) view.findViewById(R.id.fragment_compoase_mail_tag_edittext_email);
        mTagsEditTextEmail.setHint(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_search_recipients)));
        mTagsEditTextEmail.setTagsListener(this);
        mTagsEditTextEmail.setTagsWithSpacesEnabled(true);
        mTagsEditTextEmail.setThreshold(1);
        etSubject = (EditText) view.findViewById(R.id.fragment_compose_mail_et_subject);
        etMessage = (EditText) view.findViewById(R.id.fragment_compose_mail_et_body);
        etSenderEmailForReply = (EditText) view.findViewById(R.id.fragment_compoase_mail_reply_edittext_email);
        etSubject.setHint(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_subject_title)));
        etMessage.setHint(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_compose)));
        mTagsEditTextEmail.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus){
                if(hasFocus){
                    mTagsEditTextEmail.showDropDown();
                }
            }
        });
        mHomeActivity.ibMenu.setOnClickListener(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvNumbers);
        recyclerView.setLayoutManager(new GridLayoutManager(mHomeActivity, numberOfColumns));
        if(isFromreply){
            mTagsEditTextEmail.setVisibility(View.GONE);
            etSenderEmailForReply.setVisibility(View.VISIBLE);
            etSenderEmailForReply.setClickable(false);
            etSenderEmailForReply.setEnabled(false);
            etSubject.setClickable(false);
            etSubject.setEnabled(false);
            etSenderEmailForReply.setText(replyEmailId);
            etSubject.setText(replySubject);
        } else{
            mTagsEditTextEmail.setVisibility(View.VISIBLE);
            etSenderEmailForReply.setVisibility(View.GONE);
            etMessage.setClickable(true);
            etMessage.setEnabled(true);
        }

       /* mHomeActivity.ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (senderEmailIds != null) {
                    if (senderEmailIds.size() > 0) {
                        for (int i = 0; i < senderEmailIds.size(); i++) {
                            Logger.Error("IDS  " + senderEmailIds.get(i));
                        }
                    }
                }
                if (TextUtils.isEmpty(etMessage.getText().toString().trim()) || TextUtils.isEmpty(etSubject.getText().toString().trim()) || !(senderEmailIds != null && senderEmailIds.size() > 0)) {

                } else {

                }


                for (int i = 0; i < recipientListModel.getData().size(); i++) {
                    Logger.Error("<< IDS >> " + recipientListModel.getData().get(i).getId() + " << Email >> " + recipientListModel.getData().get(i).getEmail());
                }
            }
        });
*/

    }

    @Override
    public void onClick(View v){

        switch(v.getId()){
            case R.id.ib_menu:
                if(isFromreply){
                    if(TextUtils.isEmpty(etMessage.getText().toString().trim())){
                        Toast.makeText(mContext, mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_message_required)), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ReplyMailBody mreplyMailBody = new ReplyMailBody();
                    mreplyMailBody.setReplyId(senderId);
                    mreplyMailBody.setMessage(etMessage.getText().toString());
                    mreplyMailBody.setSubject(etSubject.getText().toString());
                    if(mAttachmentModelList.size() > 0){
                        String[] mailFieList = new String[mAttachmentModelList.size()];
                        for(int i = 0; i < mAttachmentModelList.size(); i++){
                            mailFieList[i] = String.valueOf(mAttachmentModelList.get(i).attachmentid);
                        }
                        mreplyMailBody.setMailFileList(mailFieList);
                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        InGaugeApp.getFirebaseAnalytics().logEvent("mail_attachment", bundle);

                    }
                    sendReplyEmail(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), mreplyMailBody);
                } else{
                    if(VerifysubmitEmail()){

                        EmailBody mEmailBody = new EmailBody();
                        mEmailBody.setEmailIds((String[]) senderEmailIds.toArray(new String[senderEmailIds.size()]));
                        mEmailBody.setMessage(etMessage.getText().toString());
                        mEmailBody.setSubject(etSubject.getText().toString());
                        if(mAttachmentModelList.size() > 0){
                            String[] mailFieList = new String[mAttachmentModelList.size()];
                            for(int i = 0; i < mAttachmentModelList.size(); i++){
                                mailFieList[i] = String.valueOf(mAttachmentModelList.get(i).attachmentid);
                            }
                            mEmailBody.setMailFileList(mailFieList);
                            Bundle bundle = new Bundle();
                            bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                            bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                            InGaugeApp.getFirebaseAnalytics().logEvent("mail_attachment", bundle);
                        }
                        Gson gson = new Gson();
                        String jsonStringEmailBody = gson.toJson(mEmailBody).toString();
                        mTagsEditTextEmail.setText("");
                        mTagsEditTextEmail.setTags(null);
                        etMessage.setText("");
                        etSubject.setText("");
                        sendEmail(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), mEmailBody);

                    }
                }

                break;
            case R.id.custom_toolbar_ib_attachement:
                if(checkPermission()){
                    showFileChooser();
                } else{
                    requestPermission();
                }
                break;
        }
    }

    public boolean VerifysubmitEmail(){


        if(!(senderEmailIds != null && senderEmailIds.size() > 0)){
            Toast.makeText(mContext, mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_recipients_required)), Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(etSubject.getText().toString().trim())){
            Toast.makeText(mContext, mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_subject_required)), Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(etMessage.getText().toString().trim())){

            Toast.makeText(mContext, mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_message_required)), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    public boolean VerifyOnBackPressed(){


        if(!(senderEmailIds != null && senderEmailIds.size() > 0)){
            return false;
        }
        if(TextUtils.isEmpty(etSubject.getText().toString().trim())){
            return false;
        }
        if(TextUtils.isEmpty(etMessage.getText().toString().trim())){
            return false;
        }
        return true;

    }

    @Override
    public void onTagsChanged(Collection<String> tags){

        Logger.Error("Tags changed: ");
        String[] arrayTagsString = (String[]) tags.toArray(new String[tags.size()]);
        try{
            if(arrayTagsString.length > 0){
                List<String> emailIds = new ArrayList<>();
                for(int i = 0; i < arrayTagsString.length; i++){
                    emailIds.add(arrayTagsString[i].split("\\(")[1]);

                }
                senderEmailIds = new ArrayList<>();
                for(int i = 0; i < emailIds.size(); i++){
                    for(int j = 0; j < recipientListModel.getData().size(); j++){
                        if(emailIds.get(i).contains(recipientListModel.getData().get(j).getEmail())){
                            senderEmailIds.add(String.valueOf(recipientListModel.getData().get(j).getId()));
                        }
                    }
                }
            } else{

                if(senderEmailIds != null && senderEmailIds.size() > 0){
                    senderEmailIds.clear();
                }
                senderEmailIds = new ArrayList<>();
            }
        } catch(Exception e){
            e.printStackTrace();
        }

        //Logger.Error(" Arrays of Tag Edit Text" + Arrays.toString(tags.toArray()));
    }

    @Override
    public void onEditingFinished(){
        InputMethodManager imm = (InputMethodManager) mHomeActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mTagsEditTextEmail.getWindowToken(), 0);
        mTagsEditTextEmail.clearFocus();
    }


    private void getRecipientListApi(String authToken, int tenantId){
        mHomeActivity.startProgress(mHomeActivity);
        mCall = apiInterface.getRecipientList(tenantId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                mHomeActivity.endProgress();

                if(response.body() != null){
                    recipientListModel = (RecipientListModel) response.body();
                    //mTagEmailAdapter = new TagEmailAdapter(mHomeActivity,R.layout.row_email_tags,recipientListModel.getData());
                    List<String> mStrings = new ArrayList<String>();
                    List<String> mEmailIds = new ArrayList<String>();
                    for(int i = 0; i < recipientListModel.getData().size(); i++){
                        String text = recipientListModel.getData().get(i).getName() + "(" + recipientListModel.getData().get(i).getEmail() + ")";
                        mStrings.add(text);
                        mEmailIds.add(recipientListModel.getData().get(i).getId().toString());

                    }
                    mTagsEditTextEmail.setAdapter(new ArrayAdapter<>(mHomeActivity,
                            android.R.layout.simple_dropdown_item_1line, mStrings));
                    //mTagsEditTextEmail.setAdapter(mTagEmailAdapter);
                } else if(response.raw() != null){

                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                Logger.Error("Exception " + t.getMessage());
                mHomeActivity.endProgress();

            }
        });

    }


    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setImageResource(R.drawable.ic_email_sent);
        mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        //mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.str_mail));
        mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail)));
        mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
        mHomeActivity.ibAttachment.setOnClickListener(this);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event){

                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    if(keyCode == KeyEvent.KEYCODE_BACK){
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        AlertDialog dialog;
                        builder.setTitle("");
                        builder.setMessage(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_discard_message)));
                        builder.setCancelable(false);
                        builder.setPositiveButton(
                                getActivity().getResources().getString(R.string.alert_yes),
                                new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                        dialog.dismiss();
                                        mHomeActivity.onBackPressed();
                                    }
                                });
                        builder.setNegativeButton(
                                getActivity().getResources().getString(R.string.alert_no), new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                        dialog.dismiss();
                                    }
                                }
                        );
                        //  builder.show();
                        dialog = builder.create();
                        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        /*if(VerifyOnBackPressed()){

                        }*/
                        dialog.show();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.ibMenu.setImageResource(R.drawable.ib_menu);
        mHomeActivity.ibMenu.setVisibility(View.GONE);

        mHomeActivity.tvApply.setVisibility(View.GONE);

        //TODO : This Attachment icon will show only replyable content is available for the thread
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
    }


    void sendReplyEmail(String authToken, ReplyMailBody replyMailBody){

        /*{"emailId":["10583","10590","646"],
            "subject":"New Mail",
                "message":"ABC"
        }*/
        // mHomeActivity.startProgress(mContext);
        mCall = apiInterface.sendreplyEmail(replyMailBody);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                mHomeActivity.endProgress();
                try{
                    JSONObject mailData = new JSONObject(response.body().string());

                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    InGaugeApp.getFirebaseAnalytics().logEvent("reply_mail", bundle);
                    Logger.Error("Response " + mailData.toString());
                    mHomeActivity.onBackPressed();
                    mHomeActivity.onBackPressed();
                    //mHomeActivity.onBackPressed();

                } catch(JSONException e){
                    e.printStackTrace();
                } catch(IOException e){
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    void sendEmail(String authToken, EmailBody emailbody){

        /*{"emailId":["10583","10590","646"],
            "subject":"New Mail",
                "message":"ABC"
        }*/
        mHomeActivity.startProgress(mContext);
        mCall = apiInterface.sendEmail(emailbody);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                mHomeActivity.endProgress();
                try{
                    JSONObject mailData = new JSONObject(response.body().string());
                    mHomeActivity.onBackPressed();
                    Logger.Error("Response " + mailData.toString());
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    InGaugeApp.getFirebaseAnalytics().logEvent("send_mail", bundle);
                } catch(JSONException e){
                    e.printStackTrace();
                } catch(IOException e){
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    @Override
    public void onItemClick(View view, int position){
        //Log.i("TAG", "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);

    }


    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(mHomeActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(result == PackageManager.PERMISSION_GRANTED){
            return true;

        } else{

            return false;
        }
    }

    private void requestPermission(){

        ActivityCompat.requestPermissions(mHomeActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch(requestCode){
            case PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    showFileChooser();
                } else{

                    Snackbar.make(recyclerView, "Permission Denied, Please allow to proceed !", Snackbar.LENGTH_LONG).show();

                }
                break;
        }
    }

    private void showFileChooser(){
/*      Intent intent = new Intent();
        intent.setType("file*//*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);*/
        final String FILE_MULTIPLE_TYPES_INTENT = "intent.putExtra(FilePickerActivity.FILE, true);\nintent.putExtra(FilePickerActivity.MULTIPLE_TYPES, new String[]{FilePickerActivity.MIME_IMAGE, FilePickerActivity.MIME_PDF});";
        Intent intent = new Intent(mHomeActivity, FilePickerActivity.class);
        intent.putExtra(FilePickerConstants.FILE, true);
        intent.putExtra(FilePickerConstants.MULTIPLE_TYPES, new String[]{FilePickerConstants.MIME_IMAGE, FilePickerConstants.MIME_PDF, FilePickerConstants.MIME_VIDEO});
        startActivityForResult(intent, MY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MY_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                selectedFileUri = FilePickerUriHelper.getUri(data);
                //Toast.makeText(mHomeActivity, FilePickerUriHelper.getUriString(data), Toast.LENGTH_SHORT).show();
                uploadDocument(InGaugeSession.read(getResources().getString(R.string.key_auth_token), ""), selectedFileUri);
                //If its not an image we don't load it

            } /*else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(FilePickerExampleActivity.this, "User Canceled", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CODE_FAILURE) {
                Toast.makeText(FilePickerExampleActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }*/
        }
       /* if (resultCode == Activity.RESULT_OK) {
            if (requestCode == MY_REQUEST_CODE) {
                if (data == null) {
                    //no data present
                    return;
                }
                selectedFileUri = data.getData();

                //  selectedFilePath = FileUtils.getPath(this, selectedFileUri);
                if (selectedFileUri != null && !selectedFileUri.equals("")) {
                    Logger.Error(" Selected File Path:" + selectedFileUri.getPath());
                    uploadDocument(InGaugeSession.read(getResources().getString(R.string.key_auth_token), ""), selectedFileUri);

                } else {
                    Toast.makeText(mHomeActivity, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }
            }
        }*/
    }

    private void uploadDocument(String authToken, final Uri mFileUriDoc){
        mHomeActivity.startProgress(mHomeActivity);


        final File mFiletoUpload = FileUtils.getFile(mHomeActivity, mFileUriDoc);

        //RequestBody requestFile = RequestBody.create(MediaType.parse(mHomeActivity.getApplicationContext().getContentResolver().getType(mFileUriDoc)), mFiletoUpload);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mFiletoUpload);


        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", mFiletoUpload.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = mFiletoUpload.getName();
        RequestBody filenameRequest = RequestBody.create(okhttp3.MultipartBody.FORM, descriptionString);

        final Call<ResponseBody> call = apiInterface.uploadFile(filenameRequest, body);


        call.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response){
                if(response.isSuccessful()){


                    try{
                        JSONObject mJsonObject = new JSONObject(response.body().string());
                        AttachmentModel attachmentModel = new AttachmentModel();
                        attachmentModel.attachmentid = mJsonObject.optJSONObject("data").optInt("mailAttachmentID");
                        attachmentModel.filename = mFiletoUpload.getName();
                        int fileSize = Integer.parseInt(String.valueOf(mFiletoUpload.length() / 1024));
                        attachmentModel.fileSize = fileSize;
                        attachmentModel.fileType = FileUtils.getMimeType(mFiletoUpload);
                        if(!TextUtils.isEmpty(FileUtils.getExtension(mFiletoUpload.getName()))){
                            attachmentModel.extension = FileUtils.getExtension(mFiletoUpload.getName());
                        } else{
                            attachmentModel.extension = "file";
                        }

                        Logger.Error("Atachment ID" + attachmentModel.attachmentid);
                        Logger.Error("MIME TYPE " + attachmentModel.fileType);
                        Logger.Error("Extension  " + attachmentModel.extension);
                        if(mAttachmentModelList != null){
                            if(mAttachmentModelList.size() > 0){
                                mAttachmentModelList.add(attachmentModel);
                                adapter.notifyDataSetChanged();
                            } else{
                                mAttachmentModelList.add(attachmentModel);
                                adapter = new AttachmentAdapter(mHomeActivity, mAttachmentModelList, true, 0);
                                recyclerView.setAdapter(adapter);
                            }
                        }
                        Toast.makeText(mContext, mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_attachment_success)), Toast.LENGTH_SHORT).show();
                        mHomeActivity.endProgress();

                    } catch(JSONException e){
                        mHomeActivity.endProgress();
                        e.printStackTrace();
                    } catch(IOException e){
                        mHomeActivity.endProgress();
                        e.printStackTrace();
                    }

                } else{
                    mHomeActivity.endProgress();
                    Logger.Error("server contact failed!!!! to Upload File");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){

             /*   Logger.Error("error >>" + t.toString());
                Logger.Error("error >>" + t.getMessage().toString());*/
                try{
                    if(ProgressActivity.instance != null)
                        ProgressActivity.instance.finish();
                } catch(Exception e){
                    e.printStackTrace();
                }

//                Logger.Error("error >>" + t.getCause().toString());

            }
        });


    }


}
