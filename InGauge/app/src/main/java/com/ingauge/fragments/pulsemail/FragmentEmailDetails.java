package com.ingauge.fragments.pulsemail;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.ProgressActivity;
import com.ingauge.adapters.AttachmentAdapter;
import com.ingauge.adapters.MailDetailsAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.pojo.AttachmentModel;
import com.ingauge.pojo.EmailBody;
import com.ingauge.pojo.InboxModel;
import com.ingauge.pojo.ReplyMailBody;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.FileUtils;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import dk.nodes.filepicker.FilePickerActivity;
import dk.nodes.filepicker.FilePickerConstants;
import dk.nodes.filepicker.uriHelper.FilePickerUriHelper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 13-Jul-17.
 */

public class FragmentEmailDetails extends BaseFragment implements View.OnClickListener,AttachmentAdapter.ItemClickListener {


    private static final int PERMISSION_REQUEST_CODE = 001;
    private static final int PICK_FILE_REQUEST = 002;
    public static int MY_REQUEST_CODE = 10;

    private HomeActivity mHomeActivity;
    private Context mContext;

    //private MailDetailsExpandableAdapter mailDetailsExpandableAdapter;
    private MailDetailsAdapter mailDetailsAdapter;
    private ListView mExpandableListViewEmailDetails;
    private ImageView ivReply;
    private TextView tvMailSubject;
    private LinearLayout llReply;
    //private TextView tvMailTime;
    View rootView;
    public static String KEY_EMAIL_ID = "key_email_id";
    public static String KEY_EMAIL_TYPE = "key_email_type";
    private String emailId = "";
    private String emailType = "";
    Call mCall;
    APIInterface apiInterface;
    private List<InboxModel> inboxModelList;
    private List<String> inboxChildMessageList;
    private List<AttachmentModel> attachmentModelList;
    private List<AttachmentModel> mAttachmentModelListForReply;

    private InboxModel inboxModel;
    private EditText etReplyText;
    private RelativeLayout llBottomReply;

    Uri selectedFileUri;
    private AttachmentAdapter adapter;
    private RecyclerView rvAttachments;
    int numberOfColumns = 3;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mAttachmentModelListForReply  = new ArrayList<>();
        adapter = new AttachmentAdapter(mHomeActivity, mAttachmentModelListForReply , true, 0);
        adapter.setClickListener(this);
        if (getArguments() != null) {
            emailId = getArguments().getString(KEY_EMAIL_ID);
            emailType = getArguments().getString(KEY_EMAIL_TYPE);
        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_email_details, container, false);
        initView(rootView);

        getEmailDetails(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), emailId, emailType);
        return rootView;
    }


    public void initView(View view) {

        mExpandableListViewEmailDetails = (ListView) view.findViewById(R.id.fragment_email_details_exp_lv_thread);
        tvMailSubject = (TextView) view.findViewById(R.id.fragment_email_details_tv_reply_subject);
        //  tvMailTime = (TextView) view.findViewById(R.id.fragment_email_details_tv_time);
        ivReply = (ImageView) view.findViewById(R.id.fragment_email_details_iv_reply);
        etReplyText = (EditText) view.findViewById(R.id.fragment_email_details_edt_reply_text);
        etReplyText.setHint(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_reply_title)));
        llBottomReply = (RelativeLayout)view.findViewById(R.id.ll_bottom_reply);
        //Toast.makeText(mContext, mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_message_required)), Toast.LENGTH_SHORT).show();
        ivReply.setOnClickListener(this);
        rvAttachments = (RecyclerView)view.findViewById(R.id.rv_attachments);
        rvAttachments .setLayoutManager(new GridLayoutManager(mHomeActivity, numberOfColumns));
        //llReply = (LinearLayout)view.findViewById(R.id.fragment_email_details_layout_reply);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.ibAttachment.setOnClickListener(this);
        mHomeActivity.ibMenu.setImageResource(R.drawable.ic_email_sent);
        //TODO : This Attachment icon will show only replyable content is available for the thread
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(etReplyText.getText().toString())){
                    if(inboxModelList!=null  && inboxModelList.size()>0){

                        ReplyMailBody mEmailBody = new ReplyMailBody();
                        mEmailBody.setReplyId(inboxModelList.get(0).getSenderID());
                        mEmailBody.setMessage(etReplyText.getText().toString());
                        mEmailBody.setSubject(inboxModelList.get(0).getSubject());
                        if (mAttachmentModelListForReply.size() > 0) {
                            String[] mailFieList = new String[mAttachmentModelListForReply.size()];
                            for (int i = 0; i < mAttachmentModelListForReply.size(); i++) {
                                mailFieList[i] = String.valueOf(mAttachmentModelListForReply.get(i).attachmentid);
                            }
                            mEmailBody.setMailFileList(mailFieList);
                        }
                        etReplyText.setText("");
                        Gson gson = new Gson();
                        String jsonStringEmailBody  = gson.toJson(mEmailBody).toString();
                        sendReplyEmail(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),mEmailBody);
                    }
                }else{
                    //Toast.makeText(mHomeActivity,mContext.getResources().getString(R.string.enter_message),Toast.LENGTH_LONG).show();
                    Toast.makeText(mContext, mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_message_required)), Toast.LENGTH_SHORT).show();
                }

            }
        });

        if (llBottomReply.getVisibility() == View.VISIBLE) {
            //etReplyText.setVisibility(View.VISIBLE);
            llBottomReply.setVisibility(View.VISIBLE);
            ivReply.setVisibility(View.GONE);
            mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
            mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
            //mExpandableListViewEmailDetails.setVisibility(View.GONE);
            //img_reply.setImageResource(R.drawable.cancel_reply);
            //img_reply_mail.setVisibility(View.VISIBLE);
            //DisplayCurrentTime();
        }
    }

    void getEmailDetails(String authToken, String emailId, String mailType) {
        mHomeActivity.startProgress(mContext);

        mCall = apiInterface.getEmailDetails(emailId, mailType);
        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mHomeActivity.endProgress();
                inboxModelList = new ArrayList<InboxModel>();
                inboxChildMessageList = new ArrayList<String>();
                attachmentModelList = new ArrayList<AttachmentModel>();
                if (response.body() != null) {

                    try {
                        JSONObject dataJsonObject = new JSONObject(response.body().string());
                        //First Leval
                        JSONArray mJsonArray = dataJsonObject.optJSONObject("data").optJSONArray("userMessageRecipientList");
                        JSONObject mailData = (JSONObject) mJsonArray.opt(0);


                        boolean hasparent = false;
                        do {
                            hasparent = false;
                            inboxModel = new InboxModel();
                            inboxModel.setHasReplies(mailData.optBoolean("hasReplies"));
                            inboxModel.setIsRead(mailData.optBoolean("isRead"));
                            JSONArray mJsonObjectAttachmentList = mailData.optJSONArray("mobileMailAttachmentList");
                            for(int i=0;i< mJsonObjectAttachmentList.length();i++){
                                AttachmentModel attachmentModel = new AttachmentModel();
                                JSONObject mJsonObjectAttachmennt =(JSONObject) mJsonObjectAttachmentList.opt(i);
                                attachmentModel.attachmentid =mJsonObjectAttachmennt.optInt("id");
                                attachmentModel.filename = mJsonObjectAttachmennt.optString("fileName");
                                attachmentModel.fileType = mJsonObjectAttachmennt.optString("fileType");
                                attachmentModelList.add(attachmentModel);
                            }

                            inboxModel.setAttachmentModelList(attachmentModelList);
                            //inboxModel.setMessageBody(mailData.toString());
                            inboxModel.setID(mailData.optInt("id"));
                            inboxModel.setIsCheck(true);
                            //JSONObject userMessageJsonObject = mailData.optJSONObject("userMessage");
                            inboxModel.setMessageSort(mailData.optString("userMessage"));


                            //   inboxModel.setSentDate(UiUtils.changeDateFormateMail(mailData.getString("timestamp")));


                            String readTime = UiUtils.ConvertToDeviceLocal(mailData.optLong("readTime"), " MMM d");
                            inboxModel.setSentDate(readTime);
                           /* DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date date = df.parse(startDateString);
                            Logger.Error("Time Stamp" + startDateString);
                            Logger.Error("Time Zone " + df.getTimeZone().toString());
                            Logger.Error("Time Zone " + df.getTimeZone().getDisplayName());
                            String aTimeAgoString = UiUtils.calculateFeedTime(date.getTime());*/



                            inboxModel.setSubject(mailData.optString("userMessageSubject"));
                            //inboxModel.setIsExpanded(false);
                            //JSONArray mJsonArraySender = mailData.optJSONArray("senderList");
                            //  JSONObject senderJsonIndex = (JSONObject) mJsonArraySender.opt(0);
                            // JSONObject sender = senderJsonIndex.optJSONObject("sender");
                            String profileUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(mContext) + "/" + mailData.optInt("senderId");
                            Logger.Error("<<  Profile URL >>>" + profileUrl);
                            inboxModel.setProfileUrl(profileUrl);

                            inboxModel.setSenderEmail(mailData.getString("senderEmail"));
                            inboxModel.setSenderID(mailData.optInt("senderId"));
                            inboxModel.setSender(mailData.getString("senderName"));

                            //  JSONObject recipient = mailData.getJSONObject("recipient");
                            inboxModel.setToName("To: " + mailData.getString("recipientName") + "\n" + mailData.getString("recipientEmail"));
                            inboxChildMessageList.add(inboxModel.getMessageSort());
                            inboxModelList.add(inboxModel);

                            if (mailData.has("parentMessageObject")) {
                                mailData = mailData.getJSONObject("parentMessageObject");//Other leval
                                hasparent = true;
                            }

                        } while (hasparent);

                        Logger.Error("<<< SIZE >>" + inboxModelList.size());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    setEmailDetailAdapter(inboxModelList, inboxChildMessageList);

                } else if (response.raw() != null) {
                    mHomeActivity.endProgress();
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    mHomeActivity.endProgress();
                }
            }


            @Override
            public void onFailure(Call call, Throwable t) {
                mHomeActivity.endProgress();
            }
        });
    }

    public void setEmailDetailAdapter(List<InboxModel> inboxModelList, List<String> inboxChildMessageList) {

        if(inboxModelList!=null && inboxModelList.size()>0){
            //hashmap : listDataChild modelListLinkedHashMap
            LinkedHashMap<InboxModel, String> modelListLinkedHashMap = new LinkedHashMap<>();

            //listdataheader = inboxModelList
            for (int i = 0; i < inboxModelList.size(); i++) {
                modelListLinkedHashMap.put(inboxModelList.get(i), inboxChildMessageList.get(i));
            }

            mailDetailsAdapter = new MailDetailsAdapter(mContext, FragmentEmailDetails.this, inboxModelList);
            mExpandableListViewEmailDetails.setAdapter(mailDetailsAdapter);

        /*mExpandableListViewEmailDetails.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                Logger.Error("On Group Clicked Worked");
                parent.expandGroup(groupPosition);
                return false;
            }
        });*/
            tvMailSubject.setText(inboxModelList.get(0).getSubject());
            if (inboxModelList.get(0).getSenderID() == UiUtils.getUserId(mContext)) {
                ivReply.setVisibility(View.GONE);
            } else {
                ivReply.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHomeActivity.ibMenu.setImageResource(R.drawable.ib_menu);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        //TODO : This Attachment icon will show only replyable content is available for the thread
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_email_details_iv_reply:
                if (llBottomReply.getVisibility() == View.GONE) {
                    //etReplyText.setVisibility(View.VISIBLE);
                    llBottomReply.setVisibility(View.VISIBLE);
                    ivReply.setVisibility(View.GONE);
                    mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
                    mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
                    //mExpandableListViewEmailDetails.setVisibility(View.GONE);
                    //img_reply.setImageResource(R.drawable.cancel_reply);
                    //img_reply_mail.setVisibility(View.VISIBLE);
                    //DisplayCurrentTime();
                } else {
                    etReplyText.setVisibility(View.GONE);
                    //llBottomReply.setVisibility(View.GONE);
                    ivReply.setVisibility(View.VISIBLE);
                    mHomeActivity.ibAttachment.setVisibility(View.GONE);
                    mHomeActivity.ibMenu.setVisibility(View.GONE);
                    //mExpandableListViewEmailDetails.setVisibility(View.VISIBLE);
                    //img_reply.setImageResource(R.drawable.mail_reply_big);
                    //img_reply_mail.setVisibility(View.GONE);
                }

                break;
            case R.id.custom_toolbar_ib_attachement:
                if (checkPermission()) {
                    showFileChooser();
                } else {
                    requestPermission();
                }
                break;
        }
    }



    void sendReplyEmail(String authToken, ReplyMailBody replyMailBody) {

        /*{"emailId":["10583","10590","646"],
            "subject":"New Mail",
                "message":"ABC"
        }*/
        mHomeActivity.startProgress(mContext);
        mCall = apiInterface.sendreplyEmail(replyMailBody);
        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mHomeActivity.endProgress();
                try {
                    JSONObject mailData = new JSONObject(response.body().string());

                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role",InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    InGaugeApp.getFirebaseAnalytics().logEvent("reply_mail", bundle);
                    Logger.Error("Response " + mailData.toString());
                    mHomeActivity.onBackPressed();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call call, Throwable t) {
                mHomeActivity.endProgress();
            }
        });
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(mHomeActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    showFileChooser();
                } else {

                    Snackbar.make(rootView, "Permission Denied, Please allow to proceed !", Snackbar.LENGTH_LONG).show();

                }
                break;
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(mHomeActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;

        } else {

            return false;
        }
    }

    private void showFileChooser() {
/*      Intent intent = new Intent();
        intent.setType("file*//*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);*/
        final String FILE_MULTIPLE_TYPES_INTENT = "intent.putExtra(FilePickerActivity.FILE, true);\nintent.putExtra(FilePickerActivity.MULTIPLE_TYPES, new String[]{FilePickerActivity.MIME_IMAGE, FilePickerActivity.MIME_PDF});";
        Intent intent = new Intent(mHomeActivity, FilePickerActivity.class);
        intent.putExtra(FilePickerConstants.FILE, true);
        intent.putExtra(FilePickerConstants.MULTIPLE_TYPES, new String[]{FilePickerConstants.MIME_IMAGE, FilePickerConstants.MIME_PDF,FilePickerConstants.MIME_VIDEO});
        startActivityForResult(intent, MY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                selectedFileUri = FilePickerUriHelper.getUri(data);
                //Toast.makeText(mHomeActivity, FilePickerUriHelper.getUriString(data), Toast.LENGTH_SHORT).show();
                uploadDocument(InGaugeSession.read(getResources().getString(R.string.key_auth_token), ""), selectedFileUri);
                //If its not an image we don't load it

            } /*else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(FilePickerExampleActivity.this, "User Canceled", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CODE_FAILURE) {
                Toast.makeText(FilePickerExampleActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }*/
        }
       /* if (resultCode == Activity.RESULT_OK) {
            if (requestCode == MY_REQUEST_CODE) {
                if (data == null) {
                    //no data present
                    return;
                }
                selectedFileUri = data.getData();

                //  selectedFilePath = FileUtils.getPath(this, selectedFileUri);
                if (selectedFileUri != null && !selectedFileUri.equals("")) {
                    Logger.Error(" Selected File Path:" + selectedFileUri.getPath());
                    uploadDocument(InGaugeSession.read(getResources().getString(R.string.key_auth_token), ""), selectedFileUri);

                } else {
                    Toast.makeText(mHomeActivity, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }
            }
        }*/
    }

    private void uploadDocument(String authToken, final Uri mFileUriDoc) {
        mHomeActivity.startProgress(mHomeActivity);




        final File mFiletoUpload = FileUtils.getFile(mHomeActivity, mFileUriDoc);

        //RequestBody requestFile = RequestBody.create(MediaType.parse(mHomeActivity.getApplicationContext().getContentResolver().getType(mFileUriDoc)), mFiletoUpload);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mFiletoUpload);


        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", mFiletoUpload.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = mFiletoUpload.getName();
        RequestBody filenameRequest = RequestBody.create(okhttp3.MultipartBody.FORM, descriptionString);

        final Call<ResponseBody> call = apiInterface.uploadFile(filenameRequest, body);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {


                    try {
                        JSONObject mJsonObject = new JSONObject(response.body().string());
                        AttachmentModel attachmentModel = new AttachmentModel();
                        attachmentModel.attachmentid = mJsonObject.optJSONObject("data").optInt("mailAttachmentID");
                        attachmentModel.filename = mFiletoUpload.getName();
                        int fileSize = Integer.parseInt(String.valueOf(mFiletoUpload.length() / 1024));
                        attachmentModel.fileSize = fileSize;
                        attachmentModel.fileType = FileUtils.getMimeType(mFiletoUpload);
                        if(!TextUtils.isEmpty(FileUtils.getExtension(mFiletoUpload.getName()))){
                            attachmentModel.extension = FileUtils.getExtension(mFiletoUpload.getName());
                        }else{
                            attachmentModel.extension = "file";
                        }

                        Logger.Error("Atachment ID" + attachmentModel.attachmentid);
                        Logger.Error("MIME TYPE " + attachmentModel.fileType);
                        Logger.Error("Extension  " + attachmentModel.extension);
                        if (mAttachmentModelListForReply != null) {
                            if (mAttachmentModelListForReply.size() > 0) {
                                mAttachmentModelListForReply.add(attachmentModel);
                                adapter.notifyDataSetChanged();
                            } else {
                                mAttachmentModelListForReply.add(attachmentModel);
                                adapter = new AttachmentAdapter(mHomeActivity, mAttachmentModelListForReply, true, 0);
                                rvAttachments.setAdapter(adapter);
                            }
                        }
                        llBottomReply.setVisibility(View.VISIBLE);
                        ivReply.setVisibility(View.GONE);
                        mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
                        mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
                        Toast.makeText(mContext, mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_mail_attachment_success)), Toast.LENGTH_SHORT).show();
                        mHomeActivity.endProgress();

                    } catch (JSONException e) {
                        mHomeActivity.endProgress();
                        e.printStackTrace();
                    } catch (IOException e) {
                        mHomeActivity.endProgress();
                        e.printStackTrace();
                    }

                } else {
                    mHomeActivity.endProgress();
                    Logger.Error("server contact failed!!!! to Upload File");
                    llBottomReply.setVisibility(View.VISIBLE);
                    ivReply.setVisibility(View.GONE);
                    mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
                    mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                llBottomReply.setVisibility(View.VISIBLE);
                ivReply.setVisibility(View.GONE);
                mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
                mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
             /*   Logger.Error("error >>" + t.toString());
                Logger.Error("error >>" + t.getMessage().toString());*/
                try {
                    if (ProgressActivity.instance != null)
                        ProgressActivity.instance.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Logger.Error("error >>" + t.getCause().toString());

            }
        });


    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
