package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.ObservationDashboardListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.dateselection.DateSelectionFragmentForObsCC;
import com.ingauge.listener.EndlessRecyclerViewScrollListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ObservationDashboardModel;
import com.ingauge.pojo.ObservationDashboardStatusModel;
import com.ingauge.pojo.ObservationSurveyCategoryModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 18-May-17.
 */

public class ObservationDashboardFragment extends BaseFragment implements View.OnClickListener{
    Context mContext;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private RecyclerView rvDashboardList;
    private ImageView imgFilter;
    APIInterface apiInterface;
    private TextView tvFromDate;
    private TextView tvToDate;
    private LinearLayout llNoDataFound;
    Calendar calendar;
    Call mCall;
    private TextView tvSelectDate;
    private TextView tvSelectPreviousDate;
    RelativeLayout relativeDateCompare;
    private HomeActivity mHomeActivity;
    private ObservationDashboardListAdapter observationDashboardListAdapter;
    private ObservationSurveyCategoryModel observationSurveyCategoryModel;
    private ObservationDashboardStatusModel observationDashboardStatusModel;
    private ArrayList<ObservationDashboardStatusModel> observationDashboardStatusModelArrayList;
    private TextView tvNoData;
    private ObservationDashboardModel observationDashboardModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModelForObserveAgent;
    private ArrayList<FilterBaseData> mFilterBaseDatas;
    private int total = 20;
    private int first = 0;
    private boolean isLoadMore = false;
    private EndlessRecyclerViewScrollListener scrollListener;

    SimpleDateFormat sdfServerDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

    ProgressBar mProgressBarRvEnd;
    View rootView = null;
    private Integer finaltotal = 0;
    private List<ObservationDashboardModel.Data.Datum> mDatumList = new ArrayList<>();

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }

    public static Fragment newInstance(String text, int color){
        Fragment frag = new ObservationDashboardFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment

        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_observation_dashboard, container, false);
            initData(rootView);
            rvDashboardList.addOnScrollListener(new RecyclerView.OnScrollListener(){
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState){
                    super.onScrollStateChanged(recyclerView, newState);

                    if(!recyclerView.canScrollVertically(1)){
                        if(isLoadMore){
                            if(observationDashboardModel.getData().getData().size() <= finaltotal){
                                first = first + total;
                                Logger.Error("<<<< First >>> " + first);
                                mProgressBarRvEnd.setVisibility(View.VISIBLE);
                                getObservationSurvey(true);
                            }
                        }

                    }
                }
            });
        } else{

            if(mHomeActivity.isDashboardObsCoachingUpdateAfterApply()){
                first = 0;
                isLoadMore = false;
                mDatumList = new ArrayList<>();
                mHomeActivity.setDashboardObsCoachingUpdateAfterApply(false);
                rvDashboardList.setVisibility(View.GONE);
                initData(rootView);
            }
            if(rootView != null && rootView.getParent() != null){
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }
        }
        return rootView;

        //mHomeActivity.setDashboardObsCoachingUpdateAfterApply(true);
    }


    public void initData(View view){

        mProgressBarRvEnd = (ProgressBar) view.findViewById(R.id.rv_end_progress);
        rvDashboardList = (RecyclerView) view.findViewById(R.id.rv_feeds_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        rvDashboardList.setLayoutManager(mLayoutManager);
        rvDashboardList.setItemAnimator(new DefaultItemAnimator());
        tvNoData = (TextView) view.findViewById(R.id.tv_nodata);
        relativeDateCompare = (RelativeLayout) view.findViewById(R.id.relative_date_compare);
        tvFromDate = (TextView) view.findViewById(R.id.tv_fromDate);
        tvToDate = (TextView) view.findViewById(R.id.tv_toDate);
        llNoDataFound = (LinearLayout)view.findViewById(R.id.fragment_observation_ll_placeholder);

        imgFilter = (ImageView) view.findViewById(R.id.img_filter);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.in_guage_blue,
                R.color.rv_grey_color,
                R.color.red);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh(){
                // Refresh items
                first = 0;
                isLoadMore = false;
                mDatumList = new ArrayList<>();
                swipeRefreshLayout.setRefreshing(true);
                getLocationForObserveAgent(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1), String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), "name", "ASC", "Normal", false);
            }
        });

        tvSelectDate = (TextView) view.findViewById(R.id.tv_selectdate);
        tvSelectPreviousDate = (TextView) view.findViewById(R.id.tv_previousdate);
        imgFilter.setOnClickListener(this);
        calendar = Calendar.getInstance();
        tvFromDate.setOnClickListener(this);
        tvToDate.setOnClickListener(this);
        mFilterBaseDatas = new ArrayList<>();
        relativeDateCompare.setOnClickListener(this);
        Calendar calendar;
        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date date = calendar.getTime();

        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_observation_date_filter), "").equalsIgnoreCase("")){
            InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_observation_date_filter), sdf.format(date));
            InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_observation_date_filter), sdf.format(new Date()));
            //InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_observation_date_filter_server), sdfServer.format(date));
            InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_observation_date_filter_server), "" + calendar.getTimeInMillis());
            //InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_observation_date_filter_server), sdfServer.format(new Date()));
            InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_observation_date_filter_server), "" + Calendar.getInstance().getTimeInMillis());
        }

        //Adding Status Category Data
        observationDashboardStatusModelArrayList = new ArrayList<>();
        observationDashboardStatusModel = new ObservationDashboardStatusModel();
        observationDashboardStatusModel.setStatusID(-1);
        observationDashboardStatusModel.setStatusName("All Status");
        observationDashboardStatusModelArrayList.add(observationDashboardStatusModel);

        observationDashboardStatusModel = new ObservationDashboardStatusModel();
        observationDashboardStatusModel.setStatusID(0);
        observationDashboardStatusModel.setStatusName("Completed");
        observationDashboardStatusModelArrayList.add(observationDashboardStatusModel);

        observationDashboardStatusModel = new ObservationDashboardStatusModel();
        observationDashboardStatusModel.setStatusID(1);
        observationDashboardStatusModel.setStatusName("Pending");
        observationDashboardStatusModelArrayList.add(observationDashboardStatusModel);

        observationDashboardStatusModel = new ObservationDashboardStatusModel();
        observationDashboardStatusModel.setStatusID(2);
        observationDashboardStatusModel.setStatusName("Cancelled");
        observationDashboardStatusModelArrayList.add(observationDashboardStatusModel);

        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), null) == null){
            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_name), "All");
            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), "-1");
        }

        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_observation_date_filter), "") != null && InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_observation_date_filter), "").length() > 0){
            tvSelectDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_observation_date_filter), "") + " To " + InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_observation_date_filter), ""));
        } else{
            tvSelectDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_observation_date_filter), ""));
        }


        getLocationForObserveAgent(InGaugeSession.read(getString(R.string.key_auth_token), ""),
                InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)),
                "name",
                "ASC",
                "Normal",
                true);

        scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) mLayoutManager){
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view){
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //loadNextDataFromApi(page);
                Logger.Error("END_SCROLL" + page + "\n" + totalItemsCount);
               /* total=10;
                first=totalItemsCount;
                getObservationSurvey();*/
            }
        };
        rvDashboardList.addOnScrollListener(scrollListener);
    }


    void getLocationForObserveAgent(final String authToken, int tenantId, String userId, String orderBy, String sort, String activeStatus, boolean showProgress){
        mCall = apiInterface.getAccessibleTenantLocationForGoalFilter(tenantId, userId, orderBy, sort, activeStatus);

        if(showProgress)
            mHomeActivity.startProgress(mContext);

        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mAccessibleTenantLocationDataModelForObserveAgent = (AccessibleTenantLocationDataModel) response.body();
                    if(mAccessibleTenantLocationDataModelForObserveAgent.getData() != null && mAccessibleTenantLocationDataModelForObserveAgent.getData().size() > 0){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.filterIndex = 0;
                        mFilterBaseData.filterName = getString(R.string.location);
                        ;
                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_id), null) == null){

                            /*InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_id), String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getId()));
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_name), String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getName()));
                            mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getId());
                            mFilterBaseData.name = mAccessibleTenantLocationDataModelForObserveAgent.getData().get(0).getName();*/

                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_id), String.valueOf(-1));
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_name), "All Locations");
                            mFilterBaseData.id = String.valueOf(-1);
                            mFilterBaseData.name = "All Locations";
                        } else{
                            mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_id), null);
                            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_name), null);
                        }
                        mFilterBaseDatas.add(mFilterBaseData);
                        setLocationforSubTitle();
                        getSurveyCategory(InGaugeSession.read(getString(R.string.key_auth_token), ""), "application/json", String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 0)), true, "Normal", InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), null));
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){

                mHomeActivity.endProgress();
            }
        });
    }

    void getSurveyCategory(final String authToken, String ContentType, String IndustryId, boolean isMobile, String activeStatus, String surveyEntityType){
        mCall = apiInterface.getSurveyCategoryObservation(activeStatus, surveyEntityType);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    observationSurveyCategoryModel = (ObservationSurveyCategoryModel) response.body();
                    if(observationSurveyCategoryModel.getData() != null && observationSurveyCategoryModel.getData().size() > 0){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), null) == null){
                            /*mFilterBaseData.id = String.valueOf(observationSurveyCategoryModel.getData().get(0).getId());
                            mFilterBaseData.name = observationSurveyCategoryModel.getData().get(0).getName();
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_name), String.valueOf(observationSurveyCategoryModel.getData().get(0).getName()));
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), String.valueOf(observationSurveyCategoryModel.getData().get(0).getId()));*/

                            mFilterBaseData.id = String.valueOf(-1);
                            mFilterBaseData.name = "All Categories";
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_name), "All Categories");
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), String.valueOf(-1));


                        } else{
                            mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), null);
                            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_name), null);
                        }

                        mFilterBaseData.filterIndex = 1;
                        mFilterBaseData.filterName = "Category";
                        mFilterBaseDatas.add(mFilterBaseData);

                        getObservationSurvey(false);
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }


    private void getObservationSurvey(boolean isFromLoadMore){
        try{
            JsonObject requestBody = new JsonObject();
            //JsonObject solrInput = new JsonObject();
//            solrInput.addProperty("total", total);
//            solrInput.addProperty("first", first);
//            solrInput.addProperty("searchText", "");

//            JsonArray sortBy = new JsonArray();
//            JsonArray sortOrder = new JsonArray();
//            solrInput.add("sortBy", sortBy);
//            solrInput.add("sortOrder", sortOrder);
//            requestBody.add("solrInput", solrInput);

            String surveyStatus = InGaugeSession.read(mContext.getString(R.string.key_obs_dashboard_selected_status_name), null);
            String surveyStatusId = InGaugeSession.read(mContext.getString(R.string.key_obs_dashboard_selected_status_id), null);

            if(surveyStatus.equalsIgnoreCase("All Status")){
                surveyStatus = "-1";
            }


            Long serverstartLong = Long.parseLong(InGaugeSession.read(mContext.getString(R.string.key_start_date_observation_date_filter_server), null));
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + serverstartLong);
            String serverStartDate = sdfServer.format(serverstartLong);
            Calendar startserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverStartDate + " 00:00:00"));
            startserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + startserverCalendar.getTimeInMillis());

            Long serverEndLong = Long.parseLong(InGaugeSession.read(mContext.getString(R.string.key_end_date_observation_date_filter_server), null));
            String serverEndDate = sdfServer.format(serverEndLong);
            Calendar endserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverEndDate + " 23:59:59"));
            endserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + serverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + endserverCalendar.getTimeInMillis());


            JsonArray tenantlocationIds = new JsonArray();
            tenantlocationIds.add(InGaugeSession.read(mContext.getString(R.string.key_obs_dashboard_selected_tenant_location_id), null));
            JsonArray sortOrderArray = new JsonArray();
            sortOrderArray.add("startDate");
            requestBody.addProperty("sortOrder", "DESC");
            requestBody.addProperty("surveyStatus", surveyStatusId);
            requestBody.addProperty("startDate", startserverCalendar.getTimeInMillis());
            requestBody.addProperty("endDate", endserverCalendar.getTimeInMillis());
            requestBody.addProperty("tenantId", InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1));
            requestBody.addProperty("surveyEntityType", InGaugeSession.read(mContext.getString(R.string.key_obs_cc_survey_entity_type_id), null));
            requestBody.addProperty("first", first);
            requestBody.addProperty("total", total);
            requestBody.addProperty("searchKeywords", "");

            requestBody.addProperty("questionnaireSurveyCategoryId", InGaugeSession.read(mContext.getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), null));
            requestBody.add("tenantLocations", tenantlocationIds);
            requestBody.add("sortBy", sortOrderArray);

            Logger.Error("REQUEST BODY" + requestBody);
            getObservationSurvey(InGaugeSession.read(getString(R.string.key_auth_token), ""), "application/json", String.valueOf(InGaugeSession.read(getString(R.string.key_selected_industry_id), 0)), "true", requestBody, InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1), String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), isFromLoadMore);

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    void getObservationSurvey(String authToken, String ContentType, String IndustryId, String isMobile, JsonObject requestBody, int tenantId, String userID, final boolean isFromLoadMore){
        Call mCall = apiInterface.GetObsSurveyType(requestBody, tenantId, userID);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                mProgressBarRvEnd.setVisibility(View.GONE);
                mHomeActivity.endProgress();

                swipeRefreshLayout.setRefreshing(false);

                observationDashboardModel = (ObservationDashboardModel) response.body();

                boolean isEdit = mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessTakeObservation");

                if(observationDashboardModel != null){
                    if(observationDashboardModel.getData() != null){
                        if(observationDashboardModel.getData().getData() != null && observationDashboardModel.getData().getData().size() > 0){
                            finaltotal = observationDashboardModel.getData().getTotal();
                            if(isFromLoadMore){

                                if(observationDashboardListAdapter != null){
                                    mDatumList.addAll(observationDashboardModel.getData().getData());
                                    observationDashboardListAdapter = new ObservationDashboardListAdapter(mContext, mHomeActivity, isEdit, mDatumList);
                                    observationDashboardListAdapter.notifyDataSetChanged();
                                    rvDashboardList.invalidate();
                                    isLoadMore = true;
                                }
                            } else{
                                mDatumList.addAll(observationDashboardModel.getData().getData());
                                observationDashboardListAdapter = new ObservationDashboardListAdapter(mContext, mHomeActivity, isEdit, mDatumList);
                                rvDashboardList.setAdapter(observationDashboardListAdapter);
                                rvDashboardList.setVisibility(View.VISIBLE);
                                tvNoData.setVisibility(View.GONE);
                                llNoDataFound.setVisibility(View.GONE);
                                isLoadMore = true;
                            }

                        } else{
                            if(isFromLoadMore){

                                isLoadMore = false;
                                //Toast.makeText(mHomeActivity,"No More Survey Found",Toast.LENGTH_SHORT).show();
                            } else{
                                tvNoData.setText("No Survey found!");
                                rvDashboardList.setVisibility(View.GONE);
                                llNoDataFound.setVisibility(View.VISIBLE);
                                tvNoData.setVisibility(View.VISIBLE);

                            }

                        }
                    } else{
                        if(isFromLoadMore){
                            isLoadMore = false;
                        } else{
                            tvNoData.setText("No Survey found!");
                            llNoDataFound.setVisibility(View.VISIBLE);
                            rvDashboardList.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                        }

                    }
                } else{

                    if(isFromLoadMore){
                        isLoadMore = false;
                    } else{
                        tvNoData.setText("No Survey found!");
                        rvDashboardList.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        llNoDataFound.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setLocationforSubTitle(){
        String locationName = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_name), "All Locations");
        mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
        mHomeActivity.tvSubTitle.setText(locationName);
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);

        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
            mHomeActivity.tvTitle.setText("Observation");
        } else{
            mHomeActivity.tvTitle.setText("Coaching");
        }
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        // mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_dashboard)));

        mHomeActivity.ibMenu.setImageResource(R.drawable.ic_add_obs_cc);
        mHomeActivity.tvTitle.setVisibility(View.VISIBLE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        if(mHomeActivity.list != null){
            if(mHomeActivity.list.size() == 0){
                mHomeActivity.tvSubTitle.setVisibility(View.GONE);
            } else{
                mHomeActivity.tvSubTitle.setVisibility(View.GONE);
            }
        } else{
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }

        boolean isAdd = true;

        if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
            isAdd = mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessTakeObservation");
        } else{
            isAdd = mHomeActivity.rolesAndPermissoinObject.optBoolean("canAccessTakeCounterCoaching");
        }

        if(isAdd){
            mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        } else{
            mHomeActivity.ibMenu.setVisibility(View.GONE);
        }

        mHomeActivity.ibMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                UiUtils.clearObsCCLocal(mContext, true);
                if(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mContext.getResources().getString(R.string.survey_entity_type_id_observation))){
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), mContext.getResources().getString(R.string.survey_entity_type_id_observation));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), mContext.getResources().getString(R.string.survey_entity_type_name_observation));
                } else{
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), mContext.getResources().getString(R.string.survey_entity_type_id_counter_coaching));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), mContext.getResources().getString(R.string.survey_entity_type_name_counter_coaching));
                }
                ObservationAgentFragment fragment = new ObservationAgentFragment();
                mHomeActivity.replace(fragment, mContext.getResources().getString(R.string.tag_tab_observation));

            }
        });
        mHomeActivity.toolbar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ObservationDynamicFilterFragment mFilterListFragment = new ObservationDynamicFilterFragment();
                Bundle mBundle = new Bundle();
                //mBundle.putParcelableArrayList(FilterListFragment.KEY_FILTER_SELECTION, mFilterBaseDatas);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_ONE, mAccessibleTenantLocationDataModelForObserveAgent);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_TWO, observationSurveyCategoryModel);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_THREE, observationDashboardStatusModelArrayList);
                mFilterListFragment.setArguments(mBundle);
                mHomeActivity.replace(mFilterListFragment, mContext.getResources().getString(R.string.tag_filter_list_for_observe_agent));
            }
        });
        setLocationforSubTitle();
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            /*case R.id.img_filter:
                ObservationDynamicFilterFragment mFilterListFragment = new ObservationDynamicFilterFragment();
                Bundle mBundle = new Bundle();
                //mBundle.putParcelableArrayList(FilterListFragment.KEY_FILTER_SELECTION, mFilterBaseDatas);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_ONE, mAccessibleTenantLocationDataModelForObserveAgent);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_TWO, observationSurveyCategoryModel);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_THREE, observationDashboardStatusModelArrayList);
                mFilterListFragment.setArguments(mBundle);
                mHomeActivity.replace(mFilterListFragment, mContext.getResources().getString(R.string.tag_filter_list_for_observe_agent));
                break;*/

            case R.id.relative_date_compare:
                DateSelectionFragmentForObsCC fragment = new DateSelectionFragmentForObsCC();
                mHomeActivity.replace(fragment, mContext.getResources().getString(R.string.tag_fragment_dashboard_date_filter_obs_coaching));
                break;
        }
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        if(mCall != null)
            mCall.cancel();
    }

}
