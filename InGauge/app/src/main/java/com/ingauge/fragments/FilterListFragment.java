package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.FilterListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.OnFilterDataSetListener;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.pojo.RegionByTenantRegionTypeModel;
import com.ingauge.pojo.RegionTypeDataModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by desainid on 5/22/2017.
 */

public class FilterListFragment extends BaseFragment implements RecyclerViewClickListener, OnFilterDataSetListener, View.OnClickListener {
    public static final String KEY_FILTER_SELECTION = "filter_seclection";
    public static final String KEY_FILTER_DETAILS = "filter_details";
    public static final String KEY_FILTER_DETAILS_ONE = "filter_details_one";
    public static final String KEY_FILTER_DETAILS_TWO = "filter_details_two";
    public static final String KEY_FILTER_DETAILS_THREE = "filter_details_three";
    public static final String KEY_FILTER_DETAILS_FOUR = "filter_details_four";
    public static final String KEY_FILTER_DETAILS_FIVE = "filter_details_five";
    public static final String KEY_FILTER_DETAILS_SIX = "filter_details_six";
    public static final String KEY_SELECTION_POSITION = "seclected_position";
    Context mContext;
    RecyclerView rvFilterList;
    FilterListAdapter listadapter;
    ArrayList<FilterBaseData> mFilterBaseDatas;
    private HomeActivity mHomeActivity;
    APIInterface apiInterface;
    private RelativeLayout rlProgress;
    private RegionTypeDataModel mRegionTypeDataModel;
    private RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel;
    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel;
    private DashboardLocationGroupListModel mDashboardLocationGroupListModel;
    private ProductListFromLocationGroupModel mProductListFromLocationGroupModel;
    private DashboardUserListModel mDashboardUserListModel;
    private Button mButtonApply;
    private Button mButtonClear;
    Call mCall;
    private RelativeLayout rlHorProgress;
    public boolean isLoaded = false;
    private boolean isNeedUpdateOnApply = false;
    View rootView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mFilterBaseDatas = new ArrayList<>();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_fliter_list, container, false);
        initViews(rootView);
        if (InGaugeSession.read(mContext.getResources().getString(R.string.key_is_filter_loaded), false)) {
            if (isLoaded) {
                setFilterAdapter(mFilterBaseDatas);
            } else {
                clickOnCleartoUpdateFilterObj();
            }

        } else {
            if (!(mFilterBaseDatas != null && mFilterBaseDatas.size() > 0)) {
                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_filter_loaded), true);
                InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), true);
                /*mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_ONE,mRegionTypeDataModel);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_TWO,mRegionByTenantRegionTypeModel);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_THREE,mAccessibleTenantLocationDataModel);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_FOUR,mDashboardLocationGroupListModel);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_FIVE,mProductListFromLocationGroupModel);
                mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_SIX,mDashboardUserListModel);*/
                //getRegionTypes(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1));

                if (getArguments() != null) {
                    /*mRegionTypeDataModel = (RegionTypeDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_ONE);
                    mRegionByTenantRegionTypeModel = (RegionByTenantRegionTypeModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_TWO);
                    mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_THREE);
                    mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_FOUR);
                    mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_FIVE);
                    mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_SIX);*/
                    bindFilterObject();
                }
            } else {
                setFilterAdapter(mFilterBaseDatas);

            }
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Logger.Error("<Call>>");
    }

    private void initViews(View rootView) {
        rlProgress = (RelativeLayout) rootView.findViewById(R.id.custom_progress_rl_main);
        rvFilterList = (RecyclerView) rootView.findViewById(R.id.fragment_filter_list_recycleview);
        mButtonApply = (Button) rootView.findViewById(R.id.btn_apply_filter);
        mButtonApply.setOnClickListener(this);
        mButtonClear = (Button) rootView.findViewById(R.id.btn_clear_filter);
        mButtonClear.setOnClickListener(this);
        rlHorProgress = (RelativeLayout) rootView.findViewById(R.id.rl_hor_progress);
        mButtonApply.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_apply)));
        mButtonClear.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_clear)));
    }

    private void setFilterAdapter(ArrayList<FilterBaseData> mFilterBaseDataArrayList) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        rvFilterList.setLayoutManager(mLayoutManager);
        rvFilterList.setItemAnimator(new DefaultItemAnimator());
        rlProgress.setVisibility(View.GONE);
        rvFilterList.setVisibility(View.VISIBLE);
        listadapter = new FilterListAdapter(mHomeActivity, mFilterBaseDataArrayList, this, false);
        rvFilterList.setAdapter(listadapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_title)));
        mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.filter));
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterposition,boolean isLeftFilter) {
        sendListData(position, filterposition);
    }

    public void sendListData(int position, int filterposition) {
        Bundle mBundle = new Bundle();
        switch (position) {
            case 0:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mRegionTypeDataModel);
                break;
            case 1:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mRegionByTenantRegionTypeModel);
                break;
            case 2:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mAccessibleTenantLocationDataModel);
                break;
            case 3:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mDashboardLocationGroupListModel);
                break;
            case 4:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mProductListFromLocationGroupModel);
                break;
            case 5:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mDashboardUserListModel);
                break;
        }

        FilterBaseData mFilterBaseData = mFilterBaseDatas.get(filterposition);
        mBundle.putParcelable(KEY_FILTER_SELECTION, mFilterBaseData);
        mBundle.putInt(KEY_SELECTION_POSITION, position);
        FilterDetailListFragment mFilterDetailListFragment = new FilterDetailListFragment();
        mFilterDetailListFragment.setArguments(mBundle);
        mHomeActivity.replace(mFilterDetailListFragment, mContext.getResources().getString(R.string.tag_filter_detail_list));
    }

    @Override
    public void onFilterDetailsClicked(FilterBaseData mFilterBaseData, int clickedPosition, int filterposition) {
        Logger.Error("< Call Listener Method >>>>");
        if (mFilterBaseData != null)
            updateFilterData(mFilterBaseData, clickedPosition);

    }

    public void updateFilterData(FilterBaseData mFilterBaseData, int clickedPosition) {

        isLoaded = true;
        InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), true);
        mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
        int index = mFilterBaseData.filterIndex + 1;

        switch (index) {
            case 0:
                mHomeActivity.startProgress(mHomeActivity);
                getRegionTypesForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), clickedPosition, InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1));
                break;
            case 1:
                mHomeActivity.startProgress(mHomeActivity);
                getRegionByTenantRegionTypeByCountryForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1), String.valueOf(mHomeActivity.mRegionTypeDataModel.getData().get(clickedPosition).getId()), clickedPosition);
                break;
            case 2:
                mHomeActivity.startProgress(mHomeActivity);
                Logger.Error(" Filter  " + mFilterBaseData.id);
                //Logger.Error(" Selected ID For Region Model " + mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(clickedPosition).getId());
                getAccessibleTenantLocationForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1),
                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", mFilterBaseData.id, "Normal", clickedPosition);
                break;
            case 3:
                mHomeActivity.startProgress(mHomeActivity);
                getDashboardLocationGroupListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), mFilterBaseData.id, String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), clickedPosition);
                break;
            case 4:
                mHomeActivity.startProgress(mHomeActivity);
                getProductListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), mFilterBaseData.id, clickedPosition, mFilterBaseDatas.get(2).id);
                break;
            case 5:
                mHomeActivity.startProgress(mHomeActivity);
                getDashboardUserListForFilter(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), mFilterBaseDatas.get(2).id, "2016-08-01", "2016-08-31", mFilterBaseDatas.get(3).id, clickedPosition);
                break;
            case 6:
                break;
            case 7:
                InGaugeSession.write(mContext.getResources().getString(R.string.key_matric_data_type), mFilterBaseData.name);
                setFilterAdapter(mFilterBaseDatas);
                break;
        }
    }


    /**
     * @param authToken
     */
    void getRegionTypesForFilter(final String authToken, final int clickedPosition, final int tenantId) {

        mCall = apiInterface.getRegionTypes(tenantId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mRegionTypeDataModel = (RegionTypeDataModel) response.body();
                    /*Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mRegionTypeDataModel);
                    InGaugeSession.write(mContext.getString(R.string.key_region_type_obj), json);*/
                    //Here first to get the first index of data
                    /*int selectPos = -1;
                    if (isFirst)
                        selectPos = 0;
                    else {
                        selectPos = position;
                    }*/
                    //if (isUpdate) {
                    if (mRegionTypeDataModel != null) {
                        if (mRegionTypeDataModel.getData() != null && mRegionTypeDataModel.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mRegionTypeDataModel.getData().get(0).getId());
                            mFilterBaseData.name = mRegionTypeDataModel.getData().get(0).getName();
                            mFilterBaseData.filterIndex = 0;
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region_type));

                            mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
//                    mLeftFilterBaseDatas.add(mFilterBaseData);
                            //}
                            //getRegionByTenantRegionTypeByCountry(authToken, String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_id), 0)), String.valueOf(mRegionTypeDataModel.getData().get(0).getId()));
                            //  InGaugeSession.write(getString(R.string.key_selected_regiontype), (mRegionTypeDataModel.getData().get(0).getId()));
                            getRegionByTenantRegionTypeByCountryForFilter(authToken, tenantId, String.valueOf(mRegionTypeDataModel.getData().get(0).getId()), clickedPosition);
                        } else {
                            VoiUpdateFilterData(0);
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);
                            VoiUpdateFilterData(4);
                            VoiUpdateFilterData(5);
                            mHomeActivity.endProgress();
                            rlProgress.setVisibility(View.GONE);
                        }
                    } else {
                        VoiUpdateFilterData(0);
                        VoiUpdateFilterData(1);
                        VoiUpdateFilterData(2);
                        VoiUpdateFilterData(3);
                        VoiUpdateFilterData(4);
                        VoiUpdateFilterData(5);
                        rlProgress.setVisibility(View.GONE);
                        mHomeActivity.endProgress();

                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
    }

    private void VoiUpdateFilterData(int filterindex) {
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "";
        mFilterBaseData.name = "";
        mFilterBaseData.filterIndex = filterindex;
        switch (filterindex) {
            case 0:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.region_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region_type));
                mRegionTypeDataModel = null;
                /*Gson gson = new Gson();
                String json = gson.toJson(mHomeActivity.mRegionTypeDataModel);
                InGaugeSession.write(mContext.getString(R.string.key_region_type_obj), json);*/
                break;
            case 1:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.region);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region));
                mRegionByTenantRegionTypeModel = null;
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mRegionByTenantRegionTypeModel);
                InGaugeSession.write(mContext.getString(R.string.key_region_obj), json);*/
                break;
            case 2:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                mAccessibleTenantLocationDataModel = null;
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
                InGaugeSession.write(mContext.getString(R.string.key_location_obj), json);*/
                break;
            case 3:
                mDashboardLocationGroupListModel = null;
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
                InGaugeSession.write(mContext.getString(R.string.key_location_group_obj), json);*/
                break;
            case 4:
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                mProductListFromLocationGroupModel = null;
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModel);
                InGaugeSession.write(mContext.getString(R.string.key_product_obj), json);*/
                break;
            case 5:
                // mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                mDashboardUserListModel = null;
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                /*gson = new Gson();
                json = gson.toJson(mHomeActivity.mDashboardUserListModel);
                InGaugeSession.write(mContext.getString(R.string.key_user_obj), json);*/
                break;
        }
        if (listadapter != null) {
            if (filterindex >= mFilterBaseDatas.size()) {
                mFilterBaseDatas.add(mFilterBaseData);
            } else {
                mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
            }

            listadapter.notifyDataSetChanged();
        } else {
            if (listadapter != null) {
                mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                listadapter.notifyDataSetChanged();
                rlProgress.setVisibility(View.GONE);
                rvFilterList.setVisibility(View.VISIBLE);
            } else {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterList.setLayoutManager(mLayoutManager);
                rvFilterList.setItemAnimator(new DefaultItemAnimator());
                mFilterBaseDatas.add(mFilterBaseData.filterIndex, mFilterBaseData);
                rvFilterList.setAdapter(listadapter);
                rlProgress.setVisibility(View.GONE);
                rvFilterList.setVisibility(View.VISIBLE);
            }
        }

    }


    /**
     * @param authToken
     * @param tenantId
     * @param regionTypeId
     */
    void getRegionByTenantRegionTypeByCountryForFilter(final String authToken, final int tenantId, String regionTypeId, final int clickedposition) {

        try {
            mCall = apiInterface.getRegionByTenantRegionType(tenantId, Integer.parseInt(regionTypeId), false);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mRegionByTenantRegionTypeModel = (RegionByTenantRegionTypeModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mRegionByTenantRegionTypeModel);
                        InGaugeSession.write(mContext.getString(R.string.key_region_obj), json);*/
                        /*int selectPos = -1;
                        if (isFirst)
                            selectPos = 0;
                        if (isUpdate) {*/
                        if (mRegionByTenantRegionTypeModel != null) {
                            if (mRegionByTenantRegionTypeModel.getData() != null && mRegionByTenantRegionTypeModel.getData().size() > 0) {
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(mRegionByTenantRegionTypeModel.getData().get(0).getId());
                                mFilterBaseData.name = mRegionByTenantRegionTypeModel.getData().get(0).getName();
                                mFilterBaseData.filterIndex = 1;
                                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.region);
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region));
                                mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                                //  InGaugeSession.write(getString(R.string.key_selected_region), (mRegionByTenantRegionTypeModel.getData().get(0).getId()));
                                getAccessibleTenantLocationForFilter(authToken, tenantId,
                                        String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", String.valueOf(mRegionByTenantRegionTypeModel.getData().get(0).getId()), "Normal", clickedposition);
                            } else {
                                VoiUpdateFilterData(1);
                                VoiUpdateFilterData(2);
                                VoiUpdateFilterData(3);
                                VoiUpdateFilterData(4);
                                VoiUpdateFilterData(5);
                                rlProgress.setVisibility(View.GONE);
                                mHomeActivity.endProgress();
                            }
                        } else {
                            VoiUpdateFilterData(1);
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);
                            VoiUpdateFilterData(4);
                            VoiUpdateFilterData(5);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                        //mLeftFilterBaseDatas.add(mFilterBaseData);
                        //}


                        /*getAccessibleTenantLocation(authToken, String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_id), 0)),
                                String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), "name", "ASC", String.valueOf(mRegionByTenantRegionTypeModel.getData().get(0).getId()), "Normal");*/


                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Logger.Error("Exception " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param regionId
     * @param activeStatus
     */
    void getAccessibleTenantLocationForFilter(final String authToken, int tenantId, String userId, String orderBy, String sort, String regionId, String activeStatus, final int clickedposition) {
        try {
            mCall = apiInterface.getAccessibleTenantLocation(tenantId, userId, orderBy, sort, regionId, activeStatus,true);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
                        InGaugeSession.write(mContext.getString(R.string.key_location_obj), json);*/
                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mAccessibleTenantLocationDataModel.getData() != null && mAccessibleTenantLocationDataModel.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId());
                            mFilterBaseData.name = mAccessibleTenantLocationDataModel.getData().get(0).getName();
                            mFilterBaseData.filterIndex = 2;
//                            mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                            mFilterBaseData.filterMetricType = mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType();
                            mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            //mLeftFilterBaseDatas.add(mFilterBaseData);
                            //}

                            //String authToken,String tenantlocationId,String userId
                            //  InGaugeSession.write(getString(R.string.key_tenant_location_id), mAccessibleTenantLocationDataModel.getData().get(0).getId());
                            getDashboardLocationGroupListForFilter(authToken, String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId()), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), clickedposition);
                        } else {
                            VoiUpdateFilterData(2);
                            VoiUpdateFilterData(3);
                            VoiUpdateFilterData(4);
                            VoiUpdateFilterData(5);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupListForFilter(final String authToken, final String tenantlocationId, String userId, final int clickedPosition) {

        try {
            mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) response.body();
                        /*Gson gson = new Gson();
                        String json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
                        InGaugeSession.write(mContext.getString(R.string.key_location_group_obj), json);*/
                        //String authToken,String locationGroupId

                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mDashboardLocationGroupListModel != null && mDashboardLocationGroupListModel.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID());
                            mFilterBaseData.name = mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName();
                            mFilterBaseData.filterIndex = 3;
                            //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                            mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            //mLeftFilterBaseDatas.add(mFilterBaseData);
                            //}
                            //InGaugeSession.write(getString(R.string.key_selected_location_group), (mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                            getProductListForFilter(authToken, String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()), clickedPosition, tenantlocationId);
                        } else {
                            VoiUpdateFilterData(3);
                            VoiUpdateFilterData(4);
                            VoiUpdateFilterData(5);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }


                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductListForFilter(final String authToken, final String locationGroupId, final int clickedPosition, final String tenantLocationID) {

        try {
            mCall = apiInterface.getProductList(locationGroupId);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) response.body();
                        /* Gson gson = new Gson();
                        String json = gson.toJson(mProductListFromLocationGroupModel);
                        InGaugeSession.write(mContext.getString(R.string.key_product_obj), json);*/

                        //int selectPos = -1;
                        /*if (isFirst)
                            selectPos = 0;
                        else {
                            selectPos = position;
                        }*/
                        //if (isUpdate) {
                        if (mProductListFromLocationGroupModel.getData() != null && mProductListFromLocationGroupModel.getData().size() > 0) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mProductListFromLocationGroupModel.getData().get(0).getId());
                            mFilterBaseData.name = mProductListFromLocationGroupModel.getData().get(0).getName();
                            mFilterBaseData.filterIndex = 4;
//                            mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                            mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                            //  mLeftFilterBaseDatas.add(mFilterBaseData);
                            mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                            //   }
                            //String authToken, String tenantLocationI
                            // d, String startDate, String endDate, String locationGroupId
                            // InGaugeSession.write(getString(R.string.key_selected_location_group_product), (mProductListFromLocationGroupModel.getData().get(0).getId()));
                            getDashboardUserListForFilter(authToken, tenantLocationID, "2016-08-01", "2016-08-31", locationGroupId, clickedPosition);
                        } else {
                            VoiUpdateFilterData(4);
                            VoiUpdateFilterData(5);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param authToken
     * @param tenantLocationId
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserListForFilter(String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId, final int clickedposition) {
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);

        try {
            mCall = apiInterface.getDashboardUserList(TenantId, startDate, endDate,tenantLocationId, locationGroupId, true,false,false);
            mCall.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.body() != null) {
                        mDashboardUserListModel = (DashboardUserListModel) response.body();

                        if (mDashboardUserListModel != null) {

                            if (mDashboardUserListModel.getData() != null && mDashboardUserListModel.getData().size() > 0) {
                                FilterBaseData mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = String.valueOf(mDashboardUserListModel.getData().get(0).getId());
                                mFilterBaseData.name = mDashboardUserListModel.getData().get(0).getName();
                                mFilterBaseData.filterIndex = 5;
//                                mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                                mFilterBaseDatas.set(mFilterBaseData.filterIndex, mFilterBaseData);
                                rlProgress.setVisibility(View.GONE);
                                mHomeActivity.endProgress();


                                String hotelMetricTypeName = mFilterBaseDatas.get(2).filterMetricType;
                                Logger.Error("Hotel Metric Type Name" + hotelMetricTypeName);
                                mFilterBaseData = new FilterBaseData();
                                mFilterBaseData.id = "" + 000000;
                                if (!TextUtils.isEmpty(hotelMetricTypeName)) {
                                    if (hotelMetricTypeName.equalsIgnoreCase("Arrival")) {
                                        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);

                                    } else if (hotelMetricTypeName.equalsIgnoreCase("Departure")) {
                                        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(1);
                                    } else if (hotelMetricTypeName.equalsIgnoreCase("Daily")) {
                                        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(2);
                                    }
                                } else {
                                    mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);
                                }
                                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
                                mFilterBaseDatas.set(6, mFilterBaseData);


                                setFilterAdapter(mFilterBaseDatas);
                            } else {
                                VoiUpdateFilterData(5);
                                rlProgress.setVisibility(View.GONE);
                                mHomeActivity.endProgress();
                            }
                        } else {
                            VoiUpdateFilterData(5);
                            rlProgress.setVisibility(View.GONE);
                            mHomeActivity.endProgress();
                        }

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_apply_filter:
                mHomeActivity.setDashboardUpdate(true);
                clickOnApplytoUpdatePreference();

                break;
            case R.id.btn_clear_filter:
                //mHomeActivity.onBackPressed();
                try {
                    mFilterBaseDatas.clear();
                    getRegionTypes(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
                    /*mLeftFilterBaseDatas.clear();
                    listadapter.notifyDataSetChanged();
                    rlProgress.setVisibility(View.VISIBLE);
                    rvFilterList.setVisibility(View.GONE);
                    getRegionTypes(InGaugeSession.read(getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1));*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //getRegionTypesForFilter(InGaugeSession.read(getString(R.string.key_auth_token),""),-1);
                break;
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mCall != null) {
            mCall.cancel();
        }
    }

    public void clickOnApplytoUpdatePreference() {

        if (InGaugeSession.read(getString(R.string.key_is_need_update_on_apply), false)) {
            InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), false);
            mHomeActivity.mRegionTypeDataModel = null;
            mHomeActivity.mRegionTypeDataModel = mRegionTypeDataModel;
            Gson gson = new Gson();
            String json = gson.toJson(mHomeActivity.mRegionTypeDataModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_region_type_obj), json);
            mHomeActivity.mRegionByTenantRegionTypeModel = null;
            mHomeActivity.mRegionByTenantRegionTypeModel = mRegionByTenantRegionTypeModel;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mRegionByTenantRegionTypeModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_region_obj), json);


            mHomeActivity.mAccessibleTenantLocationDataModel = null;
            mHomeActivity.mAccessibleTenantLocationDataModel = mAccessibleTenantLocationDataModel;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_obj), json);


            mHomeActivity.mDashboardLocationGroupListModel = null;
            mHomeActivity.mDashboardLocationGroupListModel = mDashboardLocationGroupListModel;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_location_group_obj), json);

            mHomeActivity.mProductListFromLocationGroupModel = null;
            mHomeActivity.mProductListFromLocationGroupModel = mProductListFromLocationGroupModel;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_product_obj), json);


            mHomeActivity.mDashboardUserListModel = null;
            mHomeActivity.mDashboardUserListModel = mDashboardUserListModel;
            gson = new Gson();
            json = gson.toJson(mHomeActivity.mDashboardUserListModel);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_user_obj), json);
        }

        for (int i = 0; i < mFilterBaseDatas.size(); i++) {
            switch (i) {
                case 0:
                    FilterBaseData mFilterBaseData = mFilterBaseDatas.get(i);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), Integer.parseInt(mFilterBaseData.id));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype_name), mFilterBaseData.name);
                    break;
                case 1:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), Integer.parseInt(mFilterBaseData.id));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region_name), mFilterBaseData.name);
                    break;
                case 2:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), Integer.parseInt(mFilterBaseData.id));
                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mFilterBaseData.name);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mFilterBaseData.filterMetricType);
                    Logger.Error("Hotel Metric Type Name  in Filter Object" + mFilterBaseData.filterMetricType);
                    break;
                case 3:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), Integer.parseInt(mFilterBaseData.id));
                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mFilterBaseData.name);
                    break;
                case 4:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                    }
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);
                    break;
                case 5:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), 1);
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), Integer.parseInt(mFilterBaseData.id));
                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name), mFilterBaseData.name);
                    break;
                case 6:
                    mFilterBaseData = mFilterBaseDatas.get(i);
                    if (TextUtils.isEmpty(mFilterBaseData.id)) {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_id), "");
                    } else {
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_id), mFilterBaseData.id);
                    }
                    String hotelMetricTypeName = InGaugeSession.read(mContext.getResources().getString(R.string.key_matric_data_type), "");
                    Logger.Error("Hotel Metric Type Name" + hotelMetricTypeName);
                    if (!TextUtils.isEmpty(hotelMetricTypeName)) {
                        if (hotelMetricTypeName.equalsIgnoreCase("Arrival")) {
                            mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);

                        } else if (hotelMetricTypeName.equalsIgnoreCase("Departure")) {
                            mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(1);
                        } else if (hotelMetricTypeName.equalsIgnoreCase("Daily")) {
                            mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(2);
                        }
                    } else {
                        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);
                    }

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mFilterBaseData.name);
                    break;
            }
        }

        //geography_type, region, location, location_group, product, user, metric_type, role
        Bundle bundle = new Bundle();
        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
        bundle.putString("industry", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
        bundle.putString("geography_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_regiontype_name), ""));
        bundle.putString("region", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_region_name), ""));
        bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
        bundle.putString("location_group", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name), ""));
        bundle.putString("product", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name), ""));
        bundle.putString("user", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), ""));
        bundle.putString("metric_type", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), ""));
        InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_filter", bundle);
        mHomeActivity.onBackPressed();
    }

    public void clickOnCleartoUpdateFilterObj() {
        mRegionTypeDataModel = mHomeActivity.mRegionTypeDataModel;
        mRegionByTenantRegionTypeModel = mHomeActivity.mRegionByTenantRegionTypeModel;
        mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModel;
        mDashboardLocationGroupListModel = mHomeActivity.mDashboardLocationGroupListModel;
        mProductListFromLocationGroupModel = mHomeActivity.mProductListFromLocationGroupModel;
        mDashboardUserListModel = mHomeActivity.mDashboardUserListModel;
        mFilterBaseDatas.clear();
        for (int i = 0; i < 7; i++) {
            FilterBaseData mFilterBaseData = new FilterBaseData();
            if (i == 0) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_regiontype), 1));
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_regiontype_name), "");
                //mFilterBaseData.filterName =mContext.getResources().getString(R.string.region_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region_type));
            } else if (i == 1) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_region), 1));
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_region_name), "");
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.region);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region));
            } else if (i == 2) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id), 1));
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), "");
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
                mFilterBaseData.filterMetricType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), "");
                Logger.Error("Hotel Metric Type Name in Filter Object" + mFilterBaseData.filterMetricType);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
            } else if (i == 3) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group), 1));
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_name), "");
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
            } else if (i == 4) {
                mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product), 1));
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_location_group_product_name), "");
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
            } else if (i == 5) {
                try {
                    mFilterBaseData.id = "" + InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user), 1);
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");
                    //mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                } catch (Exception e) {
                    e.printStackTrace();
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_user_name), "");
                    //mFilterBaseData.filterName = mContext.getResources().getString(R.string.user);
                    mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                }
            } else if (i == 6) {
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_id), "");
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), "");
                //mFilterBaseData.filterName = mContext.getResources().getString(R.string.metric_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));

            }
            mFilterBaseData.filterIndex = i;
            mFilterBaseDatas.add(mFilterBaseData);
        }

        setFilterAdapter(mFilterBaseDatas);
    }

    public void bindFilterObject() {

        mRegionTypeDataModel = mHomeActivity.mRegionTypeDataModel;
        mRegionByTenantRegionTypeModel = mHomeActivity.mRegionByTenantRegionTypeModel;
        mAccessibleTenantLocationDataModel = mHomeActivity.mAccessibleTenantLocationDataModel;
        mDashboardLocationGroupListModel = mHomeActivity.mDashboardLocationGroupListModel;
        mProductListFromLocationGroupModel = mHomeActivity.mProductListFromLocationGroupModel;
        mDashboardUserListModel = mHomeActivity.mDashboardUserListModel;
        //mLeftFilterBaseDatas.clear();
        /*

        DashboardLocationGroupListModel mDashboardLocationGroupListModel;
        ProductListFromLocationGroupModel mProductListFromLocationGroupModel;
        DashboardUserListModel mDashboardUserListModel;*/
        for (int i = 0; i < 7; i++) {
            FilterBaseData mFilterBaseData = new FilterBaseData();

            if (i == 0) {
                if (mRegionTypeDataModel != null && (mRegionTypeDataModel.getData() != null && mRegionTypeDataModel.getData().size() > 0)) {
                    mFilterBaseData.id = String.valueOf(mRegionTypeDataModel.getData().get(0).getId());
                    mFilterBaseData.name = mRegionTypeDataModel.getData().get(0).getName();
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), mRegionTypeDataModel.getData().get(0).getId());
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype_name), mRegionTypeDataModel.getData().get(0).getName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }
                //mFilterBaseData.filterName = getString(R.string.region_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region_type));

            } else if (i == 1) {
                if (mRegionByTenantRegionTypeModel != null && (mRegionByTenantRegionTypeModel.getData() != null && mRegionByTenantRegionTypeModel.getData().size() > 0)) {
                    mFilterBaseData.id = String.valueOf(mRegionByTenantRegionTypeModel.getData().get(0).getId());
                    mFilterBaseData.name = mRegionByTenantRegionTypeModel.getData().get(0).getName();
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), (mRegionByTenantRegionTypeModel.getData().get(0).getId()));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region_name), mRegionByTenantRegionTypeModel.getData().get(0).getName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }
                //mFilterBaseData.filterName = getString(R.string.region);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region));

            } else if (i == 2) {
                if (mAccessibleTenantLocationDataModel != null && (mAccessibleTenantLocationDataModel.getData() != null && mAccessibleTenantLocationDataModel.getData().size() > 0)) {
                    mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId());
                    mFilterBaseData.name = mAccessibleTenantLocationDataModel.getData().get(0).getName();
                    mFilterBaseData.filterMetricType = mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType();
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), mAccessibleTenantLocationDataModel.getData().get(0).getId());
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mAccessibleTenantLocationDataModel.getData().get(0).getName());
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType());
                    Logger.Error("Hotel Metric Type Name in Accessible Tenant Location Object" + mAccessibleTenantLocationDataModel.getData().get(0).getName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }

                //mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
            } else if (i == 3) {
                if (mDashboardLocationGroupListModel != null && (mDashboardLocationGroupListModel.getData() != null && mDashboardLocationGroupListModel.getData().size() > 0)) {

                    mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID());
                    mFilterBaseData.name = mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName();
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), (mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }
                //mFilterBaseData.filterName = getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
            } else if (i == 4) {

                if (mProductListFromLocationGroupModel != null && (mProductListFromLocationGroupModel != null && mProductListFromLocationGroupModel.getData().size() > 0)) {
                    mFilterBaseData.id = String.valueOf(mProductListFromLocationGroupModel.getData().get(0).getId());
                    mFilterBaseData.name = mProductListFromLocationGroupModel.getData().get(0).getName();
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), (mProductListFromLocationGroupModel.getData().get(0).getId()));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mProductListFromLocationGroupModel.getData().get(0).getName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }


                //mFilterBaseData.filterName = getString(R.string.product);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
            } else if (i == 5) {


                if (mDashboardUserListModel != null && (mDashboardUserListModel != null && mDashboardUserListModel.getData().size() > 0)) {
                    mFilterBaseData.id = String.valueOf(mDashboardUserListModel.getData().get(0).getId());
                    mFilterBaseData.name = mDashboardUserListModel.getData().get(0).getName();
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), mDashboardUserListModel.getData().get(0).getId());
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name), mDashboardUserListModel.getData().get(0).getName());
                } else {
                    mFilterBaseData.id = "";
                    mFilterBaseData.name = "";
                }


                //mFilterBaseData.filterName = getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
            } else if (i == 6) {
                String hotelMetricTypeName = InGaugeSession.read(mContext.getResources().getString(R.string.key_matric_data_type), "");
                Logger.Error("Hotel Metric Type Name " + hotelMetricTypeName);
                if (!TextUtils.isEmpty(hotelMetricTypeName)) {
                    if (hotelMetricTypeName.equalsIgnoreCase("Arrival")) {
                        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);

                    } else if (hotelMetricTypeName.equalsIgnoreCase("Departure")) {
                        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(1);
                    } else if (hotelMetricTypeName.equalsIgnoreCase("Daily")) {
                        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(2);
                    }
                } else {
                    mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);
                }
                mFilterBaseData.id = "" + 000000;
                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_id), mFilterBaseData.id);
                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_metric_type_name), mFilterBaseData.name);
                //mFilterBaseData.filterName = getString(R.string.metric_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
            }
            mFilterBaseData.filterIndex = i;
            mFilterBaseDatas.add(mFilterBaseData);
        }

        setFilterAdapter(mFilterBaseDatas);
    }


    /**
     * @param authToken
     */
    void getRegionTypes(final String authToken) {
        isNeedUpdateOnApply = true;
        mHomeActivity.startProgress(mContext);
        mCall = apiInterface.getRegionTypes(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0));
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                // mHomeActivity.endProgress();
                if (response.body() != null) {

                    mRegionTypeDataModel = (RegionTypeDataModel) response.body();
                    //Here first to get the first index of data
                    if ((mRegionTypeDataModel != null) && (mRegionTypeDataModel.getData() != null && mRegionTypeDataModel.getData().size() > 0)) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mRegionTypeDataModel.getData().get(0).getId());
                        mFilterBaseData.name = mRegionTypeDataModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 0;
//                        mFilterBaseData.filterName = mContext.getResources().getString(R.string.region_type);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region_type));
                        mFilterBaseDatas.add(mFilterBaseData);
                        //InGaugeSession.write(getString(R.string.key_selected_regiontype), mHomeActivity.mRegionTypeDataModel.getData().get(0).getId());

                        //getRegionByTenantRegionTypeByCountry(authToken, String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_id), 0)), String.valueOf(mRegionTypeDataModel.getData().get(0).getId()));
                        getRegionByTenantRegionTypeByCountry(authToken, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0)), String.valueOf(mRegionTypeDataModel.getData().get(0).getId()));

                    } else {
                        mHomeActivity.endProgress();
                        VoiUpdateFilterData(0);
                        VoiUpdateFilterData(1);
                        VoiUpdateFilterData(2);
                        VoiUpdateFilterData(3);
                        VoiUpdateFilterData(4);
                        VoiUpdateFilterData(5);
                    }
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.endProgress();
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mHomeActivity.endProgress();
                VoiUpdateFilterData(0);
                VoiUpdateFilterData(1);
                VoiUpdateFilterData(2);
                VoiUpdateFilterData(3);
                VoiUpdateFilterData(4);
                VoiUpdateFilterData(5);

            }
        });

    }

    /**
     * @param authToken
     * @param tenantId
     * @param regionTypeId
     */
    void getRegionByTenantRegionTypeByCountry(final String authToken, final String tenantId, String regionTypeId) {

        mCall = apiInterface.getRegionByTenantRegionType(Integer.parseInt(tenantId), Integer.parseInt(regionTypeId), false);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mRegionByTenantRegionTypeModel = (RegionByTenantRegionTypeModel) response.body();
                    if ((mRegionByTenantRegionTypeModel != null) && (mRegionByTenantRegionTypeModel.getData() != null && mRegionByTenantRegionTypeModel.getData().size() > 0)) {

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mRegionByTenantRegionTypeModel.getData().get(0).getId());
                        mFilterBaseData.name = mRegionByTenantRegionTypeModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 1;
                        //mFilterBaseData.filterName = mContext.getResources().getString(R.string.region);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region));
                        mFilterBaseDatas.add(mFilterBaseData);
                    /*getAccessibleTenantLocation(authToken, String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_id), 0)),
                            String.valueOf(InGaugeSession.read(getString(R.string.key_user_id), 0)), "name", "ASC", String.valueOf(mRegionByTenantRegionTypeModel.getData().get(0).getId()), "Normal");*/
                        //InGaugeSession.write(getString(R.string.key_selected_region), (mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(0).getId()));

                        getAccessibleTenantLocation(authToken, Integer.parseInt(tenantId),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", String.valueOf(mRegionByTenantRegionTypeModel.getData().get(0).getId()), "Normal");
                    } else {

                        VoiUpdateFilterData(1);
                        VoiUpdateFilterData(2);
                        VoiUpdateFilterData(3);
                        VoiUpdateFilterData(4);
                        VoiUpdateFilterData(5);
                        mHomeActivity.endProgress();

                    }
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    VoiUpdateFilterData(1);
                    VoiUpdateFilterData(2);
                    VoiUpdateFilterData(3);
                    VoiUpdateFilterData(4);
                    VoiUpdateFilterData(5);
                    mHomeActivity.endProgress();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                VoiUpdateFilterData(1);
                VoiUpdateFilterData(2);
                VoiUpdateFilterData(3);
                VoiUpdateFilterData(4);
                VoiUpdateFilterData(5);
                mHomeActivity.endProgress();

            }
        });
    }

    /**
     * @param authToken
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param regionId
     * @param activeStatus
     */
    void getAccessibleTenantLocation(final String authToken, int tenantId, String userId, String orderBy, String sort, String regionId, String activeStatus) {

        mCall = apiInterface.getAccessibleTenantLocation(tenantId, userId, orderBy, sort, regionId, activeStatus,true);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {


                    mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) response.body();

                    if ((mAccessibleTenantLocationDataModel != null && (mAccessibleTenantLocationDataModel.getData() != null && mAccessibleTenantLocationDataModel.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId());
                        mFilterBaseData.name = mAccessibleTenantLocationDataModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 2;
                        //mFilterBaseData.filterName = mContext.getResources().getString(R.string.location);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));

                        mFilterBaseDatas.add(mFilterBaseData);

                        //String authToken,String tenantlocationId,String userId

                        getDashboardLocationGroupList(authToken, String.valueOf(mAccessibleTenantLocationDataModel.getData().get(0).getId()), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                    } else {
                        VoiUpdateFilterData(2);
                        VoiUpdateFilterData(3);
                        VoiUpdateFilterData(4);
                        VoiUpdateFilterData(5);
                        mHomeActivity.endProgress();

                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    VoiUpdateFilterData(2);
                    VoiUpdateFilterData(3);
                    VoiUpdateFilterData(4);
                    VoiUpdateFilterData(5);
                    mHomeActivity.endProgress();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                VoiUpdateFilterData(2);
                VoiUpdateFilterData(3);
                VoiUpdateFilterData(4);
                VoiUpdateFilterData(5);
                mHomeActivity.endProgress();

            }
        });
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupList(final String authToken, final String tenantlocationId, String userId) {

        mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {


                    mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) response.body();

                    //String authToken,String locationGroupId
                    if ((mDashboardLocationGroupListModel != null && (mDashboardLocationGroupListModel.getData() != null && mDashboardLocationGroupListModel.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID());
                        mFilterBaseData.name = mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName();
                        mFilterBaseData.filterIndex = 3;
                        //mFilterBaseData.filterName =mContext.getResources().getString(R.string.location_group);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));

                        mFilterBaseDatas.add(mFilterBaseData);

                        getProductList(authToken, String.valueOf(mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()), tenantlocationId);
                    } else {

                        VoiUpdateFilterData(3);
                        VoiUpdateFilterData(4);
                        VoiUpdateFilterData(5);
                        mHomeActivity.endProgress();

                    }
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    VoiUpdateFilterData(3);
                    VoiUpdateFilterData(4);
                    VoiUpdateFilterData(5);
                    mHomeActivity.endProgress();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                VoiUpdateFilterData(3);
                VoiUpdateFilterData(4);
                VoiUpdateFilterData(5);
                mHomeActivity.endProgress();

            }
        });
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductList(final String authToken, final String locationGroupId, final String tenantLocationId) {

        mCall = apiInterface.getProductList(locationGroupId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {


                    mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) response.body();

                    if ((mProductListFromLocationGroupModel != null && (mProductListFromLocationGroupModel.getData() != null && mProductListFromLocationGroupModel.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mProductListFromLocationGroupModel.getData().get(0).getId());
                        mFilterBaseData.name = mProductListFromLocationGroupModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 4;
                        //mFilterBaseData.filterName = mContext.getResources().getString(R.string.product);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));

                        mFilterBaseDatas.add(mFilterBaseData);

                        //String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId
                        getDashboardUserList(authToken, tenantLocationId, InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), locationGroupId);
                    } else {

                        VoiUpdateFilterData(4);
                        VoiUpdateFilterData(5);
                        mHomeActivity.endProgress();

                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    VoiUpdateFilterData(4);
                    VoiUpdateFilterData(5);
                    mHomeActivity.endProgress();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                VoiUpdateFilterData(4);
                VoiUpdateFilterData(5);
                mHomeActivity.endProgress();

            }
        });
    }


    /**
     * @param authToken
     * @param tenantLocationId
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserList(String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId) {
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);

        mCall = apiInterface.getDashboardUserList(TenantId, startDate, endDate,tenantLocationId, locationGroupId, true,false,false);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mHomeActivity.endProgress();

                if (response.body() != null) {


                    mDashboardUserListModel = (DashboardUserListModel) response.body();

                    if ((mDashboardUserListModel != null && (mDashboardUserListModel.getData() != null && mDashboardUserListModel.getData().size() > 0))) {

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mDashboardUserListModel.getData().get(0).getId());
                        mFilterBaseData.name = mDashboardUserListModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 5;
                        //mFilterBaseData.filterName = getString(R.string.user);
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));

                        mFilterBaseDatas.add(mFilterBaseData);
                        setMetricTypeList();

                    } else {

                        VoiUpdateFilterData(5);
                        mHomeActivity.endProgress();

                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    VoiUpdateFilterData(5);
                    mHomeActivity.endProgress();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                VoiUpdateFilterData(5);
                mHomeActivity.endProgress();

            }
        });
    }


    private void setMetricTypeList() {
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "" + 000000;
        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);
        mFilterBaseData.filterIndex = 6;
        //mFilterBaseData.filterName = getString(R.string.metric_type);
        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
        //InGaugeSession.write(mContext.getResources().getString(R.string.key_matric_data_type), mHomeActivity.mStringListMetricType.get(0));
        mFilterBaseDatas.add(mFilterBaseData);
        listadapter.notifyDataSetChanged();
        //setFilterAdapter(mLeftFilterBaseDatas);
    }


}
