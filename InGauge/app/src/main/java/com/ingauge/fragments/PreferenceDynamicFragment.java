package com.ingauge.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.Listadapter;
import com.ingauge.adapters.PreferenceListadapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.DefaultFilter;
import com.ingauge.pojo.DynamicData;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.FilterMetricTypeModel;
import com.ingauge.pojo.IndustryDataModel;
import com.ingauge.pojo.TenantDataModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DatabaseHandler;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 27-Oct-17.
 */

public class PreferenceDynamicFragment extends BaseFragment implements View.OnClickListener{


    View rootView;
    private ListView lvLeft;
    private ListView lvRight;
    private SearchView mSearchView;


    Context mContext;
    List<String> leftvalues;
    List<String> rightvalues;
    ArrayList<String> tenantValues;
    List<String> descriptionLeft;
    PreferenceListadapter mPreferenceAdapter;
    Listadapter mListadapter;
    private HomeActivity mHomeActivity;
    APIInterface apiInterface;
    APIInterface apiInterfaceToTenantcall;


    List<IndustryDataModel.Datum> industryList;
    List<TenantDataModel.Data> tenantList;
    private TextView tvNoData;

    JSONObject mDataJson = null;
    List<DynamicData> mCountryList;
    private String selectedName = "";
    public boolean isFromTenantDetail = false;

    String selectedIndustry = "";
    String selectedTenant = "";

    private TextView tvApply;
    private TextView tvCancel;


    /// Variables for Tenant Selection on Right list Click
    String selectedTenantName = "";
    int selectedTenantId = 0;
    String selectedtenantRoleName = "";
    int selectedUserRoleId = 0;
    String selectedUserRoleName = "";
    String selectedLoginTenantUserRole = "";
    String selectedIndustryName = "";
    int selectedIndustryId = 0;

    Map<Integer, List<DynamicData>> integerListHashMap = new LinkedHashMap<>();

    JSONObject selectedmobilekeyObject = null;
    List<String> selectedStringListMetricType = new ArrayList<>();
    List<FilterMetricTypeModel> selectedFilterMetricTypeModelList = new ArrayList<>();
    List<String> selectedStringKeyListMetricType = new ArrayList<>();

    String selectedRolesPersmission = "";

    boolean isFromRightClick = false;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        mCountryList = new ArrayList<>();
        if(arg != null){
            descriptionLeft.add(arg.getString("selectedValue"));
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);
        apiInterfaceToTenantcall = APIClient.getClientWithoutHeaders().create(APIInterface.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_dynamic_preferences, container, false);
        tvApply = (TextView) rootView.findViewById(R.id.fragment_dynamic_dashboard_preference_tv_apply);
        tvApply.setOnClickListener(this);
        tvCancel = (TextView) rootView.findViewById(R.id.fragment_dynamic_dashboard_preference_tv_canel);
        tvCancel.setOnClickListener(this);
        lvLeft = (ListView) rootView.findViewById(R.id.fragment_dynamic_preferences_lv_left);
        lvRight = (ListView) rootView.findViewById(R.id.fragment_dynamic_preferences_lv_right);
        mSearchView = (SearchView) rootView.findViewById(R.id.fragment_dynamic_dashboard_preferences_search_view);
        tvNoData = (TextView) rootView.findViewById(R.id.fragment_dynamic_preferences_tv_no_data);
        leftvalues = new ArrayList<>();
        rightvalues = new ArrayList<>();
        tenantValues = new ArrayList<>();
        descriptionLeft = new ArrayList<>();
        selectedIndustry = InGaugeSession.read(getString(R.string.key_selected_industry), "");
        selectedTenant = InGaugeSession.read(getString(R.string.key_selected_tenant), "");
        //String valueIndustry =mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_industry));
        // String valueTenant  =mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_tenant));
        String valueIndustry = "Select Industry";
        String valueTenant = "Select Tenant";
        leftvalues.add(valueIndustry);
        leftvalues.add(valueTenant);
        descriptionLeft.add(selectedIndustry);
        descriptionLeft.add(selectedTenant);
        /*description.add("selected industry");
        description.add("selected tenant");*/
        mPreferenceAdapter = new PreferenceListadapter(getActivity(), leftvalues, descriptionLeft, selectedTenant);
        lvLeft.setAdapter(mPreferenceAdapter);
        InGaugeSession.write(getString(R.string.fragment_preference_detail), "Tenant");
        selectedName = selectedTenant;
        getRightList(isFromRightClick);
        lvLeft.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l){
                mPreferenceAdapter.lastCheckedPosition = position;
                mPreferenceAdapter.notifyDataSetChanged();
                if(position == 0){
                    InGaugeSession.write(getString(R.string.fragment_preference_detail), "Industry");
                    selectedName = selectedIndustry;
                    getRightList(false);

                } else if(position == 1){
                    InGaugeSession.write(getString(R.string.fragment_preference_detail), "Tenant");
                    selectedName = selectedTenant;
                    getRightList(false);

                }
            }
        });
        lvRight.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l){
//Clear query
                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call_goal), true);
                isFromTenantDetail = true;
                mHomeActivity.setDashboardUpdate(true);
                if(mSearchView != null){

                    mSearchView.setQuery("", false);
                    //Collapse the action view
                    mSearchView.onActionViewCollapsed();

                }
                final InputMethodManager imm = (InputMethodManager) mHomeActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                try{
                    Bundle args = new Bundle();
                    String item = mListadapter.getItem(position);
                    //  int positionFiltered = rightvalues.indexOf(item.toString());
//                    position = positionFiltered;
                    mListadapter.lastCheckedPosition = position;
                    mListadapter.notifyDataSetChanged();
                    if(InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Industry")){


                        selectedIndustryName = industryList.get(position).getDescription();
                        selectedIndustryId = industryList.get(position).getId();
                        isFromRightClick = true;
                        GetTanent(InGaugeSession.read(getString(R.string.key_auth_token), ""), UiUtils.getUserId(mContext), industryList.get(position).getId(), isFromRightClick);
                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        bundle.putString("industry_name", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
                        InGaugeApp.getFirebaseAnalytics().logEvent("industry_change", bundle);
                        selectedIndustry = item;
                        descriptionLeft.set(0, item);
                        mPreferenceAdapter.notifyDataSetChanged();
                    } else if(InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Tenant")){


                        //InGaugeSession.write(getString(R.string.key_selected_tenant), tenantList.get(position).getTenantName());
                        //InGaugeSession.write(getString(R.string.key_selected_tenant_id), tenantList.get(position).getTenantId());
                        //InGaugeSession.write(getString(R.string.key_tenant_role_name), "" + tenantList.get(position).getRoleName());
                        //InGaugeSession.write(getString(R.string.key_user_role_id), tenantList.get(position).getRoleId());
                        //InGaugeSession.write(getString(R.string.key_user_role_name), "" + tenantList.get(position).getRoleName());
                        selectedTenantName = tenantList.get(position).getTenantName();
                        selectedTenantId = tenantList.get(position).getTenantId();
                        selectedtenantRoleName = "" + tenantList.get(position).getRoleName();
                        selectedUserRoleId = tenantList.get(position).getRoleId();
                        selectedUserRoleName = "" + tenantList.get(position).getRoleName();

                        //String tenantRoleName = getRoleName(tenantList.get(position).getRoleRolePermissions());
                        //InGaugeSession.write(getString(R.string.key_login_tenant_user_role), tenantRoleName);
                        selectedLoginTenantUserRole = getRoleName(tenantList.get(position).getRoleRolePermissions());

                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        bundle.putString("tenant_name", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant), ""));
                        InGaugeApp.getFirebaseAnalytics().logEvent("tenant_change", bundle);
                        descriptionLeft.set(1, item);
                        mPreferenceAdapter.notifyDataSetChanged();
                        mHomeActivity.startProgress(mContext);


                        getRolesAndPermission("application/json",
                                InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), null), true,
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 0)),
                                String.valueOf(selectedTenantId),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -1)),
                                true, false);
                        //getDefaultFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), tenantList.get(position).getTenantId());
                    }

                } catch(Resources.NotFoundException e){
                    e.printStackTrace();
                } catch(Exception e){
                    e.printStackTrace();
                }

            }
        });
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String query){
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText){
                mListadapter.getFilter().filter(newText);
                return false;
            }
        });
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_title)));
        mHomeActivity.tvTitle.setText("Preferences");

        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
    }

    public void getRightList(boolean isFromRightClick){
        if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.fragment_preference_detail), "").equals("Industry")){
            // GetIndustryAPI();
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), true);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call_goal), true);
            GetIndustry(InGaugeSession.read(getString(R.string.key_auth_token), ""));
        } else if(InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Tenant")){
            //GetTenantAPI();
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), true);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call_goal), true);
            GetTanent(InGaugeSession.read(getString(R.string.key_auth_token), ""), UiUtils.getUserId(mContext), UiUtils.getIndustryId(mContext), isFromRightClick);
        }
    }

    void getDefaultFilter(final String authToken, final int TenantID){
        mHomeActivity.startProgress(mContext);
        Call mCall = apiInterface.getDefaultFilter(TenantID);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){

                    DefaultFilter defaultFilter = (DefaultFilter) response.body();

                    int RegionTypeId = defaultFilter.getData().getREGIONTYPE();
                    int RegionId = defaultFilter.getData().getREGION();
                    int TenantLocationId = defaultFilter.getData().getLOCATION();
                    int LocationGroupId = defaultFilter.getData().getLOCATIONGROUP();
                    int ProductId = defaultFilter.getData().getPRODUCT();
                    int UserID = defaultFilter.getData().getUSER();

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), RegionTypeId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), RegionId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), TenantLocationId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), LocationGroupId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), ProductId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), UserID);

                    // getDefaultFilterForGoal(TenantID);


                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                //endProgress();
            }
        });
    }

    /**
     * get Default Filter For Goal
     */
    void getDefaultFilterForGoal(final int TenantID){
        Call mCall = apiInterface.getDefaultFilterForGoal(TenantID, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){

                    DefaultFilter defaultFilter = (DefaultFilter) response.body();

                    //   int RegionTypeId = defaultFilter.getData().getREGIONTYPE();
                    // int RegionId = defaultFilter.getData().getREGION();
                    int TenantLocationId = defaultFilter.getData().getLOCATION();
                    int LocationGroupId = defaultFilter.getData().getLOCATIONGROUP();
                    int ProductId = defaultFilter.getData().getPRODUCT();
                    int UserID = defaultFilter.getData().getUSER();


                    /*InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), TenantLocationId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(LocationGroupId));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), ProductId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), UserID);*/

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_default_goal), TenantLocationId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_default_goal), String.valueOf(LocationGroupId));
                    //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_default_goal), ProductId);
                    //This -1 is put static Due to in Goal Setting Default Filter value in Product is Incremental revenue. As In Goal Settings Filter show default Incremental Revenue at first time
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_default_goal), -1);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_default_goal), UserID);


                    //getRolesAndPermission("application/json", InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), null), true, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 0)), String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_id), 0)), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -1)),false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                // endProgress();
            }
        });
    }

    private void getRolesAndPermission(String ContentType, String Authorization, boolean isMobile, final String IndustryId, String TenantId, final String UserId, final boolean isFromTenantListCall, final boolean isFromIndustry){
        Call mCall = apiInterface.getRolesAndPermission(TenantId, UserId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                JSONObject data = new JSONObject();
                if(response.body() != null){
                    Logger.Error("ROLES AND PERMISSION RESPONSE " + response.body());
                    try{
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        if(jsonObject.optString("status").equalsIgnoreCase("SUCCESS")){
                            data = jsonObject.optJSONObject("data");
                        }
                        selectedRolesPersmission = data.toString();
                        isFromTenantDetail = true;
                        if(isFromIndustry){
                            getMobileKeys(String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), -2)));
                        } else{
                            getDynamicFilterMap(
                                    selectedTenantId,
                                    UiUtils.getUserId(mContext),
                                    false,
                                    isFromTenantListCall);
                        }


                    } catch(JSONException e){
                        e.printStackTrace();
                        mHomeActivity.endProgress();
                    }

                } else{
                    mHomeActivity.endProgress();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
                Logger.Error("Exception " + t.getMessage());
            }
        });
    }

    void GetIndustry(String authToken){
        mHomeActivity.startProgress(mContext);
        Call mCall = apiInterface.getIndustry(UiUtils.getUserId(mContext));
        industryList = new ArrayList<>();
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                mHomeActivity.endProgress();
                if(response.body() != null){
                    IndustryDataModel getIndstrydata = (IndustryDataModel) response.body();

                    if(InGaugeSession.read(getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                        industryList = getIndstrydata.getData();
                    } else if(InGaugeSession.read(getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){

                        for(IndustryDataModel.Datum mDatum : getIndstrydata.getData()){
                            if(mDatum.getName().equalsIgnoreCase("Hotel")){
                                industryList.add(mDatum);
                                break;
                            }
                        }
                    }
                    Collections.sort(industryList, new Comparator<IndustryDataModel.Datum>(){
                        @Override
                        public int compare(IndustryDataModel.Datum datum, IndustryDataModel.Datum t1){
                            return datum.getDescription().compareTo(t1.getDescription());
                        }
                    });
                    if(rightvalues != null && rightvalues.size() > 0){
                        rightvalues.clear();
                    }
                    int count = 0;
                    for(IndustryDataModel.Datum datum : industryList){
                        if(count == 0){
                            if(!(selectedName.length() > 0))
                                selectedName = datum.getDescription();
                        }
                        count++;
                        rightvalues.add(datum.getDescription());
                    }
                    //Collections.sort(values);
                    mListadapter = new Listadapter(mHomeActivity, rightvalues, true, false, selectedName);
                    lvRight.setAdapter(mListadapter);
                    if(rightvalues.size() > 0){
                        mListadapter = new Listadapter(mHomeActivity, rightvalues, true, false, selectedName);
                        lvRight.setAdapter(mListadapter);
                        lvRight.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                    } else{
                        tvNoData.setVisibility(View.VISIBLE);
                        lvRight.setVisibility(View.GONE);
                        tvNoData.setText(mContext.getResources().getString(R.string.tv_no_industry));
                    }


                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        // TODO: Add code to remove fragment here
        super.onSaveInstanceState(outState);
    }

    void GetTanent(final String authToken, int userId, final int industryId, final boolean isFromList){
        mHomeActivity.startProgress(mContext);
        TimeZone mTimeZone = TimeZone.getDefault();
        String mTimeZoneStr = mTimeZone.getID();
        TimeZone tz = TimeZone.getTimeZone(mTimeZoneStr);
        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);

        String timeZoneString = "";
        if(hours > 0){
            timeZoneString = String.format("GMT+%d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
        } else if(hours == 0){
            timeZoneString = String.format("GMT", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
            //String temptimeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
        } else{
            timeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
        }
        String completeauthToken = "bearer" + " " + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), "");
        Logger.Error("Time Zone : " + timeZoneString);
        Logger.Error("Industry ID : " + industryId);
        Logger.Error("<< Auth >>> " + completeauthToken);
        Logger.Error("<< Platform >>> " + 1);
        Logger.Error("<< Content-Type>>> " + "application/json");
        final Call mCall = apiInterfaceToTenantcall.getTenant
                ("application/json",
                        completeauthToken,
                        String.valueOf(1),
                        String.valueOf(industryId), true,
                        timeZoneString,
                        userId);
        tenantList = new ArrayList<>();
        rightvalues = new ArrayList<>();
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    TenantDataModel tenantdataModel = (TenantDataModel) response.body();
                    tenantList = tenantdataModel.getData();
                    Collections.sort(tenantList, new Comparator<TenantDataModel.Data>(){
                        @Override
                        public int compare(TenantDataModel.Data datum, TenantDataModel.Data t1){
                            return datum.getTenantName().compareTo(t1.getTenantName());

                        }
                    });

                    int count = 0;
                    for(TenantDataModel.Data datum : tenantList){
                        if(count == 0){
                            if(!(selectedName.length() > 0)){
                                selectedName = datum.getTenantName();
                            }

                        }
                        count++;
                        rightvalues.add(datum.getTenantName());

                    }
                    if(tenantList.size() > 0){
                        lvRight.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);

                        if(!(selectedName.length() > 0)){
                            selectedName = tenantList.get(0).getTenantName();
                            selectedTenant = selectedName;

                            selectedTenantName = tenantList.get(0).getTenantName();
                            selectedTenantId = tenantList.get(0).getTenantId();
                            selectedtenantRoleName = "" + tenantList.get(0).getRoleName();
                            selectedUserRoleId = tenantList.get(0).getRoleId();
                            selectedUserRoleName = "" + tenantList.get(0).getRoleName();

                            selectedLoginTenantUserRole = getRoleName(tenantList.get(0).getRoleRolePermissions());


                        } else{
                            TenantDataModel.Data mData = getTenantDetail(selectedName);
                            if(mData != null){

                                selectedTenant = selectedName;

                                selectedTenantName = mData.getTenantName();
                                selectedTenantId = mData.getTenantId();
                                selectedtenantRoleName = "" + mData.getRoleName();
                                selectedUserRoleId = mData.getRoleId();
                                selectedUserRoleName = "" + mData.getRoleName();
                                selectedLoginTenantUserRole = getRoleName(mData.getRoleRolePermissions());

                            } else{
                                selectedName = tenantList.get(0).getTenantName();
                                selectedTenant = selectedName;

                                selectedTenantName = tenantList.get(0).getTenantName();
                                selectedTenantId = tenantList.get(0).getTenantId();
                                selectedtenantRoleName = "" + tenantList.get(0).getRoleName();
                                selectedUserRoleId = tenantList.get(0).getRoleId();
                                selectedUserRoleName = "" + tenantList.get(0).getRoleName();

                                selectedLoginTenantUserRole = getRoleName(tenantList.get(0).getRoleRolePermissions());
                            }
                        }

                        descriptionLeft.set(1, selectedTenant);
                        mPreferenceAdapter.notifyDataSetChanged();
                        //  getDefaultFilter(authToken,tenantList.get(0).getTenantId());
                        if(!isFromList){
                            // Collections.sort(values);
                            mHomeActivity.endProgress();
                            mListadapter = new Listadapter(getActivity(), rightvalues, true, false, selectedName);
                            lvRight.setAdapter(mListadapter);
                        } else{
                            selectedName = selectedTenant;
                            mPreferenceAdapter = new PreferenceListadapter(getActivity(), leftvalues, descriptionLeft, selectedTenant);
                            lvLeft.setAdapter(mPreferenceAdapter);



                            InGaugeSession.write(getString(R.string.fragment_preference_detail), "Tenant");
                            mListadapter = new Listadapter(getActivity(), rightvalues, true, false, selectedName);
                            lvRight.setAdapter(mListadapter);
                            getRolesAndPermission("application/json",
                                    InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token),
                                            null),
                                    true,
                                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 0)),
                                    String.valueOf(selectedTenantId), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -1)), true, true);
                        }

                        mPreferenceAdapter.notifyDataSetChanged();
                        Logger.Error(" <<<<<   ROLE Name  Preference Detail>>>>>>" + tenantList.get(0).getRoleName());
                        Logger.Error(" <<<<<   ROLE Name  Preference Detail>>>>>>" + InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                    } else{
                        mHomeActivity.endProgress();
                        if(!isFromList){
                            lvRight.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                            tvNoData.setText(mContext.getResources().getString(R.string.tv_no_tenant));

                        } else{
                            selectedTenantName = "";
                            selectedTenantId = -2;
                            selectedtenantRoleName = "";

                            /*InGaugeSession.write(getString(R.string.key_selected_tenant), "");
                            InGaugeSession.write(getString(R.string.key_selected_tenant_id), 0);
                            InGaugeSession.write(getString(R.string.key_tenant_role_name), "");*/
                            //   mHomeActivity.onBackPressed();
                        }

                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }

    public TenantDataModel.Data getTenantDetail(String tenantName){

        for(int i = 0; i < tenantList.size(); i++){
            if(tenantList.get(i) != null && tenantList.get(i).getTenantName().equals(tenantName)){
                return tenantList.get(i);
            }
        }

        return null;
    }

    void getMobileKeys(String IndustryId){
        Call mCall = apiInterface.getMobileKeys();
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){


                JSONObject data = null;
                if(response.body() != null){
                    Logger.Error("KEYS RESPONSE " + response.body());
                    try{
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        data = jsonObject.optJSONObject("data");
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    DatabaseHandler db = new DatabaseHandler(mContext);
                    db.addKeys(String.valueOf(data));


                    selectedmobilekeyObject = null;
                    selectedStringListMetricType.clear();
                    selectedFilterMetricTypeModelList.clear();
                    selectedmobilekeyObject = UiUtils.getMobileKeys(mHomeActivity);

                    selectedStringKeyListMetricType = Arrays.asList(mHomeActivity.getResources().getStringArray(R.array.key_metric_type_array));
                    // mStringListMetricType.clear();
                    for(int i = 0; i < selectedStringKeyListMetricType.size(); i++){
                        selectedStringListMetricType.add(selectedmobilekeyObject.optString(selectedStringKeyListMetricType.get(i)));
                    }

                    for(int i = 0; i < selectedStringListMetricType.size(); i++){
                        FilterMetricTypeModel metricTypeModel = new FilterMetricTypeModel();
                        metricTypeModel.id = "000" + String.valueOf(i);
                        metricTypeModel.name = selectedStringListMetricType.get(i);
                        metricTypeModel.filterName = selectedmobilekeyObject.optString(getResources().getString(R.string.mobile_key_filter_metric_type));
                        selectedFilterMetricTypeModelList.add(metricTypeModel);
                    }

                    if(selectedTenantId > 0){
                        getDynamicFilterMap(selectedTenantId, UiUtils.getUserId(mContext), false, false);
                    } else{
                        getDynamicFilterMap(InGaugeSession.read(getString(R.string.key_selected_tenant_id), tenantList.get(0).getTenantId()), UiUtils.getUserId(mContext), false, false);
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                t.printStackTrace();
            }
        });
    }

    void getDynamicFilterMap(int tenantId, int userID, boolean isShowDeactivatedLocation, final boolean isShowFromTenantListcall){
        Call<ResponseBody> mCall = apiInterface.getDynamicFiltersListMap(tenantId, userID, isShowDeactivatedLocation);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response){
                JSONObject data = null;
                if(response.body() != null){
                    try{
                        if(mCountryList != null){
                            if(mCountryList.size() > 0){
                                mCountryList.clear();
                                mCountryList = new ArrayList<DynamicData>();
                            }
                        }
                        JSONObject mJsonObject = new JSONObject(response.body().string());
                        mDataJson = mJsonObject.optJSONObject("data");
                        JSONArray firstJson = mDataJson.optJSONArray("-1");
                        //Map<Integer,JSONArray> integerJSONArrayMap  = new HashMap<>();


                        int level = 0;
                        if(firstJson != null && firstJson.length() > 0){
                            for(int i = 0; i < firstJson.length(); i++){
                                DynamicData mDynamicData = new DynamicData();
                                mDynamicData.setId(firstJson.getJSONObject(i).optInt("id"));
                                mDynamicData.setName(firstJson.getJSONObject(i).optString("name"));
                                mDynamicData.setRegionTypeName(firstJson.getJSONObject(i).optString("regionTypeName"));
                                mDynamicData.setRegionTypeId(firstJson.getJSONObject(i).optInt("regionTypeId"));
                                mDynamicData.setChildRegion(firstJson.getJSONObject(i).optString("childRegion"));
                                mDynamicData.setParentRegionId(firstJson.getJSONObject(i).optInt("parentRegionId"));
                                String masterLocation = firstJson.getJSONObject(i).optString("masterLocation");
                                mDynamicData.setMasterLocation(masterLocation);
                                if(masterLocation != null && masterLocation.length() > 0){
                                    mDynamicData.setMAsterLocation(true);
                                } else{
                                    mDynamicData.setMAsterLocation(false);
                                }
                                mCountryList.add(mDynamicData);
                            }
                            integerListHashMap.put(level, mCountryList);
                            for(DynamicData mDynamicDataParent : mCountryList){
                                if(mDynamicDataParent.getChildRegion() != null && mDynamicDataParent.getChildRegion().length() > 0)
                                    searchParent(mDynamicDataParent, integerListHashMap, level);
                            }
                        }
                        //integerListHashMap To Select On Click
                       /* Gson gson = new Gson();
                        String json = gson.toJson(integerListHashMap);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_dynamic_map_filter_obj), json);*/
                    } catch(JSONException e){
                        e.printStackTrace();
                        Logger.Error("<<< Exception  >>>>" + e.toString());
                    } catch(IOException e){
                        e.printStackTrace();
                        Logger.Error("<<< Exception  >>>>" + e.toString());
                    }

                    mHomeActivity.endProgress();
                    /*if(isShowFromTenantListcall)
                        mHomeActivity.onBackPressed();*/
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){
                t.printStackTrace();
                mHomeActivity.endProgress();
            }
        });
    }

    public void searchParent(DynamicData mDynamicDataParent, Map<Integer, List<DynamicData>> integerJSONArrayMap, int level){
        try{
            Logger.Error("Map" + integerJSONArrayMap);
            if(mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId())) != null){
                level++;
                JSONArray mJsonArray = mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId()));
                for(int i = 0; i < mJsonArray.length(); i++){
                    DynamicData mDynamicData = new DynamicData();
                    mDynamicData.setId(mJsonArray.getJSONObject(i).optInt("id"));
                    mDynamicData.setName(mJsonArray.getJSONObject(i).optString("name"));
                    mDynamicData.setRegionTypeName(mJsonArray.getJSONObject(i).optString("regionTypeName"));
                    mDynamicData.setRegionTypeId(mJsonArray.getJSONObject(i).optInt("regionTypeId"));
                    mDynamicData.setChildRegion(mJsonArray.getJSONObject(i).optString("childRegion"));
                    mDynamicData.setParentRegionId(mJsonArray.getJSONObject(i).optInt("parentRegionId"));
                    String masterLocation = mJsonArray.getJSONObject(i).optString("masterLocation");
                    mDynamicData.setMasterLocation(masterLocation);
                    if(masterLocation != null && masterLocation.length() > 0){
                        mDynamicData.setMAsterLocation(true);
                    } else{
                        mDynamicData.setMAsterLocation(false);
                    }
                    if(integerJSONArrayMap.get(level) != null){
                        List<DynamicData> mDynamicDataList = integerJSONArrayMap.get(level);
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level, mDynamicDataList);

                    } else{
                        List<DynamicData> mDynamicDataList = new ArrayList<>();
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level, mDynamicDataList);
                    }
                    searchParent(mDynamicData, integerJSONArrayMap, level);
                    //mCountryList.add(mDynamicData);
                }
                return;
            }
        } catch(JSONException e){
            e.printStackTrace();
        }


    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
    }

    public String getRoleName(List<TenantDataModel.RoleRolePermission> roleRolePermissions){
        if(roleRolePermissions != null && roleRolePermissions.size() > 0){
            for(int i = 0; i < roleRolePermissions.size(); i++){
                String name = roleRolePermissions.get(i).getName();
                if(name.equalsIgnoreCase("isPerformanceManager") || name.equalsIgnoreCase("isSuperAdmin") || name.equalsIgnoreCase("isFrontLineAssociate")){
                    return name;

                }
            }
        }
        return "";
    }


    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.fragment_dynamic_dashboard_preference_tv_apply:
                if(mHomeActivity.isDashboardUpdate()){
                    InGaugeSession.write(getString(R.string.key_guide_enabled_from_preference), true);

                    Gson gson = new Gson();
                    String json = gson.toJson(new ArrayList<FilterBaseData>());
                    InGaugeSession.write(getResources().getString(R.string.key_filter_obj), json);

                    gson = new Gson();
                    json = gson.toJson(new ArrayList<FilterBaseData>());
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_preference_filter_obj), json);


                    if(selectedIndustryName.length() > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_industry), selectedIndustryName);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_industry), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry), selectedIndustryName));
                    }

                    if(selectedIndustryId > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), selectedIndustryId);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), selectedIndustryId));
                    }

                    if(selectedTenantName.length() > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_tenant), selectedTenantName);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_tenant), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant), selectedTenantName));
                    }

                    if(selectedTenantId > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), selectedTenantId);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), selectedTenantId));
                    }

                    if(selectedtenantRoleName.length() > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_tenant_role_name), "" + selectedtenantRoleName);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_tenant_role_name), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_tenant_role_name), "" + selectedtenantRoleName));
                    }


                    if(selectedUserRoleName.length() > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_role_name), "" + selectedUserRoleName);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_role_name), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_role_name), "" + selectedUserRoleName));
                    }

                    if(selectedUserRoleId > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_role_id), selectedUserRoleId);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_role_id), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_role_id), selectedUserRoleId));
                    }

                    if(selectedLoginTenantUserRole.length() > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_login_tenant_user_role), selectedLoginTenantUserRole);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_login_tenant_user_role), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_login_tenant_user_role), selectedLoginTenantUserRole));
                    }

                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_video_category_name), "");
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_play_video_url), "");

                    if(selectedRolesPersmission.length() > 0){
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_role_permission), selectedRolesPersmission);
                    } else{
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_role_permission), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_role_permission), selectedRolesPersmission));
                    }


                    //// Get All Mobile Keys

                    //   selectedmobilekeyObject = null;
                    // selectedStringListMetricType.clear();
                    //selectedFilterMetricTypeModelList.clear();

                    mHomeActivity.mobilekeyObject = null;
                    mHomeActivity.mStringListMetricType.clear();
                    mHomeActivity.mFilterMetricTypeModelList.clear();


                    selectedmobilekeyObject = UiUtils.getMobileKeys(mHomeActivity);
                    mHomeActivity.mobilekeyObject = selectedmobilekeyObject;
                    selectedStringKeyListMetricType = Arrays.asList(mHomeActivity.getResources().getStringArray(R.array.key_metric_type_array));
                    mHomeActivity.mStringKeyListMetricType = selectedStringKeyListMetricType;
                    mHomeActivity.mStringListMetricType.addAll(selectedStringListMetricType);
                    mHomeActivity.mFilterMetricTypeModelList.addAll(selectedFilterMetricTypeModelList);

                    if(mHomeActivity.getIntegerListHashMap() != null && mHomeActivity.getIntegerListHashMap().size() > 0){
                        mHomeActivity.getIntegerListHashMap().clear();
                        for(List<DynamicData> mDynamicData : integerListHashMap.values()){
                            Collections.sort(mDynamicData, new Comparator<DynamicData>(){
                                @Override
                                public int compare(DynamicData d1, DynamicData d2){
                                    return Boolean.compare(d2.isMAsterLocation(), d1.isMAsterLocation());
                                }
                            });

                        }
                        mHomeActivity.setIntegerListHashMap(integerListHashMap);
                    } else{
                        for(List<DynamicData> mDynamicData : integerListHashMap.values()){
                            Collections.sort(mDynamicData, new Comparator<DynamicData>(){
                                @Override
                                public int compare(DynamicData d1, DynamicData d2){
                                    return Boolean.compare(d2.isMAsterLocation(), d1.isMAsterLocation());
                                }
                            });

                        }
                        mHomeActivity.setIntegerListHashMap(integerListHashMap);
                    }

                    InGaugeSession.write(getString(R.string.key_tag_index_obs), 4);
                    InGaugeSession.write(getString(R.string.key_tag_index), 4);
                    //integerListHashMap To Select On Click
                    gson = new Gson();
                    json = gson.toJson(mHomeActivity.getIntegerListHashMap());
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_dynamic_map_filter_obj), json);
                }


                mHomeActivity.onBackPressed();
                break;
            case R.id.fragment_dynamic_dashboard_preference_tv_canel:

                selectedmobilekeyObject = null;
                selectedmobilekeyObject = mHomeActivity.mobilekeyObject;

                selectedStringKeyListMetricType = new ArrayList<>();
                selectedStringKeyListMetricType = mHomeActivity.mStringKeyListMetricType;
                selectedStringListMetricType = new ArrayList<>();
                selectedFilterMetricTypeModelList = new ArrayList<>();
                integerListHashMap = new LinkedHashMap<>();

                leftvalues = new ArrayList<>();
                rightvalues = new ArrayList<>();
                tenantValues = new ArrayList<>();
                descriptionLeft = new ArrayList<>();
                selectedIndustry = InGaugeSession.read(getString(R.string.key_selected_industry), "");
                selectedTenant = InGaugeSession.read(getString(R.string.key_selected_tenant), "");
                //String valueIndustry =mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_industry));
                // String valueTenant  =mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_tenant));
                String valueIndustry = "Select Industry";
                String valueTenant = "Select Tenant";
                leftvalues.add(valueIndustry);
                leftvalues.add(valueTenant);
                descriptionLeft.add(selectedIndustry);
                descriptionLeft.add(selectedTenant);
        /*description.add("selected industry");
        description.add("selected tenant");*/
                mPreferenceAdapter = new PreferenceListadapter(getActivity(), leftvalues, descriptionLeft, selectedTenant);
                lvLeft.setAdapter(mPreferenceAdapter);
                InGaugeSession.write(getString(R.string.fragment_preference_detail), "Tenant");
                selectedName = selectedTenant;
                getRightList(true);
                break;
        }
    }
}
