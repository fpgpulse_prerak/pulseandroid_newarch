package com.ingauge.fragments;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.BaseActivity;
import com.ingauge.activities.HomeActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.ShareReportPojo;
import com.ingauge.session.InGaugeSession;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pathanaa on 14-06-2017.
 */

public class ShareFragment extends DialogFragment implements View.OnClickListener {
    private ImageView imgShare;
    private ImageView imgClose;
    private HomeActivity mHomeActivity;
    Context mContext;
    APIInterface apiInterface;
    EditText edtShareDescriptn;
    int customReportId;
    Call mCall;
    BaseActivity mBaseActivity;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        apiInterface=APIClient.getClientChart().create(APIInterface.class);
        return inflater.inflate(R.layout.fragment_share, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments()!=null)
        {
            customReportId=getArguments().getInt(mHomeActivity.KEY_CUSTOM_REPORT_ID);
        }
        imgShare = (ImageView) view.findViewById(R.id.img_share);
        imgClose = (ImageView)view.findViewById(R.id.img_close);
        edtShareDescriptn=(EditText) view.findViewById(R.id.edt_share_descriptn);
        imgShare.setOnClickListener(this);
        imgClose.setOnClickListener(this);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert);
    }

    @Override
    public void onStart()
    {
        if (getDialog() == null)
        {
            return;
        }

        getDialog().getWindow().setWindowAnimations(
                android.R.style.Animation_Translucent);
        getDialog().setCancelable(false);

        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
        mBaseActivity=(BaseActivity) mContext;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_share:
                ShareFeedApi(edtShareDescriptn.getText().toString(),InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token),""));
                break;
            case R.id.img_close:
                dismiss();
                break;

        }
    }

    private void ShareFeedApi(final String description,String authToken) {
        mBaseActivity.startProgress(mContext);
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype),-2);
        int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region),-2);
        int TenantLocationId =(InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
        int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group),-2);
        int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product),-2));
        int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user),-2));
        int LoginUserId = InGaugeSession.read(mContext.getString(R.string.key_user_id),-2);
        String FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server),"");
        String ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
        String CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
        String CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
        String MatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_matric_data_type),mContext.getResources().getStringArray(R.array.metric_type_array)[0]);

        boolean IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on),false);
        int IndustryId = InGaugeSession.read(mContext.getString(R.string.key_selected_industry_id), 1);

        mCall=apiInterface.shareFeed(customReportId,TenantId,RegionTypeId,RegionId,TenantLocationId,LocationGroupId,ProductId,UserId,FromDate,ToDate,IsCompare,CompFromDate,CompToDate,MatrixDataType,IndustryId,LoginUserId,authToken,new ShareReportPojo());
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
               mBaseActivity.endProgress();
                dismiss();

                Bundle bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                bundle.putString("role",InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                bundle.putString("industry",InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
                bundle.putString("location",InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putInt("reportId",customReportId);
                if(!TextUtils.isEmpty(description))
                    bundle.putString("comment",description);
                InGaugeApp.getFirebaseAnalytics().logEvent("share_report", bundle);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
             mBaseActivity.endProgress();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
    }
}
