package com.ingauge.fragments;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.ReportDetailsFeedsAdapter;
import com.ingauge.adapters.TableReportAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.socialinteraction.FragmentSocialInteractionLocationFilter;
import com.ingauge.fragments.socialinteraction.FragmentSocialInteractionLocationGroupFilter;
import com.ingauge.fragments.socialinteraction.FragmentSocialInteractionUserFilter;
import com.ingauge.listener.TableReportListener;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.FilterBaseDataForLocationGroup;
import com.ingauge.pojo.ReportDetailFeedsPojo;
import com.ingauge.pojo.ShareReportPojo;
import com.ingauge.pojo.TablePojo;
import com.ingauge.pojo.TableReportColumnPojo;
import com.ingauge.pojo.TableReportExpandItemPojo;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 30-Aug-17.
 */

public class DashboardDetailFragment extends BaseFragment implements View.OnClickListener, TableReportListener{


    View rootView;
    private LinearLayout llTableReport;
    private LinearLayout llSocialInteraction;
    private RelativeLayout rlTableReport;
    private RelativeLayout rlLikeCoommentShare;
    private ProgressBar mProgressBarForTable;
    private TextView tvHeaderOne, tvHeaderTwo, tvHeaderThree;
    private ExpandableListView lvtableReport;
    // private NestedScrollView mNestedScrollView;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private AppBarLayout mAppBarLayout;
    private TableReportAdapter mTableReportAdapter;
    private WebView webGraph, webGraphTemp;
    private WebView webGraphTable, webGraphTempTable;
    private ProgressBar mProgressBar;
    private ImageView ivShare, ivLike, ivComment;
    HomeActivity mHomeActivity;
    Context mContext;
    private ImageView ivImageScreenShot;

    public static String KEY_REPORT_ID = "key_report_id";
    public static String KEY_REPORT_Name = "key_report_name";
    public static String KEY_REPORT_TYPE = "key_report_type";
    public static String KEY_IS_SOCIAL_INTERACTION_ENABLE = "is_social_interaction_enable";
    public static String KEY_SOCIAL_ACTION_TYPE = "social_action_type";
    public static String KEY_IS_NEED_FILTER_CALL = "is_need_filter_call";
    public static String KEY_IMAGE_URI = "image_uri";
    public static String KEY_IS_MTD = "is_mtd";


    private boolean isSocialInteraction = false;
    private String reportId = "";
    private String reportName = "";
    private String reportType = "";
    private RelativeLayout rlParent;
    private String htmlRaw = "";
    private String GraphURL = "";
    private static final int PERMISSION_REQUEST_CODE = 001;
    private CoordinatorLayout mCoordinatorLayout;
    BottomSheetDialog mBottomSheetDialog;
    private TextView tvDialogPost, tvDialogShare, tvDialogCancel;
    private int webProgress = 0;
    File pdffile;

    APIInterface apiInterface;
    APIInterface apiInterfaceForShareReport;
    Call mCall;
    List<TablePojo> mTablePojos = new ArrayList<>();
    LinkedHashMap<Integer, LinkedHashMap<String, String>> pairs = new LinkedHashMap<>();
    List<String> mValueKeysString = new ArrayList<>();
    List<String> mKeysString = new ArrayList<>();
    List<String> mColumnHeader = new ArrayList<>();
    ArrayList<TableReportColumnPojo> mTableReportColumnPojos = new ArrayList<>();
    boolean isFirstColumnFilled = false;
    boolean isSecondColumnFilled = false;
    boolean isthirdColumnFilled = false;
    boolean isPermissionGranted = false;
    private TextView tvNoData;

    //Social Interaction UI Controls Declaration
    private RelativeLayout rlFilterValue;
    private LinearLayout llTenantType;
    private LinearLayout llLocationType;
    private LinearLayout llLocationGroupType;
    private LinearLayout llCollegueType;

    private ImageView ivTenantType;
    private ImageView ivLocationType;
    private ImageView ivLocationGroupType;
    private ImageView ivCollegueType;

    private TextView tvTenantType;
    private TextView tvLocationType;
    private TextView tvLocationGroupType;
    private TextView tvCollegueType;

    private TextView tvSelectedFilterType;
    private TextView tvSelectedFilterValue;
    private TextView tvNote;

    private TextInputLayout tilComment;
    private EditText etComment;
    private LinearLayout llCommentType;
    private List<FilterBaseData> mFilterBaseSelectedLocationData = new ArrayList<>();
    private List<FilterBaseData> mFilterBaseSelectedUserData = new ArrayList<>();
    private List<FilterBaseDataForLocationGroup> mFilterBaseSelectedlocationGroupData = new ArrayList<>();


    private TextView tvCommentType;

    /**
     * entityTypeTag is used to differentiate (0: Tenant, 1: Location,2:  LocationGruop & 3: user)
     */
    private int entityTypeTag = 0;

    /**
     * socialActionType is used to differentiate between (0 : Like, 1 : Comment, 3 : Share)
     */
    private int socialActionType = -1;

    private boolean isDashboardMTD = false;

    Map<Integer, List<TableReportExpandItemPojo>> map = new LinkedHashMap();


    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    SimpleDateFormat sdfServerDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    private ListView mListViewDashboardDetailFeedsList;
    // private View mViewBottom;


    ProgressBar mProgressBarLoadMore;
    private List<ReportDetailFeedsPojo.Datum> mDatumBaseList = new ArrayList<>();

    int first = 0;
    int total = 10;
    boolean isLoading = false;


    ReportDetailsFeedsAdapter mReportDetailsFeedsAdapter;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        apiInterfaceForShareReport = APIClient.getClientChart().create(APIInterface.class);

        if(getArguments() != null){
            reportId = getArguments().getString(KEY_REPORT_ID);
            reportName = getArguments().getString(KEY_REPORT_Name);
            reportType = getArguments().getString(KEY_REPORT_TYPE);
            isSocialInteraction = getArguments().getBoolean(KEY_IS_SOCIAL_INTERACTION_ENABLE);
            socialActionType = getArguments().getInt(KEY_SOCIAL_ACTION_TYPE);
            isDashboardMTD = getArguments().getBoolean(KEY_IS_MTD);
        }
        //mTableReportAdapter = new TableReportAdapter(mHomeActivity, mTablePojos, isFirstColumnFilled, isSecondColumnFilled, isthirdColumnFilled, map, reportId,false);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_dashboard_detail_feeds, container, false);
            initViews(rootView);


            AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) mCollapsingToolbarLayout.getLayoutParams();
            if(isSocialInteraction){
                // params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP); // list other flags here by |
                // mCollapsingToolbarLayout.setLayoutParams(params);
                ReportDetailsFeedsAdapter mReportDetailsFeedsAdapter = new ReportDetailsFeedsAdapter(mHomeActivity, new ArrayList<ReportDetailFeedsPojo.Datum>());
                mListViewDashboardDetailFeedsList.setAdapter(mReportDetailsFeedsAdapter);
            } else{
                params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED); // list other flags here by |
                mCollapsingToolbarLayout.setLayoutParams(params);
            }


            mListViewDashboardDetailFeedsList.setOnScrollListener(new AbsListView.OnScrollListener(){
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState){

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount){
                    int lastIndexInScreen = visibleItemCount + firstVisibleItem;
                    // It is time to load more items

                    if(lastIndexInScreen>0){
                        if(lastIndexInScreen >= totalItemCount && !isLoading){

                            // It is time to load more items

                            isLoading = true;
                            first = totalItemCount + first;

                            getReportDetailFeeds(Integer.parseInt(reportId), isLoading);
                            //    loadMore();


                        }
                    }

                }
            });



            updateFilterTypeSelectionUIOnClick("1");
            ivShare.setClickable(false);
            ivLike.setClickable(false);
            ivComment.setClickable(false);
            mHomeActivity.ibMenu.setClickable(true);


            webGraph.getSettings().setDomStorageEnabled(true);
            webGraph.getSettings().setAllowFileAccessFromFileURLs(true);
            webGraph.getSettings().setJavaScriptEnabled(true);


           /* webGraph.getSettings().setJavaScriptEnabled(true);
            webGraph.getSettings().setBuiltInZoomControls(true);
            webGraph.getSettings().setDisplayZoomControls(true);
            webGraph.getSettings().setDomStorageEnabled(true);
            webGraph.getSettings().setAllowContentAccess(true);
            webGraph.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webGraph.setDrawingCacheEnabled(true);
            UiUtils.setWebCache(webGraph);
            webGraph.getSettings().setAppCachePath(InGaugeApp.getInstance().getCacheDir().getAbsolutePath());
            webGraph.getSettings().setAllowFileAccess(true);
            webGraph.getSettings().setAppCacheEnabled(true);
            webGraph.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);*/


            webGraphTemp.getSettings().setJavaScriptEnabled(true);
            webGraphTemp.getSettings().setLoadWithOverviewMode(true);
            webGraphTemp.getSettings().setUseWideViewPort(true);

            webGraphTemp.getSettings().setSupportZoom(true);
            webGraphTemp.getSettings().setBuiltInZoomControls(true);
            webGraphTemp.getSettings().setDisplayZoomControls(false);


            webGraphTable.getSettings().setJavaScriptEnabled(true);
            webGraphTable.getSettings().setBuiltInZoomControls(true);
            webGraphTable.getSettings().setDisplayZoomControls(false);
            webGraphTable.getSettings().setDomStorageEnabled(true);
            webGraphTable.getSettings().setAllowContentAccess(true);
            webGraphTable.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webGraphTable.setDrawingCacheEnabled(true);
            UiUtils.setWebCache(webGraphTable);
            webGraphTable.getSettings().setAppCachePath(InGaugeApp.getInstance().getCacheDir().getAbsolutePath());
            webGraphTable.getSettings().setAllowFileAccess(true);
            webGraphTable.getSettings().setAppCacheEnabled(true);
            webGraphTable.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);


            webGraphTempTable.getSettings().setJavaScriptEnabled(true);
            webGraphTempTable.getSettings().setLoadWithOverviewMode(true);
            webGraphTempTable.getSettings().setUseWideViewPort(true);

            webGraphTempTable.getSettings().setSupportZoom(true);
            webGraphTempTable.getSettings().setBuiltInZoomControls(true);
            webGraphTempTable.getSettings().setDisplayZoomControls(false);

            // webGraphTemp.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            // webGraphTemp.setScrollbarFadingEnabled(false);

        /*if (Build.VERSION.SDK_INT >= 19) {
            webGraph.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            webGraph.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }*/


            //webGraph.setInitialScale(1);
            webGraph.setWebChromeClient(new WebChromeClient(){
                @Override
                public void onProgressChanged(WebView view, int progress){
                    mProgressBar.setVisibility(View.VISIBLE);
                    //webGraph.setVisibility(View.GONE);
                    if(progress == 100){
                        webProgress = progress;
                        ivShare.setClickable(true);
                        ivLike.setClickable(true);
                        ivComment.setClickable(true);
                        mHomeActivity.ibMenu.setClickable(true);
                        mProgressBar.setVisibility(View.GONE);
                        if(!isSocialInteraction)
                            webGraph.setVisibility(View.VISIBLE);
                        //view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");

                    }
                }
            });

            webGraphTable.setWebChromeClient(new WebChromeClient(){
                @Override
                public void onProgressChanged(WebView view, int progress){
                    //mProgressBar.setVisibility(View.VISIBLE);
                    //webGraph.setVisibility(View.GONE);
                    if(progress == 100){
                        webProgress = progress;
                        ivShare.setClickable(true);
                        mHomeActivity.ibMenu.setClickable(true);

                        //mProgressBar.setVisibility(View.GONE);
                        //webGraphTable.setVisibility(View.VISIBLE);
                        //view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");

                    }
                }
            });

            webGraph.addJavascriptInterface(new LoadListener(), "HTMLOUT");
            webGraph.setWebViewClient(new WebViewClient(){
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url){
                    Logger.Error(" URL " + url);
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url,
                                          Bitmap favicon){
                }

                @Override
                public void onPageFinished(WebView view, String url){
               /* view.loadUrl(
                        "javascript:(function() { " +
                                "var element = document.getElementById('tableContainer');"
                                + "element.parentNode.removeChild(element);" +
                                "})()");*/
//                view.loadUrl("javascript:alert(functionThatReturnsSomething)");
                    view.loadUrl("javascript:window.HTMLOUT.processHTML(document.documentElement.outerHTML.toString());");

                }
            });

            webGraphTable.addJavascriptInterface(new LoadListener(), "HTMLOUT");
            webGraphTable.setWebViewClient(new WebViewClient(){
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url){
                    Logger.Error(" URL " + url);
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url,
                                          Bitmap favicon){
                }

                @Override
                public void onPageFinished(WebView view, String url){
               /* view.loadUrl(
                        "javascript:(function() { " +
                                "var element = document.getElementById('tableContainer');"
                                + "element.parentNode.removeChild(element);" +
                                "})()");*/
//                view.loadUrl("javascript:alert(functionThatReturnsSomething)");
                    view.loadUrl("javascript:window.HTMLOUT.processHTML(document.documentElement.outerHTML.toString());");

                }
            });

          /*  int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
            int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype), -2);
            int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region), -2);
            int TenantLocationId = (InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
            int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group), -2);
            int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product), -2));
            int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user), -2));
            int LoginUserId = InGaugeSession.read(mContext.getString(R.string.key_user_id), -2);
            String FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
            String ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
            String CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
            String CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
            String finalMatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mContext.getResources().getStringArray(R.array.metric_type_array)[1]);
            String MatrixDataType = finalMatrixDataType;
            if (MatrixDataType.equalsIgnoreCase("Check out")) {
                MatrixDataType = "Arrival";
            } else if (MatrixDataType.equalsIgnoreCase("Check in")) {
                MatrixDataType = "Departure";
            } else if (MatrixDataType.equalsIgnoreCase("Daily")) {
                MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
            }
            boolean IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), false);*/


            int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
            int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype), -2);
            int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region), -2);
            int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product), -2));
            int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user), -2));
            int TenantLocationId = (InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
            int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group), -2);
            int LoginUserId = InGaugeSession.read(mContext.getString(R.string.key_user_id), -2);
            String finalMatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mContext.getResources().getStringArray(R.array.metric_type_array)[1]);
            String MatrixDataType = finalMatrixDataType;
            if(MatrixDataType.equalsIgnoreCase("Check out")){
                MatrixDataType = "Arrival";
            } else if(MatrixDataType.equalsIgnoreCase("Check in")){
                MatrixDataType = "Departure";
            } else if(MatrixDataType.equalsIgnoreCase("Daily")){
                MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
            } else{
                if(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                    MatrixDataType = "Arrival";
                }

            }


            String FromDate = "";
            String ToDate = "";
            String CompFromDate = "";
            String CompToDate = "";
            boolean IsCompare = false;
            if(isDashboardMTD){
                FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
                ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");

                try{
                    Date fromDateDate = sdfServer.parse(FromDate);
                    FromDate = DateTimeUtils.getFirstDay(fromDateDate);
                    Date TomDateDate = sdfServer.parse(ToDate);
                    ToDate = DateTimeUtils.getLastDay(TomDateDate);
                } catch(Exception e){
                    e.printStackTrace();
                }
                CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
                CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
                IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), true);

            } else{
                FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
                ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
                CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
                CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
                IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), true);
            }
           /* String FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
            String ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
            String CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
            String CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
            boolean IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), true);*/

            int IndustryId = InGaugeSession.read(mContext.getString(R.string.key_selected_industry_id), -2);
            if(isSocialInteraction){
                mListViewDashboardDetailFeedsList.setVisibility(View.GONE);
                //   mViewBottom.setVisibility(View.VISIBLE);
                rlLikeCoommentShare.setVisibility(View.GONE);
                llSocialInteraction.setVisibility(View.VISIBLE);
                ivImageScreenShot.setVisibility(View.VISIBLE);
                ivImageScreenShot.setImageBitmap(HomeActivity.mBitmap);
                webGraph.setVisibility(View.GONE);
                mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.share_to_feed));

                /**
                 * socialActionType is used to differentiate between (0 : Like, 1 : Comment, 3 : Share)
                 */
                if(socialActionType == 0){
                    tvCommentType.setVisibility(View.GONE);
                    etComment.setVisibility(View.GONE);
                    llCommentType.setVisibility(View.GONE);
                } else if(socialActionType == 1){
                    tvCommentType.setVisibility(View.VISIBLE);
                    tvCommentType.setText(mHomeActivity.getResources().getString(R.string.your_comment));
                    etComment.setVisibility(View.VISIBLE);
                    llCommentType.setVisibility(View.VISIBLE);
                } else if(socialActionType == 3){
                    tvCommentType.setText(mHomeActivity.getResources().getString(R.string.share_your_report));
                    tvCommentType.setVisibility(View.VISIBLE);
                    etComment.setVisibility(View.VISIBLE);
                    llCommentType.setVisibility(View.VISIBLE);
                }


            } else{
                mHomeActivity.getmLocationFilterData().clear();
                rlLikeCoommentShare.setVisibility(View.VISIBLE);
                llSocialInteraction.setVisibility(View.GONE);
                ivImageScreenShot.setVisibility(View.GONE);
                webGraph.setVisibility(View.VISIBLE);
            }
            if(reportType.equalsIgnoreCase("Block")){
                mAppBarLayout.setVisibility(View.VISIBLE);
                rlTableReport.setVisibility(View.GONE);
                tvDialogPost.setVisibility(View.VISIBLE);
                mHomeActivity.ibMenu.setVisibility(View.GONE);
                GraphURL = UiUtils.baseUrlChart + "htmlReport/" + reportId + "?tenantId=" + TenantId + "&regionTypeId=" + RegionTypeId + "&regionId=" + RegionId + "&tenantLocationId=" + TenantLocationId + "&locationGroupId=" + LocationGroupId + "&productId=" + ProductId + "&userId=" + UserId + "&from=" + FromDate + "&to=" + ToDate + "&isCompare=" + IsCompare + "&compFrom=" + CompFromDate + "&compTo=" + CompToDate + "&metricsDateType=" + MatrixDataType + "&" + "industryId=" + IndustryId + "&loginUser=" + LoginUserId + "&widgetType=column";
                getReportDetailFeeds(Integer.parseInt(reportId), isLoading);
            } else if(reportType.equalsIgnoreCase("Table")){
                mAppBarLayout.setVisibility(View.GONE);
                rlTableReport.setVisibility(View.VISIBLE);
                tvDialogPost.setVisibility(View.GONE);
                GraphURL = UiUtils.baseUrlChart + "htmlReport/" + reportId + "?tenantId=" + TenantId + "&regionTypeId=" + RegionTypeId + "&regionId=" + RegionId + "&tenantLocationId=" + TenantLocationId + "&locationGroupId=" + LocationGroupId + "&productId=" + ProductId + "&userId=" + UserId + "&from=" + FromDate + "&to=" + ToDate + "&isCompare=" + IsCompare + "&compFrom=" + CompFromDate + "&compTo=" + CompToDate + "&metricsDateType=" + MatrixDataType + "&" + "industryId=" + IndustryId + "&loginUser=" + LoginUserId;
                webGraphTable.loadUrl(GraphURL);
                Ion.with(getContext())
                        .load(GraphURL)
                        .asString()
                        .setCallback(new FutureCallback<String>(){
                                         @Override
                                         public void onCompleted(Exception e, String result){

                                             //webGraph.loadDataWithBaseURL(GraphURL, result, "text/html", "utf-8", null);


                                         }
                                     }
                        );
                getDashboardCustomResport(Integer.parseInt(reportId));
            } else{
                mHomeActivity.ibMenu.setVisibility(View.GONE);
                tvDialogPost.setVisibility(View.VISIBLE);
                mAppBarLayout.setVisibility(View.VISIBLE);
                rlTableReport.setVisibility(View.GONE);
                GraphURL = UiUtils.baseUrlChart + "htmlReport/" + reportId + "?tenantId=" + TenantId + "&regionTypeId=" + RegionTypeId + "&regionId=" + RegionId + "&tenantLocationId=" + TenantLocationId + "&locationGroupId=" + LocationGroupId + "&productId=" + ProductId + "&userId=" + UserId + "&from=" + FromDate + "&to=" + ToDate + "&isCompare=" + IsCompare + "&compFrom=" + CompFromDate + "&compTo=" + CompToDate + "&metricsDateType=" + MatrixDataType + "&" + "industryId=" + IndustryId + "&loginUser=" + LoginUserId;
                getReportDetailFeeds(Integer.parseInt(reportId), isLoading);
            }

            if(!TextUtils.isEmpty(GraphURL)){
                webGraph.loadUrl(GraphURL);
                Ion.with(getContext())
                        .load(GraphURL)
                        .asString()
                        .setCallback(new FutureCallback<String>(){
                                         @Override
                                         public void onCompleted(Exception e, String result){

                                             //webGraph.loadDataWithBaseURL(GraphURL, result, "text/html", "utf-8", null);


                                         }
                                     }
                        );


                Logger.Error("Graph URL " + GraphURL);

            }

        } else{
            if(rootView != null && rootView.getParent() != null){
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }


            Logger.Error("<<<<<< Table Pojos  >>>" + mTablePojos.size());
            Logger.Error("<<<<<< Pairs  >>>" + pairs.size());
            Logger.Error("<<<<<< Pairs  >>>" + mTableReportColumnPojos.size());

        }

        return rootView;

    }


    @Override
    public void getHeaderName(String headerone, String headerTwo, String headerThree, String headervalueone, String headervaluetwo, String headervaluethree){
        Logger.Error("<<<< Get Header Name >>>>>>");

        if(headervalueone.length() > 0){
            tvHeaderOne.setText(headervalueone);
            tvHeaderOne.setVisibility(View.VISIBLE);
        } else{
            tvHeaderOne.setVisibility(View.GONE);
        }
        if(headervaluetwo.length() > 0){
            tvHeaderTwo.setText(headervaluetwo);
            tvHeaderTwo.setVisibility(View.VISIBLE);
        } else{
            tvHeaderTwo.setVisibility(View.GONE);
        }
        if(headervaluethree.length() > 0){
            tvHeaderThree.setText(headervaluethree);
            tvHeaderThree.setVisibility(View.VISIBLE);
        } else{
            tvHeaderThree.setVisibility(View.GONE);
        }


        mTablePojos.clear();
        String columnValueOne = "";
        String columnValueTwo = "";
        String columnValueThree = "";

        for(int i = 0; i < pairs.size(); i++){
            if(headerone.length() > 0){

                if(((HashMap<String, String>) pairs.get(i)).get(headerone) == null){
                    columnValueOne = "";
                } else{
                    columnValueOne = ((HashMap<String, String>) pairs.get(i)).get(headerone).toString();
                }
                isFirstColumnFilled = true;
            } else{
                isFirstColumnFilled = false;
            }
            if(headerTwo.length() > 0){
                if(((HashMap<String, String>) pairs.get(i)).get(headerTwo) == null){
                    columnValueTwo = "";
                } else{
                    columnValueTwo = ((HashMap<String, String>) pairs.get(i)).get(headerTwo).toString();
                }
                isSecondColumnFilled = true;
            } else{
                isSecondColumnFilled = false;
            }
            if(headerThree.length() > 0){
                if(((HashMap<String, String>) pairs.get(i)).get(headerThree) == null){
                    columnValueThree = "";
                } else{
                    columnValueThree = ((HashMap<String, String>) pairs.get(i)).get(headerThree).toString();
                }
                isthirdColumnFilled = true;
            } else{
                isthirdColumnFilled = false;
            }
            getTableList(columnValueOne, columnValueTwo, columnValueThree);
        }

        if(mTableReportAdapter != null){
            int count = mTableReportAdapter.getGroupCount();
            for(int i = 0; i < count; i++)
                lvtableReport.collapseGroup(i);
        }
        mTableReportAdapter = new TableReportAdapter(mHomeActivity, mTablePojos, isFirstColumnFilled, isSecondColumnFilled, isthirdColumnFilled, map, reportId, true);
        lvtableReport.setAdapter(mTableReportAdapter);
    }


    final class MyWebChromeClient extends WebChromeClient{
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result){
            //Log.d("LogTag", message);
            Logger.Error("JS Message " + message);
            result.confirm();
            return true;
        }
    }

    private void initViews(View view){
        mProgressBarLoadMore = (ProgressBar) view.findViewById(R.id.fragment_dashboard_detail_pb_load_more);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) view.findViewById(R.id.fragment_dashboard_detail_feeds_collapsing_toolbar);
        mListViewDashboardDetailFeedsList = (ListView) view.findViewById(R.id.fragment_dashboard_detail_rv_feeds_list);
        //  mViewBottom = (View) view.findViewById(R.id.fragment_dashboard_details_view);
        mListViewDashboardDetailFeedsList.setNestedScrollingEnabled(true);
        tvCommentType = (TextView) view.findViewById(R.id.fragment_dashboard_titlee_tv_comment_type);
        tvNoData = (TextView) view.findViewById(R.id.fragment_dashboard_detail_tv_no_data);
        rlTableReport = (RelativeLayout) view.findViewById(R.id.rl_table_report);
        llCommentType = (LinearLayout) view.findViewById(R.id.fragment_dashboard_detail_comment_type);
        rlLikeCoommentShare = (RelativeLayout) view.findViewById(R.id.fragment_dashboard_detail_rl_like_comment_share);
        lvtableReport = (ExpandableListView) view.findViewById(R.id.fragment_dashboard_detail_lv_table_report);
        //mNestedScrollView = (NestedScrollView) view.findViewById(R.id.fragment_dashboard_detail_ns);
        mAppBarLayout = (AppBarLayout) view.findViewById(R.id.fragment_dashboard_app_bar_layout);
        llTableReport = (LinearLayout) view.findViewById(R.id.fragment_dashboard_detail_ll_table_report);
        llSocialInteraction = (LinearLayout) view.findViewById(R.id.fragment_dashboard_detail_ll_social_interaction);
        mProgressBarForTable = (ProgressBar) view.findViewById(R.id.progress_row_table);
        tvHeaderOne = (TextView) view.findViewById(R.id.fragment_dashboard_detail_tv_header_one);
        tvHeaderTwo = (TextView) view.findViewById(R.id.fragment_dashboard_detail_tv_header_two);
        tvHeaderThree = (TextView) view.findViewById(R.id.fragment_dashboard_detail_tv_header_three);
        webGraph = (WebView) view.findViewById(R.id.web_graph);
        webGraphTemp = (WebView) view.findViewById(R.id.web_graph_temp);
        webGraphTable = (WebView) view.findViewById(R.id.web_graph_table);
        webGraphTempTable = (WebView) view.findViewById(R.id.web_graph_temp_table);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_row);
        ivShare = (ImageView) view.findViewById(R.id.fragment_dashboard_detail_iv_share);
        ivLike = (ImageView) view.findViewById(R.id.fragment_dashboard_detail_iv_like);
        ivComment = (ImageView) view.findViewById(R.id.fragment_dashboard_detail_iv_comment);
        ivShare.setOnClickListener(this);
        ivLike.setOnClickListener(this);
        ivComment.setOnClickListener(this);
        rlParent = (RelativeLayout) view.findViewById(R.id.fragment_dashboard_rl);
        mCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator);

        View bottomsheetView = LayoutInflater.from(mHomeActivity).inflate(R.layout.bottom_sheet_dialog_share, null);
        tvDialogPost = (TextView) bottomsheetView.findViewById(R.id.tv_dialog_post);
        tvDialogShare = (TextView) bottomsheetView.findViewById(R.id.tv_dialog_share);
        tvDialogCancel = (TextView) bottomsheetView.findViewById(R.id.tv_dialog_cancel);
        tvDialogPost.setOnClickListener(this);
        tvDialogShare.setOnClickListener(this);
        tvDialogCancel.setOnClickListener(this);


        ivImageScreenShot = (ImageView) view.findViewById(R.id.fragment_dashboard_detail_iv_report_screen_shot);
        llTenantType = (LinearLayout) view.findViewById(R.id.row_social_interaction_ll_tenant_type_filter);
        llLocationType = (LinearLayout) view.findViewById(R.id.row_social_interaction_ll_location_type_filter);
        llLocationGroupType = (LinearLayout) view.findViewById(R.id.row_social_interaction_ll_location_group_type_filter);
        llCollegueType = (LinearLayout) view.findViewById(R.id.row_social_interaction_ll_user_type_filter);
        llTenantType.setOnClickListener(this);
        llLocationType.setOnClickListener(this);
        llLocationGroupType.setOnClickListener(this);
        llCollegueType.setOnClickListener(this);

        ivTenantType = (ImageView) view.findViewById(R.id.row_social_interaction_iv_tenant_type_filter);
        ivLocationType = (ImageView) view.findViewById(R.id.row_social_interaction_iv_location_type_filter);
        ivLocationGroupType = (ImageView) view.findViewById(R.id.row_social_interaction_iv_location_group_type_filter);
        ivCollegueType = (ImageView) view.findViewById(R.id.row_social_interaction_iv_user_type_filter);

        tvTenantType = (TextView) view.findViewById(R.id.row_social_interaction_tv_tenant_type_filter);
        tvLocationType = (TextView) view.findViewById(R.id.row_social_interaction_tv_location_type_filter);
        tvLocationGroupType = (TextView) view.findViewById(R.id.row_social_interaction_tv_location_group_type_filter);
        tvCollegueType = (TextView) view.findViewById(R.id.row_social_interaction_tv_user_type_filter);

        tvSelectedFilterType = (TextView) view.findViewById(R.id.fragment_dashboard_detail_tv_selected_filter_type);
        tvSelectedFilterValue = (TextView) view.findViewById(R.id.fragment_dashboard_detail_tv_selected_filter_selected_value);
        tvNote = (TextView) view.findViewById(R.id.fragment_dashboard_detail_tv_note);
        tilComment = (TextInputLayout) view.findViewById(R.id.fragment_dashboard_detail_til_comment);
        etComment = (EditText) view.findViewById(R.id.fragment_dashboard_detail_et_comment);
        rlFilterValue = (RelativeLayout) view.findViewById(R.id.fragment_dashboard_detail_rv_filter_value_click);
        rlFilterValue.setOnClickListener(this);
        mBottomSheetDialog = new BottomSheetDialog(mHomeActivity);
        mBottomSheetDialog.setContentView(bottomsheetView);
        //tvSelectedFilterValue.setText("");
    }

    @Override
    public void onResume(){
        super.onResume();
        Logger.Error("<<<< Call Home Activity>>>>");
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setTag("Back");
        ivShare.setClickable(true);
        //mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        mHomeActivity.ibMenu.setClickable(true);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);

        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        if(isSocialInteraction){
            mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.share_to_feed));
        } else{
            mHomeActivity.tvTitle.setText(reportName);
        }

        mHomeActivity.ibMenu.setImageResource(android.R.drawable.ic_menu_share);
        //mHomeActivity.ibMenu.setBackgroundTintList(ContextCompat.getColorStateList(mHomeActivity, R.color.white));

        mHomeActivity.ibAttachment.setImageResource(android.R.drawable.ic_menu_add);
        if(reportType.equalsIgnoreCase("Table")){
            mHomeActivity.ibAttachment.setVisibility(View.VISIBLE);
            mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        } else{

            mHomeActivity.ibAttachment.setVisibility(View.GONE);
            if(isSocialInteraction){
                mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
                mHomeActivity.ibMenu.setImageResource(R.drawable.ic_share_comment);
                //mHomeActivity.ibMenu.setBackgroundTintList(ContextCompat.getColorStateList(mHomeActivity, R.color.white));
            } else{
                mHomeActivity.ibMenu.setVisibility(View.GONE);
            }

        }
        // mHomeActivity.ibAttachment.setBackgroundTintList(ContextCompat.getColorStateList(mHomeActivity, R.color.white));

        mHomeActivity.ibAttachment.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //mTableReportColumnPojos.put


                Bundle mBundle = new Bundle();
                mBundle.putParcelableArrayList(MultiSelectionFragmentForTableReport.KEY_COLUMN_LIST, mTableReportColumnPojos);
                MultiSelectionFragmentForTableReport multiSelectionFragmentForTableReport = new MultiSelectionFragmentForTableReport();
                multiSelectionFragmentForTableReport.setArguments(mBundle);
                mHomeActivity.replace(multiSelectionFragmentForTableReport, mContext.getResources().getString(R.string.tag_multi_selection_fragment_for_table_report));
            }
        });

        mHomeActivity.ibMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Logger.Error("<<<<   List Of Selected Filter >>>> " + mHomeActivity.getmLocationFilterData().size());
                if(reportType.equalsIgnoreCase("Table")){
                    if(!mBottomSheetDialog.isShowing())
                        mBottomSheetDialog.show();
                } else{
                    if(isSocialInteraction){
                        //   if (entityTypeTag == 1 || entityTypeTag == 2 || entityTypeTag == 3) {
                        checkValidationNeforeShareSocial();
                        // } else {
                        //   ShareFeedApi(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), getSocialShareObject());
                        //}
                    }

                }
            }
        });
        if(entityTypeTag != 0){
            if(entityTypeTag == 1){
                if(!TextUtils.isEmpty(getLocationFilterSelectedValues())){
                    tvSelectedFilterValue.setText("");
                    tvSelectedFilterValue.setText(getLocationFilterSelectedValues());
                } else{
                    updateFilterTypeSelectionUIOnClick("2");
                }
            } else if(entityTypeTag == 2){
                if(!TextUtils.isEmpty(getLocationGroupFilterValues())){
                    tvSelectedFilterValue.setText("");
                    tvSelectedFilterValue.setText(getLocationGroupFilterValues());
                } else{
                    updateFilterTypeSelectionUIOnClick("3");
                }


            } else if(entityTypeTag == 3){

                if(!TextUtils.isEmpty(getUserFilterSelectedValues())){
                    tvSelectedFilterValue.setText("");
                    tvSelectedFilterValue.setText(getUserFilterSelectedValues());
                } else{
                    updateFilterTypeSelectionUIOnClick("4");
                }
            }
        }
    }

    public void checkValidationNeforeShareSocial(){
        ShareReportPojo mShareReportPojo = getSocialShareObject();
        if(!(mShareReportPojo.getFeedShareWithDTOs() != null && mShareReportPojo.getFeedShareWithDTOs().size() > 0)){
            if(entityTypeTag == 1){
                Toast.makeText(mHomeActivity, "Select At least One Location From Filter", Toast.LENGTH_SHORT).show();
            } else if(entityTypeTag == 2){
                Toast.makeText(mHomeActivity, "Select At least One Location Group From Filter", Toast.LENGTH_SHORT).show();
            } else if(entityTypeTag == 3){
                Toast.makeText(mHomeActivity, "Select At least One User From Filter", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        if(socialActionType == 1){
            if(TextUtils.isEmpty(etComment.getText().toString())){
                Toast.makeText(mHomeActivity, "Enter Comment First", Toast.LENGTH_SHORT).show();
                return;
            }
        }


        ShareFeedApi(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), getSocialShareObject());
    }

    public ShareReportPojo getSocialShareObject(){
        ShareReportPojo mShareReportPojo = new ShareReportPojo();
        mShareReportPojo.setDescription(etComment.getText().toString().trim());
        mShareReportPojo.setSubType(socialActionType);
        List<ShareReportPojo.FeedShareWithDTO> feedShareWithDTOList = new ArrayList<>();
        if(entityTypeTag == 0){

            ShareReportPojo.FeedShareWithDTO mFeedShareWithDTO = new ShareReportPojo().new FeedShareWithDTO();
            mFeedShareWithDTO.setEntityId(String.valueOf(InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2)));
            mFeedShareWithDTO.setEntityType(entityTypeTag);
            mFeedShareWithDTO.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
            feedShareWithDTOList.add(mFeedShareWithDTO);
        } else if(entityTypeTag == 1){
            for(int i = 0; i < mFilterBaseSelectedLocationData.size(); i++){
                ShareReportPojo.FeedShareWithDTO mFeedShareWithDTO = new ShareReportPojo().new FeedShareWithDTO();
                mFeedShareWithDTO.setEntityId(mFilterBaseSelectedLocationData.get(i).id);
                mFeedShareWithDTO.setEntityType(entityTypeTag);
                mFeedShareWithDTO.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                feedShareWithDTOList.add(mFeedShareWithDTO);

            }
        } else if(entityTypeTag == 2){
            for(int i = 0; i < mHomeActivity.getmLocationGroupForLGFilterData().size(); i++){
                for(int j = 0; j < mHomeActivity.getmLocationGroupForLGFilterData().get(i).mFilterBaseDatas.size(); j++){
                    if(mHomeActivity.getmLocationGroupForLGFilterData().get(i).mFilterBaseDatas.get(j).isSelectedPosition){
                        ShareReportPojo.FeedShareWithDTO mFeedShareWithDTO = new ShareReportPojo().new FeedShareWithDTO();
                        mFeedShareWithDTO.setEntityId(mHomeActivity.getmLocationGroupForLGFilterData().get(i).mFilterBaseDatas.get(j).id);
                        mFeedShareWithDTO.setEntityType(entityTypeTag);
                        mFeedShareWithDTO.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                        feedShareWithDTOList.add(mFeedShareWithDTO);
                    }
                }
            }

        } else if(entityTypeTag == 3){
            for(int i = 0; i < mFilterBaseSelectedUserData.size(); i++){
                ShareReportPojo.FeedShareWithDTO mFeedShareWithDTO = new ShareReportPojo().new FeedShareWithDTO();
                mFeedShareWithDTO.setEntityId(mFilterBaseSelectedUserData.get(i).id);
                mFeedShareWithDTO.setEntityType(entityTypeTag);
                mFeedShareWithDTO.setIndustryId(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
                feedShareWithDTOList.add(mFeedShareWithDTO);
            }
        }
        mShareReportPojo.setFeedShareWithDTOs(feedShareWithDTOList);
        return mShareReportPojo;
    }

    public String getUserFilterSelectedValues(){
        mFilterBaseSelectedUserData.clear();
        String filterSelectedValues = "";
        for(int i = 0; i < mHomeActivity.getmUserFilterData().size(); i++){
            if(mHomeActivity.getmUserFilterData().get(i).isSelectedPosition){
                mFilterBaseSelectedUserData.add(mHomeActivity.getmUserFilterData().get(i));
                if(filterSelectedValues.length() > 0){
                    filterSelectedValues = filterSelectedValues + "," + mHomeActivity.getmUserFilterData().get(i).name;

                } else{
                    filterSelectedValues = mHomeActivity.getmUserFilterData().get(i).name;
                }
            }
        }
        if(mFilterBaseSelectedUserData.size() > 0){
            return filterSelectedValues;
        }
        return "";
    }

    public String getLocationGroupFilterValues(){
        mFilterBaseSelectedlocationGroupData.clear();
        String filterSelectedValues = "";
        for(int i = 0; i < mHomeActivity.getmLocationGroupForLGFilterData().size(); i++){
            for(int j = 0; j < mHomeActivity.getmLocationGroupForLGFilterData().get(i).mFilterBaseDatas.size(); j++){
                if(mHomeActivity.getmLocationGroupForLGFilterData().get(i).mFilterBaseDatas.get(j).isSelectedPosition){
                    if(filterSelectedValues.length() > 0){
                        filterSelectedValues = filterSelectedValues + "," + mHomeActivity.getmLocationGroupForLGFilterData().get(i).mFilterBaseDatas.get(j).name
                                + " [" + mHomeActivity.getmLocationGroupForLGFilterData().get(i).name + "]";

                    } else{
                        filterSelectedValues = mHomeActivity.getmLocationGroupForLGFilterData().get(i).mFilterBaseDatas.get(j).name
                                + " [" + mHomeActivity.getmLocationGroupForLGFilterData().get(i).name + "]";
                    }
                }


            }
        }

        return filterSelectedValues;
    }

    public String getLocationFilterSelectedValues(){
        mFilterBaseSelectedLocationData.clear();
        String filterSelectedValues = "";
        for(int i = 0; i < mHomeActivity.getmLocationFilterData().size(); i++){
            if(mHomeActivity.getmLocationFilterData().get(i).isSelectedPosition){
                mFilterBaseSelectedLocationData.add(mHomeActivity.getmLocationFilterData().get(i));
                if(filterSelectedValues.length() > 0){
                    filterSelectedValues = filterSelectedValues + "," + mHomeActivity.getmLocationFilterData().get(i).name;

                } else{
                    filterSelectedValues = mHomeActivity.getmLocationFilterData().get(i).name;
                }
            }
        }
        if(mFilterBaseSelectedLocationData.size() > 0){
            return filterSelectedValues;
        }
        return "";
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(mHomeActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(result == PackageManager.PERMISSION_GRANTED){
            // showFileChooser(htmlRaw, reportType);
            return true;

        } else{

            return false;
        }
    }

    private void requestPermission(){

        ActivityCompat.requestPermissions(mHomeActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch(requestCode){
            case PERMISSION_REQUEST_CODE:
                Logger.Error("<<<< Calllllllll 1111111 >>>>>>");
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Logger.Error("<<<< Calllllllll  22222>>>>>>");
                    isPermissionGranted = true;
                    showFileChooser(htmlRaw, reportType);
                } else{
                    Logger.Error("<<<< Calllllllll  33333>>>>>>");
                    Snackbar.make(rlParent, "Permission Denied, Please allow to proceed !", Snackbar.LENGTH_LONG).show();

                }
                break;
        }
    }


    public class PdfWriterTask extends AsyncTask<Void, Void, Void>{

        String htmlString = "";

        PdfWriterTask(String htmlString){
            this.htmlString = htmlString;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            mHomeActivity.startProgress(mHomeActivity);
        }

        @Override
        protected Void doInBackground(Void... params){
            Bitmap bitmap = UiUtils.screenShot(webGraphTempTable);
            createPdf(bitmap);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid){
            super.onPostExecute(aVoid);

            mHomeActivity.endProgress();
        }
    }


    private void showFileChooser(String htmlString, String reportType){

        if(reportType.equalsIgnoreCase("Table")){
            new PdfWriterTask(htmlString).execute();
        } else{
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            Bitmap bitmap = UiUtils.screenShot(webGraph);
            String bitmapPath = MediaStore.Images.Media.insertImage(mHomeActivity.getContentResolver(), bitmap, "title", null);
            Uri bitmapUri = Uri.parse(bitmapPath);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
            shareIntent.setType("image/jpeg");

            try{
                startActivity(Intent.createChooser(shareIntent, "My Profile ..."));
            } catch(android.content.ActivityNotFoundException ex){

                ex.printStackTrace();
            }
        }


    }

    @Override
    public void onClick(View v){

        switch(v.getId()){
            case R.id.tv_dialog_post:
                ShareFragment dialog = new ShareFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(mHomeActivity.KEY_CUSTOM_REPORT_ID, Integer.parseInt(reportId));
                dialog.setArguments(bundle);
                dialog.show(mHomeActivity.getFragmentManager(), "dialog");
                mBottomSheetDialog.dismiss();
                break;
            case R.id.tv_dialog_share:
                if(checkPermission()){
                    showFileChooser(htmlRaw, reportType);
                } else{
                    requestPermission();
                }
                mBottomSheetDialog.dismiss();

                break;
            case R.id.tv_dialog_cancel:
                mBottomSheetDialog.dismiss();
                break;
            case R.id.fragment_dashboard_detail_iv_like:
                DashboardDetailSocialInteractionFragment mDashboardDetailSocialInteractionFragment = new DashboardDetailSocialInteractionFragment();

                Bundle mBundle = new Bundle();
                if(mProgressBar.getVisibility() == View.GONE){
                    mHomeActivity.getmLocationFilterData().clear();
                    mHomeActivity.getmUserFilterData().clear();
                    mHomeActivity.getmLocationGroupForLGFilterData().clear();
                    mDashboardDetailSocialInteractionFragment = new DashboardDetailSocialInteractionFragment();
                    mBundle = new Bundle();
                    mBundle.putString(KEY_REPORT_ID, reportId);
                    mBundle.putString(KEY_REPORT_Name, reportName);
                    mBundle.putString(KEY_REPORT_TYPE, reportType);
                    mBundle.putBoolean(KEY_IS_SOCIAL_INTERACTION_ENABLE, true);
                    mBundle.putBoolean(KEY_IS_SOCIAL_INTERACTION_ENABLE, true);
                    mBundle.putInt(KEY_SOCIAL_ACTION_TYPE, 0);
                    mDashboardDetailSocialInteractionFragment.setArguments(mBundle);
                    Picture picture = webGraph.capturePicture();
                    Bitmap b = Bitmap.createBitmap(picture.getWidth(),
                            picture.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas c = new Canvas(b);
                    picture.draw(c);
                    HomeActivity.mBitmap = b;
                    mHomeActivity.replace(mDashboardDetailSocialInteractionFragment, mContext.getResources().getString(R.string.tag_dashboard_detail));
                } else{
                    Toast.makeText(mHomeActivity, "Wait to load Graph", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.fragment_dashboard_detail_iv_comment:
                if(mProgressBar.getVisibility() == View.GONE){
                    mHomeActivity.getmLocationFilterData().clear();
                    mHomeActivity.getmUserFilterData().clear();
                    mHomeActivity.getmLocationGroupForLGFilterData().clear();
                    mDashboardDetailSocialInteractionFragment = new DashboardDetailSocialInteractionFragment();
                    mBundle = new Bundle();
                    mBundle.putString(KEY_REPORT_ID, reportId);
                    mBundle.putString(KEY_REPORT_Name, reportName);
                    mBundle.putString(KEY_REPORT_TYPE, reportType);
                    mBundle.putBoolean(KEY_IS_SOCIAL_INTERACTION_ENABLE, true);
                    mBundle.putBoolean(KEY_IS_SOCIAL_INTERACTION_ENABLE, true);
                    mBundle.putInt(KEY_SOCIAL_ACTION_TYPE, 1);
                    mDashboardDetailSocialInteractionFragment.setArguments(mBundle);
                    Picture picture = webGraph.capturePicture();
                    Bitmap b = Bitmap.createBitmap(picture.getWidth(),
                            picture.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas c = new Canvas(b);
                    picture.draw(c);
                    HomeActivity.mBitmap = b;
                    mHomeActivity.replace(mDashboardDetailSocialInteractionFragment, mContext.getResources().getString(R.string.tag_dashboard_detail));
                } else{
                    Toast.makeText(mHomeActivity, "Wait to load Graph", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.fragment_dashboard_detail_iv_share:

                if(mProgressBar.getVisibility() == View.GONE){
                    mHomeActivity.getmUserFilterData().clear();
                    mHomeActivity.getmLocationFilterData().clear();
                    mHomeActivity.getmLocationGroupForLGFilterData().clear();
                    mDashboardDetailSocialInteractionFragment = new DashboardDetailSocialInteractionFragment();
                    mBundle = new Bundle();
                    mBundle.putString(KEY_REPORT_ID, reportId);
                    mBundle.putString(KEY_REPORT_Name, reportName);
                    mBundle.putString(KEY_REPORT_TYPE, reportType);
                    mBundle.putBoolean(KEY_IS_SOCIAL_INTERACTION_ENABLE, true);
                    mBundle.putBoolean(KEY_IS_SOCIAL_INTERACTION_ENABLE, true);

                    Picture picture = webGraph.capturePicture();
                    Bitmap bm = Bitmap.createBitmap(picture.getWidth(),
                            picture.getHeight(), Bitmap.Config.ARGB_8888);
                    //bm.setWidth();
                    Canvas c = new Canvas(bm);
//                    c.drawBitmap(bm,bm.w);
                    picture.draw(c);

                    /*webGraph.measure(View.MeasureSpec.makeMeasureSpec(
                            View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED),
                            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                    webGraph.layout(0, 0, webGraph.getMeasuredWidth(),
                            webGraph.getMeasuredHeight());
                    webGraph.setDrawingCacheEnabled(true);
                    webGraph.buildDrawingCache();
                    Bitmap bm = Bitmap.createBitmap(webGraph.getMeasuredWidth(),
                            webGraph.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

                    Canvas bigcanvas = new Canvas(bm);
                    Paint paint = new Paint();
                    int iHeight = bm.getHeight();
                    bigcanvas.drawBitmap(bm, 0, iHeight, paint);*/


                    HomeActivity.mBitmap = Bitmap.createScaledBitmap(bm, bm.getWidth(), bm.getHeight(), true);
                    mBundle.putInt(KEY_SOCIAL_ACTION_TYPE, 3);
                    mDashboardDetailSocialInteractionFragment.setArguments(mBundle);
                    mHomeActivity.replace(mDashboardDetailSocialInteractionFragment, mContext.getResources().getString(R.string.tag_dashboard_detail));
                } else{
                    Toast.makeText(mHomeActivity, "Wait to load Graph", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.row_social_interaction_ll_tenant_type_filter:
                mHomeActivity.getmLocationFilterData().clear();
                mHomeActivity.getmUserFilterData().clear();
                mHomeActivity.getmLocationGroupForLGFilterData().clear();
                updateFilterTypeSelectionUIOnClick((String) llTenantType.getTag());
                break;
            case R.id.row_social_interaction_ll_location_type_filter:
                mHomeActivity.getmLocationGroupForLGFilterData().clear();
                mHomeActivity.getmUserFilterData().clear();
                updateFilterTypeSelectionUIOnClick((String) llLocationType.getTag());
                break;
            case R.id.row_social_interaction_ll_location_group_type_filter:
                mHomeActivity.getmLocationFilterData().clear();
                mHomeActivity.getmUserFilterData().clear();
                updateFilterTypeSelectionUIOnClick((String) llLocationGroupType.getTag());
                break;
            case R.id.row_social_interaction_ll_user_type_filter:
                mHomeActivity.getmLocationFilterData().clear();
                mHomeActivity.getmLocationGroupForLGFilterData().clear();
                updateFilterTypeSelectionUIOnClick((String) llCollegueType.getTag());
                break;
            case R.id.fragment_dashboard_detail_rv_filter_value_click:
                if(entityTypeTag == 1){
                    FragmentSocialInteractionLocationFilter mFragmentSocialInteractionLocationFilter = new FragmentSocialInteractionLocationFilter();
                    mBundle = new Bundle();
                    if(mHomeActivity.getmLocationFilterData().size() > 0){
                        mBundle.putBoolean(KEY_IS_NEED_FILTER_CALL, false);
                    } else{
                        mBundle.putBoolean(KEY_IS_NEED_FILTER_CALL, true);
                    }
                    mFragmentSocialInteractionLocationFilter.setArguments(mBundle);
                    mHomeActivity.replace(mFragmentSocialInteractionLocationFilter, mContext.getResources().getString(R.string.tag_fragment_social_interaction_location_filter));
                } else if(entityTypeTag == 2){
                    FragmentSocialInteractionLocationGroupFilter mFragmentSocialInteractionLocationGroupFilter = new FragmentSocialInteractionLocationGroupFilter();
                    mBundle = new Bundle();
                    if(mHomeActivity.getmLocationGroupForLGFilterData().size() > 0){
                        mBundle.putBoolean(KEY_IS_NEED_FILTER_CALL, false);
                    } else{
                        mBundle.putBoolean(KEY_IS_NEED_FILTER_CALL, true);
                    }
                    mFragmentSocialInteractionLocationGroupFilter.setArguments(mBundle);
                    mHomeActivity.replace(mFragmentSocialInteractionLocationGroupFilter, mContext.getResources().getString(R.string.tag_fragment_social_interaction_location_filter));
                } else if(entityTypeTag == 3){

                    FragmentSocialInteractionUserFilter mFragmentSocialInteractionUserFilter = new FragmentSocialInteractionUserFilter();
                    mBundle = new Bundle();
                    if(mHomeActivity.getmUserFilterData().size() > 0){
                        mBundle.putBoolean(KEY_IS_NEED_FILTER_CALL, false);
                    } else{
                        mBundle.putBoolean(KEY_IS_NEED_FILTER_CALL, true);
                    }
                    mFragmentSocialInteractionUserFilter.setArguments(mBundle);
                    mHomeActivity.replace(mFragmentSocialInteractionUserFilter, mContext.getResources().getString(R.string.tag_fragment_social_interaction_location_filter));
                }

                break;

        }
    }

    public void updateFilterTypeSelectionUIOnClick(String tag){

        switch(tag){
            case "1":
                entityTypeTag = 0;
                tvSelectedFilterType.setText(mContext.getResources().getString(R.string.tenant_preference));
                tvSelectedFilterValue.setText(InGaugeSession.read(getString(R.string.key_selected_tenant), ""));

                tvNote.setText(mContext.getResources().getString(R.string.social_interaction_tenant_note));
                ivTenantType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_selected_drawable));
                ivTenantType.setColorFilter(ContextCompat.getColor(mContext, R.color.pink_text_color_for_filter), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvTenantType.setTextColor(ContextCompat.getColor(mContext, R.color.black));

                ivLocationType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivLocationType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvLocationType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));


                ivLocationGroupType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivLocationGroupType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvLocationGroupType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                ivCollegueType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivCollegueType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvCollegueType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));


                break;
            case "2":
                entityTypeTag = 1;
                tvSelectedFilterType.setText(mContext.getResources().getString(R.string.location));

                if(!TextUtils.isEmpty(getLocationFilterSelectedValues())){
                    tvSelectedFilterValue.setText("");
                    tvSelectedFilterValue.setText(getLocationFilterSelectedValues());
                } else{
                    tvSelectedFilterValue.setText("Select Location");
                }

                tvNote.setText(mContext.getResources().getString(R.string.social_interaction_location_note));

                ivTenantType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivTenantType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvTenantType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                ivLocationType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_selected_drawable));
                ivLocationType.setColorFilter(ContextCompat.getColor(mContext, R.color.pink_text_color_for_filter), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvLocationType.setTextColor(ContextCompat.getColor(mContext, R.color.black));

                ivLocationGroupType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivLocationGroupType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvLocationGroupType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                ivCollegueType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivCollegueType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvCollegueType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                break;
            case "3":
                entityTypeTag = 2;
                tvSelectedFilterType.setText(mContext.getResources().getString(R.string.location_group));
                if(!TextUtils.isEmpty(getLocationGroupFilterValues())){
                    tvSelectedFilterValue.setText("");
                    tvSelectedFilterValue.setText(getLocationGroupFilterValues());
                } else{
                    tvSelectedFilterValue.setText("Select Location Group");
                }


                tvNote.setText(mContext.getResources().getString(R.string.social_interaction_location_group_note));
                ivTenantType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivTenantType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvTenantType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                ivLocationType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivLocationType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvLocationType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                ivLocationGroupType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_selected_drawable));
                ivLocationGroupType.setColorFilter(ContextCompat.getColor(mContext, R.color.pink_text_color_for_filter), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvLocationGroupType.setTextColor(ContextCompat.getColor(mContext, R.color.black));

                ivCollegueType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivCollegueType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvCollegueType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                break;
            case "4":
                entityTypeTag = 3;
                tvSelectedFilterType.setText(mContext.getResources().getString(R.string.user));
                if(!TextUtils.isEmpty(getUserFilterSelectedValues())){
                    tvSelectedFilterValue.setText("");
                    tvSelectedFilterValue.setText(getUserFilterSelectedValues());
                } else{
                    tvSelectedFilterValue.setText("Select User");
                }


                tvNote.setText(mContext.getResources().getString(R.string.social_interaction_colleague_note));
                ivTenantType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivTenantType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvTenantType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                ivLocationType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivLocationType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvLocationType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                ivLocationGroupType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_drawable));
                ivLocationGroupType.setColorFilter(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvLocationGroupType.setTextColor(ContextCompat.getColor(mContext, R.color.social_interaction_rounded_circle_dark_gray));

                ivCollegueType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.tenant_circle_selected_drawable));
                ivCollegueType.setColorFilter(ContextCompat.getColor(mContext, R.color.pink_text_color_for_filter), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvCollegueType.setTextColor(ContextCompat.getColor(mContext, R.color.black));

                break;
        }
    }


    private void createPdf(Bitmap bitmap){
        WindowManager wm = (WindowManager) mHomeActivity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        mHomeActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels;
        float width = displaymetrics.widthPixels;

        int convertHighet = (int) hight, convertWidth = (int) width;

        //        Resources mResources = getResources();
        //        Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.screenshot);

        PdfDocument document = new PdfDocument();

        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(webGraphTempTable.getWidth(), webGraphTempTable.getHeight(), 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();


        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#ffffff"));
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        document.finishPage(page);


        // write the document content
        //String targetPdf = "/sdcard/test.pdf";
        pdffile = new File(mHomeActivity.getApplication().getExternalFilesDir("InGauge") + "/report.pdf");

        Uri uri = Uri.fromFile(pdffile);

        //File filePath = new File(targetPdf);
        try{
            document.writeTo(new FileOutputStream(pdffile));

            mHomeActivity.endProgress();
            Intent intent;
            intent = new Intent(Intent.ACTION_SEND);
            intent.setType("application/pdf");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            try{
                startActivity(intent);
            } catch(ActivityNotFoundException e){
                // No application to view, ask to download one
                e.printStackTrace();
            }
            // btn_convert.setText("Check PDF");
            //boolean_save=true;
        } catch(IOException e){
            e.printStackTrace();
            Logger.Error("Something wrong: " + e.toString());

        }
        // close the document
        document.close();
    }

    public class LoadListener{
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processHTML(String html){
            //Logger.Error("Process Html " + html);
            htmlRaw = html;
            mHomeActivity.runOnUiThread(new Runnable(){
                @Override
                public void run(){
//stuff that updates ui
                    //webGraph.setVisibility(View.GONE);

                    String html = "<html><head><title>First parse</title></head>"
                            + "<body><p>Parsed HTML into a doc.</p></body></html>";
                    // org.jsoup.nodes.Document doc = Jsoup.parse(htmlRaw);
                    //Logger.Error(" HTML  " + doc.outerHtml());
                    /*webGraph.setVisibility(View.VISIBLE);
                    webGraphTemp.setVisibility(View.INVISIBLE);
                    webGraphTemp.loadDataWithBaseURL(null, htmlRaw.replace("<html style=\"height:", "<html "), "text/html", "utf-8", null);
                    writeFile("TempHtml", htmlRaw);*/

                    webGraphTable.setVisibility(View.INVISIBLE);
                    webGraphTempTable.setVisibility(View.INVISIBLE);
                    webGraphTempTable.loadDataWithBaseURL(null, htmlRaw.replace("<html style=\"height:", "<html "), "text/html", "utf-8", null);
                    writeFile("TempHtml", htmlRaw);
                }
            });


        }

    }

    public Boolean writeFile(String fname, String fcontent){
        try{

            String fpath = mHomeActivity.getApplication().getExternalFilesDir("InGauge") + "/" + fname + ".html";

            File file = new File(fpath);

            // If file does not exists, then create it
            if(!file.exists()){
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fcontent);
            bw.close();

            Log.d("Suceess", "Sucess");
            return true;

        } catch(IOException e){
            e.printStackTrace();
            return false;
        }

    }

    void getReportDetailFeeds(final int reportId, final boolean isLoadMore){

        if(isLoadMore)
            mProgressBarLoadMore.setVisibility(View.VISIBLE);
        int tenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        mCall = apiInterface.getReportDetailFeeds(tenantId, reportId, first, total);
        mCall.enqueue(new Callback<ReportDetailFeedsPojo>(){
            @Override
            public void onResponse(Call<ReportDetailFeedsPojo> call, Response<ReportDetailFeedsPojo> response){
                if(mProgressBarLoadMore.getVisibility() == View.VISIBLE)
                    mProgressBarLoadMore.setVisibility(View.GONE);
                if(response.code() == 200){
                    ReportDetailFeedsPojo mReportDetailFeedsPojo = (ReportDetailFeedsPojo) response.body();
                    mDatumBaseList.addAll(mReportDetailFeedsPojo.getData());
                    if(mReportDetailFeedsPojo.getData() != null && mReportDetailFeedsPojo.getData().size() > 0){
                        if(isLoadMore){
                            isLoading=false;
                            if(mReportDetailsFeedsAdapter != null){
                                mReportDetailsFeedsAdapter.notifyDataSetChanged();
                            }

                        } else{
                            mReportDetailsFeedsAdapter = new ReportDetailsFeedsAdapter(mHomeActivity, mDatumBaseList);
                            mListViewDashboardDetailFeedsList.setAdapter(mReportDetailsFeedsAdapter);
                        }
                    } else{
                        isLoading=true;
                    }


                }


            }

            @Override
            public void onFailure(Call<ReportDetailFeedsPojo> call, Throwable t){

            }
        });
    }

    void getDashboardCustomResport(final int reporotId){

        llTableReport.setVisibility(View.GONE);
        mProgressBarForTable.setVisibility(View.VISIBLE);
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype), -2);
        int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region), -2);
        int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product), -2));
        int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user), -2));
        int TenantLocationId = (InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
        int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group), -2);
//        String MatrixDataType =
        String MatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mContext.getResources().getStringArray(R.array.metric_type_array)[1]);
        if(MatrixDataType.equalsIgnoreCase("Check out")){
            MatrixDataType = "Arrival";
        } else if(MatrixDataType.equalsIgnoreCase("Check in")){
            MatrixDataType = "Departure";
        } else if(MatrixDataType.equalsIgnoreCase("Daily")){
            MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
        } else{
            if(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                MatrixDataType = "Arrival";
            }

        }

        String FromDate = "";
        String ToDate = "";
        String CompFromDate = "";
        String CompToDate = "";
        boolean IsCompare = false;
        if(isDashboardMTD){
            FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
            ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");

            try{
                Date fromDateDate = sdfServer.parse(FromDate);
                FromDate = DateTimeUtils.getFirstDay(fromDateDate);
                Date TomDateDate = sdfServer.parse(ToDate);
                ToDate = DateTimeUtils.getLastDay(TomDateDate);
            } catch(Exception e){
                e.printStackTrace();
            }
            CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
            CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
            IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), true);

        } else{
            FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
            ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
            CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
            CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
            IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), true);
        }

        int LoginUserId = InGaugeSession.read(mContext.getString(R.string.key_user_id), -2);

        //String Extra Date parametrers
        String EXTRAFromDate = FromDate;
        String EXTRAToDate = ToDate;
        String EXTRACompFromDate = CompFromDate;
        String EXTRACompToDate = CompToDate;
        String exFromDate = "";
        String exToDate = "";
        String exCompFromDate = "";
        String exCompToDate = "";
        try{
            Calendar fromcalendar = UiUtils.DateToCalendar(sdfServer.parse(EXTRAFromDate));
            fromcalendar.add(Calendar.HOUR, 00);
            fromcalendar.add(Calendar.MINUTE, 00);
            fromcalendar.add(Calendar.SECOND, 00);
            fromcalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            exFromDate = String.valueOf(fromcalendar.getTimeInMillis());


            Calendar toCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(EXTRAToDate + " 23:59:59"));
            /*fromcalendar.add(Calendar.HOUR,23);
            fromcalendar.add(Calendar.MINUTE,59);
            fromcalendar.add(Calendar.SECOND,59);*/
            toCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            exToDate = String.valueOf(toCalendar.getTimeInMillis());

            Calendar fromCompCalendar = UiUtils.DateToCalendar(sdfServer.parse(EXTRACompFromDate));
            fromcalendar.add(Calendar.HOUR, 00);
            fromcalendar.add(Calendar.MINUTE, 00);
            fromcalendar.add(Calendar.SECOND, 00);
            fromCompCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            exCompFromDate = String.valueOf(fromCompCalendar.getTimeInMillis());

            Calendar toCompCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(EXTRACompToDate + " 23:59:59"));
            /*fromcalendar.add(Calendar.HOUR,23);
            fromcalendar.add(Calendar.MINUTE,59);
            fromcalendar.add(Calendar.SECOND,59);*/
            toCompCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            exCompToDate = String.valueOf(toCompCalendar.getTimeInMillis());


        } catch(ParseException e){
            e.printStackTrace();
        }

        mCall = apiInterface.getDashboardTableReport(
                reporotId,
                TenantId,
                RegionTypeId,
                RegionId,
                ProductId,
                UserId,
                TenantLocationId,
                LocationGroupId,
                MatrixDataType,
                FromDate,
                ToDate,
                CompFromDate,
                CompToDate,
                exFromDate,
                exToDate,
                exCompFromDate,
                exCompToDate,
                IsCompare, LoginUserId);
        mCall.enqueue(new Callback<ResponseBody>(){

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                try{
                    llTableReport.setVisibility(View.VISIBLE);
                    mProgressBarForTable.setVisibility(View.GONE);

                    //JSONObject responseJson = new JSONObject(response.body().string());
                    String jsonString = response.body().string();


                    JSONObject mJsonObject = new JSONObject(jsonString);
                    JSONObject tableHeadersJson = mJsonObject.getJSONObject("data").optJSONObject("tableHeaders");
                    JSONArray tableDataJson = mJsonObject.getJSONObject("data").optJSONArray("tableDatas");
                    JSONObject mJsonSocialINteractionEnabled = mJsonObject.getJSONObject("data").optJSONObject("socialInteractionEnabledColumns");

                    pairs = new LinkedHashMap<Integer, LinkedHashMap<String, String>>();
                    for(int i = 0; i < tableDataJson.length(); i++){
                        JSONObject j = tableDataJson.optJSONObject(i);
                        Iterator it = j.keys();
                        LinkedHashMap<String, String> hashMapInner = new LinkedHashMap<String, String>();
                        while(it.hasNext()){

                            String n = (String) it.next();
                            hashMapInner.put(n, j.getString(n));
                            pairs.put(i, hashMapInner);
                        }
                    }
                    Iterator iterator = tableHeadersJson.keys();
                    int index = 0;
                    while(iterator.hasNext()){
                        TableReportColumnPojo mTableReportColumnPojo = new TableReportColumnPojo();
                        String key = (String) iterator.next();
                        mKeysString.add(key);
                        mValueKeysString.add(tableHeadersJson.optString(key));
                        mColumnHeader.add(tableHeadersJson.optString(key));
                        mTableReportColumnPojo.setColumnId(index);
                        mTableReportColumnPojo.setColumnKey(key);
                        mTableReportColumnPojo.setColumnName(tableHeadersJson.optString(key));
                        if(index == 0 || index == 1 || index == 2){
                            mTableReportColumnPojo.setSelected(true);
                        }
                        mTableReportColumnPojos.add(mTableReportColumnPojo);
                        index++;
                    }

                    String key1 = "";
                    String key2 = "";
                    String key3 = "";

                    String header1 = "";
                    String header2 = "";
                    String header3 = "";
                    boolean isAbcd = false;
                    if(mValueKeysString.size() > 0){
                        for(int i = 0; i < mValueKeysString.size(); i++){
                            switch(i){
                                case 0:
                                    key1 = mValueKeysString.get(0);
                                    break;
                                case 1:
                                    key2 = mValueKeysString.get(1);
                                    break;
                                case 2:
                                    key3 = mValueKeysString.get(2);
                                    break;
                            }
                        }
                    }
                    if(mKeysString.size() > 0){
                        for(int i = 0; i < mKeysString.size(); i++){
                            switch(i){
                                case 0:
                                    header1 = mKeysString.get(0);
                                    break;
                                case 1:
                                    header2 = mKeysString.get(1);
                                    break;
                                case 2:
                                    header3 = mKeysString.get(2);
                                    break;
                            }
                        }
                    }
                    tvHeaderOne.setText(header1);
                    tvHeaderTwo.setText(header2);
                    tvHeaderThree.setText(header3);
                    List<TableReportExpandItemPojo> mTableReportExpandItemPojos = new ArrayList<>();
                    for(int i = 0; i < pairs.size(); i++){
                        mTableReportExpandItemPojos = new ArrayList<>();
                        String columnValueOne = "";

                        if(((HashMap<String, String>) pairs.get(i)).get(key1) == null){
                            columnValueOne = "";
                        } else{
                            columnValueOne = ((HashMap<String, String>) pairs.get(i)).get(key1).toString();
                        }
                        String columnValueTwo = "";
                        if(((HashMap<String, String>) pairs.get(i)).get(key2) == null){
                            columnValueTwo = "";
                        } else{
                            columnValueTwo = ((HashMap<String, String>) pairs.get(i)).get(key2).toString();
                        }
                        String columnValueThree = "";

                        if(((HashMap<String, String>) pairs.get(i)).get(key3) == null){
                            columnValueThree = "";
                        } else{
                            columnValueThree = ((HashMap<String, String>) pairs.get(i)).get(key3).toString();
                        }

                        getTableList(columnValueOne, columnValueTwo, columnValueThree);

                        for(int j = 0; j < mValueKeysString.size(); j++){
                            TableReportExpandItemPojo mTableReportExpandItemPojo = new TableReportExpandItemPojo();
                            mTableReportExpandItemPojo.reportId = mValueKeysString.get(j);
                            mTableReportExpandItemPojo.reportNameDisplay = mKeysString.get(j);
                            mTableReportExpandItemPojo.reportIdForServer = mValueKeysString.get(j);
                            Logger.Error("<<<< Report ID >>>>" + mTableReportExpandItemPojo.reportId);
                            Logger.Error("<<<< Report Name >>>>" + mKeysString.get(j));
                            String checkConcatReportNameValue = "";
                            if(((HashMap<String, String>) pairs.get(i)).get("concatWithReportName") == null){
                                checkConcatReportNameValue = "";
                            } else{
                                checkConcatReportNameValue = ((HashMap<String, String>) pairs.get(i)).get("concatWithReportName").toString();
                            }

                            String startDateCheck = "dataStartDate" + mKeysString.get(j);
                            String endDateCheck = "dataEndDate" + mKeysString.get(j);
                            mTableReportExpandItemPojo.reportName = mKeysString.get(j);
                            mTableReportExpandItemPojo.reportNameForServer = mKeysString.get(j);
                            mTableReportExpandItemPojo.reportIdForServer = mValueKeysString.get(j);

                            if(checkConcatReportNameValue.length() > 0){
                                mTableReportExpandItemPojo.reportId = "";
                                mTableReportExpandItemPojo.reportId = checkConcatReportNameValue + mValueKeysString.get(j);
                                mTableReportExpandItemPojo.reportIdForServer = "";
                                mTableReportExpandItemPojo.reportIdForServer = checkConcatReportNameValue + " " + mValueKeysString.get(j);
                                mTableReportExpandItemPojo.reportName = "";
                                mTableReportExpandItemPojo.reportName = checkConcatReportNameValue + mKeysString.get(j);
                                mTableReportExpandItemPojo.reportNameForServer = "";
                                mTableReportExpandItemPojo.reportNameForServer = checkConcatReportNameValue + " " + mKeysString.get(j);
                            }
                            if(((HashMap<String, String>) pairs.get(i)).get(mValueKeysString.get(j)) == null){
                                mTableReportExpandItemPojo.reportValue = "";
                            } else{
                                mTableReportExpandItemPojo.reportValue = ((HashMap<String, String>) pairs.get(i)).get(mValueKeysString.get(j)).toString();
                            }

                            Logger.Error("<<<< Report Value >>>>" + mTableReportExpandItemPojo.reportValue);
                            if(((HashMap<String, String>) pairs.get(i)).get("entityId") == null){
                                mTableReportExpandItemPojo.entityId = null;
                            } else{
                                mTableReportExpandItemPojo.entityId = ((HashMap<String, String>) pairs.get(i)).get("entityId").toString();
                            }

                            if(((HashMap<String, String>) pairs.get(i)).get("entityName") == null){
                                mTableReportExpandItemPojo.entityName = "";
                            } else{
                                mTableReportExpandItemPojo.entityName = ((HashMap<String, String>) pairs.get(i)).get("entityName").toString();
                            }


                            if(((HashMap<String, String>) pairs.get(i)).get("entityType") == null){
                                mTableReportExpandItemPojo.entityType = "";
                            } else{
                                mTableReportExpandItemPojo.entityType = ((HashMap<String, String>) pairs.get(i)).get("entityType").toString();
                            }

                            if(((HashMap<String, String>) pairs.get(i)).get("dataStartDate") == null){
                                mTableReportExpandItemPojo.dataStartDate = "";
                            } else{
                                mTableReportExpandItemPojo.dataStartDate = ((HashMap<String, String>) pairs.get(i)).get("dataStartDate").toString();
                            }
                            if(((HashMap<String, String>) pairs.get(i)).get("dataEndDate") == null){
                                mTableReportExpandItemPojo.dataEndDate = "";
                            } else{
                                mTableReportExpandItemPojo.dataEndDate = ((HashMap<String, String>) pairs.get(i)).get("dataEndDate").toString();
                            }
                            if(((HashMap<String, String>) pairs.get(i)).get("compDataStartDate ") == null){
                                mTableReportExpandItemPojo.compDataStartDate = "";
                            } else{
                                mTableReportExpandItemPojo.compDataStartDate = ((HashMap<String, String>) pairs.get(i)).get("compDataStartDate ").toString();
                            }

                            if(((HashMap<String, String>) pairs.get(i)).get("compDataEndDate") == null){
                                mTableReportExpandItemPojo.compDataEndDate = "";
                            } else{
                                mTableReportExpandItemPojo.compDataEndDate = ((HashMap<String, String>) pairs.get(i)).get("compDataEndDate").toString();
                            }

                            String startDateCheckValue = "";
                            String endDateCheckValue = "";
                            if(((HashMap<String, String>) pairs.get(i)).get(startDateCheck) == null){
                                startDateCheckValue = "";
                            } else{
                                startDateCheckValue = ((HashMap<String, String>) pairs.get(i)).get(startDateCheck);
                            }
                            if(((HashMap<String, String>) pairs.get(i)).get(endDateCheck) == null){
                                endDateCheckValue = "";
                            } else{
                                endDateCheckValue = ((HashMap<String, String>) pairs.get(i)).get(endDateCheck);
                            }

                            mTableReportExpandItemPojo.startDateCheck = startDateCheckValue;
                            mTableReportExpandItemPojo.endDateCheck = endDateCheckValue;
                            mTableReportExpandItemPojo.isSocialInteractionEnabled = mJsonSocialINteractionEnabled.optBoolean(mValueKeysString.get(j));
                            mTableReportExpandItemPojos.add(mTableReportExpandItemPojo);
                        }
                        map.put(i, mTableReportExpandItemPojos);
                    }

                    Logger.Error("<<<< Child Size >>> " + mTableReportExpandItemPojos.size());
                    if(mTablePojos != null && mTablePojos.size() > 0){
                        mTableReportAdapter = new TableReportAdapter(mHomeActivity, mTablePojos, true, true, true, map, reportId, false);
                        lvtableReport.setAdapter(mTableReportAdapter);
                        Logger.Error("<<< Table List >>>>>" + mTablePojos.size());
                        lvtableReport.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);

                    } else{
                        lvtableReport.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                    }


                } catch(JSONException e){
                    e.printStackTrace();
                    lvtableReport.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                } catch(IOException e){
                    e.printStackTrace();
                    lvtableReport.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                } catch(Exception e){
                    e.printStackTrace();
                    lvtableReport.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){
                llTableReport.setVisibility(View.VISIBLE);
                lvtableReport.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
                mProgressBarForTable.setVisibility(View.GONE);
            }
        });

    }

    public List<TablePojo> getTableList(String columnNameone, String columnNameTwo, String columnThree){
        TablePojo mTablePojo = new TablePojo(columnNameone, columnNameTwo, columnThree);
        mTablePojos.add(mTablePojo);
        return mTablePojos;
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.VISIBLE);
    }

    interface GraphLoadComplete{
        void visibleGraph();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void ShareFeedApi(final String authToken, final ShareReportPojo mShareReportPojo){
        mHomeActivity.startProgress(mContext);
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype), -2);
        int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region), -2);
        int TenantLocationId = (InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
        int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group), -2);
        int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product), -2));
        int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user), -2));
        int LoginUserId = InGaugeSession.read(mContext.getString(R.string.key_user_id), -2);
        String FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
        String ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
        String CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
        String CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
        String MatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_metric_type_name), mContext.getResources().getStringArray(R.array.metric_type_array)[1]);

        boolean IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), true);
        int IndustryId = InGaugeSession.read(mContext.getString(R.string.key_selected_industry_id), 1);

        mCall = apiInterfaceForShareReport.shareFeed(Integer.parseInt(reportId), TenantId, RegionTypeId, RegionId, TenantLocationId, LocationGroupId, ProductId, UserId, FromDate, ToDate, IsCompare, CompFromDate, CompToDate, MatrixDataType, IndustryId, LoginUserId, authToken, mShareReportPojo);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                mHomeActivity.endProgress();


                Bundle bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putInt("reportId", Integer.parseInt(reportId));
                if(mShareReportPojo != null){
                    if(!TextUtils.isEmpty(mShareReportPojo.getDescription()))
                        bundle.putString("comment", mShareReportPojo.getDescription());
                }
                InGaugeApp.getFirebaseAnalytics().logEvent("share_report", bundle);
                mHomeActivity.onBackPressed();
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }


}
