package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.DashboardPanelAdapter;
import com.ingauge.pojo.DashboardModelList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 28-Aug-17.
 */

public class DashboardPanelFragment extends BaseFragment {

    HomeActivity mHomeActivity;
    View rootView;
    private RecyclerView mRecyclerViewListPanel;

    private GridLayoutManager lLayout;

    public static Fragment newInstance(String text, int color) {
        Fragment frag = new DashboardPanelFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
//        args.putString(ARG_TEXT, text);
//        args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHomeActivity = (HomeActivity)context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dashboard_panel,container,false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        mRecyclerViewListPanel = (RecyclerView)rootView.findViewById(R.id.fragment_dashboard_panel_rv);
        lLayout = new GridLayoutManager(mHomeActivity, 2);
        mRecyclerViewListPanel.setHasFixedSize(true);
        mRecyclerViewListPanel.setLayoutManager(lLayout);
        List<DashboardModelList.Datum> mData = new ArrayList<>();
        //DashboardPanelAdapter rcAdapter = new DashboardPanelAdapter(mHomeActivity, mData);
        //mRecyclerViewListPanel.setAdapter(rcAdapter);
    }
}
