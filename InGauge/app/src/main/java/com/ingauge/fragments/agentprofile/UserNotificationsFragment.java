package com.ingauge.fragments.agentprofile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.UserNotificationListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.UserNotificationListModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by mansurum on 04-Apr-18.
 */

public class UserNotificationsFragment extends Fragment{

    public static String KEY_USERID = "user_id";

    private HomeActivity mHomeActivity;
    private View rootView;
    private StickyListHeadersListView mStickyListHeadersListView;
    private UserNotificationListAdapter mUserNotificationListAdapter;
    private APIInterface apiInterface;
    private int userId;
    int first = 0;
    int total = 35;
    boolean isLoading = false;
    ProgressBar mLoadMoreProgressBar;

    List<UserNotificationListModel.Datum> mDatumArrayList = new ArrayList<>();

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_user_notifications_list, container, false);
        mStickyListHeadersListView = (StickyListHeadersListView) rootView.findViewById(R.id.fragment_user_notifications_list);
        mLoadMoreProgressBar = (ProgressBar)rootView.findViewById(R.id.fragment_user_notifications_pb);

        if(getArguments() != null){
            userId = getArguments().getInt(KEY_USERID);
            mHomeActivity.startProgress(mHomeActivity);
            getNotificationList(userId, first, total, isLoading);
        }
        mStickyListHeadersListView.setOnScrollListener(new AbsListView.OnScrollListener(){
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState){

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount){
                int lastIndexInScreen = visibleItemCount + firstVisibleItem;
                // It is time to load more items

                if(lastIndexInScreen >= totalItemCount && !isLoading){

                    // It is time to load more items

                    isLoading = true;
                    first = totalItemCount + first;

                    getNotificationList(userId, first, total, isLoading);
                    //    loadMore();


                }
            }
        });
        return rootView;
    }


    public void getNotificationList(int userId, int first, int total, final boolean isLoadMore){
        if(isLoadMore)
            mLoadMoreProgressBar.setVisibility(View.VISIBLE);
        Call mCall = apiInterface.getUserNotificationList(userId, first, total, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(mLoadMoreProgressBar.getVisibility()==View.VISIBLE)
                    mLoadMoreProgressBar.setVisibility(View.GONE);
                mHomeActivity.endProgress();
                UserNotificationListModel userdata = (UserNotificationListModel) response.body();
                mDatumArrayList.addAll(userdata.getData());
                if(userdata.getData()!=null && userdata.getData().size()>0){
                    if(isLoadMore){

                        isLoading=false;
                        if(mUserNotificationListAdapter != null){
                            mUserNotificationListAdapter.notifyDataSetChanged();
                        }

                    } else{
                        mUserNotificationListAdapter = new UserNotificationListAdapter(mHomeActivity, mDatumArrayList);
                        mStickyListHeadersListView.setAdapter(mUserNotificationListAdapter);
                    }
                }else{
                    isLoading=true;
                }


            }

            @Override
            public void onFailure(Call call, Throwable t){
                mLoadMoreProgressBar.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.tvTitle.setText("Timeline");
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibBack.setVisibility(View.VISIBLE);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
    }
}
