package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.pojo.DynamicData;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

;

/**
 * Created by mansurum on 13-Oct-17.
 */

public class TempFragment extends Fragment {


    HomeActivity mHomeActivity;

    List<DynamicData> mCountryList;

    JSONObject mDataJson = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;

        mCountryList = new ArrayList<>();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String jsonResponse = UiUtils.loadJSONFromAsset("getDynamicFilter.json", mHomeActivity);

        //    Gson gson = new GsonBuilder().registerTypeAdapter(DynamicJson.class, new ResultDeserializer()).create();

        //DynamicJson results = gson.fromJson(jsonResponse, DynamicJson.class);
        try {
            JSONObject mJsonObject = new JSONObject(jsonResponse);
            mDataJson = mJsonObject.optJSONObject("data");
            JSONArray firstJson = mDataJson.optJSONArray("-1");
            //Map<Integer,JSONArray> integerJSONArrayMap  = new HashMap<>();

            Map<Integer, List<DynamicData>> integerListHashMap = new LinkedHashMap<>();
            int level = 0;
            for (int i = 0; i < firstJson.length(); i++) {
                DynamicData mDynamicData = new DynamicData();
                mDynamicData.setId(firstJson.getJSONObject(i).optInt("id"));
                mDynamicData.setName(firstJson.getJSONObject(i).optString("name"));
                mDynamicData.setRegionTypeName(firstJson.getJSONObject(i).optString("regionTypeName"));
                mDynamicData.setRegionTypeId(firstJson.getJSONObject(i).optInt("regionTypeId"));
                mDynamicData.setChildRegion(firstJson.getJSONObject(i).optString("childRegion"));
                mCountryList.add(mDynamicData);
            }
            integerListHashMap.put(level, mCountryList);
            for (DynamicData mDynamicDataParent : mCountryList) {

                if (mDynamicDataParent.getChildRegion() != null && mDynamicDataParent.getChildRegion().length() > 0)
                    searchParent(mDynamicDataParent, integerListHashMap, level);
            }

            Logger.Error("<<< Map >>>>" + integerListHashMap);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void searchParent(DynamicData mDynamicDataParent, Map<Integer, List<DynamicData>> integerJSONArrayMap, int level) {
        try {
            Logger.Error("MAp" + integerJSONArrayMap);
            if (mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId())) != null) {
                level++;
                JSONArray mJsonArray = mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId()));
                for(int i=0;i< mJsonArray.length();i++){
                    DynamicData mDynamicData = new DynamicData();
                    mDynamicData.setId(mJsonArray.getJSONObject(i).optInt("id"));
                    mDynamicData.setName(mJsonArray.getJSONObject(i).optString("name"));
                    mDynamicData.setRegionTypeName(mJsonArray.getJSONObject(i).optString("regionTypeName"));
                    mDynamicData.setRegionTypeId(mJsonArray.getJSONObject(i).optInt("regionTypeId"));
                    mDynamicData.setChildRegion(mJsonArray.getJSONObject(i).optString("childRegion"));

                    if(integerJSONArrayMap.get(level)!=null){
                        List<DynamicData>  mDynamicDataList = integerJSONArrayMap.get(level);
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level,mDynamicDataList);

                    }else{
                        List<DynamicData>  mDynamicDataList = new ArrayList<>();
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level,mDynamicDataList);
                    }
                    searchParent(mDynamicData, integerJSONArrayMap, level);
                    //mCountryList.add(mDynamicData);
                }
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }








    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.temp_fragment, container, false);
    }
}
