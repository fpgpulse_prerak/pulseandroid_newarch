package com.ingauge.fragments.leaderboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.TableReportAdapterForLeader;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.fragments.agentprofile.AgentProfileFragment;
import com.ingauge.listener.LeaderDetailsListClickListener;
import com.ingauge.pojo.TablePojo;
import com.ingauge.pojo.TableReportColumnPojo;
import com.ingauge.pojo.TableReportExpandItemPojo;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 02-Feb-18.
 */

public class LeaderboardDetailFragment extends BaseFragment implements LeaderDetailsListClickListener, ExpandableListView.OnGroupClickListener{


    public static String KEY_REPORT_ID = "report_id";
    public static String KEY_REPORT_Name = "report_name";

    APIInterface apiInterface;

    HomeActivity mHomeActivity;
    View rootView;


    private ProgressBar mProgressBarForTable;
    private TextView tvHeaderOne, tvHeaderTwo, tvHeaderThree;
    private ExpandableListView lvtableReport;

    private TableReportAdapterForLeader mTableReportAdapter;

    Call mCall;
    List<TablePojo> mTablePojos = new ArrayList<>();
    LinkedHashMap<Integer, LinkedHashMap<String, String>> pairs = new LinkedHashMap<>();
    List<String> mValueKeysString = new ArrayList<>();
    List<String> mKeysString = new ArrayList<>();
    List<String> mColumnHeader = new ArrayList<>();
    ArrayList<TableReportColumnPojo> mTableReportColumnPojos = new ArrayList<>();
    boolean isFirstColumnFilled = false;
    boolean isSecondColumnFilled = false;
    boolean isthirdColumnFilled = false;
    boolean isPermissionGranted = false;
    private TextView tvNoData;
    List<String> mstaticAddKeysString = new ArrayList<>();


    Map<Integer, List<TableReportExpandItemPojo>> map = new LinkedHashMap();


    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    SimpleDateFormat sdfServerDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

    private int reportId;
    private String reportName = "";
    LeaderDetailsListClickListener mLeaderDetailsListClickListener;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        if(getArguments() != null){
            reportId = getArguments().getInt(KEY_REPORT_ID);
            reportName = getArguments().getString(KEY_REPORT_Name);
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mLeaderDetailsListClickListener = this;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_leader_board_details, container, false);
            tvNoData = (TextView) rootView.findViewById(R.id.fragment_dashboard_detail_tv_no_data);
            lvtableReport = (ExpandableListView) rootView.findViewById(R.id.fragment_dashboard_detail_lv_table_report);
            lvtableReport.setOnGroupClickListener(this);
            mProgressBarForTable = (ProgressBar) rootView.findViewById(R.id.fragmnet_entity_pb);
            apiInterface = APIClient.getClient().create(APIInterface.class);
            tvHeaderOne = (TextView) rootView.findViewById(R.id.fragment_dashboard_detail_tv_header_one);
            tvHeaderTwo = (TextView) rootView.findViewById(R.id.fragment_dashboard_detail_tv_header_two);
            tvHeaderThree = (TextView) rootView.findViewById(R.id.fragment_dashboard_detail_tv_header_three);
            getDashboardCustomResport(reportId);
        } else{
            if(rootView != null && rootView.getParent() != null){
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }
        }

        return rootView;
    }


    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_title)));
        mHomeActivity.tvTitle.setText(reportName);

        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
    }

    void getDashboardCustomResport(final int reporotId){

        mProgressBarForTable.setVisibility(View.VISIBLE);
        int TenantId = InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = mHomeActivity.getSelectedRegionTypeForLb();
        int RegionId = mHomeActivity.getSelectedRegionIdForLb();
        int ProductId = mHomeActivity.getSelectedProductIdForLb();
        int UserId = mHomeActivity.getSelectedUserIdForLB();
        int TenantLocationId = mHomeActivity.getSelectedTenantLocationIdForLb();
        int LocationGroupId = mHomeActivity.getSelectedLocationGroupIdForLb();


        String MatrixDataType = "";
        if(mHomeActivity.getSelectedMetricTypeNameForLb().length() > 0){
            MatrixDataType = mHomeActivity.getSelectedMetricTypeNameForLb();
            if(MatrixDataType.equalsIgnoreCase("Check out")){
                MatrixDataType = "Arrival";
            } else if(MatrixDataType.equalsIgnoreCase("Check in")){
                MatrixDataType = "Departure";
            } else if(MatrixDataType.equalsIgnoreCase("Daily")){
                MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
            } else{
                if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                    MatrixDataType = "Arrival";
                }

            }
        } else{
            MatrixDataType = mHomeActivity.getResources().getStringArray(R.array.metric_type_array)[1];


            if(MatrixDataType.equalsIgnoreCase("Check out")){
                MatrixDataType = "Arrival";
            } else if(MatrixDataType.equalsIgnoreCase("Check in")){
                MatrixDataType = "Departure";
            } else if(MatrixDataType.equalsIgnoreCase("Daily")){
                MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
            } else{
                if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                    MatrixDataType = "Arrival";
                }

            }
        }


        String FromDate = "";
        String ToDate = "";
        String CompFromDate = "";
        String CompToDate = "";
        boolean IsCompare = false;

        FromDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_start_date_serverfor_lb), "");
        ToDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_end_date_server_for_lb), "");
        CompFromDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_compare_start_date_server), "");
        CompToDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_compare_end_date_server), "");
        IsCompare = false;


        int LoginUserId = InGaugeSession.read(mHomeActivity.getString(R.string.key_user_id), -2);

        //String Extra Date parametrers
        String EXTRAFromDate = FromDate;
        String EXTRAToDate = ToDate;
        String EXTRACompFromDate = CompFromDate;
        String EXTRACompToDate = CompToDate;
        String exFromDate = "";
        String exToDate = "";
        String exCompFromDate = "";
        String exCompToDate = "";
        try{
            Calendar fromcalendar = UiUtils.DateToCalendar(sdfServer.parse(EXTRAFromDate));
            fromcalendar.add(Calendar.HOUR, 00);
            fromcalendar.add(Calendar.MINUTE, 00);
            fromcalendar.add(Calendar.SECOND, 00);
            fromcalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            exFromDate = String.valueOf(fromcalendar.getTimeInMillis());


            Calendar toCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(EXTRAToDate + " 23:59:59"));
            /*fromcalendar.add(Calendar.HOUR,23);
            fromcalendar.add(Calendar.MINUTE,59);
            fromcalendar.add(Calendar.SECOND,59);*/
            toCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            exToDate = String.valueOf(toCalendar.getTimeInMillis());

            Calendar fromCompCalendar = UiUtils.DateToCalendar(sdfServer.parse(EXTRACompFromDate));
            fromcalendar.add(Calendar.HOUR, 00);
            fromcalendar.add(Calendar.MINUTE, 00);
            fromcalendar.add(Calendar.SECOND, 00);
            fromCompCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            exCompFromDate = String.valueOf(fromCompCalendar.getTimeInMillis());

            Calendar toCompCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(EXTRACompToDate + " 23:59:59"));
            /*fromcalendar.add(Calendar.HOUR,23);
            fromcalendar.add(Calendar.MINUTE,59);
            fromcalendar.add(Calendar.SECOND,59);*/
            toCompCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            exCompToDate = String.valueOf(toCompCalendar.getTimeInMillis());


        } catch(ParseException e){
            e.printStackTrace();
        }

        mCall = apiInterface.getDashboardTableReport(
                reporotId,
                TenantId,
                RegionTypeId,
                RegionId,
                ProductId,
                UserId,
                TenantLocationId,
                LocationGroupId,
                MatrixDataType,
                FromDate,
                ToDate,
                CompFromDate,
                CompToDate,
                exFromDate,
                exToDate,
                exCompFromDate,
                exCompToDate,
                IsCompare, LoginUserId);
        mCall.enqueue(new Callback<ResponseBody>(){

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                try{

                    mProgressBarForTable.setVisibility(View.GONE);

                    //JSONObject responseJson = new JSONObject(response.body().string());
                    String jsonString = response.body().string();


                    JSONObject mJsonObject = new JSONObject(jsonString);
                    JSONObject tableHeadersJson = mJsonObject.getJSONObject("data").optJSONObject("tableHeaders");
                    JSONArray tableDataJson = mJsonObject.getJSONObject("data").optJSONArray("tableDatas");
                    JSONObject mJsonSocialINteractionEnabled = mJsonObject.getJSONObject("data").optJSONObject("socialInteractionEnabledColumns");

                    pairs = new LinkedHashMap<Integer, LinkedHashMap<String, String>>();
                    for(int i = 0; i < tableDataJson.length(); i++){
                        JSONObject j = tableDataJson.optJSONObject(i);
                        Iterator it = j.keys();
                        LinkedHashMap<String, String> hashMapInner = new LinkedHashMap<String, String>();
                        while(it.hasNext()){

                            String n = (String) it.next();
                            hashMapInner.put(n, j.getString(n));
                            pairs.put(i, hashMapInner);
                        }
                    }
                    Iterator iterator = tableHeadersJson.keys();
                    // int index = 0;
                    while(iterator.hasNext()){
                        String key = (String) iterator.next();
                        if(!(key.equalsIgnoreCase("Location") || key.equalsIgnoreCase("Region")
                                || key.equalsIgnoreCase("Rank") || key.equalsIgnoreCase("User"))){
                            mKeysString.add(key);

                        }

                    }
                    mstaticAddKeysString.add("Rank");
                    mstaticAddKeysString.add("User");
                    if(mKeysString != null && mKeysString.size() > 0){
                        mstaticAddKeysString.add(mKeysString.get(0));
                    }

                    for(int j = 0; j < mstaticAddKeysString.size(); j++){
                        TableReportColumnPojo mTableReportColumnPojo = new TableReportColumnPojo();

                        mValueKeysString.add(tableHeadersJson.optString(mstaticAddKeysString.get(j)));
                        mColumnHeader.add(tableHeadersJson.optString(mstaticAddKeysString.get(j)));
                        mTableReportColumnPojo.setColumnId(j);
                        mTableReportColumnPojo.setColumnKey(mstaticAddKeysString.get(j));
                        mTableReportColumnPojo.setColumnName(tableHeadersJson.optString(mstaticAddKeysString.get(j)));
                        if(j == 0 || j == 1 || j == 2){
                            mTableReportColumnPojo.setSelected(true);
                        }
                        mTableReportColumnPojos.add(mTableReportColumnPojo);

                    }
                    //mstaticAddKeysString
                    String key1 = "";
                    String key2 = "";
                    String key3 = "";

                    String header1 = "";
                    String header2 = "";
                    String header3 = "";
                    boolean isAbcd = false;
                    if(mValueKeysString.size() > 0){
                        for(int i = 0; i < mValueKeysString.size(); i++){
                            switch(i){
                                case 0:
                                    key1 = mValueKeysString.get(0);
                                    break;
                                case 1:
                                    key2 = mValueKeysString.get(1);
                                    break;
                                case 2:
                                    key3 = mValueKeysString.get(2);
                                    break;
                            }
                        }
                    }
                    if(mstaticAddKeysString.size() > 0){
                        for(int i = 0; i < mstaticAddKeysString.size(); i++){
                            switch(i){
                                case 0:
                                    header1 = mstaticAddKeysString.get(0);
                                    break;
                                case 1:
                                    header2 = mstaticAddKeysString.get(1);
                                    break;
                                case 2:
                                    header3 = mstaticAddKeysString.get(2);
                                    break;
                            }
                        }
                    }
                    tvHeaderOne.setText(header1);
                    tvHeaderTwo.setText(header2);
                    tvHeaderThree.setText(header3);
                    List<TableReportExpandItemPojo> mTableReportExpandItemPojos = new ArrayList<>();
                    for(int i = 0; i < pairs.size(); i++){
                        mTableReportExpandItemPojos = new ArrayList<>();
                        String columnValueOne = "";

                        if(((HashMap<String, String>) pairs.get(i)).get(key1) == null){
                            columnValueOne = "";
                        } else{
                            columnValueOne = ((HashMap<String, String>) pairs.get(i)).get(key1).toString();
                        }
                        String columnValueTwo = "";
                        if(((HashMap<String, String>) pairs.get(i)).get(key2) == null){
                            columnValueTwo = "";
                        } else{
                            columnValueTwo = ((HashMap<String, String>) pairs.get(i)).get(key2).toString();
                        }
                        String columnValueThree = "";

                        if(((HashMap<String, String>) pairs.get(i)).get(key3) == null){
                            columnValueThree = "";
                        } else{
                            columnValueThree = ((HashMap<String, String>) pairs.get(i)).get(key3).toString();
                        }

                        getTableList(columnValueOne, columnValueTwo, columnValueThree);

                        for(int j = 0; j < mValueKeysString.size(); j++){
                            TableReportExpandItemPojo mTableReportExpandItemPojo = new TableReportExpandItemPojo();
                            mTableReportExpandItemPojo.reportId = mValueKeysString.get(j);
                            mTableReportExpandItemPojo.reportNameDisplay = mstaticAddKeysString.get(j);
                            mTableReportExpandItemPojo.reportIdForServer = mValueKeysString.get(j);
                            Logger.Error("<<<< Report ID >>>>" + mTableReportExpandItemPojo.reportId);
                            Logger.Error("<<<< Report Name >>>>" + mstaticAddKeysString.get(j));
                            String checkConcatReportNameValue = "";
                            if(((HashMap<String, String>) pairs.get(i)).get("concatWithReportName") == null){
                                checkConcatReportNameValue = "";
                            } else{
                                checkConcatReportNameValue = ((HashMap<String, String>) pairs.get(i)).get("concatWithReportName").toString();
                            }

                            String startDateCheck = "dataStartDate" + mstaticAddKeysString.get(j);
                            String endDateCheck = "dataEndDate" + mstaticAddKeysString.get(j);
                            mTableReportExpandItemPojo.reportName = mstaticAddKeysString.get(j);
                            mTableReportExpandItemPojo.reportNameForServer = mstaticAddKeysString.get(j);
                            mTableReportExpandItemPojo.reportIdForServer = mValueKeysString.get(j);

                            if(checkConcatReportNameValue.length() > 0){
                                mTableReportExpandItemPojo.reportId = "";
                                mTableReportExpandItemPojo.reportId = checkConcatReportNameValue + mValueKeysString.get(j);
                                mTableReportExpandItemPojo.reportIdForServer = "";
                                mTableReportExpandItemPojo.reportIdForServer = checkConcatReportNameValue + " " + mValueKeysString.get(j);
                                mTableReportExpandItemPojo.reportName = "";
                                mTableReportExpandItemPojo.reportName = checkConcatReportNameValue + mstaticAddKeysString.get(j);
                                mTableReportExpandItemPojo.reportNameForServer = "";
                                mTableReportExpandItemPojo.reportNameForServer = checkConcatReportNameValue + " " + mstaticAddKeysString.get(j);
                            }
                            if(((HashMap<String, String>) pairs.get(i)).get(mValueKeysString.get(j)) == null){
                                mTableReportExpandItemPojo.reportValue = "";
                            } else{
                                mTableReportExpandItemPojo.reportValue = ((HashMap<String, String>) pairs.get(i)).get(mValueKeysString.get(j)).toString();
                            }

                            Logger.Error("<<<< Report Value >>>>" + mTableReportExpandItemPojo.reportValue);
                            if(((HashMap<String, String>) pairs.get(i)).get("entityId") == null){
                                mTableReportExpandItemPojo.entityId = null;
                            } else{
                                mTableReportExpandItemPojo.entityId = ((HashMap<String, String>) pairs.get(i)).get("entityId").toString();
                            }

                            if(((HashMap<String, String>) pairs.get(i)).get("entityName") == null){
                                mTableReportExpandItemPojo.entityName = "";
                            } else{
                                mTableReportExpandItemPojo.entityName = ((HashMap<String, String>) pairs.get(i)).get("entityName").toString();
                            }


                            if(((HashMap<String, String>) pairs.get(i)).get("entityType") == null){
                                mTableReportExpandItemPojo.entityType = "";
                            } else{
                                mTableReportExpandItemPojo.entityType = ((HashMap<String, String>) pairs.get(i)).get("entityType").toString();
                            }

                            if(((HashMap<String, String>) pairs.get(i)).get("dataStartDate") == null){
                                mTableReportExpandItemPojo.dataStartDate = "";
                            } else{
                                mTableReportExpandItemPojo.dataStartDate = ((HashMap<String, String>) pairs.get(i)).get("dataStartDate").toString();
                            }
                            if(((HashMap<String, String>) pairs.get(i)).get("dataEndDate") == null){
                                mTableReportExpandItemPojo.dataEndDate = "";
                            } else{
                                mTableReportExpandItemPojo.dataEndDate = ((HashMap<String, String>) pairs.get(i)).get("dataEndDate").toString();
                            }
                            if(((HashMap<String, String>) pairs.get(i)).get("compDataStartDate ") == null){
                                mTableReportExpandItemPojo.compDataStartDate = "";
                            } else{
                                mTableReportExpandItemPojo.compDataStartDate = ((HashMap<String, String>) pairs.get(i)).get("compDataStartDate ").toString();
                            }

                            if(((HashMap<String, String>) pairs.get(i)).get("compDataEndDate") == null){
                                mTableReportExpandItemPojo.compDataEndDate = "";
                            } else{
                                mTableReportExpandItemPojo.compDataEndDate = ((HashMap<String, String>) pairs.get(i)).get("compDataEndDate").toString();
                            }

                            String startDateCheckValue = "";
                            String endDateCheckValue = "";
                            if(((HashMap<String, String>) pairs.get(i)).get(startDateCheck) == null){
                                startDateCheckValue = "";
                            } else{
                                startDateCheckValue = ((HashMap<String, String>) pairs.get(i)).get(startDateCheck);
                            }
                            if(((HashMap<String, String>) pairs.get(i)).get(endDateCheck) == null){
                                endDateCheckValue = "";
                            } else{
                                endDateCheckValue = ((HashMap<String, String>) pairs.get(i)).get(endDateCheck);
                            }

                            mTableReportExpandItemPojo.startDateCheck = startDateCheckValue;
                            mTableReportExpandItemPojo.endDateCheck = endDateCheckValue;
                            mTableReportExpandItemPojo.isSocialInteractionEnabled = mJsonSocialINteractionEnabled.optBoolean(mValueKeysString.get(j));
                            mTableReportExpandItemPojos.add(mTableReportExpandItemPojo);
                        }
                        map.put(i, mTableReportExpandItemPojos);
                    }

                    Logger.Error("<<<< Child Size >>> " + mTableReportExpandItemPojos.size());
                    if(mTablePojos != null && mTablePojos.size() > 0){
                        mTableReportAdapter = new TableReportAdapterForLeader(mHomeActivity, mTablePojos, true, true, true, map, false, mLeaderDetailsListClickListener);
                        lvtableReport.setAdapter(mTableReportAdapter);
                        Logger.Error("<<< Table List >>>>>" + mTablePojos.size());
                        lvtableReport.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);

                    } else{
                        lvtableReport.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                    }


                } catch(JSONException e){
                    e.printStackTrace();
                    lvtableReport.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                } catch(IOException e){
                    e.printStackTrace();
                    lvtableReport.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                } catch(Exception e){
                    e.printStackTrace();
                    lvtableReport.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){
                lvtableReport.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
                mProgressBarForTable.setVisibility(View.GONE);
            }
        });

    }

    public List<TablePojo> getTableList(String columnNameone, String columnNameTwo, String columnThree){
        TablePojo mTablePojo = new TablePojo(columnNameone, columnNameTwo, columnThree);
        mTablePojos.add(mTablePojo);
        return mTablePojos;
    }

    @Override
    public void recyclerViewListClicked(View v, int userId){

    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id){




        Map<Integer, List<TableReportExpandItemPojo>> mapGroupClick = (Map<Integer, List<TableReportExpandItemPojo>>) mTableReportAdapter.getGroup(groupPosition);


        //Logger.Error("<<< Group Click " + mTableReportAdapter.getGroup(groupPosition));
        String entityid = "";
        if(mapGroupClick != null && mapGroupClick.size() > 0){
            if(mapGroupClick.get(groupPosition) != null && mapGroupClick.size() > 0){
                entityid = mapGroupClick.get(groupPosition).get(0).entityId;
                AgentProfileFragment mAgentProfileFragment = new AgentProfileFragment();
                Bundle mBundle = new Bundle();
                mBundle.putString(AgentProfileFragment.KEY_USER_ID_FROM_LEADER_BOARD, entityid);
                mBundle.putBoolean(AgentProfileFragment.KEY_IS_FROM_LEADER_BOARD, true);
                mAgentProfileFragment.setArguments(mBundle);
                mHomeActivity.replace(mAgentProfileFragment, getResources().getString(R.string.tag_user_profile));
            }
        }

        return false;
    }
}
