package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.FilterDetailListAdapter;
import com.ingauge.adapters.FilterDetailListAdapterObservation;
import com.ingauge.listener.OnFilterDataSetListener;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.AgentModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ObservationListModel;
import com.ingauge.pojo.PerformanceLeaderModel;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mansurum on 24-May-17.
 */

public class FilterDetailListFragmentForObservation extends BaseFragment {
    private RecyclerView rvFilterDataList;
    private HomeActivity mHomeActivity;
    View rootView;
    FilterBaseData mFilterBaseData;
    List<FilterBaseData> mFilterBaseDatas;
    private int selectedItemPosition = -1;
    private OnFilterDataSetListener onFilterDataSetListener;
    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel;
    private AgentModel mAgentModel;
    private PerformanceLeaderModel mPerformanceLeaderModel;
    private ObservationListModel mObservationListModel;
    private SearchView searchView;
    private FilterDetailListAdapterObservation mFilterListAdapter;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onAttachToParentFragment(getFragmentManager().findFragmentByTag(getString(R.string.tag_filter_list_for_observe_agent)));
        mFilterBaseDatas = new ArrayList<>();
    }

    public void onAttachToParentFragment(Fragment fragment) {
        try {
            onFilterDataSetListener = (OnFilterDataSetListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString() + " must implement OnFilterDataSetListener");
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_detail_filter_list, container, false);
        initViews(rootView);
        if (getArguments() != null) {
            selectedItemPosition = getArguments().getInt(FilterListFragment.KEY_SELECTION_POSITION, selectedItemPosition);
            getFilterData(selectedItemPosition);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mFilterListAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return rootView;
    }


    void getFilterData(int position) {
        switch (position) {
            case 0:
                mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getAccessibleTenantLocation(mAccessibleTenantLocationDataModel);
                mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_filter_location)));
                break;
            case 1:
                mPerformanceLeaderModel = (PerformanceLeaderModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getPerformanceLeader(mPerformanceLeaderModel);
                if(InGaugeSession.read(getResources().getString(R.string.key_obs_cc_survey_entity_type_id),null).equalsIgnoreCase(getResources().getString(R.string.survey_entity_type_id_observation))) {
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_obs_pl)));
                }else{
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_obs_coach)));
                }
                break;
            case 2:
                mAgentModel = (AgentModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getAgent(mAgentModel);
                if(InGaugeSession.read(getResources().getString(R.string.key_obs_cc_survey_entity_type_id),null).equalsIgnoreCase(getResources().getString(R.string.survey_entity_type_id_observation))) {
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_obs_agent)));
                }else{
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_obs_team_member)));
                }
                break;
            case 3:
                mObservationListModel = (ObservationListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getObservation(mObservationListModel);

                if(InGaugeSession.read(getResources().getString(R.string.key_obs_cc_survey_entity_type_id),null).equalsIgnoreCase(getResources().getString(R.string.survey_entity_type_id_observation))) {
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_obs_type)));
                }else{
                    mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(getResources().getString(R.string.mobile_key_coaching_type)));
                }


                break;

        }

    }
    private void initViews(View rootView) {
        rvFilterDataList = (RecyclerView) rootView.findViewById(R.id.fragment_detail_filter_list_recycleview);
        searchView = (SearchView) rootView.findViewById(R.id.fragment_detail_filter_list_search_view);
    }
    private void getAccessibleTenantLocation(AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel) {
        if (mAccessibleTenantLocationDataModel != null) {
            for (AccessibleTenantLocationDataModel.Datum mDatum : mAccessibleTenantLocationDataModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterIndex = 0;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas, true);
        }
    }

    private void getPerformanceLeader(PerformanceLeaderModel mPerformanceLeaderModel) {
        if (mPerformanceLeaderModel != null) {
            for (PerformanceLeaderModel.Datum mDatum : mPerformanceLeaderModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterIndex = 1;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas, true);
        }

    }
    private void getAgent(AgentModel mAgentModel) {
        if (mAgentModel != null) {
            for (AgentModel.Datum mDatum : mAgentModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterIndex = 2;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas, true);
        }
    }
    private void getObservation(ObservationListModel mObservationListModel) {
        if (mObservationListModel != null) {
            for (ObservationListModel.Datum mDatum : mObservationListModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());

                String observationName= UiUtils.SurroundWithBraces(mDatum.getName());
                String categoryName=UiUtils.SurroundWithBraces(mDatum.getSurveyCategory().getName());
                mFilterBaseData.name = observationName+categoryName;
                mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterIndex = 3;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas, true);
        }
    }


    private void setFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList, boolean isSort) {
        if (mFilterBaseDataList != null) {
            if (mFilterBaseDataList.size() > 0) {
                if (isSort) {
                    Collections.sort(mFilterBaseDataList, new Comparator<FilterBaseData>() {
                        public int compare(FilterBaseData v1, FilterBaseData v2) {
                            return v1.name.compareTo(v2.name);
                        }
                    });
                }
                mFilterListAdapter = new FilterDetailListAdapterObservation(getActivity(),mFilterBaseDataList,mHomeActivity);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterDataList.setLayoutManager(mLayoutManager);
                rvFilterDataList.setItemAnimator(new DefaultItemAnimator());
                rvFilterDataList.setAdapter(mFilterListAdapter);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
    }
}
