package com.ingauge.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.AgentProfileDetailsPojo;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by desainid on 5/11/2017.
 */

public class UserProfileFragment extends Fragment implements View.OnClickListener{
    Context mContext;
    private static final int PERMISSIONS_REQUEST = 1;
    private int REQUEST_CAMERA = 2;
    private static final int REQUEST_GALLERY = 3;
    private static final int MEDIA_TYPE_IMAGE = 4;
    private static final String TAG = "UserProfile";
    private EditText etFirstName;
    private EditText etMiddleName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etRoles;
    private EditText etEmpId;
    private TextView tvFpgLevel;
    private EditText etMobileNumber;
    private TextView tvHiredte;
    private TextView tvEmployeeScheduleValue;
    private EditText etEmpInGaugeId;
    private EditText etSpeaks;
    private EditText etHobbies;

    private TextInputLayout mTextInputLayoutfirstName;
    private TextInputLayout mTextInputLayoutlastName;
    private TextInputLayout mTextInputLayoutemail;
    private TextInputLayout mTextInputLayoutpassword;
    private TextInputLayout mTextInputLayoutroles;
    private TextInputLayout mTextInputLayoutempId;
    private TextInputLayout mTextInputLayoutnumber;


    CountryCodePicker mCountryCodePicker;


    private ImageView img_profile;
    private Uri fileUri;
    private String permission[];
    private String selectedImagePath = "";
    Bitmap img = null;
    private TextView tv_hireDate;
    private int mYear, mMonth, mDay;
    Calendar calendar;

    APIInterface apiInterface;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);

    private TextView tvGender;
    HomeActivity mHomeActivity;

    Dialog levelDialog;
    String FpgLevel = "";
    String fpgLevelKeys[];

    RelativeLayout rel_maxImpact, rel_puttingIntoPractice, rel_notes,
            rel_coaching, rel_power_hour, rel_consistencyCounts, rel_duelingDialogs, rel_overcomingObs,
            rel_motivatePer, rel_winningByWorking, rel_effComm, rel_advCoachingLeadership;

    ImageView tv_maxYourImpact_tick, tv_puttingIntoPractice_tick, tv_notes_tick, tv_coaching_tick, tv_power_hour_tick, tv_consistencyCounts_tick, tv_duelingDialogs_tick, tv_overcomingObs_tick, tv_motivatePer_tick, tv_winningByWorking_tick, tv_effComm_tick, tv_advCoachingLeadership_tick;
    AgentProfileDetailsPojo userdata;
    boolean isPasswordChange = false;

    TextView tvChangeProfile;

    boolean isNeedUserProfileUpdate = false;
    private String encodedBase64String = "";
    boolean isNeedDateChange = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_user_profile_update, container, false);
        initViews(rootView);
        //setViewAndChildrenEnabled(rootView, false);
        //UsertDataDisplayAPI();
        InGaugeSession.write("IsUserProfile", "yes");
        apiInterface = APIClient.getClient().create(APIInterface.class);
        tvChangeProfile.setOnClickListener(this);
        tvHiredte.setOnClickListener(this);
        setHasOptionsMenu(true);
        Bundle bundle = new Bundle();
        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
        InGaugeApp.getFirebaseAnalytics().logEvent("check_profile", bundle);

        GetUsersProfile();

        return rootView;
    }


    public int getFpgLevelIndex(String fpglevelName){
        for(int i = 0; i < fpgLevelKeys.length; i++){

            if(fpgLevelKeys[i].equalsIgnoreCase(fpglevelName)){
                return i;
            }
        }
        return 0;
    }

    public void initializeLevelDialog(){
        levelDialog = new Dialog(mContext);
        levelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        levelDialog.setContentView(R.layout.fpg_level_dialog);
        //check dialog position
        Window window = levelDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, (int) mHomeActivity.getResources().getDimension(R.dimen._280sdp));
        window.setBackgroundDrawableResource(R.drawable.level_layout);
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.x = 100;
        wlp.y = 40;
        window.setAttributes(wlp);
        //check end
        fpgLevelKeys = mHomeActivity.getResources().getStringArray(R.array.fpg_levels);
        initializeRelativeViews(levelDialog);

    }


    private void initializeRelativeViews(Dialog dialog){


        rel_maxImpact = (RelativeLayout) dialog.findViewById(R.id.rel_maxImpact);
        rel_puttingIntoPractice = (RelativeLayout) dialog.findViewById(R.id.rel_puttingIntoPractice);
        rel_notes = (RelativeLayout) dialog.findViewById(R.id.rel_notes);
        rel_coaching = (RelativeLayout) dialog.findViewById(R.id.rel_coaching);
        rel_power_hour = (RelativeLayout) dialog.findViewById(R.id.rel_power_hour);
        rel_consistencyCounts = (RelativeLayout) dialog.findViewById(R.id.rel_consistencyCounts);
        rel_duelingDialogs = (RelativeLayout) dialog.findViewById(R.id.rel_duelingDialogs);
        rel_overcomingObs = (RelativeLayout) dialog.findViewById(R.id.rel_overcomingObs);
        rel_motivatePer = (RelativeLayout) dialog.findViewById(R.id.rel_motivatePer);
        rel_winningByWorking = (RelativeLayout) dialog.findViewById(R.id.rel_winningByWorking);
        rel_effComm = (RelativeLayout) dialog.findViewById(R.id.rel_effComm);
        rel_advCoachingLeadership = (RelativeLayout) dialog.findViewById(R.id.rel_advCoachingLeadership);

        tv_maxYourImpact_tick = (ImageView) dialog.findViewById(R.id.tv_maxYourImpact_tick);
        tv_puttingIntoPractice_tick = (ImageView) dialog.findViewById(R.id.tv_puttingIntoPractice_tick);
        tv_notes_tick = (ImageView) dialog.findViewById(R.id.tv_notes_tick);
        tv_coaching_tick = (ImageView) dialog.findViewById(R.id.tv_coaching_tick);
        tv_power_hour_tick = (ImageView) dialog.findViewById(R.id.tv_power_hour_tick);
        tv_consistencyCounts_tick = (ImageView) dialog.findViewById(R.id.tv_consistencyCounts_tick);
        tv_duelingDialogs_tick = (ImageView) dialog.findViewById(R.id.tv_duelingDialogs_tick);
        tv_overcomingObs_tick = (ImageView) dialog.findViewById(R.id.tv_overcomingObs_tick);
        tv_motivatePer_tick = (ImageView) dialog.findViewById(R.id.tv_motivatePer_tick);
        tv_winningByWorking_tick = (ImageView) dialog.findViewById(R.id.tv_winningByWorking_tick);
        tv_effComm_tick = (ImageView) dialog.findViewById(R.id.tv_effComm_tick);
        tv_advCoachingLeadership_tick = (ImageView) dialog.findViewById(R.id.tv_advCoachingLeadership_tick);


        if(FpgLevel.length() > 0){
            setDialogIndexValue(getFpgLevelIndex(FpgLevel));
        } else{
            setDialogIndexValue(0);
        }


        rel_maxImpact.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                tv_maxYourImpact_tick.setVisibility(View.VISIBLE);
                FpgLevel = fpgLevelKeys[0];
                tvFpgLevel.setText(FpgLevel);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_puttingIntoPractice.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                FpgLevel = fpgLevelKeys[1];
                tvFpgLevel.setText(FpgLevel);
                tv_puttingIntoPractice_tick.setVisibility(View.VISIBLE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_notes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                FpgLevel = fpgLevelKeys[2];
                tvFpgLevel.setText(FpgLevel);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.VISIBLE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_coaching.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[3];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.VISIBLE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_power_hour.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[4];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.VISIBLE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_consistencyCounts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[5];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.VISIBLE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_duelingDialogs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[6];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.VISIBLE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_overcomingObs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[7];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.VISIBLE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_motivatePer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[8];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.VISIBLE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_winningByWorking.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[9];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.VISIBLE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_effComm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[10];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.VISIBLE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                levelDialog.dismiss();

            }
        });
        rel_advCoachingLeadership.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FpgLevel = fpgLevelKeys[11];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.VISIBLE);
                levelDialog.dismiss();

            }
        });

    }


    public void setDialogIndexValue(int position){
        switch(position){
            case 0:
                tv_maxYourImpact_tick.setVisibility(View.VISIBLE);
                FpgLevel = fpgLevelKeys[0];
                tvFpgLevel.setText(FpgLevel);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                break;
            case 1:
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                FpgLevel = fpgLevelKeys[1];
                tvFpgLevel.setText(FpgLevel);
                tv_puttingIntoPractice_tick.setVisibility(View.VISIBLE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                break;
            case 2:
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                FpgLevel = fpgLevelKeys[2];
                tvFpgLevel.setText(FpgLevel);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.VISIBLE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                break;
            case 3:
                FpgLevel = fpgLevelKeys[3];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.VISIBLE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                break;
            case 4:
                FpgLevel = fpgLevelKeys[4];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.VISIBLE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                break;
            case 5:
                FpgLevel = fpgLevelKeys[5];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.VISIBLE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                break;
            case 6:
                FpgLevel = fpgLevelKeys[6];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.VISIBLE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);

                break;
            case 7:
                FpgLevel = fpgLevelKeys[7];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.VISIBLE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);

                break;
            case 8:
                FpgLevel = fpgLevelKeys[8];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.VISIBLE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                break;
            case 9:
                FpgLevel = fpgLevelKeys[9];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.VISIBLE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);

                break;
            case 10:
                FpgLevel = fpgLevelKeys[10];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.VISIBLE);
                tv_advCoachingLeadership_tick.setVisibility(View.GONE);
                break;
            case 11:
                FpgLevel = fpgLevelKeys[11];
                tvFpgLevel.setText(FpgLevel);
                tv_maxYourImpact_tick.setVisibility(View.GONE);
                tv_puttingIntoPractice_tick.setVisibility(View.GONE);
                tv_notes_tick.setVisibility(View.GONE);
                tv_coaching_tick.setVisibility(View.GONE);
                tv_power_hour_tick.setVisibility(View.GONE);
                tv_consistencyCounts_tick.setVisibility(View.GONE);
                tv_duelingDialogs_tick.setVisibility(View.GONE);
                tv_overcomingObs_tick.setVisibility(View.GONE);
                tv_motivatePer_tick.setVisibility(View.GONE);
                tv_winningByWorking_tick.setVisibility(View.GONE);
                tv_effComm_tick.setVisibility(View.GONE);
                tv_advCoachingLeadership_tick.setVisibility(View.VISIBLE);
                break;
        }
    }

    void GetUsersProfile(){
        mHomeActivity.startProgress(mContext);
        String userId = String.valueOf(UiUtils.getUserId(mHomeActivity));
        Call mCall = apiInterface.getAgentProfileDetails(userId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                mHomeActivity.endProgress();
                if(response.body() != null){
                    mHomeActivity.tvTitle.setText("User Profile");
                    userdata = (AgentProfileDetailsPojo) response.body();
                    setValueOfUser(userdata.getData());
                    initializeLevelDialog();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                initializeLevelDialog();
                mHomeActivity.endProgress();

            }
        });
    }

    void sendUserProfileDetailsToServer(){

        mHomeActivity.startProgress(mContext);
        AgentProfileDetailsPojo mDataDetails = userdata;


        mDataDetails.getData().setFirstName(etFirstName.getText().toString().trim());
        mDataDetails.getData().setMiddleName(etMiddleName.getText().toString().trim());
        mDataDetails.getData().setLastName(etLastName.getText().toString().trim());
        mDataDetails.getData().setCountryCode(mCountryCodePicker.getSelectedCountryCode());
        mDataDetails.getData().setPhoneNumber(etMobileNumber.getText().toString().trim());
        mDataDetails.getData().setEmail(etEmail.getText().toString().trim());
        if(etPassword.getText().toString().trim().length() > 0){
            isPasswordChange = true;
            mDataDetails.getData().setPassword(UiUtils.md5(etPassword.getText().toString().trim()));
        } else{
            mDataDetails.getData().setPassword("");
        }

        mDataDetails.getData().setNumber(etEmpId.getText().toString().trim());
        if(isNeedDateChange){
            mDataDetails.getData().setHiredDate(String.valueOf(getServerDateFormat(tvHiredte.getText().toString().trim())));
        }else{

            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
            try{
                Date inputDate = inputFormat.parse(mDataDetails.getData().getHiredDate());                 // parse input
                String stroutputFormat = outputFormat.format(inputDate);
                Date mDate = outputFormat.parse(stroutputFormat);
                long timeInMilliseconds = mDate.getTime();
                mDataDetails.getData().setHiredDate("" + timeInMilliseconds);

            } catch(Exception e){
                e.printStackTrace();
            }
        }

        mDataDetails.getData().setFpgLevel(tvFpgLevel.getText().toString().trim());
        mDataDetails.getData().setJobTitle(etRoles.getText().toString().trim());
        mDataDetails.getData().setPulseId(etEmpInGaugeId.getText().toString());
        if(tvEmployeeScheduleValue.getText().toString().length() > 1){
            mDataDetails.getData().setWorkingType(tvEmployeeScheduleValue.getText().toString());
        }
        mDataDetails.getData().setSpeaks(etSpeaks.getText().toString());
        mDataDetails.getData().setHobbies(etHobbies.getText().toString());
        mDataDetails.getData().setMapTo("USER");

        Gson gson = new Gson();
        String json = gson.toJson(mDataDetails.getData());
        Logger.Error("Json String " + json);

        if(mDataDetails != null){
            String userId = String.valueOf(UiUtils.getUserId(mHomeActivity));
            Call mCall = apiInterface.putAgentProfileDetails(mDataDetails.getData());
            mCall.enqueue(new Callback(){
                @Override
                public void onResponse(Call call, Response response){
                    isNeedDateChange=false;

                   // ResponseBody mResponseBody = (ResponseBody) response.body();
                    try{
                        //Logger.Error("<<<< Response " + mResponseBody.string());
                        // JSONObject responseJson = new JSONObject(mResponseBody.string());
                        //Logger.Error("<<<< Response String " + responseJson.toString());
                        if(isPasswordChange){
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            AlertDialog dialog;
                            builder.setTitle("");
                            builder.setMessage(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_password_change_message)));
                            builder.setCancelable(false);
                            builder.setPositiveButton(
                                    getActivity().getResources().getString(R.string.alert_ok),
                                    new DialogInterface.OnClickListener(){
                                        @Override
                                        public void onClick(DialogInterface dialog, int which){
                                            dialog.dismiss();
                                            mHomeActivity.ForceLogout();
                                        }
                                    });

                            //  builder.show();
                            dialog = builder.create();
                            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                            dialog.show();
                        } else{
                            String messagePorfileUpdate = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_profile_updated));
                            Toast.makeText(mHomeActivity, messagePorfileUpdate, Toast.LENGTH_SHORT).show();
                        }

                        if(isNeedUserProfileUpdate){
                            updateAvatar(encodedBase64String);
                        } else{
                            mHomeActivity.endProgress();
                            mHomeActivity.onBackPressed();
                        }
                    } catch(Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call call, Throwable t){
                    mHomeActivity.endProgress();
                }
            });
        }

    }

    void GetUserAvatar(String authToken){
       /* Call mCall = apiInterface.getUserAvatar("application/json", "bearer" + " " + authToken, "1", "1");
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                byte[] bytes=response.body().toString().getBytes();
                img_profile.setImageBitmap(ByteArrayToBitmap(bytes));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
            }
        });*/

        String imageUrl = UiUtils.baseUrl + "api/user/avatar/" + UiUtils.getIndustryId(mContext) + "/" + UiUtils.getUserId(mContext);
        Glide.with(mContext)
                .load(imageUrl)
                .centerCrop()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .error(R.drawable.placeholder)
                .into(img_profile);

    }

    public Bitmap ByteArrayToBitmap(byte[] byteArray){
        ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(byteArray);
        Bitmap bitmap = BitmapFactory.decodeStream(arrayInputStream);
        return bitmap;
    }

    private void setValueOfUser(AgentProfileDetailsPojo.Data userInfo){
        etFirstName.setText(userInfo.getFirstName());
        //  mTextInputLayoutfirstName.setHint(mHomeactivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_fname)));

        etLastName.setText(userInfo.getLastName());
        etMiddleName.setText(userInfo.getMiddleName());
        // mTextInputLayoutlastName.setHint(mHomeactivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_lname)));

        etEmail.setText(userInfo.getEmail());
        //  mTextInputLayoutemail.setHint(mHomeactivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_email)));
        // tvContactNumber.setText(mHomeactivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_contact)));
        //etPassword.setText(userInfo.getPassword());
        //To get Role From selected tenant
        // mTextInputLayoutempId.setHint(mHomeactivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_empid)));
        // mTextInputLayouthireDate.setHint(mHomeactivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_hiredate)));
        // mTextInputLayoutfpgLevel.setHint(mHomeactivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_fpgleavel)));
        //InGaugeSession.read(getString(R.string.key_user_role_name), "");
        etRoles.setText(userInfo.getJobTitle());
        etEmpId.setText(String.valueOf(userInfo.getNumber()));
        tvHiredte.setText(getDate(userInfo));
        FpgLevel = userInfo.getFpgLevel();
        tvFpgLevel.setText(FpgLevel);

        etMobileNumber.setText(userInfo.getPhoneNumber());

        if(userInfo.getWorkingType() != null && userInfo.getWorkingType().length() > 0){
            tvEmployeeScheduleValue.setText(userInfo.getWorkingType());
        } else{
            tvEmployeeScheduleValue.setText("-");
        }

        etEmpInGaugeId.setText(userInfo.getPulseId());
        etSpeaks.setText(userInfo.getSpeaks());
        etHobbies.setText(userInfo.getHobbies());
        mCountryCodePicker.setCountryForPhoneCode(Integer.parseInt(userInfo.getCountryCode()));
        //speaks
        /*if (userInfo.getGender().equalsIgnoreCase("male")) {

        } else if (userInfo.getGender().equalsIgnoreCase("female")) {

        }
*/
        GetUserAvatar(InGaugeSession.read(getString(R.string.key_auth_token), ""));
    }

    private String getDate(AgentProfileDetailsPojo.Data userinfo){
        String date = userinfo.getHiredDate();
        String formattedDate[] = date.split(" ");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
        SimpleDateFormat sdfParse = new SimpleDateFormat("MMM dd ,yyyy", Locale.UK);
        Date parsed = null;
        try{
            parsed = sdf.parse(formattedDate[0]);

        } catch(Exception e){
            e.printStackTrace();
        }


        return sdfParse.format(parsed);

    }

    private String getCurrentDateFormat(){
        return "yyyy-MM-dd hh:mm:ss";
    }

    private void initViews(View view){
        etFirstName = (EditText) view.findViewById(R.id.et_firstName);
        // etFirstName.addTextChangedListener(new TextChange(etFirstName));
        etMiddleName = (EditText) view.findViewById(R.id.et_middleName);
        etLastName = (EditText) view.findViewById(R.id.et_lastName);
        //  etLastName.addTextChangedListener(new TextChange(etLastName));
        etEmail = (EditText) view.findViewById(R.id.et_email);
        // etEmail.addTextChangedListener(new TextChange(etEmail));
        etPassword = (EditText) view.findViewById(R.id.et_password);
        etRoles = (EditText) view.findViewById(R.id.et_roles);
        //  etRoles.addTextChangedListener(new TextChange(etRoles));
        etEmpId = (EditText) view.findViewById(R.id.et_empId);
        //  etEmpId.addTextChangedListener(new TextChange(etEmpId));
        tvHiredte = (TextView) view.findViewById(R.id.tv_hireDate);
        //  tvHiredte.addTextChangedListener(new TextChange(tvHiredte));
        tvFpgLevel = (TextView) view.findViewById(R.id.et_fpgLevel);
        tvFpgLevel.setOnClickListener(this);
        // tvFpgLevel.addTextChangedListener(new TextChange(tvFpgLevel));
        etMobileNumber = (EditText) view.findViewById(R.id.edt_phone_number);
        //   etMobileNumber.addTextChangedListener(new TextChange(etMobileNumber));
        tvEmployeeScheduleValue = (TextView) view.findViewById(R.id.fragment_user_profile_update_tv_title_emp_schedule_value);
        etEmpInGaugeId = (EditText) view.findViewById(R.id.et_in_gauge_id);
        etSpeaks = (EditText) view.findViewById(R.id.et_speaks);
        etHobbies = (EditText) view.findViewById(R.id.et_hobbies);


        mTextInputLayoutfirstName = (TextInputLayout) view.findViewById(R.id.til_firstName);
        mTextInputLayoutlastName = (TextInputLayout) view.findViewById(R.id.til_lastName);

        mTextInputLayoutemail = (TextInputLayout) view.findViewById(R.id.til_email);
        mTextInputLayoutpassword = (TextInputLayout) view.findViewById(R.id.til_password);
        mTextInputLayoutroles = (TextInputLayout) view.findViewById(R.id.til_roles);

        mTextInputLayoutnumber = (TextInputLayout) view.findViewById(R.id.til_phone_number);
        mTextInputLayoutempId = (TextInputLayout) view.findViewById(R.id.til_EmpId);

        mCountryCodePicker = (CountryCodePicker) view.findViewById(R.id.country_code_picker);

        img_profile = (ImageView) view.findViewById(R.id.img_profile);

        tvChangeProfile = (TextView) view.findViewById(R.id.fragment_user_profile_update_tv_change_profile);

        permission = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        calendar = Calendar.getInstance();
        etPassword.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event){
                final int DRAWABLE_RIGHT = 2;
                if(event.getAction() == MotionEvent.ACTION_UP){
                    if(event.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())){
                        // your action here
                        if(etPassword.getTransformationMethod() == null){
                            etPassword.setTransformationMethod(new PasswordTransformationMethod());
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.hide_password, 0);
                        } else{
                            etPassword.setTransformationMethod(null);
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.view_password, 0);
                        }

                        return true;
                    }
                }
                return false;
            }
        });

        //Initialization of date picker component
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        tvGender = (TextView) view.findViewById(R.id.tv_gender);
        // mHomeactivity.tvTitle.setText(mHomeactivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_myprofile)));
        //mHomeActivity.tvTitle.setText("My Profile");
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setOnClickListener(this);
        mHomeActivity.ibMenu.setImageResource(android.R.drawable.ic_menu_save);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
//        TODO :
        super.onCreateOptionsMenu(menu, inflater);
        // menu.add(0, 1,Menu.NONE,getResources().getString(R.string.user_profile_save)).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == 1){
            checkValidations();
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkValidations(){
        if(etFirstName.getText().toString().trim().length() == 0){
            mTextInputLayoutfirstName.setError(getString(R.string.error_empty_fname));
            return;
        } else{
            mTextInputLayoutfirstName.setError(null);
            mTextInputLayoutfirstName.setErrorEnabled(false);
        }

        if(etLastName.getText().toString().trim().length() == 0){
            mTextInputLayoutlastName.setError(getString(R.string.error_empty_lname));
            return;
        } else{
            mTextInputLayoutlastName.setError(null);
            mTextInputLayoutlastName.setErrorEnabled(false);
        }

        if(etMobileNumber.getText().toString().trim().length() == 0){
            mTextInputLayoutnumber.setError(getString(R.string.error_empty_phone));
            return;
        } else{
            mTextInputLayoutnumber.setError(null);
            mTextInputLayoutnumber.setErrorEnabled(false);
        }

        if(etEmpId.getText().toString().trim().length() == 0){
            mTextInputLayoutempId.setError(getString(R.string.error_empty_empId));
            return;
        } else{
            mTextInputLayoutempId.setError(null);
            mTextInputLayoutempId.setErrorEnabled(false);
        }

        if(etRoles.getText().toString().trim().length() == 0){
            mTextInputLayoutroles.setError(getString(R.string.error_empty_roles));
            return;
        } else{
            mTextInputLayoutroles.setError(null);
            mTextInputLayoutroles.setErrorEnabled(false);
        }
        /*if(tvHiredte.getText().toString().trim().length() == 0){
            mTextInputLayouthireDate.setError(getString(R.string.error_empty_hire_date));
            return;
        } else{
            mTextInputLayouthireDate.setError(null);
            mTextInputLayouthireDate.setErrorEnabled(false);
        }*/

        /*if(tv_hireDate.getText().toString().trim().length()==0){
           *//* mTextInputLayoutOrg.setError(getString(R.string.error_empty_organization));
            return;*//*
        }else{
            mTextInputLayoutOrg.setError(null);
        }*/

        String email = etEmail.getText().toString().trim();
        if(email.isEmpty() || !UiUtils.isValidEmail(email)){
            mTextInputLayoutemail.setError(getString(R.string.error_empty_email));
            return;
        } else{
            mTextInputLayoutemail.setError(null);
            mTextInputLayoutemail.setErrorEnabled(false);
        }


        mHomeActivity.setNeedAgentProfileUpdateForUserProfile(true);
        sendUserProfileDetailsToServer();
    }

    public static Fragment newInstance(String text, int color){
        Fragment frag = new UserProfileFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
//        args.putString(ARG_TEXT, text);
//        args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.fragment_user_profile_update_tv_change_profile:

                if(UiUtils.verifyStoragePermissions(getActivity(), PERMISSIONS_REQUEST, permission)){
                    ImagePickerDialog();
                }

                break;
            case R.id.ib_menu:

                checkValidations();

                break;

            case R.id.tv_hireDate:

                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
                // Get Current Date
                Date display = null;
                try{
                    display = sdf.parse(tvHiredte.getText().toString());
                } catch(ParseException e){
                    e.printStackTrace();
                }
                calendar = UiUtils.DateToCalendar(display);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){
                                //String selectedDate= dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                String selectedDate = checkDigit(dayOfMonth) + "-" + checkDigit(monthOfYear + 1) + "-" + year;
                                tvHiredte.setText(UiUtils.getFormattedDate(selectedDate, "dd-MM-yyyy"));
                                isNeedDateChange = true;
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            case R.id.et_fpgLevel:
                levelDialog.show();
                break;
            default:
                break;
        }
    }

    public String checkDigit(int number)
    {
        return number<=9?"0"+number:String.valueOf(number);
    }
    public void ImagePickerDialog(){
        final CharSequence[] items;
        items = new CharSequence[]{"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Options");
        builder.setItems(items, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int item){

                if(items[item].equals("Take Photo")){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if(items[item].equals("Choose from Gallery")){
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, REQUEST_GALLERY);
                } else if(items[item].equals("Cancel")){
                    dialog.dismiss();
                }
            }
        });
        AlertDialog dialog;
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }


    private Uri getOutputMediaFileUri(int mediaTypeImage){
        // TODO Auto-generated method stub
        return Uri.fromFile(getOutputMediaFile(mediaTypeImage));
    }

    private File getOutputMediaFile(int mediaTypeImage){
        // TODO Auto-generated method stub

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "FPG");

        // Create the storage directory if it does not exist
        if(!mediaStorageDir.exists()){
            if(!mediaStorageDir.mkdirs()){
                Log.d("FPG", "Oops! Failed create "
                        + "FPG" + " directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if(mediaTypeImage == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else{
            return null;
        }
        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_GALLERY){
            if(resultCode == Activity.RESULT_OK){
                previewGalleryImage(data);
            } else if(resultCode == Activity.RESULT_CANCELED){

            }
        } else if(requestCode == REQUEST_CAMERA){
            if(resultCode == Activity.RESULT_OK){
                previewCameraImage();

            } else if(resultCode == Activity.RESULT_CANCELED){

            }
        }
    }

    private void previewGalleryImage(Intent data){
        try{
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            //Get the cursor
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            selectedImagePath = cursor.getString(columnIndex);
            cursor.close();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            img = UiUtils.decodeSampledBitmapFromFile(selectedImagePath, 100, 100);
            img.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);

            byte[] byteArray = byteArrayOutputStream.toByteArray();
            encodedBase64String = Base64.encodeToString(byteArray, Base64.DEFAULT);
            isNeedUserProfileUpdate = true;

            img_profile.setImageBitmap(img);
        } catch(NullPointerException e){
            e.printStackTrace();
        }


    }

    private void previewCameraImage(){
        try{
            selectedImagePath = fileUri.getPath();
            img = UiUtils.decodeSampledBitmapFromFile(fileUri.getPath(), 100, 100);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            img.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);

            byte[] byteArray = byteArrayOutputStream.toByteArray();
            encodedBase64String = Base64.encodeToString(byteArray, Base64.DEFAULT);
            isNeedUserProfileUpdate = true;

            Logger.Error("<<< Base 64 >>> " + encodedBase64String);
            img_profile.setImageBitmap(img);
        } catch(NullPointerException e){
            e.printStackTrace();
        }

    }


    class TextChange implements TextWatcher{
        View view;

        private TextChange(View v){
            view = v;
        }//end constructor

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after){

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count){
            switch(view.getId()){
                case R.id.et_firstName:
                    if(etFirstName.getText().toString().trim().length() == 0){
                        mTextInputLayoutfirstName.setError(getString(R.string.error_empty_fname));
                        return;
                    } else{
                        mTextInputLayoutfirstName.setError(null);
                        mTextInputLayoutfirstName.setErrorEnabled(false);
                    }
                    break;
                case R.id.et_lastName:
                    if(etLastName.getText().toString().trim().length() == 0){
                        mTextInputLayoutlastName.setError(getString(R.string.error_empty_lname));
                        return;
                    } else{
                        mTextInputLayoutlastName.setError(null);
                        mTextInputLayoutlastName.setErrorEnabled(false);
                    }
                    break;
                case R.id.edt_phone_number:
                    if(etMobileNumber.getText().toString().trim().length() == 0){
                        mTextInputLayoutnumber.setError(getString(R.string.error_empty_phone));
                        return;
                    } else{
                        mTextInputLayoutnumber.setError(null);
                        mTextInputLayoutnumber.setErrorEnabled(false);
                    }
                    break;
                case R.id.et_empId:
                    if(etEmpId.getText().toString().trim().length() == 0){
                        mTextInputLayoutempId.setError(getString(R.string.error_empty_empId));
                        return;
                    } else{
                        mTextInputLayoutempId.setError(null);
                        mTextInputLayoutempId.setErrorEnabled(false);
                    }
                    break;
                case R.id.et_roles:
                    if(etRoles.getText().toString().trim().length() == 0){
                        mTextInputLayoutroles.setError(getString(R.string.error_empty_roles));
                        return;
                    } else{
                        mTextInputLayoutroles.setError(null);
                        mTextInputLayoutroles.setErrorEnabled(false);
                    }

                    break;
                /*case R.id.edt_hireDate:
                    if(tvHiredte.getText().toString().trim().length() == 0){
                        mTextInputLayouthireDate.setError(getString(R.string.error_empty_organization));
                        return;
                    } else{
                        mTextInputLayouthireDate.setError(null);
                        mTextInputLayouthireDate.setErrorEnabled(false);
                    }

                    break;*/
                case R.id.et_email:
                    String email = etEmail.getText().toString().trim();
                    if(email.isEmpty() || !UiUtils.isValidEmail(email)){
                        mTextInputLayoutemail.setError(getString(R.string.error_empty_email));
                        // requestFocus(mTextInputLayoutEmail);
                        return;
                    } else{
                        mTextInputLayoutemail.setError(null);
                        mTextInputLayoutemail.setErrorEnabled(false);
                    }
                    break;


            }

        }

        @Override
        public void afterTextChanged(Editable editable){

        }
    }

    private static void setViewAndChildrenEnabled(View view, boolean enabled){
        view.setEnabled(enabled);
        if(view instanceof ViewGroup){
            ViewGroup viewGroup = (ViewGroup) view;
            for(int i = 0; i < viewGroup.getChildCount(); i++){
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibBack.setVisibility(View.VISIBLE);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvApply.setVisibility(View.VISIBLE);
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
        mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.user_profile_save));
        mHomeActivity.tvTitle.setText("Update Profile");
        mHomeActivity.tvApply.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                checkValidations();

            }
        });
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
    }

    public Long getServerDateFormat(String inputDateStr){

        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat inputFormat = new SimpleDateFormat("MMM dd ,yyyy");
        try{
            Date inputDate = inputFormat.parse(inputDateStr);                 // parse input
            String stroutputFormat = outputFormat.format(inputDate);
            outputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date mDate = outputFormat.parse(stroutputFormat);


            long timeInMilliseconds = mDate.getTime();

            return timeInMilliseconds;    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public void updateAvatar(String imageBase64){
        String userId = String.valueOf(UiUtils.getUserId(mHomeActivity));
        String industryId = String.valueOf(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0));
        Call mCall = apiInterface.updateAvatar(industryId, userId, imageBase64);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                mHomeActivity.endProgress();

                ResponseBody mResponseBody = (ResponseBody) response.body();
                try{
                    Logger.Error("<<<< Response " + mResponseBody.string());
                    // JSONObject responseJson = new JSONObject(mResponseBody.string());
                    //Logger.Error("<<<< Response String " + responseJson.toString());
                    if(isPasswordChange){
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        AlertDialog dialog;
                        builder.setTitle("");
                        builder.setMessage(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_password_change_message)));
                        builder.setCancelable(false);
                        builder.setPositiveButton(
                                getActivity().getResources().getString(R.string.alert_ok),
                                new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                        dialog.dismiss();
                                        mHomeActivity.ForceLogout();
                                    }
                                });

                        //  builder.show();
                        dialog = builder.create();
                        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        dialog.show();
                    } else{
                        String messagePorfileUpdate = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_mail_key_profile_updated));
                        Toast.makeText(mHomeActivity, messagePorfileUpdate, Toast.LENGTH_SHORT).show();
                        mHomeActivity.onBackPressed();
                    }
                } catch(IOException e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
            }
        });
    }
}
