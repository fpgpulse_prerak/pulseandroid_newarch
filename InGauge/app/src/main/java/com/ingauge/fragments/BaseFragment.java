package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.SimpleShowcaseEventListener;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.fragments.dateselection.DateSelectionFragmentForObsCC;
import com.ingauge.fragments.leaderboard.FragmentEntityList;
import com.ingauge.fragments.leaderboard.FragmentLeaderBoard;
import com.ingauge.fragments.leaderboard.LbFragmentFilterDynamicList;
import com.ingauge.fragments.leaderboard.LeaderboardDetailFragment;
import com.ingauge.fragments.pulsemail.FragmentComposeMail;
import com.ingauge.fragments.pulsemail.FragmentEmailDetailsNew;
import com.ingauge.fragments.pulsemail.FragmentInbox;
import com.ingauge.fragments.socialinteraction.FragmentSocialInteractionLocationFilter;
import com.ingauge.fragments.socialinteraction.FragmentSocialInteractionLocationGroupFilter;
import com.ingauge.fragments.socialinteraction.FragmentSocialInteractionUserFilter;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

/**
 * Created by mansurum on 12-Jun-17.
 */

public class BaseFragment extends Fragment{
    HomeActivity mHomeActivity;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        onAttachFragment(this);
    }

    @Override
    public void onResume(){
        super.onResume();


        if(this instanceof PreferenceDynamicFragment || this instanceof DashboardDetailFragment ||
                this instanceof DashboardDetailSocialInteractionFragment ||
                this instanceof FragmentFilterDynamicList || this instanceof GoalSettings ||
                this instanceof FragmentFilterDynamicListForGoal ||
                this instanceof FragmentInbox || this instanceof FragmentComposeMail || this instanceof FragmentEmailDetailsNew
                || this instanceof DateFilterFragment || this instanceof FragmentSocialInteractionLocationFilter
                || this instanceof FragmentSocialInteractionLocationGroupFilter || this instanceof FragmentSocialInteractionUserFilter
                || this instanceof ObservationAgentFragment || this instanceof ObservationDashboardFragment
                || this instanceof ObservationDynamicFilterFragment
                || this instanceof FilterDetailListFragmentForObservation
                || this instanceof FragmentLeaderBoard
                || this instanceof FragmentEntityList
                || this instanceof LbFragmentFilterDynamicList
                || this instanceof LeaderboardDetailFragment
                || this instanceof DateSelectionFragment
                || this instanceof DateSelectionFragmentForObsCC){

            mHomeActivity.ibBack.setVisibility(View.VISIBLE);
            mHomeActivity.tvPreferences.setVisibility(View.GONE);

        } else{
            mHomeActivity.ibBack.setVisibility(View.GONE);
            mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
        }

        final Fragment mFragment = this;

        /*new Handler().postDelayed(new Runnable(){

            @Override
            public void run(){*/
                try{

                    if(mFragment instanceof FeedsFragment){
                        mHomeActivity.tvTitle.setText("");
                        mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.menu_feeds));
                        mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                        mHomeActivity.tvSubTitle.setText(InGaugeSession.read(getString(R.string.key_selected_tenant), ""));
                        mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    } else if(mFragment instanceof VideoFragment){
                        mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.menu_video));
                        // mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                    } else if(mFragment instanceof MoreFragment){

                        mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_title_more)));
                        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
                        mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    } else if(mFragment instanceof ObservationDashboardFragment){
                        if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.survey_entity_type_id_observation))){
                            mHomeActivity.tvTitle.setText("Observation");
                        } else{
                            mHomeActivity.tvTitle.setText("Coaching");
                        }
                        mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    } else if(mFragment instanceof ObservationAgentFragment){
                        if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), null).equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.survey_entity_type_id_observation))){
                            mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_title_observe_agent)));
                        } else{
                            mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_title_coaching_team_member)));
                        }
                        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
                        mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    } else if(mFragment instanceof DashboardFragmentNew){
                        mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                        //mHomeActivity.tvSubTitle.setVisibility(View.GONE);
                    }else  if(mFragment instanceof FragmentEntityList || mFragment instanceof FragmentLeaderBoard){
                        mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                    }else  if(mFragment instanceof PreferenceDynamicFragment || mFragment instanceof LbFragmentFilterDynamicList || mFragment instanceof FragmentFilterDynamicList){
                        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
                    }
                    else{
                        //No to set any title and subtitle
                    }

                } catch(Exception e){
                    e.printStackTrace();
                }
        /*    }
        }, 1500);*/
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onAttachFragment(Fragment childFragment){
        super.onAttachFragment(childFragment);
        Logger.Error("<<< Child Fragment >>>" + childFragment.toString());
        /*if(childFragment instanceof FragmentInbox){
            mHomeActivity.tvApply.setVisibility(View.VISIBLE);
            mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.delete));
        }
        else{
            mHomeActivity.tvApply.setVisibility(View.GONE);
            mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.btn_apply_filter));
        }*/
    }



    /* public RegionTypeDataModel getmRegionTypeDataModel() {
        return mRegionTypeDataModel;
    }

    public void setmRegionTypeDataModel(RegionTypeDataModel mRegionTypeDataModel) {
        this.mRegionTypeDataModel = mRegionTypeDataModel;
    }

    public RegionByTenantRegionTypeModel getmRegionByTenantRegionTypeModel() {
        return mRegionByTenantRegionTypeModel;
    }

    public void setmRegionByTenantRegionTypeModel(RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel) {
        this.mRegionByTenantRegionTypeModel = mRegionByTenantRegionTypeModel;
    }

    public AccessibleTenantLocationDataModel getmAccessibleTenantLocationDataModel() {
        return mAccessibleTenantLocationDataModel;
    }

    public void setmAccessibleTenantLocationDataModel(AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel) {
        this.mAccessibleTenantLocationDataModel = mAccessibleTenantLocationDataModel;
    }

    public DashboardLocationGroupListModel getmDashboardLocationGroupListModel() {
        return mDashboardLocationGroupListModel;
    }

    public void setmDashboardLocationGroupListModel(DashboardLocationGroupListModel mDashboardLocationGroupListModel) {
        this.mDashboardLocationGroupListModel = mDashboardLocationGroupListModel;
    }

    public ProductListFromLocationGroupModel getmProductListFromLocationGroupModel() {
        return mProductListFromLocationGroupModel;
    }

    public void setmProductListFromLocationGroupModel(ProductListFromLocationGroupModel mProductListFromLocationGroupModel) {
        this.mProductListFromLocationGroupModel = mProductListFromLocationGroupModel;
    }

    public DashboardUserListModel getmDashboardUserListModel() {
        return mDashboardUserListModel;
    }

    public void setmDashboardUserListModel(DashboardUserListModel mDashboardUserListModel) {
        this.mDashboardUserListModel = mDashboardUserListModel;
    }*/

    public void showTenantGuide(){
        ShowcaseView sv = new ShowcaseView.Builder(mHomeActivity)
                // .withMaterialShowcase()
                .setTarget(new ViewTarget(mHomeActivity.tvPreferences))
                .blockAllTouches()
                .doNotBlockTouches()
                .setStyle(R.style.CustomShowcaseTheme2)
                .setContentTitle("Change Tenant/Industry")
                .setContentText(mHomeActivity.getResources().getString(R.string.str_guide_change_tenant_industry))
                .setShowcaseEventListener(new SimpleShowcaseEventListener(){

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView){
                        super.onShowcaseViewDidHide(showcaseView);
                        showDashboardFilterGuide();
                    }
                })
                .build();
    }

    public void showDashboardFilterGuide(){

        ShowcaseView sv = new ShowcaseView.Builder(mHomeActivity)
                //.withMaterialShowcase()
                .doNotBlockTouches()
                .setTarget(new ViewTarget(mHomeActivity.tvTitle))
                .blockAllTouches()
                .setStyle(R.style.CustomShowcaseTheme2)
                .setContentTitle("Dashboard Filter")
                .setContentText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_guide_change_dashboard_filter)))
                .setShowcaseEventListener(new SimpleShowcaseEventListener(){

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView){
                        super.onShowcaseViewDidHide(showcaseView);
                        showDashboardMenuGuide();
                    }
                })
                .build();
    }

    public void showDashboardMenuGuide(){


        ShowcaseView sv = new ShowcaseView.Builder(mHomeActivity)
//                .withMaterialShowcase()
                .setTarget(new ViewTarget(mHomeActivity.ibMenu))
                .blockAllTouches()
                .setStyle(R.style.CustomShowcaseTheme2)
                .setContentTitle("Change Dashboard")
                .setContentText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_guide_change_dashboard_name)))
                .setShowcaseEventListener(new SimpleShowcaseEventListener(){

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView){
                        super.onShowcaseViewDidHide(showcaseView);
                    }
                })
                .build();
    }
}
