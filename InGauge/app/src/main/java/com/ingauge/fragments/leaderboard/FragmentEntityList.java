package com.ingauge.fragments.leaderboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.EntityListAdapter;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.fragments.FragmentFilterDynamicList;
import com.ingauge.listener.LeaderboardClickListener;
import com.ingauge.pojo.CustomDashboardModel;
import com.ingauge.utils.Logger;
import com.ingauge.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 01-Feb-18.
 */

public class FragmentEntityList extends BaseFragment implements LeaderboardClickListener{


    HomeActivity mHomeActivity;
    View rootView;

    private TextView tvNoreportFound;
    public RecyclerView mRecyclerView;
    EntityListAdapter mEntityListAdapter;

    private List<CustomDashboardModel.CustomReport> mCustomReportList = new ArrayList<>();


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_entity_list, container, false);

        Logger.Error("<<< Call Fragmetn Entity List>>>>");
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleview);
        tvNoreportFound = (TextView) rootView.findViewById(R.id.fragment_entity_tv_no_video_found);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        //setEntityListAdapter(mCustomReportList);
        return rootView;
    }

    public void setEntityListAdapter(List<CustomDashboardModel.CustomReport> mList){
        mCustomReportList = mList;
        if(mCustomReportList != null && mCustomReportList.size() > 0){

            mRecyclerView.setVisibility(View.VISIBLE);
            tvNoreportFound.setVisibility(View.GONE);

            mEntityListAdapter = new EntityListAdapter(mHomeActivity, mCustomReportList, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(mHomeActivity));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            /*if(mEntityListAdapter !=null){
                mEntityListAdapter.notifyDataSetChanged();
            }else{*/
            mRecyclerView.setAdapter(mEntityListAdapter);
            /*}*/

        } else{
            mRecyclerView.setVisibility(View.GONE);
            tvNoreportFound.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void getClickData(int position, String name, int reportId){

        LeaderboardDetailFragment mLeaderboardDetailFragment = new LeaderboardDetailFragment();
        Bundle mBundle = new Bundle();
        mBundle.putInt(LeaderboardDetailFragment.KEY_REPORT_ID,reportId);
        mBundle.putString(LeaderboardDetailFragment.KEY_REPORT_Name,name);
        mLeaderboardDetailFragment.setArguments(mBundle);
        mHomeActivity.replace(mLeaderboardDetailFragment, mHomeActivity.getResources().getString(R.string.tag_leader_board_details));
    }

    @Override
    public void onResume(){
        super.onResume();
        setToolbarTitles();


    }
    public void setToolbarTitles(){
        if(mHomeActivity.isTenantLocation()){
            mHomeActivity.tvTitle.setText("Leaderboard" + " (Location)");
        } else{
            mHomeActivity.tvTitle.setText("Leaderboard" + " (Region)");
        }


        String locationName = mHomeActivity.getSelectedTenantLocationNameForLb();
        String userName = mHomeActivity.getSelectedUserNameForLB();
        String productName = mHomeActivity.getSelectedProductNameLb();
        String countryName = mHomeActivity.getSelectedRegionNameForLb();


        mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
        if(mHomeActivity.isTenantLocationForLb() && mHomeActivity.isUserForLb()){
            mHomeActivity.tvSubTitle.setText(locationName + "-" + userName);
            return;
        }
        if(mHomeActivity.isTenantLocationForLb() && mHomeActivity.isProductForLb()){
            mHomeActivity.tvSubTitle.setText(locationName + "-" + productName);
            return;
        }
        if(mHomeActivity.isTenantLocationForLb()){
            mHomeActivity.tvSubTitle.setText(locationName);
        } else{
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }

        if(countryName.length() > 0){
            mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
        }
        mHomeActivity.tvSubTitle.setText(countryName);


    }
}
