package com.ingauge.fragments.socialinteraction;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.TableReportAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.TableReportExpandItemPojo;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by mansurum on 20-Nov-17.
 */

public class DialogPostComment extends Dialog implements View.OnClickListener {


    private Context mContext;
    private EditText etComment;
    private TextView tvCancel;
    private TextView tvPostCommnet;
    APIInterface apiInterfaceForShareReport;
    TableReportExpandItemPojo childObject;
    HomeActivity mHomeActivity;
    private String reportId="";

    Call<ResponseBody> mCall;

    TableReportAdapter mReportAdapter;
    int groupPosition;
    int childPosition;
    Map<Integer, List<TableReportExpandItemPojo>> map;

    public DialogPostComment(@NonNull Context context, TableReportExpandItemPojo chTableReportExpandItemPojo, String reportId, TableReportAdapter mTableReportAdapter,
                             int groupPosition,int childPosition,Map<Integer, List<TableReportExpandItemPojo>> map) {
        super(context);
        this.mContext = context;
        mHomeActivity = (HomeActivity)mContext;
        apiInterfaceForShareReport = APIClient.getClientChartWithoutHeaders().create(APIInterface.class);
        this.childObject = chTableReportExpandItemPojo;
        this.reportId = reportId;
        this.mReportAdapter =mTableReportAdapter;
        this.groupPosition = groupPosition;
        this.childPosition = childPosition;
        this.map = map;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_post_comment);
        initViews();

    }

    private void initViews() {
        etComment = (EditText) findViewById(R.id.dialog_post_comment_et);
        tvCancel = (TextView) findViewById(R.id.dialog_post_comment_tv_cancel);
        tvPostCommnet = (TextView) findViewById(R.id.dialog_post_comment_tv_post);
        tvCancel.setOnClickListener(this);
        tvPostCommnet.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_post_comment_tv_post:
                if(etComment.getText().toString().trim().length()>0){
                    postComment(etComment.getText().toString().trim());
                }else{
                    Toast.makeText(mHomeActivity,"Enter Comment First",Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.dialog_post_comment_tv_cancel:
                dismiss();
                break;
        }
    }

    public void postComment(String commentText){

        Logger.Error("<<<<<  Child Object >>>>" + childObject);
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("socIntType", 1);
        requestBody.addProperty("entityType",childObject.entityType);
        requestBody.addProperty("entityId",childObject.entityId);
        requestBody.addProperty("entityName",childObject.entityName);
        requestBody.addProperty("value",childObject.reportValue);
        requestBody.addProperty("reportId",childObject.reportIdForServer);
        requestBody.addProperty("reportName",childObject.reportNameForServer);
        /*requestBody.addProperty("dataStartDate",childObject.dataStartDate);
        requestBody.addProperty("dataEndDate",childObject.dataEndDate);*/

        if(childObject.startDateCheck.length()>0){
            requestBody.addProperty("dataStartDate", childObject.startDateCheck.trim());
        }else{
            requestBody.addProperty("dataStartDate", childObject.dataStartDate.trim());
        }
        if(childObject.endDateCheck.length()>0){
            requestBody.addProperty("dataEndDate", childObject.endDateCheck.trim());
        }else{
            requestBody.addProperty("dataEndDate", childObject.dataEndDate.trim());
        }



        if(childObject.compDataStartDate.length()>0){
            requestBody.addProperty("compDataStartDate",childObject.compDataStartDate);
        }
        if(childObject.compDataEndDate .length()>0){
            requestBody.addProperty("compDataEndDate ",childObject.compDataEndDate);
        }
        if(childObject.startDateCheck.length()>0){
            requestBody.addProperty("dataColumnStartDate",childObject.startDateCheck);
        }
        if(childObject.endDateCheck.length()>0){
            requestBody.addProperty("dataColumnEndDate",childObject.endDateCheck);
        }
        requestBody.addProperty("comment",commentText);
        mHomeActivity.startProgress(mHomeActivity);
        mCall = apiInterfaceForShareReport.postLikeComment(Integer.parseInt(reportId),
                InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -2),
                InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0),
                InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), ""),
                requestBody);
        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mHomeActivity.endProgress();

                List<TableReportExpandItemPojo> mTableReportExpandItemPojos= map.get(groupPosition);
                TableReportExpandItemPojo mTableReportExpandItemPojo = map.get(groupPosition).get(childPosition);
                mTableReportExpandItemPojo.commentCount = mTableReportExpandItemPojo.commentCount+ 1;
                mTableReportExpandItemPojos.set(childPosition,mTableReportExpandItemPojo);
                map.put(groupPosition, mTableReportExpandItemPojos);
                mReportAdapter.notifyDataSetChanged();
                mReportAdapter.notifyDataSetInvalidated();
                dismiss();
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mHomeActivity.endProgress();
                dismiss();
            }
        });

    }
}
