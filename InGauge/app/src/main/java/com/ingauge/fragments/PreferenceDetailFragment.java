package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.BaseActivity;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.Listadapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.DefaultFilter;
import com.ingauge.pojo.DynamicData;
import com.ingauge.pojo.IndustryDataModel;
import com.ingauge.pojo.TenantDataModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DatabaseHandler;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by desainid on 5/17/2017.
 */

public class PreferenceDetailFragment extends BaseFragment {
    Context mContext;
    List<String> values;
    EditText ed;

    ListView listview;
    Listadapter listadapter;
    SearchView searchView;
    HomeActivity mHomeActivity;
    BaseActivity mBaseActivity;
    APIInterface apiInterface;
    List<IndustryDataModel.Datum> industryList;
    List<TenantDataModel.Data> tenantList;
    private TextView tvNoData;
    public boolean isFromTenantDetail = false;
    JSONObject mDataJson = null;
    List<DynamicData> mCountryList;

    public static Fragment newInstance(String text, int color) {
        Fragment frag = new PreferenceDetailFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
        args.putString("selected_preference", text);
        //  args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);

        mCountryList = new ArrayList<>();
        if (InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Industry")) {
            //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_industry)));
            mHomeActivity.tvTitle.setText("Select Industry");

        } else if (InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Tenant")) {
            //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_tenant)));
            mHomeActivity.tvTitle.setText("Select Tenant");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_preference_detail, container, false);
        initViews(rootView);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Industry")) {
            // GetIndustryAPI();
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), true);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call_goal), true);
            GetIndustry(InGaugeSession.read(getString(R.string.key_auth_token), ""));
        } else if (InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Tenant")) {
            //GetTenantAPI();
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), true);
            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call_goal), true);
       ///     GetTanent(InGaugeSession.read(getString(R.string.key_auth_token), ""), UiUtils.getUserId(mContext), UiUtils.getIndustryId(mContext), false);
        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                PreferencesFragment fragment = new PreferencesFragment();
                Bundle args = new Bundle();
                String item = listadapter.getItem(position);
                int positionFiltered = values.indexOf(item.toString());
                position = positionFiltered;

                if (InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Industry")) {
                    InGaugeSession.write(getString(R.string.key_selected_industry), industryList.get(positionFiltered).getDescription());
                    InGaugeSession.write(getString(R.string.key_selected_industry_id), industryList.get(position).getId());
                    args.putString("selectedValue", industryList.get(position).getName());
                  //  GetTanent(InGaugeSession.read(getString(R.string.key_auth_token), ""), UiUtils.getUserId(mContext), industryList.get(position).getId(), true);
                    isFromTenantDetail = true;

                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("industry_name", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry), ""));
                    InGaugeApp.getFirebaseAnalytics().logEvent("industry_change", bundle);
                } else if (InGaugeSession.read(getString(R.string.fragment_preference_detail), "").equals("Tenant")) {
                    InGaugeSession.write(getString(R.string.key_selected_tenant), tenantList.get(position).getTenantName());
                    InGaugeSession.write(getString(R.string.key_selected_tenant_id), tenantList.get(position).getTenantId());
                    InGaugeSession.write(getString(R.string.key_tenant_role_name), "" + tenantList.get(position).getRoleName());
                    InGaugeSession.write(getString(R.string.key_user_role_id), tenantList.get(position).getRoleId());
                    args.putString("selectedValue", tenantList.get(position).getTenantName());
                    Bundle bundle = new Bundle();
                    bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                    bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                    bundle.putString("tenant_name", InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant), ""));
                    InGaugeApp.getFirebaseAnalytics().logEvent("tenant_change", bundle);
                    getDefaultFilter(InGaugeSession.read(getString(R.string.key_auth_token), ""), tenantList.get(position).getTenantId());
                }

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                listadapter.getFilter().filter(newText);
                return false;
            }
        });
        return rootView;
    }


    void GetIndustry(String authToken) {
        //mBaseActivity.startProgress(mContext);
        Call mCall = apiInterface.getIndustry(UiUtils.getUserId(mContext));
        industryList = new ArrayList<>();
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    IndustryDataModel getIndstrydata = (IndustryDataModel) response.body();
                    industryList = getIndstrydata.getData();
                    Collections.sort(industryList, new Comparator<IndustryDataModel.Datum>() {
                        @Override
                        public int compare(IndustryDataModel.Datum datum, IndustryDataModel.Datum t1) {
                            return datum.getDescription().compareTo(t1.getDescription());

                        }
                    });
                    for (IndustryDataModel.Datum datum : industryList) {
                        values.add(datum.getDescription());


                    }
                    //Collections.sort(values);
                    listadapter = new Listadapter(getActivity(), values, true, false,"");
                    listview.setAdapter(listadapter);
                    if (values.size() > 0) {
                        listadapter = new Listadapter(getActivity(), values, true, false,"");
                        listview.setAdapter(listadapter);
                        tvNoData.setVisibility(View.GONE);
                    } else {
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText(mContext.getResources().getString(R.string.tv_no_industry));
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mBaseActivity.endProgress();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO: Add code to remove fragment here
        super.onSaveInstanceState(outState);
    }

    /*void GetTanent(final String authToken, int userId, final int industryId, final boolean isFromList) {
        mBaseActivity.startProgress(mContext);
        final Call mCall = apiInterface.getTenant(userId);
        tenantList = new ArrayList<>();
        values = new ArrayList<>();
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null) {
                    TenantDataModel tenantdataModel = (TenantDataModel) response.body();
                    tenantList = tenantdataModel.getData();
                    Collections.sort(tenantList, new Comparator<TenantDataModel.Data>() {
                        @Override
                        public int compare(TenantDataModel.Data datum, TenantDataModel.Data t1) {
                            return datum.getTenantName().compareTo(t1.getTenantName());

                        }
                    });

                    for (TenantDataModel.Data datum : tenantList) {
                        values.add(datum.getTenantName());
                    }
                    if (tenantList.size() > 0) {
                        InGaugeSession.write(getString(R.string.key_selected_tenant), tenantList.get(0).getTenantName());
                        InGaugeSession.write(getString(R.string.key_selected_tenant_id), tenantList.get(0).getTenantId());
                        InGaugeSession.write(getString(R.string.key_tenant_role_name), "" + tenantList.get(0).getRoleName());
                        InGaugeSession.write(getString(R.string.key_user_role_id), tenantList.get(0).getRoleId());
                        Logger.Error(" <<<<<   ROLE ID    in  Preference Detail>>>>>>" + tenantList.get(0).getRoleId());
                        tvNoData.setVisibility(View.GONE);
                        //  getDefaultFilter(authToken,tenantList.get(0).getTenantId());
                        if (!isFromList) {
                            // Collections.sort(values);
                            mBaseActivity.endProgress();
                            listadapter = new Listadapter(getActivity(), values, true, false,"");
                            listview.setAdapter(listadapter);
                        } else {
                            getMobileKeys(String.valueOf(industryId));
                        }

                    } else {
                        mBaseActivity.endProgress();
                        if (!isFromList) {
                            tvNoData.setVisibility(View.VISIBLE);
                            tvNoData.setText(mContext.getResources().getString(R.string.tv_no_tenant));

                        } else {
                            InGaugeSession.write(getString(R.string.key_selected_tenant), "");
                            InGaugeSession.write(getString(R.string.key_selected_tenant_id), 0);
                            InGaugeSession.write(getString(R.string.key_tenant_role_name), "");
                            mHomeActivity.onBackPressed();
                        }

                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mBaseActivity.endProgress();
            }
        });
    }*/

    void getMobileKeys(String IndustryId) {
        Call mCall = apiInterface.getMobileKeys();
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                JSONObject data = null;
                if (response.body() != null) {
                    Logger.Error("KEYS RESPONSE " + response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        data = jsonObject.optJSONObject("data");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    DatabaseHandler db = new DatabaseHandler(mContext);
                    db.addKeys(String.valueOf(data));
                    mHomeActivity.mobilekeyObject = null;
                    mHomeActivity.mStringListMetricType.clear();
                    mHomeActivity.mobilekeyObject = UiUtils.getMobileKeys(mHomeActivity);

                    mHomeActivity.mStringKeyListMetricType = Arrays.asList(mHomeActivity.getResources().getStringArray(R.array.key_metric_type_array));
                    // mStringListMetricType.clear();
                    for (int i = 0; i < mHomeActivity.mStringKeyListMetricType.size(); i++) {

                        mHomeActivity.mStringListMetricType.add(mHomeActivity.mobilekeyObject.optString(mHomeActivity.mStringKeyListMetricType.get(i)));
                    }
                    getDynamicFilterMap(InGaugeSession.read(getString(R.string.key_selected_tenant_id), tenantList.get(0).getTenantId()),UiUtils.getUserId(mContext),false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    void getDynamicFilterMap(int tenantId, int userID, boolean isShowDeactivatedLocation) {
        Call<ResponseBody> mCall = apiInterface.getDynamicFiltersListMap(tenantId, userID, isShowDeactivatedLocation);
        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {


                JSONObject data = null;
                if (response.body() != null) {
                    try {

                        JSONObject mJsonObject = new JSONObject(response.body().string());
                        mDataJson = mJsonObject.optJSONObject("data");
                        JSONArray firstJson = mDataJson.optJSONArray("-1");
                        //Map<Integer,JSONArray> integerJSONArrayMap  = new HashMap<>();

                        Map<Integer, List<DynamicData>> integerListHashMap = new LinkedHashMap<>();
                        int level = 0;
                        for (int i = 0; i < firstJson.length(); i++) {
                            DynamicData mDynamicData = new DynamicData();
                            mDynamicData.setId(firstJson.getJSONObject(i).optInt("id"));
                            mDynamicData.setName(firstJson.getJSONObject(i).optString("name"));
                            mDynamicData.setRegionTypeName(firstJson.getJSONObject(i).optString("regionTypeName"));
                            mDynamicData.setRegionTypeId(firstJson.getJSONObject(i).optInt("regionTypeId"));
                            mDynamicData.setChildRegion(firstJson.getJSONObject(i).optString("childRegion"));
                            mDynamicData.setParentRegionId(firstJson.getJSONObject(i).optInt("parentRegionId"));
                            mCountryList.add(mDynamicData);
                        }
                        integerListHashMap.put(level, mCountryList);
                        for (DynamicData mDynamicDataParent : mCountryList) {
                            if (mDynamicDataParent.getChildRegion() != null && mDynamicDataParent.getChildRegion().length() > 0)
                                searchParent(mDynamicDataParent, integerListHashMap, level);
                        }

                        if (mHomeActivity.getIntegerListHashMap() != null && mHomeActivity.getIntegerListHashMap().size() > 0) {
                            mHomeActivity.getIntegerListHashMap().clear();
                            mHomeActivity.setIntegerListHashMap(integerListHashMap);
                        } else {
                            mHomeActivity.setIntegerListHashMap(integerListHashMap);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Logger.Error("<<< Exception  >>>>" + e.toString());
                    }catch (IOException e) {
                        e.printStackTrace();
                        Logger.Error("<<< Exception  >>>>" + e.toString());
                    }

                    mBaseActivity.endProgress();
                    mHomeActivity.onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void searchParent(DynamicData mDynamicDataParent, Map<Integer, List<DynamicData>> integerJSONArrayMap, int level) {
        try {
            Logger.Error("Map" + integerJSONArrayMap);
            if (mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId())) != null) {
                level++;
                JSONArray mJsonArray = mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId()));
                for (int i = 0; i < mJsonArray.length(); i++) {
                    DynamicData mDynamicData = new DynamicData();
                    mDynamicData.setId(mJsonArray.getJSONObject(i).optInt("id"));
                    mDynamicData.setName(mJsonArray.getJSONObject(i).optString("name"));
                    mDynamicData.setRegionTypeName(mJsonArray.getJSONObject(i).optString("regionTypeName"));
                    mDynamicData.setRegionTypeId(mJsonArray.getJSONObject(i).optInt("regionTypeId"));
                    mDynamicData.setChildRegion(mJsonArray.getJSONObject(i).optString("childRegion"));
                    mDynamicData.setParentRegionId(mJsonArray.getJSONObject(i).optInt("parentRegionId"));

                    if (integerJSONArrayMap.get(level) != null) {
                        List<DynamicData> mDynamicDataList = integerJSONArrayMap.get(level);
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level, mDynamicDataList);

                    } else {
                        List<DynamicData> mDynamicDataList = new ArrayList<>();
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level, mDynamicDataList);
                    }
                    searchParent(mDynamicData, integerJSONArrayMap, level);
                    //mCountryList.add(mDynamicData);
                }
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void GetTenantAPI() {
        Gson gson = new Gson();
        TenantDataModel tenantdataModel = gson.fromJson(UiUtils.loadJSONFromAsset("GetTenant.json", mContext), TenantDataModel.class);
        List<TenantDataModel.Data> tenantList = tenantdataModel.getData();
        for (TenantDataModel.Data datum : tenantList) {
            values.add(datum.getTenantName());
        }
        listadapter = new Listadapter(getActivity(), values, true, false,"");
        listview.setAdapter(listadapter);
    }

    private void GetIndustryAPI() {
        Gson gson = new Gson();
        IndustryDataModel getIndstrydata = gson.fromJson(UiUtils.loadJSONFromAsset("GetIndustry.json", mContext), IndustryDataModel.class);
        List<IndustryDataModel.Datum> data = getIndstrydata.getData();
        for (IndustryDataModel.Datum datum : data) {
            values.add(datum.getName());


        }
        listadapter = new Listadapter(getActivity(), values, true, false,"");
        listview.setAdapter(listadapter);


    }

    private void initViews(View rootView) {
        values = new ArrayList<>();
        listview = (ListView) rootView.findViewById(R.id.listViewPrefDetail);
        tvNoData = (TextView) rootView.findViewById(R.id.tv_nodata);
        listadapter = new Listadapter(getActivity(), values, true, false,"");
        listview.setTextFilterEnabled(true);
        listview.setAdapter(listadapter);
        searchView = (SearchView) rootView.findViewById(R.id.fragment_social_interaction_location_filter_sv);
        searchView.setIconifiedByDefault(true);

        searchView.setIconified(false);
        searchView.clearFocus();


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
        mBaseActivity = (BaseActivity) mContext;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    void getDefaultFilter(final String authToken, final int TenantID) {
        mHomeActivity.startProgress(mContext);
        Call mCall = apiInterface.getDefaultFilter(TenantID);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {

                    DefaultFilter defaultFilter = (DefaultFilter) response.body();

                    int RegionTypeId = defaultFilter.getData().getREGIONTYPE();
                    int RegionId = defaultFilter.getData().getREGION();
                    int TenantLocationId = defaultFilter.getData().getLOCATION();
                    int LocationGroupId = defaultFilter.getData().getLOCATIONGROUP();
                    int ProductId = defaultFilter.getData().getPRODUCT();
                    int UserID = defaultFilter.getData().getUSER();

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), RegionTypeId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), RegionId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), TenantLocationId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), LocationGroupId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), ProductId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), UserID);
                    getDefaultFilterForGoal(TenantID);
                }else{
                    mHomeActivity.endProgress();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                //endProgress();
                mHomeActivity.endProgress();
            }
        });
    }

    /**
     * get Default Filter For Goal
     */
    void getDefaultFilterForGoal(final int TenantID) {
        Call mCall = apiInterface.getDefaultFilterForGoal(TenantID, true);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {

                    DefaultFilter defaultFilter = (DefaultFilter) response.body();

                    //   int RegionTypeId = defaultFilter.getData().getREGIONTYPE();
                    // int RegionId = defaultFilter.getData().getREGION();
                    int TenantLocationId = defaultFilter.getData().getLOCATION();
                    int LocationGroupId = defaultFilter.getData().getLOCATIONGROUP();
                    int ProductId = defaultFilter.getData().getPRODUCT();
                    int UserID = defaultFilter.getData().getUSER();


                    /*InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_goal), TenantLocationId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_goal), String.valueOf(LocationGroupId));
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_goal), ProductId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_goal), UserID);*/

                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_tenant_location_id_for_default_goal), TenantLocationId);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_id_for_default_goal), String.valueOf(LocationGroupId));
                    //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_default_goal), ProductId);
                    //This -1 is put static Due to in Goal Setting Default Filter value in Product is Incremental revenue. As In Goal Settings Filter show default Incremental Revenue at first time
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_id_for_default_goal), -1);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_id_for_default_goal), UserID);


                    getRolesAndPermission("application/json", InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), null), true, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), 0)), String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_id), 0)), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2)));
                }else{
                    mHomeActivity.endProgress();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // endProgress();
                mHomeActivity.endProgress();
            }
        });
    }

    private void getRolesAndPermission(String ContentType, String Authorization, boolean isMobile, final String IndustryId, String TenantId, final String UserId) {
        Call mCall = apiInterface.getRolesAndPermission(TenantId, UserId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                JSONObject data = new JSONObject();
                if (response.body() != null) {
                    Logger.Error("ROLES AND PERMISSION RESPONSE " + response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        if (jsonObject.optString("status").equalsIgnoreCase("SUCCESS")) {
                            data = jsonObject.optJSONObject("data");
                        }
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_user_role_permission), data.toString());
                        isFromTenantDetail = true;
                        getDynamicFilterMap(InGaugeSession.read(getString(R.string.key_selected_tenant_id), tenantList.get(0).getTenantId()),UiUtils.getUserId(mContext),false);


                    } catch (JSONException e) {
                        e.printStackTrace();
                        mHomeActivity.endProgress();
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mHomeActivity.endProgress();
                Logger.Error("Exception " + t.getMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
    }
}
