package com.ingauge.fragments.dateselection;

/**
 * Created by mansurum on 07-Mar-18.
 */

public class MonthConstant{


    public static String January="January";
    public static String February="February";
    public static String March="March";
    public static String April="April";
    public static String May="May";
    public static String June="June";
    public static String July="July";
    public static String August="August";
    public static String September="September";
    public static String October="October";
    public static String November="November";
    public static String December="December";
}
