package com.ingauge.fragments.dateselection.tabsfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.fragments.dateselection.MonthConstant;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Created by mansurum on 15-Feb-18.
 */

public class MonthFragment extends Fragment implements View.OnClickListener{

    View rootView;


    TextView tvLastThirtyDays;
    TextView tvThisMonth;
    TextView tvLastMonth;
    TextView tvPreviousPeriod;
    TextView tvFourWeeksAgo;
    TextView tvPreviousYear;
    TextView tv52WeeksAgo;

    ImageView ivLastThirtyDays;
    ImageView ivThisMonth;
    ImageView ivLastMonth;
    ImageView ivPreviousPeriod;
    ImageView ivFourWeeksAgo;
    ImageView ivPreviousYear;
    ImageView iv52WeeksAgo;


    SwitchCompat mASwitchCompare;

    LinearLayout llLastThirtyDays;
    LinearLayout llThisMonth;
    LinearLayout llLastMonth;
    LinearLayout llPreviousPeriod;
    LinearLayout llFourWeeksAgo;
    LinearLayout llPreviousYear;
    LinearLayout ll52WeeksAgo;

    LinearLayout llCompare;


    public String startDate = "";


    public String CompareStartDate = "";


    //Server Dates
    public String serverStartDate = "";
    public String serverEndDate = "";
    public String serverCompareStartDate = "";
    public String serverCompareEndDate = "";


    public boolean isCompare = true;

    public int dateRangeIndex = -1;
    public int ComparedateRangeIndex = -1;


    String combinedDate = "";
    String combinedCompareDate = "";


    private int tabIndex = 2;
    LinearLayout llLeaderboardCompare;
    boolean isFromLeaderBoard = false;
    boolean isHideCompare = false;

    HomeActivity mHomeActivity;


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setTabIndex(2);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.month_fragment, container, false);
        initViews(rootView);


        if(getArguments() != null){
            Bundle mBundle = getArguments();
            /*startDate = mBundle.getString(DateSelectionFragment.KEY_START_DATE);
            endDate =  mBundle.getString(DateSelectionFragment.KEY_END_DATE);
            compareStartDate = mBundle.getString(DateSelectionFragment.KEY_COMPARE_START_DATE);
            compareEndDate =  mBundle.getString(DateSelectionFragment.KEY_COMPARE_END_DATE);
            serverStartDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_START_DATE);
            serverEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_END_DATE);
            serverCompareStartDate= mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_START_DATE);
            serverCompareEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_END_DATE);*/
            isCompare = mBundle.getBoolean(DateSelectionFragment.KEY_IS_COMPARE);
            isFromLeaderBoard = mBundle.getBoolean(DateSelectionFragment.KEY_IS_FROM_LEADERBOARD);
            isHideCompare  = mBundle.getBoolean(DateSelectionFragment.KEY_IS_HIDE_COMPARE);

        }

        dateRangeIndex = InGaugeSession.read(getString(R.string.key_from_date_range_index), -1);
        ComparedateRangeIndex = InGaugeSession.read(getString(R.string.key_compare_date_range_index), -1);

        if(isFromLeaderBoard ){
            llLeaderboardCompare.setVisibility(View.GONE);
        } else{
            llLeaderboardCompare.setVisibility(View.VISIBLE);
        }


        if(isHideCompare){
            llLeaderboardCompare.setVisibility(View.GONE);
        }else{
            llLeaderboardCompare.setVisibility(View.VISIBLE);
        }
        mASwitchCompare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isChecked){
                    llCompare.setVisibility(View.VISIBLE);
                    isCompare = true;
                    /*CompareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());*/
                    if(getComparedateRangeIndex() == -1){
                        setComparedateRangeIndex(1);
                    } else{
                        setComparedateRangeIndex(getComparedateRangeIndex());
                    }
                } else{
                    llCompare.setVisibility(View.GONE);
                    isCompare = false;
                }
            }
        });

        if(getDateRangeIndex() == -1){
            setDateRangeIndex(1);
            if(isCompare){
                if(getComparedateRangeIndex() == -1){
                    setComparedateRangeIndex(1);
                } else{
                    setComparedateRangeIndex(getComparedateRangeIndex());
                }
            }
        } else{
            setDateRangeIndex(getDateRangeIndex());
            if(isCompare){
                if(getComparedateRangeIndex() == -1){
                    setComparedateRangeIndex(1);
                } else{
                    setComparedateRangeIndex(getComparedateRangeIndex());
                }
            }
        }

        tvLastThirtyDays.setText(getLastThirtyDaysRange(new Date()));
        tvThisMonth.setText(getMonthDisplay(DateTimeUtils.getCurrentMonth()));
        tvLastMonth.setText(getMonthDisplay(DateTimeUtils.getLastMonth()));
        String onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());

        tvPreviousPeriod.setText(getPreviousMonthRangeFromLastThirtyDays(new Date()));
        tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(new Date()));
        tvPreviousYear.setText(getLastYearThirtyDaysRange(new Date()));
        tv52WeeksAgo.setText(getFiftyTwoWeeksAgoDateRangeofLastSevenDays(new Date()));

        if(dateRangeIndex == 1){
            startDate = tvLastThirtyDays.getText().toString();
            serverStartDate = getStartDateForServer(startDate);
            serverEndDate = getEndDateForServer(startDate);
            visibleRightTick(1);

            if(getComparedateRangeIndex() == 1){
                visibleCompareRightTick(1);
                CompareStartDate = tvPreviousPeriod.getText().toString();
                serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));
            } else if(getComparedateRangeIndex() == 2){
                visibleCompareRightTick(2);
                CompareStartDate = tvFourWeeksAgo.getText().toString();
                serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
            } else if(getComparedateRangeIndex() == 3){
                visibleCompareRightTick(3);
                CompareStartDate = tvPreviousYear.getText().toString();
                serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());

                Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));
            } else if(getComparedateRangeIndex() == 4){
                visibleCompareRightTick(4);
                CompareStartDate = tv52WeeksAgo.getText().toString();
                serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
            }
        } else if(dateRangeIndex == 2){
            startDate = getMonthDisplay(DateTimeUtils.getCurrentMonth());
            serverStartDate = getStartDateofThisMOnth();
            serverEndDate = getEndDateofThisMOnth();
            visibleRightTick(2);

            try{
                onedaybefore = DateTimeUtils.getLastDay(new Date());
                tvPreviousPeriod.setText(getPreviousMonthRangeFromLastThirtyDays(new Date()));

                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(getDateObj(onedaybefore)));

                String thisYear = tvThisMonth.getText().toString() + " " + new SimpleDateFormat("yyyy").format(new Date());
                Logger.Error("<<<<  YEAR  >>>> " + thisYear);
                Date mDateObj = getMonthYearDateObj(thisYear);
                tvPreviousYear.setText(getMonthYearDisplay(DateTimeUtils.getLastYear(mDateObj)));


                String firstDay = DateTimeUtils.getFirstDay(mDateObj);
                String lastDay = DateTimeUtils.getLastDay(mDateObj);


                Logger.Error("<<<<   From Day  >>>> " + getFromDateofFifityTwoDaysAgo(getDateObj(firstDay)));

                Logger.Error("<<<<   From Day  >>>> " + getFromDateofFifityTwoDaysAgo(getDateObj(lastDay)));
                String display = getDisplayStringForWeekTab(getFromDateofFifityTwoDaysAgo(getDateObj(firstDay))) + "-" +
                        getDisplayStringForWeekTab(getFromDateofFifityTwoDaysAgo(getDateObj(lastDay)));
                tv52WeeksAgo.setText(display);

                startDate = getMonthDisplay(DateTimeUtils.getCurrentMonth());
                serverStartDate = getStartDateofThisMOnth();
                serverEndDate = getEndDateofThisMOnth();

                if(getComparedateRangeIndex() == 1){
                    visibleCompareRightTick(1);
                    CompareStartDate = tvPreviousPeriod.getText().toString();
                    String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                    if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                        int year = Integer.parseInt(currentstrYear) - 1;
                        CompareStartDate = CompareStartDate + " " + String.valueOf(year);
                    } else{
                        CompareStartDate = CompareStartDate + " " + currentstrYear;
                    }

                    mDateObj = getMonthYearDateObj(CompareStartDate);
                    String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                    String serverToDate = DateTimeUtils.getLastDay(mDateObj);


                    serverCompareStartDate = serverFromDate;
                    serverCompareEndDate = serverToDate;
                        /*Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                        Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));*/
                } else if(getComparedateRangeIndex() == 2){
                    visibleCompareRightTick(2);
                    CompareStartDate = tvFourWeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                    Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                    Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
                } else if(getComparedateRangeIndex() == 3){
                    visibleCompareRightTick(3);
                    CompareStartDate = tvPreviousYear.getText().toString();

                    String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                    if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                        int year = Integer.parseInt(currentstrYear) - 1;
                        CompareStartDate = CompareStartDate + " " + String.valueOf(year);
                    } else{
                        CompareStartDate = CompareStartDate + " " + currentstrYear;
                    }

                    mDateObj = getMonthYearDateObj(CompareStartDate);
                    String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                    String serverToDate = DateTimeUtils.getLastDay(mDateObj);

                    serverCompareStartDate = serverFromDate;
                    serverCompareEndDate = serverToDate;
                        /*Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                        Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));*/
                } else if(getComparedateRangeIndex() == 4){
                    visibleCompareRightTick(4);
                    CompareStartDate = tv52WeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                    Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                    Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        } else if(dateRangeIndex == 3){
            startDate = getMonthDisplay(DateTimeUtils.getLastMonth());
            serverStartDate = getStartDateofLastMOnth();
            serverEndDate = getEndDateofLastMOnth();
            visibleRightTick(3);

            onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
            tvPreviousPeriod.setText(getPreviousMonthRangeFromLastThirtyDays(getDateObj(onedaybefore)));
            String lastMonth = tvLastMonth.getText().toString() + " 2018";


            //Date dateObj = getMonthDateObj(lastMonth);
            try{

                SimpleDateFormat mDateFormat = new SimpleDateFormat("MMMM yyyy");
                Date dateObj = mDateFormat.parse(lastMonth);
                onedaybefore = DateTimeUtils.getLastDay(dateObj);

                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(getDateObj(onedaybefore)));

                String thisYear = tvLastMonth.getText().toString() + " " + new SimpleDateFormat("yyyy").format(new Date());
                Logger.Error("<<<<  YEAR  >>>> " + thisYear);
                Date mDateObj = getMonthYearDateObj(thisYear);
                tvPreviousYear.setText(getMonthYearDisplay(DateTimeUtils.getLastYear(mDateObj)));

                String firstDay = DateTimeUtils.getFirstDay(mDateObj);
                String lastDay = DateTimeUtils.getLastDay(mDateObj);


                Logger.Error("<<<<   From Day  >>>> " + getFromDateofFifityTwoDaysAgo(getDateObj(firstDay)));

                Logger.Error("<<<<   From Day  >>>> " + getFromDateofFifityTwoDaysAgo(getDateObj(lastDay)));
                String display = getDisplayStringForWeekTab(getFromDateofFifityTwoDaysAgo(getDateObj(firstDay))) + "-" +
                        getDisplayStringForWeekTab(getFromDateofFifityTwoDaysAgo(getDateObj(lastDay)));
                tv52WeeksAgo.setText(display);
                //tv52WeeksAgo.setText(getFiftyTwoWeeksAgoDateRangeofLastSevenDays(new Date()));

                startDate = getMonthDisplay(DateTimeUtils.getLastMonth());
                serverStartDate = getStartDateofLastMOnth();
                serverEndDate = getEndDateofLastMOnth();

                if(getComparedateRangeIndex() == 1){
                    CompareStartDate = tvPreviousPeriod.getText().toString();
                    visibleCompareRightTick(1);

                    CompareStartDate = tvPreviousPeriod.getText().toString();
                    String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                    if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                        int year = Integer.parseInt(currentstrYear) - 1;
                        CompareStartDate = CompareStartDate + " " + String.valueOf(year);
                    } else{
                        CompareStartDate = CompareStartDate + " " + currentstrYear;
                    }

                    mDateObj = getMonthYearDateObj(CompareStartDate);
                    String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                    String serverToDate = DateTimeUtils.getLastDay(mDateObj);


                    serverCompareStartDate = serverFromDate;
                    serverCompareEndDate = serverToDate;


                        /*serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                        Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                        Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));*/
                } else if(getComparedateRangeIndex() == 2){
                    visibleCompareRightTick(2);
                    CompareStartDate = tvFourWeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                    Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                    Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
                } else if(getComparedateRangeIndex() == 3){

                    CompareStartDate = tvPreviousYear.getText().toString();

                    CompareStartDate = tvPreviousYear.getText().toString();
                    visibleCompareRightTick(3);
                    String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                    if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                        int year = Integer.parseInt(currentstrYear) - 1;
                        CompareStartDate = CompareStartDate + " " + String.valueOf(year);
                    } else{
                        CompareStartDate = CompareStartDate + " " + currentstrYear;
                    }

                    mDateObj = getMonthYearDateObj(CompareStartDate);
                    String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                    String serverToDate = DateTimeUtils.getLastDay(mDateObj);

                    serverCompareStartDate = serverFromDate;
                    serverCompareEndDate = serverToDate;
                        /*serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());

                        Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                        Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));*/
                } else if(getComparedateRangeIndex() == 4){
                    visibleCompareRightTick(4);
                    CompareStartDate = tv52WeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                    Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                    Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
                }
            } catch(Exception e){
                e.printStackTrace();
            }

        }

       /* if(getComparedateRangeIndex() == 1){
            CompareStartDate = tvPreviousPeriod.getText().toString();
            serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
            serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
            Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
            Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));
            visibleCompareRightTick(1);
        } else if(getComparedateRangeIndex() == 2){
            CompareStartDate = tvFourWeeksAgo.getText().toString();
            serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
            serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());

            Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
            Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
            visibleCompareRightTick(2);
        } else if(getComparedateRangeIndex() == 3){
            CompareStartDate = tvPreviousYear.getText().toString();
            serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
            serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());


            Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
            Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));
            visibleCompareRightTick(3);
        } else if(getComparedateRangeIndex() == 4){

            CompareStartDate = tv52WeeksAgo.getText().toString();
            serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
            serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

            Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
            Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
            visibleCompareRightTick(4);
        }*/


        if(isCompare()){
            llCompare.setVisibility(View.VISIBLE);
            mASwitchCompare.setChecked(true);

        } else{
            llCompare.setVisibility(View.GONE);
            mASwitchCompare.setChecked(false);

        }
        return rootView;
    }

    public String getStartDateofThisMOnth(){
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            Date dddd = calendar.getTime();
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            Logger.Error("<<<<  First Day of This Month " + sdf1.format(calendar.getTime()));
            return sdf1.format(calendar.getTime());
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getEndDateofThisMOnth(){

        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

            Date dddd = calendar.getTime();
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            Logger.Error("<<<<  Last Day of This Month " + sdf1.format(calendar.getTime()));
            return sdf1.format(calendar.getTime());
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getStartDateofLastMOnth(){

        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.MONTH, -1);

            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Logger.Error("<<<<  First Day of Last Month " + sdf1.format(calendar.getTime()));
            return sdf1.format(calendar.getTime());
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getEndDateofLastMOnth(){
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            //calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DATE));
            calendar.add(Calendar.MONTH, -1);
            //calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Logger.Error("<<<<  Last Day of Last Month " + sdf1.format(calendar.getTime()));
            return sdf1.format(calendar.getTime());
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getStartDateForServer(String combinedDate){
        try{
            String strStartDate = "";
            String split[] = combinedDate.split("-");
            strStartDate = getServerDateFormat(split[0]);
            return strStartDate;
        } catch(Exception e){
            e.printStackTrace();
        }

        return "";
    }

    public String getEndDateForServer(String combinedDate){
        try{
            String strEndDate = "";
            String split[] = combinedDate.split("-");
            strEndDate = getServerDateFormat(split[1]);
            return strEndDate;
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }


    private void initViews(View itemView){


        llLeaderboardCompare = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_compare_for_leader_board);
        tvLastThirtyDays = (TextView) itemView.findViewById(R.id.month_fragment_tv_last_thirty_days_range);
        tvThisMonth = (TextView) itemView.findViewById(R.id.month_fragment_tv_this_month_range);
        tvLastMonth = (TextView) itemView.findViewById(R.id.month_fragment_tv_last_month_range);
        tvPreviousPeriod = (TextView) itemView.findViewById(R.id.month_fragment_tv_previous_period_range);
        tvFourWeeksAgo = (TextView) itemView.findViewById(R.id.month_fragment_tv_four_weeks_ago_range);
        tvPreviousYear = (TextView) itemView.findViewById(R.id.month_fragment_tv_previous_year_range);
        tv52WeeksAgo = (TextView) itemView.findViewById(R.id.month_fragment_tv_fifty_two_weeks_ago_range);
        llCompare = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_compare);
        mASwitchCompare = (SwitchCompat) itemView.findViewById(R.id.switch_compare_date);

        ivLastThirtyDays = (ImageView) itemView.findViewById(R.id.month_fragment_iv_last_thirty_days);
        ivThisMonth = (ImageView) itemView.findViewById(R.id.month_fragment_iv_this_month);
        ivLastMonth = (ImageView) itemView.findViewById(R.id.month_fragment_iv_day_last_month);
        ivPreviousPeriod = (ImageView) itemView.findViewById(R.id.month_fragment_iv_previous_period);
        ivFourWeeksAgo = (ImageView) itemView.findViewById(R.id.month_fragment_iv_four_weeks_ago);
        ivPreviousYear = (ImageView) itemView.findViewById(R.id.month_fragment_iv_previous_year);
        iv52WeeksAgo = (ImageView) itemView.findViewById(R.id.month_fragment_iv_fifty_two_weeks_ago);


        llLastThirtyDays = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_last_thirty_days);
        llLastThirtyDays.setOnClickListener(this);
        llThisMonth = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_this_month);
        llThisMonth.setOnClickListener(this);
        llLastMonth = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_last_month);
        llLastMonth.setOnClickListener(this);
        llPreviousPeriod = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_previous_period);
        llPreviousPeriod.setOnClickListener(this);
        llFourWeeksAgo = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_four_weeks_ago);
        llFourWeeksAgo.setOnClickListener(this);
        llPreviousYear = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_previous_year);
        llPreviousYear.setOnClickListener(this);
        ll52WeeksAgo = (LinearLayout) itemView.findViewById(R.id.month_fragment_ll_fifty_two_weeks_ago);
        ll52WeeksAgo.setOnClickListener(this);


    }

    @Override
    public void onClick(View view){

        switch(view.getId()){
            case R.id.month_fragment_ll_last_thirty_days:
                visibleRightTick(1);
                tvLastThirtyDays.setText(getLastThirtyDaysRange(new Date()));
                tvThisMonth.setText(getMonthDisplay(DateTimeUtils.getCurrentMonth()));
                tvLastMonth.setText(getMonthDisplay(DateTimeUtils.getLastMonth()));
                String onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
                //tvPreviousPeriod.setText(getPreviousMonthRange(getDateObj(onedaybefore)));
                tvPreviousPeriod.setText(getPreviousMonthRangeFromLastThirtyDays(new Date()));
                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(new Date()));
                tvPreviousYear.setText(getLastYearThirtyDaysRange(new Date()));
                tv52WeeksAgo.setText(getFiftyTwoWeeksAgoDateRangeofLastSevenDays(new Date()));

                startDate = tvLastThirtyDays.getText().toString();
                serverStartDate = getStartDateForServer(startDate);
                serverEndDate = getEndDateForServer(startDate);

                if(getComparedateRangeIndex() == 1){
                    CompareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                    Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                    Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));
                } else if(getComparedateRangeIndex() == 2){
                    CompareStartDate = tvFourWeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                    Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                    Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
                } else if(getComparedateRangeIndex() == 3){
                    CompareStartDate = tvPreviousYear.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());

                    Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                    Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));
                } else if(getComparedateRangeIndex() == 4){
                    CompareStartDate = tv52WeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                    Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                    Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
                }
                break;
            case R.id.month_fragment_ll_this_month:
                visibleRightTick(2);
                try{
                    onedaybefore = DateTimeUtils.getLastDay(new Date());
                    tvPreviousPeriod.setText(getPreviousMonthRangeFromLastThirtyDays(new Date()));

                    tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(getDateObj(onedaybefore)));

                    String thisYear = tvThisMonth.getText().toString() + " " + new SimpleDateFormat("yyyy").format(new Date());
                    Logger.Error("<<<<  YEAR  >>>> " + thisYear);
                    Date mDateObj = getMonthYearDateObj(thisYear);
                    tvPreviousYear.setText(getMonthYearDisplay(DateTimeUtils.getLastYear(mDateObj)));


                    String firstDay = DateTimeUtils.getFirstDay(mDateObj);
                    String lastDay = DateTimeUtils.getLastDay(mDateObj);


                    Logger.Error("<<<<   From Day  >>>> " + getFromDateofFifityTwoDaysAgo(getDateObj(firstDay)));

                    Logger.Error("<<<<   From Day  >>>> " + getFromDateofFifityTwoDaysAgo(getDateObj(lastDay)));
                    String display = getDisplayStringForWeekTab(getFromDateofFifityTwoDaysAgo(getDateObj(firstDay))) + "-" +
                            getDisplayStringForWeekTab(getFromDateofFifityTwoDaysAgo(getDateObj(lastDay)));
                    tv52WeeksAgo.setText(display);

                    startDate = getMonthDisplay(DateTimeUtils.getCurrentMonth());
                    serverStartDate = getStartDateofThisMOnth();
                    serverEndDate = getEndDateofThisMOnth();

                    if(getComparedateRangeIndex() == 1){
                        CompareStartDate = tvPreviousPeriod.getText().toString();
                        String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                        if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                            int year = Integer.parseInt(currentstrYear) - 1;
                            CompareStartDate = CompareStartDate + " " + String.valueOf(year);
                        } else{
                            CompareStartDate = CompareStartDate + " " + currentstrYear;
                        }

                        mDateObj = getMonthYearDateObj(CompareStartDate);
                        String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                        String serverToDate = DateTimeUtils.getLastDay(mDateObj);


                        serverCompareStartDate = serverFromDate;
                        serverCompareEndDate = serverToDate;
                        /*Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                        Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));*/
                    } else if(getComparedateRangeIndex() == 2){
                        CompareStartDate = tvFourWeeksAgo.getText().toString();
                        serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                        Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                        Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
                    } else if(getComparedateRangeIndex() == 3){

                        CompareStartDate = tvPreviousYear.getText().toString();

                        String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                       /* if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                            int year = Integer.parseInt(currentstrYear) - 1;
                            CompareStartDate = CompareStartDate + " " +  String.valueOf(year);
                        } else{
                            CompareStartDate  = CompareStartDate + " " +  currentstrYear;
                        }*/

                        mDateObj = getMonthYearDateObj(CompareStartDate);
                        String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                        String serverToDate = DateTimeUtils.getLastDay(mDateObj);

                        serverCompareStartDate = serverFromDate;
                        serverCompareEndDate = serverToDate;
                        /*Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                        Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));*/
                    } else if(getComparedateRangeIndex() == 4){
                        CompareStartDate = tv52WeeksAgo.getText().toString();
                        serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                        Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                        Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.month_fragment_ll_last_month:
                visibleRightTick(3);
                onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
                tvPreviousPeriod.setText(getPreviousMonthRangeFromLastThirtyDays(getDateObj(onedaybefore)));
                String lastMonth = tvLastMonth.getText().toString() + " 2018";


                //Date dateObj = getMonthDateObj(lastMonth);
                try{

                    SimpleDateFormat mDateFormat = new SimpleDateFormat("MMMM yyyy");
                    Date dateObj = mDateFormat.parse(lastMonth);
                    onedaybefore = DateTimeUtils.getLastDay(dateObj);

                    tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(getDateObj(onedaybefore)));

                    String thisYear = tvLastMonth.getText().toString() + " " + new SimpleDateFormat("yyyy").format(new Date());
                    Logger.Error("<<<<  YEAR  >>>> " + thisYear);
                    Date mDateObj = getMonthYearDateObj(thisYear);
                    tvPreviousYear.setText(getMonthYearDisplay(DateTimeUtils.getLastYear(mDateObj)));

                    String firstDay = DateTimeUtils.getFirstDay(mDateObj);
                    String lastDay = DateTimeUtils.getLastDay(mDateObj);

                    Logger.Error("<<<<   From Day  >>>> " + getFromDateofFifityTwoDaysAgo(getDateObj(firstDay)));

                    Logger.Error("<<<<   From Day  >>>> " + getFromDateofFifityTwoDaysAgo(getDateObj(lastDay)));
                    String display = getDisplayStringForWeekTab(getFromDateofFifityTwoDaysAgo(getDateObj(firstDay))) + "-" +
                            getDisplayStringForWeekTab(getFromDateofFifityTwoDaysAgo(getDateObj(lastDay)));
                    tv52WeeksAgo.setText(display);
                    //tv52WeeksAgo.setText(getFiftyTwoWeeksAgoDateRangeofLastSevenDays(new Date()));

                    startDate = getMonthDisplay(DateTimeUtils.getLastMonth());
                    serverStartDate = getStartDateofLastMOnth();
                    serverEndDate = getEndDateofLastMOnth();

                    if(getComparedateRangeIndex() == 1){

                        CompareStartDate = tvPreviousPeriod.getText().toString();
                        String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                        if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                            int year = Integer.parseInt(currentstrYear) - 1;
                            CompareStartDate = CompareStartDate + " " + String.valueOf(year);
                        } else{
                            CompareStartDate = CompareStartDate + " " + currentstrYear;
                        }

                        mDateObj = getMonthYearDateObj(CompareStartDate);
                        String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                        String serverToDate = DateTimeUtils.getLastDay(mDateObj);


                        serverCompareStartDate = serverFromDate;
                        serverCompareEndDate = serverToDate;


                        /*serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                        Logger.Error("<<<< Previous Period Start Date>>>> " + getStartDateForServer(tvPreviousPeriod.getText().toString()));
                        Logger.Error("<<<< Previous Period End Date>>>> " + getEndDateForServer(tvPreviousPeriod.getText().toString()));*/
                    } else if(getComparedateRangeIndex() == 2){

                        CompareStartDate = tvFourWeeksAgo.getText().toString();
                        serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                        Logger.Error("<<<< Four Weeks Ago Start Date >>>> " + getStartDateForServer(tvFourWeeksAgo.getText().toString()));
                        Logger.Error("<<<< Four Weeks Ago End Date >>>> " + getEndDateForServer(tvFourWeeksAgo.getText().toString()));
                    } else if(getComparedateRangeIndex() == 3){

                        CompareStartDate = tvPreviousYear.getText().toString();

                        CompareStartDate = tvPreviousYear.getText().toString();

                        String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                        if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                            int year = Integer.parseInt(currentstrYear) - 1;
                            CompareStartDate = CompareStartDate + " " + String.valueOf(year);
                        } else{
                            CompareStartDate = CompareStartDate + " " + currentstrYear;
                        }

                        mDateObj = getMonthYearDateObj(CompareStartDate);
                        String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                        String serverToDate = DateTimeUtils.getLastDay(mDateObj);

                        serverCompareStartDate = serverFromDate;
                        serverCompareEndDate = serverToDate;
                        /*serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());

                        Logger.Error("<<<< Previous Year Start Date >>>> " + getStartDateForServer(tvPreviousYear.getText().toString()));
                        Logger.Error("<<<< Previous Year End Date >>>> " + getEndDateForServer(tvPreviousYear.getText().toString()));*/
                    } else if(getComparedateRangeIndex() == 4){

                        CompareStartDate = tv52WeeksAgo.getText().toString();
                        serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                        serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());

                        Logger.Error("<<<< 52 Weeks Ago Start Date >>>> " + getStartDateForServer(tv52WeeksAgo.getText().toString()));
                        Logger.Error("<<<< 52 Weeks Ago End Date >>>> " + getEndDateForServer(tv52WeeksAgo.getText().toString()));
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }

                break;
            case R.id.month_fragment_ll_previous_period:
                visibleCompareRightTick(1);
                CompareStartDate = tvPreviousPeriod.getText().toString();
                if(getDateRangeIndex() == 1){
                    serverCompareStartDate = getStartDateForServer(tvPreviousPeriod.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousPeriod.getText().toString());
                } else{
                    try{
                        String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                        if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                            int year = Integer.parseInt(currentstrYear) - 1;
                            CompareStartDate = CompareStartDate + " " + String.valueOf(year);
                        } else{
                            CompareStartDate = CompareStartDate + " " + currentstrYear;
                        }

                        Date mDateObj = getMonthYearDateObj(CompareStartDate);
                        String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                        String serverToDate = DateTimeUtils.getLastDay(mDateObj);


                        serverCompareStartDate = serverFromDate;
                        serverCompareEndDate = serverToDate;
                    } catch(Exception e){
                        e.printStackTrace();
                    }

                }
                break;
            case R.id.month_fragment_ll_four_weeks_ago:
                visibleCompareRightTick(2);
                CompareStartDate = tvFourWeeksAgo.getText().toString();
                serverCompareStartDate = getStartDateForServer(tvFourWeeksAgo.getText().toString());
                serverCompareEndDate = getEndDateForServer(tvFourWeeksAgo.getText().toString());
                break;
            case R.id.month_fragment_ll_previous_year:
                visibleCompareRightTick(3);
                CompareStartDate = tvPreviousYear.getText().toString();
                if(getDateRangeIndex() == 1){
                    serverCompareStartDate = getStartDateForServer(tvPreviousYear.getText().toString());
                    serverCompareEndDate = getEndDateForServer(tvPreviousYear.getText().toString());
                } else{
                    try{
                        /*String currentstrYear = new SimpleDateFormat("yyyy").format(new Date());
                        if(CompareStartDate.equalsIgnoreCase(MonthConstant.December)){
                            int year = Integer.parseInt(currentstrYear) - 1;
                            CompareStartDate = CompareStartDate + " " +  String.valueOf(year);
                        } else{
                            CompareStartDate  = CompareStartDate + " " +  currentstrYear;
                        }*/

                        Date mDateObj = getMonthYearDateObj(CompareStartDate);
                        String serverFromDate = DateTimeUtils.getFirstDay(mDateObj);
                        String serverToDate = DateTimeUtils.getLastDay(mDateObj);


                        serverCompareStartDate = serverFromDate;
                        serverCompareEndDate = serverToDate;
                    } catch(Exception e){
                        e.printStackTrace();
                    }

                }

                break;
            case R.id.month_fragment_ll_fifty_two_weeks_ago:
                visibleCompareRightTick(4);
                CompareStartDate = tv52WeeksAgo.getText().toString();
                serverCompareStartDate = getStartDateForServer(tv52WeeksAgo.getText().toString());
                serverCompareEndDate = getEndDateForServer(tv52WeeksAgo.getText().toString());
                break;

        }
    }

    public void visibleRightTick(int position){
        switch(position){
            case 1:
                ivLastThirtyDays.setVisibility(View.VISIBLE);
                ivThisMonth.setVisibility(View.GONE);
                ivLastMonth.setVisibility(View.GONE);
                setDateRangeIndex(1);
                break;
            case 2:
                ivLastThirtyDays.setVisibility(View.GONE);
                ivThisMonth.setVisibility(View.VISIBLE);
                ivLastMonth.setVisibility(View.GONE);
                setDateRangeIndex(2);
                break;
            case 3:
                ivLastThirtyDays.setVisibility(View.GONE);
                ivThisMonth.setVisibility(View.GONE);
                ivLastMonth.setVisibility(View.VISIBLE);
                setDateRangeIndex(3);
                break;
        }
    }

    public void visibleCompareRightTick(int position){
        switch(position){
            case 1:
                ivPreviousPeriod.setVisibility(View.VISIBLE);
                ivFourWeeksAgo.setVisibility(View.GONE);
                ivPreviousYear.setVisibility(View.GONE);
                iv52WeeksAgo.setVisibility(View.GONE);
                setComparedateRangeIndex(1);
                break;
            case 2:
                ivPreviousPeriod.setVisibility(View.GONE);
                ivFourWeeksAgo.setVisibility(View.VISIBLE);
                ivPreviousYear.setVisibility(View.GONE);
                iv52WeeksAgo.setVisibility(View.GONE);
                setComparedateRangeIndex(2);
                break;
            case 3:
                ivPreviousPeriod.setVisibility(View.GONE);
                ivFourWeeksAgo.setVisibility(View.GONE);
                ivPreviousYear.setVisibility(View.VISIBLE);
                iv52WeeksAgo.setVisibility(View.GONE);
                setComparedateRangeIndex(3);
                break;
            case 4:
                ivPreviousPeriod.setVisibility(View.GONE);
                ivFourWeeksAgo.setVisibility(View.GONE);
                ivPreviousYear.setVisibility(View.GONE);
                iv52WeeksAgo.setVisibility(View.VISIBLE);
                setComparedateRangeIndex(4);
                break;

        }
    }

    public String getLastThirtyDaysRange(Date inputDate){
        try{
            String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
            String sameDayofThirtyDay = DateTimeUtils.getLastThirtyDayFromToday(inputDate);
            return getDisplayStringForWeekTab(sameDayofThirtyDay) + " - " + getDisplayStringForWeekTab(yestrerday);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getLastYearThirtyDaysRange(Date inputDate){
        try{
            String yestrerday = DateTimeUtils.getYesterdayDateLastYearString(inputDate);
            String sameDayofLastYear = DateTimeUtils.getLastThirtyDayFromTodayLastYear(inputDate);
            return getDisplayStringForWeekTab(sameDayofLastYear) + " - " + getDisplayStringForWeekTab(yestrerday);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param DateStr
     * @return For Week Tab
     */
    public String getDisplayStringForWeekTab(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getMonthDisplay(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("MMMM");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getMonthYearDisplay(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("MMMM yyyy");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getServerDateFromMonthYear(String DateStr){
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat inputFormat = new SimpleDateFormat("MMMM yyyy");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getPreviousMonthRangeFromLastThirtyDays(Date inputDatebase){
        try{

            if(getDateRangeIndex() == 1){
                String yestrerday = DateTimeUtils.getYesterdayDateString(inputDatebase);
                String sameDayofThirtyDay = DateTimeUtils.getLastThirtyDayFromToday(inputDatebase);

                String to = DateTimeUtils.getDayofLastThirtyDay(getDateObj(yestrerday));
                Logger.Error("<<<<  TO  " + to);

                String from = "";

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(getDateObj(sameDayofThirtyDay));
                cal.add(Calendar.DAY_OF_YEAR, -30);
                from = dateFormat.format(cal.getTime());

                Logger.Error("<<<<  From  " + from);

                return getDisplayStringForWeekTab(from) + " - " + getDisplayStringForWeekTab(to);
            } else if(getDateRangeIndex() == 2){
                return getMonthDisplay(DateTimeUtils.getLastMonthFromDate(inputDatebase));
            } else if(getDateRangeIndex() == 3){
                String lastMonth = tvLastMonth.getText().toString();
                Date dateObj = getMonthDateObj(lastMonth);
                return getMonthDisplay(DateTimeUtils.getLastMonthFromDate(dateObj));
            }

        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getPreviousMonthRange(Date inputDate){
        try{
            String to = DateTimeUtils.getDayofLastThirtyDay(inputDate);
            Logger.Error("<<<<  TO  " + to);

            String from = "";

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTime(getDateObj(to));
            cal.add(Calendar.DAY_OF_YEAR, -30);
            from = dateFormat.format(cal.getTime());

            Logger.Error("<<<<  From  " + from);

            return getDisplayStringForWeekTab(from) + " - " + getDisplayStringForWeekTab(to);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public Date getDateObj(String dateStr){
        try{
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return mDateFormat.parse(dateStr);
        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public Date getMonthDateObj(String dateStr){
        try{
            SimpleDateFormat mDateFormat = new SimpleDateFormat("MMMM");
            return mDateFormat.parse(dateStr);
        } catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public Date getMonthYearDateObj(String dateStr){
        try{
            SimpleDateFormat mDateFormat = new SimpleDateFormat("MMMM yyyy");
            return mDateFormat.parse(dateStr);
        } catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getFourWeeksAgoDateRangeofLastSevenDays(Date inputDate){
        String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
        String sameDayofLastWeek = DateTimeUtils.getDayofLastThirtyDay(inputDate);
        return getDisplayStringForWeekTab(getLastFourWeekFirstDay(getDateObj(sameDayofLastWeek))) + " - " + getDisplayStringForWeekTab(getLastFourWeekFirstDayTo(getDateObj(yestrerday)));

    }

    public String getFiftyTwoWeeksAgoDateRangeofLastSevenDays(Date inputDate){
        String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
        String sameDayofLastWeek = DateTimeUtils.getDayofLastThirtyDay(inputDate);
        return getDisplayStringForWeekTab(getLastFifityWeekFirstDay(getDateObj(sameDayofLastWeek))) + " - " +
                getDisplayStringForWeekTab(getLastFifityWeekFirstDay(getDateObj(yestrerday)));
    }

    public String getLastFourWeekFirstDay(Date mDate){
        int daycount = -28;
        if(getDateRangeIndex() == 1){
            daycount = -28;
        }
        if(getDateRangeIndex() == 2){
            daycount = -28;
        }
        if(getDateRangeIndex() == 3){
            daycount = -24;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DAY_OF_YEAR, daycount);
        return sdf.format(cal.getTime());

    }

    public String getLastFourWeekFirstDayTo(Date mDate){

        int daycount = -28;
        if(getDateRangeIndex() == 1){
            daycount = -28;
        }
        if(getDateRangeIndex() == 2){
            daycount = -27;
        }
        if(getDateRangeIndex() == 3){
            daycount = -28;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DAY_OF_YEAR, daycount);
        return sdf.format(cal.getTime());

    }

    public String getLastFifityWeekFirstDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.WEEK_OF_MONTH, -52);
        return sdf.format(cal.getTime());

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        setTabIndex(2);

    }

    public int getDateRangeIndex(){
        return dateRangeIndex;
    }

    public void setDateRangeIndex(int dateRangeIndex){
        this.dateRangeIndex = dateRangeIndex;
    }


    public int getComparedateRangeIndex(){
        return ComparedateRangeIndex;
    }

    public void setComparedateRangeIndex(int comparedateRangeIndex){
        ComparedateRangeIndex = comparedateRangeIndex;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }

    public String getServerDateFormat(String inputDateStr){

        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        try{
            Date inputDate = inputFormat.parse(inputDateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public boolean isCompare(){
        return isCompare;
    }

    public void setCompare(boolean compare){
        isCompare = compare;
    }

    public String getStartDate(){
        return startDate;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getServerStartDate(){
        return serverStartDate;
    }

    public void setServerStartDate(String serverStartDate){
        this.serverStartDate = serverStartDate;
    }

    public String getServerEndDate(){
        return serverEndDate;
    }

    public void setServerEndDate(String serverEndDate){
        this.serverEndDate = serverEndDate;
    }

    public String getServerCompareStartDate(){
        return serverCompareStartDate;
    }

    public void setServerCompareStartDate(String serverCompareStartDate){
        this.serverCompareStartDate = serverCompareStartDate;
    }

    public String getServerCompareEndDate(){
        return serverCompareEndDate;
    }

    public void setServerCompareEndDate(String serverCompareEndDate){
        this.serverCompareEndDate = serverCompareEndDate;
    }

    public String getCompareStartDate(){
        return CompareStartDate;
    }

    public void setCompareStartDate(String compareStartDate){
        CompareStartDate = compareStartDate;
    }

    @Override
    public void onResume(){
        super.onResume();
        // mHomeActivity.tvTitle.setText("Month");
    }

    public String getFromDateofFifityTwoDaysAgo(Date datebase){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(datebase);
        cal.add(Calendar.WEEK_OF_YEAR, -52);
        return sdf.format(cal.getTime());
    }

}
