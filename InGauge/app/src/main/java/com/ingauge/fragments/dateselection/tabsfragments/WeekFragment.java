package com.ingauge.fragments.dateselection.tabsfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Created by mansurum on 15-Feb-18.
 */

public class WeekFragment extends Fragment implements View.OnClickListener{

    View rootView;


    TextView tvLastSevenDays;
    TextView tvThisWeek;
    TextView tvLastWeek;
    TextView tvPreviousPeriod;
    TextView tvFourWeeksAgo;

    ImageView ivLastSevenDays;
    ImageView ivThisWeek;
    ImageView ivLastWeek;
    ImageView ivPreviousPeriod;
    ImageView ivFourWeeksAgo;


    SwitchCompat mASwitchCompare;

    LinearLayout llLastSevenDays;
    LinearLayout llThisWeek;
    LinearLayout llLastWeek;
    LinearLayout llPreviousPeriod;
    LinearLayout llFourWeeksAgo;

    LinearLayout llCompare;
    HomeActivity mHomeActivity;


    public String startDate = "";
    public String compareStartDate = "";


    //Server Dates
    public String serverStartDate = "";
    public String serverEndDate = "";

    public String serverCompareStartDate = "";
    public String serverCompareEndDate = "";


    public boolean isCompare = true;


    /**
     * 0 = Day
     * 1 = Week
     * 2 = Month
     * 3 = Custom
     */
    public int tabIndex = 0;


    public int dateRangeIndex = -1;
    public int ComparedateRangeIndex = -1;


    String combinedDate = "";
    String combinedCompareDate = "";

    LinearLayout llLeaderboardForCompare;
    boolean isFromLeaderBoard = false;
    boolean isHideCompare = false;



    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        tabIndex = 1;
        setTabIndex(1);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.week_fragment, container, false);
        initViews(rootView);

        if(getArguments() != null){
            Bundle mBundle = getArguments();
            /*startDate = mBundle.getString(DateSelectionFragment.KEY_START_DATE);
            endDate =  mBundle.getString(DateSelectionFragment.KEY_END_DATE);
            compareStartDate = mBundle.getString(DateSelectionFragment.KEY_COMPARE_START_DATE);
            compareEndDate =  mBundle.getString(DateSelectionFragment.KEY_COMPARE_END_DATE);
            serverStartDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_START_DATE);
            serverEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_END_DATE);
            serverCompareStartDate= mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_START_DATE);
            serverCompareEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_END_DATE);*/
            isCompare = mBundle.getBoolean(DateSelectionFragment.KEY_IS_COMPARE);
            isFromLeaderBoard = mBundle.getBoolean(DateSelectionFragment.KEY_IS_FROM_LEADERBOARD);
            isHideCompare  = mBundle.getBoolean(DateSelectionFragment.KEY_IS_HIDE_COMPARE);

        }
        dateRangeIndex = InGaugeSession.read(getString(R.string.key_from_date_range_index), -1);
        ComparedateRangeIndex = InGaugeSession.read(getString(R.string.key_compare_date_range_index), -1);

        if(isFromLeaderBoard ){
            llLeaderboardForCompare.setVisibility(View.GONE);
        } else{
            llLeaderboardForCompare.setVisibility(View.VISIBLE);
        }

        if(isHideCompare){
            llLeaderboardForCompare.setVisibility(View.GONE);
        }else{
            llLeaderboardForCompare.setVisibility(View.VISIBLE);
        }
        mASwitchCompare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isChecked){
                    llCompare.setVisibility(View.VISIBLE);
                    setCompare(true);
                    compareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(compareStartDate);
                    serverCompareEndDate = getEndDateForServer(compareStartDate);

                    ComparedateRangeIndex = 1;
                } else{
                    llCompare.setVisibility(View.GONE);
                    setCompare(false);
                }
            }
        });


        if(getDateRangeIndex() == -1){
            Logger.Error("<<<<  Get Last Seven Days Range >>>> " + getLastSevenDaysRange(new Date()));
            tvLastSevenDays.setText(getLastSevenDaysRange(new Date()));
            tvThisWeek.setText(getThisWeekSevenDaysRange());
            tvLastWeek.setText(getLastSevenDaysRange(getDateObj(getFirstDateofthisWeek())));
            String onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
            tvPreviousPeriod.setText(getPreviousDateRange(getDateObj(onedaybefore)));
            tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(new Date()));
            visibleRightTick(1);
            if(isCompare){
                if(ComparedateRangeIndex == -1){
                    setCompareRangeIndex(1);
                } else{
                    setCompareRangeIndex(ComparedateRangeIndex);
                }
            }
        } else{
            setRangeIndex(getDateRangeIndex());
            if(isCompare){
                if(ComparedateRangeIndex == -1 || ComparedateRangeIndex == 3 || ComparedateRangeIndex == 4){
                    setCompareRangeIndex(1);
                } else{
                    setCompareRangeIndex(ComparedateRangeIndex);
                }
            }
        }


        if(isCompare()){
            llCompare.setVisibility(View.VISIBLE);
            mASwitchCompare.setChecked(true);
        } else{
            llCompare.setVisibility(View.GONE);
            mASwitchCompare.setChecked(false);
        }
        /*if(isCompare()){
            setCompareRangeIndex(getComparedateRangeIndex());
        }*/
        //// Week Tabs


/*
        Logger.Error("<<<<  Current Week Seven Days Range >>>> " + getThisWeekSevenDaysRange());

        Logger.Error("<<<<  Get Start Date of Week >>>> " + getFirstDateofthisWeek());

        Logger.Error("<<<<  Get Last Date of Week >>>> " + getLastDateofthisWeek());

        Logger.Error("<<<<  Get Last Week First Day>>>> " + getLastWeekFirstDay(new Date()));

        Logger.Error("<<<<  Get Last Week Last Day>>>> " + getLastWeekLastDay(new Date()));

        Logger.Error("<<<<  Get Previous Week Slab >>>> " + getLastSevenDaysRange(getDateObj(getFirstDateofthisWeek())));
*/

        ///Get Last Four Weeks Ago Date

        Logger.Error("<<<<  Four Weeks Ago Date Range  >>>  " + getFourWeeksAgoDateRangeofLastSevenDays(new Date()));


        return rootView;
    }


    private void setRangeIndex(int rangeIndex){
        tvLastSevenDays.setText(getLastSevenDaysRange(new Date()));
        tvThisWeek.setText(getThisWeekSevenDaysRange());
        tvLastWeek.setText(getLastSevenDaysRange(getDateObj(getFirstDateofthisWeek())));
        String onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
        tvPreviousPeriod.setText(getPreviousDateRange(getDateObj(onedaybefore)));
        tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(new Date()));
        switch(rangeIndex){
            case 1:
                visibleRightTick(1);
                if(getComparedateRangeIndex()==1){
                    visibleCompareRightTick(1);
                }else if(getComparedateRangeIndex()==2){
                    visibleCompareRightTick(2);
                }
                break;
            case 2:
                visibleRightTick(2);
                onedaybefore = DateTimeUtils.getYesterdayDateString(getDateObj(getLastDateofthisWeek()));

                String one = "";
                String two = "";

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(getDateObj(onedaybefore));
                cal.add(Calendar.DAY_OF_YEAR, -6);
                one = dateFormat.format(cal.getTime());
                Logger.Error("<<<<  ONE  " + one);

                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                cal = GregorianCalendar.getInstance();
                cal.setTime(getDateObj(one));
                cal.add(Calendar.DAY_OF_YEAR, -6);
                two = dateFormat.format(cal.getTime());
                Logger.Error("<<<<  TWO  " + two);
                String finalstr = getDisplayStringForWeekTab(two) + " - " + getDisplayStringForWeekTab(one);

                tvPreviousPeriod.setText(finalstr);

                Logger.Error("<<<<  Four Weeks Ago Date Range  >>>  " + getFourWeeksAgoDateRangeFromTwoDateInputs(getDateObj(getFirstDateofthisWeek()), getDateObj(getLastDateofthisWeek())));
                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeFromTwoDateInputs(getDateObj(getFirstDateofthisWeek()), getDateObj(getLastDateofthisWeek())));
                if(getComparedateRangeIndex()==1){
                    visibleCompareRightTick(1);
                }else if(getComparedateRangeIndex()==2){
                    visibleCompareRightTick(2);
                }
                break;
            case 3:
                visibleRightTick(3);
                one = getFirstDateofLastWeek();
                two = "";
                Logger.Error("<<<<  ONE  " + one);
                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                cal = GregorianCalendar.getInstance();
                cal.setTime(getDateObj(one));
                cal.add(Calendar.DAY_OF_YEAR, -6);
                two = dateFormat.format(cal.getTime());
                Logger.Error("<<<<  TWO  " + two);
                finalstr = getDisplayStringForWeekTab(two) + " - " + getDisplayStringForWeekTab(one);
                tvPreviousPeriod.setText(finalstr);
                String yestrerday = DateTimeUtils.getYesterdayDateString(getDateObj(getFirstDateofthisWeek()));
                String sameDayofLastWeek = DateTimeUtils.getSameDayofLastWeek(getDateObj(getFirstDateofthisWeek()));
                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeFromTwoDateInputs(getDateObj(sameDayofLastWeek), getDateObj(yestrerday)));
                if(getComparedateRangeIndex()==1){
                    visibleCompareRightTick(1);
                }else if(getComparedateRangeIndex()==2){
                    visibleCompareRightTick(2);
                }
                break;
        }
    }

    private void setCompareRangeIndex(int rangeIndex){
        switch(rangeIndex){
            case 1:
                visibleCompareRightTick(1);
                break;
            case 2:
                visibleCompareRightTick(2);
                break;

        }
    }

    private void initViews(View itemView){


        llLeaderboardForCompare = (LinearLayout) itemView.findViewById(R.id.week_fragment_ll_compare_for_leaderboard);
        tvLastSevenDays = (TextView) itemView.findViewById(R.id.week_fragment_tv_last_seven_days_range);
        tvThisWeek = (TextView) itemView.findViewById(R.id.week_fragment_tv_this_week_range);
        tvLastWeek = (TextView) itemView.findViewById(R.id.week_fragment_tv_day_last_week_range);
        tvPreviousPeriod = (TextView) itemView.findViewById(R.id.week_fragment_tv_previous_period_range);
        tvFourWeeksAgo = (TextView) itemView.findViewById(R.id.week_fragment_tv_four_weeks_ago_range);

        mASwitchCompare = (SwitchCompat) itemView.findViewById(R.id.switch_compare_date);
        llCompare = (LinearLayout) itemView.findViewById(R.id.week_fragment_ll_compare);
        ivLastSevenDays = (ImageView) itemView.findViewById(R.id.week_fragment_iv_last_seven_days);
        ivThisWeek = (ImageView) itemView.findViewById(R.id.week_fragment_iv_this_week);
        ivLastWeek = (ImageView) itemView.findViewById(R.id.week_fragment_iv_last_week);
        ivPreviousPeriod = (ImageView) itemView.findViewById(R.id.week_fragment_iv_previous_period);
        ivFourWeeksAgo = (ImageView) itemView.findViewById(R.id.week_fragment_iv_same_day_four_weeks_ago_range);


        llLastSevenDays = (LinearLayout) itemView.findViewById(R.id.week_fragment_ll_last_seven_days);
        llLastSevenDays.setOnClickListener(this);
        llThisWeek = (LinearLayout) itemView.findViewById(R.id.week_fragment_ll_this_week);
        llThisWeek.setOnClickListener(this);
        llLastWeek = (LinearLayout) itemView.findViewById(R.id.week_fragment_ll_day_last_week);
        llLastWeek.setOnClickListener(this);
        llPreviousPeriod = (LinearLayout) itemView.findViewById(R.id.week_fragment_ll_previous_period);
        llPreviousPeriod.setOnClickListener(this);
        llFourWeeksAgo = (LinearLayout) itemView.findViewById(R.id.week_fragment_ll_four_weeks_ago);
        llFourWeeksAgo.setOnClickListener(this);


    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.week_fragment_ll_last_seven_days:
                tvLastSevenDays.setText(getLastSevenDaysRange(new Date()));
                tvThisWeek.setText(getThisWeekSevenDaysRange());
                tvLastWeek.setText(getLastSevenDaysRange(getDateObj(getFirstDateofthisWeek())));
                String onedaybefore = DateTimeUtils.getYesterdayDateString(new Date());
                tvPreviousPeriod.setText(getPreviousDateRange(getDateObj(onedaybefore)));
                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeofLastSevenDays(new Date()));
                visibleRightTick(1);

                if(ComparedateRangeIndex == 1){
                    compareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(compareStartDate);
                    serverCompareEndDate = getEndDateForServer(compareStartDate);
                    Logger.Error("<<<<< Compare Server Start Date  " + serverCompareStartDate);
                    Logger.Error("<<<<< Compare Server End Date  " + serverCompareEndDate);


                } else if(ComparedateRangeIndex == 2){
                    compareStartDate = tvFourWeeksAgo.getText().toString();


                    serverCompareStartDate = getStartDateForServer(compareStartDate);
                    serverCompareEndDate = getEndDateForServer(compareStartDate);

                    Logger.Error("<<<<< Compare Server Start Date  " + serverCompareStartDate);
                    Logger.Error("<<<<< Compare Server End Date  " + serverCompareEndDate);

                }
                break;
            case R.id.week_fragment_ll_this_week:
                visibleRightTick(2);
                onedaybefore = DateTimeUtils.getYesterdayDateString(getDateObj(getLastDateofthisWeek()));

                String one = "";
                String two = "";

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(getDateObj(onedaybefore));
                cal.add(Calendar.DAY_OF_YEAR, -6);
                one = dateFormat.format(cal.getTime());
                Logger.Error("<<<<  ONE  " + one);

                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                cal = GregorianCalendar.getInstance();
                cal.setTime(getDateObj(one));
                cal.add(Calendar.DAY_OF_YEAR, -6);
                two = dateFormat.format(cal.getTime());

                Logger.Error("<<<<  TWO  " + two);

                String finalstr = getDisplayStringForWeekTab(two) + " - " + getDisplayStringForWeekTab(one);

                tvPreviousPeriod.setText(finalstr);

                Logger.Error("<<<<  Four Weeks Ago Date Range  >>>  " + getFourWeeksAgoDateRangeFromTwoDateInputs(getDateObj(getFirstDateofthisWeek()), getDateObj(getLastDateofthisWeek())));
                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeFromTwoDateInputs(getDateObj(getFirstDateofthisWeek()), getDateObj(getLastDateofthisWeek())));
                if(ComparedateRangeIndex == 1){
                    compareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(compareStartDate);
                    serverCompareEndDate = getEndDateForServer(compareStartDate);

                } else if(ComparedateRangeIndex == 2){
                    compareStartDate = tvFourWeeksAgo.getText().toString();
                    serverCompareStartDate = getStartDateForServer(compareStartDate);
                    serverCompareEndDate = getEndDateForServer(compareStartDate);

                }

                Logger.Error("<<<<< Compare Server Start Date  " + serverCompareStartDate);
                Logger.Error("<<<<< Compare Server End Date  " + serverCompareEndDate);
                break;
            case R.id.week_fragment_ll_day_last_week:
                visibleRightTick(3);

                one = getFirstDateofLastWeek();
                two = "";
                Logger.Error("<<<<  ONE  " + one);

                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                cal = GregorianCalendar.getInstance();
                cal.setTime(getDateObj(one));
                cal.add(Calendar.DAY_OF_YEAR, -6);
                two = dateFormat.format(cal.getTime());

                Logger.Error("<<<<  TWO  " + two);

                finalstr = getDisplayStringForWeekTab(two) + " - " + getDisplayStringForWeekTab(one);

                tvPreviousPeriod.setText(finalstr);

                String yestrerday = DateTimeUtils.getYesterdayDateString(getDateObj(getFirstDateofthisWeek()));
                String sameDayofLastWeek = DateTimeUtils.getSameDayofLastWeek(getDateObj(getFirstDateofthisWeek()));
                tvFourWeeksAgo.setText(getFourWeeksAgoDateRangeFromTwoDateInputs(getDateObj(sameDayofLastWeek), getDateObj(yestrerday)));
                if(ComparedateRangeIndex == 1){
                    compareStartDate = tvPreviousPeriod.getText().toString();
                    serverCompareStartDate = getStartDateForServer(compareStartDate);
                    serverCompareEndDate = getEndDateForServer(compareStartDate);

                } else if(ComparedateRangeIndex == 2){
                    compareStartDate = tvFourWeeksAgo.getText().toString();


                    serverCompareStartDate = getStartDateForServer(compareStartDate);
                    serverCompareEndDate = getEndDateForServer(compareStartDate);

                }

                Logger.Error("<<<<< Compare Server Start Date  " + serverCompareStartDate);
                Logger.Error("<<<<< Compare Server End Date  " + serverCompareEndDate);
                break;
            case R.id.week_fragment_ll_previous_period:
                visibleCompareRightTick(1);
                break;
            case R.id.week_fragment_ll_four_weeks_ago:
                visibleCompareRightTick(2);
                break;
        }
    }

    public String getStartDateForServer(String combinedDate){
        try{
            String strStartDate = "";
            String split[] = combinedDate.split("-");
            strStartDate = getServerDateFormat(split[0]);
            return strStartDate;
        } catch(Exception e){
            e.printStackTrace();
        }

        return "";
    }

    public String getEndDateForServer(String combinedDate){
        try{
            String strEndDate = "";
            String split[] = combinedDate.split("-");
            strEndDate = getServerDateFormat(split[1]);
            return strEndDate;
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public void visibleRightTick(int position){
        switch(position){
            case 1:
                ivLastSevenDays.setVisibility(View.VISIBLE);
                ivThisWeek.setVisibility(View.GONE);
                ivLastWeek.setVisibility(View.GONE);
                startDate = tvLastSevenDays.getText().toString();

                Logger.Error("<<<<< Combined Date " + startDate);

                serverStartDate = getStartDateForServer(startDate);
                serverEndDate = getEndDateForServer(startDate);

                Logger.Error("<<<<< Server Start Date  " + serverStartDate);
                Logger.Error("<<<<< Server End Date  " + serverEndDate);
                dateRangeIndex = 1;
                break;
            case 2:
                ivLastSevenDays.setVisibility(View.GONE);
                ivThisWeek.setVisibility(View.VISIBLE);
                ivLastWeek.setVisibility(View.GONE);
                startDate = tvThisWeek.getText().toString();
                Logger.Error("<<<<< Combined Date " + startDate);
                serverStartDate = getStartDateForServer(startDate);
                serverEndDate = getEndDateForServer(startDate);

                Logger.Error("<<<<< Server Start Date  " + serverStartDate);
                Logger.Error("<<<<< Server End Date  " + serverEndDate);

                dateRangeIndex = 2;
                break;
            case 3:
                ivLastSevenDays.setVisibility(View.GONE);
                ivThisWeek.setVisibility(View.GONE);
                ivLastWeek.setVisibility(View.VISIBLE);
                startDate = tvLastWeek.getText().toString();
                Logger.Error("<<<<< Combined Date " + startDate);
                serverStartDate = getStartDateForServer(startDate);
                serverEndDate = getEndDateForServer(startDate);

                Logger.Error("<<<<< Server Start Date  " + serverStartDate);
                Logger.Error("<<<<< Server End Date  " + serverEndDate);

                dateRangeIndex = 3;
                break;
        }
    }

    public void visibleCompareRightTick(int position){
        switch(position){
            case 1:
                ivPreviousPeriod.setVisibility(View.VISIBLE);
                ivFourWeeksAgo.setVisibility(View.GONE);

                compareStartDate = tvPreviousPeriod.getText().toString();
                serverCompareStartDate = getStartDateForServer(compareStartDate);
                serverCompareEndDate = getEndDateForServer(compareStartDate);

                ComparedateRangeIndex = 1;
                Logger.Error("<<<<< Compare Server Start Date  " + serverCompareStartDate);
                Logger.Error("<<<<< Compare Server End Date  " + serverCompareEndDate);
                break;
            case 2:
                ivPreviousPeriod.setVisibility(View.GONE);
                ivFourWeeksAgo.setVisibility(View.VISIBLE);

                compareStartDate = tvFourWeeksAgo.getText().toString();
                serverCompareStartDate = getStartDateForServer(compareStartDate);
                serverCompareEndDate = getEndDateForServer(compareStartDate);

                ComparedateRangeIndex = 2;
                Logger.Error("<<<<< Compare Server Start Date  " + serverCompareStartDate);
                Logger.Error("<<<<< Compare Server End Date  " + serverCompareEndDate);
                break;
        }
    }


    /**
     * @param DateStr
     * @return For Week Tab
     */
    public String getDisplayStringForWeekTab(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM, yyyy");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getLastSevenDaysRange(Date inputDate){
        try{
            String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
            String sameDayofLastWeek = DateTimeUtils.getSameDayofLastWeek(inputDate);
            return getDisplayStringForWeekTab(sameDayofLastWeek) + " - " + getDisplayStringForWeekTab(yestrerday);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getFourWeeksAgoDateRangeofLastSevenDays(Date inputDate){
        String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
        String sameDayofLastWeek = DateTimeUtils.getSameDayofLastWeek(inputDate);
        return getDisplayStringForWeekTab(getLastFourWeekFirstDay(getDateObj(sameDayofLastWeek))) + " - " + getDisplayStringForWeekTab(getLastFourWeekFirstDay(getDateObj(yestrerday)));

    }

    public String getFourWeeksAgoDateRangeFromTwoDateInputs(Date fromDate, Date toDate){


        return getDisplayStringForWeekTab(getLastFourWeekFirstDay(fromDate)) + " - " + getDisplayStringForWeekTab(getLastFourWeekFirstDay(toDate));

    }

    public String getPreviousDateRange(Date inputDate){
        try{
            String one = DateTimeUtils.getSameDayofLastWeek(inputDate);
            Logger.Error("<<<<  ONE  " + one);

            String two = "";

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTime(getDateObj(one));
            cal.add(Calendar.DAY_OF_YEAR, -6);
            two = dateFormat.format(cal.getTime());

            Logger.Error("<<<<  TWO  " + two);

            return getDisplayStringForWeekTab(two) + " - " + getDisplayStringForWeekTab(one);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getThisWeekSevenDaysRange(){
        try{
            String firstdayOfCurrentWeek = getFirstDateofthisWeek();
            String LastDayOfCurrenttWeek = getLastDateofthisWeek();
            return getDisplayStringForWeekTab(firstdayOfCurrentWeek) + " - " + getDisplayStringForWeekTab(LastDayOfCurrenttWeek);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }


    public static int getLastDayOfWeek(Calendar cal){
        int last = cal.getFirstDayOfWeek() - Calendar.SUNDAY;

        if(last == 0){
            last = Calendar.SATURDAY;
        }

        return last;
    }


    public String getFirstDateofthisWeek(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.set(Calendar.DAY_OF_WEEK, 1);
        return sdf.format(cal.getTime());

    }

    public String getFirstDateofLastWeek(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.WEEK_OF_YEAR, -2);
        cal.set(Calendar.DAY_OF_WEEK, getLastDayOfWeek(cal));
        return sdf.format(cal.getTime());

    }

    public String getLastDateofthisWeek(){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.set(Calendar.DAY_OF_WEEK, getLastDayOfWeek(cal));
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());
    }

    public String getLastWeekFirstDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DATE, -7);
        cal.set(Calendar.DAY_OF_WEEK, 1);
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());

    }

    public String getLastWeekLastDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DATE, -7);
        cal.set(Calendar.DAY_OF_WEEK, 7);
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());

    }

    public String getLastFourWeekFirstDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DAY_OF_YEAR, -28);
        return sdf.format(cal.getTime());

    }

    public String getLastFourWeekLastDay(Date mDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DAY_OF_YEAR, -35);
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());
    }

    public Date getDateObjFromDisplay(String dateStr){
        try{
            SimpleDateFormat mDateFormat = new SimpleDateFormat("EEEE, MMM dd yyyy");
            return mDateFormat.parse(dateStr);
        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public Date getDateObj(String dateStr){
        try{
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return mDateFormat.parse(dateStr);
        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        tabIndex = 1;

    }

    public String getServerDateFormat(String inputDateStr){

        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat inputFormat = new SimpleDateFormat("dd MMM, yyyy");
        try{
            Date inputDate = inputFormat.parse(inputDateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public int getDateRangeIndex(){
        return dateRangeIndex;
    }

    public void setDateRangeIndex(int dateRangeIndex){
        this.dateRangeIndex = dateRangeIndex;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }

    public String getCombinedDate(){
        return combinedDate;
    }

    public void setCombinedDate(String combinedDate){
        this.combinedDate = combinedDate;
    }

    public String getCombinedCompareDate(){
        return combinedCompareDate;
    }

    public void setCombinedCompareDate(String combinedCompareDate){
        this.combinedCompareDate = combinedCompareDate;
    }

    public boolean isCompare(){
        return isCompare;
    }

    public void setCompare(boolean compare){
        isCompare = compare;
    }

    public String getStartDate(){
        return startDate;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getCompareStartDate(){
        return compareStartDate;
    }

    public void setCompareStartDate(String compareStartDate){
        this.compareStartDate = compareStartDate;
    }

    public String getServerStartDate(){
        return serverStartDate;
    }

    public void setServerStartDate(String serverStartDate){
        this.serverStartDate = serverStartDate;
    }

    public String getServerEndDate(){
        return serverEndDate;
    }

    public void setServerEndDate(String serverEndDate){
        this.serverEndDate = serverEndDate;
    }

    public String getServerCompareStartDate(){
        return serverCompareStartDate;
    }

    public void setServerCompareStartDate(String serverCompareStartDate){
        this.serverCompareStartDate = serverCompareStartDate;
    }

    public String getServerCompareEndDate(){
        return serverCompareEndDate;
    }

    public void setServerCompareEndDate(String serverCompareEndDate){
        this.serverCompareEndDate = serverCompareEndDate;
    }

    public int getComparedateRangeIndex(){
        return ComparedateRangeIndex;
    }

    public void setComparedateRangeIndex(int comparedateRangeIndex){
        ComparedateRangeIndex = comparedateRangeIndex;
    }

    @Override
    public void onResume(){
        super.onResume();
        //mHomeActivity.tvTitle.setText("Week");
    }
}
