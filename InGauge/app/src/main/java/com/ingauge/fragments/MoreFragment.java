package com.ingauge.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.SignInActivity;
import com.ingauge.adapters.MoreNewAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.agentprofile.AgentProfileFragment;
import com.ingauge.fragments.pulsemail.FragmentInbox;
import com.ingauge.pojo.MenuModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 18-Jul-17.
 */

public class MoreFragment extends BaseFragment{

    List<String> listDataHeader;
    LinkedHashMap<String, List<String>> listDataChild;
    View mView;
    private ExpandableListView mExpandableListView;
    MoreNewAdapter moreNewAdapter;
    Context mContext;
    HomeActivity mHomeActivity;
    APIInterface apiInterface;
    private MenuModel menuModel;
    private RelativeLayout rlProgress;
    ArrayList<String> childItem = new ArrayList<>();
    private boolean isGuideEnableFromLogin = false;
    private boolean isGuideEnableFromChangetenant = false;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        isGuideEnableFromLogin = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_guide_enabled), false);
        isGuideEnableFromChangetenant = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_guide_enabled_from_preference), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        if(mView == null){
            mView = inflater.inflate(R.layout.fragment_more_new, container, false);
            mExpandableListView = (ExpandableListView) mView.findViewById(R.id.lvExp);
            rlProgress = (RelativeLayout) mView.findViewById(R.id.custom_progress_rl_main);
            rlProgress.setVisibility(View.VISIBLE);
            mExpandableListView.setVisibility(View.GONE);
            apiInterface = APIClient.getClient().create(APIInterface.class);
            mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener(){
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id){
                    Object pos = moreNewAdapter.getGroup(groupPosition);
                    String str = (String) pos;
                    if(str != null && str.equalsIgnoreCase("logout")){
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        AlertDialog dialog;
                        builder.setTitle("");
                        builder.setMessage(mContext.getResources().getString(R.string.alert_logout));
                        builder.setCancelable(false);
                        builder.setPositiveButton(
                                getActivity().getResources().getString(R.string.alert_ok),
                                new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){


                                        Bundle bundle = new Bundle();
                                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                                        InGaugeApp.getFirebaseAnalytics().logEvent("sign_out", bundle);
                                        Intent login = new Intent(getActivity(), SignInActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(login);
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_auth_token), "");
                                        InGaugeSession.clearAllPreferences(getContext());
                                        // getActivity().overridePendingTransition(R.anim.no_change, R.anim.slide_right_entry);
                                        getActivity().finish();
                                    }
                                });
                        builder.setNegativeButton(
                                getActivity().getResources().getString(R.string.alert_cancel), new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                    }
                                }
                        );
                        //  builder.show();
                        dialog = builder.create();
                        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        dialog.show();
                    } else if(str != null && str.equalsIgnoreCase("My Profile")){
                        // InGaugeSession.write(getString(R.string.key_fragment_type),getString(R.string.fragment_user_profile));
                        /*UserProfileFragment fragment = new UserProfileFragment();
                        mHomeActivity.replace(fragment, mContext.getResources().getString(R.string.tag_user_profile));*/

                        AgentProfileFragment mAgentProfileFragment = new AgentProfileFragment();
                        mHomeActivity.replace(mAgentProfileFragment, getResources().getString(R.string.tag_user_profile));



                    } else if(menuModel.getData().get(groupPosition).getUrl().equalsIgnoreCase("/goal/goalProgress")){
                        mHomeActivity.replace(new GoalSettings(), mContext.getResources().getString(R.string.tag_goal_settings));
                    }
                    return false;
                }
            });
            mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener(){
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id){
                    Object pos = moreNewAdapter.getChild(groupPosition, childPosition);
                    String str = (String) pos;
                    listDataChild.get(pos);
                    listDataHeader.size();

                   /* if (childItem.get(childPosition).equalsIgnoreCase("inbox")) {
                        Bundle mBundle = new Bundle();
                        mBundle.putInt(FragmentInbox.KEY_SENDER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putInt(FragmentInbox.KEY_RECEIVER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putString(FragmentInbox.KEY_MAIL_TYPE, "inbox");
                        mBundle.putString(FragmentInbox.KEY_MAIL_STATUS, "all");
                        FragmentInbox mFragmentInbox = new FragmentInbox();
                        mFragmentInbox.setArguments(mBundle);
                        mHomeActivity.replace(mFragmentInbox, mContext.getResources().getString(R.string.tag_pulse_mail));
                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        InGaugeApp.getFirebaseAnalytics().logEvent("check_mail_inbox", bundle);
                    } else if (childItem.get(childPosition).equalsIgnoreCase("sent")) {
                        Logger.Error("< Clicked on Sent>");
                        Bundle mBundle = new Bundle();
                        mBundle.putInt(FragmentInbox.KEY_SENDER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putInt(FragmentInbox.KEY_RECEIVER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putString(FragmentInbox.KEY_MAIL_TYPE, "sent");
                        mBundle.putString(FragmentInbox.KEY_MAIL_STATUS, "all");
                        FragmentInbox mFragmentInbox = new FragmentInbox();
                        mFragmentInbox.setArguments(mBundle);
                        mHomeActivity.replace(mFragmentInbox, mContext.getResources().getString(R.string.tag_pulse_mail));
                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        InGaugeApp.getFirebaseAnalytics().logEvent("check_mail_sent", bundle);
                    }*/

                    if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/inbox")){
                        Bundle mBundle = new Bundle();
                        mBundle.putInt(FragmentInbox.KEY_SENDER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putInt(FragmentInbox.KEY_RECEIVER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putString(FragmentInbox.KEY_MAIL_TYPE, "inbox");
                        mBundle.putString(FragmentInbox.KEY_MAIL_STATUS, "all");
                        FragmentInbox mFragmentInbox = new FragmentInbox();
                        mFragmentInbox.setArguments(mBundle);
                        mHomeActivity.replace(mFragmentInbox, mContext.getResources().getString(R.string.tag_pulse_mail));
                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        InGaugeApp.getFirebaseAnalytics().logEvent("check_mail_inbox", bundle);
                    } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/sent")){
                        Logger.Error("< Clicked on Sent>");
                        Bundle mBundle = new Bundle();
                        mBundle.putInt(FragmentInbox.KEY_SENDER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putInt(FragmentInbox.KEY_RECEIVER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putString(FragmentInbox.KEY_MAIL_TYPE, "sent");
                        mBundle.putString(FragmentInbox.KEY_MAIL_STATUS, "all");
                        FragmentInbox mFragmentInbox = new FragmentInbox();
                        mFragmentInbox.setArguments(mBundle);
                        mHomeActivity.replace(mFragmentInbox, mContext.getResources().getString(R.string.tag_pulse_mail));
                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        InGaugeApp.getFirebaseAnalytics().logEvent("check_mail_sent", bundle);
                    } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/trash")){
                        Bundle mBundle = new Bundle();
                        mBundle.putInt(FragmentInbox.KEY_SENDER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putInt(FragmentInbox.KEY_RECEIVER_ID, InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0));
                        mBundle.putString(FragmentInbox.KEY_MAIL_TYPE, "trash");
                        mBundle.putString(FragmentInbox.KEY_MAIL_STATUS, "all");
                        FragmentInbox mFragmentInbox = new FragmentInbox();
                        mFragmentInbox.setArguments(mBundle);
                        mHomeActivity.replace(mFragmentInbox, mContext.getResources().getString(R.string.tag_pulse_mail));
                        Bundle bundle = new Bundle();
                        bundle.putString("email", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_user_email), ""));
                        bundle.putString("role", InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_name), ""));
                        InGaugeApp.getFirebaseAnalytics().logEvent("check_mail_trash", bundle);
                        Logger.Error("< Clicked on Trash>");
                    } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/survey/observation/add")){
                        UiUtils.clearObsCCLocal(mContext, true);
                        refereshSurveyVariables();
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), mContext.getResources().getString(R.string.survey_entity_type_id_observation));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), mContext.getResources().getString(R.string.survey_entity_type_name_observation));
                        ObservationAgentFragment fragment = new ObservationAgentFragment();
                        mHomeActivity.replace(fragment, mContext.getResources().getString(R.string.tag_tab_observation));
                    } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/survey/observation/dashboard")){
                        UiUtils.clearObsCCLocal(mContext, true);
                        refereshSurveyVariables();
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), mContext.getResources().getString(R.string.survey_entity_type_id_observation));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), mContext.getResources().getString(R.string.survey_entity_type_name_observation));
                        ObservationDashboardFragment fragment = new ObservationDashboardFragment();
                        mHomeActivity.replace(fragment, mContext.getResources().getString(R.string.tag_tab_observation_dashboard));
                    } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/survey/counterCoaching/add")){
                        UiUtils.clearObsCCLocal(mContext, true);
                        refereshSurveyVariables();
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), mContext.getResources().getString(R.string.survey_entity_type_id_counter_coaching));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), mContext.getResources().getString(R.string.survey_entity_type_name_counter_coaching));
                        ObservationAgentFragment fragment = new ObservationAgentFragment();
                        mHomeActivity.replace(fragment, mContext.getResources().getString(R.string.tag_tab_observation));
                    } else if(menuModel.getData().get(groupPosition).getSubMenus().get(childPosition).getUrl().equalsIgnoreCase("/survey/counterCoaching/dashboard")){
                        UiUtils.clearObsCCLocal(mContext, true);
                        refereshSurveyVariables();
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_id), mContext.getResources().getString(R.string.survey_entity_type_id_counter_coaching));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_cc_survey_entity_type_name), mContext.getResources().getString(R.string.survey_entity_type_name_counter_coaching));
                        ObservationDashboardFragment fragment = new ObservationDashboardFragment();
                        mHomeActivity.replace(fragment, mContext.getResources().getString(R.string.tag_tab_observation_dashboard));
                    }
                    return false;
                }
            });
            getRolesAndPermission(String.valueOf(InGaugeSession.read(getString(R.string.key_selected_tenant_id), 0)),
                    String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -2)));

        } else{
            if(mView != null && mView.getParent() != null){
                ((ViewGroup) mView.getParent()).removeView(mView);
            }
        }

        return mView;
    }


    public void refereshSurveyVariables(){
        mHomeActivity.agentScore = "0";
        mHomeActivity.areaOfImprovment = "";
        mHomeActivity.isFromDashboard = false;
        mHomeActivity.isPending = false;
        mHomeActivity.isOnlyView = false;
        mHomeActivity.donotMailToAgent = false;
        mHomeActivity.donotDisplayToAgent = false;
        mHomeActivity.eventID = "";
    }

    private void getMenu(String ContentType, String Authorization, boolean isMobile, final String IndustryId, String TenantId, final String userId){
        Call mCall = apiInterface.getMenu(TenantId, userId);
        rlProgress.setVisibility(View.VISIBLE);
        mExpandableListView.setVisibility(View.GONE);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                rlProgress.setVisibility(View.GONE);
                mExpandableListView.setVisibility(View.VISIBLE);
                menuModel = (MenuModel) response.body();
                if(menuModel != null){
                    prepareListData(menuModel);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                //mHomeActivity.endProgress();
                rlProgress.setVisibility(View.GONE);
                mExpandableListView.setVisibility(View.VISIBLE);
            }
        });
    }

    /*
     * Preparing the list data
     */
    private void prepareListData(MenuModel mymenuModel){
        /*listDataHeader = new ArrayList<String>();
        listDataChild = new LinkedHashMap<String, List<String>>();

        // Adding child data
        List<String> arraysValue = Arrays.asList(mContext.getResources().getStringArray(R.array.list_more));
        for (int i = 0; i < menuModel.getData().size(); i++) {
            listDataHeader.add(arraysValue.get(i));
        }*/
        // Adding child data
       /* List<String> mailItemList = new ArrayList<String>();
        mailItemList.add("Inbox");
        mailItemList.add("Sent");
        mailItemList.add("Trash");

        List<String> observationItemList = new ArrayList<String>();
        observationItemList.add("Observe Agent");
        observationItemList.add("Observation Dashboard");

        List<String> counterItemList = new ArrayList<String>();
        counterItemList.add("Counter Caoching");
        counterItemList.add("Counter Caoching Dashboard");


        for (int i = 0; i < listDataHeader.size(); i++) {
            if (i == 3) {
                listDataChild.put(listDataHeader.get(i), mailItemList); // Header, Child data
            }
            else if(i==4){
                listDataChild.put(listDataHeader.get(i), observationItemList); // Header, Child data
            }
            else if(i==5){
                listDataChild.put(listDataHeader.get(i), counterItemList); // Header, Child data
            }
            else {
                listDataChild.put(listDataHeader.get(i), new ArrayList<String>()); // Header, Child data
            }

        }*/
        listDataHeader = new ArrayList<String>();
        listDataChild = new LinkedHashMap<String, List<String>>();
        if(mymenuModel.getData() != null && mymenuModel.getData().size() > 0){
            for(int parent = 0; parent < mymenuModel.getData().size(); parent++){
                childItem = new ArrayList<>();
                if(mymenuModel.getData().get(parent).getSubMenus() != null && mymenuModel.getData().get(parent).getSubMenus().size() > 0){
                    for(int child = 0; child < mymenuModel.getData().get(parent).getSubMenus().size(); child++){

                        Logger.Error("<<<< Child : " + mymenuModel.getData().get(parent).getSubMenus().get(child).getTitle());
                        if(!(mymenuModel.getData().get(parent).getSubMenus().get(child).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/inbox") ||
                                mymenuModel.getData().get(parent).getSubMenus().get(child).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/sent") ||
                                mymenuModel.getData().get(parent).getSubMenus().get(child).getUrl().equalsIgnoreCase("/in-gauge-mail/mail/trash"))){
                            childItem.add(mymenuModel.getData().get(parent).getSubMenus().get(child).getTitle());
                        }
                    }
                }
                try{
                    if(!(mymenuModel.getData().get(parent).getUrl().equalsIgnoreCase("/in-gauge-mail/mail"))){
                        Logger.Error("<<<<< ADDED   >>>>" + mymenuModel.getData().get(parent).getUrl());
                        listDataChild.put(mymenuModel.getData().get(parent).getTitle(), childItem);
                        listDataHeader.add(mymenuModel.getData().get(parent).getTitle());
                    }
                    Logger.Error("<<<< Child : " + mymenuModel.getData().get(parent).getTitle());
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

        listDataChild.put("My Profile", new ArrayList<String>());
        listDataHeader.add("My Profile");

       /* listDataChild.put("Preferences", new ArrayList<String>());
        listDataHeader.add("Preferences");*/

        listDataChild.put("Logout", new ArrayList<String>());
        listDataHeader.add("Logout");

        moreNewAdapter = new MoreNewAdapter(mContext, listDataHeader, listDataChild, menuModel);
        mExpandableListView.setAdapter(moreNewAdapter);

    }

    public static Fragment newInstance(String text, int color){
        Fragment frag = new MoreFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomeActivity.visibleBottomView();
        mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_title_more)));
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.VISIBLE);
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.tvPreferences.setVisibility(View.VISIBLE);
        if(isGuideEnableFromLogin){
            InGaugeSession.write(getString(R.string.key_guide_enabled), false);
            showTenantGuide();

        } else if(isGuideEnableFromChangetenant){
            InGaugeSession.write(getString(R.string.key_guide_enabled_from_preference), false);
            showDashboardFilterGuide();
        }
    }

    private void getRolesAndPermission(String TenantId, final String UserId){
        Call mCall = apiInterface.getRolesAndPermission(TenantId, UserId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                JSONObject data = new JSONObject();
                if(response.body() != null){
                    Logger.Error("ROLES AND PERMISSION RESPONSE " + response.body());
                    try{
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        if(jsonObject.optString("status").equalsIgnoreCase("SUCCESS")){
                            data = jsonObject.optJSONObject("data");
                        }
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_user_role_permission), data.toString());
                        JSONObject rolesAndPermissoinObject = new JSONObject(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_permission), null));
                        mHomeActivity.setRolesAndPermissoinObject(rolesAndPermissoinObject);
                        getMenu("application/json", InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), null), true, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), -1)), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1)), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -1)));

                    } catch(JSONException e){
                        e.printStackTrace();
                        getMenu("application/json", InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), null), true, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), -1)), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1)), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -1)));
                        mHomeActivity.endProgress();
                    }

                } else{
                    getMenu("application/json", InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), null), true, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_industry_id), -1)), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), -1)), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -1)));
                    mHomeActivity.endProgress();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                mHomeActivity.endProgress();
                Logger.Error("Exception " + t.getMessage());
            }
        });
    }
}
