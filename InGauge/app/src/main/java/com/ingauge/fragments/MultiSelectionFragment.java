package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.MultiSelectionAdapter;

/**
 * Created by pathanaa on 14-07-2017.
 */

public class MultiSelectionFragment extends Fragment{
    View rootView;
    private RecyclerView recycleMultiSelectionView;
    private MultiSelectionAdapter multiSelectionAdapter;
    private Context mContext;
    private HomeActivity mHomeActivity;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
        mHomeActivity.tvTitle.setVisibility(View.GONE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.str_done));

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_detail_filter_list, container, false);
        Init();
        mHomeActivity.ibBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){


                mHomeActivity.onBackPressed();
            }
        });
        mHomeActivity.tvApply.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mHomeActivity.onBackPressed();
            }
        });
        return rootView;
    }

    private void Init(){
        recycleMultiSelectionView = (RecyclerView) rootView.findViewById(R.id.fragment_detail_filter_list_recycleview);
        if(getArguments() != null){
            multiSelectionAdapter = new MultiSelectionAdapter(getActivity(), mHomeActivity);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
            recycleMultiSelectionView.setLayoutManager(mLayoutManager);
            recycleMultiSelectionView.setItemAnimator(new DefaultItemAnimator());
            recycleMultiSelectionView.setAdapter(multiSelectionAdapter);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mHomeActivity.tvTitle.setVisibility(View.VISIBLE);
        mHomeActivity.tvApply.setVisibility(View.GONE);

    }

    @Override
    public void onResume(){
        super.onResume();
        //mHomeActivity.toolbar.setClickable(false);
    }
}
