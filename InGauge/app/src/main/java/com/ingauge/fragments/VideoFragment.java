package com.ingauge.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.CustomPagerAdapter;
import com.ingauge.adapters.VideoCategoryListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragmentObserver.FragmentObserver;
import com.ingauge.listener.VideocategoryListener;
import com.ingauge.pojo.VideoModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.temp.FragmentOne;
import com.ingauge.temp.FragmentTwo;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 15-May-17.
 */

public class VideoFragment extends BaseFragment implements VideocategoryListener{

    //    private static final String ARG_TEXT = "arg_text";
//    private static final String ARG_COLOR = "arg_color";

    // private RecyclerView rvVideoList;
    // private RelativeLayout rlCustomProgress;
    private HomeActivity mHomeActivity;
    static AppBarLayout appBarLayout;
    static WebView mWebViewVideo;
    View rootView;
    CustomPagerAdapter mCustomPagerAdapter;
    ViewPager mViewPager;
    private static TextView tvVideoName;
    TabLayout tabLayout;
    VideoCategoryListAdapter mVideoCategoryListAdapter;

    ////////After Complete R&D

    private RecyclerView rvCategroryList;

    APIInterface apiInterface;
    Call<ResponseBody> mCall;
    public static Map<String, List<VideoModel>> mVideoHashMap = new LinkedHashMap<>();
    public static String KEY = "";
    public static String webVideoUrl = "";
    public static String VideoName = "";
    private RelativeLayout rlProgress;
    List<String> mCategoryList = new ArrayList<>();
    ViewPagerAdapter adapter;

    FragmentOne mFragmentOne = new FragmentOne();
    FragmentTwo mFragmentTwo = new FragmentTwo();
    static ProgressBar mProgressBarVideo;

    static TextView tvNoVideoFound;
    private boolean isGuideEnableFromLogin = false;
    private boolean isGuideEnableFromChangetenant = false;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }


    public static Fragment newInstance(String text, int color){
        Fragment frag = new VideoFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
//        args.putString(ARG_TEXT, text);
//        args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        isGuideEnableFromLogin = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_guide_enabled), false);
        isGuideEnableFromChangetenant = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_guide_enabled_from_preference), false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        if(rootView == null){
            rootView = inflater.inflate(R.layout.video_fragment, container, false);
            initView(rootView);
            getVideoAPIDetails();
            adapter = new ViewPagerAdapter(getFragmentManager());
            adapter.addFragment(mFragmentOne, "All Videos");
            adapter.addFragment(mFragmentTwo, "Most Watched Videos");
            mViewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(mViewPager);

        } else{
            if(rootView != null && rootView.getParent() != null){
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }

        }
        // Inflate the layout for this fragment

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onResume(){
        super.onResume();
        Logger.Error("<< Call >>>");
        mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.menu_video));
        mHomeActivity.toolbar.setClickable(false);
        mHomeActivity.visibleBottomView();

        mHomeActivity.tvSubTitle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mVideoCategoryListAdapter.notifyDataSetChanged();
                if(rvCategroryList.getVisibility() == View.GONE){
                    FooterAnimation();
                    Drawable dr = mHomeActivity.getResources().getDrawable(R.drawable.ic_up_arrow);
                    Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
                    Drawable drawable = new BitmapDrawable(mHomeActivity.getResources(), Bitmap.createScaledBitmap(bitmap, 35, 35, true));
                    mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
                    mHomeActivity.tvSubTitle.setCompoundDrawableTintList(ContextCompat.getColorStateList(mHomeActivity, R.color.white));
                    //isClick = true;
                } else if(rvCategroryList.getVisibility() == View.VISIBLE){
                    //isClick = false;
                    headerAnimation();

                    Drawable dr = mHomeActivity.getResources().getDrawable(R.drawable.ic_down_arrow);
                    Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
                    Drawable drawable = new BitmapDrawable(mHomeActivity.getResources(), Bitmap.createScaledBitmap(bitmap, 35, 35, true));

                    mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
                    mHomeActivity.tvSubTitle.setCompoundDrawableTintList(ContextCompat.getColorStateList(mHomeActivity, R.color.white));
                }
            }
        });
        if(isGuideEnableFromLogin){
            InGaugeSession.write(getString(R.string.key_guide_enabled), false);
            showTenantGuide();

        } else if(isGuideEnableFromChangetenant){
            InGaugeSession.write(getString(R.string.key_guide_enabled_from_preference), false);
            showDashboardFilterGuide();
        }
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initView(View view){
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        mProgressBarVideo = (ProgressBar) view.findViewById(R.id.video_fragment_progress_bar);
        //mProgressBarVideo .getIndeterminateDrawable().setColorFilter(mHomeActivity.getResources().getColor(R.color.chart_goal_achieved), android.graphics.PorterDuff.Mode.MULTIPLY);
        tvVideoName = (TextView) view.findViewById(R.id.video_fragment_tv_video_name);
        tvNoVideoFound = (TextView) view.findViewById(R.id.video_fragment_no_video_found);
        ThreeBounce mCubeGrid = new ThreeBounce();
        mProgressBarVideo.setVisibility(View.VISIBLE);
        mProgressBarVideo.setIndeterminateDrawable(mCubeGrid);
        appBarLayout = (AppBarLayout) view.findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);
        mViewPager = (ViewPager) view.findViewById(R.id.htab_viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        mWebViewVideo = (WebView) view.findViewById(R.id.webView);
        mWebViewVideo.setVisibility(View.VISIBLE);
        rvCategroryList = (RecyclerView) view.findViewById(R.id.video_fragment_rcategory_list);
        rvCategroryList.setVisibility(View.GONE);
        //mWebViewVideo.setVisibility ( View.GONE );
        rlProgress = (RelativeLayout) view.findViewById(R.id.custom_progress_rl_main);
        mViewPager.setVisibility(View.GONE);
        rlProgress.setVisibility(View.VISIBLE);
        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener(){
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset){
                if(scrollRange == -1){
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0){
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if(isShow){
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    @Override
    public void getVideoCategoryName(String categoryName){

        KEY = categoryName;
        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_video_category_name), KEY);

        Drawable dr = mHomeActivity.getResources().getDrawable(R.drawable.ic_down_arrow);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        Drawable drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 35, 35, true));


        mHomeActivity.tvSubTitle.setText(KEY);
        mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        mHomeActivity.tvSubTitle.setCompoundDrawableTintList(ContextCompat.getColorStateList(mHomeActivity, R.color.white));


//        mVideoCategoryListAdapter.notifyDataSetChanged();
        if(rvCategroryList.getVisibility() == View.GONE){
            FooterAnimation();
            //isClick = true;
        } else if(rvCategroryList.getVisibility() == View.VISIBLE){
            //isClick = false;
            headerAnimation();
        }
        updateFragments();

    }

    // Adapter for the viewpager using FragmentPagerAdapter
    class ViewPagerAdapter extends FragmentStatePagerAdapter{
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager){
            super(manager);
        }

        private Observable mObservers = new FragmentObserver();

        @Override
        public Fragment getItem(int position){
            /*mObservers.deleteObservers(); // Clear existing observers.
            Fragment fragment = new FragmentOne();
            if(fragment instanceof Observer)
                mObservers.addObserver((Observer) fragment);*/
            return mFragmentList.get(position);
        }

        @Override
        public int getCount(){
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title){

            if(fragment instanceof FragmentOne){
                mObservers.deleteObservers(); // Clear existing observers.

                //if(fragment instanceof Observer)
                mObservers.addObserver((Observer) fragment);
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            } else{
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            }

        }

        @Override
        public CharSequence getPageTitle(int position){
            return mFragmentTitleList.get(position);
        }

        public void updateFragments(){
            mObservers.notifyObservers();
        }
    }

    private void updateFragments(){
        adapter.updateFragments();
    }

    public static void loadwebViewForVideo(){
        tvVideoName.setText(VideoName);
        appBarLayout.setExpanded(true, true);
        WebSettings settings = mWebViewVideo.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        if(Build.VERSION.SDK_INT > 7){
            settings.setPluginState(WebSettings.PluginState.ON);
        } else{
            //settings.setPluginsEnabled(true);
        }
        Logger.Error("<<<<<<  Clicked  Video URL  >>> " + webVideoUrl);

        if(webVideoUrl != null && webVideoUrl.length() > 0){
            tvNoVideoFound.setVisibility(View.GONE);
            mWebViewVideo.loadUrl(webVideoUrl);
            mWebViewVideo.getSettings().setDomStorageEnabled(true);
            mWebViewVideo.getSettings().setAllowFileAccessFromFileURLs(true);
            mWebViewVideo.getSettings().setJavaScriptEnabled(true);
            //mWebView.addJavascriptInterface(new LoadListener(), "HTMLOUT");
       /* mWebViewVideo.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, int newProgress){
                super.onProgressChanged(view, newProgress);
                if(newProgress == 100){
                    mProgressBarVideo.setVisibility(View.GONE);
                    mWebViewVideo.setVisibility(View.VISIBLE);
                }
            }
        });*/
            mWebViewVideo.setWebViewClient(new WebViewClient(){
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url){
                    //Logger.Error(" URL " + url);
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url,
                                          Bitmap favicon){

                }


                @Override
                public void onPageFinished(WebView view, String url){

                    mProgressBarVideo.setVisibility(View.GONE);
                    mWebViewVideo.setVisibility(View.VISIBLE);
               /* view.loadUrl(
                        "javascript:(function() { " +
                                "var element = document.getElementById('tableContainer');"
                                + "element.parentNode.removeChild(element);" +
                                "})()");*/
//                view.loadUrl("javascript:alert(functionThatReturnsSomething)");
                    //view.loadUrl("javascript:window.HTMLOUT.processHTML(document.documentElement.outerHTML.toString());");

                }
            });
        } else{
            tvNoVideoFound.setVisibility(View.VISIBLE);
            mWebViewVideo.setVisibility(View.GONE);
            mProgressBarVideo.setVisibility(View.GONE);
        }

    }

    public void getVideoAPIDetails(){
        mCategoryList = new ArrayList<>();
        String type = "sectionWiseMedia";
        if(getRoleStatus()){
            type = "roleAndPermission";
        }
        mCall = apiInterface.getAllMedia(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0),
                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_role_id), 0), type);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                mViewPager.setVisibility(View.VISIBLE);
                rlProgress.setVisibility(View.GONE);
                if(mVideoHashMap != null && mVideoHashMap.size() > 0){
                    mVideoHashMap.clear();
                }

                int count = 0;
                if(response.body() != null){

                    try{
                        String jsonResponse = response.body().string();
                        Logger.Error("<<< Response  >>>>> " + jsonResponse);
                        JSONObject mJsonObject = new JSONObject(jsonResponse);
                        int statusCode = mJsonObject.optInt("statusCode");
                        JSONObject mDataJsonObject = mJsonObject.optJSONObject("data");
                        if(statusCode == 200){
                            Iterator iterator = mDataJsonObject.keys();
                            int i = 0;
                            while(iterator.hasNext()){
                                i++;
                                iterator.next();
                            }
                            if(i > 0){
                                Iterator inneriterator = mDataJsonObject.keys();
                                mCategoryList.add("All Videos");
                                KEY = "All Videos";
                                while(inneriterator.hasNext()){
                                    String key = (String) inneriterator.next();
                                    mCategoryList.add(key);
                                    JSONArray mJonArray = mDataJsonObject.optJSONArray(key);
                                    fillVideoHashMap(mJonArray, key);
                                    count++;
                                }

                                for(String name : mVideoHashMap.keySet()){
                                    String key = name.toString();
                                    List<VideoModel> mVideoModel = (List<VideoModel>) mVideoHashMap.get(name);
                                    Logger.Error("<< KEY >>>  " + key + "  << Video Id " + mVideoModel.get(0).getId() + " Video Name " + mVideoModel.get(0).getName());
                                }
                                List<VideoModel> videoModelList = new ArrayList<>();
                                for(String key : VideoFragment.mVideoHashMap.keySet()){
                                    videoModelList.addAll(VideoFragment.mVideoHashMap.get(key));
                                }
                                mVideoHashMap.put(KEY, videoModelList);
                                setcategoryAdapter();
                            } else{
                                if(mVideoHashMap != null && mVideoHashMap.size() > 0){
                                    mVideoHashMap.clear();
                                }
                                webVideoUrl = "";
                                mHomeActivity.tvSubTitle.setVisibility(View.GONE);
                                tvNoVideoFound.setVisibility(View.VISIBLE);
                                mWebViewVideo.setVisibility(View.GONE);
                                mProgressBarVideo.setVisibility(View.GONE);
                            }

                        }
                    } catch(IOException e){
                        e.printStackTrace();
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    /*endProgress();
                    Intent mIntent = new Intent(SignInActivity.this, HomeActivity.class);
                    startActivity(mIntent);
                    finish();*/
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){
                /*endProgress();*/
                mViewPager.setVisibility(View.VISIBLE);
                rlProgress.setVisibility(View.GONE);
            }
        });
    }

    public void fillVideoHashMap(JSONArray mJsonArray, String key){

        List<VideoModel> videoModelList = new ArrayList<>();
        for(int i = 0; i < mJsonArray.length(); i++){
            JSONObject mJsonObject = (JSONObject) mJsonArray.opt(i);
            VideoModel videoModel = new VideoModel();
            videoModel.setId(mJsonObject.optInt("id"));
            videoModel.setIndustryId(mJsonObject.optInt("industryId"));
            videoModel.setActiveStatus(mJsonObject.optString("activeStatus"));
            videoModel.setCreatedOn(mJsonObject.optString("createdOn"));
            videoModel.setUpdatedOn(mJsonObject.optString("updatedOn"));
            videoModel.setIndexRelatedOnly(mJsonObject.optBoolean("indexRelatedOnly"));
            videoModel.setIndexMainOnly(mJsonObject.optBoolean("indexMainOnly"));
            videoModel.setMediaId(mJsonObject.optString("mediaId"));
            videoModel.setName(mJsonObject.optString("name"));
            videoModel.setDuration(mJsonObject.optString("duration"));
            videoModel.setDescription(mJsonObject.optString("description"));
            videoModel.setStatus(mJsonObject.optString("status"));
            videoModel.setThumbnailURL(mJsonObject.optString("thumbnailURL"));
            videoModel.setProjectId(mJsonObject.optString("projectId"));
            videoModel.setProjectName(mJsonObject.optString("projectName"));
            videoModel.setEmbedCode(mJsonObject.optString("embedCode"));
            videoModel.setPlayCount(mJsonObject.optInt("playCount"));
            videoModel.setSection(mJsonObject.optString("section"));
            videoModel.setMediaAssets(mJsonObject.optString("mediaAssets"));
            videoModel.setDeleted(mJsonObject.optBoolean("isDeleted"));
            videoModel.setRoleMedia(mJsonObject.optString("roleMedia"));
            videoModelList.add(videoModel);
        }

        mVideoHashMap.put(key, videoModelList);
    }

    private boolean getRoleStatus(){
        String roleName = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_login_tenant_user_role), "");
        if(roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_super_admin))){
            return true;
        } else{
            return false;
        }

    }

    private void setcategoryAdapter(){
        String savedVideoUrl = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_play_video_url), "");
        String categoryKey = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_video_category_name), "");
        String savedVideoName = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_video_name), "");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        rvCategroryList.setLayoutManager(mLayoutManager);
        rvCategroryList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvCategroryList.setHasFixedSize(true);
        rvCategroryList.setItemAnimator(new DefaultItemAnimator());
        if(mCategoryList != null && mCategoryList.size() > 0){
            mVideoCategoryListAdapter = new VideoCategoryListAdapter(InGaugeApp.getInstance(), mCategoryList, this);
            rvCategroryList.setAdapter(mVideoCategoryListAdapter);
            Drawable dr = mHomeActivity.getResources().getDrawable(R.drawable.ic_down_arrow);
            Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
            Drawable drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 35, 35, true));

            if(categoryKey.length() > 0){
                KEY = categoryKey;
            } else{
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_video_category_name), KEY);

            }
            mHomeActivity.tvSubTitle.setText(KEY);
            mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
            mHomeActivity.tvSubTitle.setCompoundDrawableTintList(ContextCompat.getColorStateList(mHomeActivity, R.color.white));

            if(KEY.length() > 0){
                mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                String mediaId = mVideoHashMap.get(KEY).get(0).getMediaId();
                String emailId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_user_email), "");
                int userID = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), -2);
                int IndustryId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), -2);

                VideoName = mVideoHashMap.get(KEY).get(0).getName();
                String videoUrl = UiUtils.baseUrlChart + "vtPlayer?" + "email=" + emailId + "&userId=" + userID + "&mediaHasId=" + mediaId + "&industryId=" + IndustryId
                        + "&authorizationId=" + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), "");
                if(savedVideoName.length() > 0){
                    VideoName = savedVideoName;
                } else{
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_video_name), mVideoHashMap.get(KEY).get(0).getName());
                    VideoName = mVideoHashMap.get(KEY).get(0).getName();
                }
                if(savedVideoUrl.length() > 0){
                    webVideoUrl = savedVideoUrl;
                } else{
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_play_video_url), videoUrl);
                    webVideoUrl = videoUrl;
                }

                Logger.Error("<<<< Video URL >>>>" + videoUrl);
            } else{
                webVideoUrl = "";
                VideoName = "";
                mHomeActivity.tvSubTitle.setVisibility(View.GONE);
            }
        } else{
            webVideoUrl = "";
            VideoName = "";
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }

        loadwebViewForVideo();
    }

    public void FooterAnimation(){
        rvCategroryList.setVisibility(View.VISIBLE);
        Animation hide = AnimationUtils.loadAnimation(mHomeActivity, R.anim.slide_to_bottom);
        rvCategroryList.startAnimation(hide);
    }

    public void headerAnimation(){
        Animation hide = AnimationUtils.loadAnimation(mHomeActivity, R.anim.slide_to_up);
        rvCategroryList.startAnimation(hide);
        rvCategroryList.setVisibility(View.GONE);
    }

    @Override
    public void onDetach(){
        super.onDetach();
        if(mCall != null)
            mCall.cancel();
    }
}
