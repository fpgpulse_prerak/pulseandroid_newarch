package com.ingauge.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.MultiSelectionAdapterForTableReportHeader;
import com.ingauge.listener.TableReportListener;
import com.ingauge.pojo.TableReportColumnPojo;
import com.ingauge.utils.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pathanaa on 14-07-2017.
 */

public class MultiSelectionFragmentForTableReport extends Fragment {


    public static String KEY_COLUMN_LIST = "key_column_list";
    View rootView;
    private RecyclerView recycleMultiSelectionView;
    private MultiSelectionAdapterForTableReportHeader multiSelectionAdapter;
    private Context mContext;
    private List<TableReportColumnPojo> mTableReportColumnPojos = new ArrayList<>();
    private TableReportListener mTableReportListener;
    private HomeActivity mHomeActivity;
    private SearchView mSearchView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) context;
        onAttachToParentFragment(getFragmentManager().findFragmentByTag(getString(R.string.tag_dashboard_detail)));


    }

    public void onAttachToParentFragment(Fragment fragment) {
        try {
            mTableReportListener = (TableReportListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString() + " must implement MyCustomObjectListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_detail_filter_list, container, false);
        if (getArguments() != null) {
            mTableReportColumnPojos = getArguments().getParcelableArrayList(KEY_COLUMN_LIST);
        }
        Init();

        return rootView;
    }

    private void Init() {
        recycleMultiSelectionView = (RecyclerView) rootView.findViewById(R.id.fragment_detail_filter_list_recycleview);
        mSearchView = (SearchView)rootView.findViewById(R.id.fragment_detail_filter_list_search_view);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                multiSelectionAdapter.getFilter().filter(newText);
                return false;
            }
        });
        if (getArguments() != null) {
            multiSelectionAdapter = new MultiSelectionAdapterForTableReportHeader(getActivity(), mTableReportColumnPojos);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
            recycleMultiSelectionView.setLayoutManager(mLayoutManager);
            recycleMultiSelectionView.setItemAnimator(new DefaultItemAnimator());
            recycleMultiSelectionView.setAdapter(multiSelectionAdapter);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.tvApply.setVisibility(View.VISIBLE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.ibAttachment.setVisibility(View.GONE);
        mHomeActivity.tvTitle.setText(mHomeActivity.getResources().getString(R.string.select_column));
        mHomeActivity.tvApply.setText(mHomeActivity.getResources().getString(R.string.str_done));
        mHomeActivity.tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String keyHeaderOne="", keyHeaderTwo="", keyHeaderThree="";
                String valueHeaderOne="", valueHeaderTwo="", valueHeaderThree="";

                int index=0;
                for (int i = 0; i < mTableReportColumnPojos.size(); i++) {

                    //TableReportColumnPojo mTableReportColumnPojo = mTableReportColumnPojos.get(i);
                    if(mTableReportColumnPojos.get(i).isSelected()){
                        if(index==0){
                            keyHeaderOne = mTableReportColumnPojos.get(i).getColumnName();
                            valueHeaderOne= mTableReportColumnPojos.get(i).getColumnKey();
                           // index++;
                        }else if(index==1){
                            keyHeaderTwo= mTableReportColumnPojos.get(i).getColumnName();
                            valueHeaderTwo= mTableReportColumnPojos.get(i).getColumnKey();
                           // index++;
                        }else if(index==2){
                            keyHeaderThree= mTableReportColumnPojos.get(i).getColumnName();
                            valueHeaderThree= mTableReportColumnPojos.get(i).getColumnKey();
                           // index++;
                        }
                        index++;
                    }

                }
                Logger.Error("<<<<<    >>>> " + mTableReportColumnPojos.size());
                if(index==0){
                    new AlertDialog.Builder(mHomeActivity)
                            .setMessage("Please select at least one column")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }else{
                    mTableReportListener.getHeaderName(keyHeaderOne,keyHeaderTwo,keyHeaderThree,valueHeaderOne,valueHeaderTwo,valueHeaderThree);
                    mHomeActivity.onBackPressed();
                }

            }
        });
    }
}
