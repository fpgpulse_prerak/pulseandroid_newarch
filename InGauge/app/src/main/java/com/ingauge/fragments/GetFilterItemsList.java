package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.ingauge.R;
import com.ingauge.adapters.FilterListAdapter;
import com.ingauge.pojo.RegionTypeDataModel;
import com.ingauge.pojo.TenantLocationDataModel;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by desainid on 5/22/2017.
 */

public class GetFilterItemsList extends BaseFragment {


    Context mContext;
    ListView listview;
    FilterListAdapter listadapter;
    List<String> values;
    AppCompatButton btnApplyFilter,btnClearFilter;
    String selectedItems;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fliter_list, container, false);
        initViews(rootView);
        Bundle bundle=getArguments();
        selectedItems=bundle.getString("SelectedItem");
        switch (selectedItems)
        {
            case "0":
                GetRegionList();
                break;
            case "1":
                GetTanenetLocationList();
                break;
            case "2":
                GetLocationGroupList();
                break;
            case "3":
                GetProductList();
                break;
            case "4":
                GetUserList();
                default:
                    break;
        }
        return rootView;
    }

    private void GetUserList() {

    }

    private void GetLocationGroupList() {
    }

    private void GetRegionList() {
        Gson gson = new Gson();
        RegionTypeDataModel regionTypedata = gson.fromJson(UiUtils.loadJSONFromAsset("getRegionType.json", mContext), RegionTypeDataModel.class);
        List<RegionTypeDataModel.Datum> data=regionTypedata.getData();
        for(RegionTypeDataModel.Datum datum: data)
        {
            values.add(datum.getName());
        }
     //   listadapter = new FilterListAdapter(getActivity(), values);
       // listview.setAdapter(listadapter);
    }
    private void GetTanenetLocationList()
    {
        Gson gson = new Gson();
        TenantLocationDataModel tenantLocationdata = gson.fromJson(UiUtils.loadJSONFromAsset("getTenantLocation.json", mContext), TenantLocationDataModel.class);
        List<TenantLocationDataModel.Datum> data=tenantLocationdata.getData();
        for(TenantLocationDataModel.Datum datum: data)
        {
            values.add(datum.getName());
        }
       // listadapter = new FilterListAdapter(getActivity(), values);
        //listview.setAdapter(listadapter);
        
    }
    private void   GetProductList()
    {
        
    }
    private void initViews(View rootView) {
        values = new ArrayList<>();
        btnApplyFilter=(AppCompatButton)rootView.findViewById(R.id.btn_apply_filter);
        btnClearFilter=(AppCompatButton)rootView.findViewById(R.id.btn_clear_filter);
        btnApplyFilter.setVisibility(View.GONE);
        btnClearFilter.setVisibility(View.GONE);
        listview = (ListView) rootView.findViewById(R.id.listView);
      //  listadapter=new FilterListAdapter(getActivity(),values);
        //listview.setAdapter(listadapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }
}
