package com.ingauge.fragments.dateselection.tabsfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by mansurum on 15-Feb-18.
 */

public class DayFragment extends Fragment implements View.OnClickListener{

    View rootView;

    TextView tvTodayDate;
    TextView tvYesterdayDate;
    TextView tvDayBeforeYesterdayDate;
    TextView tvPreviousDate;
    TextView tvSameDayLastWeekDate;
    TextView tvSameDayLastYearDate;

    ImageView ivTodayDate;
    ImageView ivYesterdayDate;
    ImageView ivDayBeforeYesterdayDate;
    ImageView ivPreviousDate;
    ImageView ivSameDayLastWeekDate;
    ImageView ivSameDayLastYearDate;

    SwitchCompat mSwitchCompare;

    LinearLayout llToday;
    LinearLayout llYesterday;
    LinearLayout llDayBeforeYesterday;
    LinearLayout llPreviousDay;
    LinearLayout llSameDayLastWeek;
    LinearLayout llSameDayLastYear;

    LinearLayout llCompare;


    public String startDate = "";
    public String endDate = "";
    public String compareStartDate = "";
    public String compareEndDate = "";

    //Server Dates
    public String serverStartDate = "";
    public String serverEndDate = "";
    public String serverCompareStartDate = "";
    public String serverCompareEndDate = "";


    public boolean isCompare = true;
    public boolean isHideCompare = false;


    /**
     * 0 = Day
     * 1 = Week
     * 2 = Month
     * 3 = Custom
     */
    public int tabIndex = 0;

    public int dateRangeIndex = -1;
    public int ComparedateRangeIndex = -1;

    LinearLayout llLeaderBoardCompareLayout;
    boolean isFromLeaderBoard = false;

    HomeActivity mHomeActivity;


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity)context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        setTabIndex(0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.today_fragment, container, false);
        initViews(rootView);

        if(getArguments() != null){
            Bundle mBundle = getArguments();
            /*startDate = mBundle.getString(DateSelectionFragment.KEY_START_DATE);
            endDate =  mBundle.getString(DateSelectionFragment.KEY_END_DATE);
            compareStartDate = mBundle.getString(DateSelectionFragment.KEY_COMPARE_START_DATE);
            compareEndDate =  mBundle.getString(DateSelectionFragment.KEY_COMPARE_END_DATE);
            serverStartDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_START_DATE);
            serverEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_END_DATE);
            serverCompareStartDate= mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_START_DATE);
            serverCompareEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_END_DATE);*/
            isCompare = mBundle.getBoolean(DateSelectionFragment.KEY_IS_COMPARE);
            isFromLeaderBoard = mBundle.getBoolean(DateSelectionFragment.KEY_IS_FROM_LEADERBOARD);
            isHideCompare  = mBundle.getBoolean(DateSelectionFragment.KEY_IS_HIDE_COMPARE);

        }

        dateRangeIndex = InGaugeSession.read(getString(R.string.key_from_date_range_index), -1);
        ComparedateRangeIndex = InGaugeSession.read(getString(R.string.key_compare_date_range_index), -1);

        if(isFromLeaderBoard){
            llLeaderBoardCompareLayout.setVisibility(View.GONE);
        }else{
            llLeaderBoardCompareLayout.setVisibility(View.VISIBLE);
        }

        if(isHideCompare){
            llLeaderBoardCompareLayout.setVisibility(View.GONE);
        }else{
            llLeaderBoardCompareLayout.setVisibility(View.VISIBLE);
        }
        mSwitchCompare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isChecked){
                    llCompare.setVisibility(View.VISIBLE);
                    setCompare(true);

                    if(getComparedateRangeIndex()==-1){
                        setCompareRangeIndex(1);
                    }else{
                        setCompareRangeIndex(getComparedateRangeIndex());
                    }

                } else{
                    llCompare.setVisibility(View.GONE);
                    setCompare(false);
                }
            }
        });
        ////  Day Tabs
        if(getDateRangeIndex() == -1){
            tvTodayDate.setText(getDisplayString(DateTimeUtils.getTodayDateString(new Date())));
            tvYesterdayDate.setText(getDisplayString(DateTimeUtils.getYesterdayDateString(new Date())));
            tvDayBeforeYesterdayDate.setText(getDisplayString(DateTimeUtils.getDayBeforeYesterdayDateString(new Date())));
            setComparePreviousDate(tvTodayDate.getText().toString());
            setCompareSameDayOfLastWeek(tvTodayDate.getText().toString());
            setCompareSameDayOfLastYear(tvTodayDate.getText().toString());
            dateRangeIndex = 1;
            setRangeIndex(dateRangeIndex);
            if(isCompare()){
                if(getComparedateRangeIndex() == -1 || getComparedateRangeIndex()==4){
                    ComparedateRangeIndex = 1;
                    setCompareRangeIndex(ComparedateRangeIndex);
                    if(ComparedateRangeIndex == 1){
                        setComparePreviousDate(tvTodayDate.getText().toString());
                    } else if(ComparedateRangeIndex == 2){
                        setCompareSameDayOfLastWeek(tvTodayDate.getText().toString());
                    } else if(ComparedateRangeIndex == 3){
                        setCompareSameDayOfLastYear(tvTodayDate.getText().toString());
                    }

                } else{
                    setCompareRangeIndex(getComparedateRangeIndex());
                }
            }
        } else{
            setRangeIndex(getDateRangeIndex());
            if(isCompare()){
                if(getComparedateRangeIndex() == -1){
                    tvTodayDate.setText(getDisplayString(DateTimeUtils.getTodayDateString(new Date())));
                    tvYesterdayDate.setText(getDisplayString(DateTimeUtils.getYesterdayDateString(new Date())));
                    tvDayBeforeYesterdayDate.setText(getDisplayString(DateTimeUtils.getDayBeforeYesterdayDateString(new Date())));
                    setComparePreviousDate(tvTodayDate.getText().toString());
                    setCompareSameDayOfLastWeek(tvTodayDate.getText().toString());
                    setCompareSameDayOfLastYear(tvTodayDate.getText().toString());
                    dateRangeIndex = 1;

                    ComparedateRangeIndex = 1;
                    setRangeIndex(dateRangeIndex);
                    setCompareRangeIndex(ComparedateRangeIndex);
                } else{
                    setCompareRangeIndex(getComparedateRangeIndex());
                }

            }
        }

        if(isCompare()){
            llCompare.setVisibility(View.VISIBLE);
            mSwitchCompare.setChecked(true);
        }else{
            llCompare.setVisibility(View.GONE);
            mSwitchCompare.setChecked(false);
        }

        /*Logger.Error("<<<<  Today  >>>> " + getDisplayString(DateTimeUtils.getTodayDateString(new Date())));
        Logger.Error("<<<<  Yesterday  >>>> " + getDisplayString(DateTimeUtils.getYesterdayDateString(new Date())));
        Logger.Error("<<<<  Before Yesterday  >>>> " + getDisplayString(DateTimeUtils.getDayBeforeYesterdayDateString(new Date())));
        Logger.Error("<<<<  Last Week Range  >>>> " + getDisplayString(DateTimeUtils.getSameDayofLastWeek(new Date())));

        String yesterdayDate = DateTimeUtils.getYesterdayDateString(getDateObjFromDisplay(tvTodayDate.getText().toString()));
        Logger.Error("<<<<  Yesterday  >>>> " + getDisplayString(yesterdayDate));
        Logger.Error("<<<<  Same Day of LastWeek >>>> " + getDisplayString(DateTimeUtils.getSameDayofLastWeek(getDateObjFromDisplay(tvTodayDate.getText().toString()))));
        Logger.Error("<<<<  " + yesterdayDate + " of Last Year >>>> " + getDisplayStringWithYear(DateTimeUtils.getSameDayofLastYear(getDateObj(yesterdayDate))));
        Logger.Error("<<<<  Today  of Last Year >>>> " + getDisplayStringWithYear(DateTimeUtils.getSameDayofLastYear(new Date())));*/


       /* Logger.Error("<<<<<<<<<<    WEEK   >>>>>>>>>");
        //// Week Tabs

        Logger.Error("<<<<  Get Last Seven Days Range >>>> " + getLastSevenDaysRange(new Date()));

        Logger.Error("<<<<  Get Start Date of Week >>>> " + getFirstDateofthisWeek());

        Logger.Error("<<<<  Get Last Date of Week >>>> " + getLastDateofthisWeek());

        Logger.Error("<<<<  Get Last Week First Day>>>> " + getLastWeekFirstDay(new Date()));

        Logger.Error("<<<<  Get Last Week Last Day>>>> " + getLastWeekLastDay(new Date()));

        Logger.Error("<<<<  Get Previous Week Slab >>>> " + getLastSevenDaysRange(getDateObj(getFirstDateofthisWeek())));

        Logger.Error("<<<<  Get Last Four Week Slab First Date >>>> " + getLastFourWeekFirstDay(new Date()));

        Logger.Error("<<<<  Get Last Four Week Slab Last Date >>>> " + getLastFourWeekLastDay(new Date()));

        Logger.Error("<<<<  Get Last Four Week Slab Last Date >>>> " + getLastFourWeekLastDay(new Date()));

        Logger.Error("<<<<  Get Last Thirty Days Range >>>> " + getLastThirtyDaysRange(new Date()));*/
        return rootView;
    }


    private void setRangeIndex(int rangeIndex){
        tvTodayDate.setText(getDisplayString(DateTimeUtils.getTodayDateString(new Date())));
        tvYesterdayDate.setText(getDisplayString(DateTimeUtils.getYesterdayDateString(new Date())));
        tvDayBeforeYesterdayDate.setText(getDisplayString(DateTimeUtils.getDayBeforeYesterdayDateString(new Date())));

        switch(rangeIndex){
            case 1:


                /*if(ComparedateRangeIndex == 1){
                    setComparePreviousDate(tvTodayDate.getText().toString());
                } else if(ComparedateRangeIndex == 2){
                    setCompareSameDayOfLastWeek(tvTodayDate.getText().toString());
                } else if(ComparedateRangeIndex == 3){
                    setCompareSameDayOfLastYear(tvTodayDate.getText().toString());
                }*/
                setComparePreviousDate(tvTodayDate.getText().toString());
                setCompareSameDayOfLastWeek(tvTodayDate.getText().toString());
                setCompareSameDayOfLastYear(tvTodayDate.getText().toString());
                visibleRightTick(1);
                break;
            case 2:
               // startDate = tvYesterdayDate.getText().toString();
              //  tvYesterdayDate.setText(getDisplayString(DateTimeUtils.getYesterdayDateString(new Date())));
                /*if(ComparedateRangeIndex == 1){
                    setComparePreviousDate(tvYesterdayDate.getText().toString());
                } else if(ComparedateRangeIndex == 2){
                    setCompareSameDayOfLastWeek(tvYesterdayDate.getText().toString());
                } else if(ComparedateRangeIndex == 3){
                    setCompareSameDayOfLastYear(tvYesterdayDate.getText().toString());
                }*/
                setComparePreviousDate(tvYesterdayDate.getText().toString());
                setCompareSameDayOfLastWeek(tvYesterdayDate.getText().toString());
                setCompareSameDayOfLastYear(tvYesterdayDate.getText().toString());
                visibleRightTick(2);

                break;
            case 3:

                /*if(ComparedateRangeIndex == 1){
                    setComparePreviousDate(tvDayBeforeYesterdayDate.getText().toString());
                } else if(ComparedateRangeIndex == 2){
                    setCompareSameDayOfLastWeek(tvDayBeforeYesterdayDate.getText().toString());
                } else if(ComparedateRangeIndex == 3){
                    setCompareSameDayOfLastYear(tvDayBeforeYesterdayDate.getText().toString());
                }*/
                setComparePreviousDate(tvDayBeforeYesterdayDate.getText().toString());
                setCompareSameDayOfLastWeek(tvDayBeforeYesterdayDate.getText().toString());
                setCompareSameDayOfLastYear(tvDayBeforeYesterdayDate.getText().toString());
                visibleRightTick(3);
                //startDate = tvDayBeforeYesterdayDate.getText().toString();
                break;
        }
    }

    private void setCompareRangeIndex(int rangeIndex){
        switch(rangeIndex){
            case 1:
                visibleCompareRightTick(1);
                break;
            case 2:
                visibleCompareRightTick(2);
                break;
            case 3:
                visibleCompareRightTick(3);
                break;
        }
    }

    private void initViews(View itemView){

        llLeaderBoardCompareLayout = (LinearLayout)itemView.findViewById(R.id.today_fragment_ll_compare_for_leader_board);
        tvTodayDate = (TextView) itemView.findViewById(R.id.today_fragment_tv_today_date);
        tvYesterdayDate = (TextView) itemView.findViewById(R.id.today_fragment_tv_yesterday_date);
        tvDayBeforeYesterdayDate = (TextView) itemView.findViewById(R.id.today_fragment_tv_day_bfore_yesterday_date);
        tvPreviousDate = (TextView) itemView.findViewById(R.id.today_fragment_tv_previous_date);
        tvSameDayLastWeekDate = (TextView) itemView.findViewById(R.id.today_fragment_tv_same_day_last_week_date);
        tvSameDayLastYearDate = (TextView) itemView.findViewById(R.id.today_fragment_tv_same_day_last_year_date);
        mSwitchCompare = (SwitchCompat) itemView.findViewById(R.id.switch_compare_date);
        ivTodayDate = (ImageView) itemView.findViewById(R.id.today_fragment_iv_today);
        ivYesterdayDate = (ImageView) itemView.findViewById(R.id.today_fragment_iv_yesterday);
        ivDayBeforeYesterdayDate = (ImageView) itemView.findViewById(R.id.today_fragment_iv_day_before_yesterday);
        ivPreviousDate = (ImageView) itemView.findViewById(R.id.today_fragment_iv_previous_day);
        ivSameDayLastWeekDate = (ImageView) itemView.findViewById(R.id.today_fragment_iv_same_day_last_week);
        ivSameDayLastYearDate = (ImageView) itemView.findViewById(R.id.today_fragment_iv_same_day_last_year);
        llCompare = (LinearLayout) itemView.findViewById(R.id.today_fragment_ll_compare);
        llToday = (LinearLayout) itemView.findViewById(R.id.today_fragment_ll_today);
        llToday.setOnClickListener(this);
        llYesterday = (LinearLayout) itemView.findViewById(R.id.today_fragment_ll_yesterday);
        llYesterday.setOnClickListener(this);
        llDayBeforeYesterday = (LinearLayout) itemView.findViewById(R.id.today_fragment_ll_day_before_yesterday);
        llDayBeforeYesterday.setOnClickListener(this);
        llPreviousDay = (LinearLayout) itemView.findViewById(R.id.today_fragment_ll_previous_day);
        llPreviousDay.setOnClickListener(this);
        llSameDayLastWeek = (LinearLayout) itemView.findViewById(R.id.today_fragment_ll_same_day_last_week);
        llSameDayLastWeek.setOnClickListener(this);
        llSameDayLastYear = (LinearLayout) itemView.findViewById(R.id.today_fragment_ll_same_day_last_year);
        llSameDayLastYear.setOnClickListener(this);

    }


    @Override
    public void onResume(){
        super.onResume();
        //mHomeActivity.tvTitle.setText("Day");
    }

    @Override
    public void onClick(View view){

        switch(view.getId()){
            case R.id.today_fragment_ll_today:
                visibleRightTick(1);
                setComparePreviousDate(tvTodayDate.getText().toString());
                setCompareSameDayOfLastWeek(tvTodayDate.getText().toString());
                setCompareSameDayOfLastYear(tvTodayDate.getText().toString());
                if(ComparedateRangeIndex==1){
                    visibleCompareRightTick(1);
                }else if(ComparedateRangeIndex==2){
                    visibleCompareRightTick(2);
                }else if(ComparedateRangeIndex==3){
                    visibleCompareRightTick(3);
                }
                break;
            case R.id.today_fragment_ll_yesterday:
                visibleRightTick(2);
                setComparePreviousDate(tvYesterdayDate.getText().toString());
                setCompareSameDayOfLastWeek(tvYesterdayDate.getText().toString());
                setCompareSameDayOfLastYear(tvYesterdayDate.getText().toString());
                if(ComparedateRangeIndex==1){
                    visibleCompareRightTick(1);
                }else if(ComparedateRangeIndex==2){
                    visibleCompareRightTick(2);
                }else if(ComparedateRangeIndex==3){
                    visibleCompareRightTick(3);
                }
                break;
            case R.id.today_fragment_ll_day_before_yesterday:
                visibleRightTick(3);
                setComparePreviousDate(tvDayBeforeYesterdayDate.getText().toString());
                setCompareSameDayOfLastWeek(tvDayBeforeYesterdayDate.getText().toString());
                setCompareSameDayOfLastYear(tvDayBeforeYesterdayDate.getText().toString());
                if(ComparedateRangeIndex==1){
                    visibleCompareRightTick(1);
                }else if(ComparedateRangeIndex==2){
                    visibleCompareRightTick(2);
                }else if(ComparedateRangeIndex==3){
                    visibleCompareRightTick(3);
                }
                break;
            case R.id.today_fragment_ll_previous_day:
                visibleCompareRightTick(1);
                ComparedateRangeIndex = 1;
                break;
            case R.id.today_fragment_ll_same_day_last_week:
                visibleCompareRightTick(2);
                ComparedateRangeIndex = 2;
                break;
            case R.id.today_fragment_ll_same_day_last_year:
                visibleCompareRightTick(3);
                ComparedateRangeIndex = 3;
                break;
        }
    }

    public void visibleRightTick(int position){
        switch(position){
            case 1:
                ivTodayDate.setVisibility(View.VISIBLE);
                ivYesterdayDate.setVisibility(View.GONE);
                ivDayBeforeYesterdayDate.setVisibility(View.GONE);
                startDate = tvTodayDate.getText().toString();
                serverStartDate = getServerDateFormat(tvTodayDate.getText().toString());
                endDate = tvTodayDate.getText().toString();
                serverEndDate = getServerDateFormat(tvTodayDate.getText().toString());
                dateRangeIndex = 1;
                break;
            case 2:
                startDate = tvYesterdayDate.getText().toString();
                serverStartDate = getServerDateFormat(tvYesterdayDate.getText().toString());
                endDate = tvYesterdayDate.getText().toString();
                serverEndDate = getServerDateFormat(tvYesterdayDate.getText().toString());
                ivTodayDate.setVisibility(View.GONE);
                ivYesterdayDate.setVisibility(View.VISIBLE);
                ivDayBeforeYesterdayDate.setVisibility(View.GONE);
                dateRangeIndex = 2;
                break;
            case 3:

                startDate = tvDayBeforeYesterdayDate.getText().toString();
                serverStartDate = getServerDateFormat(tvDayBeforeYesterdayDate.getText().toString());
                endDate = tvDayBeforeYesterdayDate.getText().toString();
                serverEndDate = getServerDateFormat(tvDayBeforeYesterdayDate.getText().toString());

                ivTodayDate.setVisibility(View.GONE);
                ivYesterdayDate.setVisibility(View.GONE);
                ivDayBeforeYesterdayDate.setVisibility(View.VISIBLE);
                dateRangeIndex = 3;
                break;
        }
    }

    public void visibleCompareRightTick(int position){
        switch(position){
            case 1:
                ivPreviousDate.setVisibility(View.VISIBLE);
                ivSameDayLastWeekDate.setVisibility(View.GONE);
                ivSameDayLastYearDate.setVisibility(View.GONE);
                compareStartDate = tvPreviousDate.getText().toString();
                compareEndDate = tvPreviousDate.getText().toString();
                serverCompareStartDate = getServerDateFormat(tvPreviousDate.getText().toString());
                serverCompareEndDate = getServerDateFormat(tvPreviousDate.getText().toString());
                break;
            case 2:
                ivPreviousDate.setVisibility(View.GONE);
                ivSameDayLastWeekDate.setVisibility(View.VISIBLE);
                ivSameDayLastYearDate.setVisibility(View.GONE);

                compareStartDate = tvSameDayLastWeekDate.getText().toString();
                compareEndDate = tvSameDayLastWeekDate.getText().toString();
                serverCompareStartDate = getServerDateFormat(tvSameDayLastWeekDate.getText().toString());
                serverCompareEndDate = getServerDateFormat(tvSameDayLastWeekDate.getText().toString());
                break;
            case 3:
                ivPreviousDate.setVisibility(View.GONE);
                ivSameDayLastWeekDate.setVisibility(View.GONE);
                ivSameDayLastYearDate.setVisibility(View.VISIBLE);

                compareStartDate = tvSameDayLastYearDate.getText().toString();
                compareEndDate = tvSameDayLastYearDate.getText().toString();
                serverCompareStartDate = getServerDateFormat(tvSameDayLastYearDate.getText().toString());
                serverCompareEndDate = getServerDateFormat(tvSameDayLastYearDate.getText().toString());
                break;
        }
    }

    public void setComparePreviousDate(String dateStr){

        String previousdayDate = getDisplayString(DateTimeUtils.getYesterdayDateString(getDateObjFromDisplay(dateStr)));
        tvPreviousDate.setText(previousdayDate);


    }

    public void setCompareSameDayOfLastWeek(String dateStr){

        String sameDayOfLastWeekDate = getDisplayString(DateTimeUtils.getSameDayofLastWeek(getDateObjFromDisplay(dateStr)));
        tvSameDayLastWeekDate.setText(sameDayOfLastWeekDate);




    }

    public void setCompareSameDayOfLastYear(String dateStr){

        String sameDayOfLastYearDate = getDisplayStringWithYear(DateTimeUtils.getSameDayofLastYear(getDateObjFromDisplay(dateStr)));
        tvSameDayLastYearDate.setText(sameDayOfLastYearDate);


    }

    public Date getDateObjFromDisplay(String dateStr){
        try{
            SimpleDateFormat mDateFormat = new SimpleDateFormat("EEEE, MMM dd yyyy");
            return mDateFormat.parse(dateStr);
        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public Date getDateObj(String dateStr){
        try{
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return mDateFormat.parse(dateStr);
        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public String getDisplayString(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("EEEE, MMM dd yyyy");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param DateStr
     * @return For Day Tab
     */
    public String getDisplayStringWithYear(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("EEEE, MMM dd yyyy");

        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param DateStr
     * @return For Week Tab
     */
    public String getDisplayStringForWeekTab(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }


    public String getLastSevenDaysRange(Date inputDate){
        try{
            String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
            String sameDayofLastWeek = DateTimeUtils.getSameDayofLastWeek(inputDate);
            return getDisplayStringForWeekTab(sameDayofLastWeek) + "-" + getDisplayStringForWeekTab(yestrerday);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }


    public static int getLastDayOfWeek(Calendar cal){
        int last = cal.getFirstDayOfWeek() - Calendar.SUNDAY;

        if(last == 0){
            last = Calendar.SATURDAY;
        }

        return last;
    }


    public String getFirstDateofthisWeek(){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.set(Calendar.DAY_OF_WEEK, 1);
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());
    }

    public String getLastDateofthisWeek(){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.set(Calendar.DAY_OF_WEEK, getLastDayOfWeek(cal));
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());
    }

    public String getLastWeekFirstDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DATE, -7);
        cal.set(Calendar.DAY_OF_WEEK, 1);
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());

    }

    public String getLastWeekLastDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.DATE, -7);
        cal.set(Calendar.DAY_OF_WEEK, 7);
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());

    }

    public String getLastFourWeekFirstDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.WEEK_OF_MONTH, -4);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.DAY_OF_MONTH);
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());

    }

    public String getLastFourWeekLastDay(Date mDate){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        cal.add(Calendar.WEEK_OF_MONTH, -4);
        int index = Calendar.DAY_OF_MONTH + 7;
        cal.set(Calendar.DAY_OF_WEEK, 7);
        int s = cal.get(Calendar.DATE);
        Logger.Error("  S  " + s);
        return sdf.format(cal.getTime());

    }

    public String getLastThirtyDaysRange(Date inputDate){
        try{
            String yestrerday = DateTimeUtils.getYesterdayDateString(inputDate);
            String sameDayofLastWeek = DateTimeUtils.getLastThirtyDayFromToday(inputDate);
            return getDisplayStringForWeekTab(sameDayofLastWeek) + "-" + getDisplayStringForWeekTab(yestrerday);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getCurrentMonth(){
        return (String) android.text.format.DateFormat.format("MMMM", new Date());

    }

    public String getLastMonth(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat monthDate = new SimpleDateFormat("MMMM yyyy");
        cal.add(Calendar.MONTH, -1);
        return monthDate.format(cal.getTime());

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        tabIndex = 0;
        Logger.Error("<<<< Tab Index  >>>> " + getTabIndex());
        setTabIndex(0);
    }


    public String getServerDateFormat(String inputDateStr){

        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat inputFormat = new SimpleDateFormat("EEEE, MMM dd yyyy");
        try{
            Date inputDate = inputFormat.parse(inputDateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public int getDateRangeIndex(){
        return dateRangeIndex;
    }

    public void setDateRangeIndex(int dateRangeIndex){
        this.dateRangeIndex = dateRangeIndex;
    }

    public int getComparedateRangeIndex(){
        return ComparedateRangeIndex;
    }

    public void setComparedateRangeIndex(int comparedateRangeIndex){
        ComparedateRangeIndex = comparedateRangeIndex;
    }

    public String getServerStartDate(){
        return serverStartDate;
    }

    public void setServerStartDate(String serverStartDate){
        this.serverStartDate = serverStartDate;
    }

    public String getServerEndDate(){
        return serverEndDate;
    }

    public void setServerEndDate(String serverEndDate){
        this.serverEndDate = serverEndDate;
    }

    public String getServerCompareStartDate(){
        return serverCompareStartDate;
    }

    public void setServerCompareStartDate(String serverCompareStartDate){
        this.serverCompareStartDate = serverCompareStartDate;
    }

    public String getServerCompareEndDate(){
        return serverCompareEndDate;
    }

    public void setServerCompareEndDate(String serverCompareEndDate){
        this.serverCompareEndDate = serverCompareEndDate;
    }

    public boolean isCompare(){
        return isCompare;
    }

    public void setCompare(boolean compare){
        isCompare = compare;
    }

    public String getStartDate(){
        return startDate;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getEndDate(){
        return endDate;
    }

    public void setEndDate(String endDate){
        this.endDate = endDate;
    }

    public String getCompareStartDate(){
        return compareStartDate;
    }

    public void setCompareStartDate(String compareStartDate){
        this.compareStartDate = compareStartDate;
    }

    public String getCompareEndDate(){
        return compareEndDate;
    }

    public void setCompareEndDate(String compareEndDate){
        this.compareEndDate = compareEndDate;
    }
}
