/*
package com.ingauge.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.ObservationTabActivity;
import com.ingauge.adapters.FilterListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.ObservationDashboardFilterListener;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ObservationDashboardStatusModel;
import com.ingauge.pojo.ObservationSurveyCategoryModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;

*/
/**
 * Created by desainid on 5/22/2017.
 *//*


public class ObservationDashboardFilterFragment extends Fragment implements RecyclerViewClickListener, View.OnClickListener, ObservationDashboardFilterListener {
    private Context mContext;
    private View rootView;
    private RecyclerView rvObserveAgentFilter;
    private AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModelForObserveAgent;
    private ObservationSurveyCategoryModel observationSurveyCategoryModel;
    private ArrayList<ObservationDashboardStatusModel> observationDashboardStatusModelArrayList;
    private Call mCall;
    HomeActivity mHomeActivity;
    private APIInterface apiInterface;
    public static FilterListAdapter listadapter;
    public static final String KEY_FILTER_DETAILS = "filter_details";
//    public static final String KEY_FILTER_SELECTION = "filter_seclection";
    public static final String KEY_SELECTION_POSITION = "seclected_position";
    private SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
    private AppCompatButton btnApplyFilter;
    private AppCompatButton btnClearFilter;
    public ArrayList<FilterBaseData> mFilterBaseDatasforObsFilter;
    public static String val0 = "", val1 = "", val2 = "";
    public static String id0 = "", id1 = "", id2 = "";
    public static boolean isFill0 = false, isFill1 = false, isFill2 = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_observation_dashboard_filter, container, false);
        Init();
        return rootView;
    }

    private void Init() {
        rvObserveAgentFilter = (RecyclerView) rootView.findViewById(R.id.rv_observeagent_filter);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mFilterBaseDatasforObsFilter = new ArrayList<>();


        btnApplyFilter = (AppCompatButton) rootView.findViewById(R.id.btn_apply_filter);
        btnApplyFilter.setOnClickListener(this);

        btnClearFilter = (AppCompatButton) rootView.findViewById(R.id.btn_clear_filter);
        btnClearFilter.setOnClickListener(this);

        Logger.Error("FILTER ARRAY SIZE=====================" + mFilterBaseDatasforObsFilter.size());

        if (getArguments() != null) {
            mAccessibleTenantLocationDataModelForObserveAgent = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_ONE);
            observationSurveyCategoryModel = (ObservationSurveyCategoryModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_TWO);
            observationDashboardStatusModelArrayList = (ArrayList) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS_THREE);

            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 0;
            mFilterBaseData.filterName = "Location";
            mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_id), null);
            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_name), null);
            mFilterBaseDatasforObsFilter.add(mFilterBaseData);

            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 1;
            mFilterBaseData.filterName = "Category";
            mFilterBaseData.id = String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), null));
            mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_name), null);
            mFilterBaseDatasforObsFilter.add(mFilterBaseData);

            mFilterBaseData = new FilterBaseData();
            mFilterBaseData.filterIndex = 2;
            mFilterBaseData.filterName = "Status";
            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), null) == null) {
                mFilterBaseData.id = String.valueOf(observationDashboardStatusModelArrayList.get(0).getStatusID());
                mFilterBaseData.name = observationDashboardStatusModelArrayList.get(0).getStatusName();
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_name), String.valueOf(observationDashboardStatusModelArrayList.get(0).getStatusName()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), String.valueOf(observationDashboardStatusModelArrayList.get(0).getStatusID()));
            } else {
                mFilterBaseData.id = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), null);
                mFilterBaseData.name = InGaugeSession.read(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_name), null);
            }
            mFilterBaseDatasforObsFilter.add(mFilterBaseData);


            setFilterAdapter(mFilterBaseDatasforObsFilter);
        }

    }

    private void setFilterAdapter(ArrayList<FilterBaseData> mFilterBaseDataArrayList) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        rvObserveAgentFilter.setLayoutManager(mLayoutManager);
        rvObserveAgentFilter.setItemAnimator(new DefaultItemAnimator());
        rvObserveAgentFilter.setVisibility(View.GONE);
        rvObserveAgentFilter.setVisibility(View.VISIBLE);
        listadapter = new FilterListAdapter(mHomeActivity, mFilterBaseDataArrayList, this, false);
        rvObserveAgentFilter.setAdapter(listadapter);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        mHomeActivity.tvTitle.setText("Filter");
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(false);
    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterposition, boolean isLeftFilter) {
        sendListData(position, filterposition);
    }

    public void sendListData(int position, int filterposition) {
        Bundle mBundle = new Bundle();
        switch (position) {
            case 0:
                mBundle.putSerializable(KEY_FILTER_DETAILS, mAccessibleTenantLocationDataModelForObserveAgent);
                break;
            case 1:
                mBundle.putSerializable(KEY_FILTER_DETAILS, observationSurveyCategoryModel);
                break;
            case 2:
                mBundle.putSerializable(KEY_FILTER_DETAILS, observationDashboardStatusModelArrayList);
                break;
        }

        FilterBaseData mFilterBaseData = mFilterBaseDatasforObsFilter.get(filterposition);
       // mBundle.putParcelable(KEY_FILTER_SELECTION, mFilterBaseData);
        mBundle.putInt(KEY_SELECTION_POSITION, position);
        FilterDetailListFragmentForObservationDashboard mFilterDetailListFragmentforObservation = new FilterDetailListFragmentForObservationDashboard();
        mFilterDetailListFragmentforObservation.setArguments(mBundle);

        mHomeActivity.replace(mFilterDetailListFragmentforObservation, mContext.getResources().getString(R.string.tag_filter_detail_list));
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_get_started: {
                InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_getstarted_millis), String.valueOf(System.currentTimeMillis()));
                Intent intent = new Intent(getActivity(), ObservationTabActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                */
/*FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(this);
                trans.commit();
                manager.popBackStack();*//*

                startActivity(intent);
                break;
            }
            case R.id.btn_apply_filter: {
                UpdatePreferences(false);
                break;
            }

            case R.id.btn_clear_filter: {
                UpdatePreferences(true);
                break;
            }
        }
    }

    public void UpdatePreferences(boolean isClear) {
        String tenantLocationId = null;
        String tenantLocationName = null;
        String surveyCategoryId = null;
        String surveyCategoryName = null;
        String statusId = null;
        String statusName = null;

        if (!isClear) {
            tenantLocationId = String.valueOf(mFilterBaseDatasforObsFilter.get(0).id);
            tenantLocationName = String.valueOf(mFilterBaseDatasforObsFilter.get(0).name);

            surveyCategoryId = String.valueOf(mFilterBaseDatasforObsFilter.get(1).id);
            surveyCategoryName = String.valueOf(mFilterBaseDatasforObsFilter.get(1).name);

            statusId = String.valueOf(mFilterBaseDatasforObsFilter.get(2).id);
            statusName = String.valueOf(mFilterBaseDatasforObsFilter.get(2).name);
        }

        //Location Prefernce
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_id), tenantLocationId);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_location_name), tenantLocationName);

        //Survey Cateogry Preference
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_id), surveyCategoryId);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_tenant_survey_category_name), surveyCategoryName);

        //Status Preference
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_id), statusId);
        InGaugeSession.write(mContext.getResources().getString(R.string.key_obs_dashboard_selected_status_name), statusName);

        mHomeActivity.onBackPressed();
    }

    @Override
    public void updateData(String itemName, String itemId, int position, ArrayList<FilterBaseData> mFilterBaseData) {

        if (position == 0) {
            id0 = itemId;
            val0 = itemName;
            isFill0=true;
        } else if (position == 1) {
            id1 = itemId;
            val1 = itemName;
            isFill1=true;
        } else {
            id2 = itemId;
            val2 = itemName;
            isFill2=true;
        }


        if(isFill0){
            mFilterBaseDatasforObsFilter.get(0).name = val0;
            mFilterBaseDatasforObsFilter.get(0).id = id0;
        }

        if(isFill1){
            mFilterBaseDatasforObsFilter.get(1).name = val1;
            mFilterBaseDatasforObsFilter.get(1).id = id1;
        }

        if(isFill2){
            mFilterBaseDatasforObsFilter.get(2).name = val2;
            mFilterBaseDatasforObsFilter.get(2).id = id2;
        }

        listadapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        listadapter = null;
        mFilterBaseDatasforObsFilter = null;

        id0=id1=id2=null;
        val0=val1=val2=null;
        isFill0=isFill1=isFill2=false;
    }
}
*/
