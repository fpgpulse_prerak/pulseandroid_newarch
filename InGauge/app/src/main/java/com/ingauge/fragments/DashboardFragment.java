package com.ingauge.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.DashboardPanelAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.listener.RecyclerViewClickListenerForDashboardDetails;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.CustomDashboardModel;
import com.ingauge.pojo.DashboardGraphList;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardModelList;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.DynamicData;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.pojo.RegionByTenantRegionTypeModel;
import com.ingauge.pojo.RegionTypeDataModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 18-May-17.
 */

public class DashboardFragment extends BaseFragment implements View.OnClickListener, RecyclerViewClickListenerForDashboardDetails {
    Context mContext;
    //    private static final String ARG_TEXT = "arg_text";
    //    private static final String ARG_COLOR = "arg_color";
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    DashboardGraphList dashboardGraphList;
    private RecyclerView rvDashboardList;
    private ImageView imgFilter;
    ArrayList<FilterBaseData> mFilterBaseDatas;
    APIInterface apiInterface;
    private TextView tvFromDate;
    private TextView tvToDate;
    private int mYear, mMonth, mDay;
    Calendar calendar;
    private String fromDate = "";
    private String toDate = "";
    private SwipeRefreshLayout mSwipeRefreshLayout;
    Call mCall;
    private RelativeLayout mViewDate;
    private LinearLayout mViewDateCompare;
    private TextView tvSelectDate;
    private TextView tvSelectPreviousDate;
    RelativeLayout relativeDateCompare;
    private SwitchCompat mSwitchButton;
    private Button mButtonApply;
    private Button mButtonCancel;
    private ProgressBar pbFilter;
    private HomeActivity mHomeActivity;

    private RelativeLayout rlProgress;
    List<CustomDashboardModel.CustomReport> mCustomReportList = new ArrayList<>();

    /*   RegionTypeDataModel mRegionTypeDataModel;
       RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel;
       AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel;
       DashboardLocationGroupListModel mDashboardLocationGroupListModel;
       ProductListFromLocationGroupModel mProductListFromLocationGroupModel;
       DashboardUserListModel mDashboardUserListModel;*/
    //private ArrayList<String> mDashboardID;
    private TextView tvNoData;
    int selectedDashboardId;
    private String dashboardName = "";

    private List<String> mFilterListString = new ArrayList<>();

    List<DynamicData> mCountryList;

    private View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mHomeActivity = (HomeActivity) mContext;
    }


    public static Fragment newInstance(String text, int color) {
        Fragment frag = new DashboardFragment();
        Bundle args = new Bundle();
//        TODO : This will  use for that comes the data from Home
//        args.putString(ARG_TEXT, text);
//        args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFilterBaseDatas = new ArrayList<>();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mCountryList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {
            Logger.Error("<<<< View ==null >>>>>");
            view = inflater.inflate(R.layout.dashboard_fragment, container, false);
            rvDashboardList = (RecyclerView) view.findViewById(R.id.rv_feeds_list);

            GridLayoutManager mGridLayoutManager = new GridLayoutManager(mHomeActivity, 2);
            rvDashboardList.setHasFixedSize(true);
            rvDashboardList.setLayoutManager(mGridLayoutManager);
        /*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
        rvDashboardList.setLayoutManager(mLayoutManager);
        rvDashboardList.setItemAnimator(new DefaultItemAnimator());*/
            tvNoData = (TextView) view.findViewById(R.id.tv_nodata);
            relativeDateCompare = (RelativeLayout) view.findViewById(R.id.relative_date_compare);
            tvFromDate = (TextView) view.findViewById(R.id.tv_fromDate);
            tvToDate = (TextView) view.findViewById(R.id.tv_toDate);
            imgFilter = (ImageView) view.findViewById(R.id.img_filter);
            tvSelectDate = (TextView) view.findViewById(R.id.tv_selectdate);
            tvSelectPreviousDate = (TextView) view.findViewById(R.id.tv_previousdate);
            mButtonApply = (Button) view.findViewById(R.id.btn_apply_date);
            mButtonCancel = (Button) view.findViewById(R.id.btn_cancel_date);
            pbFilter = (ProgressBar) view.findViewById(R.id.dashboard_fragment_pb);
            imgFilter.setOnClickListener(this);
            calendar = Calendar.getInstance();
            tvFromDate.setOnClickListener(this);
            tvToDate.setOnClickListener(this);
            rlProgress = (RelativeLayout) view.findViewById(R.id.custom_progress_rl_main);
            rvDashboardList.setVisibility(View.VISIBLE);
            rlProgress.setVisibility(View.VISIBLE);
            relativeDateCompare.setOnClickListener(this);

            Calendar calendar;
            calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Date date = calendar.getTime();

            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "").equalsIgnoreCase("")) {
                InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date), sdf.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date), sdf.format(new Date()));

                InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_server), sdfServer.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_server), sdfServer.format(new Date()));

            }
            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "").equalsIgnoreCase("")) {
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date), sdf.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date), sdf.format(new Date()));

                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(new Date()));

            }
            tvSelectDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "") + " To " + InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date), ""));
            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_is_compare_on), false)) {
                tvSelectPreviousDate.setVisibility(View.VISIBLE);
                tvSelectPreviousDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "") + " To " + InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_end_date), ""));
            }
      /*  if(arg!=null)
        {
            tvSelectPreviousDate.setVisibility(View.VISIBLE);
            tvSelectPreviousDate.setText("ABC");

        }*/
     /*   mSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isSwitchOn) {

                if (isSwitchOn) {
                    mViewDateCompare.setVisibility(View.VISIBLE);
                } else {
                    mViewDateCompare.setVisibility(View.GONE);
                }
            }
        });*/

            mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
            mSwipeRefreshLayout.setColorSchemeResources(
                    R.color.in_guage_blue,
                    R.color.rv_grey_color,
                    R.color.red);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Refresh items
                    refreshItems();
                }
            });


            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "").equalsIgnoreCase("")) {
                InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date), sdf.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_start_date_server), sdfServer.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date), sdf.format(new Date()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_end_date_server), sdfServer.format(new Date()));

            }
            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "").equalsIgnoreCase("")) {
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date), sdf.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date), sdf.format(new Date()));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(date));
                InGaugeSession.write(mContext.getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(new Date()));

            }
            tvSelectDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date), "") + " To " + InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date), ""));
            if (InGaugeSession.read(mContext.getResources().getString(R.string.key_is_compare_on), false)) {
                tvSelectPreviousDate.setVisibility(View.VISIBLE);
                tvSelectPreviousDate.setText(InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_start_date), "") + " To " + InGaugeSession.read(mContext.getResources().getString(R.string.key_compare_end_date), ""));
            }
            selectedDashboardId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_id), -1);
            if (!(selectedDashboardId == -1)) {
                GetDashboard(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), true, false);
                //getDashboardGraphList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
            } else {
                tvNoData.setVisibility(View.VISIBLE);
                //tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_dashboard_nodata)));
                tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
            }

            //if (mDashboardID != null && mDashboardID.size() > 0) {
            //  DashboardDetailsListAdapter mFeedsListAdapter = new DashboardDetailsListAdapter(InGaugeApp.getInstance(), mDashboardID, mHomeActivity);
            //rvDashboardList.setAdapter(mFeedsListAdapter);
            //} else {
            GetDashboard(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), true, false);

            //}

            //Refreshing Dashboard Fragment if any dashboard changed from slide menu
            ((HomeActivity) getActivity()).setFragmentRefreshListener(new HomeActivity.DashboardRefreshListener() {
                @Override
                public void onRefresh() {

                    Logger.Error("<<< Click >>>>");
                    selectedDashboardId = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_dashboard_id), -1);
                    if (!(selectedDashboardId == -1)) {


                        //getDashboardGraphList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
                        Logger.Error("<<<< Call  getDashboardCustomResport     1111>>>>");
                        getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
                    } else {

                        rlProgress.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        //tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_dashboard_nodata)));
                        tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));

                    }

                    // Refresh Your Fragment
                }
            });
        } else {
            Logger.Error("<<<< ELSE   View Get Parent >>>>>");
            if (mHomeActivity.isDashboardUpdate()) {
                mHomeActivity.setDashboardUpdate(false);
                Logger.Error("<<<< Call  getDashboardCustomResport     2222>>>>");
                getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
            }
            if (view != null && view.getParent() != null) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    void GetDashboard(String authToken, final boolean isFirst, final boolean isFromNavigation) {
        Call mCall = apiInterface.getDashboardList(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0), InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0), InGaugeSession.read(mContext.getResources().getString(R.string.key_user_role_id), 0), false);
        //Call mCall = apiInterface.getDashboardList("application/json", "bearer" + " " + authToken, "1", "1", 1, 646, false);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null) {
                    DashboardModelList madDashboardModelList = (DashboardModelList) response.body();
                    //tvSubTitle.setVisibility(View.GONE);
                    if (madDashboardModelList != null) {
                        mHomeActivity.list = madDashboardModelList.getData();

                        try {
                            if (mHomeActivity.list != null && mHomeActivity.list.size() > 0) {
                                mHomeActivity.setDashboardList(mHomeActivity.list, isFirst);
                                if (!isFromNavigation) {
                                    mHomeActivity.tvNoDashboard.setVisibility(View.GONE);
                                    if (mHomeActivity.list.size() > 0) {
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard), mHomeActivity.list.get(0).getTitle());
                                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_id), mHomeActivity.list.get(0).getId());
                                        selectedDashboardId = mHomeActivity.list.get(0).getId();
                                        dashboardName = mHomeActivity.list.get(0).getTitle();
                                    } else {
                                        mHomeActivity.tvNoDashboard.setVisibility(View.VISIBLE);
                                    }

                                    mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                                    mHomeActivity.tvSubTitle.setText(dashboardName + "-" + InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
                                }
                                Logger.Error("<<<< Call  getDashboardCustomResport     333333>>>>");
                                getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
                            } else {
                                mHomeActivity.list.clear();
                                // tvSubTitle.setVisibility(View.GONE);
                                mHomeActivity.tvNoDashboard.setVisibility(View.VISIBLE);
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard), "");
                                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard_id), -1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                        //getDashboardGraphList(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);

                        String hotelMetricType = InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), "");
                        if (hotelMetricType.length() > 0) {
                            Logger.Error("<<<< Call  getDashboardCustomResport     44444>>>>");
                            getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
                        }
                        mHomeActivity.isdashboardLoaded = true;
                    } else {
                    }
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.endProgress();
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
            }
        });
    }

    void refreshItems() {
        //DashboardDetailsListAdapter mFeedsListAdapter = new DashboardDetailsListAdapter(InGaugeApp.getInstance(), mDashboardID, mHomeActivity);
        //rvDashboardList.setAdapter(mFeedsListAdapter);
        mSwipeRefreshLayout.setRefreshing(false);
    }


    private String setFormattedDate(String dateToFormate) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat displayFormat = new SimpleDateFormat("MMM dd ,yy"); //March 01 '17
        try {
            Date date = serverFormat.parse(dateToFormate);
            return displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.ibMenu.setVisibility(View.VISIBLE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.VISIBLE);
        mHomeActivity.tvTitle.setText(mContext.getResources().getString(R.string.menu_dashboard));
        // mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_title_dashboard)));
        mHomeActivity.toolbar.setClickable(true);
        mHomeActivity.ibMenu.setImageResource(R.drawable.ic_menu);
        mHomeActivity.tvTitle.setVisibility(View.VISIBLE);
        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        if (mHomeActivity.list != null && mHomeActivity.list.size() > 0) {
            mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
            if (InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_dashboard), "").equalsIgnoreCase("")) {
                dashboardName = mHomeActivity.list.get(0).getTitle();

                InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_dashboard), mHomeActivity.list.get(0).getTitle());
            } else {

                dashboardName = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_dashboard), "");

            }
        } else {
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }

        mHomeActivity.tvSubTitle.setText(dashboardName);
        if (mHomeActivity.list != null) {
            if (mHomeActivity.list.size() == 0) {
                mHomeActivity.tvSubTitle.setVisibility(View.GONE);
            } else {
                mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
            }
        } else {
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }

        mHomeActivity.ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHomeActivity.OnMenuClick();
            }
        });
        if (InGaugeSession.read(mContext.getResources().getString(R.string.key_is_need_filter_call), false)) {

            Logger.Error("<Calllll>>>>");
            getRegionTypes(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
        } else {
            mHomeActivity.tvSubTitle.setText(dashboardName + "-" + InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));
            Logger.Error("<Calllll  ELSE   >>>>");
            if (mHomeActivity.mRegionTypeDataModel == null) {
                for (int i = 0; i < 6; i++) {
                    switch (i) {
                        case 0:
                            Gson gson = new Gson();
                            String json = InGaugeSession.read(mContext.getResources().getString(R.string.key_region_type_obj), null);
                            mHomeActivity.mRegionTypeDataModel = gson.fromJson(json, RegionTypeDataModel.class);
                            break;
                        case 1:
                            gson = new Gson();
                            json = InGaugeSession.read(mContext.getResources().getString(R.string.key_region_obj), null);
                            mHomeActivity.mRegionByTenantRegionTypeModel = gson.fromJson(json, RegionByTenantRegionTypeModel.class);
                            break;
                        case 2:
                            gson = new Gson();
                            json = InGaugeSession.read(mContext.getResources().getString(R.string.key_location_obj), null);
                            mHomeActivity.mAccessibleTenantLocationDataModel = gson.fromJson(json, AccessibleTenantLocationDataModel.class);
                            break;
                        case 3:
                            gson = new Gson();
                            json = InGaugeSession.read(mContext.getResources().getString(R.string.key_location_group_obj), null);
                            mHomeActivity.mDashboardLocationGroupListModel = gson.fromJson(json, DashboardLocationGroupListModel.class);
                            break;
                        case 4:
                            gson = new Gson();
                            json = InGaugeSession.read(mContext.getResources().getString(R.string.key_product_obj), null);
                            mHomeActivity.mProductListFromLocationGroupModel = gson.fromJson(json, ProductListFromLocationGroupModel.class);
                            break;
                        case 5:
                            gson = new Gson();
                            json = InGaugeSession.read(mContext.getResources().getString(R.string.key_user_obj), null);
                            mHomeActivity.mDashboardUserListModel = gson.fromJson(json, DashboardUserListModel.class);
                            break;

                    }
                }
            } else {
//Nothing to call
            }
        }
        mHomeActivity.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imgFilter.getVisibility() == View.VISIBLE) {
                    FilterListFragment mFilterListFragment = new FilterListFragment();
                    Bundle mBundle = new Bundle();
                    mBundle.putParcelableArrayList(FilterListFragment.KEY_FILTER_SELECTION, mFilterBaseDatas);
                    mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_ONE, mHomeActivity.mRegionTypeDataModel);
                    mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_TWO, mHomeActivity.mRegionByTenantRegionTypeModel);
                    mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_THREE, mHomeActivity.mAccessibleTenantLocationDataModel);
                    mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_FOUR, mHomeActivity.mDashboardLocationGroupListModel);
                    mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_FIVE, mHomeActivity.mProductListFromLocationGroupModel);
                    mBundle.putSerializable(FilterListFragment.KEY_FILTER_DETAILS_SIX, mHomeActivity.mDashboardUserListModel);
                    mFilterListFragment.setArguments(mBundle);
                    mHomeActivity.replace(mFilterListFragment, mContext.getResources().getString(R.string.tag_filter_list));
                }

            }
        });
        Logger.Error("<< Call >>>");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_filter:
                FragmentFilterDynamicList mFragmentFilterDynamicList = new FragmentFilterDynamicList();

                mHomeActivity.replace(mFragmentFilterDynamicList, mContext.getResources().getString(R.string.tag_filter_list));
                break;
            case R.id.tv_fromDate:
                ShowDatePicker(tvFromDate);
                break;
            case R.id.tv_toDate:
                ShowDatePicker(tvToDate);
                break;

            case R.id.relative_date_compare:
                DateFilterFragment fragment = new DateFilterFragment();
                mHomeActivity.replace(fragment, "");
                break;
        }
    }

    private void ShowDatePicker(final TextView tvDate) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if (tvDate == tvFromDate) {
                            fromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        } else {
                            toDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }

                        tvDate.setText(setFormattedDate(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    /**
     * @param authToken
     */
    void getRegionTypes(final String authToken) {
        pbFilter.setVisibility(View.VISIBLE);
        imgFilter.setVisibility(View.GONE);
        mCall = apiInterface.getRegionTypes(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0));
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mHomeActivity.mRegionTypeDataModel = (RegionTypeDataModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mRegionTypeDataModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_region_type_obj), json);
                    //Here first to get the first index of data
                    if ((mHomeActivity.mRegionTypeDataModel != null) && (mHomeActivity.mRegionTypeDataModel.getData() != null && mHomeActivity.mRegionTypeDataModel.getData().size() > 0)) {
                        if ((mHomeActivity.mRegionTypeDataModel != null) && (mHomeActivity.mRegionTypeDataModel.getData() != null && mHomeActivity.mRegionTypeDataModel.getData().size() > 0)) {
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mHomeActivity.mRegionTypeDataModel.getData().get(0).getId());
                            mFilterBaseData.name = mHomeActivity.mRegionTypeDataModel.getData().get(0).getName();
                            mFilterBaseData.filterIndex = 0;
                            mFilterBaseDatas.add(mFilterBaseData);
                            //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), mHomeActivity.mRegionTypeDataModel.getData().get(0).getId());
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype), mHomeActivity.mRegionTypeDataModel.getData().get(0).getId());
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_regiontype_name), mHomeActivity.mRegionTypeDataModel.getData().get(0).getName());
                            //getRegionByTenantRegionTypeByCountry(authToken, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0)), String.valueOf(mRegionTypeDataModel.getData().get(0).getId()));
                            getRegionByTenantRegionTypeByCountry(authToken, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 1)), String.valueOf(mHomeActivity.mRegionTypeDataModel.getData().get(0).getId()));
                        } else {
                            pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);
                            InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                        }

                    } else {
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    }
                    Logger.Error("<<<< Call  getDashboardCustomResport     5555>>>>");
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
            }
        });

    }


    void getDashboardCustomResport(final String authToken, final int dashboardId) {
      /*  rvDashboardList.setVisibility(View.GONE);
        rlProgress.setVisibility(View.VISIBLE);
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = InGaugeSession.read(mContext.getString(R.string.key_selected_regiontype), -2);
        int RegionId = InGaugeSession.read(mContext.getString(R.string.key_selected_region), -2);
        int ProductId = (InGaugeSession.read(mContext.getString(R.string.key_selected_location_group_product), -2));
        int UserId = (InGaugeSession.read(mContext.getString(R.string.key_selected_user), -2));
        int TenantLocationId = (InGaugeSession.read(mContext.getString(R.string.key_tenant_location_id), -2));
        int LocationGroupId = InGaugeSession.read(mContext.getString(R.string.key_selected_location_group), -2);
        String MatrixDataType = InGaugeSession.read(mContext.getResources().getString(R.string.key_matric_data_type), mContext.getResources().getStringArray(R.array.metric_type_array)[0]);
        String FromDate = InGaugeSession.read(mContext.getString(R.string.key_start_date_server), "");
        String ToDate = InGaugeSession.read(mContext.getString(R.string.key_end_date_server), "");
        String CompFromDate = InGaugeSession.read(mContext.getString(R.string.key_compare_start_date_server), "");
        String CompToDate = InGaugeSession.read(mContext.getString(R.string.key_compare_end_date_server), "");
        boolean IsCompare = InGaugeSession.read(mContext.getString(R.string.key_is_compare_on), false);
       *//* mCall = apiInterface.getDashboardCustomReports(
                dashboardId,
                TenantId,
                RegionTypeId,
                RegionId,
                ProductId,
                UserId,
                TenantLocationId,
                LocationGroupId,
                MatrixDataType,
                FromDate,
                ToDate,
                CompFromDate,
                CompToDate,
                IsCompare);*//*
        mCall.enqueue(new Callback<ResponseBody>() {
            CustomDashboardModel.Data mCustomDashboardModel;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                rvDashboardList.setVisibility(View.VISIBLE);
                rlProgress.setVisibility(View.GONE);
                // mDashboardID = new ArrayList<String>();
                if (response.body() != null) {
                    //dashboardGraphList = (DashboardGraphList) response.body();
                    //ResponseBody responseBody = response.toString();
                    try {
                        JSONObject responseJson = new JSONObject(response.body().string());
                        JSONObject mdataJsonObject = responseJson.optJSONObject("data");
                        JSONArray mJsonArrayFilters = mdataJsonObject.optJSONArray("filters");
                        mFilterListString.clear();
                        if (mJsonArrayFilters != null && mJsonArrayFilters.length() > 0) {
                            for (int i = 0; i < mJsonArrayFilters.length(); i++) {
                                mFilterListString.add(mJsonArrayFilters.optString(i));
                            }
                        }
                        Gson gson = new Gson();
                        mCustomDashboardModel = gson.fromJson(mdataJsonObject.toString(), CustomDashboardModel.Data.class);
                        mCustomReportList =mCustomDashboardModel.getCustomReport();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Logger.Error("Exceptiopn " + e.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Logger.Error("Exceptiopn " + e.toString());
                    }


                    if (mCustomReportList.size() > 0) {
                        tvNoData.setVisibility(View.GONE);
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                        mCustomReportList = mCustomDashboardModel.getCustomReport();
                        setDashboardCustomReportAdapter(mCustomReportList);
                        //tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_dashboard_nodata)));
                        tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                    } else {
                        tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                        tvNoData.setVisibility(View.VISIBLE);
                        rvDashboardList.setVisibility(View.GONE);
                        rlProgress.setVisibility(View.GONE);
                    }
                    rlProgress.setVisibility(View.GONE);
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                rlProgress.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
                rvDashboardList.setVisibility(View.GONE);
                tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                Logger.Error("Exception :" + t.getMessage());
                //getRegionTypes(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
            }
        });*/

    }


    private void setDashboardCustomReportAdapter(List<CustomDashboardModel.CustomReport> mCustomReportList) {
        DashboardPanelAdapter mDashboardPanelAdapter = new DashboardPanelAdapter(mHomeActivity, mCustomReportList, this);
        rvDashboardList.setAdapter(mDashboardPanelAdapter);
    }

    /**
     * @param authToken
     */
    void getDashboardGraphList(final String authToken, final int dashboardId) {

        mCall = apiInterface.getDashboardGraph(dashboardId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                rvDashboardList.setVisibility(View.VISIBLE);
                rlProgress.setVisibility(View.GONE);
                // mDashboardID = new ArrayList<String>();
                if (response.body() != null) {
                    dashboardGraphList = (DashboardGraphList) response.body();
                    for (int i = 0; i < dashboardGraphList.getData().getDashboardSections().size(); i++) {
                        for (int j = 0; j < dashboardGraphList.getData().getDashboardSections().get(i).getDashboardSectionSegments().size(); j++) {
                            for (int k = 0; k < dashboardGraphList.getData().getDashboardSections().get(i).getDashboardSectionSegments().get(j).getCustomReports().size(); k++) {
                                DashboardGraphList.CustomReport mCustomReport = dashboardGraphList.getData().getDashboardSections().get(i).getDashboardSectionSegments().get(j).getCustomReports().get(k);
                                Logger.Error("Custom Report ID  " + mCustomReport.getId() + "\t" + "Report Name  " + mCustomReport.getReportName());
                                //mDashboardID.add("" + mCustomReport.getId());

                            }
                        }
                    }


                    //DashboardDetailsListAdapter mFeedsListAdapter = new DashboardDetailsListAdapter(InGaugeApp.getInstance(), mDashboardID, mHomeActivity);
                    //rvDashboardList.setAdapter(mFeedsListAdapter);

                  /*  if (mDashboardID.size() == 0) {
                        tvNoData.setVisibility(View.VISIBLE);
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                        //tvNoData.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_dashboard_nodata)));
                        tvNoData.setText(mContext.getResources().getString(R.string.tv_no_record));
                    } else {
                        tvNoData.setVisibility(View.GONE);
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                    }*/
                    rlProgress.setVisibility(View.GONE);
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                rlProgress.setVisibility(View.GONE);
                //getRegionTypes(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""));
            }
        });
    }


    /**
     * @param authToken
     * @param tenantId
     * @param regionTypeId
     */
    void getRegionByTenantRegionTypeByCountry(final String authToken, final String tenantId, String regionTypeId) {

        mCall = apiInterface.getRegionByTenantRegionType(Integer.parseInt(tenantId),
                Integer.parseInt(regionTypeId),
                false);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mHomeActivity.mRegionByTenantRegionTypeModel = (RegionByTenantRegionTypeModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mRegionByTenantRegionTypeModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_region_obj), json);
                    if ((mHomeActivity.mRegionByTenantRegionTypeModel != null) && (mHomeActivity.mRegionByTenantRegionTypeModel.getData() != null && mHomeActivity.mRegionByTenantRegionTypeModel.getData().size() > 0)) {

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 1;
                        mFilterBaseDatas.add(mFilterBaseData);
                    /*getAccessibleTenantLocation(authToken, String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_selected_tenant_id), 0)),
                            String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", String.valueOf(mRegionByTenantRegionTypeModel.getData().get(0).getId()), "Normal");*/
                        //InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), (mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(0).getId()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region), mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(0).getId());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_region_name), mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(0).getName());
                        getAccessibleTenantLocation(authToken, Integer.parseInt(tenantId),
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)), "name", "ASC", String.valueOf(mHomeActivity.mRegionByTenantRegionTypeModel.getData().get(0).getId()), "Normal");
                    } else {
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    }
                    Logger.Error("<<<< Call  getDashboardCustomResport     66666>>>>");
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
            }
        });
    }

    /**
     * @param authToken
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param regionId
     * @param activeStatus
     */
    void getAccessibleTenantLocation(final String authToken, int tenantId, String userId, String orderBy, String sort, String regionId, String activeStatus) {

        mCall = apiInterface.getAccessibleTenantLocation(tenantId, userId, orderBy, sort, regionId, activeStatus,true);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mHomeActivity.mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_location_obj), json);
                    if ((mHomeActivity.mAccessibleTenantLocationDataModel != null && (mHomeActivity.mAccessibleTenantLocationDataModel.getData() != null && mHomeActivity.mAccessibleTenantLocationDataModel.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 2;
                        mFilterBaseDatas.add(mFilterBaseData);

                        //String authToken,String tenantlocationId,String userId
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_id), mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_tenant_location_name), mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getName());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_hotel_metric_type_name), mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_matric_data_type), mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType());
                        Logger.Error("<<<< Metric TYpe >>>> " + mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getHotelMetricsDataType());
                        mHomeActivity.tvSubTitle.setText(dashboardName + "-" + InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_name), ""));

                        getDashboardLocationGroupList(authToken, String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId()), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), 0)));
                    } else {
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    }
                    Logger.Error("<<<< Call  getDashboardCustomResport     77777>>>>");
                    getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupList(final String authToken, String tenantlocationId, String userId) {

        mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mHomeActivity.mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_location_group_obj), json);
                    //String authToken,String locationGroupId
                    if ((mHomeActivity.mDashboardLocationGroupListModel != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModel.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID());
                        mFilterBaseData.name = mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName();
                        mFilterBaseData.filterIndex = 3;
                        mFilterBaseDatas.add(mFilterBaseData);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group), (mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_name), mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName());
                        getProductList(authToken, String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                    } else {
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    }
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {

                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductList(final String authToken, final String locationGroupId) {

        mCall = apiInterface.getProductList(locationGroupId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.body() != null) {
                    mHomeActivity.mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_product_obj), json);
                    if ((mHomeActivity.mProductListFromLocationGroupModel != null && (mHomeActivity.mProductListFromLocationGroupModel.getData() != null && mHomeActivity.mProductListFromLocationGroupModel.getData().size() > 0))) {
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModel.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 4;
                        mFilterBaseDatas.add(mFilterBaseData);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product), mHomeActivity.mProductListFromLocationGroupModel.getData().get(0).getId());
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_location_group_product_name), mHomeActivity.mProductListFromLocationGroupModel.getData().get(0).getName());
                        //String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId
                        getDashboardUserList(authToken,
                                String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_tenant_location_id),
                                        0)), InGaugeSession.read(mContext.getResources().getString(R.string.key_start_date_server), ""), InGaugeSession.read(mContext.getResources().getString(R.string.key_end_date_server), ""), locationGroupId);
                    } else {
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();
                    }
                } else {
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
            }
        });
    }


    /**
     * @param authToken
     * @param tenantLocationId
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserList(String authToken, String tenantLocationId, String startDate, String endDate, String locationGroupId) {
        int TenantId = InGaugeSession.read(mContext.getString(R.string.key_selected_tenant_id), -2);

        mCall = apiInterface.getDashboardUserList(TenantId, startDate, endDate, tenantLocationId,locationGroupId, true,false,false);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                if (response.body() != null) {
                    mHomeActivity.mDashboardUserListModel = (DashboardUserListModel) response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardUserListModel);
                    InGaugeSession.write(mContext.getResources().getString(R.string.key_user_obj), json);
                    if ((mHomeActivity.mDashboardUserListModel != null && (mHomeActivity.mDashboardUserListModel.getData() != null && mHomeActivity.mDashboardUserListModel.getData().size() > 0))) {

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mDashboardUserListModel.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mDashboardUserListModel.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 5;
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user), (mHomeActivity.mDashboardUserListModel.getData().get(0).getId()));
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_selected_user_name), mHomeActivity.mDashboardUserListModel.getData().get(0).getName());
                        mFilterBaseDatas.add(mFilterBaseData);
                        Logger.Error("<<<< Call  getDashboardCustomResport     8888>>>>");
                        getDashboardCustomResport(InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId);
                    } else {
                        pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);
                    }

                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        mHomeActivity.ForceLogout();

                    }
                } else {
                    pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mContext.getResources().getString(R.string.key_is_need_filter_call), false);
            }
        });
    }


    private void setMetricTypeList() {
        FilterBaseData mFilterBaseData = new FilterBaseData();
        mFilterBaseData.id = "" + 000000;
        mFilterBaseData.name = mHomeActivity.mStringListMetricType.get(0);
        mFilterBaseData.filterIndex = 6;
        InGaugeSession.write(mContext.getResources().getString(R.string.key_matric_data_type), mHomeActivity.mStringListMetricType.get(0));
        mFilterBaseDatas.add(mFilterBaseData);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCall != null)
            mCall.cancel();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

        DashboardDetailFragment mDashboardDetailFragment = new DashboardDetailFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(DashboardDetailFragment.KEY_REPORT_ID, String.valueOf(mCustomReportList.get(position).getId()));
        mBundle.putString(DashboardDetailFragment.KEY_REPORT_Name, mCustomReportList.get(position).getReportName());
        mBundle.putString(DashboardDetailFragment.KEY_REPORT_TYPE, mCustomReportList.get(position).getReportType());
        mDashboardDetailFragment.setArguments(mBundle);
        mHomeActivity.replace(mDashboardDetailFragment, mContext.getResources().getString(R.string.tag_dashboard_detail));
    }




}
