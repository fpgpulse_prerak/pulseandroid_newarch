package com.ingauge.fragments.leaderboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragmentObserver.FragmentObserver;
import com.ingauge.fragments.BaseFragment;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.CustomDashboardModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.DynamicData;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.temp.FragmentOne;
import com.ingauge.utils.DateTimeUtils;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 15-Jan-18.
 */

public class FragmentLeaderBoard extends BaseFragment implements View.OnClickListener{


    private HomeActivity mHomeActivity;
    private View rootView;
    private ViewPager mViewpager;
    private TabLayout mTabLayout;
    LeaderBoardPagerAdapter mLeaderBoardPagerAdapter;
    private TextView tvSelectDate;
    private TextView tvSelectPreviousDate;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    Date date = null;
    private RelativeLayout rlDateCompara;
    private String entityType = "";
    private List<String> mFilterListString = new ArrayList<>();

    Call<ResponseBody> mCall;
    APIInterface apiInterface;

    FragmentEntityList mFragmentEntityList = new FragmentEntityList();
    JSONObject mDataJson = null;
    List<DynamicData> mCountryList;
    public Map<Integer, List<DynamicData>> integerListHashMap = new LinkedHashMap<>();

    private String selectedRegionId = "";
    private String selectedRegionName = "";
    private String selectedRegionTypeId = "";
    private String selectedRegionTypeName = "";
    public boolean isCheckDone = false;
    ArrayList<FilterBaseData> mFilterBaseDatas;

    SimpleDateFormat sdfServerDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    private int selectedDashboardId = -2;
    private List<CustomDashboardModel.CustomReport> mCustomReportList = new ArrayList<>();

    //    public RelativeLayout rlCustomProgressbar;
    public boolean isAPICallDone = false;
    private ProgressBar rlCustomProgressbar;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        mCountryList = new ArrayList<>();
        mFilterBaseDatas = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        date = calendar.getTime();

        if(mHomeActivity.isTenantLocation()){
            entityType = "TenantLocation";
        } else{
            entityType = "Region";
        }
       /* mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_title)));
        mHomeActivity.tvTitle.setText("Leaderboard");

        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.toolbar.setClickable(true);
        //mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.tvPreferences.setTag("Back");*/

    }

    @Override
    public void onResume(){
        super.onResume();


        mHomeActivity.tvApply.setVisibility(View.GONE);
        mHomeActivity.ibMenu.setVisibility(View.GONE);
        mHomeActivity.nBottomNavigationView.setVisibility(View.GONE);
        //mHomeActivity.tvTitle.setText(mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_preference_title)));
        setToolbarTitles();

        mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        mHomeActivity.tvSubTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        mHomeActivity.toolbar.setClickable(true);
        mHomeActivity.tvPreferences.setVisibility(View.GONE);
        mHomeActivity.tvPreferences.setTag("Back");

        mHomeActivity.toolbar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(isAPICallDone){
                    LbFragmentFilterDynamicList mFragmentFilterDynamicList = new LbFragmentFilterDynamicList();
                    Bundle mBundle = new Bundle();
                    mFragmentFilterDynamicList.setArguments(mBundle);
                    mHomeActivity.replace(mFragmentFilterDynamicList, mHomeActivity.getResources().getString(R.string.tag_filter_list_leader_board));
                }


            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_leader_board, container, false);
            initView(rootView);
            getDashboardFilters();
            if(!InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date), "").equalsIgnoreCase("")){
                String dashboardStartDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date), "");
                String dashboardEndDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date), "");
                String dashboardServerStartDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_server), "");
                String dashboardServerEndDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server), "");

                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), dashboardStartDate);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), dashboardServerStartDate);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), dashboardEndDate);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), dashboardServerEndDate);
            }
            if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), "").equalsIgnoreCase("")){
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), sdf.format(date));
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), sdfServer.format(date));
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), sdf.format(new Date()));
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), sdfServer.format(new Date()));

            }

            if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), "")!=null && InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), "").length()>0){
                tvSelectDate.setText(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), "") + " To " + InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), ""));
            }else{
                tvSelectDate.setText(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), "") );
            }


            mLeaderBoardPagerAdapter = new LeaderBoardPagerAdapter(getFragmentManager());

            if(mHomeActivity.isTenantLocation()){
                mLeaderBoardPagerAdapter.addFragment(mFragmentEntityList, "Location");
            } else{
                mLeaderBoardPagerAdapter.addFragment(mFragmentEntityList, "Region");
            }
            mViewpager.setAdapter(mLeaderBoardPagerAdapter);
            mTabLayout.setupWithViewPager(mViewpager);
//        mLeaderBoardPagerAdapter.addFragment(mFragmentOne, "All Videos");

        } else{
            Logger.Error("<<<< Calll  >>>>> ");

            if(mHomeActivity.isLeaderNeedUpdate()){
                mHomeActivity.setLeaderNeedUpdate(false);
                if(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), "")!=null && InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), "").length()>0){
                    tvSelectDate.setText(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), "") + " To " + InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_for_lb), ""));
                }else{
                    tvSelectDate.setText(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_for_lb), "") );
                }


                rlCustomProgressbar.setVisibility(View.VISIBLE);
                mFragmentEntityList.mRecyclerView.setVisibility(View.GONE);
                getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
            }
            if(rootView != null && rootView.getParent() != null){
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }
        }
        return rootView;
    }

    public void initView(View itemView){
        mTabLayout = (TabLayout) itemView.findViewById(R.id.fragment_leader_board_tabs);
        mViewpager = (ViewPager) itemView.findViewById(R.id.fragment_leader_board_viewpager);
        tvSelectDate = (TextView) itemView.findViewById(R.id.tv_selectdate);
        tvSelectPreviousDate = (TextView) itemView.findViewById(R.id.tv_previousdate);
        rlDateCompara = (RelativeLayout) itemView.findViewById(R.id.rl_date_control);
        rlCustomProgressbar = (ProgressBar) rootView.findViewById(R.id.fragmnet_entity_pb);
        rlCustomProgressbar.setVisibility(View.VISIBLE);
        rlDateCompara.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){

        switch(v.getId()){
            case R.id.rl_date_control:
/*
                DateFilterFragment mDateFilterFragment = new DateFilterFragment();
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(DateFilterFragment.KEY_IS_FROM_LEADERBOARD, true);
                mDateFilterFragment.setArguments(mBundle);
                mHomeActivity.replace(mDateFilterFragment, mHomeActivity.getResources().getString(R.string.tag_fragment_dashboard_date_filter_for_leaderboard));
*/

                DateSelectionFragment fragment = new DateSelectionFragment();
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(DateSelectionFragment.KEY_IS_FROM_LEADERBOARD, true);
                mBundle.putBoolean(DateSelectionFragment.KEY_IS_HIDE_COMPARE, true);

                fragment.setArguments(mBundle);
                mHomeActivity.replace(fragment, mHomeActivity.getResources().getString(R.string.tag_fragment_dashboard_date_filter_for_leaderboard));
                break;
        }
    }

    // Adapter for the viewpager using FragmentPagerAdapter
    class LeaderBoardPagerAdapter extends FragmentStatePagerAdapter{
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public LeaderBoardPagerAdapter(FragmentManager manager){
            super(manager);
        }

        private Observable mObservers = new FragmentObserver();

        @Override
        public Fragment getItem(int position){
            /*mObservers.deleteObservers(); // Clear existing observers.
            Fragment fragment = new FragmentOne();
            if(fragment instanceof Observer)
                mObservers.addObserver((Observer) fragment);*/
            return mFragmentList.get(position);
        }

        @Override
        public int getCount(){
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title){

            if(fragment instanceof FragmentOne){
                mObservers.deleteObservers(); // Clear existing observers.

                //if(fragment instanceof Observer)
                mObservers.addObserver((Observer) fragment);
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            } else{
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            }

        }

        @Override
        public CharSequence getPageTitle(int position){
            return mFragmentTitleList.get(position);
        }

        public void updateFragments(){
            mObservers.notifyObservers();
        }
    }


    void getDashboardFilters(){
        mCall = apiInterface.getDashboardListByUniqueCode("LDR",
                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -2), entityType);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                try{
                    mHomeActivity.setRegionFilterForLb(false);
                    mHomeActivity.setTenantLocationForLb(false);
                    mHomeActivity.setCustomGoalFilterForLb(false);
                    mHomeActivity.setProductForLb(false);
                    mHomeActivity.setLocationGroupForLb(false);
                    mHomeActivity.setUserForLb(false);

                    String jsonResponse = response.body().string();
                    Logger.Error("<<< Response  >>>>> " + jsonResponse);
                    JSONObject mJsonObject = new JSONObject(jsonResponse);
                    int statusCode = mJsonObject.optInt("statusCode");
                    JSONArray mDataJsonArray = mJsonObject.optJSONArray("data");
                    if(statusCode == 200){
                        if(mDataJsonArray != null && mDataJsonArray.length() > 0){
                            selectedDashboardId = mDataJsonArray.optJSONObject(0).optInt("id");
                            getDashboardFiltersFromAPI(selectedDashboardId);
                        }else{
                            rlCustomProgressbar.setVisibility(View.GONE);
                            mFragmentEntityList.setEntityListAdapter(new ArrayList<CustomDashboardModel.CustomReport>());
                        }
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){

            }
        });
    }


    private void checkFilterFromCustomreport(String filterTypeName){

        switch(filterTypeName){
            case "Region":
                mHomeActivity.setRegionFilterForLb(true);
                break;
            case "TenantLocation":
                mHomeActivity.setTenantLocationForLb(true);
                break;
            case "CustomGoalFilter":
                mHomeActivity.setCustomGoalFilterForLb(true);
                break;
            case "Product":
                mHomeActivity.setProductForLb(true);
                break;
            case "LocationGroup":
                mHomeActivity.setLocationGroupForLb(true);
                break;
            case "User":
                mHomeActivity.setUserForLb(true);
                break;

        }

    }

    public void filterCallAfterCheck(){
        /*Gson gson = new Gson();
        String json = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_preference_filter_obj), null);
        mHomeActivity.mPrerferencedLeftFilterBaseDatas = gson.fromJson(json, new TypeToken<ArrayList<FilterBaseData>>(){}.getType());*/
        if(!mHomeActivity.isRegionFilterForLb()){
            if(mHomeActivity.isTenantLocationForLb()){

                getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1),
                        String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                        , "name", "ASC", null,
                        "Normal");
                return;
            }
            if(mHomeActivity.isLocationGroupForLb()){


                if(!mHomeActivity.isTenantLocationForLb()){
                    mHomeActivity.setTenantLocationForLb(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                getDashboardLocationGroupList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                        null,
                        String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));
                return;
            }
            if(mHomeActivity.isProductForLb()){


                if(!mHomeActivity.isTenantLocationForLb()){
                    mHomeActivity.setTenantLocationForLb(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                if(!mHomeActivity.isLocationGroupForLb()){
                    mHomeActivity.setLocationGroupForLb(true);
                    getDashboardLocationGroupList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                getProductList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                        null);

            }
            if(mHomeActivity.isUserForLb()){


                if(!mHomeActivity.isTenantLocationForLb()){
                    mHomeActivity.setTenantLocationForLb(true);
                    getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                            String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                            , "name", "ASC", null,
                            "Normal");
                    return;
                }
                if(!mHomeActivity.isLocationGroupForLb()){
                    mHomeActivity.setLocationGroupForLb(true);
                    getDashboardLocationGroupList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                        null,
                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""), null);

            } else{
                getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                return;
            }
        } else{
            List<DynamicData> mDynamicDatas = new ArrayList<>();
            integerListHashMap = mHomeActivity.getIntegerListHashMap();
            if(integerListHashMap != null && integerListHashMap.size() > 0){
                for(int i = 0; i < integerListHashMap.size(); i++){
                    //int count = 0;
                    mDynamicDatas = new ArrayList<>();
                    mDynamicDatas = integerListHashMap.get(i);
                    if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                        if(mDynamicDatas != null && mDynamicDatas.size() > 0){
                            if(!isCheckDone){
                                setSelectedRegionTypeId(String.valueOf(mDynamicDatas.get(i).getRegionTypeId()));
                                setSelectedRegionTypeName(mDynamicDatas.get(i).getRegionTypeName());
                                setSelectedRegionId(String.valueOf(mDynamicDatas.get(i).getId()));
                                setSelectedRegionName(mDynamicDatas.get(i).getName());
                                mHomeActivity.setSelectedRegionTypeForLb(Integer.parseInt(getSelectedRegionTypeId()));
                                mHomeActivity.setSelectedRegionIdForLb(Integer.parseInt(getSelectedRegionId()));
                                mHomeActivity.setSelectedRegionNameForLb(getSelectedRegionName());
                                    /*InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                                    InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));*/
                            }
                            if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                                if(checkRegionId(getSelectedRegionTypeId(), getSelectedRegionId(), mDynamicDatas) && !isCheckDone){
                                    //break;

                                } else{
                                    if(!isCheckDone){
                                        if(i == 0){
                                            if(mDynamicDatas != null && mDynamicDatas.size() > 0){
                                                setSelectedRegionTypeId(String.valueOf(mDynamicDatas.get(0).getRegionTypeId()));
                                                setSelectedRegionTypeName(mDynamicDatas.get(0).getRegionTypeName());
                                                setSelectedRegionId(String.valueOf(mDynamicDatas.get(0).getId()));
                                                setSelectedRegionName(mDynamicDatas.get(0).getName());
                                            }
                                                /*InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                                                InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));*/
                                            if(mHomeActivity.isRegionFilter()){
                                                mHomeActivity.setSelectedRegionTypeForLb(InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId())));
                                                mHomeActivity.setSelectedRegionIdForLb(InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId())));
                                                mHomeActivity.setSelectedRegionNameForLb(InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_region_name), getSelectedRegionName()));
                                            } else{
                                                mHomeActivity.setSelectedRegionTypeForLb(Integer.parseInt(getSelectedRegionTypeId()));
                                                mHomeActivity.setSelectedRegionIdForLb(Integer.parseInt(getSelectedRegionId()));
                                                mHomeActivity.setSelectedRegionNameForLb(getSelectedRegionName());
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    } else{
                        if(i == 0){
                            if(mDynamicDatas != null && mDynamicDatas.size() > 0){
                                setSelectedRegionTypeId(String.valueOf(mDynamicDatas.get(0).getRegionTypeId()));
                                setSelectedRegionTypeName(mDynamicDatas.get(0).getRegionTypeName());
                                setSelectedRegionId(String.valueOf(mDynamicDatas.get(0).getId()));
                                setSelectedRegionName(mDynamicDatas.get(0).getName());
                            }
                                /*InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                                InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));*/
                            if(mHomeActivity.isRegionFilter()){
                                mHomeActivity.setSelectedRegionTypeForLb(InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId())));
                                mHomeActivity.setSelectedRegionIdForLb(InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId())));
                                mHomeActivity.setSelectedRegionNameForLb(InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_region_name), getSelectedRegionName()));
                            } else{
                                mHomeActivity.setSelectedRegionTypeForLb(Integer.parseInt(getSelectedRegionTypeId()));
                                mHomeActivity.setSelectedRegionIdForLb(Integer.parseInt(getSelectedRegionId()));
                                mHomeActivity.setSelectedRegionNameForLb(getSelectedRegionName());
                            }

                        }
                    }


                }
                if(!mHomeActivity.isTenantLocationForLb() && !mHomeActivity.isLocationGroupForLb() && !mHomeActivity.isProductForLb() && !mHomeActivity.isUserForLb()){
                    getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    return;
                }
                if(mHomeActivity.isTenantLocationForLb()){


                    if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
                        if(checkRegionId(getSelectedRegionTypeId(), getSelectedRegionId(), mDynamicDatas)){
                            getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1),
                                    String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                                    , "name", "ASC", getSelectedRegionId(),
                                    "Normal");
                        } else{
                            getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1),
                                    String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                                    , "name", "ASC", getSelectedRegionId(),
                                    "Normal");
                        }
                    } else{
                        getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                                , "name", "ASC", getSelectedRegionId(),
                                "Normal");
                    }


                    return;
                }
                if(mHomeActivity.isLocationGroupForLb()){


                    getDashboardLocationGroupList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));
                    return;
                }
                if(mHomeActivity.isProductForLb()){


                    if(!mHomeActivity.isTenantLocationForLb()){
                        mHomeActivity.setTenantLocationForLb(true);
                        getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                                , "name", "ASC", null,
                                "Normal");
                        return;
                    }
                    if(!mHomeActivity.isLocationGroupForLb()){
                        mHomeActivity.setLocationGroupForLb(true);
                        getDashboardLocationGroupList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                null,
                                String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));
                        return;
                    }
                    getProductList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            null);
                    return;
                }
                if(mHomeActivity.isUserForLb()){


                    if(!mHomeActivity.isTenantLocationForLb()){
                        mHomeActivity.setTenantLocationForLb(true);
                        getAccessibleTenantLocation(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), InGaugeSession.read(getString(R.string.key_selected_tenant_id), -1),
                                String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0))
                                , "name", "ASC", null,
                                "Normal");
                        return;
                    }
                    if(!mHomeActivity.isLocationGroupForLb()){
                        mHomeActivity.setLocationGroupForLb(true);
                        getDashboardLocationGroupList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                null,
                                String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));
                        return;
                    }
                    getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                            null,
                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""), null);

                } else{
                    getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    return;
                }
            } else{
                getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                return;
            }

        }
    }


    void getDynamicFilterMap(final int tenantId, int userID, boolean isShowDeactivatedLocation){

        Call<ResponseBody> mCall = apiInterface.getDynamicFiltersListMap(tenantId, userID, isShowDeactivatedLocation);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response){


                JSONObject data = null;
                if(response.body() != null){
                    try{

                        JSONObject mJsonObject = new JSONObject(response.body().string());
                        mDataJson = mJsonObject.optJSONObject("data");
                        JSONArray firstJson = mDataJson.optJSONArray("-1");
                        //Map<Integer,JSONArray> integerJSONArrayMap  = new HashMap<>();

                        if(firstJson != null && firstJson.length() > 0){
                            Map<Integer, List<DynamicData>> integerListHashMap = new LinkedHashMap<>();
                            int level = 0;
                            for(int i = 0; i < firstJson.length(); i++){
                                DynamicData mDynamicData = new DynamicData();
                                mDynamicData.setId(firstJson.getJSONObject(i).optInt("id"));
                                mDynamicData.setName(firstJson.getJSONObject(i).optString("name"));
                                mDynamicData.setRegionTypeName(firstJson.getJSONObject(i).optString("regionTypeName"));
                                mDynamicData.setRegionTypeId(firstJson.getJSONObject(i).optInt("regionTypeId"));
                                mDynamicData.setChildRegion(firstJson.getJSONObject(i).optString("childRegion"));
                                mDynamicData.setParentRegionId(firstJson.getJSONObject(i).optInt("parentRegionId"));
                                mCountryList.add(mDynamicData);
                            }
                            integerListHashMap.put(level, mCountryList);
                            for(DynamicData mDynamicDataParent : mCountryList){
                                if(mDynamicDataParent.getChildRegion() != null && mDynamicDataParent.getChildRegion().length() > 0)
                                    searchParent(mDynamicDataParent, integerListHashMap, level);
                            }

                            for(List<DynamicData> mDynamicData : integerListHashMap.values()){
                                Collections.sort(mDynamicData, new Comparator<DynamicData>(){
                                    @Override
                                    public int compare(DynamicData d1, DynamicData d2){
                                        return Boolean.compare(d2.isMAsterLocation(), d1.isMAsterLocation());
                                    }
                                });

                            }
                            if(mHomeActivity.getIntegerListHashMap() != null && mHomeActivity.getIntegerListHashMap().size() > 0){
                                mHomeActivity.getIntegerListHashMap().clear();
                                mHomeActivity.setIntegerListHashMap(integerListHashMap);
                            } else{
                                mHomeActivity.setIntegerListHashMap(integerListHashMap);
                            }
                            Gson gson = new Gson();
                            String json = gson.toJson(integerListHashMap);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_dynamic_map_filter_obj), json);
                            //GetDashboard(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), true, false, false);
                        } else{
                            Gson gson = new Gson();
                            String json = gson.toJson(integerListHashMap);
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_dynamic_map_filter_obj), json);
                            //GetDashboard(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), true, false, false);
                        }

                    } catch(JSONException e){
                        e.printStackTrace();
                        Logger.Error("<<< Exception  >>>>" + e.toString());
                    } catch(IOException e){
                        e.printStackTrace();
                        Logger.Error("<<< Exception  >>>>" + e.toString());
                    }


                    //getRegionTypes(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){
                t.printStackTrace();
            }
        });
    }

    /**
     * @param mDynamicDataParent
     * @param integerJSONArrayMap
     * @param level
     */
    public void searchParent(DynamicData mDynamicDataParent, Map<Integer, List<DynamicData>> integerJSONArrayMap, int level){
        try{

            if(mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId())) != null){
                level++;
                JSONArray mJsonArray = mDataJson.optJSONArray(String.valueOf(mDynamicDataParent.getId()));
                for(int i = 0; i < mJsonArray.length(); i++){
                    DynamicData mDynamicData = new DynamicData();
                    mDynamicData.setId(mJsonArray.getJSONObject(i).optInt("id"));
                    mDynamicData.setName(mJsonArray.getJSONObject(i).optString("name"));
                    mDynamicData.setRegionTypeName(mJsonArray.getJSONObject(i).optString("regionTypeName"));
                    mDynamicData.setRegionTypeId(mJsonArray.getJSONObject(i).optInt("regionTypeId"));
                    mDynamicData.setChildRegion(mJsonArray.getJSONObject(i).optString("childRegion"));
                    mDynamicData.setParentRegionId(mJsonArray.getJSONObject(i).optInt("parentRegionId"));

                    if(integerJSONArrayMap.get(level) != null){
                        List<DynamicData> mDynamicDataList = integerJSONArrayMap.get(level);
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level, mDynamicDataList);

                    } else{
                        List<DynamicData> mDynamicDataList = new ArrayList<>();
                        mDynamicDataList.add(mDynamicData);
                        integerJSONArrayMap.put(level, mDynamicDataList);
                    }
                    searchParent(mDynamicData, integerJSONArrayMap, level);
                    //mCountryList.add(mDynamicData);
                }
                return;
            }
        } catch(JSONException e){
            e.printStackTrace();
        }


    }

    public String getSelectedRegionId(){
        return selectedRegionId;
    }

    public void setSelectedRegionId(String selectedRegionId){
        this.selectedRegionId = selectedRegionId;
    }

    public String getSelectedRegionName(){
        return selectedRegionName;
    }

    public void setSelectedRegionName(String selectedRegionName){
        this.selectedRegionName = selectedRegionName;
    }

    public String getSelectedRegionTypeId(){
        return selectedRegionTypeId;
    }

    public void setSelectedRegionTypeId(String selectedRegionTypeId){
        this.selectedRegionTypeId = selectedRegionTypeId;
    }

    public String getSelectedRegionTypeName(){
        return selectedRegionTypeName;
    }

    public void setSelectedRegionTypeName(String selectedRegionTypeName){
        this.selectedRegionTypeName = selectedRegionTypeName;
    }

    public boolean checkRegionId(String regionTypeId, String regionId, List<DynamicData> mDynamicDatas){
        if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
            for(int i = 0; i < mHomeActivity.mPrerferencedLeftFilterBaseDatas.size(); i++){
                if(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).isDynamicFilter && mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).isDynamicFilterSelected){

                    if(mDynamicDatas != null && mDynamicDatas.size() > 0){
                        for(int j = 0; j < mDynamicDatas.size(); j++){
                            if(String.valueOf(mDynamicDatas.get(j).getRegionTypeId()).equalsIgnoreCase(String.valueOf(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).regionTypeId)) &&
                                    String.valueOf(mDynamicDatas.get(j).getId()).equalsIgnoreCase(String.valueOf(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).id))){
                                setSelectedRegionTypeId(String.valueOf(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).regionTypeId));
                                setSelectedRegionTypeName(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).filterName);
                                setSelectedRegionId(String.valueOf(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).id));
                                setSelectedRegionName(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).name);
                                /*InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_regiontype), Integer.parseInt(getSelectedRegionTypeId()));
                                InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_region), Integer.parseInt(getSelectedRegionId()));*/

                                mHomeActivity.setSelectedRegionTypeForLb(Integer.parseInt(getSelectedRegionTypeId()));
                                mHomeActivity.setSelectedRegionIdForLb(Integer.parseInt(getSelectedRegionId()));
                                mHomeActivity.setSelectedRegionNameForLb(getSelectedRegionName());
                                isCheckDone = true;
                                return true;
                            }
                        }
                    }

                }
            }
        }
        return false;
    }

    public boolean checkTenantLocationData(String id){

        if(mHomeActivity.mAccessibleTenantLocationDataModel != null && (mHomeActivity.mAccessibleTenantLocationDataModel.getData() != null && mHomeActivity.mAccessibleTenantLocationDataModel.getData().size() > 0)){

            for(int i = 0; i < mHomeActivity.mAccessibleTenantLocationDataModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(i).getId()))){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkLocationGroupData(String id){

        if(mHomeActivity.mDashboardLocationGroupListModel != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModel.getData().size() > 0)){

            for(int i = 0; i < mHomeActivity.mDashboardLocationGroupListModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mHomeActivity.mDashboardLocationGroupListModel.getData().get(i).getLocationGroupID()))){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkProductData(String id){

        if(mHomeActivity.mProductListFromLocationGroupModel != null && (mHomeActivity.mProductListFromLocationGroupModel.getData() != null && mHomeActivity.mProductListFromLocationGroupModel.getData().size() > 0)){
            for(int i = 0; i < mHomeActivity.mProductListFromLocationGroupModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mHomeActivity.mProductListFromLocationGroupModel.getData().get(i).getId()))){
                    return true;
                }
            }
        } else if(id.equalsIgnoreCase("-1")){
            return true;
        } else if(id.equalsIgnoreCase("-2")){
            return true;
        }


        return false;
    }

    public boolean checkuserData(String id){

        if(mHomeActivity.mDashboardUserListModel != null && (mHomeActivity.mDashboardUserListModel.getData() != null && mHomeActivity.mDashboardUserListModel.getData().size() > 0)){

            for(int i = 0; i < mHomeActivity.mDashboardUserListModel.getData().size(); i++){
                if(id.equalsIgnoreCase(String.valueOf(mHomeActivity.mDashboardUserListModel.getData().get(i).getId()))){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isDynamicFilterExist(){

        if((mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0)){
            for(int i = 0; i < mHomeActivity.mPrerferencedLeftFilterBaseDatas.size(); i++){
                if(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).isDynamicFilter){
                    return true;
                }
            }
        }
        return false;

    }

    public FilterBaseData getSelectedPreferenceData(String filterName){
        if(mHomeActivity.mPrerferencedLeftFilterBaseDatas != null && mHomeActivity.mPrerferencedLeftFilterBaseDatas.size() > 0){
            for(int i = 0; i < mHomeActivity.mPrerferencedLeftFilterBaseDatas.size(); i++){
                if(filterName.equalsIgnoreCase(mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i).filterName)){
                    return mHomeActivity.mPrerferencedLeftFilterBaseDatas.get(i);
                }
            }
        }
        return null;
    }


    void getDashboardFiltersFromAPI(final int dashboardId){

        String firstDate = "", lastDate = "";
        int TenantId = InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_tenant_id), -2);
        int RegionTypeId = -2;
        int RegionId = -2;
        int ProductId = -2;
        int UserId = -2;
        int TenantLocationId = -2;
        int LocationGroupId = -2;
        if(!(mHomeActivity.mMainLeftFilterBaseDatas != null && mHomeActivity.mMainLeftFilterBaseDatas.size() > 0)){
            if(!mHomeActivity.isTenantLocation()){
               /* if(!mHomeActivity.isNoNeedUpdateMetric ()){
                    if(!IsNeedRecursive)
                        mHomeActivity.setNoNeedUpdateMetric ( false);
                    InGaugeSession.write ( mContext.getResources ().getString ( R.string.key_selected_metric_type_name ), mHomeActivity.mFilterMetricTypeModelList.get ( 1 ).name );
                }*/

            }
        }
        String finalMatrixDataType = mHomeActivity.getResources().getStringArray(R.array.metric_type_array)[1];
        String MatrixDataType = finalMatrixDataType;
        if(MatrixDataType.equalsIgnoreCase("Check out")){
            MatrixDataType = "Arrival";
        } else if(MatrixDataType.equalsIgnoreCase("Check in")){
            MatrixDataType = "Departure";
        } else if(MatrixDataType.equalsIgnoreCase("Daily")){
            MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
        }

        try{
            firstDate = DateTimeUtils.getFirstDay(new Date());
            lastDate = DateTimeUtils.getLastDay(new Date());
            System.out.println("First Date: " + firstDate);
            System.out.println("Last Date: " + lastDate);
        } catch(Exception e){
            e.printStackTrace();
        }
        String FromDate = "";
        String ToDate = "";
        String CompFromDate = "";
        String CompToDate = "";
        boolean IsCompare = false;

        FromDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_start_date_server), "");
        ToDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_end_date_server), "");
        CompFromDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_compare_start_date_server), "");
        CompToDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_compare_end_date_server), "");
        IsCompare = InGaugeSession.read(mHomeActivity.getString(R.string.key_is_compare_on), true);


        Calendar startserverCalendar = null;
        Calendar endserverCalendar = null;

        Calendar compstartserverCalendar = null;
        Calendar compendserverCalendar = null;
        try{

            Logger.Error("<<< From Date " + FromDate);
            Logger.Error("<<< To Date " + ToDate);
            Calendar startCalendar = UiUtils.DateToCalendar(sdfServer.parse(FromDate));
            Long serverstartLong = startCalendar.getTimeInMillis();
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + serverstartLong);
            String serverStartDate = sdfServer.format(serverstartLong);
            startserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverStartDate + " 00:00:00"));
            startserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + startserverCalendar.getTimeInMillis());


            Calendar endCalendar = UiUtils.DateToCalendar(sdfServer.parse(ToDate));
            Long serverEndLong = endCalendar.getTimeInMillis();
            String serverEndDate = sdfServer.format(serverEndLong);
            endserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverEndDate + " 23:59:59"));
            endserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + serverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + endserverCalendar.getTimeInMillis());


            Calendar compstartCalendar = UiUtils.DateToCalendar(sdfServer.parse(CompFromDate));
            Long comserverstartLong = compstartCalendar.getTimeInMillis();
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + comserverstartLong);
            String compserverStartDate = sdfServer.format(comserverstartLong);
            compstartserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(compserverStartDate + " 00:00:00"));
            compstartserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + compstartserverCalendar.getTimeInMillis());


            Calendar compendCalendar = UiUtils.DateToCalendar(sdfServer.parse(CompToDate));
            Long compserverEndLong = compendCalendar.getTimeInMillis();
            String compserverEndDate = sdfServer.format(compserverEndLong);
            compendserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(compserverEndDate + " 23:59:59"));
            compendserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + compserverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + compendserverCalendar.getTimeInMillis());


        } catch(ParseException e){
            e.printStackTrace();
        }
        mCall = apiInterface.getDashboardFiltersCheck(
                dashboardId,
                TenantId,
                RegionTypeId,
                RegionId,
                ProductId,
                UserId,
                TenantLocationId,
                LocationGroupId,
                MatrixDataType,
                FromDate,
                ToDate,
                CompFromDate,
                CompToDate,
                startserverCalendar.getTimeInMillis(),
                endserverCalendar.getTimeInMillis(),
                compstartserverCalendar.getTimeInMillis(),
                compendserverCalendar.getTimeInMillis(),
                IsCompare);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){

                if(response.body() != null){
                    try{
                        JSONObject responseJson = new JSONObject(response.body().string());
                        if(responseJson != null){
                            mHomeActivity.setRegionFilterForLb(false);
                            mHomeActivity.setTenantLocationForLb(false);
                            mHomeActivity.setCustomGoalFilterForLb(false);
                            mHomeActivity.setProductForLb(false);
                            mHomeActivity.setLocationGroupForLb(false);
                            mHomeActivity.setUserForLb(false);
                            JSONObject mdataJsonObject = responseJson.optJSONObject("data");
                            if(mdataJsonObject != null){
                                JSONArray mJsonArrayFilters = mdataJsonObject.optJSONArray("filters");
                                mFilterListString.clear();
                                if(mJsonArrayFilters != null && mJsonArrayFilters.length() > 0){
                                    for(int i = 0; i < mJsonArrayFilters.length(); i++){
                                        mFilterListString.add(mJsonArrayFilters.optString(i));
                                    }

                                    for(String filterName : mFilterListString){
                                        checkFilterFromCustomreport(filterName);
                                    }
                                    filterCallAfterCheck();
                                } else{
                                    getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                                }
                            }
                        }

                    } catch(JSONException e){
                        e.printStackTrace();
                        Logger.Error("Exceptiopn " + e.toString());
                    } catch(Exception e){
                        e.printStackTrace();

                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){

            }
        });
    }

    /**
     * @param authToken
     * @param tenantId
     * @param userId
     * @param orderBy
     * @param sort
     * @param regionId
     * @param activeStatus
     */
    void getAccessibleTenantLocation(final String authToken, int tenantId, String userId, String orderBy, String sort, String regionId, String activeStatus){

        Call mCall = apiInterface.getAccessibleTenantLocation(tenantId, userId, orderBy, sort, regionId, activeStatus,true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard = (AccessibleTenantLocationDataModel) response.body();
                    /*Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_location_obj), json);*/
                    if((mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard != null && (mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData() != null && mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().size() > 0))){

                        Collections.sort(mHomeActivity.mAccessibleTenantLocationDataModel.getData(), new Comparator<AccessibleTenantLocationDataModel.Datum>(){
                            @Override
                            public int compare(AccessibleTenantLocationDataModel.Datum d1, AccessibleTenantLocationDataModel.Datum d2){
                                return Boolean.compare(d2.getMasterLocation(), d1.getMasterLocation());
                            }
                        });
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 2;
                        mFilterBaseData.isMasterLocation = mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getMasterLocation();
                        mFilterBaseDatas.add(mFilterBaseData);

                        //InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_tenant_location_id), mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getId());
//                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_tenant_location_name), mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getName());
//                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_hotel_metric_type_name),
//                                mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getHotelMetricsDataType());
//                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_metric_type_name),
//                                mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getHotelMetricsDataType());

                        if(mHomeActivity.isTenantLocation()){
                            mHomeActivity.setSelectedTenantLocationIdForLb(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_tenant_location_id), mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getId()));
                            mHomeActivity.setSelectedTenantLocationNameForLb(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_tenant_location_name), mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getName()));
                            mHomeActivity.setSelectedHotelMetricTypeNameLb(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_hotel_metric_type_name),
                                    mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getHotelMetricsDataType()));
                        } else{
                            mHomeActivity.setSelectedTenantLocationIdForLb(mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getId());
                            mHomeActivity.setSelectedTenantLocationNameForLb(mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getName());
                            mHomeActivity.setSelectedHotelMetricTypeNameLb(mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getHotelMetricsDataType());
                        }


                        if(mHomeActivity.isProductForLb() || mHomeActivity.isUserForLb()){
                            mHomeActivity.setLocationGroupForLb(true);
                        }
                        if(mHomeActivity.isLocationGroupForLb() || mHomeActivity.isProductForLb() || mHomeActivity.isUserForLb()){
                            if(mHomeActivity.isLocationGroupForLb()){

                                getDashboardLocationGroupList(authToken,
                                        String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModel.getData().get(0).getId()),
                                        String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));


                            } else if(mHomeActivity.isProductForLb()){
                                getProductList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                        null);

                            } else if(mHomeActivity.isUserForLb()){


                                String tenantLocationIdFromFilter = null;
                                if(mHomeActivity.getSelectedTenantLocationIdForLb() != -2){
                                    tenantLocationIdFromFilter = String.valueOf(mHomeActivity.getSelectedTenantLocationIdForLb());
                                }
                                getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                        tenantLocationIdFromFilter,
                                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_server), ""),
                                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server), ""), null);


                            }

                        } else{

                            getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                            /*pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
                        }

                    } else{
                        /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_tenant_location_id), -2);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_tenant_location_name), "");
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_hotel_metric_type_name), "");
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_metric_type_name), "");*/
                        mHomeActivity.setSelectedTenantLocationIdForLb(-2);
                        mHomeActivity.setSelectedTenantLocationNameForLb("");
                        mHomeActivity.setSelectedHotelMetricTypeNameLb("");

                        if(mHomeActivity.isProductForLb() || mHomeActivity.isUserForLb()){
                            mHomeActivity.setLocationGroupForLb(true);
                        }
                        if(mHomeActivity.isLocationGroupForLb() || mHomeActivity.isProductForLb() || mHomeActivity.isUserForLb()){
                            if(mHomeActivity.isLocationGroupForLb()){
                                if((mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard != null && mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData() != null) && (mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().size() > 0)){
                                    getDashboardLocationGroupList(authToken,
                                            String.valueOf(mHomeActivity.mAccessibleTenantLocationDataModelForLeaderBoard.getData().get(0).getId()),
                                            String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));

                                } else{
                                    getDashboardLocationGroupList(authToken,
                                            String.valueOf(-2),
                                            String.valueOf(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0)));
                                }

                            } else if(mHomeActivity.isProductForLb()){
                                getProductList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                        null);

                            } else if(mHomeActivity.isUserForLb()){
                                String tenantLocationIdFromFilter = null;
                                if(mHomeActivity.getSelectedTenantLocationIdForLb() != -2){
                                    tenantLocationIdFromFilter = String.valueOf(mHomeActivity.getSelectedTenantLocationIdForLb());
                                }
                                getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                        tenantLocationIdFromFilter,
                                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""), null);

                            }

                        } else{
                            /*pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
                            getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                        }

                        //InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);
                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{
                    /*pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);*/
                    //InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);
                    getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });
    }


    /**
     * @param authToken
     * @param tenantlocationId
     * @param userId
     */
    void getDashboardLocationGroupList(final String authToken, String tenantlocationId, String userId){

        Call mCall = apiInterface.getDashboardLocationGroupList(tenantlocationId, userId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard = (DashboardLocationGroupListModel) response.body();
                    /*Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardLocationGroupListModel);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_location_group_obj), json);*/
                    if((mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().size() > 0))){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().get(0).getLocationGroupID());
                        mFilterBaseData.name = mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().get(0).getLocationGroupName();
                        mFilterBaseData.filterIndex = 3;
                        mFilterBaseDatas.add(mFilterBaseData);

                        /*public int selectedLocationGroupIdForLb;
                        public String selectedLocationGroupNameLb="";*/
                        
                        
                        /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group), (mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID()));
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group_name), mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName());*/
                        if(mHomeActivity.isLocationGroup()){
                            mHomeActivity.setSelectedLocationGroupIdForLb(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_location_group), (mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID())));
                            mHomeActivity.setSelectedLocationGroupNameLb(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_location_group_name), mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName()));
                        } else{
                            mHomeActivity.setSelectedLocationGroupIdForLb(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupID());
                            mHomeActivity.setSelectedLocationGroupNameLb(mHomeActivity.mDashboardLocationGroupListModel.getData().get(0).getLocationGroupName());
                        }


                        if(mHomeActivity.isProductForLb() || mHomeActivity.isUserForLb()){
                            if(mHomeActivity.isProductForLb()){


                                getProductList(authToken, String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().get(0).getLocationGroupID()));


                            } else if(mHomeActivity.isUserForLb()){
                                String tenantLocationIdFromFilter = null;
                                if(mHomeActivity.getSelectedTenantLocationIdForLb() != -2){
                                    tenantLocationIdFromFilter = String.valueOf(mHomeActivity.getSelectedTenantLocationIdForLb());
                                }
                                if(mHomeActivity.isTenantLocationForLb()){

                                    getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                            tenantLocationIdFromFilter,
                                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                            String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().get(0).getLocationGroupID()));

                                } else{

                                    getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                            tenantLocationIdFromFilter,
                                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                            String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().get(0).getLocationGroupID()));


                                }
                            }
                        } else{
                            /*pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
                            getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                        }

                    } else{
                        /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group), -2);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group_name), "");*/
                        mHomeActivity.setSelectedLocationGroupIdForLb(-2);
                        mHomeActivity.setSelectedLocationGroupNameLb("");

                        if(mHomeActivity.isProductForLb() || mHomeActivity.isUserForLb()){
                            if(mHomeActivity.isProductForLb()){
                                if(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().size() > 0)){
                                    getProductList(authToken, String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().get(0).getLocationGroupID()));
                                } else{
                                    getProductList(authToken, String.valueOf(-2));
                                }


                            } else if(mHomeActivity.isUserForLb()){
                                if(mHomeActivity.isTenantLocationForLb()){
                                    String tenantLocationIdFromFilter = null;
                                    if(mHomeActivity.getSelectedTenantLocationIdForLb() != -2){
                                        tenantLocationIdFromFilter = String.valueOf(mHomeActivity.getSelectedTenantLocationIdForLb());
                                    }
                                    if(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().size() > 0)){

                                        getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                                tenantLocationIdFromFilter,
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                                String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().get(0).getLocationGroupID()));
                                    } else{
                                        getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                                tenantLocationIdFromFilter,
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                                String.valueOf(-2));

                                    }

                                } else{

                                    if(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard != null && (mHomeActivity.mDashboardLocationGroupListModel.getData() != null && mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().size() > 0)){

                                        getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                                String.valueOf(mHomeActivity.mDashboardLocationGroupListModelForLeaderBoard.getData().get(0).getLocationGroupID()));
                                    } else{
                                        getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                                String.valueOf(-2));
                                    }
                                }
                            }
                        } else{
                            /*pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
                            getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                    selectedDashboardId, false);
                        }
                        //InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);
                    }
                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{

                    /*pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);*/
                    getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
               /* pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });
    }

    /**
     * @param authToken
     * @param locationGroupId
     */
    void getProductList(final String authToken, final String locationGroupId){

        Call mCall = apiInterface.getProductList(locationGroupId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard = (ProductListFromLocationGroupModel) response.body();
                    /*Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_product_obj), json);*/

                    if(mHomeActivity.isCustomGoalFilterForLb()){
                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(-1);
                        mFilterBaseData.name = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_title_arpd_inc));
                        mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mHomeActivity.getResources().getString(R.string.mobile_key_filter_product));
                        mFilterBaseData.filterIndex = 4;
                        mFilterBaseDatas.add(mFilterBaseData);


                        try{
                            /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id));
                            InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name);*/
                            if(mHomeActivity.isProduct()){
                                mHomeActivity.setSelectedProductIdForLb(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_location_group_product), Integer.parseInt(mFilterBaseData.id)));
                                mHomeActivity.setSelectedProductNameLb(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_location_group_product_name), mFilterBaseData.name));
                            } else{
                                mHomeActivity.setSelectedProductIdForLb(Integer.parseInt(mFilterBaseData.id));
                                mHomeActivity.setSelectedProductNameLb(mFilterBaseData.name);
                            }

                        } catch(Exception e){
                            e.printStackTrace();
                        }

                        if(mHomeActivity.isTenantLocationForLb() && mHomeActivity.isLocationGroupForLb()){
                            String tenantLocationIdFromFilter = null;
                            if(mHomeActivity.getSelectedTenantLocationIdForLb() != -2){
                                tenantLocationIdFromFilter = String.valueOf(mHomeActivity.getSelectedTenantLocationIdForLb());
                            }
                            getDashboardUserList(authToken,
                                    tenantLocationIdFromFilter,
                                    InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                    InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                    locationGroupId);
                        } else{
                            String tenantLocationIdFromFilter = null;
                            if(mHomeActivity.getSelectedTenantLocationIdForLb() != -2){
                                tenantLocationIdFromFilter = String.valueOf(mHomeActivity.getSelectedTenantLocationIdForLb());
                            }
                            if(mHomeActivity.isTenantLocationForLb()){
                                getDashboardUserList(authToken,
                                        tenantLocationIdFromFilter,
                                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                        null);
                            } else{

                                if(mHomeActivity.isLocationGroupForLb()){
                                    getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                            null,
                                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""), locationGroupId);
                                } else{
                                    /*pbFilter.setVisibility(View.GONE);
                                    imgFilter.setVisibility(View.VISIBLE);*/
                                    getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                                }

                            }


                        }
                    } else{
                        if((mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard != null &&
                                (mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData() != null && mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData().size() > 0))){
                            FilterBaseData mFilterBaseData = new FilterBaseData();
                            mFilterBaseData.id = String.valueOf(mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData().get(0).getId());
                            mFilterBaseData.name = mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData().get(0).getName();
                            mFilterBaseData.filterIndex = 4;
                            mFilterBaseDatas.add(mFilterBaseData);
                            if(mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard != null){
                                if(mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard != null && mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData().size() > 0){

                                    try{
                                        /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group_product), mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData().get(0).getId());
                                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group_product_name), mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData().get(0).getName());*/
                                        mHomeActivity.setSelectedProductIdForLb(mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData().get(0).getId());
                                        mHomeActivity.setSelectedProductNameLb(mHomeActivity.mProductListFromLocationGroupModelForLeaderBoard.getData().get(0).getName());
                                    } catch(Exception e){
                                        e.printStackTrace();
                                    }

                                } else{
                                    /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group_product), -2);
                                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_location_group_product_name), "");*/
                                    mHomeActivity.setSelectedProductIdForLb(-2);
                                    mHomeActivity.setSelectedProductNameLb("");
                                }

                            }


                            if(mHomeActivity.isTenantLocationForLb() && mHomeActivity.isLocationGroupForLb()){
                                String tenantLocationIdFromFilter = null;
                                if(mHomeActivity.getSelectedTenantLocationIdForLb() != -2){
                                    tenantLocationIdFromFilter = String.valueOf(mHomeActivity.getSelectedTenantLocationIdForLb());
                                }
                                getDashboardUserList(authToken,
                                        tenantLocationIdFromFilter,
                                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                        InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                        locationGroupId);
                            } else{
                                String tenantLocationIdFromFilter = null;
                                if(mHomeActivity.getSelectedTenantLocationIdForLb() != -2){
                                    tenantLocationIdFromFilter = String.valueOf(mHomeActivity.getSelectedTenantLocationIdForLb());
                                }
                                if(mHomeActivity.isTenantLocationForLb()){
                                    getDashboardUserList(authToken,
                                            tenantLocationIdFromFilter,
                                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                            InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""),
                                            null);
                                } else{

                                    if(mHomeActivity.isLocationGroupForLb()){
                                        getDashboardUserList(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""),
                                                null,
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), ""),
                                                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), ""), locationGroupId);
                                    } else{
                                        /*pbFilter.setVisibility(View.GONE);
                                        imgFilter.setVisibility(View.VISIBLE);*/
                                        getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                                    }

                                }


                            }
                        } else{
                            /*pbFilter.setVisibility(View.GONE);
                            imgFilter.setVisibility(View.VISIBLE);*/
                            getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                            /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);*/
                        }

                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                    }
                } else{
                    /*pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);*/
                    getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/
            }
        });
    }

    /**
     * @param authToken
     * @param startDate
     * @param endDate
     * @param locationGroupId
     */
    void getDashboardUserList(String authToken, String tenantLocationFromIdFromFilter, String startDate, String endDate, String locationGroupId){
        int TenantId = InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_tenant_id), -2);
        Call mCall = apiInterface.getDashboardUserListForDashboard(TenantId, true, false, false, tenantLocationFromIdFromFilter, locationGroupId, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                //InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);*/

                if(response.body() != null){
                    mHomeActivity.mDashboardUserListModelForLeaderBoard = (DashboardUserListModel) response.body();
                    /*Gson gson = new Gson();
                    String json = gson.toJson(mHomeActivity.mDashboardUserListModelForLeaderBoard);
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_user_obj), json);*/
                    if((mHomeActivity.mDashboardUserListModelForLeaderBoard != null &&
                            (mHomeActivity.mDashboardUserListModelForLeaderBoard.getData() != null &&
                                    mHomeActivity.mDashboardUserListModelForLeaderBoard.getData().size() > 0))){

                        FilterBaseData mFilterBaseData = new FilterBaseData();
                        mFilterBaseData.id = String.valueOf(mHomeActivity.mDashboardUserListModelForLeaderBoard.getData().get(0).getId());
                        mFilterBaseData.name = mHomeActivity.mDashboardUserListModelForLeaderBoard.getData().get(0).getName();
                        mFilterBaseData.filterIndex = 5;

/*                        public int selectedUserIdForLB;
                        public String selectedUserNameForLB="";*/

                        /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_user), (mHomeActivity.mDashboardUserListModel.getData().get(0).getId()));
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_user_name), mHomeActivity.mDashboardUserListModel.getData().get(0).getName());*/

                        if(mHomeActivity.isUser()){
                            mHomeActivity.setSelectedUserIdForLB(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_user), (mHomeActivity.mDashboardUserListModel.getData().get(0).getId())));
                            mHomeActivity.setSelectedUserNameForLB(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_user_name), mHomeActivity.mDashboardUserListModel.getData().get(0).getName()));
                        } else{
                            mHomeActivity.setSelectedUserIdForLB(mHomeActivity.mDashboardUserListModel.getData().get(0).getId());
                            mHomeActivity.setSelectedUserNameForLB(mHomeActivity.mDashboardUserListModel.getData().get(0).getName());
                        }

                        mFilterBaseDatas.add(mFilterBaseData);
                        getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    } else{
                        /*InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_user), -2);
                        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_user_name), "");*/
                        mHomeActivity.setSelectedUserIdForLB(-2);
                        mHomeActivity.setSelectedUserNameForLB("");
                        /*pbFilter.setVisibility(View.GONE);
                        imgFilter.setVisibility(View.VISIBLE);*/
                        getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                    }

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();

                    }
                } else{
                    /*pbFilter.setVisibility(View.GONE);
                    imgFilter.setVisibility(View.VISIBLE);*/
                    getDashboardCustomResport(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_auth_token), ""), selectedDashboardId, false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*pbFilter.setVisibility(View.GONE);
                imgFilter.setVisibility(View.VISIBLE);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_is_need_filter_call), false);*/
            }
        });
    }

    void getDashboardCustomResport(final String authToken, final int dashboardId, final boolean IsNeedRecursive){

        /*rvDashboardList.setVisibility(View.GONE);
        rlProgress.setVisibility(View.VISIBLE);*/
        String firstDate = "", lastDate = "";


        if(!mHomeActivity.isRegionFilterForLb()){
            mHomeActivity.setSelectedRegionTypeForLb(-2);
            mHomeActivity.setSelectedRegionIdForLb(-2);
        }
        if(!mHomeActivity.isTenantLocationForLb()){
            //InGaugeSession.write(mHomeActivity.getString(R.string.key_tenant_location_id), -2);
            mHomeActivity.setSelectedTenantLocationIdForLb(-2);
        }
        if(!mHomeActivity.isLocationGroupForLb()){
            //InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_location_group), -2);
            mHomeActivity.setSelectedLocationGroupIdForLb(-2);
        }
        if(!mHomeActivity.isProductForLb()){
            InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_location_group_product), -2);
            mHomeActivity.setSelectedProductIdForLb(-2);
        }
        if(!mHomeActivity.isUserForLb()){
            InGaugeSession.write(mHomeActivity.getString(R.string.key_selected_user), -2);
            mHomeActivity.setSelectedUserIdForLB(-2);
        }

        int TenantId = InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_tenant_id), -2);

//        int RegionTypeId = InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_regiontype), -2);
        int RegionTypeId = mHomeActivity.getSelectedRegionTypeForLb();
//        int RegionId = InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_region), -2);
        int RegionId = mHomeActivity.getSelectedRegionIdForLb();
        //int ProductId = (InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_location_group_product), -2));
        int ProductId = mHomeActivity.getSelectedProductIdForLb();
        //int UserId = (InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_user), -2));
        int UserId = mHomeActivity.getSelectedUserIdForLB();
        //int TenantLocationId = (InGaugeSession.read(mHomeActivity.getString(R.string.key_tenant_location_id), -2));
        int TenantLocationId = mHomeActivity.getSelectedTenantLocationIdForLb();
//        int LocationGroupId = InGaugeSession.read(mHomeActivity.getString(R.string.key_selected_location_group), -2);
        int LocationGroupId = mHomeActivity.getSelectedLocationGroupIdForLb();

        String finalMatrixDataType = mHomeActivity.getResources().getStringArray(R.array.metric_type_array)[1];
        String MatrixDataType = finalMatrixDataType;
        if(MatrixDataType.equalsIgnoreCase("Check out")){
            MatrixDataType = "Arrival";
        } else if(MatrixDataType.equalsIgnoreCase("Check in")){
            MatrixDataType = "Departure";
        } else if(MatrixDataType.equalsIgnoreCase("Daily")){
            MatrixDataType = mHomeActivity.mobilekeyObject.optString("KEY_METRIC_TYPE_DAILY");
        }

        try{
            firstDate = DateTimeUtils.getFirstDay(new Date());
            lastDate = DateTimeUtils.getLastDay(new Date());
            System.out.println("First Date: " + firstDate);
            System.out.println("Last Date: " + lastDate);
        } catch(Exception e){
            e.printStackTrace();
        }
        String FromDate = "";
        String ToDate = "";
        String CompFromDate = "";
        String CompToDate = "";
        boolean IsCompare = false;

        FromDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_start_date_serverfor_lb), "");
        ToDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_end_date_server_for_lb), "");
        CompFromDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_compare_start_date_server), "");
        CompToDate = InGaugeSession.read(mHomeActivity.getString(R.string.key_compare_end_date_server), "");
        // This false put as static because of Leader Report is based on only From Date and To Date.
        // Compare is not available right now
        IsCompare = false;


        Calendar startserverCalendar = null;
        Calendar endserverCalendar = null;

        Calendar compstartserverCalendar = null;
        Calendar compendserverCalendar = null;
        try{

            Logger.Error("<<< From Date " + FromDate);
            Logger.Error("<<< To Date " + ToDate);
            Calendar startCalendar = UiUtils.DateToCalendar(sdfServer.parse(FromDate));
            Long serverstartLong = startCalendar.getTimeInMillis();
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + serverstartLong);
            String serverStartDate = sdfServer.format(serverstartLong);
            startserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverStartDate + " 00:00:00"));
            startserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + startserverCalendar.getTimeInMillis());


            Calendar endCalendar = UiUtils.DateToCalendar(sdfServer.parse(ToDate));
            Long serverEndLong = endCalendar.getTimeInMillis();
            String serverEndDate = sdfServer.format(serverEndLong);
            endserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(serverEndDate + " 23:59:59"));
            endserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + serverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + endserverCalendar.getTimeInMillis());


            Calendar compstartCalendar = UiUtils.DateToCalendar(sdfServer.parse(CompFromDate));
            Long comserverstartLong = compstartCalendar.getTimeInMillis();
            Logger.Error("@@@@@@@@ Server Start Time Stamp " + comserverstartLong);
            String compserverStartDate = sdfServer.format(comserverstartLong);
            compstartserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(compserverStartDate + " 00:00:00"));
            compstartserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ Server Start Time Stamp After Added" + compstartserverCalendar.getTimeInMillis());


            Calendar compendCalendar = UiUtils.DateToCalendar(sdfServer.parse(CompToDate));
            Long compserverEndLong = compendCalendar.getTimeInMillis();
            String compserverEndDate = sdfServer.format(compserverEndLong);
            compendserverCalendar = UiUtils.DateToCalendar(sdfServerDateTime.parse(compserverEndDate + " 23:59:59"));
            compendserverCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Logger.Error("@@@@@@@@ End Time Stamp " + compserverEndLong);
            Logger.Error("@@@@@@@@ Server End Time Stamp After Added" + compendserverCalendar.getTimeInMillis());


        } catch(ParseException e){
            e.printStackTrace();
        }


        mCall = apiInterface.getDashboardCustomReports(
                dashboardId,
                TenantId,
                RegionTypeId,
                RegionId,
                ProductId,
                UserId,
                TenantLocationId,
                LocationGroupId,
                MatrixDataType,
                FromDate,
                ToDate,
                CompFromDate,
                CompToDate,
                startserverCalendar.getTimeInMillis(),
                endserverCalendar.getTimeInMillis(),
                compstartserverCalendar.getTimeInMillis(),
                compendserverCalendar.getTimeInMillis(),
                IsCompare);
        mCall.enqueue(new Callback<ResponseBody>(){
            CustomDashboardModel.Data mCustomDashboardModel;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                rlCustomProgressbar.setVisibility(View.GONE);
                setToolbarTitles();
                if(response.body() != null){
                    try{
                        JSONObject responseJson = new JSONObject(response.body().string());
                        if(responseJson != null){
                            JSONObject mdataJsonObject = responseJson.optJSONObject("data");
                            if(mdataJsonObject != null){
                                JSONArray mJsonArrayFilters = mdataJsonObject.optJSONArray("filters");
                                mFilterListString.clear();
                                if(mJsonArrayFilters != null && mJsonArrayFilters.length() > 0){
                                    for(int i = 0; i < mJsonArrayFilters.length(); i++){
                                        mFilterListString.add(mJsonArrayFilters.optString(i));
                                    }
                                }
                                Gson gson = new Gson();
                                mCustomDashboardModel = gson.fromJson(mdataJsonObject.toString(), CustomDashboardModel.Data.class);
                                Logger.Error("<<< Custom Dashboard Model :" + mCustomDashboardModel.getCustomReport().size());
                                mCustomReportList = mCustomDashboardModel.getCustomReport();
                                isAPICallDone = true;
                                mFragmentEntityList.setEntityListAdapter(mCustomReportList);
                            }
                        }

                    } catch(JSONException e){
                        e.printStackTrace();
                        Logger.Error("Exceptiopn " + e.toString());
                    } catch(Exception e){
                        e.printStackTrace();

                    }


                    /*if(mCustomReportList.size() > 0){
                        tvNoData.setVisibility(View.GONE);
                        rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);
                        setDashboardCustomReportAdapter(mCustomReportList);
                        tvNoData.setText(mHomeActivity.getResources().getString(R.string.tv_no_record));
                    } else{
                        tvNoData.setText(mHomeActivity.getResources().getString(R.string.tv_no_record));
                        tvNoData.setVisibility(View.VISIBLE);
                        rvDashboardList.setVisibility(View.GONE);
                        rlProgress.setVisibility(View.GONE);
                    }*/


                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        mHomeActivity.ForceLogout();
                        /*rvDashboardList.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.VISIBLE);
                        rlProgress.setVisibility(View.GONE);*/
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                /*rlProgress.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
                rvDashboardList.setVisibility(View.GONE);
                tvNoData.setText(mHomeActivity.getResources().getString(R.string.tv_no_record));*/
                t.printStackTrace();
            }
        });

    }

    public void setToolbarTitles(){
        if(mHomeActivity.isTenantLocation()){
            mHomeActivity.tvTitle.setText("Leaderboard" + " (Location)");
        } else{
            mHomeActivity.tvTitle.setText("Leaderboard" + " (Region)");
        }


        String locationName = mHomeActivity.getSelectedTenantLocationNameForLb();
        String userName = mHomeActivity.getSelectedUserNameForLB();
        String productName = mHomeActivity.getSelectedProductNameLb();
        String countryName = mHomeActivity.getSelectedRegionNameForLb();


        mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
        if(mHomeActivity.isRegionFilter()){
            if(countryName.length() > 0){
                mHomeActivity.tvSubTitle.setVisibility(View.VISIBLE);
                mHomeActivity.tvSubTitle.setText(countryName);
            }
        }

        if(mHomeActivity.isTenantLocationForLb() && mHomeActivity.isUserForLb()){
            mHomeActivity.tvSubTitle.setText(locationName + "-" + userName);
            return;
        }
        if(mHomeActivity.isTenantLocationForLb() && mHomeActivity.isProductForLb()){
            mHomeActivity.tvSubTitle.setText(locationName + "-" + productName);
            return;
        }
        if(mHomeActivity.isTenantLocationForLb()){
            mHomeActivity.tvSubTitle.setText(locationName);
        } else{
            mHomeActivity.tvSubTitle.setVisibility(View.GONE);
        }

    }
}
