package com.ingauge.fragments.dateselection.tabsfragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by mansurum on 15-Feb-18.
 */

public class CustomFragment extends Fragment implements View.OnClickListener{
    View rootView;

    public static String KEY_IS_FROM_LEADERBOARD = "is_from_leaderboard";
    Context mContext;
    HomeActivity mHomeActivity;
    TextView txtviewstrtDteFilter;
    TextView txtviewEndDteFilter;
    TextView txtStrtDte;
    TextView txtEndDte;
    TextView txtpreEndDteLable;
    TextView txtpreStrtDteLable;
    TextView txtpreEndDte;
    TextView txtpreStrtDte;
    LinearLayout linearStartDte;
    LinearLayout linearEndDte;
    LinearLayout linearPreStartDte;
    LinearLayout linearPreEndDte;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
    SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    //   private int mYear, mMonth, mDay;
    private Calendar calendar;
    SwitchCompat switchCompareDate;
    LinearLayout llCompareDateLayout;
    boolean isFromLeaderBoard = false;
    //DateSelectionFragment mDateSelectionFragment = new DateSelectionFragment();


    public String startDate = "";
    public String endDate = "";
    public String compareStartDate = "";
    public String compareEndDate = "";
    public String serverStartDate = "";
    public String serverEndDate = "";
    public String serverCompareStartDate = "";
    public String serverCompareEndDate = "";

    public boolean isCompare = true;
    private boolean isHideCompare=false;


    public int tabIndex = 3;

    LinearLayout llLeaderBoardForCompare;



    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setTabIndex(3);

        //mDateSelectionFragment.setTabIndex(4);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_date_filter, container, false);
        initViews(rootView);

        if(getArguments() != null){
            Bundle mBundle = getArguments();
            /*startDate = mBundle.getString(DateSelectionFragment.KEY_START_DATE);
            endDate =  mBundle.getString(DateSelectionFragment.KEY_END_DATE);
            compareStartDate = mBundle.getString(DateSelectionFragment.KEY_COMPARE_START_DATE);
            compareEndDate =  mBundle.getString(DateSelectionFragment.KEY_COMPARE_END_DATE);
            serverStartDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_START_DATE);
            serverEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_END_DATE);
            serverCompareStartDate= mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_START_DATE);
            serverCompareEndDate = mBundle.getString(DateSelectionFragment.KEY_SERVER_COMPARE_END_DATE);*/
            isCompare = mBundle.getBoolean(DateSelectionFragment.KEY_IS_COMPARE);
            isFromLeaderBoard = mBundle.getBoolean(DateSelectionFragment.KEY_IS_FROM_LEADERBOARD);
            isHideCompare  = mBundle.getBoolean(DateSelectionFragment.KEY_IS_HIDE_COMPARE);

        }

        if(isFromLeaderBoard){
            llLeaderBoardForCompare.setVisibility(View.GONE);
        }else{
            llLeaderBoardForCompare.setVisibility(View.VISIBLE);
        }

        if(isHideCompare){
            llLeaderBoardForCompare.setVisibility(View.GONE);
        }else{
            llLeaderBoardForCompare.setVisibility(View.VISIBLE);
        }
        if(isFromLeaderBoard){
            startDate = InGaugeSession.read(getString(R.string.key_start_date_for_lb), "");
            endDate = InGaugeSession.read(getString(R.string.key_end_date_for_lb), "");
            serverStartDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_serverfor_lb), "");
            serverEndDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server_for_lb), "");
            startDate = getDisplayStringForWeekTab(serverStartDate);
            endDate = getDisplayStringForWeekTab(serverEndDate);

        } else{
            startDate = InGaugeSession.read(getString(R.string.key_start_date), "");
            endDate = InGaugeSession.read(getString(R.string.key_end_date), "");
            compareStartDate = InGaugeSession.read(getString(R.string.key_compare_start_date), "");
            compareEndDate = InGaugeSession.read(getString(R.string.key_compare_end_date), "");

            serverStartDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_start_date_server), "");
            serverEndDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_end_date_server), "");
            serverCompareStartDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_compare_start_date_server), "");
            serverCompareEndDate = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_compare_end_date_server), "");

            startDate = getDisplayStringForWeekTab(serverStartDate);
            endDate = getDisplayStringForWeekTab(serverEndDate);

            compareStartDate = getDisplayStringForWeekTab(serverCompareStartDate);
            compareEndDate = getDisplayStringForWeekTab(serverCompareEndDate);
        }


        if(isCompare()){
            llCompareDateLayout.setVisibility(View.VISIBLE);
            switchCompareDate.setChecked(true);
        }else{
            llCompareDateLayout.setVisibility(View.GONE);
            switchCompareDate.setChecked(false);
        }
        Logger.Error("<< Start Date = " + startDate);
        Logger.Error("<< End Date = " + endDate);

        Logger.Error("<< Server Start Date = " + serverStartDate);
        Logger.Error("<< Server End Date = " + serverEndDate);

        Logger.Error("<< Compare Start Date = " + compareStartDate);
        Logger.Error("<< Compare End Date = " + compareEndDate);

        Logger.Error("<< Compare Server Start Date = " + serverCompareStartDate);
        Logger.Error("<< Compare Server End Date = " + serverCompareEndDate);


        txtStrtDte.setText(startDate);
        txtEndDte.setText(endDate);
        txtpreStrtDte.setText(compareStartDate);
        txtpreEndDte.setText(compareEndDate);

        switchCompareDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isChecked){
                    llCompareDateLayout.setVisibility(View.VISIBLE);
                    setCompare(true);
                } else{
                    llCompareDateLayout.setVisibility(View.GONE);
                    setCompare(false);
                }
            }
        });

        calendar.setTime(new Date());
        calendar.add(calendar.DATE, -30);
        linearStartDte.setOnClickListener(this);
        linearEndDte.setOnClickListener(this);
        linearPreStartDte.setOnClickListener(this);
        linearPreEndDte.setOnClickListener(this);
        /*if(mDateSelectionFragment.isCompare()){
            llCompareDateLayout.setVisibility(View.VISIBLE);
        } else{
            llCompareDateLayout.setVisibility(View.GONE);
        }*/
        return rootView;
    }

    private void initViews(View rootView){

        llLeaderBoardForCompare = (LinearLayout)rootView.findViewById(R.id.fragment_date_filter_ll_compare_date_leader_board_compare);

        llCompareDateLayout = (LinearLayout) rootView.findViewById(R.id.fragment_date_filter_ll_compare_date);
        txtviewstrtDteFilter = (TextView) rootView.findViewById(R.id.tv_startdate_label);
        txtviewEndDteFilter = (TextView) rootView.findViewById(R.id.tv_enddate_label);
        txtStrtDte = (TextView) rootView.findViewById(R.id.tv_start_date);
        txtEndDte = (TextView) rootView.findViewById(R.id.tv_end_date);
        txtpreStrtDteLable = (TextView) rootView.findViewById(R.id.tv_pre_startdate_label);
        txtpreEndDteLable = (TextView) rootView.findViewById(R.id.tv_pre_endtdate_label);
        txtpreStrtDte = (TextView) rootView.findViewById(R.id.tv_pre_start_date);
        txtpreEndDte = (TextView) rootView.findViewById(R.id.tv_pre_end_date);
        switchCompareDate = (SwitchCompat) rootView.findViewById(R.id.switch_compare_date);
        linearStartDte = (LinearLayout) rootView.findViewById(R.id.linearStartDte);
        linearEndDte = (LinearLayout) rootView.findViewById(R.id.linearEndDte);
        linearPreStartDte = (LinearLayout) rootView.findViewById(R.id.linearPreStartDte);
        linearPreEndDte = (LinearLayout) rootView.findViewById(R.id.linearPreEndDte);


        //Set Texts for Saved Dates

        calendar = Calendar.getInstance();
//        mYear = calendar.get(Calendar.YEAR);
//        mMonth = calendar.get(Calendar.MONTH);
//        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        /*mHomeActivity.tvTitle.setText("Date Filter");
        mHomeActivity.tvSubTitle.setVisibility(View.GONE);*/


    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.linearStartDte:
                Date display = null;
                try{
                    display = sdf.parse(txtStrtDte.getText().toString());
                } catch(ParseException e){
                    e.printStackTrace();
                }
                calendar = UiUtils.DateToCalendar(display);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                txtStrtDte.setText(sdf.format(calendar.getTime()));
                                // mHomeActivity.setLeaderNeedUpdate(true);

                                startDate = txtStrtDte.getText().toString();
                                serverStartDate = sdfServer.format(calendar.getTime());

                                // findPreviousDays();
                            }

                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
                Bundle bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("start_date", InGaugeSession.read(getResources().getString(R.string.key_start_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;
            case R.id.linearEndDte:

                try{
                    calendar = UiUtils.DateToCalendar(sdf.parse(txtEndDte.getText().toString()));
                } catch(ParseException e){
                    e.printStackTrace();
                }

                DatePickerDialog endDatePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){


                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                txtEndDte.setText(sdf.format(calendar.getTime()));

                                endDate = txtEndDte.getText().toString();
                                serverEndDate = sdfServer.format(calendar.getTime());
                                // findPreviousDays();
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                DateFormat format = new SimpleDateFormat("MMM dd ,yyyy", Locale.UK);
                Date date = null;
                Date minDate = null;
                final Calendar cal;
                try{
                    date = format.parse(txtStrtDte.getText().toString());
                    cal = Calendar.getInstance();
                    cal.setTime(date);
                    endDatePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
                    endDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                    endDatePickerDialog.show();

                } catch(ParseException e){
                    e.printStackTrace();
                }
                bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("end_date", InGaugeSession.read(getResources().getString(R.string.key_end_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;
            case R.id.linearPreStartDte:
                try{
                    calendar = UiUtils.DateToCalendar(sdf.parse(txtpreStrtDte.getText().toString()));
                } catch(ParseException e){
                    e.printStackTrace();
                }
                DatePickerDialog PrestrtDatePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){

                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                txtpreStrtDte.setText(sdf.format(calendar.getTime()));

                                compareStartDate = txtpreStrtDte.getText().toString();
                                serverCompareStartDate = sdfServer.format(calendar.getTime());


                                //   findPreviousDays();
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                //DateFormat   format = new SimpleDateFormat("MMM dd ,yyyy", Locale.UK);
                Date Predate = null;
                final Calendar Precal;
                try{
                    date = sdf.parse(txtStrtDte.getText().toString());
                    Precal = Calendar.getInstance();
                    Precal.setTime(date);
                    PrestrtDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    PrestrtDatePickerDialog.show();

                } catch(ParseException e){
                    e.printStackTrace();
                }

                bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("start_date", InGaugeSession.read(getResources().getString(R.string.key_start_date), ""));
                bundle.putString("end_date", InGaugeSession.read(getResources().getString(R.string.key_end_date), ""));
                bundle.putString("compare_start_date", InGaugeSession.read(getResources().getString(R.string.key_compare_start_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;
            case R.id.linearPreEndDte:
                try{
                    calendar = UiUtils.DateToCalendar(sdf.parse(txtpreEndDte.getText().toString()));
                } catch(ParseException e){
                    e.printStackTrace();
                }
                DatePickerDialog PreendDatePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){

                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                txtpreEndDte.setText(sdf.format(calendar.getTime()));


                                compareEndDate = txtpreEndDte.getText().toString();
                                serverCompareEndDate = sdfServer.format(calendar.getTime());
                                //   findPreviousDays();
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                //DateFormat   format = new SimpleDateFormat("MMM dd ,yyyy", Locale.UK);


                try{
                    date = sdf.parse(txtStrtDte.getText().toString());
                    minDate = sdf.parse(txtpreStrtDte.getText().toString());
                    Calendar preMax = Calendar.getInstance();
                    Precal = Calendar.getInstance();
                    Precal.setTime(date);
                    preMax.setTime(minDate);

                    // Precal.add(Precal.DATE,getDayDifference());
                    //PreendDatePickerDialog.getDatePicker().setMaxDate(Precal.getTimeInMillis());
                    PreendDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    PreendDatePickerDialog.getDatePicker().setMinDate(preMax.getTimeInMillis());
                    PreendDatePickerDialog.show();

                } catch(ParseException e){
                    e.printStackTrace();
                }
                bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                bundle.putString("start_date", InGaugeSession.read(getResources().getString(R.string.key_start_date), ""));
                bundle.putString("end_date", InGaugeSession.read(getResources().getString(R.string.key_end_date), ""));
                bundle.putString("compare_start_date", InGaugeSession.read(getResources().getString(R.string.key_compare_start_date), ""));
                bundle.putString("compare_end_date", InGaugeSession.read(getResources().getString(R.string.key_compare_end_date), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard_date", bundle);
                break;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);

        //mDateSelectionFragment.setTabIndex(4);

    }

    public String getStartDate(){
        return startDate;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getEndDate(){
        return endDate;
    }

    public void setEndDate(String endDate){
        this.endDate = endDate;
    }

    public String getCompareStartDate(){
        return compareStartDate;
    }

    public void setCompareStartDate(String compareStartDate){
        this.compareStartDate = compareStartDate;
    }

    public String getCompareEndDate(){
        return compareEndDate;
    }

    public void setCompareEndDate(String compareEndDate){
        this.compareEndDate = compareEndDate;
    }

    public String getServerStartDate(){
        return serverStartDate;
    }

    public void setServerStartDate(String serverStartDate){
        this.serverStartDate = serverStartDate;
    }

    public String getServerEndDate(){
        return serverEndDate;
    }

    public void setServerEndDate(String serverEndDate){
        this.serverEndDate = serverEndDate;
    }

    public String getServerCompareStartDate(){
        return serverCompareStartDate;
    }

    public void setServerCompareStartDate(String serverCompareStartDate){
        this.serverCompareStartDate = serverCompareStartDate;
    }

    public String getServerCompareEndDate(){
        return serverCompareEndDate;
    }

    public void setServerCompareEndDate(String serverCompareEndDate){
        this.serverCompareEndDate = serverCompareEndDate;
    }

    public boolean isCompare(){
        return isCompare;
    }

    public void setCompare(boolean compare){
        isCompare = compare;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }

    /**
     * @param DateStr
     * @return For Week Tab
     */
    public String getDisplayStringForWeekTab(String DateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("MMM dd ,yyyy");
        try{
            Date inputDate = inputFormat.parse(DateStr);                 // parse input
            return outputFormat.format(inputDate);    // format output
        } catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onResume(){
        super.onResume();
        //mHomeActivity.tvTitle.setText("Custom");
    }

}
