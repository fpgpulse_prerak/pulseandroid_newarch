package com.ingauge.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.FilterDetailListAdapter;
import com.ingauge.listener.OnFilterDataSetListener;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.pojo.RegionByTenantRegionTypeModel;
import com.ingauge.pojo.RegionTypeDataModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mansurum on 24-May-17.
 */

public class FilterDetailListFragment extends BaseFragment implements RecyclerViewClickListener {
    private RecyclerView rvFilterDataList;
    private HomeActivity mHomeActivity;
    View rootView;
    FilterBaseData mFilterBaseData;
    List<FilterBaseData> mFilterBaseDatas;
    private int selectedItemPosition = -1;
    private OnFilterDataSetListener onFilterDataSetListener;
    RegionTypeDataModel mRegionTypeDataModel;
    RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel;
    AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel;
    DashboardLocationGroupListModel mDashboardLocationGroupListModel;
    ProductListFromLocationGroupModel mProductListFromLocationGroupModel;
    DashboardUserListModel mDashboardUserListModel;
    private SearchView searchView;
    FilterDetailListAdapter mFilterListAdapter;
    Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onAttachToParentFragment(getFragmentManager().findFragmentByTag(getString(R.string.tag_filter_list)));
        mFilterBaseDatas = new ArrayList<>();
    }

    public void onAttachToParentFragment(Fragment fragment) {
        try {
            onFilterDataSetListener = (OnFilterDataSetListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString() + " must implement OnFilterDataSetListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_detail_filter_list, container, false);
        initViews(rootView);
        if (getArguments() != null) {
            selectedItemPosition = getArguments().getInt(FilterListFragment.KEY_SELECTION_POSITION, selectedItemPosition);
            getFilterData(selectedItemPosition);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mFilterListAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return rootView;
    }


    void getFilterData(int position) {
        switch (position) {
            case 0:
                mRegionTypeDataModel = (RegionTypeDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                GetRegionTypeService(mRegionTypeDataModel);
                break;
            case 1:
                mRegionByTenantRegionTypeModel = (RegionByTenantRegionTypeModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getRegionByTenantRegionTypeByCountry(mRegionByTenantRegionTypeModel);
                break;
            case 2:
                mAccessibleTenantLocationDataModel = (AccessibleTenantLocationDataModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getAccessibleTenantLocation(mAccessibleTenantLocationDataModel);
                break;
            case 3:
                mDashboardLocationGroupListModel = (DashboardLocationGroupListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getDashboardLocationGroupList(mDashboardLocationGroupListModel);
                break;
            case 4:
                mProductListFromLocationGroupModel = (ProductListFromLocationGroupModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getProductListFromLocationGroup(mProductListFromLocationGroupModel);
                break;
            case 5:
                mDashboardUserListModel = (DashboardUserListModel) getArguments().getSerializable(FilterListFragment.KEY_FILTER_DETAILS);
                getDashboardUserList(mDashboardUserListModel);
                break;
            case 6:
                setMertricTypeData();
                break;
        }

    }

    private void setMertricTypeData() {
        if (mHomeActivity.mStringListMetricType != null) {
            for (String filterName : mHomeActivity.mStringListMetricType) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = "" + 000000;
                mFilterBaseData.name = filterName;
                //mFilterBaseData.filterName = getString(R.string.metric_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_metric_type));
                mFilterBaseData.filterIndex = 6;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas);
        }
    }

    private void initViews(View rootView) {
        rvFilterDataList = (RecyclerView) rootView.findViewById(R.id.fragment_detail_filter_list_recycleview);
        searchView = (SearchView) rootView.findViewById(R.id.fragment_detail_filter_list_search_view);
    }

    private void GetRegionTypeService(RegionTypeDataModel regionTypedata) {
        if (regionTypedata != null) {
            for (RegionTypeDataModel.Datum datum : regionTypedata.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(datum.getId());
                mFilterBaseData.name = datum.getName();
                //mFilterBaseData.filterName = getString(R.string.region_type);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region_type));
                mFilterBaseData.filterIndex = 0;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas);
        }

    }

    private void getRegionByTenantRegionTypeByCountry(RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel) {
        if (mRegionByTenantRegionTypeModel != null) {
            for (RegionByTenantRegionTypeModel.Datum mDatum : mRegionByTenantRegionTypeModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                //mFilterBaseData.filterName = getString(R.string.region);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_region));
                mFilterBaseData.filterIndex = 1;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas);
        }

    }

    private void getRegionByTenantRegionTypeByRegion(RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel) {
        for (RegionByTenantRegionTypeModel.Datum mDatum : mRegionByTenantRegionTypeModel.getData()) {
            FilterBaseData mFilterBaseData = new FilterBaseData();
            mFilterBaseData.id = String.valueOf(mDatum.getId());
            mFilterBaseData.name = mDatum.getName();
            mFilterBaseData.filterName = getString(R.string.region);
            mFilterBaseData.filterIndex = 1;
            mFilterBaseDatas.add(mFilterBaseData);
        }
        setFilterdataListAdapter(mFilterBaseDatas);
    }

    private void getAccessibleTenantLocation(AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel) {
        if (mAccessibleTenantLocationDataModel != null) {
            for (AccessibleTenantLocationDataModel.Datum mDatum : mAccessibleTenantLocationDataModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                mFilterBaseData.filterMetricType = mDatum.getHotelMetricsDataType();
                //mFilterBaseData.filterName = getString(R.string.location);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location));
                mFilterBaseData.filterIndex = 2;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas);
        }
    }

    private void getDashboardLocationGroupList(DashboardLocationGroupListModel mGetAccessibleTenantLocationData) {
        if (mGetAccessibleTenantLocationData != null) {
            for (DashboardLocationGroupListModel.Datum mDatum : mGetAccessibleTenantLocationData.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getLocationGroupID());
                mFilterBaseData.name = mDatum.getLocationGroupName();
                //mFilterBaseData.filterName = getString(R.string.location_group);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_location_group));
                mFilterBaseData.filterIndex = 3;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas);
        }
    }

    private void getProductListFromLocationGroup(ProductListFromLocationGroupModel mProductListFromLocationGroupModel) {
        if (mProductListFromLocationGroupModel != null) {
            for (ProductListFromLocationGroupModel.Datum mDatum : mProductListFromLocationGroupModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                //mFilterBaseData.filterName = getString(R.string.product);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_product));
                mFilterBaseData.filterIndex = 4;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas);
        }
    }

    private void getDashboardUserList(DashboardUserListModel mDashboardUserListModel) {
        if (mDashboardUserListModel != null) {
            for (DashboardUserListModel.Datum mDatum : mDashboardUserListModel.getData()) {
                FilterBaseData mFilterBaseData = new FilterBaseData();
                mFilterBaseData.id = String.valueOf(mDatum.getId());
                mFilterBaseData.name = mDatum.getName();
                //mFilterBaseData.filterName = getString(R.string.user);
                mFilterBaseData.filterName = mHomeActivity.mobilekeyObject.optString(mContext.getResources().getString(R.string.mobile_key_filter_user));
                mFilterBaseData.filterIndex = 5;
                mFilterBaseDatas.add(mFilterBaseData);
            }
            setFilterdataListAdapter(mFilterBaseDatas);
        }
    }

    private void setFilterdataListAdapter(List<FilterBaseData> mFilterBaseDataList) {
        if (mFilterBaseDataList != null) {
            if (mFilterBaseDataList.size() > 0) {
                Collections.sort(mFilterBaseDataList, new Comparator<FilterBaseData>() {
                    public int compare(FilterBaseData v1, FilterBaseData v2) {
                        return v1.name.compareTo(v2.name);
                    }
                });
                mFilterListAdapter = new FilterDetailListAdapter(mHomeActivity, mFilterBaseDataList, this, true, "", "");
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InGaugeApp.getInstance());
                rvFilterDataList.setLayoutManager(mLayoutManager);
                rvFilterDataList.setItemAnimator(new DefaultItemAnimator());
                rvFilterDataList.setAdapter(mFilterListAdapter);
            }
        }
    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterPosition, boolean isLeftFilter) {
        if (onFilterDataSetListener != null) {
            onFilterDataSetListener.onFilterDetailsClicked(mFilterBaseData, position, selectedItemPosition);
            mHomeActivity.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.toolbar.setClickable(false);
    }
}
