package com.ingauge.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.adapters.CustomListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.DashboardFragmentNew;
import com.ingauge.fragments.FeedsFragment;
import com.ingauge.fragments.MoreFragment;
import com.ingauge.fragments.PreferenceDynamicFragment;
import com.ingauge.fragments.VideoFragment;
import com.ingauge.fragments.agentprofile.AgentProfileFragment;
import com.ingauge.fragments.dateselection.DateSelectionFragment;
import com.ingauge.fragments.dateselection.DateSelectionFragmentForObsCC;
import com.ingauge.listener.RecyclerViewClickListener;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.AccessibleTenantLocationDataModelForAgentProfile;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardModelList;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.DynamicData;
import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.FilterBaseDataForLocationGroup;
import com.ingauge.pojo.FilterMetricTypeModel;
import com.ingauge.pojo.ObsQuestionModelLocal;
import com.ingauge.pojo.ObservationEditViewModel;
import com.ingauge.pojo.ObservationJobType;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.pojo.RegionByTenantRegionTypeModel;
import com.ingauge.pojo.RegionTypeDataModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.BottomNavigationViewHelper;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/*import com.ingauge.fragments.ObservationDashboardFilterFragment;*/

//import com.ingauge.fragments.DashboardPanelFragment;

public class HomeActivity extends BaseActivity implements View.OnClickListener, RecyclerViewClickListener{
    private static final String SELECTED_ITEM = "arg_selected_item";
    public BottomNavigationView nBottomNavigationView;
    private int mSelectedItem;
    private MenuItem selectedItem;
    public Toolbar toolbar;
    public TextView tvTitle;
    private TextView tvDashboardTitle;
    public TextView tvNoDashboard;
    public TextView tvApply;
    public TextView tvSubTitle;
    public AppCompatImageButton ibMenu;
    public ImageButton ibAttachment;
    public TextView tvPreferences;
    public ImageButton ibBack;
    public ProgressBar mProgressBarWhite;
    public Activity mHomeActivity;
    private PopupMenu popupMenu;
    private FrameLayout mFrameLayout;
    private RecyclerView rvDashboardList;
    private boolean isClick = false;
    public List<DashboardModelList.Datum> list;
    private APIInterface apiInterface;

    public ExpandableListView mDrawerList;
    public DrawerLayout mDrawerLayout;
    public RegionTypeDataModel mRegionTypeDataModel;
    public RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModel;
    public AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModel;
    public DashboardLocationGroupListModel mDashboardLocationGroupListModel;
    public ProductListFromLocationGroupModel mProductListFromLocationGroupModel;
    public DashboardUserListModel mDashboardUserListModel;


    public RegionTypeDataModel mRegionTypeDataModelForLeaderBoard;
    public RegionByTenantRegionTypeModel mRegionByTenantRegionTypeModelForLeaderBoard;
    public AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModelForLeaderBoard;
    public DashboardLocationGroupListModel mDashboardLocationGroupListModelForLeaderBoard;
    public ProductListFromLocationGroupModel mProductListFromLocationGroupModelForLeaderBoard;
    public DashboardUserListModel mDashboardUserListModelForLeaderBoard;


    public List<String> mStringListMetricType = new ArrayList<>();
    public List<String> mStringKeyListMetricType = new ArrayList<>();

    //For Goal
    public AccessibleTenantLocationDataModel mAccessibleTenantLocationDataModelForGoal;
    public DashboardLocationGroupListModel mDashboardLocationGroupListModelForGoal;
    public ProductListFromLocationGroupModel mProductListFromLocationGroupModelForGoal;
    public DashboardUserListModel mDashboardUserListModelForGoal;
    //For Goal
    public static boolean isdashboardLoaded = false;

    public JSONObject mobilekeyObject = null;


    public JSONObject rolesAndPermissoinObject = null;
    public String KEY_CUSTOM_REPORT_ID = "custom_reportId";
    public CustomListAdapter customListAdapter;


    public boolean isDashboardUpdate = false;


    public boolean isDashboardUpdateAfterApply = false;

    public boolean isDashboardObsCoachingUpdateAfterApply = false;
    List<DynamicData> mCountryList;
    JSONObject mDataJson = null;


    public Map<Integer, List<DynamicData>> integerListHashMap;
    public boolean isRegionFilter = false;
    public boolean isTenantLocation = false;
    public boolean isCustomGoalFilter = false;
    public boolean isProduct = false;
    public boolean isLocationGroup = false;
    public boolean isUser = false;


    public boolean isRegionFilterForLb = false;
    public boolean isTenantLocationForLb = false;
    public boolean isCustomGoalFilterForLb = false;
    public boolean isProductForLb = false;
    public boolean isLocationGroupForLb = false;
    public boolean isUserForLb = false;


    public List<FilterMetricTypeModel> mFilterMetricTypeModelList = new ArrayList<>();
    public ArrayList<FilterBaseData> mMainLeftFilterBaseDatas = new ArrayList<>();

    public ArrayList<FilterBaseData> mPrerferencedLeftFilterBaseDatas = new ArrayList<>();

    public List<FilterBaseData> mLocationFilterData = new ArrayList<>();
    public List<FilterBaseData> mUserFilterData = new ArrayList<>();

    public List<FilterBaseDataForLocationGroup> mLocationGroupForLGFilterData = new ArrayList<>();
    // public List<FilterBaseDataForLocationGroup> mLocationForLGFilterData = new ArrayList<>();
    public static Bitmap mBitmap;
    public boolean isNeedCallForChildSocialInteraction = true;


    public String getSelectedRegionName(){
        return selectedRegionName;
    }

    public void setSelectedRegionName(String selectedRegionName){
        this.selectedRegionName = selectedRegionName;
    }

    public String selectedRegionName = "";
    //public boolean isMtd=false;

    /**
     * 0 = month
     * 1 = year
     */
    public int monthOrYear = -1;

    // public boolean isNoNeedUpdateMetric=true;

//    In Region

    public int selectedRegionTypeForLb;
    public int selectedRegionIdForLb;


    public String selectedRegionTypeNameForLb = "";
    public String selectedRegionNameForLb = "";


//   In Accessible Tenant Location

    public int selectedTenantLocationIdForLb = -2;
    public String selectedTenantLocationNameForLb = "";
    public String selectedHotelMetricTypeNameLb = "";


//    In Location Group

    public int selectedLocationGroupIdForLb;
    public String selectedLocationGroupNameLb = "";

//    In Product API

    public int selectedProductIdForLb;
    public String selectedProductNameLb = "";

//    In User API

    public int selectedUserIdForLB;
    public String selectedUserNameForLB = "";


    //    In Metric Type

    public int selectedMetricTypeIdForLb;
    public String selectedMetricTypeNameForLb = "";

    public ArrayList<FilterBaseData> mLeftFilterBaseDatasForLb;


    public boolean isLeaderNeedUpdate = false;


    /**
     * 0 = Day
     * 1 = Week
     * 2 = Month
     * 3 = Custom
     */
    public int tabIndex = -1;

    public int dateRangeIndex = -1;
    public int ComparedateRangeIndex = -1;


    ///Observation Coaching MModule Questionnaire Module

    // public TextView tvAgentScore;
    public String agentScore = "0.0f";
    public TextView tvDone;
    public ObservationJobType mObservationJobType;
    public String areaOfImprovment = "";
    public boolean isFromDashboard = false;
    public boolean isPending = false;
    public boolean isOnlyView = false;
    public ObservationEditViewModel observationEditViewModel;
    public ArrayList<ObsQuestionModelLocal> arrayObsQuestion;
    public boolean donotMailToAgent = false;
    public boolean donotDisplayToAgent = false;
    public String eventID = "";

    /**
     *  Declare Vairable for Agent Profile
     */

    // Start of Agent Profile Variables Declaration

    public String userIdForAPI = "";
    public String userSelectedName = "";
    public Integer tenantLocationId ;
    public String tenantLocationLocationName=""; ;
    public String userRoleName="";
    public boolean isLocationAvailable=false;
    public ArrayList<FilterBaseData> mFilterBaseDatas = new ArrayList<>();
    public AccessibleTenantLocationDataModelForAgentProfile mAccessibleTenantLocationDataModelforAgentProfile = new AccessibleTenantLocationDataModelForAgentProfile();
    public DashboardUserListModel mDashboardUserListModelForAgentProfile = new DashboardUserListModel();
    public boolean isUserOperator=false;
    public boolean isNeedAgentProfileUpdate=false;



    public boolean isNeedAgentProfileUpdateForUserProfile=false;
    // End of Agent Profile Variables Declaration



    /***
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        Logger.Error("<<<< Tenant Role Name  " + InGaugeSession.read(getString(R.string.key_login_tenant_user_role), ""));
        mCountryList = new ArrayList<>();
        initViews();
        try{
            apiInterface = APIClient.getClient().create(APIInterface.class);
            mHomeActivity = HomeActivity.this;
            mobilekeyObject = UiUtils.getMobileKeys(mHomeActivity);
            //mStringListMetricType = Arrays.asList(getResources().getStringArray(R.array.metric_type_array));
            mStringKeyListMetricType = Arrays.asList(getResources().getStringArray(R.array.key_metric_type_array));
            // mStringListMetricType.clear();
            for(int i = 0; i < mStringKeyListMetricType.size(); i++){

                mStringListMetricType.add(mobilekeyObject.optString(mStringKeyListMetricType.get(i)));
            }
            for(int i = 0; i < mStringListMetricType.size(); i++){
                FilterMetricTypeModel metricTypeModel = new FilterMetricTypeModel();
                metricTypeModel.id = "000" + String.valueOf(i);
                metricTypeModel.name = mStringListMetricType.get(i);
                metricTypeModel.filterName = mobilekeyObject.optString(getResources().getString(R.string.mobile_key_filter_metric_type));
                mFilterMetricTypeModelList.add(metricTypeModel);
            }
            rolesAndPermissoinObject = new JSONObject(InGaugeSession.read(getResources().getString(R.string.key_user_role_permission), null));
            selectedItem = nBottomNavigationView.getMenu().getItem(0);
            tvNoDashboard = (TextView) findViewById(R.id.tv_no_data);
            mProgressBarWhite = (ProgressBar) findViewById(R.id.custom_toolbar_pb);
            //GetDashboard(InGaugeSession.read(getString(R.string.key_auth_token), ""), true, false);
            // getDashboardList();
            if(savedInstanceState != null){
                mSelectedItem = savedInstanceState.getInt(SELECTED_ITEM, 0);
                selectedItem = nBottomNavigationView.getMenu().findItem(mSelectedItem);
            } else{
                selectedItem = nBottomNavigationView.getMenu().getItem(0);
            }

            //canAccessFeed : true
            if(!rolesAndPermissoinObject.optBoolean("canAccessFeed")){
                nBottomNavigationView.getMenu().removeItem(R.id.menu_feeds);
            }
            if(!rolesAndPermissoinObject.optBoolean("canAccessVideoLib")){
                nBottomNavigationView.getMenu().removeItem(R.id.menu_video);
            }

            String roleName = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_login_tenant_user_role), "");
            if(roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_super_admin))){
                // Non Agent Login
                selectFragment ( selectedItem );
            } else if(roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_performance_manager))){
                //Non Agent Login
                selectFragment ( selectedItem );
            } else if(roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_frontline_associate))){
                //Agent Login
                AgentProfileFragment mAgentProfileFragment = new AgentProfileFragment();
                replace(mAgentProfileFragment, getResources().getString(R.string.tag_user_agent_profile_from_dashboard));
            }else{
                selectFragment ( selectedItem );
            }

        } catch(Exception e){
            e.printStackTrace();

        }
    }

    /*void GetDashboard(String authToken, final boolean isFirst, final boolean isFromNavigation) {
        Call mCall = apiInterface.getDashboardList(InGaugeSession.read(getString(R.string.key_selected_tenant_id), 0), InGaugeSession.read(getString(R.string.key_user_id), 0), InGaugeSession.read(getString(R.string.key_user_role_id), 0), false);
        //Call mCall = apiInterface.getDashboardList("application/json", "bearer" + " " + authToken, "1", "1", 1, 646, false);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null) {
                    DashboardModelList madDashboardModelList = (DashboardModelList) response.body();
                    if (madDashboardModelList != null) {
                        list = madDashboardModelList.getData();
                        try {
                            if (list != null && list.size() > 0) {
                                setDashboardList(list, isFirst);
                                if (!isFromNavigation) {
                                    tvNoDashboard.setVisibility(View.GONE);
                                    if (list.size() > 0) {
                                        InGaugeSession.write(getString(R.string.key_selected_dashboard), list.get(0).getName());
                                        InGaugeSession.write(getString(R.string.key_selected_dashboard_id), list.get(0).getId());
                                    } else {
                                        rvDashboardList.setVisibility(View.GONE);
                                        tvNoDashboard.setVisibility(View.VISIBLE);
                                    }

                                }

                            } else {
                                list.clear();
                                // tvSubTitle.setVisibility(View.GONE);
                                mDrawerList.setVisibility(View.GONE);
                                tvNoDashboard.setVisibility(View.VISIBLE);
                                InGaugeSession.write(getString(R.string.key_selected_dashboard), "");
                                InGaugeSession.write(getString(R.string.key_selected_dashboard_id), -1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                        isdashboardLoaded = true;
                    } else {
                    }
                } else if (response.raw() != null) {
                    if (response.raw().code() == 401) {
                        endProgress();
                        ForceLogout();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }*/

    private void initViews(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ibBack = (ImageButton) findViewById(R.id.custom_tool_bar_ib_back);
        ibBack.setOnClickListener(this);
        mFrameLayout = (FrameLayout) findViewById(R.id.container);
        setSupportActionBar(toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tv_title);
        tvApply = (TextView) toolbar.findViewById(R.id.tv_apply);
        tvSubTitle = (TextView) toolbar.findViewById(R.id.tv_sub_title);
        ibMenu = (AppCompatImageButton) toolbar.findViewById(R.id.ib_menu);
        ibAttachment = (ImageButton) toolbar.findViewById(R.id.custom_toolbar_ib_attachement);
        tvPreferences = (TextView) toolbar.findViewById(R.id.custom_tool_bar_preferences);
        ibAttachment.setOnClickListener(this);
        ibMenu.setOnClickListener(this);
        tvPreferences.setOnClickListener(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        rvDashboardList = (RecyclerView) findViewById(R.id.rv_dashboard);
        rvDashboardList.setVisibility(View.GONE);
        mDrawerList = (ExpandableListView) findViewById(R.id.left_drawer);
        tvTitle.setText(getResources().getString(R.string.dashboard_title));
        nBottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(nBottomNavigationView);
        nBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item){
                try{
                    //GetDashboard(InGaugeSession.read(getString(R.string.key_auth_token), ""), true, true);
                    if(nBottomNavigationView.getSelectedItemId() != item.getItemId()){
                        selectFragment(item);
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        outState.putInt(SELECTED_ITEM, mSelectedItem);
        //super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed(){

        final PreferenceDynamicFragment fragment = (PreferenceDynamicFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_preferences_screen));
        final AgentProfileFragment mAgentProfileFragment = (AgentProfileFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_user_agent_profile_from_dashboard));


        if(fragment != null && fragment.isFromTenantDetail){
            MenuItem homeItem = nBottomNavigationView.getMenu().getItem(0);
            if(mSelectedItem != homeItem.getItemId()){
                // select home item9
                FragmentManager fm = getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i){
                    fm.popBackStack();
                }
                list.clear();
                //GetDashboard(InGaugeSession.read(getString(R.string.key_auth_token), ""), false, false);
                //setDashboardUpdate(true);
                selectFragment(homeItem);
            } else{
                // setDashboardUpdate(true);
                super.onBackPressed();
            }
        } else if(fragment != null && !fragment.isFromTenantDetail){
            super.onBackPressed();
            if(list != null && list.size() > 0){
                list.clear();
            }
            //GetDashboard(InGaugeSession.read(getString(R.string.key_auth_token), ""), false, false);
        } else if(mAgentProfileFragment != null && mAgentProfileFragment.isAgentProfileFragment){
            MenuItem homeItem = nBottomNavigationView.getMenu().getItem(0);
            if(mSelectedItem != homeItem.getItemId()){
                // select home item
                FragmentManager fm = getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i){
                    fm.popBackStack();
                }
                if(list != null)
                    list.clear();
                //GetDashboard(InGaugeSession.read(getString(R.string.key_auth_token), ""), false, false);
                //setDashboardUpdate(true);
                selectFragment(homeItem);
            } else{
                // setDashboardUpdate(true);
                super.onBackPressed();
            }
        } else if(mAgentProfileFragment != null && !mAgentProfileFragment.isAgentProfileFragment){
            super.onBackPressed();
            if(list != null && list.size() > 0){
                list.clear();
            }
        } else{
            final DateSelectionFragment mDateFilterFragment = (DateSelectionFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_fragment_dashboard_date_filter));
            final DateSelectionFragmentForObsCC mDateFilterFragmentForObsCC = (DateSelectionFragmentForObsCC) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_fragment_dashboard_date_filter_obs_coaching));

            if(mDateFilterFragment instanceof DateSelectionFragment){
                // setDashboardUpdateAfterApply ( false );
            }
            if(mDateFilterFragmentForObsCC instanceof DateSelectionFragmentForObsCC){
                // setDashboardUpdateAfterApply ( false );
            }


            super.onBackPressed();
        }
    }

    private void selectFragment(MenuItem item){
        Fragment frag = null;
        // init corresponding fragment
        switch(item.getItemId()){
            case R.id.menu_dashboard:
                frag = DashboardFragmentNew.newInstance("", 0);
                //TODO This static 0 is for to get the first index of dashboard from response
                if(list != null && list.size() > 0){
                    tvSubTitle.setVisibility(View.VISIBLE);
                    if(InGaugeSession.read(HomeActivity.this.getResources().getString(R.string.key_selected_dashboard), "").equalsIgnoreCase("")){
                        tvSubTitle.setText("(" + list.get(0).getName() + ")");
                        InGaugeSession.write(getString(R.string.key_selected_dashboard), list.get(0).getName());
                    } else{
                        tvSubTitle.setText("(" + InGaugeSession.read(HomeActivity.this.getResources().getString(R.string.key_selected_dashboard), "") + ")");
                    }
                } else{
                    tvSubTitle.setVisibility(View.GONE);
                }
//                TODO This static 0 is for to get the first index of dashboard from response

                //tvTitle.setText(getString(R.string.menu_dashboard));
                ibMenu.setVisibility(View.VISIBLE);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                break;
            case R.id.menu_feeds:
                tvTitle.setText(getString(R.string.menu_feeds));
                tvSubTitle.setVisibility(View.VISIBLE);
                tvSubTitle.setText(InGaugeSession.read(getString(R.string.key_selected_tenant), ""));
                frag = FeedsFragment.newInstance("", 0);
                ibMenu.setVisibility(View.GONE);
                rvDashboardList.setVisibility(View.GONE);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
            case R.id.menu_video:
                frag = VideoFragment.newInstance("", 0);
                tvSubTitle.setVisibility(View.GONE);
                ibMenu.setVisibility(View.GONE);
                tvTitle.setText(getString(R.string.menu_video));
                rvDashboardList.setVisibility(View.GONE);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
            case R.id.menu_more:
                tvSubTitle.setVisibility(View.GONE);
                ibMenu.setVisibility(View.GONE);
                frag = MoreFragment.newInstance("", 0);
                tvTitle.setText(getString(R.string.menu_more));
                rvDashboardList.setVisibility(View.GONE);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
        }

        // update selected item
        mSelectedItem = item.getItemId();
        // uncheck the other items.
        for(int i = 0; i < nBottomNavigationView.getMenu().size(); i++){
            MenuItem menuItem = nBottomNavigationView.getMenu().getItem(i);
            if(menuItem.getItemId() == item.getItemId())
                menuItem.setChecked(true);
        }
        if(frag != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            ft.replace(R.id.container, frag, frag.getTag());
            ft.commit();
        }
    }


    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.custom_tool_bar_preferences:


                if(DashboardFragmentNew.rlProgress != null && DashboardFragmentNew.imgFilter != null){
                    if(DashboardFragmentNew.rlProgress.getVisibility() != View.VISIBLE){
                        if(DashboardFragmentNew.imgFilter.getVisibility() == View.VISIBLE){
                            PreferenceDynamicFragment fragment = new PreferenceDynamicFragment();
                            replace(fragment, getResources().getString(R.string.tag_preferences_screen));
                        }
                    }
                } else{
                    PreferenceDynamicFragment fragment = new PreferenceDynamicFragment();
                    replace(fragment, getResources().getString(R.string.tag_preferences_screen));

                }


                break;
            case R.id.custom_tool_bar_ib_back:
                onBackPressed();
                break;
        }
    }

    public void OnMenuClick(){
        mDrawerLayout.openDrawer(Gravity.RIGHT);

    }


    public void replace(Fragment fragment, String tag){
        if(fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.addToBackStack(tag);
            Logger.Error("TAG  " + tag);
            ft.replace(R.id.container, fragment, tag);
            ft.commit();
        }
    }

    public void replacewithoutAddtoStack(Fragment fragment){
        if(fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragment, null);
            ft.commit();
        }
    }

    public void add(Fragment fragment, String tag){
        if(fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.addToBackStack(tag);
            Logger.Error("TAG  " + tag);
            ft.add(R.id.container, fragment, tag);
            ft.commit();
        }
    }

    public void visibleBottomView(){
        nBottomNavigationView.setVisibility(View.VISIBLE);
        selectedItem = nBottomNavigationView.getMenu().getItem(1);
    }

    public DashboardModelList.Datum checkSubMenuData(final List<DashboardModelList.Datum> mDatumList, int dashboardId){

        for(int i = 0; i < mDatumList.size(); i++){

            /*if(mDatumList.get ( i ).getShowInMenu()){*/
            if(dashboardId == mDatumList.get(i).getId()){
                /*DashboardModelList.Datum  mDatum= mDatumList.get ( i );
                for(int j=0;j<mDatumList.get ( i ).getDashboardLinks ().size ();j++){
                    if(mDatum.getDashboardLinks ().get ( j ).getDashboardLinkId ()==mDatumList.get ( i ).getDashboardLinks ().get ( j ).getDashboardLinkId ()){
                        DashboardModelList.Datum.DashboardLink mDashboardLink = mDatumList.get ( i ).getDashboardLinks ().get ( j );
                        Logger.Error ( "<<<<<Link Dashboard Name "  + mDashboardLink.getLinkText ());
                        mDatum.setName (  mDashboardLink.getLinkText ());
                        return mDatum;
                    }
                }*/
                return mDatumList.get(i);
            }
            /*}*/


        }

        return null;
    }

    public void setDashboardList(final List<DashboardModelList.Datum> mylist, boolean isFirst){
        try{
            rolesAndPermissoinObject = new JSONObject(InGaugeSession.read(getResources().getString(R.string.key_user_role_permission), null));
        } catch(JSONException e){
            e.printStackTrace();
        }
        List<DashboardModelList.Datum> listType1 = new ArrayList<>();
        List<DashboardModelList.Datum> listType2 = new ArrayList<>();
        List<DashboardModelList.Datum> listType3 = new ArrayList<>();

        for(int i = 0; i < mylist.size(); i++){
            if(mylist.get(i).getDashboardType() == 1){
                if(mylist.get(i).getShowInMenu()){
                    if(mylist.get(i).getmDatumList() == null){
                        mylist.get(i).setmDatumList(new ArrayList<DashboardModelList.Datum>());

                    }
                    if(mylist.get(i).getmLinedDashboardMaps() != null && mylist.get(i).getmLinedDashboardMaps().size() > 0){
                        for(int dashboardIndex : mylist.get(i).getmLinedDashboardMaps().keySet()){
                            DashboardModelList.Datum mDatum = checkSubMenuData(mylist, dashboardIndex);

                            Logger.Error("<<< Dashboard Name " + mDatum.getTitle());
                            Logger.Error("<<< Dashboard Name " + mylist.get(i).getmLinedDashboardMaps().get(dashboardIndex));
                            mDatum.setTitle(mylist.get(i).getmLinedDashboardMaps().get(dashboardIndex));
                            mylist.get(i).getmDatumList().add(mDatum);
                        }
                        listType1.add(mylist.get(i));
                    } else{
                        listType1.add(mylist.get(i));
                    }


                }
            } else if(mylist.get(i).getDashboardType() == 2){

                if(mylist.get(i).getShowInMenu()){
                    if(mylist.get(i).getmDatumList() == null){
                        mylist.get(i).setmDatumList(new ArrayList<DashboardModelList.Datum>());
                    }
                    if(mylist.get(i).getmLinedDashboardMaps() != null && mylist.get(i).getmLinedDashboardMaps().size() > 0){
                        for(int dashboardIndex : mylist.get(i).getmLinedDashboardMaps().keySet()){
                            DashboardModelList.Datum mDatum = checkSubMenuData(mylist, dashboardIndex);

                            mDatum.setTitle(mylist.get(i).getmLinedDashboardMaps().get(dashboardIndex));

                            Logger.Error("<<< Dashboard Name " + mDatum.getTitle());
                            Logger.Error("<<< Dashboard Name " + mylist.get(i).getmLinedDashboardMaps().get(dashboardIndex));
                            mylist.get(i).getmDatumList().add(mDatum);
                        }
                        listType2.add(mylist.get(i));
                    } else{
                        listType2.add(mylist.get(i));
                    }
                }

            } else if(mylist.get(i).getDashboardType() == 3){
                if(mylist.get(i).getShowInMenu()){
                    if(mylist.get(i).getmDatumList() == null){
                        mylist.get(i).setmDatumList(new ArrayList<DashboardModelList.Datum>());

                    }
                    if(mylist.get(i).getmLinedDashboardMaps() != null && mylist.get(i).getmLinedDashboardMaps().size() > 0){
                        for(int dashboardIndex : mylist.get(i).getmLinedDashboardMaps().keySet()){
                            DashboardModelList.Datum mDatum = checkSubMenuData(mylist, dashboardIndex);
                            mDatum.setTitle(mylist.get(i).getmLinedDashboardMaps().get(dashboardIndex));

                            Logger.Error("<<< Dashboard Name " + mDatum.getTitle());
                            Logger.Error("<<< Dashboard Name " + mylist.get(i).getmLinedDashboardMaps().get(dashboardIndex));
                            mylist.get(i).getmDatumList().add(mDatum);
                        }
                        listType3.add(mylist.get(i));
                    } else{
                        listType3.add(mylist.get(i));
                    }
                }
            }
        }
        mylist.clear();

        if(rolesAndPermissoinObject.optBoolean("canAccessHowAmIDoing")){
            mylist.addAll(listType2);
        }
        if(rolesAndPermissoinObject.optBoolean("canAccessHowDoIImprove")){
            mylist.addAll(listType3);
        }
        if(rolesAndPermissoinObject.optBoolean("canAccessDashboards")){
            mylist.addAll(listType1);
        }
        /*for(int i=0;i<mylist.size();i++) {
              Logger.Error("DASHBORAD_TYPE"+mylist.get(i).getDashboardType());
        }*/
        for(int i = 0; i < mylist.size(); i++){
            if(i == 0){
                mylist.get(0).setSectionHeaderToShow(true);
            } else{
                if(mylist.get(i).getDashboardType() != mylist.get(i - 1).getDashboardType()){
                    mylist.get(i).setSectionHeaderToShow(true);
                } else{
                    mylist.get(i).setSectionHeaderToShow(false);
                }
            }
        }

        if(mylist.size() > 0){
            customListAdapter = new CustomListAdapter(HomeActivity.this, mylist);
            mDrawerList.setAdapter(customListAdapter);
        } else{
            mDrawerList.setVisibility(View.GONE);
            tvNoDashboard.setVisibility(View.VISIBLE);
        }

        mDrawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener(){
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id){

                Logger.Error("<<<< Click on Group>>>>");
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
                SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                Date date = null;

                Date firstDateOfPreviousMonth = null;
                Date prevDate = null;


                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                date = calendar.getTime();


                Calendar cal = Calendar.getInstance();

                cal.setTime(new Date());
                cal.add(Calendar.MONTH, -1);
                cal.set(Calendar.DATE, 1);
                firstDateOfPreviousMonth = cal.getTime();


                Calendar previouslastDate = Calendar.getInstance();
                previouslastDate.setTime(new Date());
                int daysInMonth = previouslastDate.getActualMaximum(Calendar.DAY_OF_MONTH);
                if(daysInMonth == 31){
                    daysInMonth = daysInMonth - 1;
                } else{
                    daysInMonth = daysInMonth + 1;
                }

                Logger.Error("Days  " + daysInMonth);
                previouslastDate.add(Calendar.DATE, -daysInMonth);  //not sure
                prevDate = previouslastDate.getTime();


                tvSubTitle.setText(mylist.get(groupPosition).getName() + "-" + InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));

                Logger.Error("<<<< Call >>>>");
                //setmMainLeftFilterBaseDatas(new ArrayList<FilterBaseData>());


                Gson gson = new Gson();
                String json = gson.toJson(new ArrayList<FilterBaseData>());
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_filter_obj), json);
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_dashboard), mylist.get(groupPosition).getName());
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_dashboard_id), mylist.get(groupPosition).getId());
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_dashboard_mtd), mylist.get(groupPosition).isMTD());
                //InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), mylist.get(groupPosition).isHideCompareDate());

                // This is Change for IN-830 Jira to pass the satatic false because to hide the compare date
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), true);

                //InGaugeSession.write(getResources().getString(R.string.key_is_compare_on), !mylist.get(groupPosition).isHideCompareDate());
                //This change is for IN-830 to disable compare feature in date Selection
                InGaugeSession.write(getResources().getString(R.string.key_is_compare_on), false);
                try{
                    InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_metric_type_name), mFilterMetricTypeModelList.get(1).name);
                } catch(Exception e){
                    e.printStackTrace();
                }
                if(getMonthOrYear() == 0){
                    if(mylist.get(groupPosition).isMTD()){
                        Logger.Error("<<<< No Change SAme For MTD >>>>>");
                    } else{
                        setMonthOrYear(1);
                        //   InGaugeSession.write ( getResources ().getString ( R.string.key_is_compare_on ), true );
                        InGaugeSession.write(getResources().getString(R.string.key_start_date), sdf.format(date));
                        InGaugeSession.write(getResources().getString(R.string.key_start_date_server), sdfServer.format(date));
                        InGaugeSession.write(getResources().getString(R.string.key_end_date), sdf.format(new Date()));
                        InGaugeSession.write(getResources().getString(R.string.key_end_date_server), sdfServer.format(new Date()));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_start_date), sdf.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_end_date), sdf.format(prevDate));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(prevDate));
                    }
                } else if(getMonthOrYear() == 1){
                    if(mylist.get(groupPosition).isMTD()){
                        setMonthOrYear(1);
                        //  InGaugeSession.write ( getResources ().getString ( R.string.key_is_compare_on ), true );
                        InGaugeSession.write(getResources().getString(R.string.key_start_date), sdf.format(date));
                        InGaugeSession.write(getResources().getString(R.string.key_start_date_server), sdfServer.format(date));
                        InGaugeSession.write(getResources().getString(R.string.key_end_date), sdf.format(new Date()));
                        InGaugeSession.write(getResources().getString(R.string.key_end_date_server), sdfServer.format(new Date()));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_start_date), sdf.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_end_date), sdf.format(prevDate));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(prevDate));
                    } else{
                        Logger.Error("<<<< No Change SAme For YTD >>>>>");
                    }
                }

                Bundle bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard", bundle);
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                customListAdapter.notifyDataSetChanged();
                if(getDashboardRefreshListener() != null){
                    getDashboardRefreshListener().onRefresh();
                }
                return true;
            }
        });
        mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener(){
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int position, int childPosition, long id){
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ,yyyy", Locale.US);
                SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                Date date = null;

                Date firstDateOfPreviousMonth = null;
                Date prevDate = null;


                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                date = calendar.getTime();


                Calendar cal = Calendar.getInstance();

                cal.setTime(new Date());
                cal.add(Calendar.MONTH, -1);
                cal.set(Calendar.DATE, 1);
                firstDateOfPreviousMonth = cal.getTime();


                Calendar previouslastDate = Calendar.getInstance();
                previouslastDate.setTime(new Date());
                int daysInMonth = previouslastDate.getActualMaximum(Calendar.DAY_OF_MONTH);
                if(daysInMonth == 31){
                    daysInMonth = daysInMonth - 1;
                } else{
                    daysInMonth = daysInMonth + 1;
                }

                Logger.Error("Days  " + daysInMonth);
                previouslastDate.add(Calendar.DATE, -daysInMonth);  //not sure
                prevDate = previouslastDate.getTime();


                tvSubTitle.setText(mylist.get(position).getmDatumList().get(childPosition).getName() + "-" + InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));


                //setmMainLeftFilterBaseDatas(new ArrayList<FilterBaseData>());


                Gson gson = new Gson();
                String json = gson.toJson(new ArrayList<FilterBaseData>());

                Logger.Error("<<<< Dashboard ID : " + mylist.get(position).getmDatumList().get(childPosition).getId());
                InGaugeSession.write(getResources().getString(R.string.key_filter_obj), json);
                InGaugeSession.write(getResources().getString(R.string.key_selected_dashboard), mylist.get(position).getmDatumList().get(childPosition).getTitle());
                InGaugeSession.write(getResources().getString(R.string.key_selected_dashboard_id), mylist.get(position).getmDatumList().get(childPosition).getId());
                InGaugeSession.write(getResources().getString(R.string.key_selected_dashboard_mtd), mylist.get(position).getmDatumList().get(childPosition).isMTD());


//                InGaugeSession.write(getResources().getString(R.string.key_selected_dashboard_hide_compare_date), mylist.get(position).getmDatumList().get(childPosition).isHideCompareDate());

                // This is Change for IN-830 Jira to pass the satatic false because to hide the compare date
                InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_selected_dashboard_hide_compare_date), true);
//                InGaugeSession.write(getResources().getString(R.string.key_is_compare_on), !mylist.get(position).getmDatumList().get(childPosition).isHideCompareDate());
                InGaugeSession.write(getResources().getString(R.string.key_is_compare_on), false);
                if(mFilterMetricTypeModelList != null && mFilterMetricTypeModelList.size() > 0){
                    InGaugeSession.write(getResources().getString(R.string.key_selected_metric_type_name), mFilterMetricTypeModelList.get(1).name);
                } else{
                    mStringKeyListMetricType = Arrays.asList(getResources().getStringArray(R.array.key_metric_type_array));
                    // mStringListMetricType.clear();
                    for(int i = 0; i < mStringKeyListMetricType.size(); i++){

                        mStringListMetricType.add(mobilekeyObject.optString(mStringKeyListMetricType.get(i)));
                    }
                    for(int i = 0; i < mStringListMetricType.size(); i++){
                        FilterMetricTypeModel metricTypeModel = new FilterMetricTypeModel();
                        metricTypeModel.id = "000" + String.valueOf(i);
                        metricTypeModel.name = mStringListMetricType.get(i);
                        metricTypeModel.filterName = mobilekeyObject.optString(getResources().getString(R.string.mobile_key_filter_metric_type));
                        mFilterMetricTypeModelList.add(metricTypeModel);
                    }
                    InGaugeSession.write(getResources().getString(R.string.key_selected_metric_type_name), mFilterMetricTypeModelList.get(1).name);
                }

                if(getMonthOrYear() == 0){
                    if(mylist.get(position).getmDatumList().get(childPosition).isMTD()){
                        Logger.Error("<<<< No Change SAme For MTD >>>>>");
                    } else{
                        setMonthOrYear(1);
                        InGaugeSession.write(getResources().getString(R.string.key_is_compare_on), false);
                        InGaugeSession.write(getResources().getString(R.string.key_start_date), sdf.format(date));
                        InGaugeSession.write(getResources().getString(R.string.key_start_date_server), sdfServer.format(date));
                        InGaugeSession.write(getResources().getString(R.string.key_end_date), sdf.format(new Date()));
                        InGaugeSession.write(getResources().getString(R.string.key_end_date_server), sdfServer.format(new Date()));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_start_date), sdf.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_end_date), sdf.format(prevDate));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(prevDate));
                    }
                } else if(getMonthOrYear() == 1){
                    if(mylist.get(position).getmDatumList().get(childPosition).isMTD()){
                        setMonthOrYear(1);
                        InGaugeSession.write(getResources().getString(R.string.key_is_compare_on), false);
                        InGaugeSession.write(getResources().getString(R.string.key_start_date), sdf.format(date));
                        InGaugeSession.write(getResources().getString(R.string.key_start_date_server), sdfServer.format(date));
                        InGaugeSession.write(getResources().getString(R.string.key_end_date), sdf.format(new Date()));
                        InGaugeSession.write(getResources().getString(R.string.key_end_date_server), sdfServer.format(new Date()));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_start_date), sdf.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_end_date), sdf.format(prevDate));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_start_date_server), sdfServer.format(firstDateOfPreviousMonth));
                        InGaugeSession.write(getResources().getString(R.string.key_compare_end_date_server), sdfServer.format(prevDate));
                    } else{
                        Logger.Error("<<<< No Change SAme For YTD >>>>>");
                    }
                }


                Bundle bundle = new Bundle();
                bundle.putString("email", InGaugeSession.read(getString(R.string.key_user_user_email), ""));
                bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                bundle.putString("location", InGaugeSession.read(getResources().getString(R.string.key_tenant_location_name), ""));
                InGaugeApp.getFirebaseAnalytics().logEvent("change_dashboard", bundle);
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                customListAdapter.notifyDataSetChanged();
                if(getDashboardRefreshListener() != null){
                    getDashboardRefreshListener().onRefresh();
                }
                return true;
            }
        });
        /*mDrawerList.setOnItemClickListener ( new AdapterView.OnItemClickListener () {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                SimpleDateFormat sdf = new SimpleDateFormat ( "MMM dd ,yyyy", Locale.US );
                SimpleDateFormat sdfServer = new SimpleDateFormat ( "yyyy-MM-dd", Locale.US );
                Date date = null;

                Date firstDateOfPreviousMonth = null;
                Date prevDate = null;


                Calendar calendar = Calendar.getInstance ();
                calendar.set ( Calendar.DAY_OF_MONTH, 1 );
                date = calendar.getTime ();


                Calendar cal = Calendar.getInstance ();

                cal.setTime ( new Date () );
                cal.add ( Calendar.MONTH, -1 );
                cal.set ( Calendar.DATE, 1 );
                firstDateOfPreviousMonth = cal.getTime ();


                Calendar previouslastDate = Calendar.getInstance ();
                previouslastDate.setTime ( new Date () );
                int daysInMonth = previouslastDate.getActualMaximum ( Calendar.DAY_OF_MONTH );
                if ( daysInMonth == 31 ) {
                    daysInMonth = daysInMonth - 1;
                } else {
                    daysInMonth = daysInMonth + 1;
                }

                Logger.Error ( "Days  " + daysInMonth );
                previouslastDate.add ( Calendar.DATE, -daysInMonth );  //not sure
                prevDate = previouslastDate.getTime ();


                tvSubTitle.setText ( mylist.get ( position ).getName () + "-" + InGaugeSession.read ( getResources ().getString ( R.string.key_tenant_location_name ), "" ) );

                Logger.Error ( "<<<< Call >>>>" );
                //setmMainLeftFilterBaseDatas(new ArrayList<FilterBaseData>());


                Gson gson = new Gson ();
                String json = gson.toJson ( new ArrayList<FilterBaseData> () );
                InGaugeSession.write ( getResources ().getString ( R.string.key_filter_obj ), json );
                InGaugeSession.write ( getString ( R.string.key_selected_dashboard ), mylist.get ( position ).getTitle () );
                InGaugeSession.write ( getString ( R.string.key_selected_dashboard_id ), mylist.get ( position ).getId () );
                InGaugeSession.write ( getString ( R.string.key_selected_dashboard_mtd ), mylist.get ( position ).isMTD () );
                InGaugeSession.write ( getString ( R.string.key_selected_dashboard_hide_compare_date ), !mylist.get ( position ).isHideCompareDate () );

                if ( getMonthOrYear () == 0 ) {
                    if ( mylist.get ( position ).isMTD () ) {
                        Logger.Error ( "<<<< No Change SAme For MTD >>>>>" );
                    } else {
                        setMonthOrYear ( 1 );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_is_compare_on ), true );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_start_date ), sdf.format ( date ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_start_date_server ), sdfServer.format ( date ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_end_date ), sdf.format ( new Date () ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_end_date_server ), sdfServer.format ( new Date () ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_compare_start_date ), sdf.format ( firstDateOfPreviousMonth ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_compare_end_date ), sdf.format ( prevDate ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_compare_start_date_server ), sdfServer.format ( firstDateOfPreviousMonth ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_compare_end_date_server ), sdfServer.format ( prevDate ) );
                    }
                } else if ( getMonthOrYear () == 1 ) {
                    if ( mylist.get ( position ).isMTD () ) {
                        setMonthOrYear ( 1 );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_is_compare_on ), true );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_start_date ), sdf.format ( date ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_start_date_server ), sdfServer.format ( date ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_end_date ), sdf.format ( new Date () ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_end_date_server ), sdfServer.format ( new Date () ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_compare_start_date ), sdf.format ( firstDateOfPreviousMonth ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_compare_end_date ), sdf.format ( prevDate ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_compare_start_date_server ), sdfServer.format ( firstDateOfPreviousMonth ) );
                        InGaugeSession.write ( getResources ().getString ( R.string.key_compare_end_date_server ), sdfServer.format ( prevDate ) );
                    } else {
                        Logger.Error ( "<<<< No Change SAme For YTD >>>>>" );
                    }
                }


                Bundle bundle = new Bundle ();
                bundle.putString ( "email", InGaugeSession.read ( getString ( R.string.key_user_user_email ), "" ) );
                bundle.putString ( "role", InGaugeSession.read ( getString ( R.string.key_user_role_name ), "" ) );
                bundle.putString ( "industry", InGaugeSession.read ( getString ( R.string.key_selected_industry ), "" ) );
                bundle.putString ( "location", InGaugeSession.read ( getResources ().getString ( R.string.key_tenant_location_name ), "" ) );
                InGaugeApp.getFirebaseAnalytics ().logEvent ( "change_dashboard", bundle );
                mDrawerLayout.closeDrawer ( Gravity.RIGHT );
                customListAdapter.notifyDataSetChanged ();
                if ( getDashboardRefreshListener () != null ) {
                    getDashboardRefreshListener ().onRefresh ();
                }
            }
        } );*/
    }


    //Making Listener to refresh dashboard while any dashboard changed from slide menu
    public DashboardRefreshListener getDashboardRefreshListener(){
        return dashboardRefreshListener;
    }

    public void setFragmentRefreshListener(DashboardRefreshListener fragmentRefreshListener){
        this.dashboardRefreshListener = fragmentRefreshListener;
    }

    private DashboardRefreshListener dashboardRefreshListener;


    public interface DashboardRefreshListener{
        void onRefresh();
    }

    public void FooterAnimation(){
        rvDashboardList.setVisibility(View.VISIBLE);
        Animation hide = AnimationUtils.loadAnimation(this, R.anim.slide_to_bottom);
        rvDashboardList.startAnimation(hide);
    }

    public void headerAnimation(){
        Animation hide = AnimationUtils.loadAnimation(this, R.anim.slide_to_up);
        rvDashboardList.startAnimation(hide);
        rvDashboardList.setVisibility(View.GONE);
    }

    @Override
    public void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterPositionm, boolean isLeft){
        //TODO Here mFilterBaseData is not usable need to change
        headerAnimation();
        tvSubTitle.setText(list.get(position).getName());
        InGaugeSession.write(getString(R.string.key_selected_dashboard), list.get(position).getName());
        InGaugeSession.write(getString(R.string.key_selected_dashboard_id), list.get(position).getId());
    }

   /* @Override
    public void updateData(String itemName, String itemId, int position, ArrayList<FilterBaseData> mFilterBaseDatasforObsFilter) {
        ObservationDashboardFilterFragment observationDashboardFilterFragment = new ObservationDashboardFilterFragment ();
        observationDashboardFilterFragment.updateData ( itemName, itemId, position, mFilterBaseDatasforObsFilter );
    }*/

    public boolean isDashboardUpdate(){
        return isDashboardUpdate;
    }

    public void setDashboardUpdate(boolean dashboardUpdate){
        isDashboardUpdate = dashboardUpdate;
    }


    public Map<Integer, List<DynamicData>> getIntegerListHashMap(){
        return integerListHashMap;
    }

    public void setIntegerListHashMap(Map<Integer, List<DynamicData>> integerListHashMap){
        this.integerListHashMap = integerListHashMap;
    }

    public boolean isDashboardUpdateAfterApply(){
        return isDashboardUpdateAfterApply;
    }

    public void setDashboardUpdateAfterApply(boolean dashboardUpdateAfterApply){
        isDashboardUpdateAfterApply = dashboardUpdateAfterApply;
    }


    public ArrayList<FilterBaseData> getmMainLeftFilterBaseDatas(){
        return mMainLeftFilterBaseDatas;
    }

    public void setmMainLeftFilterBaseDatas(ArrayList<FilterBaseData> mMainLeftFilterBaseDatas){
        this.mMainLeftFilterBaseDatas = mMainLeftFilterBaseDatas;
    }

    public boolean isRegionFilter(){
        return isRegionFilter;
    }

    public void setRegionFilter(boolean regionFilter){
        isRegionFilter = regionFilter;
    }

    public boolean isTenantLocation(){
        return isTenantLocation;
    }

    public void setTenantLocation(boolean tenantLocation){
        isTenantLocation = tenantLocation;
    }

    public boolean isCustomGoalFilter(){
        return isCustomGoalFilter;
    }

    public void setCustomGoalFilter(boolean customGoalFilter){
        isCustomGoalFilter = customGoalFilter;
    }

    public boolean isProduct(){
        return isProduct;
    }

    public void setProduct(boolean product){
        isProduct = product;
    }

    public boolean isLocationGroup(){
        return isLocationGroup;
    }

    public void setLocationGroup(boolean locationGroup){
        isLocationGroup = locationGroup;
    }

    public boolean isUser(){
        return isUser;
    }

    public void setUser(boolean user){
        isUser = user;
    }

    public List<FilterBaseData> getmLocationFilterData(){
        return mLocationFilterData;
    }

    public void setmLocationFilterData(List<FilterBaseData> mLocationFilterData){
        this.mLocationFilterData = mLocationFilterData;
    }


    public List<FilterBaseData> getmUserFilterData(){
        return mUserFilterData;
    }

    public void setmUserFilterData(List<FilterBaseData> mUserFilterData){
        this.mUserFilterData = mUserFilterData;
    }
/*
    public List<FilterBaseDataForLocationGroup> getmLocationForLGFilterData() {
        return mLocationForLGFilterData;
    }

    public void setmLocationForLGFilterData(List<FilterBaseDataForLocationGroup> mLocationForLGFilterData) {
        this.mLocationForLGFilterData = mLocationForLGFilterData;
    }
*/

    public List<FilterBaseDataForLocationGroup> getmLocationGroupForLGFilterData(){
        return mLocationGroupForLGFilterData;
    }

    public void setmLocationGroupForLGFilterData(List<FilterBaseDataForLocationGroup> mLocationGroupForLGFilterData){
        this.mLocationGroupForLGFilterData = mLocationGroupForLGFilterData;
    }

    public boolean isNeedCallForChildSocialInteraction(){
        return isNeedCallForChildSocialInteraction;
    }

    public void setNeedCallForChildSocialInteraction(boolean needCallForChildSocialInteraction){
        isNeedCallForChildSocialInteraction = needCallForChildSocialInteraction;
    }

    public JSONObject getRolesAndPermissoinObject(){
        return rolesAndPermissoinObject;
    }

    public void setRolesAndPermissoinObject(JSONObject rolesAndPermissoinObject){
        this.rolesAndPermissoinObject = rolesAndPermissoinObject;
    }

    public boolean isDashboardObsCoachingUpdateAfterApply(){
        return isDashboardObsCoachingUpdateAfterApply;
    }

    public void setDashboardObsCoachingUpdateAfterApply(boolean dashboardObsCoachingUpdateAfterApply){
        isDashboardObsCoachingUpdateAfterApply = dashboardObsCoachingUpdateAfterApply;
    }

    @Override
    protected void onNewIntent(Intent intent){
        super.onNewIntent(intent);
        Logger.Error("<<<<<, Call ON New Intent() >>>>>>>>");
    }

    public ArrayList<FilterBaseData> getmPrerferencedLeftFilterBaseDatas(){
        return mPrerferencedLeftFilterBaseDatas;
    }

    public void setmPrerferencedLeftFilterBaseDatas(ArrayList<FilterBaseData> mPrerferencedLeftFilterBaseDatas){
        this.mPrerferencedLeftFilterBaseDatas = mPrerferencedLeftFilterBaseDatas;
    }

    public int getMonthOrYear(){
        return monthOrYear;
    }

    public void setMonthOrYear(int monthOrYear){
        this.monthOrYear = monthOrYear;
    }


   /* public boolean isNoNeedUpdateMetric() {
        return isNoNeedUpdateMetric;
    }

    public void setNoNeedUpdateMetric(boolean noNeedUpdateMetric) {
        isNoNeedUpdateMetric = noNeedUpdateMetric;
    }*/

    public boolean isRegionFilterForLb(){
        return isRegionFilterForLb;
    }

    public void setRegionFilterForLb(boolean regionFilterForLb){
        isRegionFilterForLb = regionFilterForLb;
    }

    public boolean isTenantLocationForLb(){
        return isTenantLocationForLb;
    }

    public void setTenantLocationForLb(boolean tenantLocationForLb){
        isTenantLocationForLb = tenantLocationForLb;
    }

    public boolean isCustomGoalFilterForLb(){
        return isCustomGoalFilterForLb;
    }

    public void setCustomGoalFilterForLb(boolean customGoalFilterForLb){
        isCustomGoalFilterForLb = customGoalFilterForLb;
    }

    public boolean isProductForLb(){
        return isProductForLb;
    }

    public void setProductForLb(boolean productForLb){
        isProductForLb = productForLb;
    }

    public boolean isLocationGroupForLb(){
        return isLocationGroupForLb;
    }

    public void setLocationGroupForLb(boolean locationGroupForLb){
        isLocationGroupForLb = locationGroupForLb;
    }

    public boolean isUserForLb(){
        return isUserForLb;
    }

    public void setUserForLb(boolean userForLb){
        isUserForLb = userForLb;
    }

    public int getSelectedTenantLocationIdForLb(){
        return selectedTenantLocationIdForLb;
    }

    public void setSelectedTenantLocationIdForLb(int selectedTenantLocationIdForLb){
        this.selectedTenantLocationIdForLb = selectedTenantLocationIdForLb;
    }

    public String getSelectedTenantLocationNameForLb(){
        return selectedTenantLocationNameForLb;
    }

    public void setSelectedTenantLocationNameForLb(String selectedTenantLocationNameForLb){
        this.selectedTenantLocationNameForLb = selectedTenantLocationNameForLb;
    }

    public String getSelectedHotelMetricTypeNameLb(){
        return selectedHotelMetricTypeNameLb;
    }

    public void setSelectedHotelMetricTypeNameLb(String selectedHotelMetricTypeNameLb){
        this.selectedHotelMetricTypeNameLb = selectedHotelMetricTypeNameLb;
    }

    public int getSelectedLocationGroupIdForLb(){
        return selectedLocationGroupIdForLb;
    }

    public void setSelectedLocationGroupIdForLb(int selectedLocationGroupIdForLb){
        this.selectedLocationGroupIdForLb = selectedLocationGroupIdForLb;
    }

    public String getSelectedLocationGroupNameLb(){
        return selectedLocationGroupNameLb;
    }

    public void setSelectedLocationGroupNameLb(String selectedLocationGroupNameLb){
        this.selectedLocationGroupNameLb = selectedLocationGroupNameLb;
    }

    public int getSelectedProductIdForLb(){
        return selectedProductIdForLb;
    }

    public void setSelectedProductIdForLb(int selectedProductIdForLb){
        this.selectedProductIdForLb = selectedProductIdForLb;
    }

    public String getSelectedProductNameLb(){
        return selectedProductNameLb;
    }

    public void setSelectedProductNameLb(String selectedProductNameLb){
        this.selectedProductNameLb = selectedProductNameLb;
    }

    public int getSelectedUserIdForLB(){
        return selectedUserIdForLB;
    }

    public void setSelectedUserIdForLB(int selectedUserIdForLB){
        this.selectedUserIdForLB = selectedUserIdForLB;
    }

    public String getSelectedUserNameForLB(){
        return selectedUserNameForLB;
    }

    public void setSelectedUserNameForLB(String selectedUserNameForLB){
        this.selectedUserNameForLB = selectedUserNameForLB;
    }

    public int getSelectedRegionTypeForLb(){
        return selectedRegionTypeForLb;
    }

    public void setSelectedRegionTypeForLb(int selectedRegionTypeForLb){
        this.selectedRegionTypeForLb = selectedRegionTypeForLb;
    }

    public int getSelectedRegionIdForLb(){
        return selectedRegionIdForLb;
    }

    public void setSelectedRegionIdForLb(int selectedRegionIdForLb){
        this.selectedRegionIdForLb = selectedRegionIdForLb;
    }

    public ArrayList<FilterBaseData> getmLeftFilterBaseDatasForLb(){
        return mLeftFilterBaseDatasForLb;
    }

    public void setmLeftFilterBaseDatasForLb(ArrayList<FilterBaseData> mLeftFilterBaseDatasForLb){
        this.mLeftFilterBaseDatasForLb = mLeftFilterBaseDatasForLb;
    }

    public String getSelectedRegionTypeNameForLb(){
        return selectedRegionTypeNameForLb;
    }

    public void setSelectedRegionTypeNameForLb(String selectedRegionTypeNameForLb){
        this.selectedRegionTypeNameForLb = selectedRegionTypeNameForLb;
    }

    public String getSelectedRegionNameForLb(){
        return selectedRegionNameForLb;
    }

    public void setSelectedRegionNameForLb(String selectedRegionNameForLb){
        this.selectedRegionNameForLb = selectedRegionNameForLb;
    }

    public int getSelectedMetricTypeIdForLb(){
        return selectedMetricTypeIdForLb;
    }

    public void setSelectedMetricTypeIdForLb(int selectedMetricTypeIdForLb){
        this.selectedMetricTypeIdForLb = selectedMetricTypeIdForLb;
    }

    public String getSelectedMetricTypeNameForLb(){
        return selectedMetricTypeNameForLb;
    }

    public void setSelectedMetricTypeNameForLb(String selectedMetricTypeNameForLb){
        this.selectedMetricTypeNameForLb = selectedMetricTypeNameForLb;
    }

    public boolean isLeaderNeedUpdate(){
        return isLeaderNeedUpdate;
    }

    public void setLeaderNeedUpdate(boolean leaderNeedUpdate){
        isLeaderNeedUpdate = leaderNeedUpdate;
    }


    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public int getDateRangeIndex(){
        return dateRangeIndex;
    }

    public int getComparedateRangeIndex(){
        return ComparedateRangeIndex;
    }

    public boolean isNeedAgentProfileUpdate(){
        return isNeedAgentProfileUpdate;
    }

    public void setNeedAgentProfileUpdate(boolean needAgentProfileUpdate){
        isNeedAgentProfileUpdate = needAgentProfileUpdate;
    }

    public boolean isNeedAgentProfileUpdateForUserProfile(){
        return isNeedAgentProfileUpdateForUserProfile;
    }

    public void setNeedAgentProfileUpdateForUserProfile(boolean needAgentProfileUpdateForUserProfile){
        isNeedAgentProfileUpdateForUserProfile = needAgentProfileUpdateForUserProfile;
    }

}
