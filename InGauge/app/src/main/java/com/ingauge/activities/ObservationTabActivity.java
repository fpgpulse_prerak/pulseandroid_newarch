package com.ingauge.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;

import com.ingauge.R;
import com.ingauge.fragments.ObservationQuestionFragment;
import com.ingauge.fragments.ObservationStrengthFragment;
import com.ingauge.utils.BottomNavigationViewHelper;
import com.ingauge.utils.Logger;

/**
 * Created by pathanaa on 13-07-2017.
 */
public class ObservationTabActivity extends BaseActivity implements View.OnClickListener{
    public BottomNavigationView nBottomNavigationView;
    private MenuItem selectedItem;
    private int mSelectedItem;
    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_observation_tab);

        Init();
    }

    private void Init(){
        mContext = ObservationTabActivity.this;
        nBottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation_observation);
        BottomNavigationViewHelper.disableShiftMode(nBottomNavigationView);
        /*tvAgentScore=(TextView)findViewById(R.id.tv_agent_score);
        tvDone=(TextView)findViewById(R.id.tv_done);
        tvDone.setOnClickListener(this);*/
        selectedItem = nBottomNavigationView.getMenu().getItem(0);
        selectFragment(selectedItem);
        nBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item){
                try{
                    selectFragment(item);
                } catch(Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        });
    }

    public void selectFragment(MenuItem item){
        Fragment frag = null;
        // init corresponding fragment
        switch(item.getItemId()){
            case R.id.menu_question:
                frag = ObservationQuestionFragment.newInstance("", 0);
                break;
            case R.id.menu_great_job:
                frag = ObservationStrengthFragment.newInstance("", 0);
                break;

        }

        // update selected item
        mSelectedItem = item.getItemId();

        // uncheck the other items.
        for(int i = 0; i < nBottomNavigationView.getMenu().size(); i++){
            MenuItem menuItem = nBottomNavigationView.getMenu().getItem(i);
            if(menuItem.getItemId() == item.getItemId())
                menuItem.setChecked(true);
        }
        if(frag != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            ft.add(R.id.container, frag, frag.getTag());
            ft.commit();
        }
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.tv_done:{

                break;
            }
        }
    }

    public void add(Fragment fragment, String tag){
        if(fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.addToBackStack(tag);
            Logger.Error("TAG  " + tag);
            ft.add(R.id.container, fragment, tag);
            ft.commit();
        }
    }

    public void replace(Fragment fragment, String tag){
        if(fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.addToBackStack(tag);
            Logger.Error("TAG  " + tag);
            ft.replace(R.id.container, fragment, tag);
            ft.commit();
        }
    }

}
