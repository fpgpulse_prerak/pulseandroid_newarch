package com.ingauge.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.utils.Logger;

import static com.ingauge.activities.DownloadTemp.MESSAGE_PROGRESS;

/**
 * Created by mansurum on 10-May-17.
 */

public class DownloadProgressActivity extends Activity {

    public static DownloadProgressActivity instance = null;

    private ProgressBar mProgressBar;
    private TextView loadingTxtView;

    /**
     * Used to store the above text information used to display the operation of the text
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerReceiver();
        setContentView(R.layout.download_activity_progress);
        instance = this;
        loadingTxtView = (TextView) findViewById(R.id.processingtxt);
        mProgressBar = (ProgressBar) findViewById(R.id.activity_download_progress);
        // loadingTxtView.setText(mobilekeyObject.optString(instance.getResources().getString(R.string.mobile_key_progress_indicator)));
      //  loadingTxtView.setText(instance.getResources().getString(R.string.processing_text));
    }


    private void registerReceiver() {

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    /*private void UnregisterReceiver() {

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.unregisterReceiver(broadcastReceiver);

    }*/

    /**
     * @param keyCode
     * @param event
     * @return This will return False and that is used to disable the back button
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return false;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(MESSAGE_PROGRESS)) {

                int progress = intent.getIntExtra("progress", 0);
                int currentFileSize = intent.getIntExtra("currentfile_size", 0);
                int totalFileSize = intent.getIntExtra("totalfile_size", 0);
                Logger.Error("Progress  >>>>>>> " + progress);
                mProgressBar.setProgress(progress);
                if (progress == 100) {

                    loadingTxtView.setText("File Download Complete");

                } else {

                    loadingTxtView.setText(String.format("Downloaded (%d/%d) MB", currentFileSize, totalFileSize));

                }
            }
        }
    };
}
