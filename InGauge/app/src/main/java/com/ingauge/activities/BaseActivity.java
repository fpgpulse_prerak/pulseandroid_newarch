package com.ingauge.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ingauge.R;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

/**
 * Created by mansurum on 10-May-17.
 */
public class BaseActivity extends AppCompatActivity {
    int count = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void startProgress(Context mContext) {
        try {
            Logger.Error("<<<<< CALLLLL Start Progress     >>>>>   " + count++);
            Intent i = new Intent(mContext, ProgressActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void endProgress() {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Logger.Error("<<<<< CALLLLL End Progress Progress     >>>>>   " + count++);
                    if (ProgressActivity.instance != null)
                        ProgressActivity.instance.finish();

                }
            }, 8);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ForceLogout() {
        Intent login = new Intent(this, SignInActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(login);
        InGaugeSession.write(getResources().getString(R.string.key_auth_token), "");
        InGaugeSession.clearAllPreferences(this);
        finish();
    }
}
