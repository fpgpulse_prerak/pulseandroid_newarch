package com.ingauge.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.anim.InGaugeBounceInterpolator;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.session.InGaugeSession;

/**
 * Created by mansurum on 21-Apr-17.
 */

public class SplashActivity extends Activity implements View.OnClickListener{
    private static int SPLASH_TIME_OUT = 3000;
    APIInterface apiInterface;
    public String testBranchCommit = "";
    private Button mButtonAccept;
    private boolean isAcceptTermsCondition = false;
    private TextView tvAcceptCondition;
    private TextView tvTermsOfService;
    private LinearLayout llAceptTermsAndCondition;

    ///key_accept_terms_condition
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mButtonAccept = (Button) findViewById(R.id.activity_splash_btn_agree_terms);
        tvAcceptCondition = (TextView) findViewById(R.id.activity_splash_tv_please_accept);
        tvTermsOfService = (TextView) findViewById(R.id.activity_splash_tv_terms_of_service);
        llAceptTermsAndCondition = (LinearLayout)findViewById(R.id.activity_splash_ll_please_accept);
        tvTermsOfService.setOnClickListener(this);
        mButtonAccept.setOnClickListener(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        isAcceptTermsCondition = InGaugeSession.read(getString(R.string.key_accept_terms_condition), false);
        if(isAcceptTermsCondition){
            mButtonAccept.setVisibility(View.INVISIBLE);
            llAceptTermsAndCondition.setVisibility(View.INVISIBLE);
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run(){
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    if(!InGaugeSession.read(getString(R.string.key_auth_token), "").equals("")){
                        Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    } else{
                        Intent i = new Intent(SplashActivity.this, SignInActivity.class);
                        startActivity(i);
                        finish();
                    }

                }
            }, SPLASH_TIME_OUT);
        } else{
            mButtonAccept.setVisibility(View.VISIBLE);
            llAceptTermsAndCondition.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v){

        switch(v.getId()){
            case R.id.activity_splash_btn_agree_terms:
                Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);

                // Use bounce interpolator with amplitude 0.2 and frequency 20
                InGaugeBounceInterpolator interpolator = new InGaugeBounceInterpolator(0.03, 1);
                myAnim.setInterpolator(interpolator);
                InGaugeSession.write(getString(R.string.key_accept_terms_condition), true);
                mButtonAccept.startAnimation(myAnim);
                Intent i = new Intent(SplashActivity.this, SignInActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.activity_splash_tv_terms_of_service:
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(APIClient.TermsUrl));
                startActivity(i);
                break;
        }
    }
}
