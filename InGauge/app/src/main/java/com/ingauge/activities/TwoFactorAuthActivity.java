package com.ingauge.activities;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ingauge.R;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.session.InGaugeSession;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pathanaa on 04-08-2017.
 */
public class TwoFactorAuthActivity extends BaseActivity implements View.OnClickListener{
    private String authToken="";
    private int userId=0;
    private AppCompatButton btnVerify;
    private TextInputLayout mTillVerificationCode;
    private String twofaReceiverID="twofa-receiver";
    private EditText edtVerifyCode;
    APIInterface apiInterface;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twofactor);
        getSupportActionBar().hide();
        Init();
    }

    private void Init() {
        btnVerify=(AppCompatButton)findViewById(R.id.btn_verify);
        edtVerifyCode=(EditText)findViewById(R.id.edt_verify_code);

        mTillVerificationCode=(TextInputLayout)findViewById(R.id.till_verification_code);
        edtVerifyCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(charSequence.length()>0){
                    edtVerifyCode.setError(null);
                    mTillVerificationCode.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
         if(getIntent().getExtras()!=null){
             authToken=getIntent().getStringExtra("authToken");
             userId=getIntent().getIntExtra("userId",0);
         }

        apiInterface = APIClient.getClient().create(APIInterface.class);

        btnVerify.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(edtVerifyCode.getText().toString().trim().length()==0){
            mTillVerificationCode.setError(getResources().getString(R.string.error_empty_verification_code));
            return;
        }
        startProgress(TwoFactorAuthActivity.this);
        verify2FA(authToken,"1",true,edtVerifyCode.getText().toString().trim(),userId);
    }

    private void verify2FA(final String authToken, String platform, boolean isMobile, String secureToken, final int userId) {
        Call mCall = apiInterface.verify2FA(userId);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        if(jsonObject.optString("status").equalsIgnoreCase("SUCCESS")) {
                            Intent intent=new Intent(twofaReceiverID);
                            intent.putExtra("authToken",authToken);
                            intent.putExtra("userId",userId);
                            LocalBroadcastManager.getInstance(TwoFactorAuthActivity.this).sendBroadcast(intent);
                            finish();
                        }else{
                            endProgress();
                            InGaugeSession.clearAllPreferences(TwoFactorAuthActivity.this);
                            mTillVerificationCode.setError(getResources().getString(R.string.error_invalid_verification_code));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                endProgress();
            }
        });
    }


}
