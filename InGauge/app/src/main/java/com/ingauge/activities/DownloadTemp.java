package com.ingauge.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ingauge.R;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.FileUtils;
import com.ingauge.utils.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 26-Jul-17.
 */

public class DownloadTemp extends BaseActivity {


    private static final int PICK_FILE_REQUEST = 1;
    public static final String MESSAGE_PROGRESS = "message_progress";
    private String selectedFilePath;

    private static final int PERMISSION_REQUEST_CODE = 1;
    Uri selectedFileUri;
    ProgressBar mProgressBar;
    TextView mProgressText;
    Button mButtonDownload, mButtonUpload;


    Call mCall;
    APIInterface apiInterface;
    String fileName = "ABCD.docx";
    String fileUrl = "http://192.168.3.136/pulse/api/userMessage/attachment/5";
    private int totalFileSize;
    Context mContext;


    int downloadprogress = 0;
    int downloadcurrentFileSize = 0;
    int downloadtotalFileSize = 0;
    //ProgressDialog progressDialog;
    int progressStatus = 0;
    // Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_activity);
        mContext = DownloadTemp.this;

        apiInterface = APIClient.getClient().create(APIInterface.class);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);
        mProgressText = (TextView) findViewById(R.id.progress_text);
        mButtonDownload = (Button) findViewById(R.id.btn_download);
        mButtonDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    startDownload(fileUrl, fileName, DownloadTemp.this);
                } else {
                    requestPermission();
                }
            }
        });
        mButtonUpload = (Button) findViewById(R.id.btn_upload);
        mButtonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    showFileChooser();
                } else {
                    requestPermission();
                }
            }
        });
    }


    private void uploadDocument(String authToken, Uri mFileUriDoc) {
        File mFiletoUpload = FileUtils.getFile(this, mFileUriDoc);
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getContentResolver().getType(mFileUriDoc)), mFiletoUpload);
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", mFiletoUpload.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = mFiletoUpload.getName();
        RequestBody filenameRequest =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, descriptionString);

        final Call<ResponseBody> call = apiInterface.uploadFile(filenameRequest, body);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {


                } else {

                    Logger.Error("server contact failed!!!! to Upload File");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Logger.Error("error");

            }
        });

    }

    private void showFileChooser() {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
    }

    private void startDownload(String fileURL, final String filename, Context mContext) {
        final Call<ResponseBody> call = apiInterface.downloadFileWithDynamicUrlAsync(fileURL);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    Logger.Error("<<<  server contacted and has file >>>>");

                    new AsyncTask<String, String, String>() {

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            //showDialog(progress_bar_type);
                            startProgress(DownloadTemp.this);


                        }

                        @Override
                        protected String doInBackground(String... voids) {
                            Logger.Error("<<<  server contacted and has file >>>>");
                            try {
                                ResponseBody body = call.clone().execute().body();
                                int count;
                                byte data[] = new byte[1024 * 4];
                                long fileSize = body.contentLength();
                                InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
                                //File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
                                File outputFile = new File(getExternalFilesDir(null) + File.separator + fileName);
                                OutputStream output = new FileOutputStream(outputFile);
                                long total = 0;
                                long startTime = System.currentTimeMillis();
                                int timeCount = 1;

                                while ((count = bis.read(data)) != -1) {
                                    total += count;
                                    totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
                                    // publishProgress("" + (int) ((total * 100) / totalFileSize));

//                                    progressDialog.setProgress((int) ((total * 100) / totalFileSize));
                                    double current = Math.round(total / (Math.pow(1024, 2)));
                                    //showProgressDialogHorizontal((int)total * 100);
                                    int progress = (int) ((total * 100) / fileSize);
                                    long currentTime = System.currentTimeMillis() - startTime;
                                    //Download download = new Download();
                                    //download.setTotalFileSize(totalFileSize);
                                    if (currentTime > 1000 * timeCount) {
                                        //download.setCurrentFileSize((int) current);
                                        //download.setProgress(progress);
                                        //sendNotification(download);
                                        downloadprogress = progress;
                                        downloadcurrentFileSize = (int) current;
                                        downloadtotalFileSize = totalFileSize;
                                        sendIntent(downloadtotalFileSize, downloadcurrentFileSize, downloadprogress);
                                        timeCount++;
                                    }
                                    output.write(data, 0, count);
                                }
                                output.flush();
                                output.close();
                                bis.close();
                                /*boolean writtenToDisk = false;
                                writtenToDisk = writeResponseBodyToDisk(call.clone().execute().body(), filename);
                                Logger.Error("<<<  file download was a success? " + writtenToDisk + ">>>>");*/
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                            return null;
                        }

                        @Override
                        protected void onPostExecute(String aVoid) {
                            super.onPostExecute(aVoid);
                            //progressDialog.dismiss();
                            // endProgress();
                        }

                        @Override
                        protected void onProgressUpdate(String... values) {
                            super.onProgressUpdate(values);
                            // progressDialog.setProgress(Integer.parseInt(values[0]));
                        }
                    }.execute();
                } else {

                    Logger.Error("server contact failed!!!! to download File");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Logger.Error("error");

            }
        });
    }

    private void sendIntent(int totalsize, int currentfileSize, int progress) {

        Intent intent = new Intent(MESSAGE_PROGRESS);
        intent.putExtra("progress", progress);
        intent.putExtra("currentfile_size", currentfileSize);
        intent.putExtra("totalfile_size", totalsize);
        LocalBroadcastManager.getInstance(DownloadTemp.this).sendBroadcast(intent);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;

        } else {

            return false;
        }
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    startDownload(fileUrl, fileName, this);
                } else {

                    Snackbar.make(findViewById(R.id.coordinatorLayout), "Permission Denied, Please allow to proceed !", Snackbar.LENGTH_LONG).show();

                }
                break;
        }
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String fileName) {
        try {
            int count;
            byte data[] = new byte[1024 * 4];
            long fileSize = body.contentLength();
            InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
            //File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
            File outputFile = new File(getExternalFilesDir(null) + File.separator + fileName);
            OutputStream output = new FileOutputStream(outputFile);
            long total = 0;
            long startTime = System.currentTimeMillis();
            int timeCount = 1;
            while ((count = bis.read(data)) != -1) {

                total += count;
                totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
                double current = Math.round(total / (Math.pow(1024, 2)));
                //showProgressDialogHorizontal((int) total * 100);
                int progress = (int) ((total * 100) / fileSize);
                long currentTime = System.currentTimeMillis() - startTime;
                //Download download = new Download();
                //download.setTotalFileSize(totalFileSize);
                if (currentTime > 1000 * timeCount) {
                    //download.setCurrentFileSize((int) current);
                    //download.setProgress(progress);
                    //sendNotification(download);
                    timeCount++;
                }

                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            bis.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }


   /* private void showProgressDialogHorizontal(final int progressMaxSize) {

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait..");
        progressDialog.setMessage("Downloading Video ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setMax(progressMaxSize);
        progressDialog.show();

        // Start lengthy operation in a background thread
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < progressMaxSize) {
                    try {
                        // Here I'm making thread sleep to show progress
                        Thread.sleep(200);
                        progressStatus += 5;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // Update the progress bar
                    handler.post(new Runnable() {
                        public void run() {
                            progressDialog.setProgress(progressStatus);
                        }
                    });
                }
                progressDialog.dismiss();
            }
        }).start();
    }*/

    public void startProgress(Context mContext) {
        try {
            Intent i = new Intent(mContext, DownloadProgressActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void endProgress() {
        try {
            if (DownloadProgressActivity.instance != null)
                DownloadProgressActivity.instance.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FILE_REQUEST) {
                if (data == null) {
                    //no data present
                    return;
                }
                selectedFileUri = data.getData();

                //  selectedFilePath = FileUtils.getPath(this, selectedFileUri);
                if (selectedFileUri != null && !selectedFileUri.equals("")) {
                    Logger.Error(" Selected File Path:" + selectedFilePath);
                    uploadDocument(InGaugeSession.read(getResources().getString(R.string.key_auth_token), ""), selectedFileUri);

                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
