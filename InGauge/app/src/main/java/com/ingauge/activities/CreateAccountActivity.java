package com.ingauge.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.ObservationJobType;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pathanaa on 16-05-2017.
 */

public class CreateAccountActivity extends BaseActivity implements View.OnClickListener {
    private EditText edtFname;
    private TextInputLayout mTextInputLayoutFname;

    private EditText edtLname;
    private TextInputLayout mTextInputLayoutLname;

    private EditText edtPhoneNumber;
    private TextInputLayout mTextInputLayoutPhoneNumber;

    private EditText edtEmpId;
    private TextInputLayout mTextInputLayoutEmpID;

    private EditText edtJobTitle;
    private TextInputLayout mTextInputLayoutTitle;

    private EditText edtOrg;
    private TextInputLayout mTextInputLayoutOrg;

    private EditText edtEmail;
    private TextInputLayout mTextInputLayoutEmail;

    private EditText edtPassword;
    private TextInputLayout mTextInputLayoutPassword;

    private EditText edtConfirmPassword;
    private TextInputLayout mTextInputLayoutConfirmPassword;

    //private EditText edtCountryCode;

    private AppCompatButton btn_create_account;
    private TextView tv_title;

    private MaterialSpinner spinnerMonth;
    private MaterialSpinner spinnerDay;
    private MaterialSpinner spinnerDomain;
    private String dayOfMonth="1";
    private String Month="1";
    private String SelectedDomain="Hospitality";
    private RadioButton mRadiobtnMail;
    APIInterface apiInterface;
    private CountryCodePicker mCountryCodePicker;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_create_account);
        Init();
    }

    private void Init() {
        edtFname = (EditText) findViewById(R.id.edt_fname);
        edtFname.addTextChangedListener(new TextChange(edtFname));
        mTextInputLayoutFname = (TextInputLayout) findViewById(R.id.til_fname);

        edtLname = (EditText) findViewById(R.id.edt_lname);
        edtLname.addTextChangedListener(new TextChange(edtLname));
        mTextInputLayoutLname = (TextInputLayout) findViewById(R.id.til_lname);

        edtPhoneNumber = (EditText) findViewById(R.id.edt_phone_number);
        edtPhoneNumber.addTextChangedListener(new TextChange(edtPhoneNumber));
        mTextInputLayoutPhoneNumber = (TextInputLayout) findViewById(R.id.til_phone_number);

        edtEmpId = (EditText) findViewById(R.id.edt_emp_id);
        edtEmpId.addTextChangedListener(new TextChange(edtEmpId));
        mTextInputLayoutEmpID = (TextInputLayout) findViewById(R.id.til_emp_id);

        edtJobTitle = (EditText) findViewById(R.id.edt_job_title);
        edtJobTitle.addTextChangedListener(new TextChange(edtJobTitle));
        mTextInputLayoutTitle = (TextInputLayout) findViewById(R.id.til_job_title);

        edtOrg = (EditText) findViewById(R.id.edt_org_title);
        edtOrg.addTextChangedListener(new TextChange(edtOrg));
        mTextInputLayoutOrg = (TextInputLayout) findViewById(R.id.til_org_title);

        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtEmail.addTextChangedListener(new TextChange(edtEmail));
        mTextInputLayoutEmail = (TextInputLayout) findViewById(R.id.til_email);

        edtPassword = (EditText) findViewById(R.id.edt_pword);
        edtPassword.addTextChangedListener(new TextChange(edtPassword));
        mTextInputLayoutPassword = (TextInputLayout) findViewById(R.id.til_pword);

        edtConfirmPassword = (EditText) findViewById(R.id.edt_cpword);
        edtConfirmPassword.addTextChangedListener(new TextChange(edtConfirmPassword));

        mCountryCodePicker= (CountryCodePicker) findViewById(R.id.country_code_picker);
        mTextInputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.til_cpword);

        btn_create_account = (AppCompatButton) findViewById(R.id.btn_create_account);
        btn_create_account.setOnClickListener(this);

        mRadiobtnMail=(RadioButton) findViewById(R.id.radioBtn_male);

        spinnerMonth=(MaterialSpinner)findViewById(R.id.spinner_month);
        spinnerMonth.setItems("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        spinnerMonth.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
              //  Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                Month=String.valueOf(position+1);
            }
        });

        spinnerDay=(MaterialSpinner)findViewById(R.id.spinner_day);
        spinnerDay.setItems("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31");
        spinnerDay.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                //  Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                dayOfMonth=item;
            }
        });

        spinnerDomain=(MaterialSpinner)findViewById(R.id.spinner_domain);
        spinnerDomain.setItems("Hospitality","Car Rental");
        spinnerDomain.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                //  Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                if(position==0){
                    UiUtils.baseUrl = APIClient.UATbaseUrl;
                    UiUtils.baseUrlChart = APIClient.UATbaseUrlChart;
                }else{
                    UiUtils.baseUrl = APIClient.ECbaseUrl;
                    UiUtils.baseUrlChart = APIClient.ECbaseUrlChart;
                }
                SelectedDomain=item;
            }
        });

        tv_title = (TextView) findViewById(R.id.tv_custom_title);
        tv_title.setText(getResources().getString(R.string.title_createaccount));

        edtConfirmPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (edtConfirmPassword.getRight() - edtConfirmPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if (edtConfirmPassword.getTransformationMethod() == null) {
                            edtConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
                            edtConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.hide_password, 0);
                        } else {
                            edtConfirmPassword.setTransformationMethod(null);
                            edtConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.view_password, 0);
                        }

                        return true;
                    }
                }
                return false;
            }
        });

    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        validateControl();
    }

    public void validateControl() {
        if (edtFname.getText().toString().trim().length() == 0) {
            mTextInputLayoutFname.setError(getString(R.string.error_empty_fname));
            return;
        } else {
            mTextInputLayoutFname.setError(null);
            mTextInputLayoutFname.setErrorEnabled(false);
        }

        if (edtLname.getText().toString().trim().length() == 0) {
            mTextInputLayoutLname.setError(getString(R.string.error_empty_lname));
            return;
        } else {
            mTextInputLayoutLname.setError(null);
            mTextInputLayoutLname.setErrorEnabled(false);
        }

        if (edtPhoneNumber.getText().toString().trim().length() == 0) {
            mTextInputLayoutPhoneNumber.setError(getString(R.string.error_empty_phone));
            return;
        } else {
            mTextInputLayoutPhoneNumber.setError(null);
            mTextInputLayoutPhoneNumber.setErrorEnabled(false);
        }

        if (edtEmpId.getText().toString().trim().length() == 0) {
            mTextInputLayoutEmpID.setError(getString(R.string.error_empty_empId));
            return;
        } else {

            mTextInputLayoutEmpID.setError(null);
            mTextInputLayoutEmpID.setErrorEnabled(false);
        }

        if (edtJobTitle.getText().toString().trim().length() == 0) {
            mTextInputLayoutTitle.setError(getString(R.string.error_empty_jobtitle));
            return;
        } else {
            mTextInputLayoutTitle.setError(null);
            mTextInputLayoutTitle.setErrorEnabled(false);
        }



        //ORGANIZATION
        if (edtOrg.getText().toString().trim().length() == 0) {
            mTextInputLayoutOrg.setError(getString(R.string.error_empty_organization));
            return;
        } else {
            mTextInputLayoutOrg.setError(null);
            mTextInputLayoutOrg.setErrorEnabled(false);
        }

        String email = edtEmail.getText().toString().trim();
        if (email.isEmpty()) {
            mTextInputLayoutEmail.setError(getString(R.string.error_empty_email));
            requestFocus(mTextInputLayoutEmail);
            return;
        }else  if (!UiUtils.isValidEmail(email)) {
            mTextInputLayoutEmail.setError(getString(R.string.error_invalid_email));
            requestFocus(mTextInputLayoutEmail);
            return;
        } else {
            mTextInputLayoutEmail.setError(null);
            mTextInputLayoutEmail.setErrorEnabled(false);
        }

        if (edtPassword.getText().toString().trim().length() == 0) {
            mTextInputLayoutPassword.setError(getString(R.string.error_empty_password));
            return;
        } else {
            mTextInputLayoutPassword.setError(null);
            mTextInputLayoutPassword.setErrorEnabled(false);
        }

        if (edtPassword.getText().toString().trim().length() < 6) {
            mTextInputLayoutPassword.setError(getString(R.string.error_length_password));
            return;
        } else {
            mTextInputLayoutPassword.setError(null);
            mTextInputLayoutPassword.setErrorEnabled(false);
        }

        if (edtConfirmPassword.getText().toString().trim().length() == 0) {
            mTextInputLayoutConfirmPassword.setError(getString(R.string.error_empty_cpassword));
            return;
        } else {
            mTextInputLayoutConfirmPassword.setError(null);
            mTextInputLayoutConfirmPassword.setErrorEnabled(false);
        }

        if (!(edtPassword.getText().toString().trim().equalsIgnoreCase(edtConfirmPassword.getText().toString().trim()))) {
            mTextInputLayoutConfirmPassword.setError(getString(R.string.error_pword_notmatch));
            return;
        } else {
            mTextInputLayoutConfirmPassword.setError(null);
            mTextInputLayoutConfirmPassword.setErrorEnabled(false);
        }

        CreateAccount();

    }

    private void CreateAccount() {
        try {
            JsonObject requestBody = new JsonObject();
            requestBody.addProperty("email",edtEmail.getText().toString().trim());
            requestBody.addProperty("password",edtPassword.getText().toString().trim());
            requestBody.addProperty("firstName",edtFname.getText().toString().trim());
            requestBody.addProperty("lastName", edtLname.getText().toString().trim());

            if(mRadiobtnMail.isChecked()) {
                requestBody.addProperty("gender", "Male");
            }else{
                requestBody.addProperty("gender", "Female");
            }

            requestBody.addProperty("number", edtEmpId.getText().toString().trim());
            requestBody.addProperty("birthDay",dayOfMonth);
            requestBody.addProperty("birthMonth",Month);
            requestBody.addProperty("countryCode",mCountryCodePicker.getSelectedCountryCode());
            requestBody.addProperty("phoneNumber",edtPhoneNumber.getText().toString().trim());
            requestBody.addProperty("jobTitle",edtJobTitle.getText().toString().trim());
            requestBody.addProperty("desiredLocation",edtOrg.getText().toString().trim());

            apiInterface = APIClient.getClientForCreateAccount().create(APIInterface.class);
            GetObsJobType("application/json",true,1,requestBody);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void GetObsJobType(String ContentType,boolean isMobile,int platform,JsonObject requestBody) {
        final Call mCall = apiInterface.createNewAccount(requestBody);
        startProgress(CreateAccountActivity.this);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                endProgress();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        String alertMessage = "";
                        boolean isSucess = true;
                        if (jsonObject.optString("status").equalsIgnoreCase("SUCCESS")) {
                            alertMessage = "Thank you for registering for IN-Gauge. The account is in pending status and will be activated by your System administrator.";

                            Bundle bundle = new Bundle();
                            bundle.putString("email", edtEmail.getText().toString().trim());
                            bundle.putString("success", "Registration Successful");
                            InGaugeApp.getFirebaseAnalytics().logEvent("register_new_user", bundle);



                        } else {
                            alertMessage =jsonObject.optString("message");
                            isSucess = false;
                        }

                        final boolean finalIsSucess = isSucess;
                        new AlertDialog.Builder(CreateAccountActivity.this)
                                .setTitle("Success")
                                .setMessage(alertMessage)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        if (finalIsSucess) {
                                            finish();
                                        }

                                    }
                                }).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                endProgress();
            }
        });
    }


    class TextChange implements TextWatcher {
        View view;

        private TextChange(View v) {
            view = v;
        }//end constructor

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()) {
                case R.id.edt_fname:
                    if (edtFname.getText().toString().trim().length() == 0) {
                        mTextInputLayoutFname.setError(getString(R.string.error_empty_fname));
                        return;
                    } else {
                        mTextInputLayoutFname.setError(null);
                        mTextInputLayoutFname.setErrorEnabled(false);
                    }
                    break;
                case R.id.edt_lname:
                    if (edtLname.getText().toString().trim().length() == 0) {
                        mTextInputLayoutLname.setError(getString(R.string.error_empty_lname));
                        return;
                    } else {
                        mTextInputLayoutLname.setError(null);
                        mTextInputLayoutLname.setErrorEnabled(false);
                    }
                    break;
                case R.id.edt_phone_number:
                    if (edtPhoneNumber.getText().toString().trim().length() == 0) {
                        mTextInputLayoutPhoneNumber.setError(getString(R.string.error_empty_phone));
                        return;
                    } else {
                        mTextInputLayoutPhoneNumber.setError(null);
                        mTextInputLayoutPhoneNumber.setErrorEnabled(false);
                    }
                    break;
                case R.id.edt_emp_id:
                    if (edtEmpId.getText().toString().trim().length() == 0) {
                        mTextInputLayoutEmpID.setError(getString(R.string.error_empty_empId));
                        return;
                    } else {
                        mTextInputLayoutEmpID.setError(null);
                        mTextInputLayoutEmpID.setErrorEnabled(false);
                    }
                    break;
                case R.id.edt_job_title:
                    if (edtJobTitle.getText().toString().trim().length() == 0) {
                        mTextInputLayoutTitle.setError(getString(R.string.error_empty_jobtitle));
                        return;
                    } else {
                        mTextInputLayoutTitle.setError(null);
                        mTextInputLayoutTitle.setErrorEnabled(false);
                    }

                    break;

                case R.id.edt_org_title:
                    if (edtOrg.getText().toString().trim().length() == 0) {
                        mTextInputLayoutOrg.setError(getString(R.string.error_empty_organization));
                        return;
                    } else {
                        mTextInputLayoutOrg.setError(null);
                        mTextInputLayoutOrg.setErrorEnabled(false);
                    }
                    break;

                case R.id.edt_email:
                    String email = edtEmail.getText().toString().trim();
                    if (email.isEmpty() || !UiUtils.isValidEmail(email)) {
                        mTextInputLayoutEmail.setError(getString(R.string.error_empty_email));
                        requestFocus(mTextInputLayoutEmail);
                        return;
                    } else {
                        mTextInputLayoutEmail.setError(null);
                        mTextInputLayoutEmail.setErrorEnabled(false);
                    }
                    break;
                case R.id.edt_password:
                    if (edtPassword.getText().toString().trim().length() == 0) {
                        mTextInputLayoutPassword.setError(getString(R.string.error_empty_password));
                        return;
                    } else {
                        mTextInputLayoutPassword.setError(null);
                        mTextInputLayoutPassword.setErrorEnabled(false);
                    }

                    if (edtPassword.getText().toString().trim().length() < 6) {
                        mTextInputLayoutPassword.setError(getString(R.string.error_length_password));
                        return;
                    } else {
                        mTextInputLayoutPassword.setError(null);
                        mTextInputLayoutPassword.setErrorEnabled(false);
                    }
                    break;
                case R.id.edt_cpword:
                    if (edtConfirmPassword.getText().toString().trim().length() == 0) {
                        mTextInputLayoutConfirmPassword.setError(getString(R.string.error_empty_cpassword));
                        return;
                    } else {
                        mTextInputLayoutConfirmPassword.setError(null);
                        mTextInputLayoutConfirmPassword.setErrorEnabled(false);
                    }
                    break;
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }
}
