package com.ingauge.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.ingauge.R;
import com.ingauge.utils.UiUtils;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONObject;

/**
 * Created by mansurum on 10-May-17.
 */

public class ProgressActivity extends Activity {
    public static ProgressActivity instance = null;

    public static boolean active = false;

    /**
     * Used to store the above text information used to display the operation of the text
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_progress);
        instance = this;

        TextView loadingTxtView = (TextView) findViewById(R.id.processingtxt);
       // loadingTxtView.setText(mobilekeyObject.optString(instance.getResources().getString(R.string.mobile_key_progress_indicator)));
        loadingTxtView.setText(instance.getResources().getString(R.string.processing_text));
        loadingTxtView.setVisibility(View.GONE);
    }

    /**
     *
     * @param keyCode
     * @param event
     * @return This will return False and that is used to disable the back button
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

}
