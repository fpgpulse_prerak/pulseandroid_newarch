package com.ingauge.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.anim.InGaugeBounceInterpolator;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.DefaultFilter;
import com.ingauge.pojo.IndustryDataModel;
import com.ingauge.pojo.LoginData;
import com.ingauge.pojo.TenantDataModel;
import com.ingauge.pojo.UserBody;
import com.ingauge.pojo.UserData;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.DatabaseHandler;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 21-Apr-17.
 */

public class SignInActivity extends BaseActivity implements View.OnClickListener{
    private EditText edtEmail;
    private EditText edtPassWord;
    private TextInputLayout mTextInputLayoutEmail;
    private TextInputLayout mTextInputLayoutPasWord;
    private AppCompatButton btnSignin;
    APIInterface apiInterface;
    private Context mContext;
    private TextView tvForgotPassword;
    private LinearLayout llCreateAccount;
    private BroadcastReceiver twofaReceiver;
    private String twofaReceiverID = "twofa-receiver";
    private ImageView imgHotel;
    private ImageView imgCarRental;
    private Animation myAnim;
    private LinearLayout mLinearCar;
    private LinearLayout mLinearHotel;

    List<IndustryDataModel.Datum> industryList;
    boolean isHotelIndustry = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mContext = SignInActivity.this;
        getSupportActionBar().hide();
        setContentView(R.layout.activity_signin);
        industryList = new ArrayList<>();
        InGaugeSession.write(getString(R.string.key_is_need_filter_call), true);
        InGaugeSession.write(getString(R.string.key_is_need_filter_call_goal), true);
        InGaugeSession.write(getString(R.string.key_is_need_update_on_apply), false);
        InGaugeSession.write(getString(R.string.key_is_need_update_on_apply_for_goal), false);
        initViews();

        //        TODO : Below static data is used for testing purpose
        /* edtEmail.setText("akhan@fpgpulse.com");
         edtPassWord.setText("123456");*/

    }

    class TextChange implements TextWatcher{
        View view;

        private TextChange(View v){
            view = v;
        }//end constructor

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after){

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count){


        }

        @Override
        public void afterTextChanged(Editable editable){
            switch(view.getId()){
                case R.id.edt_email:
                    String email = edtEmail.getText().toString().trim();
                    if(email.isEmpty() || !UiUtils.isValidEmail(email)){
                        mTextInputLayoutEmail.setError(getString(R.string.error_empty_email));
                        requestFocus(mTextInputLayoutEmail);
                        return;

                    } else{
                        mTextInputLayoutEmail.setError(null);
                        mTextInputLayoutEmail.setErrorEnabled(false);

                    }
                    validateEmail();
                    break;
                case R.id.edt_password:
                    mTextInputLayoutEmail.setError(null);
                    mTextInputLayoutEmail.setErrorEnabled(false);
                    if(edtPassWord.getText().toString().trim().length() == 0){
                        mTextInputLayoutPasWord.setError(getString(R.string.error_empty_password));
                        requestFocus(mTextInputLayoutPasWord);
                        return;
                    } else{
                        mTextInputLayoutPasWord.setError(null);
                        mTextInputLayoutPasWord.setErrorEnabled(false);

                    }
                    validatePassword();
                    break;
            }
        }
    }

    private void initViews(){
        edtEmail = (EditText) findViewById(R.id.edt_email);
        // edtEmail.addTextChangedListener(new TextChange(edtEmail));

        edtPassWord = (EditText) findViewById(R.id.edt_password);
        edtPassWord.addTextChangedListener(new TextChange(edtPassWord));

        tvForgotPassword = (TextView) findViewById(R.id.tv_forgotpassword);
        llCreateAccount = (LinearLayout) findViewById(R.id.ll_create_account);
        mTextInputLayoutEmail = (TextInputLayout) findViewById(R.id.til_email);

        mTextInputLayoutPasWord = (TextInputLayout) findViewById(R.id.til_password);
        btnSignin = (AppCompatButton) findViewById(R.id.btn_signin);

        imgHotel = (ImageView) findViewById(R.id.img_hotel);
        imgCarRental = (ImageView) findViewById(R.id.img_car);

        mLinearCar = (LinearLayout) findViewById(R.id.linear_car);
        mLinearHotel = (LinearLayout) findViewById(R.id.linear_hotel);

//        imgHotel.setAlpha(50);

        //Click Listener Registers
        btnSignin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        llCreateAccount.setOnClickListener(this);
        mLinearHotel.setOnClickListener(this);
        mLinearCar.setOnClickListener(this);

        edtPassWord.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event){
                final int DRAWABLE_RIGHT = 2;
                if(event.getAction() == MotionEvent.ACTION_UP){
                    if(event.getRawX() >= (edtPassWord.getRight() - edtPassWord.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())){
                        // your action here
                        if(edtPassWord.getTransformationMethod() == null){
                            edtPassWord.setTransformationMethod(new PasswordTransformationMethod());
                            edtPassWord.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_login_password, 0, R.drawable.hide_password, 0);
                        } else{
                            edtPassWord.setTransformationMethod(null);
                            edtPassWord.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_login_password, 0, R.drawable.view_password, 0);
                        }

                        return true;
                    }
                }
                return false;
            }


        });
        myAnim = AnimationUtils.loadAnimation(this, R.anim.select_industry);
        SelectHotel();
        registerBroadCastfor2FA();
    }

    private void registerBroadCastfor2FA(){
        twofaReceiver = new BroadcastReceiver(){

            @Override
            public void onReceive(Context context, Intent intent){
                startProgress(SignInActivity.this);
                String authToken = intent.getStringExtra("authToken");
                int userId = intent.getIntExtra("userId", 0);

                GetIndustry(authToken, userId);
                //


            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(twofaReceiver, new IntentFilter(twofaReceiverID));
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(twofaReceiver);
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.btn_signin:
                Animation mAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce);

                // Use bounce interpolator with amplitude 0.2 and frequency 20
                InGaugeBounceInterpolator interpolator = new InGaugeBounceInterpolator(0.03, 1);
                mAnimation.setInterpolator(interpolator);

                btnSignin.startAnimation(mAnimation);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                submitLoginData();

                break;
            case R.id.tv_forgotpassword:
                startActivity(new Intent(mContext, ForgotPasswordActivity.class));
                break;
            case R.id.ll_create_account:
                startActivity(new Intent(mContext, CreateAccountActivity.class));
                break;
            case R.id.linear_hotel:
                imgHotel.startAnimation(myAnim);
                SelectHotel();
                break;
            case R.id.linear_car:
                imgCarRental.startAnimation(myAnim);
                SelectCarRental();
                break;
        }
    }

    public void SelectHotel(){
        UiUtils.baseUrl = APIClient.UATbaseUrl;
        UiUtils.baseUrlChart = APIClient.UATbaseUrlChart;
        mLinearHotel.setBackgroundResource(R.drawable.border_industry_selection);
        mLinearCar.setBackgroundResource(0);
        apiInterface = APIClient.getClientForLogin().create(APIInterface.class);
        isHotelIndustry = true;
    }

    public void SelectCarRental(){
        UiUtils.baseUrl = APIClient.ECbaseUrl;
        UiUtils.baseUrlChart = APIClient.ECbaseUrlChart;
        apiInterface = APIClient.getClientForLogin().create(APIInterface.class);
        mLinearHotel.setBackgroundResource(0);
        mLinearCar.setBackgroundResource(R.drawable.border_industry_selection);
        isHotelIndustry = false;
    }

    /**
     * Validating Login form and API Request
     */
    private void submitLoginData(){

        if(edtEmail.getText().toString().trim().length() == 0){
            mTextInputLayoutEmail.setError(getString(R.string.error_empty_email));
            requestFocus(mTextInputLayoutEmail);
            return;
        }

        if(!validateEmail()){
            return;
        }

        if(!validatePassword()){
            return;
        }


        //LoginChoice();
        apiInterface = APIClient.getClientForLogin().create(APIInterface.class);
        LoginAPI(edtEmail.getText().toString(), UiUtils.md5(edtPassWord.getText().toString()));
        //LoginAPIProduction(edtEmail.getText().toString(), UiUtils.md5(edtPassWord.getText().toString()));

/*        Gson gson = new Gson();
        LoginData loginData = gson.fromJson(UiUtils.loadJSONFromAsset("LoginResponse.json", mContext), LoginData.class);
        InGaugeSession.write(getString(R.string.key_auth_token), loginData.accessToken);
        Intent mIntent = new Intent(SignInActivity.this, HomeActivity.class);
        startActivity(mIntent);*/
    }



/*
    *//**
     * Validate the Email Address
     *
     * @return
     *//*
    private boolean CheckNulEmail() {
        String email = edtEmail.getText().toString().trim();
        if (!UiUtils.isValidEmail(email)) {
            requestFocus(mTextInputLayoutEmail);
            mTextInputLayoutEmail.setError(getString(R.string.error_empty_email));
            requestFocus(mTextInputLayoutEmail);
            return false;
        } else {
            mTextInputLayoutEmail.setError(null);
            mTextInputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }*/

    /**
     * Validate the Email Address
     *
     * @return
     */
    private boolean validateEmail(){
        String email = edtEmail.getText().toString().trim();
        if(TextUtils.isEmpty(email)){
            requestFocus(mTextInputLayoutEmail);
            mTextInputLayoutEmail.setError(getString(R.string.error_empty_email));
            requestFocus(mTextInputLayoutEmail);
            return false;
        } else if(!UiUtils.isValidEmail(email)){
            requestFocus(mTextInputLayoutEmail);
            mTextInputLayoutEmail.setError(getString(R.string.error_invalid_email));
            requestFocus(mTextInputLayoutEmail);
            return false;
        } else{
            mTextInputLayoutEmail.setError(null);
            mTextInputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    /**
     * Validate User Inserted Password
     *
     * @return
     */
    private boolean validatePassword(){
        if(edtPassWord.getText().toString().trim().isEmpty()){
            mTextInputLayoutPasWord.setError(getString(R.string.error_empty_password));
            requestFocus(mTextInputLayoutPasWord);
            return false;
        } else{
            mTextInputLayoutPasWord.setError(null);
            mTextInputLayoutPasWord.setErrorEnabled(false);
        }

        return true;
    }

    /**
     * Request Focus to enable to type
     *
     * @param view
     */
    private void requestFocus(View view){
        if(view.requestFocus()){
            edtEmail.setBackgroundColor(Color.TRANSPARENT);
            edtPassWord.setBackgroundColor(Color.TRANSPARENT);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    /**
     * This is API Request block of Login
     *
     * @param email
     * @param hashPassword
     */
    void LoginAPIProduction(String email, String hashPassword){


        /*{"client_id": "restapp",
                "client_secret": "restapp",
                "grant_type": "password",
                "username": "prerak@fpgpulse.com",
                "password": "e10adc3949ba59abbe56e057f20f883e",
                "deviceId": 123456787897987,
                "platform": 2

        }*/
        startProgress(SignInActivity.this);


        String deviceId = InGaugeSession.read(this.getResources().getString(R.string.key_device_id_for_fcm), "");
        if(TextUtils.isEmpty(deviceId)){
            try{
                deviceId = FirebaseInstanceId.getInstance().getToken();
                InGaugeSession.write(getResources().getString(R.string.key_device_id_for_fcm), deviceId);
                Logger.Error("Referesh Token " + deviceId);
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        JsonObject mRequestJsonObject = new JsonObject();

        mRequestJsonObject.addProperty("client_id", "restapp");
        mRequestJsonObject.addProperty("client_secret", "restapp");
        mRequestJsonObject.addProperty("grant_type", "password");
        mRequestJsonObject.addProperty("username", email);
        mRequestJsonObject.addProperty("password", hashPassword);
        mRequestJsonObject.addProperty("deviceId", deviceId);
        mRequestJsonObject.addProperty("platform", 1);
        /*@Header("platform") String platform,
        @Field("client_id") String clientId,
        @Field("client_secret") String secret,
        @Field("grant_type") String grantType,
        @Field("username") String userName,
        @Field("password") String password*/
        Call mCall = apiInterface.getUserLoginProd("1", "restapp", "restapp", "password", email, hashPassword);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    LoginData mLoginData = (LoginData) response.body();
                    InGaugeSession.write(getString(R.string.key_auth_token), mLoginData.getData().getAccessToken());
                    Logger.Error("Response " + mLoginData.getData().getAccessToken() + "\n" + mLoginData.getData().getRefreshToken() + "\n" + mLoginData.getData().getScope() + "\n" + mLoginData.getData().getExpiresIn());
                    GetUsersAPI(mLoginData.getData().getAccessToken());

                } else if(response.raw() != null){
                    if(response.raw().code() == 401){
                        endProgress();
                        Snackbar.make(btnSignin, getString(R.string.invalid_login_message), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
    }

    /**
     * This is API Request block of Login
     *
     * @param email
     * @param hashPassword
     */
    void LoginAPI(final String email, String hashPassword){


        /*{"client_id": "restapp",
                "client_secret": "restapp",
                "grant_type": "password",
                "username": "prerak@fpgpulse.com",
                "password": "e10adc3949ba59abbe56e057f20f883e",
                "deviceId": 123456787897987,
                "platform": 2

        }*/
        startProgress(SignInActivity.this);


        String deviceId = InGaugeSession.read(this.getResources().getString(R.string.key_device_id_for_fcm), "");
        if(TextUtils.isEmpty(deviceId)){
            try{
                deviceId = FirebaseInstanceId.getInstance().getToken();
                InGaugeSession.write(getResources().getString(R.string.key_device_id_for_fcm), deviceId);
                Logger.Error("Referesh Token " + deviceId);
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        JsonObject mRequestJsonObject = new JsonObject();

        mRequestJsonObject.addProperty("client_id", "restapp");
        mRequestJsonObject.addProperty("client_secret", "restapp");
        mRequestJsonObject.addProperty("grant_type", "password");
        mRequestJsonObject.addProperty("username", email);
        mRequestJsonObject.addProperty("password", hashPassword);
        mRequestJsonObject.addProperty("deviceId", deviceId);
        mRequestJsonObject.addProperty("platform", 1);

        Call<ResponseBody> mCall = apiInterface.getUserLogin(mRequestJsonObject);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                if(response.body() != null){

                    try{

                        String jsonResponse = response.body().string();
                        Logger.Error("<<< Response  >>>>> " + jsonResponse);
                        JSONObject mJsonObject = new JSONObject(jsonResponse);
                        int statusCode = mJsonObject.optInt("statusCode");

                        if(statusCode == 200){
                            JSONObject mDataJsonObject = mJsonObject.optJSONObject("data");
                            Logger.Error("<<< Response  Access Token >>>>> " + mDataJsonObject.optString("access_token"));
                            InGaugeSession.write(getString(R.string.key_auth_token), mDataJsonObject.optString("access_token"));
                            GetUsersAPI(mJsonObject.optString("access_token"));
                        } else{
                            endProgress();
                            JSONArray mDataJArrayObj = mJsonObject.optJSONArray("data");
                            JSONObject errorJsonObject = (JSONObject) mDataJArrayObj.get(0);

                            String message = errorJsonObject.optString("message");
                            if(statusCode == 500 || statusCode == 401){
                                if(message != null && message.length() > 0){
                                    Snackbar.make(btnSignin, message, Snackbar.LENGTH_SHORT).show();
                                } else{
                                    Snackbar.make(btnSignin, getString(R.string.invalid_login_message), Snackbar.LENGTH_SHORT).show();
                                }
                            } else if(statusCode == 405){
                                Snackbar.make(btnSignin, getString(R.string.server_under_maintenance), Snackbar.LENGTH_SHORT).show();
                            } else{
                                Snackbar.make(btnSignin, getString(R.string.try_again_later), Snackbar.LENGTH_SHORT).show();

                            }
                        }


                        /*{"data":
                            {"access_token":"db994f63-a5c2-4773-bc6e-cda14acd4de4",
                                    "expires_in":2.9562945E7,
                                    "refresh_token":"82b65df6-546a-4cd8-bb26-cd264500a4db",
                                    "scope":"read write trust",
                                    "token_type":"bearer"},
                            "status":"SUCCESS","statusCode":200}*/
                        /*{"data":
                            [{"resource":"Login","field":"email","code":"INVALID_USER",
                                "message":"User has no account"}],"status":"FAIL","statusCode":500}*/
                    } catch(JSONException e){
                        e.printStackTrace();
                    } catch(IOException e){
                        e.printStackTrace();
                    }

                    /*LoginData mLoginData = (LoginData) response.body();
                    InGaugeSession.write(getString(R.string.key_auth_token), mLoginData.getData().getAccessToken());
                    Logger.Error("Response " + mLoginData.getData().getAccessToken() + "\n" + mLoginData.getData().getRefreshToken() + "\n" + mLoginData.getData().getScope() + "\n" + mLoginData.getData().getExpiresIn());
                    GetUsersAPI(mLoginData.getData().getAccessToken());*/

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
                Snackbar.make(btnSignin, getString(R.string.try_again_later), Snackbar.LENGTH_SHORT).show();

            }
        });
    }


    /**
     * @param authToken
     */
    void GetUsersAPI(final String authToken){
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call mCall = apiInterface.getAccessToken();
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    UserData mUserData = (UserData) response.body();
                    InGaugeSession.write(getString(R.string.key_user_id), mUserData.getData().getId());
                    InGaugeSession.write(getString(R.string.key_user_name), mUserData.getData().getName());
                    Logger.Error("<<<< User Name  >>> " + mUserData.getData().getName());
                    // InGaugeSession.write(getString(R.string.key_selected_industry_id), mUserData.getData().getDefaultIndustry());
                    //getUserRoleAndTeneantList(authToken, mUserData.getData().getId());
                    check2FA(authToken, "1", true, mUserData.getData().getId(), true);

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
    }


    private void check2FA(final String authToken, String platform, boolean isMobile, final int userId, boolean isAuthyCheck){
        Call mCall = apiInterface.check2FA(userId, isAuthyCheck);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                JSONObject data = new JSONObject();
                boolean is2FARequired = false;
                if(response.body() != null){
                    try{
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        if(jsonObject.optString("status").equalsIgnoreCase("SUCCESS")){
                            data = jsonObject.optJSONObject("data");
                            is2FARequired = data.optBoolean("is2FARequired");
                        }
                    } catch(JSONException e){
                        e.printStackTrace();
                    }

                }
                //
                if(is2FARequired){
                    endProgress();
                    ///showSMSVerification(authToken,userId);
                    Intent intent = new Intent(SignInActivity.this, TwoFactorAuthActivity.class);
                    intent.putExtra("authToken", authToken);
                    intent.putExtra("userId", userId);
                    startActivity(intent);
                } else{

                    GetIndustry(authToken, userId);
                    //getUserRoleAndTeneantList(authToken, userId);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
    }


    /**
     * @param authToken
     * @param userId
     */
    void getUserRoleAndTeneantList(final String authToken, int userId, int industryId){


        TimeZone mTimeZone = TimeZone.getDefault();
        String mTimeZoneStr = mTimeZone.getID();
        TimeZone tz = TimeZone.getTimeZone(mTimeZoneStr);
        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);

        String timeZoneString = "";
        if(hours > 0){
            timeZoneString = String.format("GMT+%d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
        } else if(hours == 0){
            timeZoneString = String.format("GMT", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
            //String temptimeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
        } else{
            timeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
        }
        String completeauthToken = "bearer" + " " + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), "");
        Logger.Error("Time Zone : " + timeZoneString);
        Logger.Error("Industry ID : " + industryId);
        Logger.Error("<< Auth >>> " + completeauthToken);
        Logger.Error("<< Platform >>> " + 1);
        Logger.Error("<< Content-Type>>> " + "application/json");
        final Call mCall = apiInterface.getTenant
                ("application/json",
                        completeauthToken,
                        String.valueOf(1),
                        String.valueOf(industryId), true,
                        timeZoneString,
                        userId);

        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    TenantDataModel userRoleTenantListModel = (TenantDataModel) response.body();
                    if(userRoleTenantListModel.getData() != null && userRoleTenantListModel.getData().size() > 0){
                        InGaugeSession.write(getString(R.string.key_selected_tenant), userRoleTenantListModel.getData().get(0).getTenantName());
                        InGaugeSession.write(getString(R.string.key_selected_tenant_id), userRoleTenantListModel.getData().get(0).getTenantId());
                        Logger.Error(" <<<<<   ROLE ID   >>>>>>" + userRoleTenantListModel.getData().get(0).getRoleId());
                        InGaugeSession.write(getString(R.string.key_user_role_id), userRoleTenantListModel.getData().get(0).getRoleId());
                        InGaugeSession.write(getString(R.string.key_user_role_name), userRoleTenantListModel.getData().get(0).getRoleName());
                        //InGaugeSession.write(getString(R.string.key_selected_industry_id), userRoleTenantListModel.getData().get(0).getIndustryId());
                        InGaugeSession.write(getString(R.string.key_selected_industry), userRoleTenantListModel.getData().get(0).getIndustryDescription());
                        String tenantRoleName = getRoleName(userRoleTenantListModel.getData().get(0).getRoleRolePermissions());
                        InGaugeSession.write(getString(R.string.key_login_tenant_user_role), tenantRoleName);


                        //InGaugeSession.write(getString(R.string.key_login_tenant_user_role), userRoleTenantListModel.getData().get(0).getIndustryDescription());

                        getDefaultFilter(authToken, userRoleTenantListModel.getData().get(0).getTenantId(), userRoleTenantListModel.getData().get(0).getIndustryId());
                    } else{
                        InGaugeSession.write(getString(R.string.key_selected_tenant), "");
                        InGaugeSession.write(getString(R.string.key_selected_tenant_id), -1);
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
    }


    void GetIndustry(final String authToken, final int userId){


        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call mCall = apiInterface.getIndustry(UiUtils.getUserId(mContext));
        industryList = new ArrayList<>();
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                if(response.body() != null){
                    IndustryDataModel getIndstrydata = (IndustryDataModel) response.body();
                    industryList = getIndstrydata.getData();

                    if(isHotelIndustry){
                        if(industryList != null && industryList.size() > 0){
                            for(IndustryDataModel.Datum data : industryList){
                                if(data.getName().equals("Hotel")){
                                    InGaugeSession.write(getString(R.string.key_selected_industry_id), data.getId());
                                    break;
                                }
                            }
                        }
                    } else{
                        for(IndustryDataModel.Datum data : industryList){
                            if(data.getName().equals("CarRental")){
                                InGaugeSession.write(getString(R.string.key_selected_industry_id), data.getId());
                                break;
                            }
                        }


                    }
                    if(InGaugeSession.read(getResources().getString(R.string.key_selected_industry_id), 1).equals(2)){
                        if(industryList != null && industryList.size() > 0){
                            getUserRoleAndTeneantList(authToken, userId, industryList.get(0).getId());
                        }
                    } else if(InGaugeSession.read(getResources().getString(R.string.key_selected_industry_id), 1).equals(1)){
                        for(IndustryDataModel.Datum data : industryList){
                            if(data.getName().equals("Hotel")){
                                getUserRoleAndTeneantList(authToken, userId, data.getId());
                                break;
                            }
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call call, Throwable t){

            }
        });
    }

    public String getRoleName(List<TenantDataModel.RoleRolePermission> roleRolePermissions){
        if(roleRolePermissions != null && roleRolePermissions.size() > 0){
            for(int i = 0; i < roleRolePermissions.size(); i++){
                String name = roleRolePermissions.get(i).getName();
                if(name.equalsIgnoreCase("isPerformanceManager") || name.equalsIgnoreCase("isSuperAdmin") || name.equalsIgnoreCase("isFrontLineAssociate")){
                    return name;

                }
            }
        }
        return "";
    }

    /**
     * get Default Filter
     */
    void getDefaultFilter(String authToken, final int TenantID, final int IndustryId){
        Call mCall = apiInterface.getDefaultFilter(TenantID);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){

                    DefaultFilter defaultFilter = (DefaultFilter) response.body();

                    int RegionTypeId = defaultFilter.getData().getREGIONTYPE();
                    int RegionId = defaultFilter.getData().getREGION();
                    int TenantLocationId = defaultFilter.getData().getLOCATION();
                    int LocationGroupId = defaultFilter.getData().getLOCATIONGROUP();
                    int ProductId = defaultFilter.getData().getPRODUCT();
                    int UserID = defaultFilter.getData().getUSER();

                    InGaugeSession.write(getString(R.string.key_selected_regiontype), -2);
                    InGaugeSession.write(getString(R.string.key_selected_region), -2);
                    InGaugeSession.write(getString(R.string.key_tenant_location_id), -2);
                    InGaugeSession.write(getString(R.string.key_selected_location_group), -2);
                    InGaugeSession.write(getString(R.string.key_selected_location_group_product), -2);
                    InGaugeSession.write(getString(R.string.key_selected_user), -2);


                    getDefaultFilterForGoal(TenantID);
                    // getRolesAndPermission("application/json", InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token), null), true, String.valueOf(InGaugeSession.read(getString(R.string.key_selected_industry_id), 0)), String.valueOf(TenantID), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -1)));
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
    }

    /**
     * get Default Filter For Goal
     */
    void getDefaultFilterForGoal(final int TenantID){
        Call mCall = apiInterface.getDefaultFilterForGoal(TenantID, true);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){

                    DefaultFilter defaultFilter = (DefaultFilter) response.body();

                    //   int RegionTypeId = defaultFilter.getData().getREGIONTYPE();
                    // int RegionId = defaultFilter.getData().getREGION();
                    int TenantLocationId = defaultFilter.getData().getLOCATION();
                    int LocationGroupId = defaultFilter.getData().getLOCATIONGROUP();
                    int ProductId = defaultFilter.getData().getPRODUCT();
                    int UserID = defaultFilter.getData().getUSER();

                    InGaugeSession.write(getString(R.string.key_selected_tenant_location_id_for_default_goal), TenantLocationId);
                    InGaugeSession.write(getString(R.string.key_selected_location_group_id_for_default_goal), String.valueOf(LocationGroupId));
                    //InGaugeSession.write(getString(R.string.key_selected_location_group_product_id_for_default_goal), ProductId);
                    //This -1 is put static Due to in Goal Setting Default Filter value in Product is Incremental revenue. As In Goal Settings Filter show default Incremental Revenue at first time
                    InGaugeSession.write(getString(R.string.key_selected_location_group_product_id_for_default_goal), -1);
                    InGaugeSession.write(getString(R.string.key_selected_user_id_for_default_goal), UserID);
                    getRolesAndPermission("application/json",
                            InGaugeSession.read(mContext.getResources().getString(R.string.key_auth_token),
                                    null), true,
                            String.valueOf(InGaugeSession.read(getString(R.string.key_selected_industry_id), 0)),
                            String.valueOf(TenantID), String.valueOf(InGaugeSession.read(mContext.getResources().getString(R.string.key_user_id), -1)));
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
    }

    private void getRolesAndPermission(String ContentType, String Authorization, boolean isMobile, final String IndustryId, String TenantId, final String UserId){
        Call mCall = apiInterface.getRolesAndPermission(TenantId, UserId);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                JSONObject data = new JSONObject();
                if(response.body() != null){
                    Logger.Error("ROLES AND PERMISSION RESPONSE " + response.body());
                    try{
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        if(jsonObject.optString("status").equalsIgnoreCase("SUCCESS")){
                            data = jsonObject.optJSONObject("data");
                        }
                        InGaugeSession.write(mContext.getResources().getString(R.string.key_user_role_permission), data.toString());
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    getMobileKeys(IndustryId);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
    }


    void getMobileKeys(String IndustryId){
        Call mCall = apiInterface.getMobileKeys();
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){
                JSONObject data = null;
                if(response.body() != null){
                    Logger.Error("KEYS RESPONSE " + response.body());
                    try{
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        data = jsonObject.optJSONObject("data");
                    } catch(JSONException e){
                        e.printStackTrace();
                    }


                    InGaugeSession.write(getString(R.string.key_user_user_email), edtEmail.getText().toString().trim());
                    Bundle bundle = new Bundle();
                    bundle.putString("email", edtEmail.getText().toString().trim());
                    bundle.putString("role", InGaugeSession.read(getString(R.string.key_user_role_name), ""));
                    bundle.putString("industry", InGaugeSession.read(getString(R.string.key_selected_industry), ""));
                    bundle.putString("success", "Registration Successful");
                    InGaugeApp.getFirebaseAnalytics().logEvent("sign_in", bundle);

                    DatabaseHandler db = new DatabaseHandler(SignInActivity.this);
                    db.addKeys(String.valueOf(data));
                    // endProgress();
                    //GetDeviceId(InGaugeSession.read(getString(R.string.key_auth_token), ""));
                    if(response.body() != null){
                        endProgress();
                        InGaugeSession.write(getString(R.string.key_guide_enabled), true);


                        InGaugeSession.write(getString(R.string.key_tag_index), 4);
                        InGaugeSession.write(getString(R.string.key_from_date_range_index), 1);
                        InGaugeSession.write(getString(R.string.key_compare_date_range_index), 1);

                        Intent mIntent = new Intent(SignInActivity.this, HomeActivity.class);
                        startActivity(mIntent);
                        finish();
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
    }

    /**
     * @param authToken
     */
    void GetDeviceId(final String authToken){

        String deviceId = InGaugeSession.read(this.getResources().getString(R.string.key_device_id_for_fcm), "");
        if(TextUtils.isEmpty(deviceId)){
            try{
                deviceId = FirebaseInstanceId.getInstance().getToken();
                InGaugeSession.write(getResources().getString(R.string.key_device_id_for_fcm), deviceId);
                Logger.Error("Referesh Token " + deviceId);
            } catch(Exception e){
                e.printStackTrace();
            }
        }
        Logger.Error("Device Id : " + deviceId);
        UserBody mUserBody = new UserBody();
        int userID = InGaugeSession.read(getString(R.string.key_user_id), -1);

        if(userID > 0){
            mUserBody.id = userID;
        }
        mUserBody.deviceId = deviceId;
        mUserBody.platform = 1;
        Gson gson = new Gson();
        String jsonStringUserBody = gson.toJson(mUserBody).toString();
        JSONObject mJsonObject = null;
        try{
            mJsonObject = new JSONObject(jsonStringUserBody);
        } catch(JSONException e){
            e.printStackTrace();
        }

        Logger.Error("Json Body to Get Device ID  " + jsonStringUserBody);
        //  if (mJsonObject!=null){
        Call mCall = apiInterface.getDeviceId(mUserBody);
        mCall.enqueue(new Callback(){
            @Override
            public void onResponse(Call call, Response response){

                if(response.body() != null){
                    endProgress();
                    Intent mIntent = new Intent(SignInActivity.this, HomeActivity.class);
                    startActivity(mIntent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t){
                endProgress();
            }
        });
        // }

    }

    public void LoginChoice(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        AlertDialog dialog;
        builder.setTitle("");
        builder.setMessage(mContext.getResources().getString(R.string.alert_message_ec));
        builder.setCancelable(false);

        builder.setPositiveButton(
                getResources().getString(R.string.alert_yes),
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        UiUtils.baseUrl = APIClient.ECbaseUrl;
                        UiUtils.baseUrlChart = APIClient.ECbaseUrlChart;
                        apiInterface = APIClient.getClientForLogin().create(APIInterface.class);
                        LoginAPI(edtEmail.getText().toString(), UiUtils.md5(edtPassWord.getText().toString()));
//                        LoginAPIProduction(edtEmail.getText().toString(), UiUtils.md5(edtPassWord.getText().toString()));
                    }
                });

        builder.setNegativeButton(
                getResources().getString(R.string.alert_no), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        UiUtils.baseUrl = APIClient.UATbaseUrl;
                        UiUtils.baseUrlChart = APIClient.UATbaseUrlChart;
                        apiInterface = APIClient.getClientForLogin().create(APIInterface.class);
                        LoginAPI(edtEmail.getText().toString(), UiUtils.md5(edtPassWord.getText().toString()));
//                        LoginAPIProduction(edtEmail.getText().toString(), UiUtils.md5(edtPassWord.getText().toString()));
                    }
                }
        );
        //  builder.show();
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

}
