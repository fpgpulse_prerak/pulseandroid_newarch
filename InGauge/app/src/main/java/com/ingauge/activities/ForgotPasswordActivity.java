package com.ingauge.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pathanaa on 16-05-2017.
 */

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {
    private EditText edtEmail;
    private TextView tv_title;
    private TextInputLayout mTextInputLayoutEmail;
    private AppCompatButton btn_getpass;
    APIInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_forgotpassword);
        Init();
    }

    private void Init() {
        edtEmail = (EditText) findViewById(R.id.edt_email);
        mTextInputLayoutEmail = (TextInputLayout) findViewById(R.id.til_email);
        btn_getpass = (AppCompatButton) findViewById(R.id.btn_getpass);
        btn_getpass.setOnClickListener(this);
        tv_title = (TextView) findViewById(R.id.tv_custom_title);
        tv_title.setText(getResources().getString(R.string.title_forgotpassword));
        edtEmail.addTextChangedListener(new TextChange(edtEmail));
        apiInterface = APIClient.getClientForCreateAccount().create(APIInterface.class);
    }

    private void getPassword(boolean isMobile, int platform, final String email) {
        Call mCall = apiInterface.getPassword(email);
        startProgress(ForgotPasswordActivity.this);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                endProgress();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        String alertMessage = "";
                        boolean isSucess = true;
                        if (jsonObject.optString("status").equalsIgnoreCase("SUCCESS")) {
                            alertMessage = "You requested to reset the password for account " + email + ", an email was sent to this mailbox, please follow the email of reset your password.";
                        } else {
                            if (jsonObject.optInt("statusCode") == 500) {

                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                JSONObject jobj = jsonArray.getJSONObject(0);
                                alertMessage = jobj.optString("message");

                            } else {
                                alertMessage = "Something went wrong, try again";
                            }
                            isSucess = false;
                        }
                        final boolean finalIsSucess = isSucess;
                        new AlertDialog.Builder(ForgotPasswordActivity.this)
                                .setTitle("Email Sent Successfully")
                                .setMessage(alertMessage)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        if (finalIsSucess) {
                                            finish();
                                        }

                                    }
                                }).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                endProgress();
            }
        });
    }

    class TextChange implements TextWatcher {
        View view;

        private TextChange(View v) {
            view = v;
        }//end constructor

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()) {
                case R.id.edt_email:
                    String email = edtEmail.getText().toString().trim();
                    if (email.isEmpty() || !UiUtils.isValidEmail(email)) {
                        mTextInputLayoutEmail.setError(getString(R.string.error_empty_email));
                        requestFocus(mTextInputLayoutEmail);
                    } else {
                        mTextInputLayoutEmail.setError(null);
                        mTextInputLayoutEmail.setErrorEnabled(false);
                    }
                    break;
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    private boolean validateEmail() {
        String email = edtEmail.getText().toString().trim();
        if (email.isEmpty()) {
            mTextInputLayoutEmail.setError(getString(R.string.error_empty_email));
            requestFocus(mTextInputLayoutEmail);
            return false;
        } else if (!UiUtils.isValidEmail(email)) {
            mTextInputLayoutEmail.setError(getString(R.string.error_invalid_email));
            requestFocus(mTextInputLayoutEmail);
            return false;
        } else {
            mTextInputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        if (!validateEmail()) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("email", edtEmail.getText().toString().trim());
        InGaugeApp.getFirebaseAnalytics().logEvent("forgot_password", bundle);
        getPassword(true, 1, edtEmail.getText().toString().trim());
    }
}
