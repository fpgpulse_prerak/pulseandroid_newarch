package com.ingauge.anim;

/**
 * Created by mansurum on 11-Jan-18.
 */

public class InGaugeBounceInterpolator implements android.view.animation.Interpolator{
    private double mAmplitude = 1;
    private double mFrequency = 10;

    public InGaugeBounceInterpolator(double amplitude, double frequency){
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time){
        return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }
}