package com.ingauge.listener;

import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.ObservationJobType;

import java.util.ArrayList;

public interface ObservationDashboardFilterListener {
        public void updateData(String itemName,String itemId,int position,ArrayList<FilterBaseData> mFilterBaseDatasforObsFilter);
    }