package com.ingauge.listener;

import android.view.View;

import com.ingauge.pojo.FilterBaseData;
import com.ingauge.pojo.FilterBaseDataForLocationGroup;

/**
 * Created by mansurum on 23-May-17.
 */

public interface RvClickListenerForLocationGrpSocialInteraction {

    void recyclerViewListClicked(View v, FilterBaseDataForLocationGroup mFilterBaseData, int position, int filterPosition, boolean isLeftFilter);

}
