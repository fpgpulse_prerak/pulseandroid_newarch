package com.ingauge.listener;

/**
 * Created by mansurum on 02-Feb-18.
 */

public interface LeaderboardClickListener{

    public void getClickData(int position, String name,int reportId);
}
