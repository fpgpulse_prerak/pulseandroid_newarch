package com.ingauge.listener;

import android.view.View;

/**
 * Created by mansurum on 23-May-17.
 */

public interface RecyclerViewClickListenerForDashboardDetails {

    void recyclerViewListClicked(View v, int position);

}
