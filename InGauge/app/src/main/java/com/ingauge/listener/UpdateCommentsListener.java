package com.ingauge.listener;

import com.ingauge.pojo.FeedsModelList;

import java.util.List;

/**
 * Created by pathanaa on 22-06-2017.
 */

public interface UpdateCommentsListener {
    //public void sendComment(FeedsModelList feedsModelList);
    public void sendComment(List<FeedsModelList.Datum> datumList,int position);
}
