package com.ingauge.listener;

import com.ingauge.pojo.FilterBaseData;

import java.util.List;

/**
 * Created by mansurum on 24-May-17.
 */

public interface OnFilterDataSetListener {

    public void onFilterDetailsClicked(FilterBaseData mFilterBaseData, int clickedPositon, int filterpositoin);
}
