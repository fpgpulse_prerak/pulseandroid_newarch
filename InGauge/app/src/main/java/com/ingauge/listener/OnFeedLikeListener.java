package com.ingauge.listener;

import com.ingauge.pojo.FeedsModelList;

import java.util.List;

/**
 * Created by desainid on 6/21/2017.
 */

public interface OnFeedLikeListener {
    public int onLikeFeed(List<FeedsModelList.Datum> mFeedModelList, int clickedPositon);


}
