package com.ingauge.listener;

import android.webkit.JavascriptInterface;



public interface LoadListener {
    @SuppressWarnings("unused")
    @JavascriptInterface
    public void processHTML(String html);
}