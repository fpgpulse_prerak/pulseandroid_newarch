package com.ingauge.listener;

import android.view.View;

import com.ingauge.pojo.FilterBaseData;

/**
 * Created by mansurum on 23-May-17.
 */

public interface RecyclerViewClickListenerForMail {

    void recyclerViewListClicked(View v, int position);

}
