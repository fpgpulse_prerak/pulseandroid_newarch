package com.ingauge.listener;

/**
 * Created by mansurum on 05-Jan-18.
 */

public interface VideoListClickListener{

    public void onVideoClick(String mediaId,String tabIndex,String VideoName);
}
