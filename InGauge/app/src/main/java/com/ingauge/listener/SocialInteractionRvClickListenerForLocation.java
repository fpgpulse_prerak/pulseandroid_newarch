package com.ingauge.listener;

import android.view.View;

import com.ingauge.pojo.FilterBaseData;

import java.util.List;

/**
 * Created by mansurum on 23-May-17.
 */

public interface SocialInteractionRvClickListenerForLocation {

    void recyclerViewListClicked(View v, FilterBaseData mFilterBaseData, int position, int filterPosition, List<FilterBaseData> mFilterBaseDataList);

}
