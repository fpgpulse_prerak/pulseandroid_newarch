package com.ingauge.temp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.MostWatchedVideoListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.fragments.VideoFragment;
import com.ingauge.listener.VideoListClickListener;
import com.ingauge.pojo.VideoModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Anu on 22/04/17.
 */


public class FragmentTwo extends Fragment implements Observer,VideoListClickListener{

    APIInterface apiInterface;
    private HomeActivity mHomeActivity;
    private RelativeLayout rlProgress;
    View rootView;
    private RecyclerView rvVideoList;
    List<VideoModel> videoModelList = new ArrayList<>();
    MostWatchedVideoListAdapter mVideoListAdapter;
    private TextView tvNoVideoFound;
    Call<ResponseBody>  mCall;
    public FragmentTwo(){
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_two, container, false);
        rvVideoList = (RecyclerView) rootView.findViewById(R.id.fragment_two_rv_video_list);
        rlProgress = (RelativeLayout) rootView.findViewById(R.id.custom_progress_rl_main);
        tvNoVideoFound = (TextView)rootView.findViewById(R.id.fragment_two_tv_no_video_found);
        getMostWatchedVideo();
        return rootView;
    }

    @Override
    public void update(Observable o, Object arg){
        View view = getView();
    }

    public void getMostWatchedVideo(){

        String type = "mostWatchedList";
        if(getRoleStatus()){
            type = "mostWatchedSuperAdmin";
        }
        mCall = apiInterface.getAllMedia(InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), 0),
                InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_role_id), 0), type);
        mCall.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){

                rlProgress.setVisibility(View.GONE);
                int count = 0;
                if(response.body() != null){

                    try{
                        String jsonResponse = response.body().string();
                        Logger.Error("<<< Response  >>>>> " + jsonResponse);
                        JSONObject mJsonObject = new JSONObject(jsonResponse);
                        int statusCode = mJsonObject.optInt("statusCode");
                        JSONObject mDataJsonObject = mJsonObject.optJSONObject("data");
                        if(statusCode == 200){
                            Iterator iterator = mDataJsonObject.keys();
                            while(iterator.hasNext()){
                                String key = (String) iterator.next();
                                JSONArray mJonArray = mDataJsonObject.optJSONArray(key);
                                fillVideoHashMap(mJonArray, key);
                                count++;
                            }

                            setupRecycler();


                        }else {
                            rvVideoList.setVisibility(View.GONE);
                            tvNoVideoFound.setVisibility(View.VISIBLE);
                        }
                    } catch(IOException e){
                        e.printStackTrace();
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    /*endProgress();
                    Intent mIntent = new Intent(SignInActivity.this, HomeActivity.class);
                    startActivity(mIntent);
                    finish();*/
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){
                /*endProgress();*/
                rlProgress.setVisibility(View.GONE);
                rvVideoList.setVisibility(View.GONE);
                tvNoVideoFound.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setupRecycler(){


        if(videoModelList != null && videoModelList.size() > 0){
            mVideoListAdapter = new MostWatchedVideoListAdapter(mHomeActivity, videoModelList,this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            rvVideoList.setLayoutManager(mLayoutManager);
            rvVideoList.setItemAnimator(new DefaultItemAnimator());
            rvVideoList.setAdapter(mVideoListAdapter);
            tvNoVideoFound.setVisibility(View.GONE);
            rvVideoList.setVisibility(View.VISIBLE);
        } else{
            tvNoVideoFound.setVisibility(View.VISIBLE);
            rvVideoList.setVisibility(View.GONE);
        }

    }

    public void fillVideoHashMap(JSONArray mJsonArray, String key){


        for(int i = 0; i < mJsonArray.length(); i++){
            JSONObject mJsonObject = (JSONObject) mJsonArray.opt(i);
            VideoModel videoModel = new VideoModel();
            videoModel.setId(mJsonObject.optInt("id"));
            videoModel.setIndustryId(mJsonObject.optInt("industryId"));
            videoModel.setActiveStatus(mJsonObject.optString("activeStatus"));
            videoModel.setCreatedOn(mJsonObject.optString("createdOn"));
            videoModel.setUpdatedOn(mJsonObject.optString("updatedOn"));
            videoModel.setIndexRelatedOnly(mJsonObject.optBoolean("indexRelatedOnly"));
            videoModel.setIndexMainOnly(mJsonObject.optBoolean("indexMainOnly"));
            videoModel.setMediaId(mJsonObject.optString("mediaId"));
            videoModel.setName(mJsonObject.optString("name"));
            videoModel.setDuration(mJsonObject.optString("duration"));
            videoModel.setDescription(mJsonObject.optString("description"));
            videoModel.setStatus(mJsonObject.optString("status"));
            videoModel.setThumbnailURL(mJsonObject.optString("thumbnailURL"));
            videoModel.setProjectId(mJsonObject.optString("projectId"));
            videoModel.setProjectName(mJsonObject.optString("projectName"));
            videoModel.setEmbedCode(mJsonObject.optString("embedCode"));
            videoModel.setPlayCount(mJsonObject.optInt("playCount"));
            videoModel.setSection(mJsonObject.optString("section"));
            videoModel.setMediaAssets(mJsonObject.optString("mediaAssets"));
            videoModel.setDeleted(mJsonObject.optBoolean("isDeleted"));
            videoModel.setRoleMedia(mJsonObject.optString("roleMedia"));
            videoModelList.add(videoModel);
        }


    }

    private boolean getRoleStatus(){
        String roleName = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_login_tenant_user_role), "");
        if(roleName.equalsIgnoreCase(mHomeActivity.getResources().getString(R.string.is_super_admin))){
            return true;
        } else{
            return false;
        }

    }

    @Override
    public void onVideoClick(String mediaId, String tabIndex,String videoName){
        String strmediaId= mediaId;
        String emailId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_user_email), "");
        int userID = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), -2);
        int IndustryId=InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), -2);

        //http://75.101.236.193:3000/vtPlayer?email=prerak@fpgpulse.com&industryId=1&userId=646&authorizationId=3cb9a188-1302-4657-b09d-f5914b19f9f2&mediaHasId=twckbxfoab
        String videoUrl = UiUtils.baseUrlChart + "vtPlayer?" + "email=" + emailId + "&userId="+userID + "&mediaHasId=" + strmediaId + "&industryId=" + IndustryId
                + "&authorizationId=" + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), "");
        VideoFragment.webVideoUrl = videoUrl;
        VideoFragment.VideoName = videoName;
        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_play_video_url),videoUrl);
        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_video_name), videoName);
        VideoFragment.loadwebViewForVideo();
    }

    @Override
    public void onDetach(){
        super.onDetach();
        if ( mCall != null )
            mCall.cancel ();
    }
}