package com.ingauge.temp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.adapters.VideoListAdapter;
import com.ingauge.api.APIClient;
import com.ingauge.fragments.VideoFragment;
import com.ingauge.listener.VideoListClickListener;
import com.ingauge.pojo.VideoModel;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Anu on 22/04/17.
 */


public class FragmentOne extends Fragment implements Observer, VideoListClickListener{

    RecyclerView mRecyclerView;

    private List<VideoModel> videoModelList = new ArrayList<>();

    private VideoListAdapter mVideoListAdapter;
    private RelativeLayout rlProgress;
    private View rootView;
    private HomeActivity mHomeActivity;
    private TextView tvNoVideoFound;

    public FragmentOne(){
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mHomeActivity = (HomeActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_one, container, false);
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        tvNoVideoFound = (TextView) view.findViewById(R.id.fragment_one_tv_no_video_found);
        setupRecycler(VideoFragment.KEY);
    }

    private void setupRecycler(String categoryKey){

        videoModelList = VideoFragment.mVideoHashMap.get(categoryKey);
        if(videoModelList != null && videoModelList.size() > 0){

            mRecyclerView.setVisibility(View.VISIBLE);
            tvNoVideoFound.setVisibility(View.GONE);

            mVideoListAdapter = new VideoListAdapter(mHomeActivity, videoModelList, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mVideoListAdapter);

        } else{
            mRecyclerView.setVisibility(View.GONE);
            tvNoVideoFound.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void update(Observable o, Object arg){
        View root = getView();
        Logger.Error("<<<< Clicked On Observer >>>> " + VideoFragment.KEY);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleview);
        //setupRecycler(VideoFragment.KEY);
        mVideoListAdapter = null;
        videoModelList = new ArrayList<>();
        /*if(VideoFragment.KEY.equalsIgnoreCase("All Videos")){
            videoModelList.clear();
            for(String key : VideoFragment.mVideoHashMap.keySet()){
                videoModelList.addAll(VideoFragment.mVideoHashMap.get(key));
            }
        } else{
            videoModelList.clear();*/
            videoModelList = VideoFragment.mVideoHashMap.get(VideoFragment.KEY);
        /*}*/

        if(videoModelList != null && videoModelList.size() > 0){

            mRecyclerView.setVisibility(View.VISIBLE);
            tvNoVideoFound.setVisibility(View.GONE);
            mVideoListAdapter = new VideoListAdapter(mHomeActivity, videoModelList, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mVideoListAdapter);
        } else{
            mRecyclerView.setVisibility(View.GONE);
            tvNoVideoFound.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void onVideoClick(String mediaId, String tabIndex, String videoName){

        String strmediaId = mediaId;
        String emailId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_user_email), "");
        int userID = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_user_id), -2);
        int IndustryId = InGaugeSession.read(mHomeActivity.getResources().getString(R.string.key_selected_industry_id), -2);
        String videoUrl = UiUtils.baseUrlChart + "vtPlayer?" + "email=" + emailId + "&userId=" + userID + "&mediaHasId=" + strmediaId + "&industryId=" + IndustryId
                + "&authorizationId=" + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), "");
        VideoFragment.webVideoUrl = videoUrl;
        VideoFragment.VideoName = videoName;
        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_play_video_url),videoUrl);
        InGaugeSession.write(mHomeActivity.getResources().getString(R.string.key_video_name), videoName);
        VideoFragment.loadwebViewForVideo();
    }
}