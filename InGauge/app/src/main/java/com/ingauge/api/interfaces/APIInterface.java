package com.ingauge.api.interfaces;

import com.google.gson.JsonObject;
import com.ingauge.pojo.AccessibleTenantLocationDataModel;
import com.ingauge.pojo.AccessibleTenantLocationDataModelForAgentProfile;
import com.ingauge.pojo.AgentModel;
import com.ingauge.pojo.AgentProfileDetailsPojo;
import com.ingauge.pojo.DashboardGraphList;
import com.ingauge.pojo.DashboardLocationGroupListModel;
import com.ingauge.pojo.DashboardModelList;
import com.ingauge.pojo.DashboardUserListModel;
import com.ingauge.pojo.DefaultFilter;
import com.ingauge.pojo.DeleteMailBody;
import com.ingauge.pojo.EmailBody;
import com.ingauge.pojo.FeedsModelList;
import com.ingauge.pojo.FetchMailBody;
import com.ingauge.pojo.GamificationPostAPIPojo;
import com.ingauge.pojo.IndustryDataModel;
import com.ingauge.pojo.LoginData;
import com.ingauge.pojo.MailListDataModel;
import com.ingauge.pojo.MenuModel;
import com.ingauge.pojo.NotificationSettingsModel;
import com.ingauge.pojo.ObsQuestionModel;
import com.ingauge.pojo.ObservationDashboardModel;
import com.ingauge.pojo.ObservationEditViewModel;
import com.ingauge.pojo.ObservationJobType;
import com.ingauge.pojo.ObservationListModel;
import com.ingauge.pojo.ObservationSurveyCategoryModel;
import com.ingauge.pojo.PerformanceLeaderModel;
import com.ingauge.pojo.ProductListFromLocationGroupModel;
import com.ingauge.pojo.RecipientListModel;
import com.ingauge.pojo.RegionByTenantRegionTypeModel;
import com.ingauge.pojo.RegionTypeDataModel;
import com.ingauge.pojo.ReplyMailBody;
import com.ingauge.pojo.ReportDetailFeedsPojo;
import com.ingauge.pojo.SaveFeedCommentModel;
import com.ingauge.pojo.SaveLikeFeedModel;
import com.ingauge.pojo.ShareFeedModel;
import com.ingauge.pojo.ShareReportPojo;
import com.ingauge.pojo.TenantDataModel;
import com.ingauge.pojo.UserBody;
import com.ingauge.pojo.UserData;
import com.ingauge.pojo.UserNotificationListModel;
import com.ingauge.pojo.UserRoleTenantListModel;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by mansurum on 09-May-17.
 */

public interface APIInterface{

    @GET("api/user/secure/getByToken")
    Call<UserData> getAccessToken();

/*    @GET("api/user/secure/getByToken")
    Call<UserData> getUserInfo();*/


    @POST("api/user/secure/getDeviceID")
    Call<ResponseBody> getDeviceId(@Body UserBody userBody);


    @GET("api/industry/getAccessibleIndustry/{user_id}")
    Call<IndustryDataModel> getIndustry(@Path("user_id") int userID);


    @GET("api/userTenantRole/secure/getUserTenantRole")
    Call<TenantDataModel> getTenant(@Header("Content-Type") String ContentType,
                                    @Header("Authorization") String authToken,
                                    @Header("platform") String platform,
                                    @Header("IndustryId") String IndustryId,
                                    @Header("isMobile") boolean isMobile,
                                    @Header("TimeZone") String TimeZone,
                                    @Query("userID") int userid);

    /*@GET("api/user/avatar/1/646")
    Call<ResponseBody> getUserAvatar(@Header("Content-Type") String ContentType, @Header("Authorization") String authToken, @Header("platform") String platform, @Header("IndustryId") String IndustryId);*/

    @GET("api/dashboard/secure/{dashboard_id}")
    Call<DashboardGraphList> getDashboardGraph(@Path("dashboard_id") int dashboard_id);

    @GET("api/dashboard/secure/m/{dashboard_id}")
    Call<ResponseBody> getDashboardCustomReports(@Path("dashboard_id") int dashboard_id,
                                                 @Query("tenantId") int tenantId,
                                                 @Query("regionTypeId") int regionTypeId,
                                                 @Query("regionId") int regionId,
                                                 @Query("productId") int productId,
                                                 @Query("userId") int userId,
                                                 @Query("tenantLocationId") int tenantLocationId,
                                                 @Query("locationGroupId") int locationGroupId,
                                                 @Query("metricsDateType") String metricsDateType,
                                                 @Query("from") String from,
                                                 @Query("to") String to,
                                                 @Query("compFrom") String compFrom,
                                                 @Query("compTo") String compTo,
                                                 @Query("fromDateTime") Long fromDateTime,
                                                 @Query("toDateTime") Long toDateTime,
                                                 @Query("compFromDateTime") Long compFromDateTime,
                                                 @Query("compToDateTime") Long compToDateTime,
                                                 @Query("isCompare") boolean isCompare);


    @GET("api/dashboard/secure/m/filters/{dashboard_id}")
    Call<ResponseBody> getDashboardFiltersCheck(@Path("dashboard_id") int dashboard_id,
                                                @Query("tenantId") int tenantId,
                                                @Query("regionTypeId") int regionTypeId,
                                                @Query("regionId") int regionId,
                                                @Query("productId") int productId,
                                                @Query("userId") int userId,
                                                @Query("tenantLocationId") int tenantLocationId,
                                                @Query("locationGroupId") int locationGroupId,
                                                @Query("metricsDateType") String metricsDateType,
                                                @Query("from") String from,
                                                @Query("to") String to,
                                                @Query("compFrom") String compFrom,
                                                @Query("compTo") String compTo,
                                                @Query("fromDateTime") Long fromDateTime,
                                                @Query("toDateTime") Long toDateTime,
                                                @Query("compFromDateTime") Long compFromDateTime,
                                                @Query("compToDateTime") Long compToDateTime,
                                                @Query("isCompare") boolean isCompare);

    @GET("api/dashboard/secure/findSpecial")
    Call<DashboardModelList> getDashboardList(@Query("tenantId") int tenantId,
                                              @Query("userId") int userId,
                                              @Query("roleId") int roleId,
                                              @Query("getSection") boolean getSection);


    @POST("api/oneToOneType/secure/m/find")
    Call<ObservationJobType> GetObsJobType(@Body JsonObject requestBody);

    @POST("api/survey/secure/getSurveys")
    Call<ObservationDashboardModel> GetObsSurveyType(@Body JsonObject requestBody,
                                                     @Query("tenantId") int tenantId,
                                                     @Query("userId") String userId);

    @POST("api/survey/secure")
    Call<JsonObject> SaveObservation(@Body JsonObject requestBody);


    @PUT("api/survey/secure")
    Call<JsonObject> EditObservation(@Body JsonObject requestBody);

    //"application/json", "bearer" + " " + authToken,ContentType,IndustryId,isMobile
    @GET("api/questionnaire/secure/m/{observation_id}")
    Call<ObsQuestionModel> GetObsQuestions(@Path("observation_id") String observationId);


    @POST("api/user/authenticateUser")
    Call<ResponseBody> getUserLogin(@Body JsonObject requestBody);


    @FormUrlEncoded
    @POST("oauth/token")
    Call<LoginData> getUserLoginProd(
            @Header("platform") String platform,
            @Field("client_id") String clientId,
            @Field("client_secret") String secret,
            @Field("grant_type") String grantType,
            @Field("username") String userName,
            @Field("password") String password);


    @GET("api/industry/getIndustry")
    Call<IndustryDataModel> getIndustryList();

    @GET("api/userTenantRole/secure/getUserTenantRole")
    Call<UserRoleTenantListModel> getUserRoleAndTenantList(@Query("userID") String userId);

    @GET("api/regionType/secure/getRegionTypeByTenantId/{tenantId}")
    Call<RegionTypeDataModel> getRegionTypes(@Path("tenantId") int tenantId);


    @GET("api/util/getDefaultFilters/{tenantId}")
    Call<DefaultFilter> getDefaultFilter(@Path("tenantId") int tenantId);

    @GET("api/util/getDefaultFilters/{tenantId}")
    Call<DefaultFilter> getDefaultFilterForGoal(@Path("tenantId") int tenantId,
                                                @Query("isGoalProgress") boolean isGoalProgress);


    @GET("api/region/secure/getRegionByTenantRegionType/{tenant_id}/{regionType_id}")
    Call<RegionByTenantRegionTypeModel> getRegionByTenantRegionType(@Path("tenant_id") int tenantId,
                                                                    @Path("regionType_id") int regionTypeId,
                                                                    @Query("accessible") boolean isAccessible);


    @GET("api/tenantLocation/secure/getAccessibleTenantLocation/{tenant_id}/{user_id}")
    Call<AccessibleTenantLocationDataModel> getAccessibleTenantLocation(@Path("tenant_id") int tenantId,
                                                                        @Path("user_id") String userId,
                                                                        @Query("orderBy") String orderBy,
                                                                        @Query("sort") String sort,
                                                                        @Query("regionId") String regionId,
                                                                        @Query("activeStatus") String activeStatus,
                                                                        @Query("displayMasterLocation") boolean displayMasterLocation);

    @GET("api/tenantLocation/secure/getAccessibleTenantLocation/{tenant_id}/{user_id}")
    Call<AccessibleTenantLocationDataModel> getAccessibleTenantLocationForSocialInteraction(@Path("tenant_id") int tenantId,
                                                                                            @Path("user_id") String userId,
                                                                                            @Query("orderBy") String orderBy,
                                                                                            @Query("sort") String sort,
                                                                                            @Query("regionId") String regionId,
                                                                                            @Query("activeStatus") String activeStatus);

    @GET("api/tenantLocation/secure/getAccessibleTenantLocation/{tenant_id}/{user_id}")
    Call<AccessibleTenantLocationDataModel> getAccessibleTenantLocationForGoalFilter(@Path("tenant_id") int tenantId,
                                                                                     @Path("user_id") String userId,
                                                                                     @Query("orderBy") String orderBy,
                                                                                     @Query("sort") String sort,
                                                                                     @Query("activeStatus") String activeStatus);

    @GET("api/tenantLocation/secure/getAccessibleTenantLocationWithMasterLocation/{tenant_id}/{user_id}")
    Call<AccessibleTenantLocationDataModelForAgentProfile> getAccessibleTenantLocationForAgentProfile(@Path("tenant_id") int tenantId,
                                                                                                      @Path("user_id") String userId,
                                                                                                      @Query("orderBy") String orderBy,
                                                                                                      @Query("sort") String sort,
                                                                                                      @Query("activeStatus") String activeStatus,
                                                                                                      @Query("displayMasterLocation") boolean displayMasterLocation);

    @GET("api/locationGroup/secure/isOperator/{tenant_id}/{tenant_locationId}/{user_id}")
    Call<ResponseBody> getIsOperator(@Path("tenant_id") int tenantId,
                                     @Path("tenant_locationId") String tenantLocationId,
                                     @Path("user_id") String userId);


    @DELETE("api/survey/secure/{observation_id}")
    Call<JsonObject> deleteObservation(@Path("observation_id") String ObservationId);

    @GET("api/survey/secure/{observation_id}")
    Call<ObservationEditViewModel> getEventDetailObs(@Path("observation_id") String ObservationId);

    @GET("api/user/secure/getUsersWithPermissionForSurvey/{tenant_id}")
    Call<PerformanceLeaderModel> getPerformanceLeader(@Path("tenant_id") int tenantId,
                                                      @Query("tenantLocationId") String tenantLocationId,
                                                      @Query("permissionName") String permissionName,
                                                      @Query("getOperatorsOnly") boolean getOperatorsOnly,
                                                      @Query("getusersWhoSetMasLoc") boolean getusersWhoSetMasLoc,
                                                      @Query("getFPGMembers") boolean getFPGMembers);

    @GET("api/surveyCategory/secure/getSurveyCategory")
    Call<ObservationSurveyCategoryModel> getSurveyCategoryObservation(@Query("activeStatus") String activeStatus,
                                                                      @Query("surveyEntityType") String surveyEntityType
    );

    @GET("api/user/secure/getUsersForSurvey/{tenant_id}")
    Call<AgentModel> getAgent(@Path("tenant_id") int tenantId,
                              @Query("tenantLocationId") String tenantLocationId,
                              @Query("getContributorsOnly") int getContributor,
                              @Query("getWithAllStatus") int getWithAllStatus,
                              @Query("getOperatorsOnly") int getoperators,
                              @Query("permissionName") String permissionName,
                              @Query("decideContributorOpratorLocationLevel") boolean decideContributorOpratorLocationLevel,
                              @Query("getusersWhoSetMasLoc") boolean getusersWhoSetMasLoc,
                              @Query("getFPGMembers") boolean getFPGMembers);

    @GET("api/questionnaire/secure/getQuestionnaire")
    Call<ObservationListModel> getObservation(@Query("tenantLocationId") int tenantlocationId,
                                              @Query("surveyEntityType") String surveyEntityType);


    @GET("api/locationGroup/secure/getDashboardLocationGroupList/{tenantLocationId}/{user_id}")
    Call<DashboardLocationGroupListModel> getDashboardLocationGroupList(@Path("tenantLocationId") String tenantlocationId,
                                                                        @Path("user_id") String userId);


    @GET("api/locationGroup/secure/assignedProduct")
    Call<ProductListFromLocationGroupModel> getProductList(@Query("locationGroupId") String locationGroupId);


    //    http://54.196.98.18/pulse/api/user/secure/getAvailableUsers/25?
    // tenantLocationID=462&locationGroupID=1066&


    // getContributorsOnly=1&
    // getWithAllStatus=0&
    // getOperatorsOnly=0&
    // isWeightWiseUser=1&
    // allowSameWeightUser=0


    @GET("api/user/secure/getAvailableUsers/{tenantLocation_id}")
    Call<DashboardUserListModel> getDashboardUserList(@Path("tenantLocation_id") int tenantLocationId,
                                                      @Query("startDate") String startDate,
                                                      @Query("endDate") String endDate,
                                                      @Query("tenantLocationID") String tenantLocationID,
                                                      @Query("locationGroupIDs") String locationGroupId,
                                                      @Query("getContributorsOnly") boolean isContributorsOnly,
                                                      @Query("getWithAllStatus") boolean getWithAllStatus,
                                                      @Query("getOperatorsOnly") boolean getOperatorsOnly);

    @GET("api/user/secure/getAvailableUsers/{tenantLocation_id}")
    Call<DashboardUserListModel> getDashboardUserListForDashboard(@Path("tenantLocation_id") int tenantLocationId,
                                                                  @Query("getContributorsOnly") boolean isContributorsOnly,
                                                                  @Query("getWithAllStatus") boolean getWithAllStatus,
                                                                  @Query("getOperatorsOnly") boolean getOperatorsOnly,
                                                                  @Query("tenantLocationID") String tenantLocationIdFromFilter,
                                                                  @Query("locationGroupID") String locationGrpId,
                                                                  @Query("checkLoggedinUserOperatorContributor") boolean checkLoggedinUserOperatorContributor);


    @GET("api/user/secure/getAvailableUsers/{tenant_id}")
    Call<DashboardUserListModel> getDashboardUserListForAgentProfile(@Path("tenant_id") int tenantId,
                                                                     @Query("getContributorsOnly") boolean isContributorsOnly,
                                                                     @Query("getWithAllStatus") boolean getWithAllStatus,
                                                                     @Query("getOperatorsOnly") boolean getOperatorsOnly,
                                                                     @Query("isWeightWiseUser") boolean getIsWeightUser,
                                                                     @Query("allowSameWeightUser") boolean getAllowSameWeightUser,
                                                                     @Query("tenantLocationID") int tenantLocationId);


    @GET("api/user/secure/getDashboardUserList")
    Call<DashboardUserListModel> getDashboardUserListForGoalFilter(@Query("tenantLocationID") String tenantLocationId,
                                                                   @Query("locationGroupIDs") String locationGroupId,
                                                                   @Query("getContributorsOnly") boolean isContributors);


    @GET("api/feed/secure/fetchFeeds")
    Call<FeedsModelList> getfeedsList(@Query("tenantId") int tenantId,
                                      @Query("first") long first,
                                      @Query("total") long total);


    @GET("api/preference/getMobileKeys")
    Call<JsonObject> getMobileKeys();

    @GET("api/util/fetchPermission/{TenantId}/{UserId}")
    Call<JsonObject> getRolesAndPermission(@Path("TenantId") String TenantId,
                                           @Path("UserId") String UserId);

    @GET("api/user/is2FARequired")
    Call<JsonObject> check2FA(@Query("userId") int UserId,
                              @Query("isAuthyCheck") boolean isAuthyCheck);

    @GET("api/user/verifySecure2FAToken")
    Call<JsonObject> verify2FA(@Query("userId") int UserId);


    @POST("api/user/forgot_password")
    Call<JsonObject> getPassword(@Query("email") String Email);

    @POST("api/user/signUp")
    Call<JsonObject> createNewAccount(@Body JsonObject requestBody);

    @GET("api/menu/secure/getMenuList/{tenant_id}/{user_id}")
    Call<MenuModel> getMenu(@Path("tenant_id") String TenantId,
                            @Path("user_id") String UserId);


    @POST("api/feedComment/secure/m")
    Call<SaveFeedCommentModel> saveFeedComment(@Body HashMap<String, Object> feedComment);

    @GET("api/feed/secure/likeFeed/{feedId}/{userId}")
    Call<SaveLikeFeedModel> saveLikeFeed(@Path("feedId") int feedId,
                                         @Path("userId") int userId);

    @POST("shareReport/{custom_reportId}")
    Call<ShareFeedModel> shareFeed(@Path("custom_reportId") int custom_reportId,
                                   @Query("tenantId") int tenantId,
                                   @Query("regionTypeId") int regionTypeId,
                                   @Query("regionId") int regionId,
                                   @Query("tenantLocationId") int tenantLocationId,
                                   @Query("locationGroupId") int locationGrpId,
                                   @Query("productId") int productId,
                                   @Query("userId") int userId,
                                   @Query("from") String fromDate,
                                   @Query("to") String toDate,
                                   @Query("isCompare") Boolean isCompare,
                                   @Query("compFrom") String compfromDate,
                                   @Query("compTo") String comptoDate,
                                   @Query("metricsDateType") String matricsDataType,
                                   @Query("industryId") int industryId,
                                   @Query("loginUser") int loginuserId,
                                   @Query("authorizationId") String authToken,
                                   @Body ShareReportPojo mShareReportPojo);

    @GET("api/goal/goalProgress")
    Call<ResponseBody> getGoalProgress(@Query("month") int month,
                                       @Query("year") int year,
                                       @Query("tenantId") int tenantId,
                                       @Query("tenantLocationId") int tenantLocationId,
                                       @Query("locationGroupId") String locationGroupId,
                                       @Query("userId") int userId,
                                       @Query("locationGroupProductId") int locationGroupProductId);


    @GET("api/userMessage/getMailDetails")
    Call<ResponseBody> getEmailDetails(@Query("id") String emailId,
                                       @Query("type") String mailType);


    @GET("api/user/secure/getUsersFromTenant/{tenant_id}?excludeSuperAdmin=false&checkAccessibility=true")
    Call<RecipientListModel> getRecipientList(@Path("tenant_id") int custom_reportId);

    @POST("api/userMessage/secure/saveMessage")
    Call<ResponseBody> sendEmail(@Body EmailBody emailBody);

    @POST("api/userMessage/replyFrom")
    Call<ResponseBody> sendreplyEmail(@Body ReplyMailBody replyMailBody);


    @POST("api/userMessage/secure/m/find")
    Call<MailListDataModel> fetchMail(@Body FetchMailBody emailBody);


    @PUT("api/userMessage/deleteEmails")
    Call<ResponseBody> deleteMail(@Body DeleteMailBody detDeleteMailBody);


    @GET("api/userMessage/getUnReadCount")
    Call<ResponseBody> getUnreadCount();

    @Streaming
    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlAsync(@Url String fileUrl);


    @Multipart
    @POST("api/userMessage/uploadFile")
    Call<ResponseBody> uploadFile(@Part("filename") RequestBody filename,
                                  @Part MultipartBody.Part file);


    @GET("api/util/secure/getTableData/{report_id}")
    Call<ResponseBody> getDashboardTableReport(@Path("report_id") int dashboard_id,
                                               @Query("tenantId") int tenantId,
                                               @Query("regionTypeId") int regionTypeId,
                                               @Query("regionId") int regionId,
                                               @Query("productId") int productId,
                                               @Query("userId") int userId,
                                               @Query("tenantLocationId") int tenantLocationId,
                                               @Query("locationGroupId") int locationGroupId,
                                               @Query("metricsDateType") String metricsDateType,
                                               @Query("from") String from,
                                               @Query("to") String to,
                                               @Query("compFrom") String compFrom,
                                               @Query("compTo") String compTo,
                                               @Query("fromDateTime") String fromDateTime,
                                               @Query("toDateTime") String toDateTime,
                                               @Query("compFromDateTime") String compFromDateTime,
                                               @Query("compToDateTime") String compToDateTime,
                                               @Query("isCompare") boolean isCompare,
                                               @Query("loggedInUserId") int loginuserId);


    @GET("api/tenantLocation/secure/getTenantWiseRegionLocation/{tenant_id}")
    Call<ResponseBody> getDynamicFiltersListMap(@Path("tenant_id") int tenantId, @Query("userID") int userid,
                                                @Query("showDeactivatedLocation") boolean showDeactivatedLocation);


    @POST("likeCommentReport/{custom_reportId}")
    Call<ResponseBody> postLikeComment(@Path("custom_reportId") int custom_reportId,
                                       @Query("tenantId") int tenantId,
                                       @Query("industryId") int industryId,
                                       @Query("authorizationId") String authorizationID,
                                       @Body JsonObject requestBody);

    @POST("api/feed/secure/m/socIntCount")
    Call<ResponseBody> getSocialStatus(@Body JsonObject requestBody);

    @GET("api/media/secure/getAllMedia")
    Call<ResponseBody> getAllMedia(@Query("userId") int userId, @Query("roleId") int roleId, @Query("type") String type);

    @GET("api/dashboard/secure/getDashboardListByUniqueCode")
    Call<ResponseBody> getDashboardListByUniqueCode(@Query("uniqueCode") String uniqueCode,
                                                    @Query("tenantId") int tenantId,
                                                    @Query("entity") String entity);


    @GET("api/user/secure/{user_id}")
    Call<AgentProfileDetailsPojo> getAgentProfileDetails(@Path("user_id") String userID);

    @PUT("api/user/secure")
    Call<ResponseBody> putAgentProfileDetails(@Body AgentProfileDetailsPojo.Data mAgentProfileDetailsPojo);

    @POST("api/user/profile/change_avatar/{industry_id}/{user_id}")
    Call<ResponseBody> updateAvatar(@Path("industry_id") String industryId,
                                    @Path("user_id") String userId, @Body String imageBase64);


    @POST("api/custom_report/secure/getCustomReportDataByCode/MY_AGENT_PROFILE")
    Call<ResponseBody> getGamificationDetails(@Body GamificationPostAPIPojo mGamificationPostAPIPojo);


    @GET("api/userNotification/secure/getAllUserNotification")
    Call<UserNotificationListModel> getUserNotificationList(@Query("userId") int userId,
                                                            @Query("first") int first,
                                                            @Query("total") int total,
                                                            @Query("isLikeCommentOnly") boolean isLikeCommentOnly);

    @GET("api/userNotificationConfiguration/secure/getNotificationTypeData")
    Call<NotificationSettingsModel> getNotificationSettingsList(@Query("userId") int userId);


    @POST("api/userNotificationConfiguration/secure")
    Call<ResponseBody> savedNotificationSettings(@Body NotificationSettingsModel.Datum mDatum);


    @GET("api/feed/secure/feedsForReport")
    Call<ReportDetailFeedsPojo> getReportDetailFeeds(@Query("tenantId") int tenantId,
                                                     @Query("reportId") int reportId,
                                                     @Query("first") int first,
                                                     @Query("total") int total);


}
