package com.ingauge.api;


import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.pojo.BadDoubleDeserializer;
import com.ingauge.pojo.UriDeserializer;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;
import com.ingauge.utils.UiUtils;

import java.io.IOException;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mansurum on 08-May-17.
 */

public class APIClient{

    public static String TermsUrl = "https://app.fpg-ingauge.com/assets/termsofservice.html";
    private static Retrofit retrofit = null;
    //Production Enviroment
/*
    public static String UATbaseUrl = "http://app.fpg-ingauge.com/pulse/";
    public static String UATbaseUrlChart = "http://app.fpg-ingauge.com:3000/";

    //Production Enviroment For EC
    public static String ECbaseUrl = "http://ec.fpg-ingauge.com/pulse/";
    public static String ECbaseUrlChart = "http://ec.fpg-ingauge.com:3000/";
*/

    //UAT Enviroment
    /*public static String UATbaseUrl = "http://uat.app.fpg-ingauge.com/pulse/";
    public static String UATbaseUrlChart = "http://uat.app.fpg-ingauge.com:3000/";

    //UAT Enviroment For EC
    public static String ECbaseUrl = "http://18.195.106.186/pulse/";
    public static String ECbaseUrlChart = "http://18.195.106.186:3000/";*/

    //QA Enviroment
    public static String UATbaseUrl = "http://75.101.236.193/pulse/";
    public static String UATbaseUrlChart = "http://75.101.236.193:3000/";

    //QA Enviroment For EC
    public static String ECbaseUrl = "http://52.28.65.16/pulse/";
    public static String ECbaseUrlChart = "http://52.28.65.16:3000/";

    /*//Rizwan's PC Enviroment
    public static String UATbaseUrl = "http://192.168.2.157/pulse/";
    public static String UATbaseUrlChart = "http://192.168.2.157:3000/";

    //QA Enviroment For EC
    public static String ECbaseUrl = "http://192.168.2.157/pulse/";
    public static String ECbaseUrlChart = "http://192.168.2.157:3000/";*/

    //Bharti's System
    /*public static String UATbaseUrl = "http://192.168.3.132/pulse/";
    public static String UATbaseUrlChart = "http://192.168.3.132:3000/";

    public static String ECbaseUrl = "http://192.168.3.132/pulse/";
    public static String ECbaseUrlChart = "http://192.168.3.132:3000/";*/

    //Hardik's System
  /*  public static String UATbaseUrl = "http://192.168.3.199/pulse/";
    public static String UATbaseUrlChart = "http://192.168.3.199:3000/";

    public static String ECbaseUrl = "http://192.168.3.199/pulse/";
    public static String ECbaseUrlChart = "http://192.168.3.199:3000/";*/


    //QA Enviroment
   /* public static String UATbaseUrl = "http://192.168.3.118/pulse/";
    public static String UATbaseUrlChart = "http://192.168.3.118:3000/";

    //QA Enviroment For EC
    public static String ECbaseUrl = "http://192.168.2.167/pulse/";
    public static String ECbaseUrlChart = "http://192.168.2.167:3000/";*/


    //Hemali's System
    /*public static String UATbaseUrl = "http://192.168.3.101/pulse/";
    public static String UATbaseUrlChart = "http://192.168.3.101:3000/";

    public static String ECbaseUrl = "http://192.168.3.101/pulse/";
    public static String ECbaseUrlChart = "http://192.168.3.101:3000/";*/

  /*  //Parth Makwan System
    public static String UATbaseUrl = "http://192.168.3.208/pulse/";
    public static String UATbaseUrlChart = "http://192.168.3.208:3000/";

    public static String ECbaseUrl = "http://192.168.3.208/pulse/";
    public static String ECbaseUrlChart = "http://192.168.3.208:3000/";*/

    //Parth Sabuwala System
/*    public static String UATbaseUrl = "http://192.168.3.211/pulse/";
    public static String UATbaseUrlChart = "http://192.168.3.211:3000/";

    public static String ECbaseUrl = "http://192.168.3.211/pulse/";
    public static String ECbaseUrlChart = "http://192.168.3.211:3000/";*/

    /*//Vishal's System
    public static String UATbaseUrl = "http://192.168.3.102/pulse/";
    public static String UATbaseUrlChart = "http://192.168.3.102:3000/";

    public static String ECbaseUrl = "http://192.168.3.102/pulse/";
    public static String ECbaseUrlChart = "http://192.168.3.102:3000/";*/



    public static Retrofit getClient(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        Interceptor mInterceptorHeader = new Interceptor(){
            @Override
            public Response intercept(Chain chain) throws IOException{
                Request original = chain.request();
                TimeZone mTimeZone = TimeZone.getDefault();
                String mTimeZoneStr = mTimeZone.getID();
                TimeZone tz = TimeZone.getTimeZone(mTimeZoneStr);
                long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
                long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                        - TimeUnit.HOURS.toMinutes(hours);

                String timeZoneString = "";
                if(hours > 0){
                    timeZoneString = String.format("GMT+%d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                } else if(hours == 0){
                    timeZoneString = String.format("GMT", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                    //String temptimeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                } else{
                    timeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                }
                Logger.Error("Time Zone : " + timeZoneString);
                Logger.Error("Industry ID : " + String.valueOf(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 1)));
                Logger.Error("<< Auth >>> " + "bearer" + " " + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), ""));
                Logger.Error("<< Platform >>> " + 1);
                Logger.Error("<< Content-Type>>> " + "application/json");

                Request request = original.newBuilder()
                        .header("platform", "1")
                        .header("Content-Type", "application/json")
                        .header("Authorization", "bearer" + " " + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), ""))
                        .header("IndustryId", String.valueOf(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 1)))
                        .header("isMobile", "true")
                        .header("TimeZone", timeZoneString)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        };
        if(Logger.LOGGING_ENABLED){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        GsonBuilder gb = new GsonBuilder();
        gb.registerTypeAdapter(Integer.class, new TypeAdapter<Integer>(){
            @Override
            public Integer read(JsonReader reader) throws IOException{
                if(reader.peek() == JsonToken.NULL){
                    reader.nextNull();
                    return null;
                }
                String stringValue = reader.nextString();
                try{
                    Integer value = Integer.valueOf(stringValue);
                    return value;
                } catch(NumberFormatException e){
                    return null;
                }
            }

            @Override
            public void write(JsonWriter writer, Integer value) throws IOException{
                if(value == null){
                    writer.nullValue();
                    return;
                }
                writer.value(value);
            }
        });
     /*   OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("User-Agent", "Your-App-Name")
                        .header("Accept", "application/vnd.yourapi.v1.full+json")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });
*/
        Gson gson = gb.registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
        Gson mGson1Uri = gb.registerTypeAdapter(Uri.class, new UriDeserializer()).create();
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(interceptor).addInterceptor(mInterceptorHeader).build();
        /*client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);*/
        retrofit = new Retrofit.Builder()
                .baseUrl(UiUtils.baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create(mGson1Uri))
                .client(client)
                .build();
        return retrofit;
    }

    public static Retrofit getClientChart(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        Interceptor mInterceptorHeader = new Interceptor(){
            @Override
            public Response intercept(Chain chain) throws IOException{
                Request original = chain.request();
                TimeZone mTimeZone = TimeZone.getDefault();
                String mTimeZoneStr = mTimeZone.getID();
                TimeZone tz = TimeZone.getTimeZone(mTimeZoneStr);
                long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
                long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                        - TimeUnit.HOURS.toMinutes(hours);

                String timeZoneString = "";
                if(hours > 0){
                    timeZoneString = String.format("GMT+%d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                } else if(hours == 0){
                    timeZoneString = String.format("GMT", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                    //String temptimeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                } else{
                    timeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                }
                Logger.Error("Time Zone : " + timeZoneString);
                Logger.Error("Industry ID : " + String.valueOf(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0)));
                Logger.Error("<< Auth >>> " + "bearer" + " " + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), ""));
                Logger.Error("<< Platform >>> " + 1);
                Logger.Error("<< Content-Type>>> " + "application/json");

                Request request = original.newBuilder()
                        .header("platform", "1")
                        .header("Content-Type", "application/json")
                        .header("Authorization", "bearer" + " " + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), ""))
                        .header("IndustryId", String.valueOf(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0)))
                        .header("isMobile", "true")
                        .header("TimeZone", timeZoneString)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        };

        if(Logger.LOGGING_ENABLED){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        GsonBuilder gb = new GsonBuilder();
        gb.registerTypeAdapter(Integer.class, new TypeAdapter<Integer>(){

            @Override
            public Integer read(JsonReader reader) throws IOException{
                if(reader.peek() == JsonToken.NULL){
                    reader.nextNull();
                    return null;
                }
                String stringValue = reader.nextString();
                try{
                    Integer value = Integer.valueOf(stringValue);
                    return value;
                } catch(NumberFormatException e){
                    return null;
                }
            }

            @Override
            public void write(JsonWriter writer, Integer value) throws IOException{
                if(value == null){
                    writer.nullValue();
                    return;
                }
                writer.value(value);
            }

        });
        Gson gson = gb.registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
        Gson mGson1Uri = gb.registerTypeAdapter(Uri.class, new UriDeserializer()).create();
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(interceptor).addInterceptor(mInterceptorHeader).build();
        /*client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);*/
        retrofit = new Retrofit.Builder()
                .baseUrl(UiUtils.baseUrlChart)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create(mGson1Uri))
                .client(client)
                .build();
        return retrofit;
    }


    public static Retrofit getClientWithoutHeaders(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        if(Logger.LOGGING_ENABLED){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        GsonBuilder gb = new GsonBuilder();
        gb.registerTypeAdapter(Integer.class, new TypeAdapter<Integer>(){

            @Override
            public Integer read(JsonReader reader) throws IOException{
                if(reader.peek() == JsonToken.NULL){
                    reader.nextNull();
                    return null;
                }
                String stringValue = reader.nextString();
                try{
                    Integer value = Integer.valueOf(stringValue);
                    return value;
                } catch(NumberFormatException e){
                    return null;
                }
            }

            @Override
            public void write(JsonWriter writer, Integer value) throws IOException{
                if(value == null){
                    writer.nullValue();
                    return;
                }
                writer.value(value);
            }

        });
        Gson gson = gb.registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
        Gson mGson1Uri = gb.registerTypeAdapter(Uri.class, new UriDeserializer()).create();
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(interceptor).build();
        /*client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);*/
        retrofit = new Retrofit.Builder()
                .baseUrl(UiUtils.baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create(mGson1Uri))
                .client(client)
                .build();
        return retrofit;
    }
    public static Retrofit getClientChartWithoutHeaders(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        if(Logger.LOGGING_ENABLED){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        GsonBuilder gb = new GsonBuilder();
        gb.registerTypeAdapter(Integer.class, new TypeAdapter<Integer>(){

            @Override
            public Integer read(JsonReader reader) throws IOException{
                if(reader.peek() == JsonToken.NULL){
                    reader.nextNull();
                    return null;
                }
                String stringValue = reader.nextString();
                try{
                    Integer value = Integer.valueOf(stringValue);
                    return value;
                } catch(NumberFormatException e){
                    return null;
                }
            }

            @Override
            public void write(JsonWriter writer, Integer value) throws IOException{
                if(value == null){
                    writer.nullValue();
                    return;
                }
                writer.value(value);
            }

        });
        Gson gson = gb.registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
        Gson mGson1Uri = gb.registerTypeAdapter(Uri.class, new UriDeserializer()).create();
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(interceptor).build();
        /*client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);*/
        retrofit = new Retrofit.Builder()
                .baseUrl(UiUtils.baseUrlChart)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create(mGson1Uri))
                .client(client)
                .build();
        return retrofit;
    }

    public static Retrofit getClientForLogin(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if(Logger.LOGGING_ENABLED){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        GsonBuilder gb = new GsonBuilder();
        gb.registerTypeAdapter(Integer.class, new TypeAdapter<Integer>(){

            @Override
            public Integer read(JsonReader reader) throws IOException{
                if(reader.peek() == JsonToken.NULL){
                    reader.nextNull();
                    return null;
                }
                String stringValue = reader.nextString();
                try{
                    Integer value = Integer.valueOf(stringValue);
                    return value;
                } catch(NumberFormatException e){
                    return null;
                }
            }

            @Override
            public void write(JsonWriter writer, Integer value) throws IOException{
                if(value == null){
                    writer.nullValue();
                    return;
                }
                writer.value(value);
            }

        });
        Gson gson = gb.registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
        Gson mGson1Uri = gb.registerTypeAdapter(Uri.class, new UriDeserializer()).create();
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(interceptor).build();
        /*client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);*/
        retrofit = new Retrofit.Builder()
                .baseUrl(UiUtils.baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create(mGson1Uri))
                .client(client)
                .build();
        return retrofit;
    }

    public static Retrofit getClientForCreateAccount(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if(Logger.LOGGING_ENABLED){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        Interceptor mInterceptorHeader = new Interceptor(){
            @Override
            public Response intercept(Chain chain) throws IOException{
                Request original = chain.request();
                TimeZone mTimeZone = TimeZone.getDefault();
                String mTimeZoneStr = mTimeZone.getID();
                TimeZone tz = TimeZone.getTimeZone(mTimeZoneStr);
                long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
                long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                        - TimeUnit.HOURS.toMinutes(hours);

                String timeZoneString = "";
                if(hours > 0){
                    timeZoneString = String.format("GMT+%d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                } else if(hours == 0){
                    timeZoneString = String.format("GMT", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                    //String temptimeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                } else{
                    timeZoneString = String.format("GMT %d:%02d", hours, minutes, tz.getDisplayName(), mTimeZoneStr);
                }
                Logger.Error("Time Zone : " + timeZoneString);
                Logger.Error("Industry ID : " + String.valueOf(InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0)));
                Logger.Error("<< Auth >>> " + "bearer" + " " + InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_auth_token), ""));
                Logger.Error("<< Platform >>> " + 1);
                Logger.Error("<< Content-Type>>> " + "application/json");

                Request request = original.newBuilder()
                        .header("platform", "1")
                        .header("Content-Type", "application/json")
                        .header("isMobile", "true")
                        .header("TimeZone", timeZoneString)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        };
        GsonBuilder gb = new GsonBuilder();
        gb.registerTypeAdapter(Integer.class, new TypeAdapter<Integer>(){

            @Override
            public Integer read(JsonReader reader) throws IOException{
                if(reader.peek() == JsonToken.NULL){
                    reader.nextNull();
                    return null;
                }
                String stringValue = reader.nextString();
                try{
                    Integer value = Integer.valueOf(stringValue);
                    return value;
                } catch(NumberFormatException e){
                    return null;
                }
            }

            @Override
            public void write(JsonWriter writer, Integer value) throws IOException{
                if(value == null){
                    writer.nullValue();
                    return;
                }
                writer.value(value);
            }

        });
        Gson gson = gb.registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
        Gson mGson1Uri = gb.registerTypeAdapter(Uri.class, new UriDeserializer()).create();
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(interceptor).addInterceptor(mInterceptorHeader).build();
        /*client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);*/
        retrofit = new Retrofit.Builder()
                .baseUrl(UiUtils.baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create(mGson1Uri))
                .client(client)
                .build();
        return retrofit;
    }
}
