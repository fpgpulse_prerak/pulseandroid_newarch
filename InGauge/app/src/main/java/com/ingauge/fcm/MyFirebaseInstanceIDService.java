package com.ingauge.fcm;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;
import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.activities.SignInActivity;
import com.ingauge.api.APIClient;
import com.ingauge.api.interfaces.APIInterface;
import com.ingauge.pojo.UserBody;
import com.ingauge.session.InGaugeSession;
import com.ingauge.utils.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mansurum on 21-Aug-17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    APIInterface apiInterface;
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        apiInterface = APIClient.getClient().create(APIInterface.class);
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(refreshedToken))
            storeRegIdInPref(refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);
    }
    // [END refresh_token]


    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param refreshedToken The new token.
     */
    private void storeRegIdInPref(String refreshedToken) {
        InGaugeSession.write(getResources().getString(R.string.key_device_id_for_fcm), refreshedToken);
        if((!TextUtils.isEmpty(refreshedToken)) && (!TextUtils.isEmpty(InGaugeSession.read(getString(R.string.key_auth_token), "")))){
           // setDeviceId(InGaugeSession.read(getString(R.string.key_auth_token), ""));
        }
    }

    /**
     * @param authToken
     */
    void setDeviceId(final String authToken) {

        String deviceId = InGaugeSession.read(this.getResources().getString(R.string.key_device_id_for_fcm), "");
        if (TextUtils.isEmpty(deviceId)) {
            try {
                deviceId = FirebaseInstanceId.getInstance().getToken();
                InGaugeSession.write(getResources().getString(R.string.key_device_id_for_fcm), deviceId);
                Logger.Error("Referesh Token " + deviceId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Logger.Error("Device Id : " + deviceId);
        UserBody mUserBody = new UserBody();
        int userID = InGaugeSession.read(getString(R.string.key_user_id), -1);

        if (userID > 0) {
            mUserBody.id = userID;
        }
        mUserBody.deviceId = deviceId;
        mUserBody.platform = 1;
        Gson gson = new Gson();
        String jsonStringUserBody = gson.toJson(mUserBody).toString();
        JSONObject mJsonObject=null;
        try {
            mJsonObject = new JSONObject(jsonStringUserBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Logger.Error("Json Body to Get Device ID  " + jsonStringUserBody);
        //  if (mJsonObject!=null){
        Call mCall = apiInterface.getDeviceId(mUserBody);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
        // }

    }
}