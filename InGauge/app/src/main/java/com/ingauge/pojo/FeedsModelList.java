package com.ingauge.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mansurum on 05-Jun-17.
 */

public class FeedsModelList implements Serializable{

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;
    private FeedComment mFeedComment;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public FeedComment getmFeedComment() {
        return mFeedComment;
    }

    public void setmFeedComment(FeedComment mFeedComment) {
        this.mFeedComment = mFeedComment;
    }

    public class CreatedBy implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("tokenExpiry")
        @Expose
        private Integer tokenExpiry;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("signedInTime")
        @Expose
        private String signedInTime;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("birthMonth")
        @Expose
        private Integer birthMonth;
        @SerializedName("birthDay")
        @Expose
        private Integer birthDay;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Integer getTokenExpiry() {
            return tokenExpiry;
        }

        public void setTokenExpiry(Integer tokenExpiry) {
            this.tokenExpiry = tokenExpiry;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getSignedInTime() {
            return signedInTime;
        }

        public void setSignedInTime(String signedInTime) {
            this.signedInTime = signedInTime;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public Integer getBirthMonth() {
            return birthMonth;
        }

        public void setBirthMonth(Integer birthMonth) {
            this.birthMonth = birthMonth;
        }

        public Integer getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(Integer birthDay) {
            this.birthDay = birthDay;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class CreatedBy_ implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("tokenExpiry")
        @Expose
        private Integer tokenExpiry;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("signedInTime")
        @Expose
        private String signedInTime;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("linkedinId")
        @Expose
        private String linkedinId;
        @SerializedName("linkedinImage")
        @Expose
        private String linkedinImage;
        @SerializedName("birthMonth")
        @Expose
        private Integer birthMonth;
        @SerializedName("birthDay")
        @Expose
        private Integer birthDay;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isFpgMember")
        @Expose
        private Boolean isFpgMember;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("countryCode")
        @Expose
        private String countryCode;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("deviceId")
        @Expose
        private String deviceId;
        @SerializedName("platform")
        @Expose
        private Integer platform;
        @SerializedName("isFirstLogin")
        @Expose
        private Boolean isFirstLogin;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Integer getTokenExpiry() {
            return tokenExpiry;
        }

        public void setTokenExpiry(Integer tokenExpiry) {
            this.tokenExpiry = tokenExpiry;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getSignedInTime() {
            return signedInTime;
        }

        public void setSignedInTime(String signedInTime) {
            this.signedInTime = signedInTime;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public String getLinkedinId() {
            return linkedinId;
        }

        public void setLinkedinId(String linkedinId) {
            this.linkedinId = linkedinId;
        }

        public String getLinkedinImage() {
            return linkedinImage;
        }

        public void setLinkedinImage(String linkedinImage) {
            this.linkedinImage = linkedinImage;
        }

        public Integer getBirthMonth() {
            return birthMonth;
        }

        public void setBirthMonth(Integer birthMonth) {
            this.birthMonth = birthMonth;
        }

        public Integer getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(Integer birthDay) {
            this.birthDay = birthDay;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsFpgMember() {
            return isFpgMember;
        }

        public void setIsFpgMember(Boolean isFpgMember) {
            this.isFpgMember = isFpgMember;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public Integer getPlatform() {
            return platform;
        }

        public void setPlatform(Integer platform) {
            this.platform = platform;
        }

        public Boolean getIsFirstLogin() {
            return isFirstLogin;
        }

        public void setIsFirstLogin(Boolean isFirstLogin) {
            this.isFirstLogin = isFirstLogin;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
    public static class FeedComment implements Serializable
    {
        @SerializedName("id")
        @Expose
        public Integer id;

        @SerializedName("statusCode")
        @Expose
        public Integer statusCode;
        @SerializedName("createdOn")
        @Expose
        public String createdOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        public Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        public Boolean indexMainOnly;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @SerializedName("text")
        @Expose
        public String text;


        public FeedCommentUser getUser() {
            return user;
        }

        public void setUser(FeedCommentUser user) {
            this.user = user;
        }

        @SerializedName("user")
        @Expose
        public FeedCommentUser user = null;


    }
    public class FeedCommentUser implements Serializable
    {
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("name")
        @Expose
        public String name;

    }
    public class CustomReportWidget implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("chartType")
        @Expose
        private String chartType;
        @SerializedName("compareType")
        @Expose
        private String compareType;
        @SerializedName("inverted")
        @Expose
        private Boolean inverted;
        @SerializedName("polar")
        @Expose
        private Boolean polar;
        @SerializedName("summaryWidget")
        @Expose
        private Boolean summaryWidget;
        @SerializedName("displayType")
        @Expose
        private String displayType;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getChartType() {
            return chartType;
        }

        public void setChartType(String chartType) {
            this.chartType = chartType;
        }

        public String getCompareType() {
            return compareType;
        }

        public void setCompareType(String compareType) {
            this.compareType = compareType;
        }

        public Boolean getInverted() {
            return inverted;
        }

        public void setInverted(Boolean inverted) {
            this.inverted = inverted;
        }

        public Boolean getPolar() {
            return polar;
        }

        public void setPolar(Boolean polar) {
            this.polar = polar;
        }

        public Boolean getSummaryWidget() {
            return summaryWidget;
        }

        public void setSummaryWidget(Boolean summaryWidget) {
            this.summaryWidget = summaryWidget;
        }

        public String getDisplayType() {
            return displayType;
        }

        public void setDisplayType(String displayType) {
            this.displayType = displayType;
        }

    }

    public class Datum implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("createdBy")
        @Expose
        private CreatedBy createdBy;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("tenant")
        @Expose
        private Tenant tenant;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("customReportWidget")
        @Expose
        private CustomReportWidget customReportWidget;
        @SerializedName("options")
        @Expose
        private String options;
        @SerializedName("feedComments")
        @Expose
        private List<FeedComment> feedComments = null;
        @SerializedName("likeUsers")
        @Expose
        private List<Object> likeUsers = null;
        @SerializedName("noOfLikes")
        @Expose
        public int noOfLikes;
        @SerializedName("noOfComments")
        @Expose
        public int noOfComments;
        @SerializedName("isLiked")
        @Expose
        public boolean isLiked;
        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public CreatedBy getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(CreatedBy createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public Tenant getTenant() {
            return tenant;
        }

        public void setTenant(Tenant tenant) {
            this.tenant = tenant;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public CustomReportWidget getCustomReportWidget() {
            return customReportWidget;
        }

        public void setCustomReportWidget(CustomReportWidget customReportWidget) {
            this.customReportWidget = customReportWidget;
        }

        public String getOptions() {
            return options;
        }

        public void setOptions(String options) {
            this.options = options;
        }

        public List<FeedComment> getFeedComments() {
            return feedComments;
        }

        public void setFeedComments(List<FeedComment> feedComments) {
            this.feedComments = feedComments;
        }

        public List<Object> getLikeUsers() {
            return likeUsers;
        }

        public void setLikeUsers(List<Object> likeUsers) {
            this.likeUsers = likeUsers;
        }

    }


    public class Geography implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("locality")
        @Expose
        private String locality;
        @SerializedName("postalcode")
        @Expose
        private String postalcode;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getPostalcode() {
            return postalcode;
        }

        public void setPostalcode(String postalcode) {
            this.postalcode = postalcode;
        }

    }

    public class Tenant implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("createdBy")
        @Expose
        private CreatedBy_ createdBy;
        @SerializedName("updatedBy")
        @Expose
        private UpdatedBy updatedBy;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("geography")
        @Expose
        private Geography geography;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("hotels")
        @Expose
        private Integer hotels;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("oldId")
        @Expose
        private Integer oldId;
        @SerializedName("roles")
        @Expose
        private Object roles;
        @SerializedName("regions")
        @Expose
        private Object regions;
        @SerializedName("groupTypes")
        @Expose
        private Object groupTypes;
        @SerializedName("verdicts")
        @Expose
        private Object verdicts;
        @SerializedName("tenantReports")
        @Expose
        private Object tenantReports;
        @SerializedName("tenantLocations")
        @Expose
        private Object tenantLocations;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public CreatedBy_ getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(CreatedBy_ createdBy) {
            this.createdBy = createdBy;
        }

        public UpdatedBy getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(UpdatedBy updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public Geography getGeography() {
            return geography;
        }

        public void setGeography(Geography geography) {
            this.geography = geography;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getHotels() {
            return hotels;
        }

        public void setHotels(Integer hotels) {
            this.hotels = hotels;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Integer getOldId() {
            return oldId;
        }

        public void setOldId(Integer oldId) {
            this.oldId = oldId;
        }

        public Object getRoles() {
            return roles;
        }

        public void setRoles(Object roles) {
            this.roles = roles;
        }

        public Object getRegions() {
            return regions;
        }

        public void setRegions(Object regions) {
            this.regions = regions;
        }

        public Object getGroupTypes() {
            return groupTypes;
        }

        public void setGroupTypes(Object groupTypes) {
            this.groupTypes = groupTypes;
        }

        public Object getVerdicts() {
            return verdicts;
        }

        public void setVerdicts(Object verdicts) {
            this.verdicts = verdicts;
        }

        public Object getTenantReports() {
            return tenantReports;
        }

        public void setTenantReports(Object tenantReports) {
            this.tenantReports = tenantReports;
        }

        public Object getTenantLocations() {
            return tenantLocations;
        }

        public void setTenantLocations(Object tenantLocations) {
            this.tenantLocations = tenantLocations;
        }

    }


    public class UpdatedBy implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("tokenExpiry")
        @Expose
        private Integer tokenExpiry;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("signedInTime")
        @Expose
        private String signedInTime;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("linkedinId")
        @Expose
        private String linkedinId;
        @SerializedName("linkedinImage")
        @Expose
        private String linkedinImage;
        @SerializedName("birthMonth")
        @Expose
        private Integer birthMonth;
        @SerializedName("birthDay")
        @Expose
        private Integer birthDay;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isFpgMember")
        @Expose
        private Boolean isFpgMember;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("countryCode")
        @Expose
        private String countryCode;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("deviceId")
        @Expose
        private String deviceId;
        @SerializedName("platform")
        @Expose
        private Integer platform;
        @SerializedName("isFirstLogin")
        @Expose
        private Boolean isFirstLogin;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Integer getTokenExpiry() {
            return tokenExpiry;
        }

        public void setTokenExpiry(Integer tokenExpiry) {
            this.tokenExpiry = tokenExpiry;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getSignedInTime() {
            return signedInTime;
        }

        public void setSignedInTime(String signedInTime) {
            this.signedInTime = signedInTime;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public String getLinkedinId() {
            return linkedinId;
        }

        public void setLinkedinId(String linkedinId) {
            this.linkedinId = linkedinId;
        }

        public String getLinkedinImage() {
            return linkedinImage;
        }

        public void setLinkedinImage(String linkedinImage) {
            this.linkedinImage = linkedinImage;
        }

        public Integer getBirthMonth() {
            return birthMonth;
        }

        public void setBirthMonth(Integer birthMonth) {
            this.birthMonth = birthMonth;
        }

        public Integer getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(Integer birthDay) {
            this.birthDay = birthDay;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsFpgMember() {
            return isFpgMember;
        }

        public void setIsFpgMember(Boolean isFpgMember) {
            this.isFpgMember = isFpgMember;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public Integer getPlatform() {
            return platform;
        }

        public void setPlatform(Integer platform) {
            this.platform = platform;
        }

        public Boolean getIsFirstLogin() {
            return isFirstLogin;
        }

        public void setIsFirstLogin(Boolean isFirstLogin) {
            this.isFirstLogin = isFirstLogin;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
