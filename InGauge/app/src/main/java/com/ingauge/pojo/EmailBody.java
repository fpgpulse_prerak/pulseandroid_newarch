package com.ingauge.pojo;

/**
 * Created by mansurum on 17-Jul-17.
 */

public class EmailBody {

    private String[] emailId;
    private String subject;
    private String message;
    private String[] mailFileList;

    public String[] getEmailIds() {
        return emailId;
    }

    public void setEmailIds(String[] emailIds) {
        this.emailId = emailIds;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getMailFileList() {
        return mailFileList;
    }

    public void setMailFileList(String[] mailFileList) {
        this.mailFileList = mailFileList;
    }
}
