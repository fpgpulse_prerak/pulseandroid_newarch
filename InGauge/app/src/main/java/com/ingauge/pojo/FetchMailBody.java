package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mansurum on 18-Jul-17.
 */

public class FetchMailBody {

    @SerializedName("solrInput")
    @Expose
    private SolrInput solrInput;
    @SerializedName("recipientId")
    @Expose
    private Integer recipientId;
    @SerializedName("senderId")
    @Expose
    private Integer senderId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("mailReadStatus")
    @Expose
    private String mailReadStatus;
    public FetchMailBody(){
        solrInput = new SolrInput();
    }

    public SolrInput getSolrInput() {
        return solrInput;
    }

    public void setSolrInput(SolrInput solrInput) {
        this.solrInput = solrInput;
    }

    public Integer getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMailReadStatus() {
        return mailReadStatus;
    }

    public void setMailReadStatus(String mailReadStatus) {
        this.mailReadStatus = mailReadStatus;
    }

    public class SolrInput {

        @SerializedName("first")
        @Expose
        private Integer first;
        @SerializedName("total")
        @Expose
        private Integer total;
        @SerializedName("sortBy")
        @Expose
        private List<String> sortBy = null;
        @SerializedName("sortOrder")
        @Expose
        private List<String> sortOrder = null;
        @SerializedName("searchText")
        @Expose
        private String searchText;

        public Integer getFirst() {
            return first;
        }

        public void setFirst(Integer first) {
            this.first = first;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public List<String> getSortBy() {
            return sortBy;
        }

        public void setSortBy(List<String> sortBy) {
            this.sortBy = sortBy;
        }

        public List<String> getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(List<String> sortOrder) {
            this.sortOrder = sortOrder;
        }

        public String getSearchText() {
            return searchText;
        }

        public void setSearchText(String searchText) {
            this.searchText = searchText;
        }

    }
}
