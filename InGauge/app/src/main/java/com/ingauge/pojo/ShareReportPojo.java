package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mansurum on 15-Nov-17.
 */

public class ShareReportPojo {

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("feedShareWithDTOs")
    @Expose
    private List<FeedShareWithDTO> feedShareWithDTOs = null;
    @SerializedName("subType")
    @Expose
    private Integer subType;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FeedShareWithDTO> getFeedShareWithDTOs() {
        return feedShareWithDTOs;
    }

    public void setFeedShareWithDTOs(List<FeedShareWithDTO> feedShareWithDTOs) {
        this.feedShareWithDTOs = feedShareWithDTOs;
    }

    public Integer getSubType() {
        return subType;
    }

    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    public class FeedShareWithDTO {

        public FeedShareWithDTO(){

        }

        @SerializedName("entityType")
        @Expose
        private Integer entityType;
        @SerializedName("entityId")
        @Expose
        private String entityId;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;

        public Integer getEntityType() {
            return entityType;
        }

        public void setEntityType(Integer entityType) {
            this.entityType = entityType;
        }

        public String getEntityId() {
            return entityId;
        }

        public void setEntityId(String entityId) {
            this.entityId = entityId;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

    }

}
