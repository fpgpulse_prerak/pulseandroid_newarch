package com.ingauge.pojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */
public class GoalCurrentMonthMap implements Serializable {
    public String locationGrpProductName;
    public int fpgLevel;
    public boolean isAchieved;

    public String getLocationGrpProductName() {
        return locationGrpProductName;
    }

    public void setLocationGrpProductName(String locationGrpProductName) {
        this.locationGrpProductName = locationGrpProductName;
    }

    public int getFpgLevel() {
        return fpgLevel;
    }

    public void setFpgLevel(int fpgLevel) {
        this.fpgLevel = fpgLevel;
    }

    public boolean isAchieved() {
        return isAchieved;
    }

    public void setAchieved(boolean achieved) {
        isAchieved = achieved;
    }
}
