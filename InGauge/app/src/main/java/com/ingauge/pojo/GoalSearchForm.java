package com.ingauge.pojo;


import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * Created by davepriy on 3/2/2016.
 */
public class GoalSearchForm implements Serializable {

	private static final long serialVersionUID = 1L;

	/*1. userID
2. month
3. year
4. departmentID
5. organizationID
6. productId
7. organizationInfos
?8. products
?9. GoalSettingProduct
10. departments
11. users*/
	/*@Expose(serialize = false)*/
	@Expose
	private Integer userID;
	@Expose
	private Integer month;
	@Expose
	private Integer year;
	@Expose
	private Integer departmentID;
	@Expose
	private Integer organizationID;
	@Expose
	private Integer productId;
	@Expose(serialize = false)
	private String goalSettingProduct;


	@Expose(serialize = false)
	private List<GoalSettingProduct> products;
	@Expose(serialize = false)
	private List<GoalSettingDepartment> departments;








	public List<GoalSettingProduct> getProducts() {
		return products;
	}

	public void setProducts(List<GoalSettingProduct> products) {
		this.products = products;
	}

	public List<GoalSettingDepartment> getDepartments() {
		return departments;
	}

	public void setDepartments(List<GoalSettingDepartment> departments) {
		this.departments = departments;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getDepartmentID() {
		return departmentID;
	}

	public void setDepartmentID(Integer departmentID) {
		this.departmentID = departmentID;
	}

	public Integer getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(Integer organizationID) {
		this.organizationID = organizationID;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}


	public String getGoalSettingProduct() {
		return goalSettingProduct;
	}

	public void setGoalSettingProduct(String goalSettingProduct) {
		this.goalSettingProduct = goalSettingProduct;
	}


}
