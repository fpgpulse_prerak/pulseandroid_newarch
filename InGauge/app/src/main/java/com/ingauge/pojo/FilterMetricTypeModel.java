package com.ingauge.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mansurum on 24-Oct-17.
 */

public class FilterMetricTypeModel implements Parcelable{

    public String id = "";
    public String name = "";
    public String filterName = "";
    public int filterIndex;
    public boolean isSelectedPosition = false;
    public boolean isDynamicFilter= false;

    public FilterMetricTypeModel(){

    }

    public FilterMetricTypeModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        filterName = in.readString();
        filterIndex = in.readInt();
        isSelectedPosition = in.readByte() != 0;
        isDynamicFilter = in.readByte() != 0;
    }

    public static final Creator<FilterMetricTypeModel> CREATOR = new Creator<FilterMetricTypeModel>() {
        @Override
        public FilterMetricTypeModel createFromParcel(Parcel in) {
            return new FilterMetricTypeModel(in);
        }

        @Override
        public FilterMetricTypeModel[] newArray(int size) {
            return new FilterMetricTypeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(filterName);
        dest.writeInt(filterIndex);
        dest.writeByte((byte) (isSelectedPosition ? 1 : 0));
        dest.writeByte((byte) (isDynamicFilter ? 1 : 0));
    }
}
