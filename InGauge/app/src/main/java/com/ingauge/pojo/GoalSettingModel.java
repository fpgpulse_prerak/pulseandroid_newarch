package com.ingauge.pojo;



import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */

public class GoalSettingModel implements Serializable {







	private static final long serialVersionUID = 1L;
	private SelfGoalProgressModel SelfGoalProgress;
	private SelfGoalProgressModel FpgGoalProgress;
	private GoalWeeklyPrediction weeklyGoalPrediction;
	private GoalSearchForm searchGoalPredictionForm;

	public GoalWeeklyPrediction getWeeklyGoalPrediction() {
		return weeklyGoalPrediction;
	}

	public void setWeeklyGoalPrediction(GoalWeeklyPrediction weeklyGoalPrediction) {
		this.weeklyGoalPrediction = weeklyGoalPrediction;
	}

	public SelfGoalProgressModel getSelfGoalProgress() {
		return SelfGoalProgress;
	}

	public void setSelfGoalProgress(SelfGoalProgressModel selfGoalProgress) {
		SelfGoalProgress = selfGoalProgress;
	}

	public SelfGoalProgressModel getFpgGoalProgress() {
		return FpgGoalProgress;
	}

	public void setFpgGoalProgress(SelfGoalProgressModel fpgGoalProgress) {
		FpgGoalProgress = fpgGoalProgress;
	}

	public GoalSearchForm getSearchGoalPredictionForm() {
		return searchGoalPredictionForm;
	}

	public void setSearchGoalPredictionForm(GoalSearchForm searchGoalPredictionForm) {
		this.searchGoalPredictionForm = searchGoalPredictionForm;
	}


}
