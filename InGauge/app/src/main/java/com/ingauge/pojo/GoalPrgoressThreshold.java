package com.ingauge.pojo;


import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */
public class GoalPrgoressThreshold implements Serializable {

	private static final long serialVersionUID = 1L;

	/*@property (nonatomic, strong) NSNumber                      *departmentId;
@property (nonatomic, strong) NSDate                        *fromDate;
@property (nonatomic, strong) NSNumber                      *incentiveFutureLowerTierAmt;
@property (nonatomic, strong) NSNumber                      *incentiveFutureMiddleTierAmt;

@property (nonatomic, strong) NSNumber                      *incentiveFutureUpperTierAmt;
@property (nonatomic, strong) NSNumber                      *incentiveLowerTierPerDetail;

@property (nonatomic, strong) NSNumber                      *incentiveMiddleTierPerDetail;
@property (nonatomic, strong) NSNumber                      *incentivePlanId;
@property (nonatomic, strong) NSNumber                      *incentiveUpperTierPerDetail;
@property (nonatomic, strong) NSNumber                      *organizationId;
@property (nonatomic, strong) NSDate                        *toDate;
@property (nonatomic, strong) NSNumber                      *userId;
*/
	private Integer departmentId;
	private String fromDate;
	private Integer incentiveFutureLowerTierAmt;
	private Integer incentiveFutureMiddleTierAmt;
	private Integer incentiveFutureUpperTierAmt;
	private Integer incentiveLowerTierPerDetail;
	private Integer incentiveMiddleTierPerDetail;
	private Integer incentivePlanId;
	private Integer incentiveUpperTierPerDetail;
	private Integer organizationId;
	private String toDate;
	private Integer userId;

	public Integer getIncentiveFutureUpperTierAmt() {
		return incentiveFutureUpperTierAmt;
	}

	public void setIncentiveFutureUpperTierAmt(Integer incentiveFutureUpperTierAmt) {
		this.incentiveFutureUpperTierAmt = incentiveFutureUpperTierAmt;
	}

	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public Integer getIncentiveFutureLowerTierAmt() {
		return incentiveFutureLowerTierAmt;
	}

	public void setIncentiveFutureLowerTierAmt(Integer incentiveFutureLowerTierAmt) {
		this.incentiveFutureLowerTierAmt = incentiveFutureLowerTierAmt;
	}

	public Integer getIncentiveFutureMiddleTierAmt() {
		return incentiveFutureMiddleTierAmt;
	}

	public void setIncentiveFutureMiddleTierAmt(Integer incentiveFutureMiddleTierAmt) {
		this.incentiveFutureMiddleTierAmt = incentiveFutureMiddleTierAmt;
	}

	public Integer getIncentiveLowerTierPerDetail() {
		return incentiveLowerTierPerDetail;
	}

	public void setIncentiveLowerTierPerDetail(Integer incentiveLowerTierPerDetail) {
		this.incentiveLowerTierPerDetail = incentiveLowerTierPerDetail;
	}

	public Integer getIncentiveMiddleTierPerDetail() {
		return incentiveMiddleTierPerDetail;
	}

	public void setIncentiveMiddleTierPerDetail(Integer incentiveMiddleTierPerDetail) {
		this.incentiveMiddleTierPerDetail = incentiveMiddleTierPerDetail;
	}

	public Integer getIncentivePlanId() {
		return incentivePlanId;
	}

	public void setIncentivePlanId(Integer incentivePlanId) {
		this.incentivePlanId = incentivePlanId;
	}

	public Integer getIncentiveUpperTierPerDetail() {
		return incentiveUpperTierPerDetail;
	}

	public void setIncentiveUpperTierPerDetail(Integer incentiveUpperTierPerDetail) {
		this.incentiveUpperTierPerDetail = incentiveUpperTierPerDetail;
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
