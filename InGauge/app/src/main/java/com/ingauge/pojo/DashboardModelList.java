package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mansurum on 18-May-17.
 */

public class DashboardModelList {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    Datum mDatum = new Datum ();





    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("createdOn")
        @Expose
        private long createdOn;
        @SerializedName("updatedOn")
        @Expose
        private long updatedOn;
        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("dashboardLinks")
        @Expose
        private List<DashboardLink> dashboardLinks = null;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("isMTD")
        @Expose
        private boolean isMTD = false;
        @SerializedName("hideCompareDate")
        @Expose
        private boolean hideCompareDate = false;
        @SerializedName("tenantId")
        @Expose
        private Integer tenantId;
        @SerializedName("tenantName")
        @Expose
        private String tenantName;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("userFirstName")
        @Expose
        private String userFirstName;
        @SerializedName("userLastName")
        @Expose
        private String userLastName;
        @SerializedName("userEmail")
        @Expose
        private String userEmail;
        @SerializedName("share")
        @Expose
        private Boolean share;
        @SerializedName("roleRights")
        @Expose
        private RoleRights roleRights;
        @SerializedName("rank")
        @Expose
        private Integer rank;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;

        @SerializedName("showInMenu")
        @Expose
        private Boolean showInMenu;


        /*@SerializedName("linkedDashboardList")
        @Expose
        private List<Integer> linkedDashboardList = null;*/
        @SerializedName("linkedDashboardList")
        @Expose
        private Map<Integer, String> mLinedDashboardMaps=null;

        private List<Datum> mDatumList = new ArrayList<> ();


        public Datum() {
            mDatumList = new ArrayList<> ();
        }

        public void setUpdatedOn(long updatedOn) {
            this.updatedOn = updatedOn;
        }

        public boolean isSectionHeaderToShow() {
            return isSectionHeaderToShow;
        }

        public void setSectionHeaderToShow(boolean sectionHeaderToShow) {
            isSectionHeaderToShow = sectionHeaderToShow;
        }

        private boolean isSectionHeaderToShow = false;

        @SerializedName("dashboardType")
        @Expose
        private Integer dashboardType;

        public Integer getId() {
            return id;
        }

        public Integer getDashboardType() {
            return dashboardType;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public void setDashboardType(Integer id) {
            this.dashboardType = dashboardType;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public long getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(long createdOn) {
            this.createdOn = createdOn;
        }

        public long getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }

        public String getTenantName() {
            return tenantName;
        }

        public void setTenantName(String tenantName) {
            this.tenantName = tenantName;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getUserFirstName() {
            return userFirstName;
        }

        public void setUserFirstName(String userFirstName) {
            this.userFirstName = userFirstName;
        }

        public String getUserLastName() {
            return userLastName;
        }

        public void setUserLastName(String userLastName) {
            this.userLastName = userLastName;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public Boolean getShare() {
            return share;
        }

        public void setShare(Boolean share) {
            this.share = share;
        }

        public RoleRights getRoleRights() {
            return roleRights;
        }

        public void setRoleRights(RoleRights roleRights) {
            this.roleRights = roleRights;
        }

        public Integer getRank() {
            return rank;
        }

        public void setRank(Integer rank) {
            this.rank = rank;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isMTD() {
            return isMTD;
        }

        public void setMTD(boolean MTD) {
            isMTD = MTD;
        }

        public boolean isHideCompareDate() {
            return hideCompareDate;
        }

        public void setHideCompareDate(boolean hideCompareDate) {
            this.hideCompareDate = hideCompareDate;
        }

       /* public List<Integer> getLinkedDashboardList() {
            return linkedDashboardList;
        }

        public void setLinkedDashboardList(List<Integer> linkedDashboardList) {
            this.linkedDashboardList = linkedDashboardList;
        }*/

        public Boolean getShowInMenu() {
            return showInMenu;
        }

        public void setShowInMenu(Boolean showInMenu) {
            this.showInMenu = showInMenu;
        }

        public List<Datum> getmDatumList() {
            return mDatumList;
        }

        public void setmDatumList(List<Datum> mDatumList) {
            this.mDatumList = mDatumList;
        }

        public List<DashboardLink> getDashboardLinks() {
            return dashboardLinks;
        }

        public void setDashboardLinks(List<DashboardLink> dashboardLinks) {
            this.dashboardLinks = dashboardLinks;
        }

        public Map<Integer, String> getmLinedDashboardMaps() {
            return mLinedDashboardMaps;
        }

        public void setmLinedDashboardMaps(Map<Integer, String> mLinedDashboardMaps) {
            this.mLinedDashboardMaps = mLinedDashboardMaps;
        }


        public class DashboardLink {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("dashboardId")
            @Expose
            private Integer dashboardId;
            @SerializedName("dashboardLinkId")
            @Expose
            private Integer dashboardLinkId;
            @SerializedName("dashboardLinkDashboardType")
            @Expose
            private Integer dashboardLinkDashboardType;
            @SerializedName("linkText")
            @Expose
            private String linkText;
            @SerializedName("isDeleted")
            @Expose
            private Boolean isDeleted;
            @SerializedName("cnameTime")
            @Expose
            private String cnameTime;
            @SerializedName("unameTime")
            @Expose
            private String unameTime;
            @SerializedName("createdByWithDateTime")
            @Expose
            private String createdByWithDateTime;
            @SerializedName("updatedByWithDateTime")
            @Expose
            private String updatedByWithDateTime;
            @SerializedName("reviewedByWithDateTime")
            @Expose
            private String reviewedByWithDateTime;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getDashboardId() {
                return dashboardId;
            }

            public void setDashboardId(Integer dashboardId) {
                this.dashboardId = dashboardId;
            }

            public Integer getDashboardLinkId() {
                return dashboardLinkId;
            }

            public void setDashboardLinkId(Integer dashboardLinkId) {
                this.dashboardLinkId = dashboardLinkId;
            }

            public Integer getDashboardLinkDashboardType() {
                return dashboardLinkDashboardType;
            }

            public void setDashboardLinkDashboardType(Integer dashboardLinkDashboardType) {
                this.dashboardLinkDashboardType = dashboardLinkDashboardType;
            }

            public String getLinkText() {
                return linkText;
            }

            public void setLinkText(String linkText) {
                this.linkText = linkText;
            }

            public Boolean getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(Boolean isDeleted) {
                this.isDeleted = isDeleted;
            }

            public String getCnameTime() {
                return cnameTime;
            }

            public void setCnameTime(String cnameTime) {
                this.cnameTime = cnameTime;
            }

            public String getUnameTime() {
                return unameTime;
            }

            public void setUnameTime(String unameTime) {
                this.unameTime = unameTime;
            }

            public String getCreatedByWithDateTime() {
                return createdByWithDateTime;
            }

            public void setCreatedByWithDateTime(String createdByWithDateTime) {
                this.createdByWithDateTime = createdByWithDateTime;
            }

            public String getUpdatedByWithDateTime() {
                return updatedByWithDateTime;
            }

            public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                this.updatedByWithDateTime = updatedByWithDateTime;
            }

            public String getReviewedByWithDateTime() {
                return reviewedByWithDateTime;
            }

            public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                this.reviewedByWithDateTime = reviewedByWithDateTime;
            }

        }


    }


    public class RoleRights {


    }

}
