
package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DashboardGraphList {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


    public class CustomReport {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("createdOn")
        @Expose
        private Integer createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Integer updatedOn;
        @SerializedName("tenantId")
        @Expose
        private Integer tenantId;
        @SerializedName("tenantName")
        @Expose
        private String tenantName;
        @SerializedName("reportName")
        @Expose
        private String reportName;
        @SerializedName("queryBasedReport")
        @Expose
        private Boolean queryBasedReport;
        @SerializedName("tenantReports")
        @Expose
        private List<TenantReport> tenantReports = null;
        @SerializedName("dimensions")
        @Expose
        private List<Dimension> dimensions = null;
        @SerializedName("groupBy")
        @Expose
        private String groupBy;
        @SerializedName("customReportWidgetId")
        @Expose
        private Integer customReportWidgetId;
        @SerializedName("customReportWidgetChartType")
        @Expose
        private String customReportWidgetChartType;
        @SerializedName("customReportWidgetInverted")
        @Expose
        private Boolean customReportWidgetInverted;
        @SerializedName("customReportWidgetPolar")
        @Expose
        private Boolean customReportWidgetPolar;
        @SerializedName("customReportWidgetCompareType")
        @Expose
        private String customReportWidgetCompareType;
        @SerializedName("customReportWidgetDisplayType")
        @Expose
        private String customReportWidgetDisplayType;
        @SerializedName("avgColumns")
        @Expose
        private String avgColumns;
        @SerializedName("ranking")
        @Expose
        private String ranking;
        @SerializedName("isDynamicHeader")
        @Expose
        private Boolean isDynamicHeader;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public Integer getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Integer createdOn) {
            this.createdOn = createdOn;
        }

        public Integer getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }

        public String getTenantName() {
            return tenantName;
        }

        public void setTenantName(String tenantName) {
            this.tenantName = tenantName;
        }

        public String getReportName() {
            return reportName;
        }

        public void setReportName(String reportName) {
            this.reportName = reportName;
        }

        public Boolean getQueryBasedReport() {
            return queryBasedReport;
        }

        public void setQueryBasedReport(Boolean queryBasedReport) {
            this.queryBasedReport = queryBasedReport;
        }

        public List<TenantReport> getTenantReports() {
            return tenantReports;
        }

        public void setTenantReports(List<TenantReport> tenantReports) {
            this.tenantReports = tenantReports;
        }

        public List<Dimension> getDimensions() {
            return dimensions;
        }

        public void setDimensions(List<Dimension> dimensions) {
            this.dimensions = dimensions;
        }

        public String getGroupBy() {
            return groupBy;
        }

        public void setGroupBy(String groupBy) {
            this.groupBy = groupBy;
        }

        public Integer getCustomReportWidgetId() {
            return customReportWidgetId;
        }

        public void setCustomReportWidgetId(Integer customReportWidgetId) {
            this.customReportWidgetId = customReportWidgetId;
        }

        public String getCustomReportWidgetChartType() {
            return customReportWidgetChartType;
        }

        public void setCustomReportWidgetChartType(String customReportWidgetChartType) {
            this.customReportWidgetChartType = customReportWidgetChartType;
        }

        public Boolean getCustomReportWidgetInverted() {
            return customReportWidgetInverted;
        }

        public void setCustomReportWidgetInverted(Boolean customReportWidgetInverted) {
            this.customReportWidgetInverted = customReportWidgetInverted;
        }

        public Boolean getCustomReportWidgetPolar() {
            return customReportWidgetPolar;
        }

        public void setCustomReportWidgetPolar(Boolean customReportWidgetPolar) {
            this.customReportWidgetPolar = customReportWidgetPolar;
        }

        public String getCustomReportWidgetCompareType() {
            return customReportWidgetCompareType;
        }

        public void setCustomReportWidgetCompareType(String customReportWidgetCompareType) {
            this.customReportWidgetCompareType = customReportWidgetCompareType;
        }

        public String getCustomReportWidgetDisplayType() {
            return customReportWidgetDisplayType;
        }

        public void setCustomReportWidgetDisplayType(String customReportWidgetDisplayType) {
            this.customReportWidgetDisplayType = customReportWidgetDisplayType;
        }

        public String getAvgColumns() {
            return avgColumns;
        }

        public void setAvgColumns(String avgColumns) {
            this.avgColumns = avgColumns;
        }

        public String getRanking() {
            return ranking;
        }

        public void setRanking(String ranking) {
            this.ranking = ranking;
        }

        public Boolean getIsDynamicHeader() {
            return isDynamicHeader;
        }

        public void setIsDynamicHeader(Boolean isDynamicHeader) {
            this.isDynamicHeader = isDynamicHeader;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

    }

    public class DashboardSection {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("sectionIndex")
        @Expose
        private Integer sectionIndex;
        @SerializedName("isDeleted")
        @Expose
        private Boolean isDeleted;
        @SerializedName("dashboardSectionTemplateId")
        @Expose
        private Integer dashboardSectionTemplateId;
        @SerializedName("dashboardSectionTemplateDimension")
        @Expose
        private String dashboardSectionTemplateDimension;
        @SerializedName("dashboardSectionSegments")
        @Expose
        private List<DashboardSectionSegment> dashboardSectionSegments = null;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getSectionIndex() {
            return sectionIndex;
        }

        public void setSectionIndex(Integer sectionIndex) {
            this.sectionIndex = sectionIndex;
        }

        public Boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(Boolean isDeleted) {
            this.isDeleted = isDeleted;
        }

        public Integer getDashboardSectionTemplateId() {
            return dashboardSectionTemplateId;
        }

        public void setDashboardSectionTemplateId(Integer dashboardSectionTemplateId) {
            this.dashboardSectionTemplateId = dashboardSectionTemplateId;
        }

        public String getDashboardSectionTemplateDimension() {
            return dashboardSectionTemplateDimension;
        }

        public void setDashboardSectionTemplateDimension(String dashboardSectionTemplateDimension) {
            this.dashboardSectionTemplateDimension = dashboardSectionTemplateDimension;
        }

        public List<DashboardSectionSegment> getDashboardSectionSegments() {
            return dashboardSectionSegments;
        }

        public void setDashboardSectionSegments(List<DashboardSectionSegment> dashboardSectionSegments) {
            this.dashboardSectionSegments = dashboardSectionSegments;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

    }


    public class DashboardSectionSegment {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("segmentIndex")
        @Expose
        private Integer segmentIndex;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("customReports")
        @Expose
        private List<CustomReport> customReports = null;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getSegmentIndex() {
            return segmentIndex;
        }

        public void setSegmentIndex(Integer segmentIndex) {
            this.segmentIndex = segmentIndex;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<CustomReport> getCustomReports() {
            return customReports;
        }

        public void setCustomReports(List<CustomReport> customReports) {
            this.customReports = customReports;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

    }


    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("createdOn")
        @Expose
        private Integer createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Integer updatedOn;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("dashboardSections")
        @Expose
        private List<DashboardSection> dashboardSections = null;
        @SerializedName("tenantId")
        @Expose
        private Integer tenantId;
        @SerializedName("tenantName")
        @Expose
        private String tenantName;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("userFirstName")
        @Expose
        private String userFirstName;
        @SerializedName("userLastName")
        @Expose
        private String userLastName;
        @SerializedName("userEmail")
        @Expose
        private String userEmail;
        @SerializedName("share")
        @Expose
        private Boolean share;
        @SerializedName("roleRights")
        @Expose
        private RoleRights roleRights;
        @SerializedName("rank")
        @Expose
        private Integer rank;
        @SerializedName("filters")
        @Expose
        private List<String> filters = null;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public Integer getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Integer createdOn) {
            this.createdOn = createdOn;
        }

        public Integer getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<DashboardSection> getDashboardSections() {
            return dashboardSections;
        }

        public void setDashboardSections(List<DashboardSection> dashboardSections) {
            this.dashboardSections = dashboardSections;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }

        public String getTenantName() {
            return tenantName;
        }

        public void setTenantName(String tenantName) {
            this.tenantName = tenantName;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getUserFirstName() {
            return userFirstName;
        }

        public void setUserFirstName(String userFirstName) {
            this.userFirstName = userFirstName;
        }

        public String getUserLastName() {
            return userLastName;
        }

        public void setUserLastName(String userLastName) {
            this.userLastName = userLastName;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public Boolean getShare() {
            return share;
        }

        public void setShare(Boolean share) {
            this.share = share;
        }

        public RoleRights getRoleRights() {
            return roleRights;
        }

        public void setRoleRights(RoleRights roleRights) {
            this.roleRights = roleRights;
        }

        public Integer getRank() {
            return rank;
        }

        public void setRank(Integer rank) {
            this.rank = rank;
        }

        public List<String> getFilters() {
            return filters;
        }

        public void setFilters(List<String> filters) {
            this.filters = filters;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

    }


    public class Dimension {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("displayName")
        @Expose
        private String displayName;
        @SerializedName("dimension")
        @Expose
        private String dimension;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getDimension() {
            return dimension;
        }

        public void setDimension(String dimension) {
            this.dimension = dimension;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

    }

    public class RoleRights {


    }


    public class TenantReport {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("createdOn")
        @Expose
        private Integer createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Integer updatedOn;
        @SerializedName("commissionable")
        @Expose
        private Boolean commissionable;
        @SerializedName("defaultAllow")
        @Expose
        private Boolean defaultAllow;
        @SerializedName("department")
        @Expose
        private Boolean department;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("displayShortName")
        @Expose
        private Boolean displayShortName;
        @SerializedName("financial")
        @Expose
        private Boolean financial;
        @SerializedName("formulaArrival")
        @Expose
        private String formulaArrival;
        @SerializedName("formulaDaily")
        @Expose
        private String formulaDaily;
        @SerializedName("formulaDeparture")
        @Expose
        private String formulaDeparture;
        @SerializedName("hasDailyBaseline")
        @Expose
        private Boolean hasDailyBaseline;
        @SerializedName("hasDailyTarget")
        @Expose
        private Boolean hasDailyTarget;
        @SerializedName("highlighted")
        @Expose
        private Boolean highlighted;
        @SerializedName("hotelMetricsDataType")
        @Expose
        private String hotelMetricsDataType;
        @SerializedName("incentiveReports")
        @Expose
        private Boolean incentiveReports;
        @SerializedName("isMoney")
        @Expose
        private Boolean isMoney;
        @SerializedName("isSummable")
        @Expose
        private Boolean isSummable;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("rank")
        @Expose
        private Integer rank;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("reportFromProductMetrics")
        @Expose
        private Boolean reportFromProductMetrics;
        @SerializedName("reportUniqueCode")
        @Expose
        private String reportUniqueCode;
        @SerializedName("shortName")
        @Expose
        private String shortName;
        @SerializedName("tenantId")
        @Expose
        private Integer tenantId;
        @SerializedName("tenantName")
        @Expose
        private String tenantName;
        @SerializedName("unit")
        @Expose
        private String unit;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public Integer getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Integer createdOn) {
            this.createdOn = createdOn;
        }

        public Integer getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getCommissionable() {
            return commissionable;
        }

        public void setCommissionable(Boolean commissionable) {
            this.commissionable = commissionable;
        }

        public Boolean getDefaultAllow() {
            return defaultAllow;
        }

        public void setDefaultAllow(Boolean defaultAllow) {
            this.defaultAllow = defaultAllow;
        }

        public Boolean getDepartment() {
            return department;
        }

        public void setDepartment(Boolean department) {
            this.department = department;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Boolean getDisplayShortName() {
            return displayShortName;
        }

        public void setDisplayShortName(Boolean displayShortName) {
            this.displayShortName = displayShortName;
        }

        public Boolean getFinancial() {
            return financial;
        }

        public void setFinancial(Boolean financial) {
            this.financial = financial;
        }

        public String getFormulaArrival() {
            return formulaArrival;
        }

        public void setFormulaArrival(String formulaArrival) {
            this.formulaArrival = formulaArrival;
        }

        public String getFormulaDaily() {
            return formulaDaily;
        }

        public void setFormulaDaily(String formulaDaily) {
            this.formulaDaily = formulaDaily;
        }

        public String getFormulaDeparture() {
            return formulaDeparture;
        }

        public void setFormulaDeparture(String formulaDeparture) {
            this.formulaDeparture = formulaDeparture;
        }

        public Boolean getHasDailyBaseline() {
            return hasDailyBaseline;
        }

        public void setHasDailyBaseline(Boolean hasDailyBaseline) {
            this.hasDailyBaseline = hasDailyBaseline;
        }

        public Boolean getHasDailyTarget() {
            return hasDailyTarget;
        }

        public void setHasDailyTarget(Boolean hasDailyTarget) {
            this.hasDailyTarget = hasDailyTarget;
        }

        public Boolean getHighlighted() {
            return highlighted;
        }

        public void setHighlighted(Boolean highlighted) {
            this.highlighted = highlighted;
        }

        public String getHotelMetricsDataType() {
            return hotelMetricsDataType;
        }

        public void setHotelMetricsDataType(String hotelMetricsDataType) {
            this.hotelMetricsDataType = hotelMetricsDataType;
        }

        public Boolean getIncentiveReports() {
            return incentiveReports;
        }

        public void setIncentiveReports(Boolean incentiveReports) {
            this.incentiveReports = incentiveReports;
        }

        public Boolean getIsMoney() {
            return isMoney;
        }

        public void setIsMoney(Boolean isMoney) {
            this.isMoney = isMoney;
        }

        public Boolean getIsSummable() {
            return isSummable;
        }

        public void setIsSummable(Boolean isSummable) {
            this.isSummable = isSummable;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getRank() {
            return rank;
        }

        public void setRank(Integer rank) {
            this.rank = rank;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Boolean getReportFromProductMetrics() {
            return reportFromProductMetrics;
        }

        public void setReportFromProductMetrics(Boolean reportFromProductMetrics) {
            this.reportFromProductMetrics = reportFromProductMetrics;
        }

        public String getReportUniqueCode() {
            return reportUniqueCode;
        }

        public void setReportUniqueCode(String reportUniqueCode) {
            this.reportUniqueCode = reportUniqueCode;
        }

        public String getShortName() {
            return shortName;
        }

        public void setShortName(String shortName) {
            this.shortName = shortName;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }

        public String getTenantName() {
            return tenantName;
        }

        public void setTenantName(String tenantName) {
            this.tenantName = tenantName;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }
    }
}