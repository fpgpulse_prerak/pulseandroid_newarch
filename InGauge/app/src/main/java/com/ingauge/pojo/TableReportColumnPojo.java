package com.ingauge.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mansurum on 21-Sep-17.
 */

public class TableReportColumnPojo implements Parcelable {

    private int columnId;
    private String columnName = "";
    private boolean isSelected = false;
    private String columnKey = "";


    public TableReportColumnPojo() {

    }
    public TableReportColumnPojo(Parcel in) {
        columnId = in.readInt();
        columnName = in.readString();
        isSelected = in.readByte() != 0;
        columnKey = in.readString();
    }

    public static final Creator<TableReportColumnPojo> CREATOR = new Creator<TableReportColumnPojo>() {
        @Override
        public TableReportColumnPojo createFromParcel(Parcel in) {
            return new TableReportColumnPojo(in);
        }

        @Override
        public TableReportColumnPojo[] newArray(int size) {
            return new TableReportColumnPojo[size];
        }
    };

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getColumnKey() {
        return columnKey;
    }

    public void setColumnKey(String columnKey) {
        this.columnKey = columnKey;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(columnId);
        dest.writeString(columnName);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeString(columnKey);
    }
}
