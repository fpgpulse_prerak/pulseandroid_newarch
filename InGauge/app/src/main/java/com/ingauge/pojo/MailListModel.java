package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mansurum on 19-Jul-17.
 */

public class MailListModel {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;


    @SerializedName("first")
    @Expose
    private Integer first;
    @SerializedName("totalInList")
    @Expose
    private Integer totalInList;
    @SerializedName("total")
    @Expose
    private Integer total;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getFirst() {
        return first;
    }

    public void setFirst(Integer first) {
        this.first = first;
    }

    public Integer getTotalInList() {
        return totalInList;
    }

    public void setTotalInList(Integer totalInList) {
        this.totalInList = totalInList;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("createdOn")
        @Expose
        private Long createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Long updatedOn;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("hasReplies_ur")
        @Expose
        private Boolean hasRepliesUr;
        @SerializedName("parentMessageId_ur")
        @Expose
        private Integer parentMessageIdUr;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("recipient_name")
        @Expose
        private String recipientName;
        @SerializedName("senderId")
        @Expose
        private Integer senderId;
        /*@SerializedName("recipientId_ur")
        @Expose
        private String recipientIdUr;*/
        @SerializedName("isRead_ur")
        @Expose
        private Boolean isReadUr;
        @SerializedName("isDeleted_ur")
        @Expose
        private Boolean isDeletedUr;
        @SerializedName("isDeleted_usrSender")
        @Expose
        private Boolean isDeletedUsrSender;
        @SerializedName("id_ur")
        @Expose
        private Integer idUr;
        @SerializedName("usermailId_ur")
        @Expose
        private Integer usermailIdUr;
        @SerializedName("parentMessage")
        @Expose
        private Integer parentMessage;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Long getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Long createdOn) {
            this.createdOn = createdOn;
        }

        public Long getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Long updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Boolean getHasRepliesUr() {
            return hasRepliesUr;
        }

        public void setHasRepliesUr(Boolean hasRepliesUr) {
            this.hasRepliesUr = hasRepliesUr;
        }

        public Integer getParentMessageIdUr() {
            return parentMessageIdUr;
        }

        public void setParentMessageIdUr(Integer parentMessageIdUr) {
            this.parentMessageIdUr = parentMessageIdUr;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getRecipientName() {
            return recipientName;
        }

        public void setRecipientName(String recipientName) {
            this.recipientName = recipientName;
        }

        public Integer getSenderId() {
            return senderId;
        }

        public void setSenderId(Integer senderId) {
            this.senderId = senderId;
        }

/*        public String getRecipientIdUr() {
            return recipientIdUr;
        }

        public void setRecipientIdUr(String recipientIdUr) {
            this.recipientIdUr = recipientIdUr;
        }*/

        public Boolean getIsReadUr() {
            return isReadUr;
        }

        public void setIsReadUr(Boolean isReadUr) {
            this.isReadUr = isReadUr;
        }

        public Boolean getIsDeletedUr() {
            return isDeletedUr;
        }

        public void setIsDeletedUr(Boolean isDeletedUr) {
            this.isDeletedUr = isDeletedUr;
        }

        public Boolean getIsDeletedUsrSender() {
            return isDeletedUsrSender;
        }

        public void setIsDeletedUsrSender(Boolean isDeletedUsrSender) {
            this.isDeletedUsrSender = isDeletedUsrSender;
        }

        public Integer getIdUr() {
            return idUr;
        }

        public void setIdUr(Integer idUr) {
            this.idUr = idUr;
        }

        public Integer getUsermailIdUr() {
            return usermailIdUr;
        }

        public void setUsermailIdUr(Integer usermailIdUr) {
            this.usermailIdUr = usermailIdUr;
        }

        public Integer getParentMessage() {
            return parentMessage;
        }

        public void setParentMessage(Integer parentMessage) {
            this.parentMessage = parentMessage;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

    }


}
