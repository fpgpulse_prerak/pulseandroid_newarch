package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pathanaa on 11-07-2017.
 */

public class AgentModel implements Serializable{

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("linkedinId")
        @Expose
        private String linkedinId;
        @SerializedName("linkedinImage")
        @Expose
        private String linkedinImage;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getLinkedinId() {
            return linkedinId;
        }

        public void setLinkedinId(String linkedinId) {
            this.linkedinId = linkedinId;
        }

        public String getLinkedinImage() {
            return linkedinImage;
        }

        public void setLinkedinImage(String linkedinImage) {
            this.linkedinImage = linkedinImage;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
    }
