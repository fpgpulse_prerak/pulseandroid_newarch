package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * Created by pathanaa on 14-07-2017.
 */

public class ObservationJobType implements Serializable{
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("cByIdentity")
        @Expose
        private String cByIdentity;
        @SerializedName("uByIdentity")
        @Expose
        private String uByIdentity;
        @SerializedName("createdOn")
        @Expose
        private Integer createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Integer updatedOn;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        private boolean isSelected=false;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public String getCByIdentity() {
            return cByIdentity;
        }

        public void setCByIdentity(String cByIdentity) {
            this.cByIdentity = cByIdentity;
        }

        public String getUByIdentity() {
            return uByIdentity;
        }

        public void setUByIdentity(String uByIdentity) {
            this.uByIdentity = uByIdentity;
        }

        public Integer getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Integer createdOn) {
            this.createdOn = createdOn;
        }

        public Integer getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }
    }
}
