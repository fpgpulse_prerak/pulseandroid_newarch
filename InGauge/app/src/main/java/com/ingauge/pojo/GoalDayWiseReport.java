package com.ingauge.pojo;


import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */
public class GoalDayWiseReport implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer day;
	private double revenue;
	private Boolean isAvailable;

	public double getRevenue() {
		return revenue;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

}
