package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pathanaa on 18-07-2017.
 */

public class ObservationDashboardModel {
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


    public class Data {

        @SerializedName("data")
        @Expose
        private List<Datum> data = null;
        @SerializedName("first")
        @Expose
        private Integer first;
        @SerializedName("totalInList")
        @Expose
        private Integer totalInList;
        @SerializedName("total")
        @Expose
        private Integer total;
        @SerializedName("sorts")
        @Expose
        private List<Object> sorts = null;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

        public Integer getFirst() {
            return first;
        }

        public void setFirst(Integer first) {
            this.first = first;
        }

        public Integer getTotalInList() {
            return totalInList;
        }

        public void setTotalInList(Integer totalInList) {
            this.totalInList = totalInList;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public List<Object> getSorts() {
            return sorts;
        }

        public void setSorts(List<Object> sorts) {
            this.sorts = sorts;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }


        public class Datum {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("industryId")
            @Expose
            private Integer industryId;
            @SerializedName("activeStatus")
            @Expose
            private String activeStatus;
            @SerializedName("createdById")
            @Expose
            private Integer createdById;
            @SerializedName("updatedById")
            @Expose
            private Integer updatedById;
            @SerializedName("cByIdentity")
            @Expose
            private String cByIdentity;
            @SerializedName("uByIdentity")
            @Expose
            private String uByIdentity;
            @SerializedName("createdOn")
            @Expose
            private Integer createdOn;
            @SerializedName("updatedOn")
            @Expose
            private Integer updatedOn;
            @SerializedName("donotMailToAgent")
            @Expose
            private Boolean donotMailToAgent;
            @SerializedName("endDate")
            @Expose
            private String endDate;
            @SerializedName("isScheduled")
            @Expose
            private Boolean isScheduled;
            @SerializedName("note")
            @Expose
            private String note;
            @SerializedName("questionnaireId")
            @Expose
            private Integer questionnaireId;
            @SerializedName("questionnaireName")
            @Expose
            private String questionnaireName;
            @SerializedName("recommendation")
            @Expose
            private String recommendation;
            @SerializedName("questionnaireSurveyCategoryId")
            @Expose
            private Integer questionnaireSurveyCategoryId;
            @SerializedName("questionnaireSurveyCategoryName")
            @Expose
            private String questionnaireSurveyCategoryName;
            @SerializedName("respondentId")
            @Expose
            private Integer respondentId;
            @SerializedName("respondentName")
            @Expose
            private String respondentName;
            @SerializedName("scoreVal")
            @Expose
            private Double scoreVal;
            @SerializedName("startDate")
            @Expose
            private long startDate;
            @SerializedName("surveyStatus")
            @Expose
            private String surveyStatus;
            @SerializedName("strengthText")
            @Expose
            private String strengthText;
            @SerializedName("surveyEntityType")
            @Expose
            private String surveyEntityType;
            @SerializedName("surveyorId")
            @Expose
            private Integer surveyorId;
            @SerializedName("surveyorName")
            @Expose
            private String surveyorName;
            @SerializedName("surveyTakenTime")
            @Expose
            private Integer surveyTakenTime;
            @SerializedName("symptomText")
            @Expose
            private String symptomText;
            @SerializedName("tenantLocationId")
            @Expose
            private Integer tenantLocationId;
            @SerializedName("tenantLocationName")
            @Expose
            private String tenantLocationName;
            @SerializedName("calendarEventId")
            @Expose
            private Integer calendarEventId;
            @SerializedName("strengths")
            @Expose
            private List<Object> strengths = null;
            @SerializedName("surveyStrengths")
            @Expose
            private List<Object> surveyStrengths = null;
            @SerializedName("questionAnswer")
            @Expose
            private List<Object> questionAnswer = null;
            @SerializedName("tenantId")
            @Expose
            private Integer tenantId;
            @SerializedName("cnameTime")
            @Expose
            private String cnameTime;
            @SerializedName("unameTime")
            @Expose
            private String unameTime;
            @SerializedName("createdByWithDateTime")
            @Expose
            private String createdByWithDateTime;
            @SerializedName("reviewedByWithDateTime")
            @Expose
            private String reviewedByWithDateTime;
            @SerializedName("updatedByWithDateTime")
            @Expose
            private String updatedByWithDateTime;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getIndustryId() {
                return industryId;
            }

            public void setIndustryId(Integer industryId) {
                this.industryId = industryId;
            }

            public String getActiveStatus() {
                return activeStatus;
            }

            public void setActiveStatus(String activeStatus) {
                this.activeStatus = activeStatus;
            }

            public Integer getCreatedById() {
                return createdById;
            }

            public void setCreatedById(Integer createdById) {
                this.createdById = createdById;
            }

            public Integer getUpdatedById() {
                return updatedById;
            }

            public void setUpdatedById(Integer updatedById) {
                this.updatedById = updatedById;
            }

            public String getCByIdentity() {
                return cByIdentity;
            }

            public void setCByIdentity(String cByIdentity) {
                this.cByIdentity = cByIdentity;
            }

            public String getUByIdentity() {
                return uByIdentity;
            }

            public void setUByIdentity(String uByIdentity) {
                this.uByIdentity = uByIdentity;
            }

            public Integer getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(Integer createdOn) {
                this.createdOn = createdOn;
            }

            public Integer getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(Integer updatedOn) {
                this.updatedOn = updatedOn;
            }

            public Boolean getDonotMailToAgent() {
                return donotMailToAgent;
            }

            public void setDonotMailToAgent(Boolean donotMailToAgent) {
                this.donotMailToAgent = donotMailToAgent;
            }

            public String getEndDate() {
                return endDate;
            }

            public void setEndDate(String endDate) {
                this.endDate = endDate;
            }

            public Boolean getIsScheduled() {
                return isScheduled;
            }

            public void setIsScheduled(Boolean isScheduled) {
                this.isScheduled = isScheduled;
            }

            public String getNote() {
                return note;
            }

            public void setNote(String note) {
                this.note = note;
            }

            public Integer getQuestionnaireId() {
                return questionnaireId;
            }

            public void setQuestionnaireId(Integer questionnaireId) {
                this.questionnaireId = questionnaireId;
            }

            public String getQuestionnaireName() {
                return questionnaireName;
            }

            public void setQuestionnaireName(String questionnaireName) {
                this.questionnaireName = questionnaireName;
            }

            public String getRecommendation() {
                return recommendation;
            }

            public void setRecommendation(String recommendation) {
                this.recommendation = recommendation;
            }

            public Integer getQuestionnaireSurveyCategoryId() {
                return questionnaireSurveyCategoryId;
            }

            public void setQuestionnaireSurveyCategoryId(Integer questionnaireSurveyCategoryId) {
                this.questionnaireSurveyCategoryId = questionnaireSurveyCategoryId;
            }

            public String getQuestionnaireSurveyCategoryName() {
                return questionnaireSurveyCategoryName;
            }

            public void setQuestionnaireSurveyCategoryName(String questionnaireSurveyCategoryName) {
                this.questionnaireSurveyCategoryName = questionnaireSurveyCategoryName;
            }

            public Integer getRespondentId() {
                return respondentId;
            }

            public void setRespondentId(Integer respondentId) {
                this.respondentId = respondentId;
            }

            public String getRespondentName() {
                return respondentName;
            }

            public void setRespondentName(String respondentName) {
                this.respondentName = respondentName;
            }

            public Double getScoreVal() {
                return scoreVal;
            }

            public void setScoreVal(Double scoreVal) {
                this.scoreVal = scoreVal;
            }

            public long getStartDate() {
                return startDate;
            }

            public void setStartDate(long startDate) {
                this.startDate = startDate;
            }

            public String getSurveyStatus() {
                return surveyStatus;
            }

            public void setSurveyStatus(String surveyStatus) {
                this.surveyStatus = surveyStatus;
            }

            public String getStrengthText() {
                return strengthText;
            }

            public void setStrengthText(String strengthText) {
                this.strengthText = strengthText;
            }

            public String getSurveyEntityType() {
                return surveyEntityType;
            }

            public void setSurveyEntityType(String surveyEntityType) {
                this.surveyEntityType = surveyEntityType;
            }

            public Integer getSurveyorId() {
                return surveyorId;
            }

            public void setSurveyorId(Integer surveyorId) {
                this.surveyorId = surveyorId;
            }

            public String getSurveyorName() {
                return surveyorName;
            }

            public void setSurveyorName(String surveyorName) {
                this.surveyorName = surveyorName;
            }

            public Integer getSurveyTakenTime() {
                return surveyTakenTime;
            }

            public void setSurveyTakenTime(Integer surveyTakenTime) {
                this.surveyTakenTime = surveyTakenTime;
            }

            public String getSymptomText() {
                return symptomText;
            }

            public void setSymptomText(String symptomText) {
                this.symptomText = symptomText;
            }

            public Integer getTenantLocationId() {
                return tenantLocationId;
            }

            public void setTenantLocationId(Integer tenantLocationId) {
                this.tenantLocationId = tenantLocationId;
            }

            public String getTenantLocationName() {
                return tenantLocationName;
            }

            public void setTenantLocationName(String tenantLocationName) {
                this.tenantLocationName = tenantLocationName;
            }

            public Integer getCalendarEventId() {
                return calendarEventId;
            }

            public void setCalendarEventId(Integer calendarEventId) {
                this.calendarEventId = calendarEventId;
            }

            public List<Object> getStrengths() {
                return strengths;
            }

            public void setStrengths(List<Object> strengths) {
                this.strengths = strengths;
            }

            public List<Object> getSurveyStrengths() {
                return surveyStrengths;
            }

            public void setSurveyStrengths(List<Object> surveyStrengths) {
                this.surveyStrengths = surveyStrengths;
            }

            public List<Object> getQuestionAnswer() {
                return questionAnswer;
            }

            public void setQuestionAnswer(List<Object> questionAnswer) {
                this.questionAnswer = questionAnswer;
            }

            public Integer getTenantId() {
                return tenantId;
            }

            public void setTenantId(Integer tenantId) {
                this.tenantId = tenantId;
            }

            public String getCnameTime() {
                return cnameTime;
            }

            public void setCnameTime(String cnameTime) {
                this.cnameTime = cnameTime;
            }

            public String getUnameTime() {
                return unameTime;
            }

            public void setUnameTime(String unameTime) {
                this.unameTime = unameTime;
            }

            public String getCreatedByWithDateTime() {
                return createdByWithDateTime;
            }

            public void setCreatedByWithDateTime(String createdByWithDateTime) {
                this.createdByWithDateTime = createdByWithDateTime;
            }

            public String getReviewedByWithDateTime() {
                return reviewedByWithDateTime;
            }

            public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                this.reviewedByWithDateTime = reviewedByWithDateTime;
            }

            public String getUpdatedByWithDateTime() {
                return updatedByWithDateTime;
            }

            public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                this.updatedByWithDateTime = updatedByWithDateTime;
            }
        }


    }

}

