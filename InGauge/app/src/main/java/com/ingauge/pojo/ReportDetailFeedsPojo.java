package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ingauge.utils.Logger;

import java.util.List;

/**
 * Created by mansurum on 27-Apr-18.
 */

public class ReportDetailFeedsPojo{

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


    public class Datum {

        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("userName")
        @Expose
        private String userName;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("date")
        @Expose
        private Long date;
        @SerializedName("isComment")
        @Expose
        private Boolean isComment;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("isLike")
        @Expose
        private Boolean isLike;
        @SerializedName("isShare")
        @Expose
        private Boolean isShare;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Long getDate() {
            return date;
        }

        public void setDate(Long date) {
            this.date = date;
        }

        public Boolean getIsComment() {
            return isComment;
        }

        public void setIsComment(Boolean isComment) {
            this.isComment = isComment;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public Boolean getIsLike() {
            return isLike;
        }

        public void setIsLike(Boolean isLike) {
            this.isLike = isLike;
        }

        public Boolean getIsShare() {
            return isShare;
        }

        public void setIsShare(Boolean isShare) {
            this.isShare = isShare;
        }

    }
}
