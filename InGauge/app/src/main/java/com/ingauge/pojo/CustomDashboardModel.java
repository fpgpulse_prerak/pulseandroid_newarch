package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mansurum on 29-Aug-17.
 */

public class CustomDashboardModel{

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData(){
        return data;
    }

    public void setData(Data data){
        this.data = data;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public Integer getStatusCode(){
        return statusCode;
    }

    public void setStatusCode(Integer statusCode){
        this.statusCode = statusCode;
    }

    public class Data{

        @SerializedName("customReports")
        @Expose
        private List<CustomReport> customReport = null;

        public List<CustomReport> getCustomReport(){
            return customReport;
        }

        public void setCustomReport(List<CustomReport> customReport){
            this.customReport = customReport;
        }

    }

    public class CustomReport{

        @SerializedName("dataCompareBaselineValue")
        @Expose
        private Double dataCompareBaselineValue = new Double(String.valueOf(""));
        ;
        @SerializedName("reportName")
        @Expose
        private String reportName = "";
        @SerializedName("dataCompareTargetValue")
        @Expose
        private Double dataCompareTargetValue = new Double(String.valueOf(""));
        ;
        @SerializedName("prefix")
        @Expose
        private String prefix = "";
        @SerializedName("baseline")
        @Expose
        private Double baseline = new Double(String.valueOf(""));
        ;
        @SerializedName("dataCompareValue")
        @Expose
        private double dataCompareValue = new Double(String.valueOf(""));
        ;
        @SerializedName("target")
        @Expose
        private Double target = new Double(String.valueOf(""));
        ;
        @SerializedName("reportType")
        @Expose
        private String reportType = "";
        @SerializedName("entityName")
        @Expose
        private String entityName = "";


        @SerializedName("isQueryBasedReport")
        @Expose
        private boolean isQueryBasedReport = false;

        @SerializedName("compEntityName")
        @Expose
        private String compEntityName = "";
        @SerializedName("chartType")
        @Expose
        private String chartType = "";
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("text")
        @Expose
        private String text = "";
        @SerializedName("value")
        @Expose
        private double value = new Double(String.valueOf(""));
        @SerializedName("suffix")
        @Expose
        private String suffix = "";
        @SerializedName("iconHexcode")
        @Expose
        private String iconHexcode = "";
        @SerializedName("iconColor")
        @Expose
        private String iconColor = "";
        @SerializedName("isError")
        @Expose
        private Boolean isError;

        @SerializedName("iconClass")
        @Expose
        private String iconClass = "";

        public String getIconClass(){
            return iconClass;
        }

        public void setIconClass(String iconClass){
            this.iconClass = iconClass;
        }


        public Boolean getIsError(){
            return isError;
        }

        public void setIsError(Boolean isError){
            this.isError = isError;
        }

        public Double getDataCompareBaselineValue(){
            return dataCompareBaselineValue;
        }

        public void setDataCompareBaselineValue(Double dataCompareBaselineValue){
            this.dataCompareBaselineValue = dataCompareBaselineValue;
        }

        public String getReportName(){
            return reportName;
        }

        public void setReportName(String reportName){
            this.reportName = reportName;
        }

        public Double getDataCompareTargetValue(){
            return dataCompareTargetValue;
        }

        public void setDataCompareTargetValue(Double dataCompareTargetValue){
            this.dataCompareTargetValue = dataCompareTargetValue;
        }

        public String getPrefix(){
            return prefix;
        }

        public void setPrefix(String prefix){
            this.prefix = prefix;
        }

        public Double getBaseline(){
            return baseline;
        }

        public void setBaseline(Double baseline){
            this.baseline = baseline;
        }

        public double getDataCompareValue(){
            return dataCompareValue;
        }

        public void setDataCompareValue(double dataCompareValue){
            this.dataCompareValue = dataCompareValue;
        }

        public Double getTarget(){
            return target;
        }

        public void setTarget(Double target){
            this.target = target;
        }

        public String getReportType(){
            return reportType;
        }

        public void setReportType(String reportType){
            this.reportType = reportType;
        }

        public String getEntityName(){
            return entityName;
        }

        public void setEntityName(String entityName){
            this.entityName = entityName;
        }

        public String getChartType(){
            return chartType;
        }

        public void setChartType(String chartType){
            this.chartType = chartType;
        }

        public Integer getId(){
            return id;
        }

        public void setId(Integer id){
            this.id = id;
        }

        public String getText(){
            return text;
        }

        public void setText(String text){
            this.text = text;
        }

        public Double getValue(){
            return value;
        }

        public void setValue(Double value){
            this.value = value;
        }

        public String getSuffix(){
            return suffix;
        }

        public void setSuffix(String suffix){
            this.suffix = suffix;
        }

        public boolean isQueryBasedReport(){
            return isQueryBasedReport;
        }

        public void setQueryBasedReport(boolean queryBasedReport){
            isQueryBasedReport = queryBasedReport;
        }

        public String getCompEntityName(){
            return compEntityName;
        }

        public void setCompEntityName(String compEntityName){
            this.compEntityName = compEntityName;
        }

        public String getIconHexcode(){
            return iconHexcode;
        }

        public void setIconHexcode(String iconHexcode){
            this.iconHexcode = iconHexcode;
        }

        public String getIconColor(){
            return iconColor;
        }

        public void setIconColor(String iconColor){
            this.iconColor = iconColor;
        }
    }
}
