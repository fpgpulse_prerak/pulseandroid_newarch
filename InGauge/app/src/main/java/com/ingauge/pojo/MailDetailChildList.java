package com.ingauge.pojo;

import java.util.List;

/**
 * Created by mansurum on 03-Oct-17.
 */

public class MailDetailChildList {


    private String Message="";
    private List<AttachmentModel> attachmentModelList;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public List<AttachmentModel> getAttachmentModelList() {
        return attachmentModelList;
    }

    public void setAttachmentModelList(List<AttachmentModel> attachmentModelList) {
        this.attachmentModelList = attachmentModelList;
    }
}
