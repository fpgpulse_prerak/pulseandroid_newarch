package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mansurum on 29-Sep-17.
 */

public class MailListDataModel {



    @SerializedName("data")
    @Expose
    public MailListModel mailListModel;

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public MailListModel getMailListModel() {
        return mailListModel;
    }

    public void setMailListModel(MailListModel mailListModel) {
        this.mailListModel = mailListModel;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


}
