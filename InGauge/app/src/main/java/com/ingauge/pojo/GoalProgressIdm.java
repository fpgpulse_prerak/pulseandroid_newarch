package com.ingauge.pojo;



import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */

public class GoalProgressIdm implements Serializable {

	private static final long serialVersionUID = 1L;

	/*@property (nonatomic, strong) NSNumber                      *differenceToPriorMonth;
@property (nonatomic, strong) NSString                      *maxIncMonthStr;
@property (nonatomic, strong) NSString                      *minIncMonthStr;
@property (nonatomic, strong) NSNumber                      *mtd;
@property (nonatomic, strong) NSNumber                      *priorMonth;
@property (nonatomic, strong) NSNumber                      *priorMonthPercentage;
@property (nonatomic, strong) NSNumber                      *quaterly;
@property (nonatomic, strong) NSNumber                      *ranking;
@property (nonatomic, strong) GoalPrgoressThreshold         *threshold;
@property (nonatomic, strong) NSNumber                      *tier;
@property (nonatomic, strong) NSNumber                      *trend;
@property (nonatomic, strong) NSNumber                      *ytd;
*/
	private Integer differenceToPriorMonth;
	private String maxIncMonthStr;
	private String minIncMonthStr;
	private Integer mtd;
	private Integer priorMonth;
	private Integer priorMonthPercentage;
	private Integer quaterly;
	private Integer ranking;
	private GoalPrgoressThreshold threshold;
	private Integer tier;
	private Integer trend;
	private Integer ytd;


	public GoalPrgoressThreshold getThreshold() {
		return threshold;
	}

	public void setThreshold(GoalPrgoressThreshold threshold) {
		this.threshold = threshold;
	}

	public Integer getDifferenceToPriorMonth() {
		return differenceToPriorMonth;
	}

	public void setDifferenceToPriorMonth(Integer differenceToPriorMonth) {
		this.differenceToPriorMonth = differenceToPriorMonth;
	}

	public String getMaxIncMonthStr() {
		return maxIncMonthStr;
	}

	public void setMaxIncMonthStr(String maxIncMonthStr) {
		this.maxIncMonthStr = maxIncMonthStr;
	}

	public String getMinIncMonthStr() {
		return minIncMonthStr;
	}

	public void setMinIncMonthStr(String minIncMonthStr) {
		this.minIncMonthStr = minIncMonthStr;
	}

	public Integer getMtd() {
		return mtd;
	}

	public void setMtd(Integer mtd) {
		this.mtd = mtd;
	}

	public Integer getPriorMonth() {
		return priorMonth;
	}

	public void setPriorMonth(Integer priorMonth) {
		this.priorMonth = priorMonth;
	}

	public Integer getPriorMonthPercentage() {
		return priorMonthPercentage;
	}

	public void setPriorMonthPercentage(Integer priorMonthPercentage) {
		this.priorMonthPercentage = priorMonthPercentage;
	}

	public Integer getQuaterly() {
		return quaterly;
	}

	public void setQuaterly(Integer quaterly) {
		this.quaterly = quaterly;
	}

	public Integer getRanking() {
		return ranking;
	}

	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}

	public Integer getTier() {
		return tier;
	}

	public void setTier(Integer tier) {
		this.tier = tier;
	}

	public Integer getTrend() {
		return trend;
	}

	public void setTrend(Integer trend) {
		this.trend = trend;
	}

	public Integer getYtd() {
		return ytd;
	}

	public void setYtd(Integer ytd) {
		this.ytd = ytd;
	}


}
