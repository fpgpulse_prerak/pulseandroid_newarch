package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by desainid on 6/21/2017.
 */

public class ShareFeedModel {
    @SerializedName("data")
    @Expose
    public String data;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;
}
