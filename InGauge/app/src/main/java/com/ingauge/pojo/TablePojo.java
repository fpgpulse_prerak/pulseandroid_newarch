package com.ingauge.pojo;

/**
 * @author anfer
 */
public class TablePojo {


    private String columnOne;
    private String columnTwo;
    private String columnThree;
    private boolean isSocialStatusCall = true;




    public TablePojo(String columnOne, String columnTwo, String columnThree) {
        this.columnOne = columnOne;
        this.columnTwo = columnTwo;
        this.columnThree = columnThree;

    }


    public String getColumnOne() {
        return columnOne;
    }

    public String getColumnTwo() {
        return columnTwo;
    }

    public String getColumnThree() {
        return columnThree;
    }

    public boolean isSocialStatusCall() {
        return isSocialStatusCall;
    }

    public void setSocialStatusCall(boolean socialStatusCall) {
        isSocialStatusCall = socialStatusCall;
    }


}
