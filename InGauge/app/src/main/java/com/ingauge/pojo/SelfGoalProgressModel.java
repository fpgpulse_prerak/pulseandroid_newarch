package com.ingauge.pojo;



import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */

public class SelfGoalProgressModel implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
@property (nonatomic, strong) NSNumber                      *averageRevenueMTD;
@property (nonatomic, strong) NSNumber                      *avgRevenuPerUnit;
@property (nonatomic, strong) NSNumber                      *currentIncentiveTier;
@property (nonatomic, strong) NSNumber                      *currentIncentiveTierPer;
@property (nonatomic, strong) GoalProgressIdm               *idm;
@property (nonatomic, strong) NSNumber                      *incentiveEarned;
@property (nonatomic, strong) NSNumber                      *isAchieved;
@property (nonatomic, strong) NSNumber                      *isUserGoalPrediction;
@property (nonatomic, strong) NSNumber                      *nextIncentiveTier;
@property (nonatomic, strong) NSNumber                      *nextIncentiveTierPer;
@property (nonatomic, strong) NSNumber                      *numberOfUnitsPerDay;
@property (nonatomic, strong) NSNumber                      *numberOfUnitsToHitNextTier;
@property (nonatomic, strong) NSNumber                      *productGoal;
@property (nonatomic, strong) NSNumber                      *productIRThisMonth;
@property (nonatomic, strong) NSNumber                      *productIncentiveEarned;
@property (nonatomic, strong) GoalProductInsights           *productInsights;
@property (nonatomic, strong) NSNumber                      *projectedProductEOMIncentive;
@property (nonatomic, strong) NSNumber                      *projectedRevenueEOM;
@property (nonatomic, strong) NSNumber                      *remainingDays;
@property (nonatomic, strong) NSNumber                      *requiredNumberOfUnitsPerDay;
@property (nonatomic, strong) NSNumber                      *revenueToHitNextTier;
@property (nonatomic, strong) NSNumber                      *revenuetoHitGoal;
@property (nonatomic, strong) NSNumber                      *targetAchievedPer;
@property (nonatomic, strong) NSNumber                      *timeElapsed;
@property (nonatomic, strong) NSNumber                      *totalIRGoal;
@property (nonatomic, strong) NSNumber                      *totalIRPrevMonth;
@property (nonatomic, strong) NSNumber                      *totalIRThisMonth;
@property (nonatomic, strong) NSString                      *verdict;
@property (nonatomic, strong) NSString                      *notes;

// New field added: 03 May 2016
@property (nonatomic, strong) NSNumber                      *productCommissionableIRThisMonth;*/


	private Double averageRevenueMTD;
	private Double avgRevenuPerUnit;
	private Double currentIncentiveTier;
	private Double currentIncentiveTierPer;
	private Double incentiveEarned;
	private Boolean isAchieved;
	private Boolean isUserGoalPrediction;
	private Double nextIncentiveTier;
	private Double nextIncentiveTierPer;
	private Double numberOfUnitsPerDay;
	private Double numberOfUnitsToHitNextTier;
	private Double productGoal;

	public Double getCurrentYearPayout() {
		return currentYearPayout;
	}

	public void setCurrentYearPayout(Double currentYearPayout) {
		this.currentYearPayout = currentYearPayout;
	}

	private Double currentYearPayout;
	private Double productIRThisMonth;
	private Double currentMonthPayout;

	public Double getCurrentMonthPayout() {
		return currentMonthPayout;
	}

	public void setCurrentMonthPayout(Double currentMonthPayout) {
		this.currentMonthPayout = currentMonthPayout;
	}

	public Double getProductYTDIncRevenue() {
		return productYTDIncRevenue;
	}

	public void setProductYTDIncRevenue(Double productYTDIncRevenue) {
		this.productYTDIncRevenue = productYTDIncRevenue;
	}

	private Double productYTDIncRevenue;
	private Double productIncentiveEarned;
	private Double projectedProductEOMIncentive;
	private Double projectedRevenueEOM;


	public Double getProductARPDThisMonth() {
		return productARPDThisMonth;
	}

	public void setProductARPDThisMonth(Double productARPDThisMonth) {
		this.productARPDThisMonth = productARPDThisMonth;
	}

	private Double productARPDThisMonth;
	private Double remainingDays;
	private Double requiredNumberOfUnitsPerDay;
	private Double revenueToHitNextTier;
	private Double revenuetoHitGoal;
	private Double targetAchievedPer;
	private Double timeElapsed;
	private Double totalIRGoal;
	private Double totalIRPrevMonth;
	private Double totalIRThisMonth;
	private String verdict;
	private String notes;
	private Double productCommissionableIRThisMonth;
	private GoalProgressIdm idm;
	private GoalProductInsights productInsights;


	public Double getNumberOfUnitsPerDay() {
		return numberOfUnitsPerDay;
	}

	public void setNumberOfUnitsPerDay(Double numberOfUnitsPerDay) {
		this.numberOfUnitsPerDay = numberOfUnitsPerDay;
	}

	public Double getAverageRevenueMTD() {
		return averageRevenueMTD;
	}

	public void setAverageRevenueMTD(Double averageRevenueMTD) {
		this.averageRevenueMTD = averageRevenueMTD;
	}

	public Double getAvgRevenuPerUnit() {
		return avgRevenuPerUnit;
	}

	public void setAvgRevenuPerUnit(Double avgRevenuPerUnit) {
		this.avgRevenuPerUnit = avgRevenuPerUnit;
	}

	public Double getCurrentIncentiveTier() {
		return currentIncentiveTier;
	}

	public void setCurrentIncentiveTier(Double currentIncentiveTier) {
		this.currentIncentiveTier = currentIncentiveTier;
	}

	public Double getCurrentIncentiveTierPer() {
		return currentIncentiveTierPer;
	}

	public void setCurrentIncentiveTierPer(Double currentIncentiveTierPer) {
		this.currentIncentiveTierPer = currentIncentiveTierPer;
	}

	public Double getIncentiveEarned() {
		return incentiveEarned;
	}

	public void setIncentiveEarned(Double incentiveEarned) {
		this.incentiveEarned = incentiveEarned;
	}

	public Boolean getIsAchieved() {
		return isAchieved;
	}

	public void setIsAchieved(Boolean isAchieved) {
		this.isAchieved = isAchieved;
	}

	public Boolean getIsUserGoalPrediction() {
		return isUserGoalPrediction;
	}

	public void setIsUserGoalPrediction(Boolean isUserGoalPrediction) {
		this.isUserGoalPrediction = isUserGoalPrediction;
	}

	public Double getNextIncentiveTier() {
		return nextIncentiveTier;
	}

	public void setNextIncentiveTier(Double nextIncentiveTier) {
		this.nextIncentiveTier = nextIncentiveTier;
	}

	public Double getNextIncentiveTierPer() {
		return nextIncentiveTierPer;
	}

	public void setNextIncentiveTierPer(Double nextIncentiveTierPer) {
		this.nextIncentiveTierPer = nextIncentiveTierPer;
	}

	public Double getNumberOfUnitsToHitNextTier() {
		return numberOfUnitsToHitNextTier;
	}

	public void setNumberOfUnitsToHitNextTier(Double numberOfUnitsToHitNextTier) {
		this.numberOfUnitsToHitNextTier = numberOfUnitsToHitNextTier;
	}

	public Double getProductGoal() {
		return productGoal;
	}

	public void setProductGoal(Double productGoal) {
		this.productGoal = productGoal;
	}

	public Double getProductIRThisMonth() {
		return productIRThisMonth;
	}

	public void setProductIRThisMonth(Double productIRThisMonth) {
		this.productIRThisMonth = productIRThisMonth;
	}

	public Double getProductIncentiveEarned() {
		return productIncentiveEarned;
	}

	public void setProductIncentiveEarned(Double productIncentiveEarned) {
		this.productIncentiveEarned = productIncentiveEarned;
	}

	public Double getProjectedProductEOMIncentive() {
		return projectedProductEOMIncentive;
	}

	public void setProjectedProductEOMIncentive(Double projectedProductEOMIncentive) {
		this.projectedProductEOMIncentive = projectedProductEOMIncentive;
	}

	public Double getProjectedRevenueEOM() {
		return projectedRevenueEOM;
	}

	public void setProjectedRevenueEOM(Double projectedRevenueEOM) {
		this.projectedRevenueEOM = projectedRevenueEOM;
	}

	public Double getRemainingDays() {
		return remainingDays;
	}

	public void setRemainingDays(Double remainingDays) {
		this.remainingDays = remainingDays;
	}

	public Double getRequiredNumberOfUnitsPerDay() {
		return requiredNumberOfUnitsPerDay;
	}

	public void setRequiredNumberOfUnitsPerDay(Double requiredNumberOfUnitsPerDay) {
		this.requiredNumberOfUnitsPerDay = requiredNumberOfUnitsPerDay;
	}

	public Double getRevenueToHitNextTier() {
		return revenueToHitNextTier;
	}

	public void setRevenueToHitNextTier(Double revenueToHitNextTier) {
		this.revenueToHitNextTier = revenueToHitNextTier;
	}

	public Double getRevenuetoHitGoal() {
		return revenuetoHitGoal;
	}

	public void setRevenuetoHitGoal(Double revenuetoHitGoal) {
		this.revenuetoHitGoal = revenuetoHitGoal;
	}

	public Double getTargetAchievedPer() {
		return targetAchievedPer;
	}

	public void setTargetAchievedPer(Double targetAchievedPer) {
		this.targetAchievedPer = targetAchievedPer;
	}

	public Double getTimeElapsed() {
		return timeElapsed;
	}

	public void setTimeElapsed(Double timeElapsed) {
		this.timeElapsed = timeElapsed;
	}

	public Double getTotalIRGoal() {
		return totalIRGoal;
	}

	public void setTotalIRGoal(Double totalIRGoal) {
		this.totalIRGoal = totalIRGoal;
	}

	public Double getTotalIRPrevMonth() {
		return totalIRPrevMonth;
	}

	public void setTotalIRPrevMonth(Double totalIRPrevMonth) {
		this.totalIRPrevMonth = totalIRPrevMonth;
	}

	public Double getTotalIRThisMonth() {
		return totalIRThisMonth;
	}

	public void setTotalIRThisMonth(Double totalIRThisMonth) {
		this.totalIRThisMonth = totalIRThisMonth;
	}

	public String getVerdict() {
		return verdict;
	}

	public void setVerdict(String verdict) {
		this.verdict = verdict;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Double getProductCommissionableIRThisMonth() {
		return productCommissionableIRThisMonth;
	}

	public void setProductCommissionableIRThisMonth(Double productCommissionableIRThisMonth) {
		this.productCommissionableIRThisMonth = productCommissionableIRThisMonth;
	}

	public GoalProgressIdm getIdm() {
		return idm;
	}

	public void setIdm(GoalProgressIdm idm) {
		this.idm = idm;
	}

	public GoalProductInsights getProductInsights() {
		return productInsights;
	}

	public void setProductInsights(GoalProductInsights productInsights) {
		this.productInsights = productInsights;
	}

}
