package com.ingauge.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 23-May-17.
 */

public class FilterBaseDataForLocationGroup implements Parcelable {
    public String id = "";
    public String name = "";
    public String filterName = "";
    public int filterIndex;
    public String filterMetricType = "";
    public boolean isSelectedPosition = false;
    public List<FilterBaseDataForLocationGroup> mFilterBaseDatas = new ArrayList<>();
    public int selectedLocationGroupCount = 0;
    public int leftFilterIndex = -1;

    public FilterBaseDataForLocationGroup() {

    }

    protected FilterBaseDataForLocationGroup(Parcel in) {
        id = in.readString();
        name = in.readString();
        filterName = in.readString();
        filterIndex = in.readInt();
        filterMetricType = in.readString();
        isSelectedPosition = in.readByte() != 0;
        selectedLocationGroupCount = in.readInt();
        leftFilterIndex = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(filterName);
        dest.writeInt(filterIndex);
        dest.writeString(filterMetricType);
        dest.writeByte((byte) (isSelectedPosition ? 1 : 0));
        dest.writeInt(selectedLocationGroupCount);
        dest.writeInt(leftFilterIndex);
    }
    @Override
    public int describeContents() {
        return 0;
    }


    public static final Creator<FilterBaseDataForLocationGroup> CREATOR = new Creator<FilterBaseDataForLocationGroup>() {
        @Override
        public FilterBaseDataForLocationGroup createFromParcel(Parcel in) {
            return new FilterBaseDataForLocationGroup(in);
        }

        @Override
        public FilterBaseDataForLocationGroup[] newArray(int size) {
            return new FilterBaseDataForLocationGroup[size];
        }
    };
}
