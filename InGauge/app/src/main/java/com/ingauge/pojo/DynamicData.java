package com.ingauge.pojo;

/**
 * Created by mansurum on 16-Oct-17.
 */

public class DynamicData {

    private int id;
    private String name="";
    private String regionTypeName="";
    private int regionTypeId ;
    private String childRegion="";
    private String masterLocation="";
    private boolean isMAsterLocation=false;


    public int getParentRegionId() {
        return parentRegionId;
    }

    public void setParentRegionId(int parentRegionId) {
        this.parentRegionId = parentRegionId;
    }

    private int parentRegionId;

    public int getRegionTypeId() {
        return regionTypeId;
    }

    public void setRegionTypeId(int regionTypeId) {
        this.regionTypeId = regionTypeId;
    }

    public String getChildRegion() {
        return childRegion;
    }

    public void setChildRegion(String childRegion) {
        this.childRegion = childRegion;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getRegionTypeName() {
        return regionTypeName;
    }

    public void setRegionTypeName(String regionTypeName) {
        this.regionTypeName = regionTypeName;
    }


    public String getMasterLocation(){
        return masterLocation;
    }

    public void setMasterLocation(String masterLocation){
        this.masterLocation = masterLocation;
    }

    public boolean isMAsterLocation(){
        return isMAsterLocation;
    }

    public void setMAsterLocation(boolean MAsterLocation){
        isMAsterLocation = MAsterLocation;
    }
}
