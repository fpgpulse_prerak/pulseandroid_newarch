package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mansurum on 02-Apr-18.
 */

public class GamificationPostAPIPojo{
    @SerializedName("tenantId")
    @Expose
    private Integer tenantId;
    @SerializedName("regionTypeId")
    @Expose
    private Integer regionTypeId;
    @SerializedName("regionId")
    @Expose
    private Integer regionId;
    @SerializedName("tenantLocationId")
    @Expose
    private Integer tenantLocationId;
    @SerializedName("locationGroupId")
    @Expose
    private Integer locationGroupId;
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("isCompare")
    @Expose
    private Boolean isCompare;
    @SerializedName("compFrom")
    @Expose
    private Object compFrom;
    @SerializedName("compTo")
    @Expose
    private Object compTo;
    @SerializedName("fromDateTime")
    @Expose
    private Long fromDateTime;
    @SerializedName("toDateTime")
    @Expose
    private Long toDateTime;
    @SerializedName("compFromDateTime")
    @Expose
    private Object compFromDateTime;
    @SerializedName("compToDateTime")
    @Expose
    private Object compToDateTime;
    @SerializedName("metricsDateType")
    @Expose
    private String metricsDateType;
    @SerializedName("jsonValue")
    @Expose
    private Object jsonValue;
    @SerializedName("groupBy")
    @Expose
    private Object groupBy;
    @SerializedName("dimensionSet")
    @Expose
    private Object dimensionSet;

    public Integer getTenantId(){
        return tenantId;
    }

    public void setTenantId(Integer tenantId){
        this.tenantId = tenantId;
    }

    public Integer getRegionTypeId(){
        return regionTypeId;
    }

    public void setRegionTypeId(Integer regionTypeId){
        this.regionTypeId = regionTypeId;
    }

    public Integer getRegionId(){
        return regionId;
    }

    public void setRegionId(Integer regionId){
        this.regionId = regionId;
    }

    public Integer getTenantLocationId(){
        return tenantLocationId;
    }

    public void setTenantLocationId(Integer tenantLocationId){
        this.tenantLocationId = tenantLocationId;
    }

    public Integer getLocationGroupId(){
        return locationGroupId;
    }

    public void setLocationGroupId(Integer locationGroupId){
        this.locationGroupId = locationGroupId;
    }

    public Integer getProductId(){
        return productId;
    }

    public void setProductId(Integer productId){
        this.productId = productId;
    }

    public Integer getUserId(){
        return userId;
    }

    public void setUserId(Integer userId){
        this.userId = userId;
    }

    public String getFrom(){
        return from;
    }

    public void setFrom(String from){
        this.from = from;
    }

    public String getTo(){
        return to;
    }

    public void setTo(String to){
        this.to = to;
    }

    public Boolean getIsCompare(){
        return isCompare;
    }

    public void setIsCompare(Boolean isCompare){
        this.isCompare = isCompare;
    }

    public Object getCompFrom(){
        return compFrom;
    }

    public void setCompFrom(Object compFrom){
        this.compFrom = compFrom;
    }

    public Object getCompTo(){
        return compTo;
    }

    public void setCompTo(Object compTo){
        this.compTo = compTo;
    }

    public Long getFromDateTime(){
        return fromDateTime;
    }

    public void setFromDateTime(Long fromDateTime){
        this.fromDateTime = fromDateTime;
    }

    public Long getToDateTime(){
        return toDateTime;
    }

    public void setToDateTime(Long toDateTime){
        this.toDateTime = toDateTime;
    }

    public Object getCompFromDateTime(){
        return compFromDateTime;
    }

    public void setCompFromDateTime(Object compFromDateTime){
        this.compFromDateTime = compFromDateTime;
    }

    public Object getCompToDateTime(){
        return compToDateTime;
    }

    public void setCompToDateTime(Object compToDateTime){
        this.compToDateTime = compToDateTime;
    }

    public String getMetricsDateType(){
        return metricsDateType;
    }

    public void setMetricsDateType(String metricsDateType){
        this.metricsDateType = metricsDateType;
    }

    public Object getJsonValue(){
        return jsonValue;
    }

    public void setJsonValue(Object jsonValue){
        this.jsonValue = jsonValue;
    }

    public Object getGroupBy(){
        return groupBy;
    }

    public void setGroupBy(Object groupBy){
        this.groupBy = groupBy;
    }

    public Object getDimensionSet(){
        return dimensionSet;
    }

    public void setDimensionSet(Object dimensionSet){
        this.dimensionSet = dimensionSet;
    }
}
