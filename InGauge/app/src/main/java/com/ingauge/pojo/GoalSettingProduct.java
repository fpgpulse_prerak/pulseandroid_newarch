package com.ingauge.pojo;


import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */
public class GoalSettingProduct implements Serializable {

	private static final long serialVersionUID = 1L;


	/*@property (nonatomic, strong) NSNumber              *productID;
    @property (nonatomic, strong) NSNumber              *industry;
    @property (nonatomic, strong) NSNumber              *isMajor;
    @property (nonatomic, strong) NSNumber              *isRecurring;
    @property (nonatomic, strong) NSNumber              *major;
    @property (nonatomic, strong) NSString              *name;
    @property (nonatomic, strong) NSString              *productCategory;
    @property (nonatomic, strong) NSString              *productSKU;
    @property (nonatomic, strong) NSNumber              *productType;
    @property (nonatomic, strong) NSNumber              *recurring;
    @property (nonatomic, strong) NSNumber              *status;
    @property (nonatomic, strong) NSDate                *timestamp;
    @property (nonatomic, strong) NSDate                *updatedTime;
    */
	private Integer id;
	private Integer industry;
	private Boolean isMajor;
	private Boolean isRecurring;
	private Boolean major;
	private String name;
	private String productCategory;
	private String productSKU;
	private String productType;
	private Boolean recurring;
	private Integer status;
	private String timestamp;
	private String updatedTime;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductSKU() {
		return productSKU;
	}

	public void setProductSKU(String productSKU) {
		this.productSKU = productSKU;
	}

	public Integer getIndustry() {
		return industry;
	}

	public void setIndustry(Integer industry) {
		this.industry = industry;
	}

	public Boolean getIsMajor() {
		return isMajor;
	}

	public void setIsMajor(Boolean isMajor) {
		this.isMajor = isMajor;
	}

	public Boolean getIsRecurring() {
		return isRecurring;
	}

	public void setIsRecurring(Boolean isRecurring) {
		this.isRecurring = isRecurring;
	}

	public Boolean getMajor() {
		return major;
	}

	public void setMajor(Boolean major) {
		this.major = major;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public Boolean getRecurring() {
		return recurring;
	}

	public void setRecurring(Boolean recurring) {
		this.recurring = recurring;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}


}
