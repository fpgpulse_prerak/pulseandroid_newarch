package com.ingauge.pojo;

import java.io.Serializable;

/**
 * Created by pathanaa on 19-07-2017.
 */

public class ObservationDashboardStatusModel implements Serializable{

    private int StatusID=0;
    private String StatusName="";
    private boolean isSelectedPosition = false;


    public int getStatusID() {
        return StatusID;
    }

    public void setStatusID(int statusID) {
        StatusID = statusID;
    }

    public String getStatusName() {
        return StatusName;
    }

    public void setStatusName(String statusName) {
        StatusName = statusName;
    }

    public boolean isSelectedPosition() {
        return isSelectedPosition;
    }

    public void setSelectedPosition(boolean selectedPosition) {
        isSelectedPosition = selectedPosition;
    }
}
