package com.ingauge.pojo;

/**
 * Created by mansurum on 19-Jul-17.
 */

public class MoreDataModel {

    public int labelDrawable;
    public String label;

    public MoreDataModel(int labelDrawable, String label) {
        this.labelDrawable = labelDrawable;
        this.label = label;
    }
}
