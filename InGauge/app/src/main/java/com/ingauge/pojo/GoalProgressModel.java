package com.ingauge.pojo;

import android.content.Context;

import com.ingauge.R;
import com.ingauge.activities.HomeActivity;
import com.ingauge.session.InGaugeSession;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by mansurum on 16-Jun-17.
 */

public class GoalProgressModel {
    public static Context mContext;

    public static LinkedHashMap<String, List<String>> getData(Context mcontext, HomeActivity mHomeActivity) {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<>();

        mContext = mcontext;
        List<String> weeklyGoalDetails = new ArrayList<String>();
        weeklyGoalDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_title_weekly_goal)));
        weeklyGoalDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_this_week)));

        List<String> goalDetails = new ArrayList<String>();

        goalDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_goal_details_value_1)));
        goalDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_goal_details_value_2)));
        goalDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_goal_details_value_3)));
        goalDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_goal_details_value_4)));

        List<String> revenueDetails = new ArrayList<String>();


        if (InGaugeSession.read(mcontext.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)) {
            revenueDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details_value_1)));
            revenueDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details_value_2)));
            revenueDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details_value_3)));
            revenueDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details_value_4)));
            revenueDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details_value_5)));
        } else {
            revenueDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details_value_1)));
            revenueDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details_value_2)));
            revenueDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details_value_3)));

        }

        List<String> productGoals = new ArrayList<String>();
        productGoals.add("");


        List<String> effortDetails = new ArrayList<String>();

        if (InGaugeSession.read(mcontext.getResources().getString(R.string.key_selected_industry_id), 1).equals(1)) {
            effortDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_effort_details_value_1)));
            effortDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_effort_details_value_2)));
            effortDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_effort_details_value_3)));
            effortDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_effort_details_value_4)));
        } else {
            effortDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_effort_details_value_3)));
            effortDetails.add(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_effort_details_value_4)));
        }

        expandableListDetail.put(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_title_weekly_goal)), weeklyGoalDetails);
        expandableListDetail.put(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_title_goal_details)), goalDetails);
        expandableListDetail.put(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_revenue_details)), revenueDetails);
        expandableListDetail.put(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_product_goals)), productGoals);
        expandableListDetail.put(mHomeActivity.mobilekeyObject.optString(mcontext.getResources().getString(R.string.mobile_key_title_effort_details)), effortDetails);
        return expandableListDetail;
    }
}
