package com.ingauge.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 23-May-17.
 */

public class FilterBaseData implements Parcelable {
    public String id = "";
    public String name = "";
    public String filterName = "";
    public int filterIndex;
    public String filterMetricType = "";
    public boolean isSelectedPosition = false;
    public boolean isDynamicFilter = false;
    public int isEditable = 1;
    public int regionTypeId = -1;
    public int level = -1;
    public boolean isDynamicFilterSelected = false;
    public boolean isSubRegionOpen= false;
    public int parentRegionId=0;
    public List<FilterBaseData> mFilterBaseDatas = new ArrayList<>();
    public boolean isMajor = false;
    public int selectedItemCount=0;
    public String hotelMetricType="";
    public boolean isMasterLocation=false;

    public FilterBaseData() {

    }

    protected FilterBaseData(Parcel in) {
        id = in.readString();
        name = in.readString();
        isEditable = in.readInt();
        filterName = in.readString();
        filterIndex = in.readInt();
        filterMetricType = in.readString();
        isSelectedPosition = in.readByte() != 0;
        isDynamicFilter = in.readByte() != 0;
        regionTypeId = in.readInt();
        level = in.readInt();
        isDynamicFilterSelected = in.readByte() != 0;
        parentRegionId = in.readInt();
        isMajor = in.readByte() != 0;
        hotelMetricType = in.readString();
        isMasterLocation= in.readByte() != 0;


    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(filterName);
        dest.writeInt(filterIndex);
        dest.writeInt(isEditable);
        dest.writeString(filterMetricType);
        dest.writeByte((byte) (isSelectedPosition ? 1 : 0));
        dest.writeByte((byte) (isDynamicFilter ? 1 : 0));
        dest.writeInt(regionTypeId);
        dest.writeInt(level);
        dest.writeByte((byte) (isDynamicFilterSelected ? 1 : 0));
        dest.writeInt(parentRegionId);
        dest.writeByte((byte) (isMajor ? 1 : 0));
        dest.writeString(hotelMetricType);
        dest.writeByte((byte) (isMasterLocation ? 1 : 0));

    }

    @Override
    public int describeContents() {
        return 0;
    }


    public static final Creator<FilterBaseData> CREATOR = new Creator<FilterBaseData>() {
        @Override
        public FilterBaseData createFromParcel(Parcel in) {
            return new FilterBaseData(in);
        }

        @Override
        public FilterBaseData[] newArray(int size) {
            return new FilterBaseData[size];
        }
    };
}
