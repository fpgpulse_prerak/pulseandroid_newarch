package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mansurum on 24-May-17.
 */

public class ProductListFromLocationGroupModel implements Serializable{

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("createdOn")
        @Expose
        private Double createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Double updatedOn;
        @SerializedName("availableDate")
        @Expose
        private Integer availableDate;
        @SerializedName("groupProductCategoryId")
        @Expose
        private Integer groupProductCategoryId;
        @SerializedName("groupProductCategoryName")
        @Expose
        private String groupProductCategoryName;
        @SerializedName("incentiveDiscount")
        @Expose
        private String incentiveDiscount;
        @SerializedName("isExcludedProduct")
        @Expose
        private Boolean isExcludedProduct;
        @SerializedName("isGoalExcludedProduct")
        @Expose
        private Boolean isGoalExcludedProduct;
        @SerializedName("isIndividualUpsell")
        @Expose
        private Boolean isIndividualUpsell;
        @SerializedName("isMajor")
        @Expose
        private Boolean isMajor;
        @SerializedName("isRecurring")
        @Expose
        private Boolean isRecurring;
        @SerializedName("measurementUnit")
        @Expose
        private String measurementUnit;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("productAvailability")
        @Expose
        private String productAvailability;
        @SerializedName("productCost")
        @Expose
        private Integer productCost;
        @SerializedName("productType")
        @Expose
        private String productType;
        @SerializedName("thumbnailImage")
        @Expose
        private Integer thumbnailImage;
        @SerializedName("locationGroupId")
        @Expose
        private Integer locationGroupId;
        @SerializedName("locationGroupName")
        @Expose
        private String locationGroupName;
        @SerializedName("tenantProductId")
        @Expose
        private Integer tenantProductId;
        @SerializedName("tenantProductName")
        @Expose
        private String tenantProductName;
        @SerializedName("locationGroupTenantLocationId")
        @Expose
        private Integer locationGroupTenantLocationId;
        @SerializedName("locationGroupTenantLocationName")
        @Expose
        private String locationGroupTenantLocationName;
        @SerializedName("locationGroupGroupTypeId")
        @Expose
        private Integer locationGroupGroupTypeId;
        @SerializedName("locationGroupGroupTypeName")
        @Expose
        private String locationGroupGroupTypeName;
        @SerializedName("locationGroupTenantLocationTenantId")
        @Expose
        private Integer locationGroupTenantLocationTenantId;
        @SerializedName("locationGroupTenantProductId")
        @Expose
        private Integer locationGroupTenantProductId;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("productSKU")
        @Expose
        private String productSKU;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public Double getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Double createdOn) {
            this.createdOn = createdOn;
        }

        public Double getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(double updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Integer getAvailableDate() {
            return availableDate;
        }

        public void setAvailableDate(Integer availableDate) {
            this.availableDate = availableDate;
        }

        public Integer getGroupProductCategoryId() {
            return groupProductCategoryId;
        }

        public void setGroupProductCategoryId(Integer groupProductCategoryId) {
            this.groupProductCategoryId = groupProductCategoryId;
        }

        public String getGroupProductCategoryName() {
            return groupProductCategoryName;
        }

        public void setGroupProductCategoryName(String groupProductCategoryName) {
            this.groupProductCategoryName = groupProductCategoryName;
        }

        public String getIncentiveDiscount() {
            return incentiveDiscount;
        }

        public void setIncentiveDiscount(String incentiveDiscount) {
            this.incentiveDiscount = incentiveDiscount;
        }

        public Boolean getIsExcludedProduct() {
            return isExcludedProduct;
        }

        public void setIsExcludedProduct(Boolean isExcludedProduct) {
            this.isExcludedProduct = isExcludedProduct;
        }

        public Boolean getIsGoalExcludedProduct() {
            return isGoalExcludedProduct;
        }

        public void setIsGoalExcludedProduct(Boolean isGoalExcludedProduct) {
            this.isGoalExcludedProduct = isGoalExcludedProduct;
        }

        public Boolean getIsIndividualUpsell() {
            return isIndividualUpsell;
        }

        public void setIsIndividualUpsell(Boolean isIndividualUpsell) {
            this.isIndividualUpsell = isIndividualUpsell;
        }

        public Boolean getIsMajor() {
            return isMajor;
        }

        public void setIsMajor(Boolean isMajor) {
            this.isMajor = isMajor;
        }

        public Boolean getIsRecurring() {
            return isRecurring;
        }

        public void setIsRecurring(Boolean isRecurring) {
            this.isRecurring = isRecurring;
        }

        public String getMeasurementUnit() {
            return measurementUnit;
        }

        public void setMeasurementUnit(String measurementUnit) {
            this.measurementUnit = measurementUnit;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProductAvailability() {
            return productAvailability;
        }

        public void setProductAvailability(String productAvailability) {
            this.productAvailability = productAvailability;
        }

        public Integer getProductCost() {
            return productCost;
        }

        public void setProductCost(Integer productCost) {
            this.productCost = productCost;
        }

        public String getProductType() {
            return productType;
        }

        public void setProductType(String productType) {
            this.productType = productType;
        }

        public Integer getThumbnailImage() {
            return thumbnailImage;
        }

        public void setThumbnailImage(Integer thumbnailImage) {
            this.thumbnailImage = thumbnailImage;
        }

        public Integer getLocationGroupId() {
            return locationGroupId;
        }

        public void setLocationGroupId(Integer locationGroupId) {
            this.locationGroupId = locationGroupId;
        }

        public String getLocationGroupName() {
            return locationGroupName;
        }

        public void setLocationGroupName(String locationGroupName) {
            this.locationGroupName = locationGroupName;
        }

        public Integer getTenantProductId() {
            return tenantProductId;
        }

        public void setTenantProductId(Integer tenantProductId) {
            this.tenantProductId = tenantProductId;
        }

        public String getTenantProductName() {
            return tenantProductName;
        }

        public void setTenantProductName(String tenantProductName) {
            this.tenantProductName = tenantProductName;
        }

        public Integer getLocationGroupTenantLocationId() {
            return locationGroupTenantLocationId;
        }

        public void setLocationGroupTenantLocationId(Integer locationGroupTenantLocationId) {
            this.locationGroupTenantLocationId = locationGroupTenantLocationId;
        }

        public String getLocationGroupTenantLocationName() {
            return locationGroupTenantLocationName;
        }

        public void setLocationGroupTenantLocationName(String locationGroupTenantLocationName) {
            this.locationGroupTenantLocationName = locationGroupTenantLocationName;
        }

        public Integer getLocationGroupGroupTypeId() {
            return locationGroupGroupTypeId;
        }

        public void setLocationGroupGroupTypeId(Integer locationGroupGroupTypeId) {
            this.locationGroupGroupTypeId = locationGroupGroupTypeId;
        }

        public String getLocationGroupGroupTypeName() {
            return locationGroupGroupTypeName;
        }

        public void setLocationGroupGroupTypeName(String locationGroupGroupTypeName) {
            this.locationGroupGroupTypeName = locationGroupGroupTypeName;
        }

        public Integer getLocationGroupTenantLocationTenantId() {
            return locationGroupTenantLocationTenantId;
        }

        public void setLocationGroupTenantLocationTenantId(Integer locationGroupTenantLocationTenantId) {
            this.locationGroupTenantLocationTenantId = locationGroupTenantLocationTenantId;
        }

        public Integer getLocationGroupTenantProductId() {
            return locationGroupTenantProductId;
        }

        public void setLocationGroupTenantProductId(Integer locationGroupTenantProductId) {
            this.locationGroupTenantProductId = locationGroupTenantProductId;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getProductSKU() {
            return productSKU;
        }

        public void setProductSKU(String productSKU) {
            this.productSKU = productSKU;
        }

    }
}
