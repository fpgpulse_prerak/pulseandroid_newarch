package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mansurum on 02-Apr-18.
 */

public class AgentProfileDetailsPojo{

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData(){
        return data;
    }

    public void setData(Data data){
        this.data = data;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public Integer getStatusCode(){
        return statusCode;
    }

    public void setStatusCode(Integer statusCode){
        this.statusCode = statusCode;
    }

    public class Data{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private Integer createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Integer updatedOn;
        @SerializedName("birthDay")
        @Expose
        private Integer birthDay;
        @SerializedName("birthMonth")
        @Expose
        private Integer birthMonth;
        @SerializedName("countryCode")
        @Expose
        private String countryCode;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userFullName")
        @Expose
        private String userFullName;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("isFirstLogin")
        @Expose
        private Boolean isFirstLogin;
        @SerializedName("isFpgMember")
        @Expose
        private Boolean isFpgMember;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("linkedinId")
        @Expose
        private String linkedinId;
        @SerializedName("linkedinImage")
        @Expose
        private String linkedinImage;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("workingType")
        @Expose
        private String workingType;
        @SerializedName("blockEvent")
        @Expose
        private String blockEvent;
        @SerializedName("userVal")
        @Expose
        private String userVal;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("speaks")
        @Expose
        private String speaks;

        @SerializedName("hobbies")
        @Expose
        private String hobbies;

        @SerializedName("mapTo")
        @Expose
        private String mapTo;




        public String getMapTo(){
            return mapTo;
        }

        public void setMapTo(String mapTo){
            this.mapTo = mapTo;
        }




        public String getSpeaks(){
            return speaks;
        }

        public void setSpeaks(String speaks){
            this.speaks = speaks;
        }

        public String getHobbies(){
            return hobbies;
        }

        public void setHobbies(String hobbies){
            this.hobbies = hobbies;
        }


        public Integer getId(){
            return id;
        }

        public void setId(Integer id){
            this.id = id;
        }

        public String getActiveStatus(){
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus){
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedOn(){
            return createdOn;
        }

        public void setCreatedOn(Integer createdOn){
            this.createdOn = createdOn;
        }

        public Integer getUpdatedOn(){
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn){
            this.updatedOn = updatedOn;
        }

        public Integer getBirthDay(){
            return birthDay;
        }

        public void setBirthDay(Integer birthDay){
            this.birthDay = birthDay;
        }

        public Integer getBirthMonth(){
            return birthMonth;
        }

        public void setBirthMonth(Integer birthMonth){
            this.birthMonth = birthMonth;
        }

        public String getCountryCode(){
            return countryCode;
        }

        public void setCountryCode(String countryCode){
            this.countryCode = countryCode;
        }

        public Integer getDefaultIndustry(){
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry){
            this.defaultIndustry = defaultIndustry;
        }

        public String getEmail(){
            return email;
        }

        public void setEmail(String email){
            this.email = email;
        }

        public String getUserFullName(){
            return userFullName;
        }

        public void setUserFullName(String userFullName){
            this.userFullName = userFullName;
        }

        public String getFirstName(){
            return firstName;
        }

        public void setFirstName(String firstName){
            this.firstName = firstName;
        }

        public String getFpgLevel(){
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel){
            this.fpgLevel = fpgLevel;
        }

        public String getGender(){
            return gender;
        }

        public void setGender(String gender){
            this.gender = gender;
        }

        public String getHiredDate(){
            return hiredDate;
        }

        public void setHiredDate(String hiredDate){
            this.hiredDate = hiredDate;
        }

        public Boolean getIsAuthyAttempted(){
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted){
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public Boolean getIsFirstLogin(){
            return isFirstLogin;
        }

        public void setIsFirstLogin(Boolean isFirstLogin){
            this.isFirstLogin = isFirstLogin;
        }

        public Boolean getIsFpgMember(){
            return isFpgMember;
        }

        public void setIsFpgMember(Boolean isFpgMember){
            this.isFpgMember = isFpgMember;
        }

        public String getJobTitle(){
            return jobTitle;
        }

        public void setJobTitle(String jobTitle){
            this.jobTitle = jobTitle;
        }

        public String getLastName(){
            return lastName;
        }

        public void setLastName(String lastName){
            this.lastName = lastName;
        }

        public String getLinkedinId(){
            return linkedinId;
        }

        public void setLinkedinId(String linkedinId){
            this.linkedinId = linkedinId;
        }

        public String getLinkedinImage(){
            return linkedinImage;
        }

        public void setLinkedinImage(String linkedinImage){
            this.linkedinImage = linkedinImage;
        }

        public String getMiddleName(){
            return middleName;
        }

        public void setMiddleName(String middleName){
            this.middleName = middleName;
        }

        public String getNumber(){
            return number;
        }

        public void setNumber(String number){
            this.number = number;
        }

        public String getPassword(){
            return password;
        }

        public void setPassword(String password){
            this.password = password;
        }

        public String getPhoneNumber(){
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber){
            this.phoneNumber = phoneNumber;
        }

        public String getPulseId(){
            return pulseId;
        }

        public void setPulseId(String pulseId){
            this.pulseId = pulseId;
        }

        public String getSalt(){
            return salt;
        }

        public void setSalt(String salt){
            this.salt = salt;
        }

        public String getWorkAge(){
            return workAge;
        }

        public void setWorkAge(String workAge){
            this.workAge = workAge;
        }

        public String getWorkingType(){
            return workingType;
        }

        public void setWorkingType(String workingType){
            this.workingType = workingType;
        }

        public String getBlockEvent(){
            return blockEvent;
        }

        public void setBlockEvent(String blockEvent){
            this.blockEvent = blockEvent;
        }

        public String getUserVal(){
            return userVal;
        }

        public void setUserVal(String userVal){
            this.userVal = userVal;
        }

        public String getName(){
            return name;
        }

        public void setName(String name){
            this.name = name;
        }

        public String getCnameTime(){
            return cnameTime;
        }

        public void setCnameTime(String cnameTime){
            this.cnameTime = cnameTime;
        }

        public String getUnameTime(){
            return unameTime;
        }

        public void setUnameTime(String unameTime){
            this.unameTime = unameTime;
        }

        public String getCreatedByWithDateTime(){
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime){
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getUpdatedByWithDateTime(){
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime){
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime(){
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime){
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

    }
}
