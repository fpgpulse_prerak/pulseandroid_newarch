package com.ingauge.pojo;


import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */
public class GoalProductInsights implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double avgRevenue;
	private Double avgUnits;
	private String maxDate;
	private Integer maximumDays;
	private Integer productId;
	private Integer remainingDays;
	private Integer totalNumberOfUnits;
	private Double totalRevenue;

	public Integer getMaximumDays() {
		return maximumDays;
	}

	public void setMaximumDays(Integer maximumDays) {
		this.maximumDays = maximumDays;
	}

	public Double getAvgRevenue() {
		return avgRevenue;
	}

	public void setAvgRevenue(Double avgRevenue) {
		this.avgRevenue = avgRevenue;
	}

	public Double getAvgUnits() {
		return avgUnits;
	}

	public void setAvgUnits(Double avgUnits) {
		this.avgUnits = avgUnits;
	}

	public String getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getRemainingDays() {
		return remainingDays;
	}

	public void setRemainingDays(Integer remainingDays) {
		this.remainingDays = remainingDays;
	}

	public Integer getTotalNumberOfUnits() {
		return totalNumberOfUnits;
	}

	public void setTotalNumberOfUnits(Integer totalNumberOfUnits) {
		this.totalNumberOfUnits = totalNumberOfUnits;
	}

	public Double getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(Double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

}
