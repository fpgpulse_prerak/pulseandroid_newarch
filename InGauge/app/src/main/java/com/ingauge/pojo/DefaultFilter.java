package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pathanaa on 07-06-2017.
 */

public class DefaultFilter {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Data {

        @SerializedName("REGION_TYPE")
        @Expose
        private Integer rEGIONTYPE;
        @SerializedName("LOCATION")
        @Expose
        private Integer lOCATION;
        @SerializedName("LOCATION_GROUP")
        @Expose
        private Integer lOCATIONGROUP;
        @SerializedName("PRODUCT")
        @Expose
        private Integer pRODUCT;
        @SerializedName("USER")
        @Expose
        private Integer uSER;
        @SerializedName("REGION")
        @Expose
        private Integer rEGION;

        public Integer getREGIONTYPE() {
            return rEGIONTYPE;
        }

        public void setREGIONTYPE(Integer rEGIONTYPE) {
            this.rEGIONTYPE = rEGIONTYPE;
        }

        public Integer getLOCATION() {
            return lOCATION;
        }

        public void setLOCATION(Integer lOCATION) {
            this.lOCATION = lOCATION;
        }

        public Integer getLOCATIONGROUP() {
            return lOCATIONGROUP;
        }

        public void setLOCATIONGROUP(Integer lOCATIONGROUP) {
            this.lOCATIONGROUP = lOCATIONGROUP;
        }

        public Integer getPRODUCT() {
            return pRODUCT;
        }

        public void setPRODUCT(Integer pRODUCT) {
            this.pRODUCT = pRODUCT;
        }

        public Integer getUSER() {
            return uSER;
        }

        public void setUSER(Integer uSER) {
            this.uSER = uSER;
        }

        public Integer getREGION() {
            return rEGION;
        }

        public void setREGION(Integer rEGION) {
            this.rEGION = rEGION;
        }

    }

}