package com.ingauge.pojo;



import java.io.Serializable;

/**
 * Created by davepriy on 3/2/2016.
 */
public class GoalSettingDepartment implements Serializable {

	private static final long serialVersionUID = 1L;



	/*@property (nonatomic, strong) NSNumber              *departmentID;
        @property (nonatomic, strong) NSString              *locationName;
        @property (nonatomic, strong) NSString              *name;
        @property (nonatomic, strong) NSString              *organizationName;
        @property (nonatomic, strong) NSNumber              *selected;
        @property (nonatomic, strong) NSNumber              *status;
        @property (nonatomic, strong) NSDate                *timestamp;
        @property (nonatomic, strong) NSDate                *updatedtime;
    */
	private Integer id;
	private String locationName;
	private String name;
	private String organizationName;
	private Boolean selected;
	private Integer status;
	private String timestamp;
	private String updatedtime;

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getUpdatedtime() {
		return updatedtime;
	}

	public void setUpdatedtime(String updatedtime) {
		this.updatedtime = updatedtime;
	}



}
