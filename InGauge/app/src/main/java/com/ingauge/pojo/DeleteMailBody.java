package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mansurum on 19-Jul-17.
 */

public class DeleteMailBody {

    @SerializedName("ids")
    @Expose
    private List<String> ids = null;
    @SerializedName("type")
    @Expose
    private String type;

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
