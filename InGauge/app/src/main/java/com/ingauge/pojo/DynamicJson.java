package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mansurum on 16-Oct-17.
 */

public class DynamicJson {



    @SerializedName("data")
    public DataJsonObject mDataJsonObject;
    public DataJsonObject getmDataJsonObject() {
        return mDataJsonObject;
    }

    public void setmDataJsonObject(DataJsonObject mDataJsonObject) {
        this.mDataJsonObject = mDataJsonObject;
    }


    public class DataJsonObject{

       // Map<String,Map<Integer,DataInnerObject>> integerListMap = new LinkedHashMap<>();

        /*public Map<String, Map<Integer, DataInnerObject>> getIntegerListMap() {
            return integerListMap;
        }

        public void setIntegerListMap(Map<String, Map<Integer, DataInnerObject>> integerListMap) {
            this.integerListMap = integerListMap;
        }*/


    }

    public class DataInnerObject{


        @SerializedName("id")
        @Expose
        private int id;

        @SerializedName("name")
        @Expose
        private String name ="";

        @SerializedName("activeStatus")
        @Expose
        private int activeStatus;

        @SerializedName("regionTypeId")
        @Expose
        private int regionTypeId;

        @SerializedName("regionTypeName")
        @Expose
        private String regionTypeName="";

        @SerializedName("regionHierarchy")
        @Expose
        private String regionHierarchy="";

        @SerializedName("childRegion")
        @Expose
        private String childRegion="";

        @SerializedName("industryId")
        @Expose
        private int industryId;

        @SerializedName("childLocation")
        @Expose
        private String childLocation="";

        @SerializedName("childLocationName")
        @Expose
        private String childLocationName="";

        @SerializedName("complete")
        @Expose
        private boolean complete;
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(int activeStatus) {
            this.activeStatus = activeStatus;
        }

        public int getRegionTypeId() {
            return regionTypeId;
        }

        public void setRegionTypeId(int regionTypeId) {
            this.regionTypeId = regionTypeId;
        }

        public String getRegionTypeName() {
            return regionTypeName;
        }

        public void setRegionTypeName(String regionTypeName) {
            this.regionTypeName = regionTypeName;
        }

        public String getRegionHierarchy() {
            return regionHierarchy;
        }

        public void setRegionHierarchy(String regionHierarchy) {
            this.regionHierarchy = regionHierarchy;
        }

        public String getChildRegion() {
            return childRegion;
        }

        public void setChildRegion(String childRegion) {
            this.childRegion = childRegion;
        }

        public int getIndustryId() {
            return industryId;
        }

        public void setIndustryId(int industryId) {
            this.industryId = industryId;
        }

        public String getChildLocation() {
            return childLocation;
        }

        public void setChildLocation(String childLocation) {
            this.childLocation = childLocation;
        }

        public String getChildLocationName() {
            return childLocationName;
        }

        public void setChildLocationName(String childLocationName) {
            this.childLocationName = childLocationName;
        }

        public boolean isComplete() {
            return complete;
        }

        public void setComplete(boolean complete) {
            this.complete = complete;
        }


    }
}
