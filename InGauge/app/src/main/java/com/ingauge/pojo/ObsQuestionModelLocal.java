package com.ingauge.pojo;

/**
 * Created by pathanaa on 13-07-2017.
 */

public class ObsQuestionModelLocal {

    String q_title = "";
    String q_text = "";
    String q_yesnona_applicable = "";
    String q_yes_nona = "";
    int q_weight = 0;
    int q_degree = 0;
    boolean isNA = false;
    int section = 0;
    int q_number = 0;
    String q_sym = "";
    String q_id = "";
    boolean isYesSelected=false;
    boolean isNASelected=false;


    public String getQ_yesnona_applicable() {
        return q_yesnona_applicable;
    }

    public void setQ_yesnona_applicable(String q_yesnona_applicable) {
        this.q_yesnona_applicable = q_yesnona_applicable;
    }

    public boolean isNA() {
        return isNA;
    }

    public void setIsNA(boolean isNA) {
        this.isNA = isNA;
    }


    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }


    public int getQ_number() {
        return q_number;
    }

    public void setQ_number(int q_number) {
        this.q_number = q_number;
    }


    public String getQ_sym() {
        return q_sym;
    }

    public void setQ_sym(String q_sym) {
        this.q_sym = q_sym;
    }


    public int getQ_degree() {
        return q_degree;
    }

    public void setQ_degree(int q_degree) {
        this.q_degree = q_degree;
    }

    public int getQ_weight() {
        return q_weight;
    }

    public void setQ_weight(int q_weight) {
        this.q_weight = q_weight;
    }

    public String getQ_id() {
        return q_id;
    }

    public void setQ_id(String q_id) {
        this.q_id = q_id;
    }


    public String getQ_yes_nona() {
        return q_yes_nona;
    }

    public void setQ_yes_nona(String q_yes_nona) {
        this.q_yes_nona = q_yes_nona;
    }

    public String getQ_title() {
        return q_title;
    }

    public void setQ_title(String q_title) {
        this.q_title = q_title;
    }

    public String getQ_text() {
        return q_text;
    }

    public void setQ_text(String q_text) {
        this.q_text = q_text;
    }
    public boolean isYesSelected(){
        return isYesSelected;
    }

    public void setYesSelected(boolean yesSelected){
        isYesSelected = yesSelected;
    }

    public boolean isNASelected(){
        return isNASelected;
    }

    public void setNASelected(boolean NASelected){
        isNASelected = NASelected;
    }

}
