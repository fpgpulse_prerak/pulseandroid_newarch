package com.ingauge.pojo;

/**
 * Created by mansurum on 03-Oct-17.
 */

public class MailDetailParentList {


    private String sendderImageUrl="";
    private String senderName="";
    private String senderTime="";
    private String subject="";


    public String getSendderImageUrl() {
        return sendderImageUrl;
    }

    public void setSendderImageUrl(String sendderImageUrl) {
        this.sendderImageUrl = sendderImageUrl;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderTime() {
        return senderTime;
    }

    public void setSenderTime(String senderTime) {
        this.senderTime = senderTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


}
