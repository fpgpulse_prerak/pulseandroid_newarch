package com.ingauge.pojo;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by pathanaa on 19-07-2017.
 */

public class ObservationEditViewModel{
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private Integer createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Integer updatedOn;
        @SerializedName("androidVersion")
        @Expose
        private String androidVersion;
        @SerializedName("donotDisplayToAgent")
        @Expose
        private Boolean donotDisplayToAgent;
        @SerializedName("donotMailToAgent")
        @Expose
        private Boolean donotMailToAgent;
        @SerializedName("endDate")
        @Expose
        private long endDate;
        @SerializedName("iosVersion")
        @Expose
        private String iosVersion;
        @SerializedName("isScheduled")
        @Expose
        private Boolean isScheduled;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("questionnaireId")
        @Expose
        private Integer questionnaireId;
        @SerializedName("questionnaireName")
        @Expose
        private String questionnaireName;
        @SerializedName("questionnaireQuestionSections")
        @Expose
        private List<QuestionnaireQuestionSection> questionnaireQuestionSections = null;
        @SerializedName("recommendation")
        @Expose
        private String recommendation;
        @SerializedName("questionnaireSurveyCategoryId")
        @Expose
        private Integer questionnaireSurveyCategoryId;
        @SerializedName("questionnaireSurveyCategoryName")
        @Expose
        private String questionnaireSurveyCategoryName;
        @SerializedName("respondentId")
        @Expose
        private Integer respondentId;
        @SerializedName("respondentName")
        @Expose
        private String respondentName;
        @SerializedName("scoreVal")
        @Expose
        private Double scoreVal;
        @SerializedName("startDate")
        @Expose
        private long startDate;
        @SerializedName("surveyStatus")
        @Expose
        private String surveyStatus;
        @SerializedName("strengthText")
        @Expose
        private String strengthText;
        @SerializedName("surveyEntityType")
        @Expose
        private String surveyEntityType;
        @SerializedName("surveyorId")
        @Expose
        private Integer surveyorId;
        @SerializedName("surveyorName")
        @Expose
        private String surveyorName;
        @SerializedName("surveyTakenTime")
        @Expose
        private Integer surveyTakenTime;
        @SerializedName("symptomText")
        @Expose
        private String symptomText;
        @SerializedName("tenantLocationId")
        @Expose
        private Integer tenantLocationId;
        @SerializedName("tenantLocationName")
        @Expose
        private String tenantLocationName;
        @SerializedName("calendarEventId")
        @Expose
        private Integer calendarEventId;
        @SerializedName("strengths")
        @Expose
        private List<Strength> strengths = null;
        @SerializedName("surveyStrengths")
        @Expose
        private List<SurveyStrength> surveyStrengths = null;
        @SerializedName("questionAnswer")
        @Expose
        private List<QuestionAnswer> questionAnswer = null;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("tenantId")
        @Expose
        private Integer tenantId;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Integer createdOn) {
            this.createdOn = createdOn;
        }

        public Integer getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getAndroidVersion() {
            return androidVersion;
        }

        public void setAndroidVersion(String androidVersion) {
            this.androidVersion = androidVersion;
        }

        public Boolean getDonotDisplayToAgent() {
            return donotDisplayToAgent;
        }

        public void setDonotDisplayToAgent(Boolean donotDisplayToAgent) {
            this.donotDisplayToAgent = donotDisplayToAgent;
        }

        public Boolean getDonotMailToAgent() {
            return donotMailToAgent;
        }

        public void setDonotMailToAgent(Boolean donotMailToAgent) {
            this.donotMailToAgent = donotMailToAgent;
        }

        public long getEndDate() {
            return endDate;
        }

        public void setEndDate(long endDate) {
            this.endDate = endDate;
        }

        public String getIosVersion() {
            return iosVersion;
        }

        public void setIosVersion(String iosVersion) {
            this.iosVersion = iosVersion;
        }

        public Boolean getIsScheduled() {
            return isScheduled;
        }

        public void setIsScheduled(Boolean isScheduled) {
            this.isScheduled = isScheduled;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public Integer getQuestionnaireId() {
            return questionnaireId;
        }

        public void setQuestionnaireId(Integer questionnaireId) {
            this.questionnaireId = questionnaireId;
        }

        public String getQuestionnaireName() {
            return questionnaireName;
        }

        public void setQuestionnaireName(String questionnaireName) {
            this.questionnaireName = questionnaireName;
        }

        public List<QuestionnaireQuestionSection> getQuestionnaireQuestionSections() {
            return questionnaireQuestionSections;
        }

        public void setQuestionnaireQuestionSections(List<QuestionnaireQuestionSection> questionnaireQuestionSections) {
            this.questionnaireQuestionSections = questionnaireQuestionSections;
        }

        public String getRecommendation() {
            return recommendation;
        }

        public void setRecommendation(String recommendation) {
            this.recommendation = recommendation;
        }

        public Integer getQuestionnaireSurveyCategoryId() {
            return questionnaireSurveyCategoryId;
        }

        public void setQuestionnaireSurveyCategoryId(Integer questionnaireSurveyCategoryId) {
            this.questionnaireSurveyCategoryId = questionnaireSurveyCategoryId;
        }

        public String getQuestionnaireSurveyCategoryName() {
            return questionnaireSurveyCategoryName;
        }

        public void setQuestionnaireSurveyCategoryName(String questionnaireSurveyCategoryName) {
            this.questionnaireSurveyCategoryName = questionnaireSurveyCategoryName;
        }

        public Integer getRespondentId() {
            return respondentId;
        }

        public void setRespondentId(Integer respondentId) {
            this.respondentId = respondentId;
        }

        public String getRespondentName() {
            return respondentName;
        }

        public void setRespondentName(String respondentName) {
            this.respondentName = respondentName;
        }

        public Double getScoreVal() {
            return scoreVal;
        }

        public void setScoreVal(Double scoreVal) {
            this.scoreVal = scoreVal;
        }

        public long getStartDate() {
            return startDate;
        }

        public void setStartDate(long startDate) {
            this.startDate = startDate;
        }

        public String getSurveyStatus() {
            return surveyStatus;
        }

        public void setSurveyStatus(String surveyStatus) {
            this.surveyStatus = surveyStatus;
        }

        public String getStrengthText() {
            return strengthText;
        }

        public void setStrengthText(String strengthText) {
            this.strengthText = strengthText;
        }

        public String getSurveyEntityType() {
            return surveyEntityType;
        }

        public void setSurveyEntityType(String surveyEntityType) {
            this.surveyEntityType = surveyEntityType;
        }

        public Integer getSurveyorId() {
            return surveyorId;
        }

        public void setSurveyorId(Integer surveyorId) {
            this.surveyorId = surveyorId;
        }

        public String getSurveyorName() {
            return surveyorName;
        }

        public void setSurveyorName(String surveyorName) {
            this.surveyorName = surveyorName;
        }

        public Integer getSurveyTakenTime() {
            return surveyTakenTime;
        }

        public void setSurveyTakenTime(Integer surveyTakenTime) {
            this.surveyTakenTime = surveyTakenTime;
        }

        public String getSymptomText() {
            return symptomText;
        }

        public void setSymptomText(String symptomText) {
            this.symptomText = symptomText;
        }

        public Integer getTenantLocationId() {
            return tenantLocationId;
        }

        public void setTenantLocationId(Integer tenantLocationId) {
            this.tenantLocationId = tenantLocationId;
        }

        public String getTenantLocationName() {
            return tenantLocationName;
        }

        public void setTenantLocationName(String tenantLocationName) {
            this.tenantLocationName = tenantLocationName;
        }

        public Integer getCalendarEventId() {
            return calendarEventId;
        }

        public void setCalendarEventId(Integer calendarEventId) {
            this.calendarEventId = calendarEventId;
        }

        public List<Strength> getStrengths() {
            return strengths;
        }

        public void setStrengths(List<Strength> strengths) {
            this.strengths = strengths;
        }

        public List<SurveyStrength> getSurveyStrengths() {
            return surveyStrengths;
        }

        public void setSurveyStrengths(List<SurveyStrength> surveyStrengths) {
            this.surveyStrengths = surveyStrengths;
        }

        public List<QuestionAnswer> getQuestionAnswer() {
            return questionAnswer;
        }

        public void setQuestionAnswer(List<QuestionAnswer> questionAnswer) {
            this.questionAnswer = questionAnswer;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }


        public class QuestionnaireQuestionSection {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("industryId")
            @Expose
            private Integer industryId;
            @SerializedName("activeStatus")
            @Expose
            private String activeStatus;
            @SerializedName("degreeOfImportance")
            @Expose
            private Integer degreeOfImportance;
            @SerializedName("isDeleted")
            @Expose
            private Boolean isDeleted;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("questions")
            @Expose
            private List<Question> questions = null;
            @SerializedName("rank")
            @Expose
            private Integer rank;
            @SerializedName("strengthScore")
            @Expose
            private Integer strengthScore;
            @SerializedName("strengthText")
            @Expose
            private String strengthText;
            @SerializedName("surveyEntityType")
            @Expose
            private String surveyEntityType;
            @SerializedName("cnameTime")
            @Expose
            private String cnameTime;
            @SerializedName("unameTime")
            @Expose
            private String unameTime;
            @SerializedName("createdByWithDateTime")
            @Expose
            private String createdByWithDateTime;
            @SerializedName("updatedByWithDateTime")
            @Expose
            private String updatedByWithDateTime;
            @SerializedName("reviewedByWithDateTime")
            @Expose
            private String reviewedByWithDateTime;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getIndustryId() {
                return industryId;
            }

            public void setIndustryId(Integer industryId) {
                this.industryId = industryId;
            }

            public String getActiveStatus() {
                return activeStatus;
            }

            public void setActiveStatus(String activeStatus) {
                this.activeStatus = activeStatus;
            }

            public Integer getDegreeOfImportance() {
                return degreeOfImportance;
            }

            public void setDegreeOfImportance(Integer degreeOfImportance) {
                this.degreeOfImportance = degreeOfImportance;
            }

            public Boolean getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(Boolean isDeleted) {
                this.isDeleted = isDeleted;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<Question> getQuestions() {
                return questions;
            }

            public void setQuestions(List<Question> questions) {
                this.questions = questions;
            }

            public Integer getRank() {
                return rank;
            }

            public void setRank(Integer rank) {
                this.rank = rank;
            }

            public Integer getStrengthScore() {
                return strengthScore;
            }

            public void setStrengthScore(Integer strengthScore) {
                this.strengthScore = strengthScore;
            }

            public String getStrengthText() {
                return strengthText;
            }

            public void setStrengthText(String strengthText) {
                this.strengthText = strengthText;
            }

            public String getSurveyEntityType() {
                return surveyEntityType;
            }

            public void setSurveyEntityType(String surveyEntityType) {
                this.surveyEntityType = surveyEntityType;
            }

            public String getCnameTime() {
                return cnameTime;
            }

            public void setCnameTime(String cnameTime) {
                this.cnameTime = cnameTime;
            }

            public String getUnameTime() {
                return unameTime;
            }

            public void setUnameTime(String unameTime) {
                this.unameTime = unameTime;
            }

            public String getCreatedByWithDateTime() {
                return createdByWithDateTime;
            }

            public void setCreatedByWithDateTime(String createdByWithDateTime) {
                this.createdByWithDateTime = createdByWithDateTime;
            }

            public String getUpdatedByWithDateTime() {
                return updatedByWithDateTime;
            }

            public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                this.updatedByWithDateTime = updatedByWithDateTime;
            }

            public String getReviewedByWithDateTime() {
                return reviewedByWithDateTime;
            }

            public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                this.reviewedByWithDateTime = reviewedByWithDateTime;
            }


            public class Question {

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("industryId")
                @Expose
                private Integer industryId;
                @SerializedName("activeStatus")
                @Expose
                private String activeStatus;
                @SerializedName("isDeleted")
                @Expose
                private Boolean isDeleted;
                @SerializedName("questionText")
                @Expose
                private String questionText;
                @SerializedName("surveyEntityType")
                @Expose
                private String surveyEntityType;
                @SerializedName("symptom")
                @Expose
                private String symptom;
                @SerializedName("questionType")
                @Expose
                private String questionType;
                @SerializedName("weight")
                @Expose
                private Integer weight;
                @SerializedName("type")
                @Expose
                private String type;
                @SerializedName("required")
                @Expose
                private Boolean required;
                @SerializedName("cnameTime")
                @Expose
                private String cnameTime;
                @SerializedName("unameTime")
                @Expose
                private String unameTime;
                @SerializedName("createdByWithDateTime")
                @Expose
                private String createdByWithDateTime;
                @SerializedName("updatedByWithDateTime")
                @Expose
                private String updatedByWithDateTime;
                @SerializedName("reviewedByWithDateTime")
                @Expose
                private String reviewedByWithDateTime;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public Integer getIndustryId() {
                    return industryId;
                }

                public void setIndustryId(Integer industryId) {
                    this.industryId = industryId;
                }

                public String getActiveStatus() {
                    return activeStatus;
                }

                public void setActiveStatus(String activeStatus) {
                    this.activeStatus = activeStatus;
                }

                public Boolean getIsDeleted() {
                    return isDeleted;
                }

                public void setIsDeleted(Boolean isDeleted) {
                    this.isDeleted = isDeleted;
                }

                public String getQuestionText() {
                    return questionText;
                }

                public void setQuestionText(String questionText) {
                    this.questionText = questionText;
                }

                public String getSurveyEntityType() {
                    return surveyEntityType;
                }

                public void setSurveyEntityType(String surveyEntityType) {
                    this.surveyEntityType = surveyEntityType;
                }

                public String getSymptom() {
                    return symptom;
                }

                public void setSymptom(String symptom) {
                    this.symptom = symptom;
                }

                public String getQuestionType() {
                    return questionType;
                }

                public void setQuestionType(String questionType) {
                    this.questionType = questionType;
                }

                public Integer getWeight() {
                    return weight;
                }

                public void setWeight(Integer weight) {
                    this.weight = weight;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public Boolean getRequired() {
                    return required;
                }

                public void setRequired(Boolean required) {
                    this.required = required;
                }

                public String getCnameTime() {
                    return cnameTime;
                }

                public void setCnameTime(String cnameTime) {
                    this.cnameTime = cnameTime;
                }

                public String getUnameTime() {
                    return unameTime;
                }

                public void setUnameTime(String unameTime) {
                    this.unameTime = unameTime;
                }

                public String getCreatedByWithDateTime() {
                    return createdByWithDateTime;
                }

                public void setCreatedByWithDateTime(String createdByWithDateTime) {
                    this.createdByWithDateTime = createdByWithDateTime;
                }

                public String getUpdatedByWithDateTime() {
                    return updatedByWithDateTime;
                }

                public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                    this.updatedByWithDateTime = updatedByWithDateTime;
                }

                public String getReviewedByWithDateTime() {
                    return reviewedByWithDateTime;
                }

                public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                    this.reviewedByWithDateTime = reviewedByWithDateTime;
                }

            }


        }

        public class Strength {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("industryId")
            @Expose
            private Integer industryId;
            @SerializedName("activeStatus")
            @Expose
            private String activeStatus;
            @SerializedName("createdById")
            @Expose
            private Integer createdById;
            @SerializedName("updatedById")
            @Expose
            private Integer updatedById;
            @SerializedName("createdOn")
            @Expose
            private Integer createdOn;
            @SerializedName("updatedOn")
            @Expose
            private Integer updatedOn;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("cnameTime")
            @Expose
            private String cnameTime;
            @SerializedName("unameTime")
            @Expose
            private String unameTime;
            @SerializedName("createdByWithDateTime")
            @Expose
            private String createdByWithDateTime;
            @SerializedName("updatedByWithDateTime")
            @Expose
            private String updatedByWithDateTime;
            @SerializedName("reviewedByWithDateTime")
            @Expose
            private String reviewedByWithDateTime;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getIndustryId() {
                return industryId;
            }

            public void setIndustryId(Integer industryId) {
                this.industryId = industryId;
            }

            public String getActiveStatus() {
                return activeStatus;
            }

            public void setActiveStatus(String activeStatus) {
                this.activeStatus = activeStatus;
            }

            public Integer getCreatedById() {
                return createdById;
            }

            public void setCreatedById(Integer createdById) {
                this.createdById = createdById;
            }

            public Integer getUpdatedById() {
                return updatedById;
            }

            public void setUpdatedById(Integer updatedById) {
                this.updatedById = updatedById;
            }

            public Integer getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(Integer createdOn) {
                this.createdOn = createdOn;
            }

            public Integer getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(Integer updatedOn) {
                this.updatedOn = updatedOn;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCnameTime() {
                return cnameTime;
            }

            public void setCnameTime(String cnameTime) {
                this.cnameTime = cnameTime;
            }

            public String getUnameTime() {
                return unameTime;
            }

            public void setUnameTime(String unameTime) {
                this.unameTime = unameTime;
            }

            public String getCreatedByWithDateTime() {
                return createdByWithDateTime;
            }

            public void setCreatedByWithDateTime(String createdByWithDateTime) {
                this.createdByWithDateTime = createdByWithDateTime;
            }

            public String getUpdatedByWithDateTime() {
                return updatedByWithDateTime;
            }

            public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                this.updatedByWithDateTime = updatedByWithDateTime;
            }

            public String getReviewedByWithDateTime() {
                return reviewedByWithDateTime;
            }

            public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                this.reviewedByWithDateTime = reviewedByWithDateTime;
            }

        }

        public class SurveyStrength {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("industryId")
            @Expose
            private Integer industryId;
            @SerializedName("activeStatus")
            @Expose
            private String activeStatus;
            @SerializedName("createdById")
            @Expose
            private Integer createdById;
            @SerializedName("updatedById")
            @Expose
            private Integer updatedById;
            @SerializedName("createdOn")
            @Expose
            private Integer createdOn;
            @SerializedName("updatedOn")
            @Expose
            private Integer updatedOn;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("cnameTime")
            @Expose
            private String cnameTime;
            @SerializedName("unameTime")
            @Expose
            private String unameTime;
            @SerializedName("createdByWithDateTime")
            @Expose
            private String createdByWithDateTime;
            @SerializedName("updatedByWithDateTime")
            @Expose
            private String updatedByWithDateTime;
            @SerializedName("reviewedByWithDateTime")
            @Expose
            private String reviewedByWithDateTime;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getIndustryId() {
                return industryId;
            }

            public void setIndustryId(Integer industryId) {
                this.industryId = industryId;
            }

            public String getActiveStatus() {
                return activeStatus;
            }

            public void setActiveStatus(String activeStatus) {
                this.activeStatus = activeStatus;
            }

            public Integer getCreatedById() {
                return createdById;
            }

            public void setCreatedById(Integer createdById) {
                this.createdById = createdById;
            }

            public Integer getUpdatedById() {
                return updatedById;
            }

            public void setUpdatedById(Integer updatedById) {
                this.updatedById = updatedById;
            }

            public Integer getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(Integer createdOn) {
                this.createdOn = createdOn;
            }

            public Integer getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(Integer updatedOn) {
                this.updatedOn = updatedOn;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCnameTime() {
                return cnameTime;
            }

            public void setCnameTime(String cnameTime) {
                this.cnameTime = cnameTime;
            }

            public String getUnameTime() {
                return unameTime;
            }

            public void setUnameTime(String unameTime) {
                this.unameTime = unameTime;
            }

            public String getCreatedByWithDateTime() {
                return createdByWithDateTime;
            }

            public void setCreatedByWithDateTime(String createdByWithDateTime) {
                this.createdByWithDateTime = createdByWithDateTime;
            }

            public String getUpdatedByWithDateTime() {
                return updatedByWithDateTime;
            }

            public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                this.updatedByWithDateTime = updatedByWithDateTime;
            }

            public String getReviewedByWithDateTime() {
                return reviewedByWithDateTime;
            }

            public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                this.reviewedByWithDateTime = reviewedByWithDateTime;
            }

        }

        public class QuestionAnswer {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("activeStatus")
            @Expose
            private String activeStatus;
            @SerializedName("answer")
            @Expose
            private String answer;
            @SerializedName("questionId")
            @Expose
            private Integer questionId;
            @SerializedName("surveyId")
            @Expose
            private Integer surveyId;
            @SerializedName("surveyEntityType")
            @Expose
            private String surveyEntityType;
            @SerializedName("cnameTime")
            @Expose
            private String cnameTime;
            @SerializedName("unameTime")
            @Expose
            private String unameTime;
            @SerializedName("createdByWithDateTime")
            @Expose
            private String createdByWithDateTime;
            @SerializedName("updatedByWithDateTime")
            @Expose
            private String updatedByWithDateTime;
            @SerializedName("reviewedByWithDateTime")
            @Expose
            private String reviewedByWithDateTime;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getActiveStatus() {
                return activeStatus;
            }

            public void setActiveStatus(String activeStatus) {
                this.activeStatus = activeStatus;
            }

            public String getAnswer() {
                return answer;
            }

            public void setAnswer(String answer) {
                this.answer = answer;
            }

            public Integer getQuestionId() {
                return questionId;
            }

            public void setQuestionId(Integer questionId) {
                this.questionId = questionId;
            }

            public Integer getSurveyId() {
                return surveyId;
            }

            public void setSurveyId(Integer surveyId) {
                this.surveyId = surveyId;
            }

            public String getSurveyEntityType() {
                return surveyEntityType;
            }

            public void setSurveyEntityType(String surveyEntityType) {
                this.surveyEntityType = surveyEntityType;
            }

            public String getCnameTime() {
                return cnameTime;
            }

            public void setCnameTime(String cnameTime) {
                this.cnameTime = cnameTime;
            }

            public String getUnameTime() {
                return unameTime;
            }

            public void setUnameTime(String unameTime) {
                this.unameTime = unameTime;
            }

            public String getCreatedByWithDateTime() {
                return createdByWithDateTime;
            }

            public void setCreatedByWithDateTime(String createdByWithDateTime) {
                this.createdByWithDateTime = createdByWithDateTime;
            }

            public String getUpdatedByWithDateTime() {
                return updatedByWithDateTime;
            }

            public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                this.updatedByWithDateTime = updatedByWithDateTime;
            }

            public String getReviewedByWithDateTime() {
                return reviewedByWithDateTime;
            }

            public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                this.reviewedByWithDateTime = reviewedByWithDateTime;
            }

        }
    }


}
