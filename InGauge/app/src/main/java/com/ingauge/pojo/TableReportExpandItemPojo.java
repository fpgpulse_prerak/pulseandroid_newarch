package com.ingauge.pojo;

/**
 * Created by mansurum on 17-Nov-17.
 */

public class TableReportExpandItemPojo {

    public String reportId;
    public String reportName="";
    public String reportIdForServer;
    public String reportNameForServer="";
    public String reportNameDisplay="";
    public String reportValue="";
    public boolean isSocialInteractionEnabled=false;
    public String entityId="";
    public String entityName="";
    public String entityType ="";
    public String dataStartDate="";
    public String dataEndDate ="";
    public String compDataStartDate ="";
    public String compDataEndDate="";
    public String startDateCheck="";
    public String endDateCheck="";
    public int likeCount=0;
    public int commentCount=0;
    public boolean isLiked=false;
    public int reportPositionMapId=0;

}
