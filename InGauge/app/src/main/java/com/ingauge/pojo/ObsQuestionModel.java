package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pathanaa on 13-07-2017.
 */

public class ObsQuestionModel {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("createdOn")
        @Expose
        private Integer createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Integer updatedOn;
        @SerializedName("isForAllTenant")
        @Expose
        private Boolean isForAllTenant;
        @SerializedName("isForAllLocation")
        @Expose
        private Boolean isForAllLocation;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("surveyCategoryId")
        @Expose
        private Integer surveyCategoryId;
        @SerializedName("surveyCategoryName")
        @Expose
        private String surveyCategoryName;
        @SerializedName("surveyCategoryStatus")
        @Expose
        private String surveyCategoryStatus;
        @SerializedName("surveyEntityType")
        @Expose
        private String surveyEntityType;
        @SerializedName("questionSections")
        @Expose
        private List<QuestionSection> questionSections = null;
        @SerializedName("createdByFirstName")
        @Expose
        private String createdByFirstName;
        @SerializedName("createdByLastName")
        @Expose
        private String createdByLastName;
        @SerializedName("updatedByLastName")
        @Expose
        private String updatedByLastName;
        @SerializedName("updatedByFirstName")
        @Expose
        private String updatedByFirstName;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public Integer getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Integer createdOn) {
            this.createdOn = createdOn;
        }

        public Integer getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIsForAllTenant() {
            return isForAllTenant;
        }

        public void setIsForAllTenant(Boolean isForAllTenant) {
            this.isForAllTenant = isForAllTenant;
        }

        public Boolean getIsForAllLocation() {
            return isForAllLocation;
        }

        public void setIsForAllLocation(Boolean isForAllLocation) {
            this.isForAllLocation = isForAllLocation;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getSurveyCategoryId() {
            return surveyCategoryId;
        }

        public void setSurveyCategoryId(Integer surveyCategoryId) {
            this.surveyCategoryId = surveyCategoryId;
        }

        public String getSurveyCategoryName() {
            return surveyCategoryName;
        }

        public void setSurveyCategoryName(String surveyCategoryName) {
            this.surveyCategoryName = surveyCategoryName;
        }

        public String getSurveyCategoryStatus() {
            return surveyCategoryStatus;
        }

        public void setSurveyCategoryStatus(String surveyCategoryStatus) {
            this.surveyCategoryStatus = surveyCategoryStatus;
        }

        public String getSurveyEntityType() {
            return surveyEntityType;
        }

        public void setSurveyEntityType(String surveyEntityType) {
            this.surveyEntityType = surveyEntityType;
        }

        public List<QuestionSection> getQuestionSections() {
            return questionSections;
        }

        public void setQuestionSections(List<QuestionSection> questionSections) {
            this.questionSections = questionSections;
        }

        public String getCreatedByFirstName() {
            return createdByFirstName;
        }

        public void setCreatedByFirstName(String createdByFirstName) {
            this.createdByFirstName = createdByFirstName;
        }

        public String getCreatedByLastName() {
            return createdByLastName;
        }

        public void setCreatedByLastName(String createdByLastName) {
            this.createdByLastName = createdByLastName;
        }

        public String getUpdatedByLastName() {
            return updatedByLastName;
        }

        public void setUpdatedByLastName(String updatedByLastName) {
            this.updatedByLastName = updatedByLastName;
        }

        public String getUpdatedByFirstName() {
            return updatedByFirstName;
        }

        public void setUpdatedByFirstName(String updatedByFirstName) {
            this.updatedByFirstName = updatedByFirstName;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }


        public class QuestionSection {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("industryId")
            @Expose
            private Integer industryId;
            @SerializedName("activeStatus")
            @Expose
            private String activeStatus;
            @SerializedName("degreeOfImportance")
            @Expose
            private Integer degreeOfImportance;
            @SerializedName("isDeleted")
            @Expose
            private Boolean isDeleted;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("questions")
            @Expose
            private List<Question> questions = null;
            @SerializedName("rank")
            @Expose
            private Integer rank;
            @SerializedName("strengthScore")
            @Expose
            private Integer strengthScore;
            @SerializedName("strengthText")
            @Expose
            private String strengthText;
            @SerializedName("surveyEntityType")
            @Expose
            private String surveyEntityType;
            @SerializedName("unameTime")
            @Expose
            private String unameTime;
            @SerializedName("cnameTime")
            @Expose
            private String cnameTime;
            @SerializedName("reviewedByWithDateTime")
            @Expose
            private String reviewedByWithDateTime;
            @SerializedName("createdByWithDateTime")
            @Expose
            private String createdByWithDateTime;
            @SerializedName("updatedByWithDateTime")
            @Expose
            private String updatedByWithDateTime;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getIndustryId() {
                return industryId;
            }

            public void setIndustryId(Integer industryId) {
                this.industryId = industryId;
            }

            public String getActiveStatus() {
                return activeStatus;
            }

            public void setActiveStatus(String activeStatus) {
                this.activeStatus = activeStatus;
            }

            public Integer getDegreeOfImportance() {
                return degreeOfImportance;
            }

            public void setDegreeOfImportance(Integer degreeOfImportance) {
                this.degreeOfImportance = degreeOfImportance;
            }

            public Boolean getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(Boolean isDeleted) {
                this.isDeleted = isDeleted;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<Question> getQuestions() {
                return questions;
            }

            public void setQuestions(List<Question> questions) {
                this.questions = questions;
            }

            public Integer getRank() {
                return rank;
            }

            public void setRank(Integer rank) {
                this.rank = rank;
            }

            public Integer getStrengthScore() {
                return strengthScore;
            }

            public void setStrengthScore(Integer strengthScore) {
                this.strengthScore = strengthScore;
            }

            public String getStrengthText() {
                return strengthText;
            }

            public void setStrengthText(String strengthText) {
                this.strengthText = strengthText;
            }

            public String getSurveyEntityType() {
                return surveyEntityType;
            }

            public void setSurveyEntityType(String surveyEntityType) {
                this.surveyEntityType = surveyEntityType;
            }

            public String getUnameTime() {
                return unameTime;
            }

            public void setUnameTime(String unameTime) {
                this.unameTime = unameTime;
            }

            public String getCnameTime() {
                return cnameTime;
            }

            public void setCnameTime(String cnameTime) {
                this.cnameTime = cnameTime;
            }

            public String getReviewedByWithDateTime() {
                return reviewedByWithDateTime;
            }

            public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                this.reviewedByWithDateTime = reviewedByWithDateTime;
            }

            public String getCreatedByWithDateTime() {
                return createdByWithDateTime;
            }

            public void setCreatedByWithDateTime(String createdByWithDateTime) {
                this.createdByWithDateTime = createdByWithDateTime;
            }

            public String getUpdatedByWithDateTime() {
                return updatedByWithDateTime;
            }

            public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                this.updatedByWithDateTime = updatedByWithDateTime;
            }
        }


        public class Question {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("industryId")
            @Expose
            private Integer industryId;
            @SerializedName("activeStatus")
            @Expose
            private String activeStatus;
            @SerializedName("isDeleted")
            @Expose
            private Boolean isDeleted;
            @SerializedName("questionText")
            @Expose
            private String questionText;
            @SerializedName("surveyEntityType")
            @Expose
            private String surveyEntityType;
            @SerializedName("symptom")
            @Expose
            private String symptom;
            @SerializedName("questionType")
            @Expose
            private String questionType;
            @SerializedName("weight")
            @Expose
            private Integer weight;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("required")
            @Expose
            private Boolean required;
            @SerializedName("unameTime")
            @Expose
            private String unameTime;
            @SerializedName("cnameTime")
            @Expose
            private String cnameTime;
            @SerializedName("reviewedByWithDateTime")
            @Expose
            private String reviewedByWithDateTime;
            @SerializedName("createdByWithDateTime")
            @Expose
            private String createdByWithDateTime;
            @SerializedName("updatedByWithDateTime")
            @Expose
            private String updatedByWithDateTime;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getIndustryId() {
                return industryId;
            }

            public void setIndustryId(Integer industryId) {
                this.industryId = industryId;
            }

            public String getActiveStatus() {
                return activeStatus;
            }

            public void setActiveStatus(String activeStatus) {
                this.activeStatus = activeStatus;
            }

            public Boolean getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(Boolean isDeleted) {
                this.isDeleted = isDeleted;
            }

            public String getQuestionText() {
                return questionText;
            }

            public void setQuestionText(String questionText) {
                this.questionText = questionText;
            }

            public String getSurveyEntityType() {
                return surveyEntityType;
            }

            public void setSurveyEntityType(String surveyEntityType) {
                this.surveyEntityType = surveyEntityType;
            }

            public String getSymptom() {
                return symptom;
            }

            public void setSymptom(String symptom) {
                this.symptom = symptom;
            }

            public String getQuestionType() {
                return questionType;
            }

            public void setQuestionType(String questionType) {
                this.questionType = questionType;
            }

            public Integer getWeight() {
                return weight;
            }

            public void setWeight(Integer weight) {
                this.weight = weight;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public Boolean getRequired() {
                return required;
            }

            public void setRequired(Boolean required) {
                this.required = required;
            }

            public String getUnameTime() {
                return unameTime;
            }

            public void setUnameTime(String unameTime) {
                this.unameTime = unameTime;
            }

            public String getCnameTime() {
                return cnameTime;
            }

            public void setCnameTime(String cnameTime) {
                this.cnameTime = cnameTime;
            }

            public String getReviewedByWithDateTime() {
                return reviewedByWithDateTime;
            }

            public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
                this.reviewedByWithDateTime = reviewedByWithDateTime;
            }

            public String getCreatedByWithDateTime() {
                return createdByWithDateTime;
            }

            public void setCreatedByWithDateTime(String createdByWithDateTime) {
                this.createdByWithDateTime = createdByWithDateTime;
            }

            public String getUpdatedByWithDateTime() {
                return updatedByWithDateTime;
            }

            public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
                this.updatedByWithDateTime = updatedByWithDateTime;
            }

        }


    }


}


