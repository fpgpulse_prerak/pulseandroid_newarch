package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mansurum on 11-May-17.
 */

public class UserData {
    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("tokenExpiry")
        @Expose
        private Long tokenExpiry;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("signedInTime")
        @Expose
        private String signedInTime;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("linkedinId")
        @Expose
        private String linkedinId;
        @SerializedName("linkedinImage")
        @Expose
        private String linkedinImage;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Long getTokenExpiry() {
            return tokenExpiry;
        }

        public void setTokenExpiry(Long tokenExpiry) {
            this.tokenExpiry = tokenExpiry;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getSignedInTime() {
            return signedInTime;
        }

        public void setSignedInTime(String signedInTime) {
            this.signedInTime = signedInTime;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public String getLinkedinId() {
            return linkedinId;
        }

        public void setLinkedinId(String linkedinId) {
            this.linkedinId = linkedinId;
        }

        public String getLinkedinImage() {
            return linkedinImage;
        }

        public void setLinkedinImage(String linkedinImage) {
            this.linkedinImage = linkedinImage;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }


    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


}
