package com.ingauge.pojo;

/**
 * Created by mansurum on 24-Nov-17.
 */

public class SocialInteractionDetailReportData {

    public String regId="";
    public LikeCommentsValues mLikeCommentsValues;
    public SocialInteractionDetailReportData(){
        mLikeCommentsValues = new LikeCommentsValues();
    }

    public class LikeCommentsValues{
        public String totalLikes="";
        public String totalComments="";
        public boolean isLikeUsers=false;
        public boolean isCommentuser=false;

    }
}
