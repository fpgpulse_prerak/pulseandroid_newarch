package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mansurum on 24-May-17.
 */

public class AccessibleTenantLocationDataModel implements Serializable{

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("createdOn")
        @Expose
        private Double createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Double updatedOn;
        @SerializedName("jsonData")
        @Expose
        private String jsonData;
        @SerializedName("jsonMap")
        @Expose
        private JsonMap jsonMap;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("cityLead")
        @Expose
        private String cityLead;
        @SerializedName("dataDeliveryMethod")
        @Expose
        private String dataDeliveryMethod;
        @SerializedName("dataStatus")
        @Expose
        private String dataStatus;
        @SerializedName("dc1")
        @Expose
        private String dc1;
        @SerializedName("dc1Email")
        @Expose
        private String dc1Email;
        @SerializedName("dc1Phone")
        @Expose
        private String dc1Phone;
        @SerializedName("emailGm")
        @Expose
        private String emailGm;
        @SerializedName("force2fa")
        @Expose
        private Boolean force2fa;
        @SerializedName("frontOfficeMgr")
        @Expose
        private String frontOfficeMgr;
        @SerializedName("frontOfficeMgrEmail")
        @Expose
        private String frontOfficeMgrEmail;
        @SerializedName("generalManager")
        @Expose
        private String generalManager;
        @SerializedName("geoAddress")
        @Expose
        private String geoAddress;
        @SerializedName("geoId")
        @Expose
        private Integer geoId;
        @SerializedName("geoLocality")
        @Expose
        private String geoLocality;
        @SerializedName("geoPostalcode")
        @Expose
        private String geoPostalcode;
        @SerializedName("geoState")
        @Expose
        private String geoState;
        @SerializedName("hotelMetricsDataType")
        @Expose
        private String hotelMetricsDataType;
        @SerializedName("hotelMktTier")
        @Expose
        private String hotelMktTier;
        @SerializedName("launchDate")
        @Expose
        private String launchDate;
        @SerializedName("legalName")
        @Expose
        private String legalName;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("noOfWorkingDaysPerMonth")
        @Expose
        private Integer noOfWorkingDaysPerMonth;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("pl")
        @Expose
        private String pl;
        @SerializedName("productMetricsValidationType")
        @Expose
        private String productMetricsValidationType;
        @SerializedName("quarterback")
        @Expose
        private String quarterback;
        @SerializedName("regionId")
        @Expose
        private Integer regionId;
        @SerializedName("regionName")
        @Expose
        private String regionName;
        @SerializedName("regionCurrency")
        @Expose
        private String regionCurrency;
        @SerializedName("rooms")
        @Expose
        private Integer rooms;
        @SerializedName("tenantId")
        @Expose
        private Integer tenantId;
        @SerializedName("tenantName")
        @Expose
        private String tenantName;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("weightLastMonth")
        @Expose
        private Integer weightLastMonth;
        @SerializedName("weightLastTwoMonth")
        @Expose
        private Integer weightLastTwoMonth;
        @SerializedName("weightLastThreeMonth")
        @Expose
        private Integer weightLastThreeMonth;
        @SerializedName("weightPriorYear")
        @Expose
        private Integer weightPriorYear;
        @SerializedName("deemphasizeByTierOne")
        @Expose
        private Integer deemphasizeByTierOne;
        @SerializedName("deemphasizeByTierTwo")
        @Expose
        private Integer deemphasizeByTierTwo;
        @SerializedName("deemphasizeByTierThree")
        @Expose
        private Integer deemphasizeByTierThree;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("cityLeadEmail")
        @Expose
        private String cityLeadEmail;
        @SerializedName("dc2")
        @Expose
        private String dc2;
        @SerializedName("dc2Email")
        @Expose
        private String dc2Email;
        @SerializedName("dc2Phone")
        @Expose
        private String dc2Phone;
        @SerializedName("emailChampion")
        @Expose
        private String emailChampion;
        @SerializedName("hotelAuditorMain")
        @Expose
        private String hotelAuditorMain;
        @SerializedName("hotelAuditorMainEmail")
        @Expose
        private String hotelAuditorMainEmail;
        @SerializedName("locationID")
        @Expose
        private String locationID;
        @SerializedName("perfomanceLeaderEmail")
        @Expose
        private String perfomanceLeaderEmail;

        public Boolean getMasterLocation(){
            return isMasterLocation;
        }

        public void setMasterLocation(Boolean masterLocation){
            isMasterLocation = masterLocation;
        }

        @SerializedName("isMasterLocation")
        @Expose
        private Boolean isMasterLocation=false;



        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public Double getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Double createdOn) {
            this.createdOn = createdOn;
        }

        public Double getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Double updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getJsonData() {
            return jsonData;
        }

        public void setJsonData(String jsonData) {
            this.jsonData = jsonData;
        }

        public JsonMap getJsonMap() {
            return jsonMap;
        }

        public void setJsonMap(JsonMap jsonMap) {
            this.jsonMap = jsonMap;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getCityLead() {
            return cityLead;
        }

        public void setCityLead(String cityLead) {
            this.cityLead = cityLead;
        }

        public String getDataDeliveryMethod() {
            return dataDeliveryMethod;
        }

        public void setDataDeliveryMethod(String dataDeliveryMethod) {
            this.dataDeliveryMethod = dataDeliveryMethod;
        }

        public String getDataStatus() {
            return dataStatus;
        }

        public void setDataStatus(String dataStatus) {
            this.dataStatus = dataStatus;
        }

        public String getDc1() {
            return dc1;
        }

        public void setDc1(String dc1) {
            this.dc1 = dc1;
        }

        public String getDc1Email() {
            return dc1Email;
        }

        public void setDc1Email(String dc1Email) {
            this.dc1Email = dc1Email;
        }

        public String getDc1Phone() {
            return dc1Phone;
        }

        public void setDc1Phone(String dc1Phone) {
            this.dc1Phone = dc1Phone;
        }

        public String getEmailGm() {
            return emailGm;
        }

        public void setEmailGm(String emailGm) {
            this.emailGm = emailGm;
        }

        public Boolean getForce2fa() {
            return force2fa;
        }

        public void setForce2fa(Boolean force2fa) {
            this.force2fa = force2fa;
        }

        public String getFrontOfficeMgr() {
            return frontOfficeMgr;
        }

        public void setFrontOfficeMgr(String frontOfficeMgr) {
            this.frontOfficeMgr = frontOfficeMgr;
        }

        public String getFrontOfficeMgrEmail() {
            return frontOfficeMgrEmail;
        }

        public void setFrontOfficeMgrEmail(String frontOfficeMgrEmail) {
            this.frontOfficeMgrEmail = frontOfficeMgrEmail;
        }

        public String getGeneralManager() {
            return generalManager;
        }

        public void setGeneralManager(String generalManager) {
            this.generalManager = generalManager;
        }

        public String getGeoAddress() {
            return geoAddress;
        }

        public void setGeoAddress(String geoAddress) {
            this.geoAddress = geoAddress;
        }

        public Integer getGeoId() {
            return geoId;
        }

        public void setGeoId(Integer geoId) {
            this.geoId = geoId;
        }

        public String getGeoLocality() {
            return geoLocality;
        }

        public void setGeoLocality(String geoLocality) {
            this.geoLocality = geoLocality;
        }

        public String getGeoPostalcode() {
            return geoPostalcode;
        }

        public void setGeoPostalcode(String geoPostalcode) {
            this.geoPostalcode = geoPostalcode;
        }

        public String getGeoState() {
            return geoState;
        }

        public void setGeoState(String geoState) {
            this.geoState = geoState;
        }

        public String getHotelMetricsDataType() {
            return hotelMetricsDataType;
        }

        public void setHotelMetricsDataType(String hotelMetricsDataType) {
            this.hotelMetricsDataType = hotelMetricsDataType;
        }

        public String getHotelMktTier() {
            return hotelMktTier;
        }

        public void setHotelMktTier(String hotelMktTier) {
            this.hotelMktTier = hotelMktTier;
        }

        public String getLaunchDate() {
            return launchDate;
        }

        public void setLaunchDate(String launchDate) {
            this.launchDate = launchDate;
        }

        public String getLegalName() {
            return legalName;
        }

        public void setLegalName(String legalName) {
            this.legalName = legalName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getNoOfWorkingDaysPerMonth() {
            return noOfWorkingDaysPerMonth;
        }

        public void setNoOfWorkingDaysPerMonth(Integer noOfWorkingDaysPerMonth) {
            this.noOfWorkingDaysPerMonth = noOfWorkingDaysPerMonth;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPl() {
            return pl;
        }

        public void setPl(String pl) {
            this.pl = pl;
        }

        public String getProductMetricsValidationType() {
            return productMetricsValidationType;
        }

        public void setProductMetricsValidationType(String productMetricsValidationType) {
            this.productMetricsValidationType = productMetricsValidationType;
        }

        public String getQuarterback() {
            return quarterback;
        }

        public void setQuarterback(String quarterback) {
            this.quarterback = quarterback;
        }

        public Integer getRegionId() {
            return regionId;
        }

        public void setRegionId(Integer regionId) {
            this.regionId = regionId;
        }

        public String getRegionName() {
            return regionName;
        }

        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }

        public String getRegionCurrency() {
            return regionCurrency;
        }

        public void setRegionCurrency(String regionCurrency) {
            this.regionCurrency = regionCurrency;
        }

        public Integer getRooms() {
            return rooms;
        }

        public void setRooms(Integer rooms) {
            this.rooms = rooms;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }

        public String getTenantName() {
            return tenantName;
        }

        public void setTenantName(String tenantName) {
            this.tenantName = tenantName;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public Integer getWeightLastMonth() {
            return weightLastMonth;
        }

        public void setWeightLastMonth(Integer weightLastMonth) {
            this.weightLastMonth = weightLastMonth;
        }

        public Integer getWeightLastTwoMonth() {
            return weightLastTwoMonth;
        }

        public void setWeightLastTwoMonth(Integer weightLastTwoMonth) {
            this.weightLastTwoMonth = weightLastTwoMonth;
        }

        public Integer getWeightLastThreeMonth() {
            return weightLastThreeMonth;
        }

        public void setWeightLastThreeMonth(Integer weightLastThreeMonth) {
            this.weightLastThreeMonth = weightLastThreeMonth;
        }

        public Integer getWeightPriorYear() {
            return weightPriorYear;
        }

        public void setWeightPriorYear(Integer weightPriorYear) {
            this.weightPriorYear = weightPriorYear;
        }

        public Integer getDeemphasizeByTierOne() {
            return deemphasizeByTierOne;
        }

        public void setDeemphasizeByTierOne(Integer deemphasizeByTierOne) {
            this.deemphasizeByTierOne = deemphasizeByTierOne;
        }

        public Integer getDeemphasizeByTierTwo() {
            return deemphasizeByTierTwo;
        }

        public void setDeemphasizeByTierTwo(Integer deemphasizeByTierTwo) {
            this.deemphasizeByTierTwo = deemphasizeByTierTwo;
        }

        public Integer getDeemphasizeByTierThree() {
            return deemphasizeByTierThree;
        }

        public void setDeemphasizeByTierThree(Integer deemphasizeByTierThree) {
            this.deemphasizeByTierThree = deemphasizeByTierThree;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getCityLeadEmail() {
            return cityLeadEmail;
        }

        public void setCityLeadEmail(String cityLeadEmail) {
            this.cityLeadEmail = cityLeadEmail;
        }

        public String getDc2() {
            return dc2;
        }

        public void setDc2(String dc2) {
            this.dc2 = dc2;
        }

        public String getDc2Email() {
            return dc2Email;
        }

        public void setDc2Email(String dc2Email) {
            this.dc2Email = dc2Email;
        }

        public String getDc2Phone() {
            return dc2Phone;
        }

        public void setDc2Phone(String dc2Phone) {
            this.dc2Phone = dc2Phone;
        }

        public String getEmailChampion() {
            return emailChampion;
        }

        public void setEmailChampion(String emailChampion) {
            this.emailChampion = emailChampion;
        }

        public String getHotelAuditorMain() {
            return hotelAuditorMain;
        }

        public void setHotelAuditorMain(String hotelAuditorMain) {
            this.hotelAuditorMain = hotelAuditorMain;
        }

        public String getHotelAuditorMainEmail() {
            return hotelAuditorMainEmail;
        }

        public void setHotelAuditorMainEmail(String hotelAuditorMainEmail) {
            this.hotelAuditorMainEmail = hotelAuditorMainEmail;
        }

        public String getLocationID() {
            return locationID;
        }

        public void setLocationID(String locationID) {
            this.locationID = locationID;
        }

        public String getPerfomanceLeaderEmail() {
            return perfomanceLeaderEmail;
        }

        public void setPerfomanceLeaderEmail(String perfomanceLeaderEmail) {
            this.perfomanceLeaderEmail = perfomanceLeaderEmail;
        }

    }

    public class JsonMap {


    }
}
