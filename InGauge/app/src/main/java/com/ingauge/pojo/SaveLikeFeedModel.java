package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by desainid on 6/20/2017.
 */

public class SaveLikeFeedModel {
    @SerializedName("data")
    @Expose
    private Boolean data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

}
