package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mansurum on 23-May-17.
 */

public class RegionByTenantRegionTypeModel implements Serializable{

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Geography {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("country")
        @Expose
        private Country country;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("locality")
        @Expose
        private String locality;
        @SerializedName("postalcode")
        @Expose
        private String postalcode;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Country getCountry() {
            return country;
        }

        public void setCountry(Country country) {
            this.country = country;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getPostalcode() {
            return postalcode;
        }

        public void setPostalcode(String postalcode) {
            this.postalcode = postalcode;
        }

    }

    public class Geography_ {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("country")
        @Expose
        private Country_ country;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("locality")
        @Expose
        private String locality;
        @SerializedName("postalcode")
        @Expose
        private String postalcode;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Country_ getCountry() {
            return country;
        }

        public void setCountry(Country_ country) {
            this.country = country;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getPostalcode() {
            return postalcode;
        }

        public void setPostalcode(String postalcode) {
            this.postalcode = postalcode;
        }

    }

    public class JsonMap {

        @SerializedName("json470")
        @Expose
        private String json470;

        public String getJson470() {
            return json470;
        }

        public void setJson470(String json470) {
            this.json470 = json470;
        }

    }

    public class JsonMap_ {

        @SerializedName("json470")
        @Expose
        private String json470;

        public String getJson470() {
            return json470;
        }

        public void setJson470(String json470) {
            this.json470 = json470;
        }

    }

    public class RegionType {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("level")
        @Expose
        private Integer level;
        @SerializedName("tenant")
        @Expose
        private Tenant_ tenant;
        @SerializedName("regions")
        @Expose
        private Object regions;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getLevel() {
            return level;
        }

        public void setLevel(Integer level) {
            this.level = level;
        }

        public Tenant_ getTenant() {
            return tenant;
        }

        public void setTenant(Tenant_ tenant) {
            this.tenant = tenant;
        }

        public Object getRegions() {
            return regions;
        }

        public void setRegions(Object regions) {
            this.regions = regions;
        }

    }

    public class Tenant {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("createdBy")
        @Expose
        private CreatedBy createdBy;
        @SerializedName("updatedBy")
        @Expose
        private UpdatedBy updatedBy;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("jsonData")
        @Expose
        private String jsonData;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("geography")
        @Expose
        private Geography geography;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("hotels")
        @Expose
        private Integer hotels;
        @SerializedName("turnover")
        @Expose
        private Integer turnover;
        @SerializedName("ebitda")
        @Expose
        private Integer ebitda;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("roles")
        @Expose
        private Object roles;
        @SerializedName("regions")
        @Expose
        private Object regions;
        @SerializedName("groupTypes")
        @Expose
        private Object groupTypes;
        @SerializedName("verdicts")
        @Expose
        private Object verdicts;
        @SerializedName("tenantReports")
        @Expose
        private Object tenantReports;
        @SerializedName("tenantLocations")
        @Expose
        private Object tenantLocations;
        @SerializedName("jsonMap")
        @Expose
        private JsonMap jsonMap;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public CreatedBy getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(CreatedBy createdBy) {
            this.createdBy = createdBy;
        }

        public UpdatedBy getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(UpdatedBy updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getJsonData() {
            return jsonData;
        }

        public void setJsonData(String jsonData) {
            this.jsonData = jsonData;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public Geography getGeography() {
            return geography;
        }

        public void setGeography(Geography geography) {
            this.geography = geography;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getHotels() {
            return hotels;
        }

        public void setHotels(Integer hotels) {
            this.hotels = hotels;
        }

        public Integer getTurnover() {
            return turnover;
        }

        public void setTurnover(Integer turnover) {
            this.turnover = turnover;
        }

        public Integer getEbitda() {
            return ebitda;
        }

        public void setEbitda(Integer ebitda) {
            this.ebitda = ebitda;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Object getRoles() {
            return roles;
        }

        public void setRoles(Object roles) {
            this.roles = roles;
        }

        public Object getRegions() {
            return regions;
        }

        public void setRegions(Object regions) {
            this.regions = regions;
        }

        public Object getGroupTypes() {
            return groupTypes;
        }

        public void setGroupTypes(Object groupTypes) {
            this.groupTypes = groupTypes;
        }

        public Object getVerdicts() {
            return verdicts;
        }

        public void setVerdicts(Object verdicts) {
            this.verdicts = verdicts;
        }

        public Object getTenantReports() {
            return tenantReports;
        }

        public void setTenantReports(Object tenantReports) {
            this.tenantReports = tenantReports;
        }

        public Object getTenantLocations() {
            return tenantLocations;
        }

        public void setTenantLocations(Object tenantLocations) {
            this.tenantLocations = tenantLocations;
        }

        public JsonMap getJsonMap() {
            return jsonMap;
        }

        public void setJsonMap(JsonMap jsonMap) {
            this.jsonMap = jsonMap;
        }

    }

    public class Tenant_ {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("createdBy")
        @Expose
        private CreatedBy_ createdBy;
        @SerializedName("updatedBy")
        @Expose
        private UpdatedBy_ updatedBy;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("jsonData")
        @Expose
        private String jsonData;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("geography")
        @Expose
        private Geography_ geography;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("hotels")
        @Expose
        private Integer hotels;
        @SerializedName("turnover")
        @Expose
        private Integer turnover;
        @SerializedName("ebitda")
        @Expose
        private Integer ebitda;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("roles")
        @Expose
        private Object roles;
        @SerializedName("regions")
        @Expose
        private Object regions;
        @SerializedName("groupTypes")
        @Expose
        private Object groupTypes;
        @SerializedName("verdicts")
        @Expose
        private Object verdicts;
        @SerializedName("tenantReports")
        @Expose
        private Object tenantReports;
        @SerializedName("tenantLocations")
        @Expose
        private Object tenantLocations;
        @SerializedName("jsonMap")
        @Expose
        private JsonMap_ jsonMap;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public CreatedBy_ getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(CreatedBy_ createdBy) {
            this.createdBy = createdBy;
        }

        public UpdatedBy_ getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(UpdatedBy_ updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getJsonData() {
            return jsonData;
        }

        public void setJsonData(String jsonData) {
            this.jsonData = jsonData;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public Geography_ getGeography() {
            return geography;
        }

        public void setGeography(Geography_ geography) {
            this.geography = geography;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getHotels() {
            return hotels;
        }

        public void setHotels(Integer hotels) {
            this.hotels = hotels;
        }

        public Integer getTurnover() {
            return turnover;
        }

        public void setTurnover(Integer turnover) {
            this.turnover = turnover;
        }

        public Integer getEbitda() {
            return ebitda;
        }

        public void setEbitda(Integer ebitda) {
            this.ebitda = ebitda;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Object getRoles() {
            return roles;
        }

        public void setRoles(Object roles) {
            this.roles = roles;
        }

        public Object getRegions() {
            return regions;
        }

        public void setRegions(Object regions) {
            this.regions = regions;
        }

        public Object getGroupTypes() {
            return groupTypes;
        }

        public void setGroupTypes(Object groupTypes) {
            this.groupTypes = groupTypes;
        }

        public Object getVerdicts() {
            return verdicts;
        }

        public void setVerdicts(Object verdicts) {
            this.verdicts = verdicts;
        }

        public Object getTenantReports() {
            return tenantReports;
        }

        public void setTenantReports(Object tenantReports) {
            this.tenantReports = tenantReports;
        }

        public Object getTenantLocations() {
            return tenantLocations;
        }

        public void setTenantLocations(Object tenantLocations) {
            this.tenantLocations = tenantLocations;
        }

        public JsonMap_ getJsonMap() {
            return jsonMap;
        }

        public void setJsonMap(JsonMap_ jsonMap) {
            this.jsonMap = jsonMap;
        }

    }

    public class UpdatedBy {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("tokenExpiry")
        @Expose
        private Integer tokenExpiry;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("signedInTime")
        @Expose
        private String signedInTime;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("birthMonth")
        @Expose
        private Integer birthMonth;
        @SerializedName("birthDay")
        @Expose
        private Integer birthDay;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Integer getTokenExpiry() {
            return tokenExpiry;
        }

        public void setTokenExpiry(Integer tokenExpiry) {
            this.tokenExpiry = tokenExpiry;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getSignedInTime() {
            return signedInTime;
        }

        public void setSignedInTime(String signedInTime) {
            this.signedInTime = signedInTime;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public Integer getBirthMonth() {
            return birthMonth;
        }

        public void setBirthMonth(Integer birthMonth) {
            this.birthMonth = birthMonth;
        }

        public Integer getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(Integer birthDay) {
            this.birthDay = birthDay;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }



    public class UpdatedBy_ {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("tokenExpiry")
        @Expose
        private Integer tokenExpiry;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("signedInTime")
        @Expose
        private String signedInTime;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("birthMonth")
        @Expose
        private Integer birthMonth;
        @SerializedName("birthDay")
        @Expose
        private Integer birthDay;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Integer getTokenExpiry() {
            return tokenExpiry;
        }

        public void setTokenExpiry(Integer tokenExpiry) {
            this.tokenExpiry = tokenExpiry;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getSignedInTime() {
            return signedInTime;
        }

        public void setSignedInTime(String signedInTime) {
            this.signedInTime = signedInTime;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public Integer getBirthMonth() {
            return birthMonth;
        }

        public void setBirthMonth(Integer birthMonth) {
            this.birthMonth = birthMonth;
        }

        public Integer getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(Integer birthDay) {
            this.birthDay = birthDay;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("createdBy")
        @Expose
        private Object createdBy;
        @SerializedName("updatedBy")
        @Expose
        private Object updatedBy;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("tenant")
        @Expose
        private Tenant tenant;
        @SerializedName("regionType")
        @Expose
        private RegionType regionType;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("oldId")
        @Expose
        private Integer oldId;
        @SerializedName("regionHierarchy")
        @Expose
        private String regionHierarchy;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("country")
        @Expose
        private Object country;
        @SerializedName("dataUtilityEnable")
        @Expose
        private String dataUtilityEnable;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public Object getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Object createdBy) {
            this.createdBy = createdBy;
        }

        public Object getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Object updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public Tenant getTenant() {
            return tenant;
        }

        public void setTenant(Tenant tenant) {
            this.tenant = tenant;
        }

        public RegionType getRegionType() {
            return regionType;
        }

        public void setRegionType(RegionType regionType) {
            this.regionType = regionType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getOldId() {
            return oldId;
        }

        public void setOldId(Integer oldId) {
            this.oldId = oldId;
        }

        public String getRegionHierarchy() {
            return regionHierarchy;
        }

        public void setRegionHierarchy(String regionHierarchy) {
            this.regionHierarchy = regionHierarchy;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Object getCountry() {
            return country;
        }

        public void setCountry(Object country) {
            this.country = country;
        }

        public String getDataUtilityEnable() {
            return dataUtilityEnable;
        }

        public void setDataUtilityEnable(String dataUtilityEnable) {
            this.dataUtilityEnable = dataUtilityEnable;
        }

    }

    public class CreatedBy_ {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("tokenExpiry")
        @Expose
        private Integer tokenExpiry;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("signedInTime")
        @Expose
        private String signedInTime;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("birthMonth")
        @Expose
        private Integer birthMonth;
        @SerializedName("birthDay")
        @Expose
        private Integer birthDay;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isFpgMember")
        @Expose
        private Boolean isFpgMember;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("countryCode")
        @Expose
        private String countryCode;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Integer getTokenExpiry() {
            return tokenExpiry;
        }

        public void setTokenExpiry(Integer tokenExpiry) {
            this.tokenExpiry = tokenExpiry;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getSignedInTime() {
            return signedInTime;
        }

        public void setSignedInTime(String signedInTime) {
            this.signedInTime = signedInTime;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public Integer getBirthMonth() {
            return birthMonth;
        }

        public void setBirthMonth(Integer birthMonth) {
            this.birthMonth = birthMonth;
        }

        public Integer getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(Integer birthDay) {
            this.birthDay = birthDay;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsFpgMember() {
            return isFpgMember;
        }

        public void setIsFpgMember(Boolean isFpgMember) {
            this.isFpgMember = isFpgMember;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }


    public class CreatedBy {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("tokenExpiry")
        @Expose
        private Integer tokenExpiry;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("signedInTime")
        @Expose
        private String signedInTime;
        @SerializedName("defaultIndustry")
        @Expose
        private Integer defaultIndustry;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("middleName")
        @Expose
        private String middleName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("hiredDate")
        @Expose
        private String hiredDate;
        @SerializedName("fpgLevel")
        @Expose
        private String fpgLevel;
        @SerializedName("jobTitle")
        @Expose
        private String jobTitle;
        @SerializedName("birthMonth")
        @Expose
        private Integer birthMonth;
        @SerializedName("birthDay")
        @Expose
        private Integer birthDay;
        @SerializedName("pulseId")
        @Expose
        private String pulseId;
        @SerializedName("isFpgMember")
        @Expose
        private Boolean isFpgMember;
        @SerializedName("isAuthyAttempted")
        @Expose
        private Boolean isAuthyAttempted;
        @SerializedName("countryCode")
        @Expose
        private String countryCode;
        @SerializedName("workAge")
        @Expose
        private String workAge;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Integer getTokenExpiry() {
            return tokenExpiry;
        }

        public void setTokenExpiry(Integer tokenExpiry) {
            this.tokenExpiry = tokenExpiry;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getSignedInTime() {
            return signedInTime;
        }

        public void setSignedInTime(String signedInTime) {
            this.signedInTime = signedInTime;
        }

        public Integer getDefaultIndustry() {
            return defaultIndustry;
        }

        public void setDefaultIndustry(Integer defaultIndustry) {
            this.defaultIndustry = defaultIndustry;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getHiredDate() {
            return hiredDate;
        }

        public void setHiredDate(String hiredDate) {
            this.hiredDate = hiredDate;
        }

        public String getFpgLevel() {
            return fpgLevel;
        }

        public void setFpgLevel(String fpgLevel) {
            this.fpgLevel = fpgLevel;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public Integer getBirthMonth() {
            return birthMonth;
        }

        public void setBirthMonth(Integer birthMonth) {
            this.birthMonth = birthMonth;
        }

        public Integer getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(Integer birthDay) {
            this.birthDay = birthDay;
        }

        public String getPulseId() {
            return pulseId;
        }

        public void setPulseId(String pulseId) {
            this.pulseId = pulseId;
        }

        public Boolean getIsFpgMember() {
            return isFpgMember;
        }

        public void setIsFpgMember(Boolean isFpgMember) {
            this.isFpgMember = isFpgMember;
        }

        public Boolean getIsAuthyAttempted() {
            return isAuthyAttempted;
        }

        public void setIsAuthyAttempted(Boolean isAuthyAttempted) {
            this.isAuthyAttempted = isAuthyAttempted;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getWorkAge() {
            return workAge;
        }

        public void setWorkAge(String workAge) {
            this.workAge = workAge;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Country_ {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("countryCode")
        @Expose
        private String countryCode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("isoCode")
        @Expose
        private String isoCode;
        @SerializedName("symbol")
        @Expose
        private String symbol;
        @SerializedName("hexSymbol")
        @Expose
        private String hexSymbol;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getIsoCode() {
            return isoCode;
        }

        public void setIsoCode(String isoCode) {
            this.isoCode = isoCode;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getHexSymbol() {
            return hexSymbol;
        }

        public void setHexSymbol(String hexSymbol) {
            this.hexSymbol = hexSymbol;
        }

    }

    public class Country {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("indexRelatedOnly")
        @Expose
        private Boolean indexRelatedOnly;
        @SerializedName("indexMainOnly")
        @Expose
        private Boolean indexMainOnly;
        @SerializedName("countryCode")
        @Expose
        private String countryCode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("isoCode")
        @Expose
        private String isoCode;
        @SerializedName("symbol")
        @Expose
        private String symbol;
        @SerializedName("hexSymbol")
        @Expose
        private String hexSymbol;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Boolean getIndexRelatedOnly() {
            return indexRelatedOnly;
        }

        public void setIndexRelatedOnly(Boolean indexRelatedOnly) {
            this.indexRelatedOnly = indexRelatedOnly;
        }

        public Boolean getIndexMainOnly() {
            return indexMainOnly;
        }

        public void setIndexMainOnly(Boolean indexMainOnly) {
            this.indexMainOnly = indexMainOnly;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getIsoCode() {
            return isoCode;
        }

        public void setIsoCode(String isoCode) {
            this.isoCode = isoCode;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getHexSymbol() {
            return hexSymbol;
        }

        public void setHexSymbol(String hexSymbol) {
            this.hexSymbol = hexSymbol;
        }

    }
}
