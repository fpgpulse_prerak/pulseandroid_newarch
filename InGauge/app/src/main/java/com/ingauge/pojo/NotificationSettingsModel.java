package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ingauge.InGaugeApp;
import com.ingauge.R;
import com.ingauge.session.InGaugeSession;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mansurum on 05-Apr-18.
 */

public class NotificationSettingsModel implements Serializable{


    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData(){
        return data;
    }

    public void setData(List<Datum> data){
        this.data = data;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public Integer getStatusCode(){
        return statusCode;
    }

    public void setStatusCode(Integer statusCode){
        this.statusCode = statusCode;
    }

    public class Datum{

        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("notificationType")
        @Expose
        private String notificationType;
        @SerializedName("isExternalMail")
        @Expose
        private Boolean isExternalMail;
        @SerializedName("isInternalMail")
        @Expose
        private Boolean isInternalMail;
        @SerializedName("isPushNotification")
        @Expose
        private Boolean isPushNotification;
        @SerializedName("tooltip")
        @Expose
        private String tooltip;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("toolTipKey")
        @Expose
        private String toolTipKey;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;

        @SerializedName("industryId")
        @Expose
        private Integer industryId = InGaugeSession.read(InGaugeApp.getInstance().getString(R.string.key_selected_industry_id), 0);

        @SerializedName("mapTo")
        @Expose
        private String mapTo = "USERNOTIFICATIONCONFIGURATION";


        public boolean isGroupOpended(){
            return isGroupOpended;
        }

        public void setGroupOpended(boolean groupOpended){
            isGroupOpended = groupOpended;
        }

        private boolean isGroupOpended = false;

        public String getLabel(){
            return label;
        }

        public void setLabel(String label){
            this.label = label;
        }

        public String getNotificationType(){
            return notificationType;
        }

        public void setNotificationType(String notificationType){
            this.notificationType = notificationType;
        }

        public Boolean getIsExternalMail(){
            return isExternalMail;
        }

        public void setIsExternalMail(Boolean isExternalMail){
            this.isExternalMail = isExternalMail;
        }

        public Boolean getIsInternalMail(){
            return isInternalMail;
        }

        public void setIsInternalMail(Boolean isInternalMail){
            this.isInternalMail = isInternalMail;
        }

        public Boolean getIsPushNotification(){
            return isPushNotification;
        }

        public void setIsPushNotification(Boolean isPushNotification){
            this.isPushNotification = isPushNotification;
        }

        public String getTooltip(){
            return tooltip;
        }

        public void setTooltip(String tooltip){
            this.tooltip = tooltip;
        }

        public Integer getUserId(){
            return userId;
        }

        public void setUserId(Integer userId){
            this.userId = userId;
        }

        public String getToolTipKey(){
            return toolTipKey;
        }

        public void setToolTipKey(String toolTipKey){
            this.toolTipKey = toolTipKey;
        }

        public String getCnameTime(){
            return cnameTime;
        }

        public void setCnameTime(String cnameTime){
            this.cnameTime = cnameTime;
        }

        public String getUnameTime(){
            return unameTime;
        }

        public void setUnameTime(String unameTime){
            this.unameTime = unameTime;
        }

        public String getCreatedByWithDateTime(){
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime){
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getUpdatedByWithDateTime(){
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime){
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime(){
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime){
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

        public Integer getIndustryId(){
            return industryId;
        }

        public void setIndustryId(Integer industryId){
            this.industryId = industryId;
        }

        public String getMapTo(){
            return mapTo;
        }

        public void setMapTo(String mapTo){
            this.mapTo = mapTo;
        }

    }
}
