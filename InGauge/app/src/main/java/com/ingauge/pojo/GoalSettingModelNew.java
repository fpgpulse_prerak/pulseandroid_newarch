package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by desainid on 6/26/2017.
 */

public class GoalSettingModelNew {

    @SerializedName("data")
    @Expose
    public Data data;
    public class Data
    {
        private static final long serialVersionUID = 1L;
        private SelfGoalProgressModel SelfGoalProgress;
        private SelfGoalProgressModel FpgGoalProgress;
        private GoalWeeklyPrediction weeklyGoalPrediction;

        public GoalCurrentMonthMap getGoalCurrentMonthMap() {
            return goalCurrentMonthMap;
        }

        public void setGoalCurrentMonthMap(GoalCurrentMonthMap goalCurrentMonthMap) {
            this.goalCurrentMonthMap = goalCurrentMonthMap;
        }

        private GoalCurrentMonthMap goalCurrentMonthMap;
        private GoalSearchForm searchGoalPredictionForm;
        public GoalWeeklyPrediction getWeeklyGoalPrediction() {
            return weeklyGoalPrediction;
        }

        public void setWeeklyGoalPrediction(GoalWeeklyPrediction weeklyGoalPrediction) {
            this.weeklyGoalPrediction = weeklyGoalPrediction;
        }

        public SelfGoalProgressModel getSelfGoalProgress() {
            return SelfGoalProgress;
        }

        public void setSelfGoalProgress(SelfGoalProgressModel selfGoalProgress) {
            SelfGoalProgress = selfGoalProgress;
        }

        public SelfGoalProgressModel getFpgGoalProgress() {
            return FpgGoalProgress;
        }

        public void setFpgGoalProgress(SelfGoalProgressModel fpgGoalProgress) {
            FpgGoalProgress = fpgGoalProgress;
        }

        public GoalSearchForm getSearchGoalPredictionForm() {
            return searchGoalPredictionForm;
        }

        public void setSearchGoalPredictionForm(GoalSearchForm searchGoalPredictionForm) {
            this.searchGoalPredictionForm = searchGoalPredictionForm;
        }


    }
}
