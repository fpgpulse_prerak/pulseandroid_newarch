package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mansurum on 24-May-17.
 */

public class DashboardLocationGroupListModel implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("locationGroupID")
        @Expose
        private Integer locationGroupID;
        @SerializedName("locationGroupName")
        @Expose
        private String locationGroupName;
        @SerializedName("userID")
        @Expose
        private Integer userID;
        @SerializedName("isContributor")
        @Expose
        private Boolean isContributor;
        @SerializedName("isOperator")
        @Expose
        private Boolean isOperator;
        @SerializedName("locationGroupHierarchy")
        @Expose
        private String locationGroupHierarchy;
        @SerializedName("isAccessibleToUser")
        @Expose
        private Boolean isAccessibleToUser;
        @SerializedName("defaultHiddenGroup")
        @Expose
        private Boolean defaultHiddenGroup;
        @SerializedName("isRetail")
        @Expose
        private Boolean isRetail;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getLocationGroupID() {
            return locationGroupID;
        }

        public void setLocationGroupID(Integer locationGroupID) {
            this.locationGroupID = locationGroupID;
        }

        public String getLocationGroupName() {
            return locationGroupName;
        }

        public void setLocationGroupName(String locationGroupName) {
            this.locationGroupName = locationGroupName;
        }

        public Integer getUserID() {
            return userID;
        }

        public void setUserID(Integer userID) {
            this.userID = userID;
        }

        public Boolean getIsContributor() {
            return isContributor;
        }

        public void setIsContributor(Boolean isContributor) {
            this.isContributor = isContributor;
        }

        public Boolean getIsOperator() {
            return isOperator;
        }

        public void setIsOperator(Boolean isOperator) {
            this.isOperator = isOperator;
        }

        public String getLocationGroupHierarchy() {
            return locationGroupHierarchy;
        }

        public void setLocationGroupHierarchy(String locationGroupHierarchy) {
            this.locationGroupHierarchy = locationGroupHierarchy;
        }

        public Boolean getIsAccessibleToUser() {
            return isAccessibleToUser;
        }

        public void setIsAccessibleToUser(Boolean isAccessibleToUser) {
            this.isAccessibleToUser = isAccessibleToUser;
        }

        public Boolean getDefaultHiddenGroup() {
            return defaultHiddenGroup;
        }

        public void setDefaultHiddenGroup(Boolean defaultHiddenGroup) {
            this.defaultHiddenGroup = defaultHiddenGroup;
        }

        public Boolean getIsRetail() {
            return isRetail;
        }

        public void setIsRetail(Boolean isRetail) {
            this.isRetail = isRetail;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

    }
}
