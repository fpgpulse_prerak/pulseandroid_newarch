package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mansurum on 25-May-17.
 */

public class UserRoleTenantListModel {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("userName")
        @Expose
        private String userName;
        @SerializedName("tenantId")
        @Expose
        private Integer tenantId;
        @SerializedName("tenantName")
        @Expose
        private String tenantName;
        @SerializedName("roleId")
        @Expose
        private Integer roleId;
        @SerializedName("roleName")
        @Expose
        private String roleName;
        @SerializedName("roleWeight")
        @Expose
        private Integer roleWeight;
        @SerializedName("roleRolePermissions")
        @Expose
        private List<RoleRolePermission> roleRolePermissions = null;
        @SerializedName("industryName")
        @Expose
        private String industryName;
        @SerializedName("industryDescription")
        @Expose
        private String industryDescription;
        @SerializedName("roleReports")
        @Expose
        private List<Object> roleReports = null;
        @SerializedName("roleMetricAuditStatus")
        @Expose
        private List<Object> roleMetricAuditStatus = null;
        @SerializedName("roleIndustryMenus")
        @Expose
        private List<Object> roleIndustryMenus = null;
        @SerializedName("roleIndustryRoutes")
        @Expose
        private List<Object> roleIndustryRoutes = null;
        @SerializedName("solrTimeZone")
        @Expose
        private String solrTimeZone;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }

        public String getTenantName() {
            return tenantName;
        }

        public void setTenantName(String tenantName) {
            this.tenantName = tenantName;
        }

        public Integer getRoleId() {
            return roleId;
        }

        public void setRoleId(Integer roleId) {
            this.roleId = roleId;
        }

        public String getRoleName() {
            return roleName;
        }

        public void setRoleName(String roleName) {
            this.roleName = roleName;
        }

        public Integer getRoleWeight() {
            return roleWeight;
        }

        public void setRoleWeight(Integer roleWeight) {
            this.roleWeight = roleWeight;
        }

        public List<RoleRolePermission> getRoleRolePermissions() {
            return roleRolePermissions;
        }

        public void setRoleRolePermissions(List<RoleRolePermission> roleRolePermissions) {
            this.roleRolePermissions = roleRolePermissions;
        }

        public String getIndustryName() {
            return industryName;
        }

        public void setIndustryName(String industryName) {
            this.industryName = industryName;
        }

        public String getIndustryDescription() {
            return industryDescription;
        }

        public void setIndustryDescription(String industryDescription) {
            this.industryDescription = industryDescription;
        }

        public List<Object> getRoleReports() {
            return roleReports;
        }

        public void setRoleReports(List<Object> roleReports) {
            this.roleReports = roleReports;
        }

        public List<Object> getRoleMetricAuditStatus() {
            return roleMetricAuditStatus;
        }

        public void setRoleMetricAuditStatus(List<Object> roleMetricAuditStatus) {
            this.roleMetricAuditStatus = roleMetricAuditStatus;
        }

        public List<Object> getRoleIndustryMenus() {
            return roleIndustryMenus;
        }

        public void setRoleIndustryMenus(List<Object> roleIndustryMenus) {
            this.roleIndustryMenus = roleIndustryMenus;
        }

        public List<Object> getRoleIndustryRoutes() {
            return roleIndustryRoutes;
        }

        public void setRoleIndustryRoutes(List<Object> roleIndustryRoutes) {
            this.roleIndustryRoutes = roleIndustryRoutes;
        }

        public String getSolrTimeZone() {
            return solrTimeZone;
        }

        public void setSolrTimeZone(String solrTimeZone) {
            this.solrTimeZone = solrTimeZone;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

    }

    public class RoleRolePermission {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("displayName")
        @Expose
        private String displayName;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("permissionType")
        @Expose
        private String permissionType;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPermissionType() {
            return permissionType;
        }

        public void setPermissionType(String permissionType) {
            this.permissionType = permissionType;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

    }
}
