package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mansurum on 03-Apr-18.
 */

public class AccessibleTenantLocationDataModelForAgentProfile implements Serializable{

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public class Data {

        @SerializedName("allLocation")
        @Expose
        private List<AllLocation> allLocation = null;

        public List<AllLocation> getAllLocation() {
            return allLocation;
        }

        public void setAllLocation(List<AllLocation> allLocation) {
            this.allLocation = allLocation;
        }

    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class AllLocation {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("industryId")
        @Expose
        private Integer industryId;
        @SerializedName("activeStatus")
        @Expose
        private String activeStatus;
        @SerializedName("createdById")
        @Expose
        private Integer createdById;
        @SerializedName("updatedById")
        @Expose
        private Integer updatedById;
        @SerializedName("createdOn")
        @Expose
        private Integer createdOn;
        @SerializedName("updatedOn")
        @Expose
        private Integer updatedOn;
        @SerializedName("jsonData")
        @Expose
        private String jsonData;
        @SerializedName("jsonMap")
        @Expose
        private JsonMap jsonMap;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("cityLead")
        @Expose
        private String cityLead;
        @SerializedName("cityLeadEmail")
        @Expose
        private String cityLeadEmail;
        @SerializedName("column1")
        @Expose
        private String column1;
        @SerializedName("dataDeliveryMethod")
        @Expose
        private String dataDeliveryMethod;
        @SerializedName("dataStatus")
        @Expose
        private String dataStatus;
        @SerializedName("dataStatusNotes")
        @Expose
        private String dataStatusNotes;
        @SerializedName("dc1")
        @Expose
        private String dc1;
        @SerializedName("dc1Email")
        @Expose
        private String dc1Email;
        @SerializedName("dc1Phone")
        @Expose
        private String dc1Phone;
        @SerializedName("dc2")
        @Expose
        private String dc2;
        @SerializedName("dc2Email")
        @Expose
        private String dc2Email;
        @SerializedName("dc2Phone")
        @Expose
        private String dc2Phone;
        @SerializedName("emailChampion")
        @Expose
        private String emailChampion;
        @SerializedName("emailGm")
        @Expose
        private String emailGm;
        @SerializedName("executioveLounge")
        @Expose
        private String executioveLounge;
        @SerializedName("force2fa")
        @Expose
        private Boolean force2fa;
        @SerializedName("frontDeskPOS")
        @Expose
        private String frontDeskPOS;
        @SerializedName("frontOfficeMgr")
        @Expose
        private String frontOfficeMgr;
        @SerializedName("frontOfficeMgrEmail")
        @Expose
        private String frontOfficeMgrEmail;
        @SerializedName("generalManager")
        @Expose
        private String generalManager;
        @SerializedName("geoAddress")
        @Expose
        private String geoAddress;
        @SerializedName("geoId")
        @Expose
        private Integer geoId;
        @SerializedName("geoLocality")
        @Expose
        private String geoLocality;
        @SerializedName("geoPostalcode")
        @Expose
        private String geoPostalcode;
        @SerializedName("geoState")
        @Expose
        private String geoState;
        @SerializedName("hasSpa")
        @Expose
        private String hasSpa;
        @SerializedName("hotelAuditorMain")
        @Expose
        private String hotelAuditorMain;
        @SerializedName("hotelAuditorMainEmail")
        @Expose
        private String hotelAuditorMainEmail;
        @SerializedName("hotelAuditorSecondary")
        @Expose
        private String hotelAuditorSecondary;
        @SerializedName("hotelAuditorSecondaryEmail")
        @Expose
        private String hotelAuditorSecondaryEmail;
        @SerializedName("hotelMetricsDataType")
        @Expose
        private String hotelMetricsDataType;
        @SerializedName("hotelMktTier")
        @Expose
        private String hotelMktTier;
        @SerializedName("isMasterLocation")
        @Expose
        private Boolean isMasterLocation;
        @SerializedName("launchDate")
        @Expose
        private String launchDate;
        @SerializedName("legalName")
        @Expose
        private String legalName;
        @SerializedName("locationID")
        @Expose
        private String locationID;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("noOfWorkingDaysPerMonth")
        @Expose
        private Integer noOfWorkingDaysPerMonth;
        @SerializedName("perfomanceLeaderEmail")
        @Expose
        private String perfomanceLeaderEmail;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("pl")
        @Expose
        private String pl;
        @SerializedName("productMetricsValidationType")
        @Expose
        private String productMetricsValidationType;
        @SerializedName("quarterback")
        @Expose
        private String quarterback;
        @SerializedName("regionId")
        @Expose
        private Integer regionId;
        @SerializedName("regionRegionTypeId")
        @Expose
        private Integer regionRegionTypeId;
        @SerializedName("regionName")
        @Expose
        private String regionName;
        @SerializedName("regionCurrency")
        @Expose
        private String regionCurrency;
        @SerializedName("rooms")
        @Expose
        private Integer rooms;
        @SerializedName("tenantId")
        @Expose
        private Integer tenantId;
        @SerializedName("tenantName")
        @Expose
        private String tenantName;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("weightLastMonth")
        @Expose
        private Integer weightLastMonth;
        @SerializedName("weightLastTwoMonth")
        @Expose
        private Integer weightLastTwoMonth;
        @SerializedName("weightLastThreeMonth")
        @Expose
        private Integer weightLastThreeMonth;
        @SerializedName("weightPriorYear")
        @Expose
        private Integer weightPriorYear;
        @SerializedName("deemphasizeByTierOne")
        @Expose
        private Integer deemphasizeByTierOne;
        @SerializedName("deemphasizeByTierTwo")
        @Expose
        private Integer deemphasizeByTierTwo;
        @SerializedName("deemphasizeByTierThree")
        @Expose
        private Integer deemphasizeByTierThree;
        @SerializedName("fbLocation")
        @Expose
        private Boolean fbLocation;
        @SerializedName("weeklyGoalCycle")
        @Expose
        private String weeklyGoalCycle;
        @SerializedName("monthlyGoalCycle")
        @Expose
        private String monthlyGoalCycle;
        @SerializedName("goalAutomationCycle")
        @Expose
        private String goalAutomationCycle;
        @SerializedName("goalDisableOn")
        @Expose
        private String goalDisableOn;
        @SerializedName("notifyOnCast")
        @Expose
        private Boolean notifyOnCast;
        @SerializedName("notifyOnUpdate")
        @Expose
        private Boolean notifyOnUpdate;
        @SerializedName("sendPushNotification")
        @Expose
        private Boolean sendPushNotification;
        @SerializedName("sendEmail")
        @Expose
        private Boolean sendEmail;
        @SerializedName("sendPulseMail")
        @Expose
        private Boolean sendPulseMail;
        @SerializedName("autoLockedProcessDate")
        @Expose
        private String autoLockedProcessDate;
        @SerializedName("autoLockedProcessNotifier")
        @Expose
        private String autoLockedProcessNotifier;
        @SerializedName("cnameTime")
        @Expose
        private String cnameTime;
        @SerializedName("unameTime")
        @Expose
        private String unameTime;
        @SerializedName("createdByWithDateTime")
        @Expose
        private String createdByWithDateTime;
        @SerializedName("updatedByWithDateTime")
        @Expose
        private String updatedByWithDateTime;
        @SerializedName("reviewedByWithDateTime")
        @Expose
        private String reviewedByWithDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIndustryId() {
            return industryId;
        }

        public void setIndustryId(Integer industryId) {
            this.industryId = industryId;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public Integer getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Integer createdOn) {
            this.createdOn = createdOn;
        }

        public Integer getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Integer updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getJsonData() {
            return jsonData;
        }

        public void setJsonData(String jsonData) {
            this.jsonData = jsonData;
        }

        public JsonMap getJsonMap() {
            return jsonMap;
        }

        public void setJsonMap(JsonMap jsonMap) {
            this.jsonMap = jsonMap;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getCityLead() {
            return cityLead;
        }

        public void setCityLead(String cityLead) {
            this.cityLead = cityLead;
        }

        public String getCityLeadEmail() {
            return cityLeadEmail;
        }

        public void setCityLeadEmail(String cityLeadEmail) {
            this.cityLeadEmail = cityLeadEmail;
        }

        public String getColumn1() {
            return column1;
        }

        public void setColumn1(String column1) {
            this.column1 = column1;
        }

        public String getDataDeliveryMethod() {
            return dataDeliveryMethod;
        }

        public void setDataDeliveryMethod(String dataDeliveryMethod) {
            this.dataDeliveryMethod = dataDeliveryMethod;
        }

        public String getDataStatus() {
            return dataStatus;
        }

        public void setDataStatus(String dataStatus) {
            this.dataStatus = dataStatus;
        }

        public String getDataStatusNotes() {
            return dataStatusNotes;
        }

        public void setDataStatusNotes(String dataStatusNotes) {
            this.dataStatusNotes = dataStatusNotes;
        }

        public String getDc1() {
            return dc1;
        }

        public void setDc1(String dc1) {
            this.dc1 = dc1;
        }

        public String getDc1Email() {
            return dc1Email;
        }

        public void setDc1Email(String dc1Email) {
            this.dc1Email = dc1Email;
        }

        public String getDc1Phone() {
            return dc1Phone;
        }

        public void setDc1Phone(String dc1Phone) {
            this.dc1Phone = dc1Phone;
        }

        public String getDc2() {
            return dc2;
        }

        public void setDc2(String dc2) {
            this.dc2 = dc2;
        }

        public String getDc2Email() {
            return dc2Email;
        }

        public void setDc2Email(String dc2Email) {
            this.dc2Email = dc2Email;
        }

        public String getDc2Phone() {
            return dc2Phone;
        }

        public void setDc2Phone(String dc2Phone) {
            this.dc2Phone = dc2Phone;
        }

        public String getEmailChampion() {
            return emailChampion;
        }

        public void setEmailChampion(String emailChampion) {
            this.emailChampion = emailChampion;
        }

        public String getEmailGm() {
            return emailGm;
        }

        public void setEmailGm(String emailGm) {
            this.emailGm = emailGm;
        }

        public String getExecutioveLounge() {
            return executioveLounge;
        }

        public void setExecutioveLounge(String executioveLounge) {
            this.executioveLounge = executioveLounge;
        }

        public Boolean getForce2fa() {
            return force2fa;
        }

        public void setForce2fa(Boolean force2fa) {
            this.force2fa = force2fa;
        }

        public String getFrontDeskPOS() {
            return frontDeskPOS;
        }

        public void setFrontDeskPOS(String frontDeskPOS) {
            this.frontDeskPOS = frontDeskPOS;
        }

        public String getFrontOfficeMgr() {
            return frontOfficeMgr;
        }

        public void setFrontOfficeMgr(String frontOfficeMgr) {
            this.frontOfficeMgr = frontOfficeMgr;
        }

        public String getFrontOfficeMgrEmail() {
            return frontOfficeMgrEmail;
        }

        public void setFrontOfficeMgrEmail(String frontOfficeMgrEmail) {
            this.frontOfficeMgrEmail = frontOfficeMgrEmail;
        }

        public String getGeneralManager() {
            return generalManager;
        }

        public void setGeneralManager(String generalManager) {
            this.generalManager = generalManager;
        }

        public String getGeoAddress() {
            return geoAddress;
        }

        public void setGeoAddress(String geoAddress) {
            this.geoAddress = geoAddress;
        }

        public Integer getGeoId() {
            return geoId;
        }

        public void setGeoId(Integer geoId) {
            this.geoId = geoId;
        }

        public String getGeoLocality() {
            return geoLocality;
        }

        public void setGeoLocality(String geoLocality) {
            this.geoLocality = geoLocality;
        }

        public String getGeoPostalcode() {
            return geoPostalcode;
        }

        public void setGeoPostalcode(String geoPostalcode) {
            this.geoPostalcode = geoPostalcode;
        }

        public String getGeoState() {
            return geoState;
        }

        public void setGeoState(String geoState) {
            this.geoState = geoState;
        }

        public String getHasSpa() {
            return hasSpa;
        }

        public void setHasSpa(String hasSpa) {
            this.hasSpa = hasSpa;
        }

        public String getHotelAuditorMain() {
            return hotelAuditorMain;
        }

        public void setHotelAuditorMain(String hotelAuditorMain) {
            this.hotelAuditorMain = hotelAuditorMain;
        }

        public String getHotelAuditorMainEmail() {
            return hotelAuditorMainEmail;
        }

        public void setHotelAuditorMainEmail(String hotelAuditorMainEmail) {
            this.hotelAuditorMainEmail = hotelAuditorMainEmail;
        }

        public String getHotelAuditorSecondary() {
            return hotelAuditorSecondary;
        }

        public void setHotelAuditorSecondary(String hotelAuditorSecondary) {
            this.hotelAuditorSecondary = hotelAuditorSecondary;
        }

        public String getHotelAuditorSecondaryEmail() {
            return hotelAuditorSecondaryEmail;
        }

        public void setHotelAuditorSecondaryEmail(String hotelAuditorSecondaryEmail) {
            this.hotelAuditorSecondaryEmail = hotelAuditorSecondaryEmail;
        }

        public String getHotelMetricsDataType() {
            return hotelMetricsDataType;
        }

        public void setHotelMetricsDataType(String hotelMetricsDataType) {
            this.hotelMetricsDataType = hotelMetricsDataType;
        }

        public String getHotelMktTier() {
            return hotelMktTier;
        }

        public void setHotelMktTier(String hotelMktTier) {
            this.hotelMktTier = hotelMktTier;
        }

        public Boolean getIsMasterLocation() {
            return isMasterLocation;
        }

        public void setIsMasterLocation(Boolean isMasterLocation) {
            this.isMasterLocation = isMasterLocation;
        }

        public String getLaunchDate() {
            return launchDate;
        }

        public void setLaunchDate(String launchDate) {
            this.launchDate = launchDate;
        }

        public String getLegalName() {
            return legalName;
        }

        public void setLegalName(String legalName) {
            this.legalName = legalName;
        }

        public String getLocationID() {
            return locationID;
        }

        public void setLocationID(String locationID) {
            this.locationID = locationID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getNoOfWorkingDaysPerMonth() {
            return noOfWorkingDaysPerMonth;
        }

        public void setNoOfWorkingDaysPerMonth(Integer noOfWorkingDaysPerMonth) {
            this.noOfWorkingDaysPerMonth = noOfWorkingDaysPerMonth;
        }

        public String getPerfomanceLeaderEmail() {
            return perfomanceLeaderEmail;
        }

        public void setPerfomanceLeaderEmail(String perfomanceLeaderEmail) {
            this.perfomanceLeaderEmail = perfomanceLeaderEmail;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPl() {
            return pl;
        }

        public void setPl(String pl) {
            this.pl = pl;
        }

        public String getProductMetricsValidationType() {
            return productMetricsValidationType;
        }

        public void setProductMetricsValidationType(String productMetricsValidationType) {
            this.productMetricsValidationType = productMetricsValidationType;
        }

        public String getQuarterback() {
            return quarterback;
        }

        public void setQuarterback(String quarterback) {
            this.quarterback = quarterback;
        }

        public Integer getRegionId() {
            return regionId;
        }

        public void setRegionId(Integer regionId) {
            this.regionId = regionId;
        }

        public Integer getRegionRegionTypeId() {
            return regionRegionTypeId;
        }

        public void setRegionRegionTypeId(Integer regionRegionTypeId) {
            this.regionRegionTypeId = regionRegionTypeId;
        }

        public String getRegionName() {
            return regionName;
        }

        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }

        public String getRegionCurrency() {
            return regionCurrency;
        }

        public void setRegionCurrency(String regionCurrency) {
            this.regionCurrency = regionCurrency;
        }

        public Integer getRooms() {
            return rooms;
        }

        public void setRooms(Integer rooms) {
            this.rooms = rooms;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }

        public String getTenantName() {
            return tenantName;
        }

        public void setTenantName(String tenantName) {
            this.tenantName = tenantName;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public Integer getWeightLastMonth() {
            return weightLastMonth;
        }

        public void setWeightLastMonth(Integer weightLastMonth) {
            this.weightLastMonth = weightLastMonth;
        }

        public Integer getWeightLastTwoMonth() {
            return weightLastTwoMonth;
        }

        public void setWeightLastTwoMonth(Integer weightLastTwoMonth) {
            this.weightLastTwoMonth = weightLastTwoMonth;
        }

        public Integer getWeightLastThreeMonth() {
            return weightLastThreeMonth;
        }

        public void setWeightLastThreeMonth(Integer weightLastThreeMonth) {
            this.weightLastThreeMonth = weightLastThreeMonth;
        }

        public Integer getWeightPriorYear() {
            return weightPriorYear;
        }

        public void setWeightPriorYear(Integer weightPriorYear) {
            this.weightPriorYear = weightPriorYear;
        }

        public Integer getDeemphasizeByTierOne() {
            return deemphasizeByTierOne;
        }

        public void setDeemphasizeByTierOne(Integer deemphasizeByTierOne) {
            this.deemphasizeByTierOne = deemphasizeByTierOne;
        }

        public Integer getDeemphasizeByTierTwo() {
            return deemphasizeByTierTwo;
        }

        public void setDeemphasizeByTierTwo(Integer deemphasizeByTierTwo) {
            this.deemphasizeByTierTwo = deemphasizeByTierTwo;
        }

        public Integer getDeemphasizeByTierThree() {
            return deemphasizeByTierThree;
        }

        public void setDeemphasizeByTierThree(Integer deemphasizeByTierThree) {
            this.deemphasizeByTierThree = deemphasizeByTierThree;
        }

        public Boolean getFbLocation() {
            return fbLocation;
        }

        public void setFbLocation(Boolean fbLocation) {
            this.fbLocation = fbLocation;
        }

        public String getWeeklyGoalCycle() {
            return weeklyGoalCycle;
        }

        public void setWeeklyGoalCycle(String weeklyGoalCycle) {
            this.weeklyGoalCycle = weeklyGoalCycle;
        }

        public String getMonthlyGoalCycle() {
            return monthlyGoalCycle;
        }

        public void setMonthlyGoalCycle(String monthlyGoalCycle) {
            this.monthlyGoalCycle = monthlyGoalCycle;
        }

        public String getGoalAutomationCycle() {
            return goalAutomationCycle;
        }

        public void setGoalAutomationCycle(String goalAutomationCycle) {
            this.goalAutomationCycle = goalAutomationCycle;
        }

        public String getGoalDisableOn() {
            return goalDisableOn;
        }

        public void setGoalDisableOn(String goalDisableOn) {
            this.goalDisableOn = goalDisableOn;
        }

        public Boolean getNotifyOnCast() {
            return notifyOnCast;
        }

        public void setNotifyOnCast(Boolean notifyOnCast) {
            this.notifyOnCast = notifyOnCast;
        }

        public Boolean getNotifyOnUpdate() {
            return notifyOnUpdate;
        }

        public void setNotifyOnUpdate(Boolean notifyOnUpdate) {
            this.notifyOnUpdate = notifyOnUpdate;
        }

        public Boolean getSendPushNotification() {
            return sendPushNotification;
        }

        public void setSendPushNotification(Boolean sendPushNotification) {
            this.sendPushNotification = sendPushNotification;
        }

        public Boolean getSendEmail() {
            return sendEmail;
        }

        public void setSendEmail(Boolean sendEmail) {
            this.sendEmail = sendEmail;
        }

        public Boolean getSendPulseMail() {
            return sendPulseMail;
        }

        public void setSendPulseMail(Boolean sendPulseMail) {
            this.sendPulseMail = sendPulseMail;
        }

        public String getAutoLockedProcessDate() {
            return autoLockedProcessDate;
        }

        public void setAutoLockedProcessDate(String autoLockedProcessDate) {
            this.autoLockedProcessDate = autoLockedProcessDate;
        }

        public String getAutoLockedProcessNotifier() {
            return autoLockedProcessNotifier;
        }

        public void setAutoLockedProcessNotifier(String autoLockedProcessNotifier) {
            this.autoLockedProcessNotifier = autoLockedProcessNotifier;
        }

        public String getCnameTime() {
            return cnameTime;
        }

        public void setCnameTime(String cnameTime) {
            this.cnameTime = cnameTime;
        }

        public String getUnameTime() {
            return unameTime;
        }

        public void setUnameTime(String unameTime) {
            this.unameTime = unameTime;
        }

        public String getCreatedByWithDateTime() {
            return createdByWithDateTime;
        }

        public void setCreatedByWithDateTime(String createdByWithDateTime) {
            this.createdByWithDateTime = createdByWithDateTime;
        }

        public String getUpdatedByWithDateTime() {
            return updatedByWithDateTime;
        }

        public void setUpdatedByWithDateTime(String updatedByWithDateTime) {
            this.updatedByWithDateTime = updatedByWithDateTime;
        }

        public String getReviewedByWithDateTime() {
            return reviewedByWithDateTime;
        }

        public void setReviewedByWithDateTime(String reviewedByWithDateTime) {
            this.reviewedByWithDateTime = reviewedByWithDateTime;
        }

    }
    public class JsonMap {


    }
}
