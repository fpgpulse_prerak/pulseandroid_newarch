package com.ingauge.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mansurum on 25-Jul-17.
 */

public class ReplyMailBody {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("replyId")
    @Expose
    private Integer replyId;
    @SerializedName("mailFileList")
    @Expose
    private String[] mailFileList = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    public String[] getMailFileList() {
        return mailFileList;
    }

    public void setMailFileList(String[] mailFileList) {
        this.mailFileList = mailFileList;
    }

}
