package com.ingauge.pojo;

/**
 * Created by mansurum on 03-Jan-18.
 */

public class VideoModel {

    private int id;
    private int industryId;
    private String activeStatus = "";
    private String createdOn = "";
    private String updatedOn = "";
    private boolean indexRelatedOnly = false;
    private boolean indexMainOnly = false;
    private String mediaId = "";
    private String name = "";
    private String duration = "";
    private String description = "";
    private String status = "";
    private String thumbnailURL = "";
    private String projectId = "";
    private String projectName = "";
    private String embedCode = "";
    private int playCount;
    private String section = "";
    private String mediaAssets = "";
    private boolean isDeleted = false;
    private String roleMedia = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndustryId() {
        return industryId;
    }

    public void setIndustryId(int industryId) {
        this.industryId = industryId;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public boolean isIndexRelatedOnly() {
        return indexRelatedOnly;
    }

    public void setIndexRelatedOnly(boolean indexRelatedOnly) {
        this.indexRelatedOnly = indexRelatedOnly;
    }

    public boolean isIndexMainOnly() {
        return indexMainOnly;
    }

    public void setIndexMainOnly(boolean indexMainOnly) {
        this.indexMainOnly = indexMainOnly;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getEmbedCode() {
        return embedCode;
    }

    public void setEmbedCode(String embedCode) {
        this.embedCode = embedCode;
    }

    public int getPlayCount() {
        return playCount;
    }

    public void setPlayCount(int playCount) {
        this.playCount = playCount;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getMediaAssets() {
        return mediaAssets;
    }

    public void setMediaAssets(String mediaAssets) {
        this.mediaAssets = mediaAssets;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getRoleMedia() {
        return roleMedia;
    }

    public void setRoleMedia(String roleMedia) {
        this.roleMedia = roleMedia;
    }
}
