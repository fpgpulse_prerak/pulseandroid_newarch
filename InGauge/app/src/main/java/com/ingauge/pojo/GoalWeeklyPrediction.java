package com.ingauge.pojo;


import java.io.Serializable;
import java.util.List;

/**
 * Created by davepriy on 3/2/2016.
 */
public class GoalWeeklyPrediction implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<GoalDayWiseReport> dayWiseReport;
	private Double earnedRevenueWeekly;
	private Double reqRevWeeklyFPG;
	private Double reqRevWeeklySelf;
	private Double unitsRequiredPerDayFPG;
	private Double unitsRequiredPerDaySelf;
	private Double weekStartDeltaFPG;
	private Double weekStartDeltaSelf;
	private Boolean weeklyGoalAcheivedFPG;
	private Boolean weeklyGoalAcheivedSelf;

	public Double getUnitsRequiredPerDayFPG() {
		return unitsRequiredPerDayFPG;
	}

	public void setUnitsRequiredPerDayFPG(Double unitsRequiredPerDayFPG) {
		this.unitsRequiredPerDayFPG = unitsRequiredPerDayFPG;
	}

	public List<GoalDayWiseReport> getDayWiseReport() {
		return dayWiseReport;
	}

	public void setDayWiseReport(List<GoalDayWiseReport> dayWiseReport) {
		this.dayWiseReport = dayWiseReport;
	}

	public Double getEarnedRevenueWeekly() {
		return earnedRevenueWeekly;
	}

	public void setEarnedRevenueWeekly(Double earnedRevenueWeekly) {
		this.earnedRevenueWeekly = earnedRevenueWeekly;
	}

	public Double getReqRevWeeklyFPG() {
		return reqRevWeeklyFPG;
	}

	public void setReqRevWeeklyFPG(Double reqRevWeeklyFPG) {
		this.reqRevWeeklyFPG = reqRevWeeklyFPG;
	}

	public Double getReqRevWeeklySelf() {
		return reqRevWeeklySelf;
	}

	public void setReqRevWeeklySelf(Double reqRevWeeklySelf) {
		this.reqRevWeeklySelf = reqRevWeeklySelf;
	}

	public Double getUnitsRequiredPerDaySelf() {
		return unitsRequiredPerDaySelf;
	}

	public void setUnitsRequiredPerDaySelf(Double unitsRequiredPerDaySelf) {
		this.unitsRequiredPerDaySelf = unitsRequiredPerDaySelf;
	}

	public Double getWeekStartDeltaFPG() {
		return weekStartDeltaFPG;
	}

	public void setWeekStartDeltaFPG(Double weekStartDeltaFPG) {
		this.weekStartDeltaFPG = weekStartDeltaFPG;
	}

	public Double getWeekStartDeltaSelf() {
		return weekStartDeltaSelf;
	}

	public void setWeekStartDeltaSelf(Double weekStartDeltaSelf) {
		this.weekStartDeltaSelf = weekStartDeltaSelf;
	}

	public Boolean getWeeklyGoalAcheivedFPG() {
		return weeklyGoalAcheivedFPG;
	}

	public void setWeeklyGoalAcheivedFPG(Boolean weeklyGoalAcheivedFPG) {
		this.weeklyGoalAcheivedFPG = weeklyGoalAcheivedFPG;
	}

	public Boolean getWeeklyGoalAcheivedSelf() {
		return weeklyGoalAcheivedSelf;
	}

	public void setWeeklyGoalAcheivedSelf(Boolean weeklyGoalAcheivedSelf) {
		this.weeklyGoalAcheivedSelf = weeklyGoalAcheivedSelf;
	}


}
